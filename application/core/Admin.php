<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Core controller Admin
class Admin extends MX_Controller {

	protected $data;
	protected $meta;
	protected $title;
	protected $menu;
	protected $theme_id;
	protected $zone_id;
	protected $zone;
	protected $admin_id;
	protected $role_id;
	protected $s_login;
	protected $module_pkey;
	protected $module_key;
	protected $module_name;
	protected $module_link;
	protected $user;
	protected $copyright;
	protected $provided;
	protected $profile;
	protected $logout;
	protected $login;
	protected $dashboard;
	protected $language;
	protected $role_language;
	protected $is_active_domain;
	protected $notif;
	
	function __construct()
    {
		
        parent::__construct();
		if ( function_exists( 'date_default_timezone_set' ) )
        date_default_timezone_set('Asia/Jakarta');
		if(in_array($_SERVER['HTTP_HOST'],array("mars.rezero.id","www.mars.rezero.id"))){
			if(strpos($_SERVER['HTTP_HOST'], 'www.') !== false){
				$a_zone = explode('.',$_SERVER['HTTP_HOST']);
				$this->zone = $a_zone[1];
			}else{
				$a_zone = explode('.',$_SERVER['HTTP_HOST']);
				$this->zone = $a_zone[0];
			}
			$is_active_domain = TRUE;
			$this->is_active_domain = TRUE;
		}else{
			$this->zone = strtolower($this->uri->segment(1, 0));
			$is_active_domain = FALSE;
			$this->is_active_domain = FALSE;
		}
		if($this->dbfun_model->get_data_bys('ccs_id','LOWER(name) like "'.$this->zone.'"','zone')){
			$cek = $this->dbfun_model->get_data_bys('ccs_id, copyright,theme_id','LOWER(name) like "'.$this->zone.'"','zone');
			$this->zone_id = $cek->ccs_id;
			$this->theme_id = $cek->theme_id;
		}
        $role = $this->session->userdata($this->zone.'_role');
		$this->notif = array();
		$this->meta = array(
        	'description' => 'Dashboard',
        	'author' => 'Fadhil - '
        );
		$this->user = array(
        	'nama' => $this->session->userdata($this->zone.'_nama'),
        	'tipe' => $this->session->userdata($this->zone.'_type'),
        	'profpic' => $this->session->userdata($this->zone.'_foto')
        );
		if($this->session->userdata($this->zone.'_type')){
			$menu_parent = $this->dbfun_model->get_all_data_bys_order('distinct(ccs_mdl.module_id), mdl.parent_id, mdl.icon, mdl.name,mdl.link','ccs_mdr.type_id = '.$this->session->userdata($this->zone.'_role').' and ccs_mdl.status != 0 and ccs_mdl.module_id = ccs_mtr.module_id and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdr.type_id = ccs_mtr.role_id and ccs_mdl.parent_id = 0','mdl, mtr, mdr', 'ccs_mdl.module_id', 'ASC');
				foreach($menu_parent as $mp){
					$menu_child = $this->dbfun_model->get_all_data_bys('distinct(ccs_mdl.module_id), mdl.parent_id, mdl.icon, mdl.name, mdl.link','ccs_mdl.module_id = ccs_mtr.module_id and ccs_mdr.type_id = ccs_mtr.role_id and ccs_mdl.status !=0 and ccs_mdl.parent_id = '.$mp->module_id.'','mdl, mtr, mdr');
					if($is_active_domain){
						$menu[] = array('menu' => $mp, 'link' => str_replace('.co.id/','.co.id',base_url()).'/admin', 'child' => $menu_child);
					}else{
						$menu[] = array('menu' => $mp, 'link' => base_url($this->zone).'/admin', 'child' => $menu_child);
					}
					if($this->notification($mp->name)){
						$this->notif[] = $this->notification($mp->name);
					};
				}
			if($is_active_domain){
				$menu['link'] = 'http://'.$_SERVER['HTTP_HOST'];
			}else{
				$menu['link'] = base_url($this->zone);
			}	
			$this->menu = $menu;
			if($is_active_domain){
				$uri = $this->uri->segment(2, 0);
			}else{
				$uri = $this->uri->segment(3, 0);
			}
			if($uri){
				$module = $this->dbfun_model->get_data_bys('name,link,module_id,parent_id','ccs_mdl.link like "'.$uri.'" and ccs_mdl.ccs_id = '.$this->zone_id.'','mdl');
				if($module){
					$this->module_pkey = $module->parent_id;
					$this->module_key = $module->module_id;
					$this->module_name = $module->name;
					$this->module_link = $module->link;
				}
			}
			$this->s_login = $this->session->userdata($this->zone.'_logged_in');
			$this->admin_id = $this->session->userdata($this->zone.'_admin_id');
			$this->role_id = $this->session->userdata($this->zone.'_role');
		}
		
		$this->title = ''.strtoupper($this->zone).' | Dashboard';
		$this->copyright = ''.$this->zone.'';
		//if(strtolower($this->uri->segment(0, 0)) == 'project'){
			$this->provided = $cek->copyright;
		/* }else{
			$this->provided = 'CANDRADIMUKA CS';
		} */
		$this->profile = 'students/user_management';
		if($is_active_domain){
			$this->logout = 'admin/logout';
			$this->login = 'admin/login';
			$this->dashboard = 'admin/dashboard';
		}else{
			$this->logout = ''.$this->zone.'/admin/logout';
			$this->login = ''.$this->zone.'/admin/login';
			$this->dashboard = ''.$this->zone.'/admin/dashboard';
		}
        
        $this->load->helper('array');
    }

    protected function log_user($admin_id, $modul, $url_id, $tipe_log)
    {
    	$data_log = array(
    		'admin_id' => $admin_id,
			'ccs_id' => $this->zone_id,
    		'ccs_key' => $modul,
			'url_id' => $url_id,
			'type' => $tipe_log
    	);
    	$this->dbfun_model->ins_to('adl', $data_log);
    }

    protected function is_mobile()
	{
		return preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|bo‌​ost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}
	
	protected function send_mail($receiver, $subject, $content, $view,$by)
	{
		$email_settings = $this->dbfun_model->get_data_bys('char_1,char_2,char_3,char_4,char_5,char_6','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "'.$by.'" and status = 1','orc');
		if(!empty($email_settings)){
			$port = $email_settings->char_1;
			$smtp_host = $email_settings->char_2;
			$smtp_pass = str_replace('b03zc0n','',$email_settings->char_3);
			$from = $email_settings->char_4;
			$smtp_user = $email_settings->char_5;
			if($port == '465'){
				$protocol = 'smtp';
			}else{
				$protocol = 'sendmail';
			}
		}else{
			$port = '';
			$smtp_host = '';
			$smtp_pass = '';
			$from = '';
			$smtp_user = '';
			$protocol = '';
		}
		$data = $content;
		$message = $this->load->view($view, $data, true);
		
		$config['protocol'] = $protocol;
		$config['smtp_host'] = $smtp_host;
		$config['smtp_user'] = $smtp_user;
		$config['smtp_pass'] = $smtp_pass;
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$config['validate'] = TRUE;
		$config['priority'] = 1;

		$this->email->initialize($config);
		
		$this->email->from($smtp_user, $from);
		$this->email->to($receiver); 

		$this->email->subject($subject);
		$this->email->message($message);	
		
		$this->email->send();
	}
	
	protected function send_mail_attach($receiver, $subject, $content, $attach, $view,$by)
	{
		$email_settings = $this->dbfun_model->get_data_bys('char_1,char_2,char_3,char_4,char_5,char_6','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "'.$by.'" and status = 1','orc');
		if(!empty($email_settings)){
			$port = $email_settings->char_1;
			$smtp_host = $email_settings->char_2;
			$smtp_pass = str_replace('b03zc0n','',$email_settings->char_3);
			$from = $email_settings->char_4;
			$smtp_user = $email_settings->char_5;
			if($port == '465'){
				$protocol = 'smtp';
			}else{
				$protocol = 'sendmail';
			}
		}else{
			$port = '';
			$smtp_host = '';
			$smtp_pass = '';
			$from = '';
			$smtp_user = '';
			$protocol = '';
		}
		$data = $content;
		$message = $this->load->view($view, $data, true);
		
		$config['protocol'] = $protocol;
		$config['smtp_host'] = $smtp_host;
		$config['smtp_user'] = $smtp_user;
		$config['smtp_pass'] = $smtp_pass;
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$config['validate'] = TRUE;
		$config['priority'] = 1;
		$config['crlf'] = "\r\n";
		$config['newline'] = "\r\n";

		$this->email->initialize($config);
		
		$this->email->from($smtp_user, $from);
		$this->email->to($receiver); 

		$this->email->subject($subject);
		$this->email->message($message);	
		
		if(is_array($attach)){
			foreach($attach as $a){
				$this->email->attach($a);
			}
		}else{
			if(!empty($attach)){
				$this->email->attach($attach);
			}
		}
		
		$this->email->send();
		//return $this->email->print_debugger();
	}
	
	protected function create_pdf($path,$data)
	{
		$this->load->library('m_pdf');
		$html=$this->load->view('invoice', $data, true);
		$pdfFilePath = FCPATH.$path;//"/assets/shisha/test.pdf";
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->Output($pdfFilePath, "F"); 
	}
	
	protected function update_data_bys($select, $where, $table, $file_column, $path, $file, $type)
	{  
		foreach($_POST as $key => $val)  
		{
			$data1[$key] = $this->input->post($key); 
			if($key != 'updatedby'){
				$data1['updatedby'] = $this->admin_id;
			}
			if($key != 'dateupdated'){
				$data1['dateupdated'] = date('Y-md-d H:i:s');
			}
		}
		$data2 = get_object_vars($this->dbfun_model->get_data_bys($select,$where,$table));
		if(isset($data1['ccs_key']) && isset($data1['url_id'])){
			$this->log_user($this->admin_id, $data1['ccs_key'],$data1['url_id'], 3);
		}
		if($file){
			if (!is_dir('assets/'.$path.'/'))
			{
				mkdir('./assets/'.$path.'/', 0777, true);
			}
			if(!empty($data2[$file_column])){
				$file_path = './assets/'.$path.'/'.trim($data2[$file_column]);
				if (file_exists($file_path))
				{
					unlink($file_path);
				}
			};
			
			$nama_file = trim($file.'-'.date('Ymd_His'));
			
			if (strpos($type, 'mp4') === false)
			{
				$config = unggah_berkas($path, $nama_file, $type);
			}else{
				$config = unggah_file($path, $nama_file, $type);
			}
			
			$this->upload->initialize($config);
			
			if (!$this->upload->do_upload($file))
			{
				$status = 'error';
				$message = $this->upload->display_errors('', '');
			}else{
				$upload_data = $this->upload->data();
				$data1[$file_column] = $nama_file.$upload_data['file_ext'];
			}
		}
		$datas = array_intersect_key(array_diff_assoc($data1, $data2),$data2);
		return $datas;
		//$this->dbfun_model->update_table($where, $datas, $table);
	}
	
	protected function new_data($table,$custom, $column,$file,$path,$type)
	{  
		foreach($_POST as $key => $val)  
		{
			$data1[$key] = $this->input->post($key);  
			if($key != 'createdby'){
				$data1['createdby'] = $this->admin_id;
			}
		}
		if($table){
			$q = $this->db->list_fields($table);
			foreach($q as $key => $v){
				$data2[$v] = ''; 
			}
		}
		if($custom){
			foreach($custom as $key => $value){
				$data1[$key] = $value;
			}
		}
		
		if($file){
			if (!is_dir('assets/'.$path.'/'))
			{
				mkdir('./assets/'.$path.'/', 0777, true);
			}
			$nama_file = trim($file.'-'.date('Ymd_His'));
			if (strpos($type, 'mp4') === false)
			{
				$config = unggah_berkas($path, $nama_file, $type);
			}else{
				$config = unggah_file($path, $nama_file, $type);
			}
			$this->upload->initialize($config);
			
			if (!$this->upload->do_upload($file))
			{
				$status = 'error';
				$message = $this->upload->display_errors('', '');
			}else{
				$upload_data = $this->upload->data();
				$data1[$column] = $nama_file.$upload_data['file_ext'];
			}
		}
		
		if($table){
			$datas = array_intersect_key(array_diff_assoc($data1, $data2),$data2);
		}else{
			$datas = $data1;
		}
		$return_id = $this->dbfun_model->ins_return_id($table,$datas);
		if(isset($data1['ccs_key'])){
			$this->log_user($this->admin_id, $data1['ccs_key'],$return_id, 1);
		}
		return $return_id;
	}
	
	protected function new_data_secondary($table, $custom, $column,$file,$path,$type)
	{  
		foreach($_POST as $key => $val)  
		{
			$data1[$key] = $this->input->post($key);  
		}
		$q = $this->db->list_fields($table);
		foreach($q as $key => $v){
			$data2[$v] = ''; 
		}
		if($file){
			$nama_file = trim($file.'-'.date('Ymd_His'));
			if (strpos($type, 'mp4') === false)
			{
				$config = unggah_berkas($path, $nama_file, $type);
			}else{
				$config = unggah_file($path, $nama_file, $type);
			}
			$this->upload->initialize($config);
			
			if (!$this->upload->do_upload($file))
			{
				$status = 'error';
				$message = $this->upload->display_errors('', '');
			}else{
				$upload_data = $this->upload->data();
				$data1[$column] = $nama_file.$upload_data['file_ext'];
			}
		}
		if($custom){
			foreach($custom as $key => $value){
				$data1[$key] = $value;
			}
		}
		$datas = array_intersect_key(array_diff_assoc($data1, $data2),$data2);
		
		return $datas;
	}
	
	protected function cek_n_delete_data($p_select, $p_where, $p_table, $file_column, $path, $s_where, $s_table, $param)
	{
		$cek_isi = get_object_vars($this->dbfun_model->get_data_bys($p_select,$p_where,$p_table));
		if($s_table)
		{
			$cek = $this->dbfun_model->count_result($s_where,$s_table);
			if ($cek != 1)
			{
				$this->dbfun_model->del_tables($param, $s_table);
			}else{
				if($file_column){
					$file_path = './assets/'.$path.'/'.trim($cek_isi[$file_column]);
					if(!empty($cek_isi[$file_column])){
						if (file_exists($file_path))
						{
							unlink($file_path);
						}
					}
				}
				$this->dbfun_model->del_tables($param, $s_table);
				
				$this->dbfun_model->del_tables($p_where, $p_table);
			}
		}else{
			if($file_column){
				$file_path = './assets/'.$path.'/'.trim($cek_isi[$file_column]);
				if(!empty($cek_isi[$file_column])){
					if (file_exists($file_path))
					{
						unlink($file_path);
					}
				}
			}
			$this->dbfun_model->del_tables($p_where, $p_table);
		}
		//$this->log_user($this->admin_id, $ccs_key, $this->input->post('id'), 4);
	}
	
	protected function module_to_ccs_key($module)
	{
		$cek = $this->dbfun_model->get_data_bys('module_id','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.link like "'.$module.'"','mdl');
		if($cek){
			$ccs_key = $cek->module_id;
		}else{
			$ccs_key = '';
		}
		return $ccs_key;
	}
	
	protected function module_to_all_category($module)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($ccs_key)){
			$cek = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,parent_title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and parent_id = 0 and ccs_oct.status != 0','oct');
			foreach($cek as $c){
				$total = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$c->category_id.' and status != 0 ','odc'));
				$cek2 = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,parent_title','ccs_oct.ccs_id = '.$this->zone_id.' and parent_id = '.$c->category_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0','oct');
				$total_child = 0;
				$cc2 = array();
				foreach($cek2 as $c2){
					$total2 = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$c2->category_id.' and status != 0 ','odc'));
					$cc2[] = (object) array_merge((array) $c2, array('sum' => $total2));
					$total_child += $total2;
				}
				if(isset($total_child)){
					$total = $total + $total_child;
				}
				$cc[] = (object) array_merge((array) $c, array('sum' => $total,'child'=>$cc2));
			}
			if(!empty($cc)){
				$categorys = $cc;
			}else{
				$categorys = FALSE;
			}
		}else{
			$categorys = FALSE;
		}
		return $categorys;
	}
	
	protected function module_to_all_category_w_desc($module)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($ccs_key)){
			$cek = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,parent_title,description','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and parent_id = 0 and ccs_oct.status != 0','oct');
			foreach($cek as $c){
				$total = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$c->category_id.' and status != 0 ','odc'));
				$cek2 = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,parent_title,description','ccs_oct.ccs_id = '.$this->zone_id.' and parent_id = '.$c->category_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0','oct');
				$total_child = 0;
				$cc2 = array();
				foreach($cek2 as $c2){
					$total2 = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$c2->category_id.' and status != 0 ','odc'));
					$cc2[] = (object) array_merge((array) $c2, array('sum' => $total2));
					$total_child += $total2;
				}
				if(isset($total_child)){
					$total = $total + $total_child;
				}
				$cc[] = (object) array_merge((array) $c, array('sum' => $total,'child'=>$cc2));
			}
			if(!empty($cc)){
				$categorys = $cc;
			}else{
				$categorys = FALSE;
			}
		}else{
			$categorys = FALSE;
		}
		return $categorys;
	}
	
	protected function category_to_category_id($category)
	{
		$cek = $this->dbfun_model->get_data_bys('category_id','ccs_oct.ccs_id = '.$this->zone_id.' and LOWER(ccs_oct.title) like "'.strtolower($category).'"','oct');
		if(!empty($cek)){
			$category_id = $cek->category_id;
		}else{
			$category_id = FALSE;
		}
		
		return $category_id;
	}
	
	protected function category_id_to_category($ccs_key,$category_id)
	{
		$cek = $this->dbfun_model->get_data_bys('title','ccs_oct.ccs_id = '.$this->zone_id.' and status != 0 and category_id = '.$category_id.' and ccs_key = '.$ccs_key.'','oct');
		if(!empty($cek)){
			$category = $cek->title;
		}else{
			$category = FALSE;
		}
		
		return $category;
	}
	
	protected function module_to_category_id($module, $category)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($ccs_key)){
			$cek = $this->dbfun_model->get_data_bys('category_id','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and LOWER(ccs_oct.title) like "'.$category.'" and ccs_oct.status != 0','oct');
			if(!empty($cek)){
				$category_id = $cek->category_id;
			}else{
				$category_id = FALSE;
			}
		}else{
			$category_id = FALSE;
		}
		return $category_id;
	}
	
	protected function get_all_object($module, $category_id, $language_id)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($category_id)){
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$category_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' ','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_all_object_order_by($module, $category_id, $language_id,$order)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($category_id)){
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$category_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' '.$order.' ','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_all_object_from_module($module, $language_id)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($ccs_key)){
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus,ccs_odc.category_id','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status != 0 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' ','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_all_object_from_module_with_order_by_limit($module, $category_id, $language_id,$order,$limit,$offset)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($ccs_key)){
			$datas = $this->dbfun_model->get_all_data_bys('ccs_o.datecreated,ccs_o.dateupdated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status != 0 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$category_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' '.$order.' '.$limit.' '.$offset.'','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_all_object_from_module_with_order_by_limit_no_cat($module, $language_id,$order,$limit,$offset)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($ccs_key)){
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.category_id,ccs_odc.description,ccs_o.datecreated,ccs_o.dateupdated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' '.$order.' '.$limit.' '.$offset.'','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_all_object_from_module_with_order_by_limit_no_cat_pagination($module, $language_id,$order,$limit,$offset)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($ccs_key)){
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.category_id,ccs_o.datecreated,ccs_o.dateupdated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' '.$order.' '.$limit.' '.$offset.'','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_all_object_with_order_by_limit($module, $category_id, $language_id)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($category_id)){
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_odc.ccs_key,ccs_odc.category_id,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status != 0 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$category_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id,'o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_all_object_gallery($module,$category_id,$object_id)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($category_id)){
			$datas = $this->dbfun_model->get_all_data_bys('gallery_id,object_id,image_square,title,description,istatus','ccs_id = '.$this->zone_id.' and category_id = '.$category_id.' and ccs_key = '.$ccs_key.' and object_id = '.$object_id.' and language_id = '.$this->current_language.' ','ogl');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_object_gallery_main_image($object_id)
	{
		if(!empty($object_id)){
			$datas = $this->dbfun_model->get_data_bys('object_id,image_square,istatus','ccs_id = '.$this->zone_id.' and object_id = '.$object_id.' and language_id = '.$this->current_language.' and istatus = 0','ogl');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_prc($module,$category_id,$object_id)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(empty($ccs_key)){
			$ccs_key = $module;
		}
		if(!empty($category_id)){
			$datas = $this->dbfun_model->get_data_bys('price_id,publish,discount,stock,dstatus,vstatus','ccs_id = '.$this->zone_id.' and category_id = '.$category_id.' and ccs_key = '.$ccs_key.' and object_id = '.$object_id.' ','prc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_variant($module,$category_id,$object_id)
	{
		$ccs_key = $this->module_to_ccs_key($module);
		if(!empty($category_id)){
			$datas = $this->dbfun_model->get_all_data_bys('distinct(settings_id),varian_id,publish,stock','ccs_prv.ccs_id = '.$this->zone_id.' and ccs_prv.category_id = '.$category_id.' and ccs_prv.ccs_key = '.$ccs_key.' and ccs_prv.object_id = '.$object_id.' and ccs_prv.status = 1 and stock != 0','prv');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_ocs($settings_id)
	{
		if(!empty($settings_id)){
			$datas = $this->dbfun_model->get_data_bys('ccs_ocs.image_square,ccs_ocs.title,ccs_oct.title as category,rule_1,rule_2,','ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.settings_id = '.$settings_id.' and ccs_ocs.status = 1 and ccs_ocs.ccs_id = ccs_oct.ccs_id and ccs_ocs.ccs_key = ccs_oct.ccs_key','ocs,oct');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_tkt($oid,$variant)
	{
		if(!empty($oid)){
			$datas = $this->dbfun_model->get_data_bys('oid,discount','ccs_id = '.$this->zone_id.' and oid = '.$oid.' and varian_id = '.$variant.' and stock != 0 and status = 1','ccs_tkt');
			if(empty($datas)){
				$datas = '';
			}
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_object_by_title($title, $category_id,$language_id)
	{
		if(!empty($category_id)){
			$datas = $this->dbfun_model->get_data_bys('ccs_o.datecreated,ccs_o.dateupdated,ccs_odc.datepublish, ccs_odc.tagline,ccs_o.object_id,ccs_o.image_square,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$category_id.' and ccs_odc.language_id = '.$language_id.' and LOWER(ccs_odc.title) like "'.strtolower($title).'"','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_popular_by_module($module,$language_id,$param)
	{
		if(!empty($module)){
			$ccs_key = $this->module_to_ccs_key($module);
			if(empty($param)){
				$param = 'order by ccs_odc.viewer DESC limit 5';
			}
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status != 0 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' '.$param.'','o, odc');
		}else{
			$datas = '';
		}
		
		return $datas;
	}
	
	protected function get_popular_by_category($module,$category,$language_id,$param,$limit)
	{
		if(!empty($module)){
			$ccs_key = $this->module_to_ccs_key($module);
			$category_id = $this->category_to_category_id($category);
			
			if(empty($limit)){
				$limit = 'limit 5';
			}
			if(empty($param)){
				$param = 'order by ccs_odc.viewer DESC '.$limit.'';
				$db_param = '';
			}else{
				if($param == 'prc'){
					$param = 'and ccs_prc.object_id = ccs_odc.object_id order by ccs_prc.sold DESC '.$limit.'';
					$db_param = ', prc';
				}else{
					$db_param = '';
				}
			}
			$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status != 0 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$category_id.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$language_id.' '.$param.'','o, odc '.$db_param.'');
		}else{
			$datas = '';
		}
		
		return $datas;
	}

	protected function glue($array,$url)
	{
		$cek_frt = $this->dbfun_model->get_data_bys('shell','ccs_id = '.$this->zone_id.' and type like "data" and url like "'.$url.'"','frt');
		if(!empty($cek_frt)){
			$data_frt = call_user_func_array('array_merge',json_decode($cek_frt->shell,true));
			$total = count($data_frt);
			$i = $total;
			$datas = array();
			foreach ($array as $k => $v)
			{
				if(is_array($v)){
					if(in_array($v['object_id'],$data_frt)){
						$keys = array_search($v['object_id'], $data_frt);
						$datas[$keys] = $v;
					}else{
						$datas[$i] = $v;
						$i++;
					}
				}else{
					if(in_array($v->object_id,$data_frt)){
						$keys = array_search($v->object_id, $data_frt);
						$datas[$keys] = $v;
					}else{
						$datas[$i] = $v;
						$i++;
					}
				}
				
			} 
			ksort($datas, SORT_NUMERIC);
		}else{
			$datas = false;
		}
		return $datas;
	}
	
	protected function glue_homepage($array,$data_type,$url)
	{
		$cek_frt = $this->dbfun_model->get_data_bys('shell','ccs_id = '.$this->zone_id.' and type like "data" and url like "'.$url.'"','frt');
		if(!empty($cek_frt)){
			$data = json_decode($cek_frt->shell,true);
			if(isset($data[$data_type])){
				$data_frt = call_user_func_array('array_merge',$data[$data_type]);
			}else{
				$data_frt = call_user_func_array('array_merge',$data);
			}
			
			$total = count($data_frt);
			$i = $total;
			$datas = array();
			foreach ($array as $k => $v)
			{
				if(is_array($v)){
					if(in_array($v['object_id'],$data_frt)){
						$keys = array_search($v['object_id'], $data_frt);
						$datas[$keys] = $v;
					}else{
						$datas[$i] = $v;
						$i++;
					}
				}else{
					if(in_array($v->object_id,$data_frt)){
						$keys = array_search($v->object_id, $data_frt);
						$datas[$keys] = $v;
					}else{
						$datas[$i] = $v;
						$i++;
					}
				}
				
			} 
			ksort($datas, SORT_NUMERIC);
		}else{
			$datas = false;
		}
		return $datas;
	}
	
	protected function relation($ccs_key, $o_array)
	{
		
		if(is_array($o_array)){
			$datas = array();
			foreach($o_array as $o){
				$url = '#';
				$cek = $this->dbfun_model->get_data_bys('url_key,catid,oid,link','ccs_id = '.$this->zone_id.' and object_id = '.$o->object_id.' and ccs_key = '.$ccs_key.'','orl');
				if(!empty($cek)){
					$mdl = $this->dbfun_model->get_data_bys('link','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.module_id = '.$cek->url_key.'','mdl')->link;
					$cat = $this->category_id_to_category($cek->url_key,$cek->catid);
					if(!empty($cek->oid)){
						$title = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$cek->oid.' and ccs_key = '.$cek->url_key.'','odc')->title;
						$url = base_url($this->zone).'/'.strtolower(str_replace(' ','_',$mdl.'/'.$cat.'/'.$title));
					}else{
						$url = base_url($this->zone).'/'.strtolower(str_replace(' ','_',$mdl.'/'.$cat));
					}
					$datas[] = (object) array_merge((array)$o, array('url'=>$url));
				}else{
					$datas[] = (object) array_merge((array)$o, array('url'=>$url));
				}
			}
		}else{
			$url = '#';
			$cek = $this->dbfun_model->get_data_bys('url_key,catid,oid,link','ccs_id = '.$this->zone_id.' and object_id = '.$o_array->object_id.' and ccs_key = '.$ccs_key.'','orl');
			if(!empty($cek)){
				$mdl = $this->dbfun_model->get_data_bys('link','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.module_id = '.$cek->url_key.'','mdl')->link;
				$cat = $this->category_id_to_category($cek->url_key,$cek->catid);
				if(!empty($cek->oid)){
					$title = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$cek->oid.' and ccs_key = '.$cek->url_key.'','odc')->title;
					$url = base_url($this->zone).'/'.strtolower(str_replace(' ','_',$mdl.'/'.$cat.'/'.$title));
				}else{
					$url = base_url($this->zone).'/'.strtolower(str_replace(' ','_',$mdl.'/'.$cat));
				}
				$datas = (object) array_merge((array)$o_array, array('url'=>$url));
			}else{
				$datas = (object) array_merge((array)$o_array, array('url'=>$url));
			}
		}
		return $datas;
	}
	
	protected function notification($mod)
	{
		$datas = '';
		$module = strtolower($mod);
		if(in_array($module,array('transactions','transaction'))){
			$status = 0;
			//$data = $this->dbfun_model->get_all_data_bys('trx_code','ccs_id = '.$this->zone_id.' and code like "CNF" and status = '.$status.' order by inv_id DESC','inv');
			$data = $this->dbfun_model->get_all_data_bys('distinct(ccs_inv.datecreated),ccs_inv.trx_code,ccs_inv.code,ccs_inv.status,ccs_trx.user_id,ccs_trx.c_status','ccs_trx.ccs_id = '.$this->zone_id.' and ccs_trx.ccs_id = ccs_inv.ccs_id and ccs_trx.ccs_id = ccs_tri.ccs_id and ccs_tri.info_id = ccs_trx.info_id and ccs_trx.trx_code = ccs_inv.trx_code and ccs_inv.code like "CNF" and ccs_inv.status = '.$status.' order by ccs_trx.datecreated DESC','ccs_trx,ccs_inv, ccs_tri');
			/* $i = 0;
			foreach($data as $d){
				$cek = $this->dbfun_model->get_data_bys('trx_code','ccs_trx.ccs_id = ccs_odc.ccs_id and ccs_trx.ccs_id = ccs_prc.ccs_id and ccs_prc.object_id = ccs_trx.object_id and ccs_trx.object_id = ccs_odc.object_id and ccs_prc.status = 1 and ccs_odc.status = 1 and ccs_trx.ccs_id = '.$this->zone_id.' and ccs_trx.trx_code = '.$d->trx_code.' and ccs_trx.object_id != 0 and ccs_trx.status = 0','trx, odc,prc');
				if(!empty($cek)){
					$i++;
				}
			} */
			$i = count($data);
			$datas = array($mod,$i);
		}elseif(in_array($module,array('message','messages','pesan'))){
			$status = 3;
			$data = count($this->dbfun_model->get_all_data_bys('message_id','ccs_id = '.$this->zone_id.' and tr_id = 0 and receiver = '.$this->session->userdata($this->zone.'_admin_id').' and status = 1 and read_status = '.$status.'','msg'));
			$datas = array($mod,$data);
		}
		
		return $datas;
	}

	protected function update_stock($o_array)
	{
		if(!empty($o_array)){
			if(is_array($o_array)){
				foreach($o_array as $o){
					if(!empty($o->object_id)){
						$oid = $o->object_id;
						$ccs_id = $this->zone_id;
						$cat_id = $o->cat_id;
						$total = $o->total;
						if(!empty($o->varian_id)){
							$where = 'ccs_id = '.$ccs_id.' and category_id = "'.$cat_id.'" and object_id = "'.$oid.'" and varian_id = '.$o->varian_id.'';
							$table = 'ccs_prv';
						}else{
							$where = 'ccs_id = '.$ccs_id.' and category_id = "'.$cat_id.'" and object_id = "'.$oid.'" ';
							$table = 'ccs_prc';
						}
						$cek_o = $this->dbfun_model->get_data_bys('stock,sold,status',$where,$table);
						if(!empty($cek_o)){
							$status = 1;
							$stock = $cek_o->stock - $total;
							if($stock < 1){
								$status = 0;
							}
							$custom = array(
										'stock'=> $cek_o->stock - $total,
										'sold'=> $cek_o->sold + $total,
										'status'=> $status,
									);
							$this->dbfun_model->update_table($where, $custom, $table);
						}
					}
					
				}
			}else{
				if(!empty($o_array->object_id)){
					$oid = $o_array->object_id;
					$ccs_id = $this->zone_id;
					$cat_id = $o_array->cat_id;
					$total = $o_array->total;
					if(!empty($o_array->varian_id)){
						$where = 'ccs_id = '.$ccs_id.' and category_id = "'.$cat_id.'" and object_id = "'.$oid.'" and varian_id = '.$o_array->varian_id.'';
						$table = 'ccs_prv';
					}else{
						$where = 'ccs_id = '.$ccs_id.' and category_id = "'.$cat_id.'" and object_id = "'.$oid.'" ';
						$table = 'ccs_prc';
					}
					$cek_o = $this->dbfun_model->get_data_bys('stock,sold,status',$where,$table);
					if(!empty($cek_o)){
						$status = 1;
						$stock = $cek_o->stock - $total;
						if($stock < 1){
							$status = 0;
						};
						$custom = array(
									'stock'=> $cek_o->stock - $total,
									'sold'=> $cek_o->sold + $total,
									'status'=> $status,
								);
						$this->dbfun_model->update_table($where, $custom, $table);
					}
				}
			}
		}
	}
	
	protected function g_analytics()
	{
		$ga_params = array(
			'redirectUri' => $this->menu['link'].'/admin/dashboard',
			'datestart' => date('Y-m-d', strtotime('-1 month')),
			'dateend' => date('Y-m-d'),
		);
		
		if($this->zone == 'shisha'){
			$ga_params['applicationName'] = 'Shisha Cafe';
			$ga_params['clientID'] = '463386662574-e39m36gp6t6a2561k80at7udiurj7k9v.apps.googleusercontent.com';
			$ga_params['clientSecret'] = 'pGr_p3MJw7xnLkwGhgSzYHEw';
			$ga_params['developerKey'] = '3408398f45e152b4e630f3ce03d5db34035bc79d';
			$ga_params['profileID'] = '123319703';
		}elseif($this->zone == 'ilovekb'){
			$ga_params['applicationName'] = 'KB Indonesia';
			$ga_params['clientID'] = '1035867458042-585j9uhi5hsk9sp4h6nu8nero1rbdsf2.apps.googleusercontent.com';
			$ga_params['clientSecret'] = 'DxFPSSi-vkgyOfgGDtBKDswD';
			$ga_params['developerKey'] = '215a229afe14c1af87d8b1b66cec7a49c2b17337';
			$ga_params['profileID'] = '123721911';
		}elseif($this->zone == 'moonshine'){
			$ga_params['applicationName'] = 'Moonshine';
			$ga_params['clientID'] = '834781748465-vc64e6bsv4pcqgvm4trub1k46sj02r92.apps.googleusercontent.com';
			$ga_params['clientSecret'] = 'p_o6gxC2U__fXYvnnspfa8UF';
			$ga_params['developerKey'] = '33a8bba3ee6eb456124b1f4047485e2ce42acff7';
			$ga_params['profileID'] = '123693679';
		}
		
		$this->load->library('google/GoogleAnalytics', $ga_params);
		
		$data = array(
            'users' => $this->googleanalytics->get_total('users'),
            'sessions' => $this->googleanalytics->get_total('sessions'),
            'browsers' => $this->googleanalytics->get_dimensions('browser','sessions'),
            'operatingSystems' => $this->googleanalytics->get_dimensions('operatingSystem','sessions'),
            'realtime' => $this->googleanalytics->get_data_realtime(),
            'map' => $this->googleanalytics->get_total_by_city('users','ga:country'),
            'most_visited' => $this->googleanalytics->get_popular('ga:pagePath','ga:pageviews,ga:uniquePageviews,ga:timeOnPage,ga:bounces,ga:entrances,ga:exits','-ga:pageviews'),
            'device' => $this->googleanalytics->get_dimensions('deviceCategory','sessions'),
            'gender' => $this->googleanalytics->get_dimensions('userGender','sessions'),
            'time' => $this->googleanalytics->get_dimensions('day','30dayUsers'),
			'login' => $this->googleanalytics->v_login,
        );
		//'profileInfo' => $this->googleanalytics->get_profile_info()
		
		return $data;
		//$this->googleanalytics->logout();
	}
	
	protected function export_excel($type,$data)
	{
		if(in_array($type,array('extract','create'),true)){
			$excelFile = $data;
		
			if($excelFile) {
			
				$this->load->library('PHPExcel');	
					
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
				$objPHPExcel = $objReader->load($excelFile);
				 
				$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
				$highestRow = $objWorksheet->getHighestRow();
				$highestColumn = $objWorksheet->getHighestColumn();

				$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
				$headingsArray = $headingsArray[1];

				$r = -1;
				$rr = 0;
				$namedDataArray = array();
				$extractArray = array();
				
				for ($row = 2; $row <= $highestRow; ++$row) {
					$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
					if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
						++$r;
						foreach($headingsArray as $columnKey => $columnHeading) {
							if($columnHeading != $dataRow[$row][$columnKey]){
								
								if($type == 'create'){
									
									$datas = $objWorksheet->getCell($columnKey.$row)->getValue();
									if(PHPExcel_Shared_Date::isDateTime($objWorksheet->getCell($columnKey.$row))){
										$dateValue = PHPExcel_Shared_Date::ExcelToPHP($datas);
										$datas = date("Y-m-d",$dateValue);
									}
									$temp = array();
									$usr = array(
											'name' => 'name',
											'member id' => 'reg_id',
											'username' => 'username',
											'email' => 'email',
											'phone' => 'phone',
											'mobile' => 'mobile',
											'gender' => 'gender',
											'birthday' => 'birthday',
											'postal code' => 'zip',
											'date start' => 'datestart',
											'date end' => 'dateend',
											'address' => 'address',
											'description' => 'description',
											'activation' => 'status'
										);
									foreach($usr as $keys => $u){
										if(!isset($extractArray[$r][$u])){
											$extractArray[$r][$u] = 0;
										}
										if(array_key_exists(strtolower($columnHeading),$usr)){
											if(!empty($datas)){
												$extractArray[$r][$usr[strtolower($columnHeading)]] = $datas;
											}else{
												$extractArray[$r][$usr[strtolower($columnHeading)]] = 0;
											}
										}else{
											$temp[$columnHeading] = $datas;
										}
									}
									if(empty($extractArray[$r]['description'])){
										$extractArray[$r]['description'] = (array) $temp;
									}
								}else{
									$datas = $objWorksheet->getCell($columnKey.$row)->getValue();
									if(PHPExcel_Shared_Date::isDateTime($objWorksheet->getCell($columnKey.$row))){
										$dateValue = PHPExcel_Shared_Date::ExcelToPHP($datas);
										$datas = date("Y-m-d",$dateValue);
									}
									
									if(strtolower($columnHeading) == 'email'){
										$namedDataArray[$r][$columnHeading] = $datas;
										if(!empty($datas)){
											$cek_email = $this->dbfun_model->get_data_bys('email', 'LOWER(email) like "'.strtolower($datas).'" and ccs_id = '.$this->zone_id.'','usr');
											if($cek_email){
												$namedDataArray[$r]['is_inserted'] = 1;
											}else{
												$namedDataArray[$r]['is_inserted'] = 0;
											}
										}else{
											$namedDataArray[$r]['is_inserted'] = 2;
										}
										
									}else{
										$namedDataArray[$r][$columnHeading] = $datas;
									}
								}
							}
						}
						
						if($type == 'extract'){
							if(!empty($namedDataArray[$r])){
								if(!isset($namedDataArray[$r]['is_inserted'])){
									$namedDataArray[$r]['is_inserted'] = 2;
								}
							}
						}
					}
				} 
			 
			}else{
				$namedDataArray = array();
			}
			if($type == 'create'){
				return $datas = array('ready'=>$extractArray, 'not'=>$namedDataArray);
			}else{
				return $namedDataArray;
			}
			
		}
	}
	
	protected function get_hash($input,$type)
	{
		$this->load->library('encrypt');
		$key = $this->zone;
		if($type == 'en'){
			$data = $this->encrypt->encode($input, $key);
		}elseif($type == 'de'){
			$data = $this->encrypt->decode($input, $key);
		}
		return $data;
	}
	
	protected function get_additional_o($object_id) {
		$olc = $this->dbfun_model->get_data_bys('loc_id, latitude, longitude', $primary_where, 'olc');
		$prc = $this->dbfun_model->get_data_bys('price_id, basic', $primary_where, 'prc');
		$ogl = $this->dbfun_model->get_all_data_bys('gallery_id, image_square, title, description', $primary_where, 'ogl');
		$gallery = array();
		foreach ($ogl as $g) {
			$thumbnail = base_url('assets').'/'.$this->zone.'/gallery/thumbnail/'.$g->image_square;
			$main = base_url('assets').'/'.$this->zone.'/gallery/'.$g->image_square;
			$gallery[] = (object) array(
				'thumbnail'=> $thumbnail,
				'main' => $main,
				'gallery_id' => $g->gallery_id
			);
		};
		$result = array(
			'location' => $olc,
			'price' => $prc,
			'gallery' => $gallery
		);
		return $result;
	}
	
	protected function get_o($object_id)
	{
		$result = (object) array();
		$primary_where = 'ccs_id = '.$this->zone_id.' and object_id = '.$object_id;
		$o_select = 'ccs.o.object_id, ccs_o.image_square, ccs_odc.title, ccs_odc.title, ccs_odc.description, ccs_odc.metakeyword, ccs_odc.metadescription, ccs_odc.ccs_key, ccs_odc.category_id';
		$o_where = 'ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = '.$this->zone_id;
		$o = $this->dbfun_model->get_data_bys($o_select, $o_where.' and ccs_o.parent_id is null and ccs_o.object_id = '.$object_id, 'o, odc');
		$childs = array();
		if (!empty($o)) {
			$o_child = $this->dbfun_model->get_all_data_bys_order($o_select, $o_where.' and ccs_o.parent_id = '.$object_id, 'o, odc','ccs_o.object_id','ASC');
			foreach ($o_child as $oc) {
				$childs[] = (object) array_merge((array) $oc, $this->get_additional_o($oc->object_id));
			}
			$result = (object) array_merge((array) $o, $this->get_additional_o($object_id), (array) $childs);
		}
		
		return $result;
	}
}

/* End of file Admin.php */
/* Location: ./main_app/core/Admin.php */