<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//$route['default_controller'] = 'Welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['default_controller'] = "home";
	$route['404_override'] = '';
	/* Admin View Start*/
		/* Admin */
			$route['(:any)/admin'] = 'adm_login/login/index/$1';
			$route['(:any)/admin/index'] = 'adm_login/login/index/$1';
			$route['(:any)/admin/login'] = 'adm_login/login/index/$1';
			$route['(:any)/admin/checking-account'] = 'adm_login/login/login_check/$1';
			$route['(:any)/admin/logout'] = 'adm_login/login/logout/$1';
			$route['(:any)/admin/upload_summernote'] = 'adm_login/login/upload_summernote/$1';
			$route['(:any)/admin/upload_gallery/(:any)'] = 'adm_login/login/upload_gallery/$1/$2/$3/$4';
		/* Admin End */
			
			$route['(:any)/admin/dashboard'] = 'adm_dashboard/dashboard/index/$1';
			$route['admin/dashboard/view'] = 'adm_dashboard/dashboard/view';
			$route['(:any)/admin/dashboard/view_detail/(:any)'] = 'adm_dashboard/dashboard/detail_ajax/$1/$2';
			$route['(:any)/admin/dashboard/pointer'] = 'adm_dashboard/dashboard/pointer/$1';
			
			$route['(:any)/admin/frontend'] = 'adm_frontend/frontend/index/$1';
			$route['(:any)/admin/frontend/create'] = 'adm_frontend/frontend/create/$1';
			$route['(:any)/admin/frontend/view_detail/(:any)'] = 'adm_frontend/frontend/detail_ajax/$1/$2';

		/* approval */
		$route['(:any)/admin/approval/(:num)'] = 'adm_approval/approval/view/$1/$2';
		$route['(:any)/admin/approval/view_detail/(:any)'] = 'adm_approval/approval/detail_ajax/$1/$2';
		$route['(:any)/admin/approval/create'] = 'adm_approval/approval/create/$1/approval';
		$route['(:any)/admin/approval/status'] = 'adm_approval/approval/status/$1/approval'; 
		/* end approval */

		/* report */
		$route['(:any)/admin/report/(:num)'] = 'adm_report/report/view/$1/$2';
		$route['(:any)/admin/report/view_detail/(:any)'] = 'adm_report/report/detail_ajax/$1/$2';
		$route['(:any)/admin/report/create'] = 'adm_report/report/create/$1/report';
		$route['(:any)/admin/report/status'] = 'adm_report/report/status/$1/report'; 
		/* end report */

		/* calendar */
		$route['(:any)/admin/calendar/(:num)'] = 'adm_calendar/calendar/view/$1/$2';
		$route['(:any)/admin/calendar/view_detail/(:any)'] = 'adm_calendar/calendar/detail_ajax/$1/$2';
		$route['(:any)/admin/calendar/create'] = 'adm_calendar/calendar/create/$1/calendar';
		$route['(:any)/admin/calendar/status'] = 'adm_calendar/calendar/status/$1/calendar'; 
		/* end calendar */

		/* finance */
		$route['(:any)/admin/finance/(:num)'] = 'adm_finance/finance/view/$1/$2';
		$route['(:any)/admin/finance/view_detail/(:any)'] = 'adm_finance/finance/detail_ajax/$1/$2';
		$route['(:any)/admin/finance/create'] = 'adm_finance/finance/create/$1/finance';
		$route['(:any)/admin/finance/status'] = 'adm_finance/finance/status/$1/finance'; 
		/* end finance */
		
		/* settings */
		$route['(:any)/admin/settings/(:num)'] = 'adm_settings/settings/view/$1/$2';
		$route['(:any)/admin/module/view_detail/settings'] = 'adm_settings/settings/detail_ajax/$1/settings';
		$route['(:any)/admin/settings/create'] = 'adm_settings/settings/create/$1/calendar';
		$route['(:any)/admin/settings/status'] = 'adm_settings/settings/status/$1/calendar'; 
		/* end settings */		
		
		/* messages */
		$route['(:any)/admin/messages'] = 'adm_messages/messages/view/$1/admin';
		$route['(:any)/admin/messages/view_detail/(:any)'] = 'adm_messages/messages/detail_ajax/$1/messages/$2';
		$route['(:any)/admin/messages/create'] = 'adm_messages/messages/create/$1/messages';
		$route['(:any)/admin/messages/status'] = 'adm_messages/messages/status/$1/messages'; 
		/* end messages */

		/* Module */
			$route['(:any)/admin/module/view_detail/(:any)'] = 'adm_module/module/detail_ajax/$1/$2';
			$route['(:any)/admin/(:any)/(:num)'] = 'adm_module/module/view/$1/$2';
			$route['(:any)/admin/module/view_detail/(:any)'] = 'adm_module/module/detail_ajax/$1/$2';
			$route['(:any)/admin/(:any)/create_mdl'] = 'adm_module/module/create/$1/$2';
			$route['(:any)/admin/(:any)/zone_status'] = 'adm_module/module/status/$1/$2';
			$route['(:any)/admin/(:any)/zone_delete'] = 'adm_module/module/delete/$1/$2';
			//$route['(:any)/admin/role'] = 'adm_object/objects/view/$1/role';
			//$route['(:any)/admin/admin'] = 'adm_object/objects/view/$1/admin';
			//$route['(:any)/admin/page'] = 'adm_object/objects/view/$1/page';
		/* Module End */
		
		/* Zone */
		$route['(:any)/admin/zone'] = 'adm_zone/zone/index/$1';
			$route['(:any)/admin/zone/auto_create/(:any)'] = 'adm_zone/zone/auto_create/$1/$2';
			$route['(:any)/admin/zone/remove/(:any)'] = 'adm_zone/zone/remove/$1/$2';
			$route['(:any)/admin/zone/view_detail/(:any)'] = 'adm_zone/zone/detail_ajax/$1/$2';
			$route['(:any)/admin/(:any)/status'] = 'adm_zone/zone/status/$1/$2';
			$route['(:any)/admin/(:any)/create'] = 'adm_zone/zone/create/$1/$2';
		/* Zone End */
		
	/* Admin View End*/

	/* User */
		$route['(:any)/user'] = 'usr_login/login/index/$1';
		$route['(:any)/user/index'] = 'usr_login/login/index/$1';
		$route['(:any)/user/login'] = 'usr_login/login/index/$1';
		$route['(:any)/user/checking-account'] = 'usr_login/login/login_check/$1';
		$route['(:any)/user/logout'] = 'usr_login/login/logout/$1';
		$route['(:any)/user/upload_summernote'] = 'usr_login/login/upload_summernote/$1';
		$route['(:any)/user/upload_gallery/(:any)'] = 'usr_login/login/upload_gallery/$1/$2/$3/$4';
		
		$route['(:any)/user/dashboard'] = 'usr_dashboard/dashboard/index/$1';
		$route['(:any)/user/dashboard/pointer'] = 'usr_dashboard/dashboard/pointer/$1';
		$route['(:any)/user/dashboard/view_detail/(:any)'] = 'usr_dashboard/dashboard/detail_ajax/$1/$2';
		$route['(:any)/user/module/view_detail/(:any)'] = 'usr_module/module/detail_ajax/$1/$2';
		$route['(:any)/user/(:any)/(:num)'] = 'usr_module/module/view/$1/$2';
		$route['(:any)/user/(:any)/create_mdl'] = 'usr_module/module/create/$1/$2';
		$route['(:any)/user/(:any)/checkout'] = 'usr_module/module/checkout/$1/$2';
		$route['(:any)/user/(:any)/zone_status'] = 'usr_module/module/status/$1/$2';
		$route['(:any)/user/(:any)/zone_delete'] = 'usr_module/module/delete/$1/$2';
		/* messages */
		$route['(:any)/user/messages'] = 'usr_messages/messages/view/$1/user';
		$route['(:any)/user/messages/view_detail/(:any)'] = 'usr_messages/messages/detail_ajax/$1/messages/$2';
		$route['(:any)/user/messages/create'] = 'usr_messages/messages/create/$1/messages';
		$route['(:any)/user/messages/status'] = 'usr_messages/messages/status/$1/messages'; 
		/* end messages */
	/* End User */
	
	/* Fronted View Start */
		/* messages */
			$route['(:any)/dashboard/messages'] = 'usr_messages/messages/view/$1/user';
			$route['(:any)/dashboard/messages/view_detail/(:any)'] = 'usr_messages/messages/detail_ajax/$1/messages/$2';
			$route['(:any)/dashboard/messages/create'] = 'usr_messages/messages/create/$1/messages';
			$route['(:any)/dashboard/messages/status'] = 'usr_messages/messages/status/$1/messages'; 
		/* end messages */
		$route['(:any)/sign_up'] = 'frontend/frontend/sign_up/$1';
		$route['(:any)/forgot'] = 'frontend/frontend/forgot_password/$1';
		$route['(:any)/member_area'] = 'frontend/frontend/member_area/$1';
		$route['(:any)/login'] = 'frontend/frontend/login/$1';
		$route['(:any)/logout'] = 'frontend/frontend/logout/$1';
		$route['(:any)/list/view_detail/(:any)'] = 'frontend/frontend/detail_ajax/$1/list/$2';
		$route['(:any)/dashboard/view_detail/(:any)'] = 'frontend/frontend/detail_ajax/$1/$2';
		$route['(:any)/cart/view_detail/(:any)'] = 'frontend/frontend/detail_ajax/$1/$2';
		$route['(:any)/create/view_detail/(:any)'] = 'frontend/frontend/detail_ajax/$1/$2';
		$route['(:any)/cek/api/(:any)'] = 'frontend/frontend/cek_api/$1/$2';
		$route['(:any)/shop/(:any)'] = 'frontend/frontend/index/$1/$2/$3/$4/$5';
		$route['(:any)'] = 'frontend/frontend/index/$1/$2/$3/$4/$5';
	/* Fronted View End */

