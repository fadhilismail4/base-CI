<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Dbfun_model extends CI_Model { 

		function ins_to($table, $data)
		{
			$this->db->insert($table, $data);
			$this->db->close();
		}

		function ins_return_id($table, $data)
		{
			$this->db->insert($table, $data);
			return $this->db->insert_id();
			$this->db->close();
		}

		function get_all_data_bys_with_limit($select, $where, $table, $limit)
		{
			$this->db->select($select);
			$this->db->where($where);
			$this->db->limit($limit);
			$result = $this->db->get($table)->result();
			return $result;
			$this->db->close();
		}
		
		function get_max_data($select, $table)
		{
			$this->db->select($select);
			$result = $this->db->get($table)->row(1);
			return $result;
			$this->db->close();
		}
		
		function get_max_data_where($select, $where, $table)
		{
			$this->db->select($select);
			$this->db->where($where);
			$result = $this->db->get($table)->row(1);
			return $result;
			$this->db->close();
		}
		
		function get_all_data($select, $table)
		{
			$this->db->select($select);
			$result = $this->db->get($table)->result();
			return $result;
			$this->db->close();
		}
		
		function get_all_data_by($select, $where, $param_where, $table)
		{
			$this->db->select($select);
			$this->db->where($where ,$param_where);
			$result = $this->db->get($table)->result();
			return $result;
			$this->db->close();
		}
		
		function get_all_data_bys($select, $where, $table)
		{
			$this->db->select($select);
			$this->db->where($where);
			$result = $this->db->get($table)->result();
			return $result;
			$this->db->close();
		}
		
		function get_all_data_bys_order($select, $where, $table, $order, $order_type)
		{
			$this->db->select($select);
			$this->db->where($where);
			$this->db->order_by($order, $order_type);
			$result = $this->db->get($table)->result();
			return $result;
			$this->db->close();
		}

		function get_data_by($select, $where, $param_where, $table)
		{
			$this->db->select($select);
			$this->db->where($where ,$param_where);
			$this->db->limit(1);
			$result = $this->db->get($table)->row(1);
			return $result;
			$this->db->close();
		}

		function get_data_bys($select, $where, $table)
		{
			$this->db->select($select);
			$this->db->where($where);
			$this->db->limit(1);
			$result = $this->db->get($table)->row(1);
			return $result;
			$this->db->close();
		}

		function count_result_where($where, $param_where, $table)
		{
			$this->db->where($where, $param_where);
			$this->db->from($table);
			$result = $this->db->count_all_results();
			return $result;
			$this->db->close();
		}

		function count_result($where, $table)
		{
			$this->db->where($where);
			$this->db->from($table);
			$result = $this->db->count_all_results();
			return $result;
			$this->db->close();
		}

		function upd_table($where, $param_where, $data, $table)
		{
			$this->db->where($where, $param_where);
			$this->db->update($table, $data);
			$this->db->close();
		}

		function update_table($where, $data, $table)
		{
			$this->db->where($where);
			$this->db->update($table, $data);
			$this->db->close();
		}
		
		function counter_viewer($where, $table){
			$this->db->set('viewer','viewer+1', FALSE);
			$this->db->where($where);
			$this->db->update($table);
			$this->db->close();
		}
		
		function counter_love($where, $table){
			$this->db->set('love','love+1', FALSE);
			$this->db->where($where);
			$this->db->update($table);
			$this->db->close();
		}
		
		function counter_un_love($where, $table){
			$this->db->set('love','love-1', FALSE);
			$this->db->where($where);
			$this->db->update($table);
			$this->db->close();
		}

		function del_table($where, $param_where, $table)
		{
			$this->db->where($where, $param_where);
			$this->db->delete($table);
			$this->db->close();
		}
		
		function del_tables($where, $table)
		{
			$this->db->where($where);
			$this->db->delete($table);
			$this->db->close();
		}
	}