<!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
				<p>
					To recover your password please fill in your email address
				</p>
				<div class="alert alert-danger" id="fail-reset" style="display:none;"></div>
				<div class="alert alert-info" id="success-reset" style="display:none;"></div>
				<form role="form" id="forget-password" action="<?php echo $forgot_url;?>" method="post">
					<div class="form-group">
						<label for="forget-email" class="hide">Email</label>
						<input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
					</div>
					<div class="form-group">
						<button type="submit" id="fp" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
						<a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
					</div>
				</form>
			</div>
			<div class="modal-footer c-no-border">
				<span class="c-text-account">Don't Have An Account Yet ?</span>
				<a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade c-content-login-form" id="member-area-form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Member Area</h3>
				<p>
					Welcome back, friends. If you don't have username or password. Just input your member ID and your name on the member card. 
				</p>
				<div class="alert alert-danger" id="fail-member" style="display:none;"></div>
				<div class="alert alert-info" id="success-member" style="display:none;"></div>
				<form role="form" id="member-area" action="member_area" method="post">
					<div class="form-group">
						<label for="forget-email" class="hide">Member</label>
						<input type="reg_id" class="form-control input-lg c-square" id="member-email" placeholder="Member ID">
					</div>
					<div class="form-group">
						<label for="forget-email" class="hide">Name</label>
						<input type="name" class="form-control input-lg c-square" id="member-name" placeholder="Name">
					</div>
					<div class="form-group">
						<button type="submit" id="ma" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
						<a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
					</div>
				</form>
			</div>
			<div class="modal-footer c-no-border">
				<span class="c-text-account">Don't Have An Account Yet ?</span>
				<a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Create An Account</h3>
				<?php if($zone == 'contribyouth'){;?>
				<div id="" class="" style="">
					<div class="form-group">
						<button href="javascript:;" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" style="width: 100%;">For Youth</button>
					</div>	
					<div class="clearfix">
						<div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
							<span>or</span>
						</div>
					</div>
					<div class="form-group">
						<button href="javascript:;" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-green c-btn-border-2x c-btn-square" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" style="width: 100%;">For Organization</button>
					</div>
				</div>
				<div id="" class="" style="display:none;">
				<?php }else{;?>
				<div id="" class="" style="">
				<?php };?>
					<p>
						Please fill in below form to create an account with us
					</p>
					<div class="alert alert-danger" id="s-fail" style="display:none;"></div>
					<div class="alert alert-info" id="s-success" style="display:none;"></div>
					<form role="form" id="s-login" action="<?php echo $sign_up_url;?>" method="post">
						<div class="form-group">
							<label for="signup-fullname" class="hide">Fullname</label>
							<input type="text" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
						</div>
						<div class="form-group">
							<label for="signup-email" class="hide">Email</label>
							<input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
						</div>
						<div class="form-group">
							<label for="signup-username" class="hide">Username</label>
							<input type="text" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
						</div>
						<div class="form-group">
							<label for="signup-password" class="hide">Password</label>
							<input type="password" class="form-control input-lg c-square" id="signup-password" placeholder="Password">
						</div>
						<div class="form-group">
							<label for="signup-password2" class="hide">Confirmation Password</label>
							<input type="password" class="form-control input-lg c-square" id="signup-password2" placeholder="Confirmation Password">
						</div>
						<!--<div class="form-group">
							<label for="signup-country" class="hide">Country</label>
							<select class="form-control input-lg c-square" id="signup-country">
								<option value="1">Country</option>
							</select>
						</div>-->
						<div class="form-group">
							<button type="submit" id="signup-button" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
							<a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/USER/SIGNUP-FORM -->
<?php if($zone == 'ilovekb'){;?>
<div class="modal fade c-content-login-form" id="signup-form-reseller" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Create An Account</h3>
				<div id="" class="" style="">
					<p>
						Please fill in below form to create an account with us
					</p>
					<div class="alert alert-danger" id="s2-fail" style="display:none;"></div>
					<div class="alert alert-info" id="s2-success" style="display:none;"></div>
					<form role="form" id="s2-login" action="<?php echo $sign_up_url;?>" method="post">
						<div class="form-group">
							<label for="signup2-fullname" class="hide">Fullname</label>
							<input type="text" class="form-control input-lg c-square" id="signup2-fullname" placeholder="Fullname">
						</div>
						<div class="form-group">
							<label for="signup2-fullname" class="hide">Mobile Phone</label>
							<input type="number" class="form-control input-lg c-square" id="signup2-mobile" placeholder="Mobile Phone">
						</div>
						<?php foreach($user_type as $ut){;?>
							<?php if($ut->category_id == 3){;?>
							<input type="text" class="hide" id="type_id" value="<?php echo $ut->category_id;?>" style="display:none">
							<div class="form-group">
								<label for="type_reseller" class="hide">Type <?php echo $ut->title;?></label>
								<select class="form-control input-lg c-square" id="type_reseller">
								<?php foreach($ut->child as $ut_child){;?>
									<option value="<?php echo $ut_child->category_id;?>"><?php echo $ut_child->title;?></option>
								<?php };?>
								</select>
							</div>
							<?php };?>
						<?php };?>
						<div class="form-group">
							<label for="signup2-email" class="hide">Email</label>
							<input type="email" class="form-control input-lg c-square" id="signup2-email" placeholder="Email">
						</div>
						<div class="form-group">
							<label for="signup2-username" class="hide">Username</label>
							<input type="text" class="form-control input-lg c-square" id="signup2-username" placeholder="Username">
						</div>
						<!--<div class="form-group">
							<label for="signup2-password" class="hide">Password</label>
							<input type="password" class="form-control input-lg c-square" id="signup2-password" placeholder="Password">
						</div>
						<div class="form-group">
							<label for="signup2-password2" class="hide">Confirmation Password</label>
							<input type="password" class="form-control input-lg c-square" id="signup2-password2" placeholder="Confirmation Password">
						</div>-->
						<div class="form-group">
							<button type="submit" id="signup2-button" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php };?>
<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
				<p>
					Let's make today a great day!
				</p>
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<form role="form" id="f-login" action="<?php echo $login_url;?>" method="post">
					<div class="form-group">
						<label for="login-username" class="hide">Username</label>
						<input type="text" class="form-control input-lg c-square" id="login-username" placeholder="Username">
					</div>
					<div class="form-group">
						<label for="login-password" class="hide">Password</label>
						<input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
					</div>
					<div class="form-group">
						<div class="c-checkbox">
							<input type="checkbox" id="login-rememberme" class="c-check">
							<label for="login-rememberme" class="c-font-thin c-font-17">
							<span></span>
							<span class="check"></span>
							<span class="box"></span>
							Remember Me </label>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" id="login-button" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
						<a href="#" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
					</div>
					<?php if($zone == 'shisha'){ ;?>

						<button href="#" data-toggle="modal" data-target="#member-area-form" data-dismiss="modal" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Member Area</button>
					<?php } ;?>
					<!--<div class="clearfix">
						<div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
							<span style="width: 110px">or signup with</span>
						</div>
						<ul class="c-content-list-adjusted">
							<li>
								<a class="btn btn-block c-btn-square btn-social btn-twitter">
								<i class="fa fa-twitter"></i>
								Twitter </a>
							</li>
							<li>
								<a class="btn btn-block c-btn-square btn-social btn-facebook">
								<i class="fa fa-facebook"></i>
								Facebook </a>
							</li>
							<li>
								<a class="btn btn-block c-btn-square btn-social btn-google">
								<i class="fa fa-google"></i>
								Google </a>
							</li>
						</ul>
					</div>-->
				</form>
			</div>
			<div class="modal-footer c-no-border">
				<span class="c-text-account">Don't Have An Account Yet ?</span>
				<a href="#" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/USER/LOGIN-FORM -->
<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
</nav>