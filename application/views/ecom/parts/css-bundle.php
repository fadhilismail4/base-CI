<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url();?>assets/frontend/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/animate/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN: BASE PLUGINS  -->
<link href="<?php echo base_url();?>assets/frontend/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<!-- END: BASE PLUGINS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url();?>assets/frontend/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css"/>
<link href="<?php echo base_url();?>assets/frontend/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<style>
@media only screen and (max-width: 760px) {
  #mobile_none { display: none; }
}
.c-overlay-wrapper.active {
	opacity: 1;
    top: 0;
	cursor: auto;
}
</style>
<link rel="shortcut icon" href="<?php echo $menu['logo'];?>"/>
<?php if ($css){load_css($css, base_url());} ?>
<?php if (isset($css_page)){load_css($css_page, base_url());} ?>

<?php if($menu['primaryc']){;?>
<style>
/*********************
THEME COLOR - DEFAULT
*********************/
<?php if(!empty($menu['backgroundc'])){;?>
.c-layout-header.c-layout-header-6{
	background: <?php echo $menu['backgroundc'];?>
}
.c-page-on-scroll .c-layout-header.c-layout-header-6{
	background: <?php echo $menu['backgroundc'];?>
}
<?php };?>
<?php if($zone == 'moonshine'){;?>
.c-theme-link:focus, .c-theme-link:active, .c-theme-link:hover{
	color: #000 !important;
}
.c-font-white.c-theme-btn.btn{
	color: #000 !important;
}
.checkout:hover{
	color: #000 !important;
}
.checkout {
	color: #000 !important;
}
.coupon {
	color: #000 !important;
	border-color: #000 !important;
}
.coupon:hover {
	color: #000 !important;
	border-color: <?php echo $menu['primaryc'];?> !important;
}
p#total_price{
	color: #000 !important;
}
a.c-item-name:hover{
	color: #000 !important;
}
.c-cart-menu-content span.c-item-price {
	color: #000 !important;
}
.c-layout-sidebar-menu.c-theme .c-sidebar-menu li.c-active > a {
    color: #000 !important;
}
.c-layout-sidebar-menu.c-theme .c-sidebar-menu li .c-dropdown-menu > li:hover > a {
  color: #000 !important; }
.c-layout-header .c-cart-toggler-wrapper .c-cart-number{
	color: #000;
}
.c-cart-menu-footer > a.c-theme-btn.btn{
	color: #000 !important;
}
.btn-default.btn-no-focus:hover, .btn-default:hover, .btn-default.btn-no-focus:active, .btn-default:active, .btn-default.active, .open > .btn-default.dropdown-toggle{
	background:#000;
}
.c-btn-grey-3{
	background: #000 !important;
}

<?php }elseif($zone == 'shisha'){;?>
@media (min-width: 991px) and (max-width:1200px){
	.c-layout-header .c-navbar .c-mega-menu.c-fonts-uppercase > .nav.navbar-nav > li > .c-link {
		font-size: 12px;
	}
}
<?php };?>
a {
  color: #3f444a; }
  a:active,
  a:hover,
  a:focus {
    color: <?php echo $menu['primaryc'];?>; }

.c-theme-link:focus,
.c-theme-link:active,
.c-theme-link:hover {
  color: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-link:focus > i,
  .c-theme-link:active > i,
  .c-theme-link:hover > i {
    color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-link.c-active {
  color: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-link.c-active > i {
    color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-nav li:focus > a:not(.btn),
.c-theme-nav li:active > a:not(.btn),
.c-theme-nav li:hover > a:not(.btn) {
  color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-nav li:focus > .c-quick-sidebar-toggler > .c-line,
.c-theme-nav li:active > .c-quick-sidebar-toggler > .c-line,
.c-theme-nav li:hover > .c-quick-sidebar-toggler > .c-line {
  background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-nav li.c-active {
  color: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-nav li.c-active > a:not(.btn) {
    color: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-nav li.c-active > .c-quick-sidebar-toggler > .c-line {
    background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-nav > li > .dropdown-menu.c-menu-type-mega > .nav.nav-tabs > li:hover > a {
  color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-nav > li > .dropdown-menu.c-menu-type-mega > .nav.nav-tabs > li.active {
  border-bottom: 1px solid <?php echo $menu['primaryc'];?> !important; }
  .c-theme-nav > li > .dropdown-menu.c-menu-type-mega > .nav.nav-tabs > li.active > a {
    color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-nav.nav.nav-tabs > li:hover > a {
  color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-nav.nav.nav-tabs > li.active {
  border-bottom: 1px solid <?php echo $menu['primaryc'];?> !important; }
  .c-theme-nav.nav.nav-tabs > li.active > a {
    color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-ul > li:focus > a:not(.btn),
.c-theme-ul > li:active > a:not(.btn),
.c-theme-ul > li:hover > a:not(.btn) {
  color: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-ul > li:focus > a:not(.btn) > i,
  .c-theme-ul > li:active > a:not(.btn) > i,
  .c-theme-ul > li:hover > a:not(.btn) > i {
    color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-ul > li:focus > button > .c-line,
.c-theme-ul > li:active > button > .c-line,
.c-theme-ul > li:hover > button > .c-line {
  background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-ul > li.active,
.c-theme-ul > li.c-active {
  color: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-ul > li.active > a:not(.btn),
  .c-theme-ul > li.c-active > a:not(.btn) {
    color: <?php echo $menu['primaryc'];?> !important; }
    .c-theme-ul > li.active > a:not(.btn) > i,
    .c-theme-ul > li.c-active > a:not(.btn) > i {
      color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-ul-bg > li {
  color: #ffffff;
  background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-font,
.c-theme-color {
  color: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-font > i,
  .c-theme-color > i {
    color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-bg {
  background: <?php echo $menu['primaryc'];?> !important; }
  .c-theme-bg.c-theme-darken {
    background: <?php echo $menu['secondaryc'];?> !important; }

.c-theme-bg-after:after {
  background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-bg-before:before {
  background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-border-after:after {
  border-color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-border-before:before {
  border-color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-bg-on-hover:hover {
  background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-border {
  border-color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-on-hover:hover {
  color: <?php echo $menu['primaryc'];?> !important; }

.c-theme-bg-parent-hover:hover .c-theme-bg-on-parent-hover {
  background: <?php echo $menu['primaryc'];?> !important; }

.c-theme-border {
  border-color: <?php echo $menu['primaryc'];?>; }

.c-content-iconlist-1.c-theme > li:hover i {
  color: #fff;
  background: <?php echo $menu['primaryc'];?>; }

.c-content-ver-nav .c-menu.c-theme > li:before {
  color: <?php echo $menu['primaryc'];?>; }

.c-content-ver-nav .c-menu.c-theme.c-arrow-dot > li:before {
  background: <?php echo $menu['primaryc'];?>; }

.c-content-pagination.c-theme > li:hover > a {
  border-color: <?php echo $menu['primaryc'];?>;
  background: <?php echo $menu['primaryc'];?>;
  color: #fff; }
  .c-content-pagination.c-theme > li:hover > a > i {
    color: #fff; }

.c-content-pagination.c-theme > li.c-active > span,
.c-content-pagination.c-theme > li.c-active > a {
  border-color: <?php echo $menu['primaryc'];?>;
  background: <?php echo $menu['primaryc'];?>;
  color: #fff; }
  .c-content-pagination.c-theme > li.c-active > span > i,
  .c-content-pagination.c-theme > li.c-active > a > i {
    color: #fff; }

.c-theme-btn.btn {
   <?php if($zone == 'moonshine'){;?>
	color: #000;
	<?php }else{;?>  
	color: #ffffff;
	<?php };?>  
  background: <?php echo $menu['primaryc'];?>;
  border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.btn.btn-no-focus:focus,
  .c-theme-btn.btn.btn-no-focus.focus {
    color: #ffffff;
    background: <?php echo $menu['primaryc'];?>;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.btn.btn-no-focus:hover,
  .c-theme-btn.btn:hover,
  .c-theme-btn.btn.btn-no-focus:active,
  .c-theme-btn.btn:active,
  .c-theme-btn.btn.active,
  .open > .c-theme-btn.btn.dropdown-toggle {
    <?php if($zone == 'moonshine'){;?>
	color: #000;
	<?php }else{;?>  
	color: #ffffff;
	<?php };?>  
    background: <?php echo $menu['secondaryc'];?>;
    border-color: <?php echo $menu['secondaryc'];?>; }
  .c-theme-btn.btn:active,
  .c-theme-btn.btn.active,
  .open > .c-theme-btn.btn.dropdown-toggle {
    background-image: none; }
  .c-theme-btn.btn.disabled,
  .c-theme-btn.btn.disabled:hover,
  .c-theme-btn.btn.disabled:not(.btn-no-focus):focus,
  .c-theme-btn.btn.disabled:not(.btn-no-focus).focus,
  .c-theme-btn.btn.disabled:active,
  .c-theme-btn.btn.disabled.active,
  .c-theme-btn.btn[disabled],
  .c-theme-btn.btn[disabled]:hover,
  .c-theme-btn.btn[disabled]:not(.btn-no-focus):focus,
  .c-theme-btn.btn[disabled]:not(.btn-no-focus).focus,
  .c-theme-btn.btn[disabled]:active,
  .c-theme-btn.btn[disabled].active,
  fieldset[disabled] .c-theme-btn.btn,
  fieldset[disabled] .c-theme-btn.btn:hover,
  fieldset[disabled] .c-theme-btn.btn:not(.btn-no-focus):focus,
  fieldset[disabled] .c-theme-btn.btn:not(.btn-no-focus).focus,
  fieldset[disabled] .c-theme-btn.btn:active,
  fieldset[disabled] .c-theme-btn.btn.active {
    background: <?php echo $menu['primaryc'];?>;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.btn .badge {
    color: <?php echo $menu['primaryc'];?>;
    background: #ffffff; }

.c-theme-btn.c-btn-border-1x {
  border-color: <?php echo $menu['primaryc'];?>;
  border-width: 1px;
  color: <?php echo $menu['primaryc'];?>;
  background: none;
  border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-1x.btn-no-focus:focus,
  .c-theme-btn.c-btn-border-1x.btn-no-focus.focus {
    color: <?php echo $menu['primaryc'];?>;
    background: none;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-1x.btn-no-focus:hover,
  .c-theme-btn.c-btn-border-1x:hover,
  .c-theme-btn.c-btn-border-1x.btn-no-focus:active,
  .c-theme-btn.c-btn-border-1x:active,
  .c-theme-btn.c-btn-border-1x.active,
  .open > .c-theme-btn.c-btn-border-1x.dropdown-toggle {
	<?php if($zone == 'moonshine'){;?>
	color: #000;
	<?php }else{;?>  
	color: #ffffff;
	<?php };?>  
    background: <?php echo $menu['primaryc'];?>;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-1x:active,
  .c-theme-btn.c-btn-border-1x.active,
  .open > .c-theme-btn.c-btn-border-1x.dropdown-toggle {
    background-image: none; }
  .c-theme-btn.c-btn-border-1x.disabled,
  .c-theme-btn.c-btn-border-1x.disabled:hover,
  .c-theme-btn.c-btn-border-1x.disabled:not(.btn-no-focus):focus,
  .c-theme-btn.c-btn-border-1x.disabled:not(.btn-no-focus).focus,
  .c-theme-btn.c-btn-border-1x.disabled:active,
  .c-theme-btn.c-btn-border-1x.disabled.active,
  .c-theme-btn.c-btn-border-1x[disabled],
  .c-theme-btn.c-btn-border-1x[disabled]:hover,
  .c-theme-btn.c-btn-border-1x[disabled]:not(.btn-no-focus):focus,
  .c-theme-btn.c-btn-border-1x[disabled]:not(.btn-no-focus).focus,
  .c-theme-btn.c-btn-border-1x[disabled]:active,
  .c-theme-btn.c-btn-border-1x[disabled].active,
  fieldset[disabled] .c-theme-btn.c-btn-border-1x,
  fieldset[disabled] .c-theme-btn.c-btn-border-1x:hover,
  fieldset[disabled] .c-theme-btn.c-btn-border-1x:not(.btn-no-focus):focus,
  fieldset[disabled] .c-theme-btn.c-btn-border-1x:not(.btn-no-focus).focus,
  fieldset[disabled] .c-theme-btn.c-btn-border-1x:active,
  fieldset[disabled] .c-theme-btn.c-btn-border-1x.active {
    background: none;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-1x .badge {
    color: none;
    background: <?php echo $menu['primaryc'];?>; }

.c-theme-btn.c-btn-border-2x {
  border-color: <?php echo $menu['primaryc'];?>;
  border-width: 2px;
  color: <?php echo $menu['primaryc'];?>;
  background: none;
  border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-2x.btn-no-focus:focus,
  .c-theme-btn.c-btn-border-2x.btn-no-focus.focus {
    color: <?php echo $menu['primaryc'];?>;
    background: none;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-2x.btn-no-focus:hover,
  .c-theme-btn.c-btn-border-2x:hover,
  .c-theme-btn.c-btn-border-2x.btn-no-focus:active,
  .c-theme-btn.c-btn-border-2x:active,
  .c-theme-btn.c-btn-border-2x.active,
  .open > .c-theme-btn.c-btn-border-2x.dropdown-toggle {
    color: #ffffff;
    background: <?php echo $menu['primaryc'];?>;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-2x:active,
  .c-theme-btn.c-btn-border-2x.active,
  .open > .c-theme-btn.c-btn-border-2x.dropdown-toggle {
    background-image: none; }
  .c-theme-btn.c-btn-border-2x.disabled,
  .c-theme-btn.c-btn-border-2x.disabled:hover,
  .c-theme-btn.c-btn-border-2x.disabled:not(.btn-no-focus):focus,
  .c-theme-btn.c-btn-border-2x.disabled:not(.btn-no-focus).focus,
  .c-theme-btn.c-btn-border-2x.disabled:active,
  .c-theme-btn.c-btn-border-2x.disabled.active,
  .c-theme-btn.c-btn-border-2x[disabled],
  .c-theme-btn.c-btn-border-2x[disabled]:hover,
  .c-theme-btn.c-btn-border-2x[disabled]:not(.btn-no-focus):focus,
  .c-theme-btn.c-btn-border-2x[disabled]:not(.btn-no-focus).focus,
  .c-theme-btn.c-btn-border-2x[disabled]:active,
  .c-theme-btn.c-btn-border-2x[disabled].active,
  fieldset[disabled] .c-theme-btn.c-btn-border-2x,
  fieldset[disabled] .c-theme-btn.c-btn-border-2x:hover,
  fieldset[disabled] .c-theme-btn.c-btn-border-2x:not(.btn-no-focus):focus,
  fieldset[disabled] .c-theme-btn.c-btn-border-2x:not(.btn-no-focus).focus,
  fieldset[disabled] .c-theme-btn.c-btn-border-2x:active,
  fieldset[disabled] .c-theme-btn.c-btn-border-2x.active {
    background: none;
    border-color: <?php echo $menu['primaryc'];?>; }
  .c-theme-btn.c-btn-border-2x .badge {
    color: none;
    background: <?php echo $menu['primaryc'];?>; }

.c-theme.form-control:focus,
.c-theme.form-control:active,
.c-theme.form-control.active {
  border-color: <?php echo $menu['primaryc'];?> !important; }

.c-content-line-icon.c-theme {
 // background-image: url(../../img/content/line-icons/blue1.png); }

.c-content-list-1.c-theme.c-separator-dot > li:before,
.c-content-list-1.c-theme > li:before {
  background: <?php echo $menu['primaryc'];?>; }

.c-content-tab-1.c-theme .nav > li:hover > a,
.c-content-tab-1.c-theme .nav > li:focus > a,
.c-content-tab-1.c-theme .nav > li:active > a,
.c-content-tab-1.c-theme .nav > li.active > a,
.c-content-tab-1.c-theme .nav.nav-justified > li:hover > a,
.c-content-tab-1.c-theme .nav.nav-justified > li:focus > a,
.c-content-tab-1.c-theme .nav.nav-justified > li:active > a,
.c-content-tab-1.c-theme .nav.nav-justified > li.active > a {
  border-bottom-color: <?php echo $menu['primaryc'];?>; }

.c-content-tab-2.c-theme > ul > li.active > a > .c-title,
.c-content-tab-2.c-theme > ul > li:hover > a > .c-title {
  color: <?php echo $menu['primaryc'];?>; }

.c-content-tab-2.c-theme > ul > li.active > a > .c-content-line-icon,
.c-content-tab-2.c-theme > ul > li:hover > a > .c-content-line-icon {
  background-image: url(../../img/content/line-icons/blue1.png); }

.c-content-tab-5.c-theme .c-nav-tab > li.active > a,
.c-content-tab-5.c-theme .c-nav-tab > li a:hover {
  color: #fff;
  background-color: <?php echo $menu['primaryc'];?>; }

.c-content-tab-5.c-theme .c-nav-tab.c-arrow a:hover:after {
  border-left-color: <?php echo $menu['primaryc'];?>; }

.c-content-tab-5.c-theme .c-nav-tab.c-arrow .active > a:after {
  border-left-color: <?php echo $menu['primaryc'];?>; }

.c-content-accordion-1.c-theme .panel > .panel-heading > .panel-title > a {
  background-color: <?php echo $menu['primaryc'];?>; }
  .c-content-accordion-1.c-theme .panel > .panel-heading > .panel-title > a.collapsed {
    background-color: #ffffff; }
    .c-content-accordion-1.c-theme .panel > .panel-heading > .panel-title > a.collapsed:hover,
    .c-content-accordion-1.c-theme .panel > .panel-heading > .panel-title > a.collapsed:focus {
      background-color: <?php echo $menu['primaryc'];?>; }

.c-content-accordion-1.c-theme .panel > .panel-collapse > .panel-body {
  background-color: <?php echo $menu['primaryc'];?>; }

.c-mega-menu-onepage-dots.c-theme .c-onepage-dots-nav > li.c-onepage-link:hover > a,
.c-mega-menu-onepage-dots.c-theme .c-onepage-dots-nav > li.c-onepage-link.c-active > a,
.c-mega-menu-onepage-dots.c-theme .c-onepage-dots-nav > li.c-onepage-link.active > a {
  background: <?php echo $menu['primaryc'];?>; }

.c-layout-sidebar-menu.c-theme .c-sidebar-menu li.c-active > a {
  transition: all 0.2s;
  color: <?php echo $menu['primaryc'];?>; }
  .c-layout-sidebar-menu.c-theme .c-sidebar-menu li.c-active > a > .c-arrow {
    color: <?php echo $menu['primaryc'];?>; }

.c-layout-sidebar-menu.c-theme .c-sidebar-menu li .c-dropdown-menu > li:hover > a {
  color: <?php echo $menu['primaryc'];?>; }

.c-layout-sidebar-menu.c-theme .c-sidebar-menu.c-option-2 > li.c-active > a {
  transition: all 0.2s;
  color: #ffffff;
  background-color: <?php echo $menu['primaryc'];?>; }
  .c-layout-sidebar-menu.c-theme .c-sidebar-menu.c-option-2 > li.c-active > a > .c-arrow {
    color: #ffffff; }

.c-content-title-4.c-theme .c-line-strike:before {
  border-top: 1px solid <?php echo $menu['primaryc'];?>; }

.owl-carousel.c-theme .owl-pagination .owl-page span {
  background: #e1e1e1; }

.owl-carousel.c-theme .owl-pagination .owl-page.active span {
  background: <?php echo $menu['primaryc'];?>; }

.cbp-l-filters-button .cbp-filter-counter,
.cbp-l-filters-buttonCenter .cbp-filter-counter {
  background-color: <?php echo $menu['primaryc'];?>; }
  .cbp-l-filters-button .cbp-filter-counter:before,
  .cbp-l-filters-buttonCenter .cbp-filter-counter:before {
    border-top: 4px solid <?php echo $menu['primaryc'];?>; }

.cbp-l-filters-alignCenter .cbp-filter-item.cbp-filter-item-active,
.cbp-l-filters-text .cbp-filter-item.cbp-filter-item-active {
  color: <?php echo $menu['primaryc'];?>; }

.cbp-l-filters-alignCenter .cbp-filter-counter,
.cbp-l-filters-text .cbp-filter-counter {
  background: none repeat scroll 0 0 <?php echo $menu['primaryc'];?>; }
  .cbp-l-filters-alignCenter .cbp-filter-counter:before,
  .cbp-l-filters-text .cbp-filter-counter:before {
    border-top: 4px solid <?php echo $menu['primaryc'];?>; }

.cbp-l-filters-underline .cbp-filter-item.cbp-filter-item-active {
  border-bottom-color: <?php echo $menu['primaryc'];?>; }

.cbp-l-project-desc-title:before,
.cbp-l-project-details-title:before {
  background: <?php echo $menu['primaryc'];?>; }

.tp-bullets.round.c-theme .bullet {
  margin-right: 5px; }
  .tp-bullets.round.c-theme .bullet.selected {
    background: <?php echo $menu['primaryc'];?>; }

.tp-banner-container.c-theme .tparrows.circle {
  background-image: url(../../img/content/line-icons/blue1.png); }
/* FDL WAS HERE */
/* ADD NEW CUSTOM */
.c-content-title-1 > .c-line-right, .c-content-title-1 > .c-line-left{
	background-color: <?php echo $menu['primaryc'];?> !important;
}  
<?php if($zone == 'moonshine'){;?>
.c-theme-link:focus, .c-theme-link:active, .c-theme-link:hover{
	color: #000 !important;
}
<?php };?>
</style>
<?php };?>