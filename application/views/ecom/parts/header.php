<?php if(($style_header['topbar'] == 'none') && ($style_header['navbar'] == 'none')){;?>
<style>
.c-layout-header-fixed .c-layout-page{
	margin-top: 0px;
}
</style>
<?php }elseif($style_header['topbar'] == 'none'){;?>
<style>
.c-layout-header .c-brand{
	margin: 10px 0 7px;
}
.c-page-on-scroll.c-layout-header-fixed .c-layout-header .c-brand{
	margin: 10px 0 7px;
}
<?php if(isset($style_header['logo'])){;?>
.c-desktop-logo{
	height: <?php echo $style_header['logo'];?>;
}
.c-desktop-logo-inverse{
	height: <?php echo $style_header['logo'];?>;
}
.c-layout-header .c-brand{
	margin:0px;
}
.c-page-on-scroll.c-layout-header-fixed .c-layout-header .c-brand{
	margin-top: 0px;
	margin-bottom: 0px;
}
<?php }else{;?>
.c-desktop-logo{
	height: 50px;
}
.c-desktop-logo-inverse{
	height: 50px;
}
<?php };?>

.c-mobile-logo{
	height: 33px
}
.c-layout-header-fixed .c-layout-page{
	margin-top: 66px;
}
@media(max-width: 991px){
	.c-layout-header.c-layout-header-default-mobile {
		background: #ffffff;
		border-bottom: 0;
	}
	.c-layout-header .c-brand{
		margin: 15px 15px 11px 15px;
	}
	.c-page-on-scroll.c-layout-header-fixed .c-layout-header .c-brand{
		margin: 15px 15px 11px 15px;
	}
	.c-layout-header.c-layout-header-dark-mobile .c-brand > .c-hor-nav-toggler{
		background: none;
		border: 2px solid #E6E6E6;
		padding: 3px 6px;
	}
	.c-layout-header.c-layout-header-dark-mobile .c-brand > .c-hor-nav-toggler > .c-line{
		background: #E6E6E6;
	}
}
@media(max-width: 991px){
	.c-layout-header-fixed.c-layout-header-mobile-fixed .c-layout-page{
		margin-top: 57px;
	}
	.c-layout-breadcrumbs-1 .c-page-breadcrumbs{
		float:right !important;
	}
}
.c-layout-footer.c-layout-footer-3 .c-prefooter .c-container .c-socials > li > a > i{
	background: none;
    color: #fff;
}
</style>
<?php }else{;?>
<style>
@media(max-width: 991px){
	.c-layout-header .c-brand{
		margin: 20px 15px 12px 15px !important;
	}
}
.c-desktop-logo{
	height: 50px;margin: -12px 0;
}
.c-desktop-logo-inverse{
	height: 50px;margin-top: -14px;
}
.c-mobile-logo{
	height: 33px
}
</style>
<?php };?>

<?php if($zone == 'shisha'){;?>
<style>
	.c-mobile-logo {
		height: 43px;
		margin-top: -10px;
	}
</style>	
<?php };?>

<?php if($style_header['navbar'] != 'none'){;?>
<?php if($style_header['navbar'] == 'transparent'){
		$ccs_m_nav = 'c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-theme c-fonts-uppercase c-fonts-bold';
		$ccs_m_h = 'c-layout-header c-layout-header-2 c-layout-header-dark-mobile c-header-transparent-dark';
		$ccs_m_t = 'c-topbar c-topbar-dark';
		$ccs_m_h_custom = '';
	}elseif($style_header['navbar'] == 'dark'){
		$ccs_m_nav = 'c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold c-mega-menu-onepage';
		$ccs_m_h = 'c-layout-header c-layout-header-6 c-layout-header-dark-mobile ';
		$ccs_m_t = 'c-topbar c-topbar-dark';
		if($zone == 'shisha'){
			$ccs_m_h_custom = 'background-image:url('.base_url('assets/'.$zone).'/header.png)';
		}else{
			$ccs_m_h_custom = '';
		}
	}elseif($style_header['navbar'] == 'light'){
		$ccs_m_nav = 'c-mega-menu c-pull-right c-mega-menu-light c-mega-menu-light-mobile c-fonts-uppercase c-fonts-bold';
		$ccs_m_h = 'c-layout-header c-layout-header-4 c-layout-header-default-mobile';
		$ccs_m_t = 'c-topbar c-topbar-light';
		$ccs_m_h_custom = '';
	}elseif($style_header['navbar'] == 'light-transparent'){
		echo '<style>.c-layout-header.c-layout-header-4 .c-navbar,.c-layout-header .c-topbar-light{background-color: rgba(255,255,255,0.59);}.c-page-on-scroll.c-layout-header-fixed .c-layout-header .c-brand{margin: 22px 0 0px 0;}.c-layout-header .c-brand{margin: 37px 0 37px 0;}</style>';
		if(isset($bc['module'])){
			if($bc['module'] == 'homepage'){
				echo '<style>.c-layout-header-fixed.c-layout-header-topbar .c-layout-page{margin-top: 0px;}</style>';
			}else{
				echo '<style>.c-layout-header-fixed.c-layout-header-topbar .c-layout-page{margin-top: 100px;}</style>';
			}
		}else{
			echo '<style>.c-layout-header-fixed.c-layout-header-topbar .c-layout-page{margin-top: 100px;}</style>';
		}
		$ccs_m_nav = 'c-mega-menu c-pull-right c-mega-menu-light c-mega-menu-light-mobile c-fonts-uppercase c-fonts-bold';
		$ccs_m_h = 'c-layout-header c-layout-header-4 c-layout-header-default-mobile';
		$ccs_m_t = 'c-topbar c-topbar-light';
		$ccs_m_h_custom = '';
	}else{
		$ccs_m_nav = '';
		$ccs_m_h = 'c-layout-header-6';
		$ccs_m_t = 'c-topbar';
		$ccs_m_h_custom = '';
	}
	;?>
<header class="<?php echo $ccs_m_h;?>" style="<?php echo $ccs_m_h_custom;?>" data-minimize-offset="80">
	<?php if(isset($style_header['topbar'])){;?>
	<?php if($style_header['topbar'] != 'none'){;?>
	<?php if(!empty($menu['header'])){;?>
	<div class="<?php echo $ccs_m_t;?>">
		<div class="container">
			<!-- BEGIN: INLINE NAV -->
			<nav class="c-top-menu c-pull-left">
				<?php if(!empty($menu['header'])){;?>
				<ul class="c-icons c-theme-ul">
					<?php $count_m_socmed = 0;foreach($menu['header'] as $h){;?>
						<?php if($h->metadescription == 'socmed'){;?>
						<li>
							<a href="<?php echo $h->tagline;?>">
								<i class="socicon socicon-<?php echo $h->title;?> tooltips"></i>
							</a>
						</li>
						<?php $count_m_socmed++;};?>
					<?php };?>
				</ul>
				<?php };?>
				
			</nav>
			<!-- END: INLINE NAV -->
			<!-- BEGIN: INLINE NAV -->
			<nav class="c-top-menu c-pull-right">
				<?php if(!empty($menu['header'])){;?>
				<ul class="c-links c-theme-ul" style="margin-top: 8px;">
					<?php $count_m_page = 0;foreach($menu['header'] as $hh){;?>
						<?php if($hh->metadescription == 'page'){
							$count_m_page++;?>
						<li>
							<a href="<?php echo $menu['link'];?>/<?php echo $hh->tagline;?>"><?php echo $hh->title;?></a>
						</li>
						<?php if(count($menu['header']) > ($count_m_socmed+$count_m_page)){;?>
						<li class="c-divider">|</li>
						<?php };?>
						<?php };?>
					<?php };?>	
				</ul>
				<?php };?>
				<ul class="c-ext c-theme-ul">
					<?php if(isset($menu['language'])){;?>
					<li class="c-lang dropdown c-last">
						<a href="#">EN</a>
						<ul class="dropdown-menu pull-right" role="menu">
						<?php foreach($menu['language'] as $l){;?>
							<li class="">
								<a href="#"><?php echo $l->name;?></a>
							</li>
						<?php };?>
						</ul>
					</li>	
					<?php };?>
					<li class="c-search hide">
						<!-- BEGIN: QUICK SEARCH -->
						<form action="#">
							<input type="text" name="query" placeholder="search..." value="" class="form-control" autocomplete="off">
							<i class="fa fa-search"></i>
						</form>
						<!-- END: QUICK SEARCH -->
					</li>
				</ul>
			</nav>
			<!-- END: INLINE NAV -->
		</div>
	</div>
	<?php };?>
	<?php };?>
	
	<?php };?>
	<?php if(isset($style_header['navbar'])){;?>
	<?php if($style_header['navbar'] != 'none'){;?>
	<div class="c-navbar">
		<div class="container">
			<!-- BEGIN: BRAND -->
			<div class="c-navbar-wrapper clearfix">
				<div class="c-brand c-pull-left">
					<a href="<?php echo $menu['link'];?>" class="c-logo">
						<img src="<?php echo $menu['logo'];?>" style="" alt="<?php echo $zone;?>" class="c-desktop-logo">
						<img src="<?php echo $menu['logo'];?>" style="" alt="<?php echo $zone;?>" class="c-desktop-logo-inverse">
						<img src="<?php echo $menu['logo'];?>" style="" alt="<?php echo $zone;?>" class="c-mobile-logo"> </a>
					<button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
						<span class="c-line"></span>
						<span class="c-line"></span>
						<span class="c-line"></span>
					</button>
					<?php if(!empty($menu['header'])){;?>
					<button class="c-topbar-toggler" type="button">
						<i class="fa fa-ellipsis-v"></i>
					</button>
					<?php };?>
					<button class="c-search-toggler" type="button">
						<i class="fa fa-search"></i>
					</button>
					<?php if($zone_type == 3) : ?>
					<button class="c-cart-toggler" type="button">
						<i class="icon-handbag"></i>
						<span class="c-cart-number c-theme-bg" id="cart_number_mobile" <?php if ($this->cart->total_items() == '0') : echo 'style="display : none"'; endif;?>><?php if ($this->cart->total_items() != '0') : echo $this->cart->total_items(); endif;?></span>
					</button>
					<?php endif;?>
				</div>
				<!-- END: BRAND -->
				<!-- BEGIN: QUICK SEARCH -->
				<?php if($zone == 'moonshine'){;?>
				<form class="c-quick-search" action="<?php echo $menu['link'];?>/search" method="POST" style="background-color: rgba(255, 255, 255, 1);padding: 0 5%;">
				<?php }else{;?>
				<form class="c-quick-search" action="<?php echo $menu['link'];?>/search" method="POST" style="background-color: rgba(255, 255, 255, 0.72);padding: 0 5%;">
				<?php };?>
					<input type="text" name="query" placeholder="Search..." value="" class="form-control" autocomplete="off">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" placeholder="" value="<?php echo trim($this->security->get_csrf_hash()); ?>" class="form-control" autocomplete="off">
					<span class="c-theme-link" style="padding: 0 5%">&times;</span>
				</form>
				<!-- END: QUICK SEARCH -->
				<!-- BEGIN: HOR NAV -->
				<!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
				<!-- BEGIN: MEGA MENU -->
				<!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
				<nav class="<?php echo $ccs_m_nav;?>" data-onepage-animation-speed="700">
					<?php 
						if( base_url() != "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"){
							$url = base_url();
						}else{
							$url = '';
						}
					?>
					<?php if(isset($menu)){;?>
					<ul class="nav navbar-nav c-theme-nav">
						<li class="<?php if(isset($bc['module'])){ 
									if(strtolower($bc['module']) == 'homepage'){
										echo 'c-active';
						};};?>">
							<a href="<?php echo $menu['link'];?>" class="c-link dropdown-toggle">Home
							</a>
						</li>
						<?php foreach($menu['menu'] as $m){
							if(!empty($m)){
								if(in_array(strtolower($m['title']),$this->uri->segment_array())){
									$p_active = 'c-active';
								}else{
									$p_active = '';
								}
								if(!empty($m['type'])){
									if( $m['type'] == 'paralax'){
										$type = 'paralax';
									}else{
										$type = 'multisite';
									}
								}else{
									$type = '';
								};?>
								<?php if(!isset($m['children'])){;?>
									<?php if( $type == 'paralax'){;?>
										<li class="c-onepage-link">
											<a href="<?php echo str_replace(' ','_',$m['title']);?>" class="c-link"><?php echo $m['title'];?>
												
											</a>
										</li>
									<?php }elseif($type == 'multisite'){;?>
										<li class="<?php echo $p_active;?>">
											<a href="<?php echo $menu['link'];?>/<?php echo str_replace(' ','_',strtolower($m['title']));?>" class="c-link dropdown-toggle"><?php echo $m['title'];?>
												
											</a>
										</li>
									<?php }else{;?>
										<li class="<?php echo $p_active;?>">
											<a href="<?php echo $menu['link'];?>/<?php echo str_replace(' ','_',strtolower($m['title']));?>" class="c-link dropdown-toggle"><?php echo $m['title'];?>
												
											</a>
										</li>
									<?php };?>
								<?php }else{;?>
									<?php if(isset($m['children'])){;?>
										<?php if($style_header['menu'] == 'mega'){;?>
										<li class="<?php echo $p_active;?>">
											<a href="#" class="c-link dropdown-toggle"><?php echo $m['title'];?>
											<span class="c-arrow c-toggler"></span>
											</a>
											<ul class="dropdown-menu c-menu-type-mega c-menu-type-fullwidth" style="min-width: auto">
											<?php foreach($m['children'] as $c){
												if(isset($c['type'])){
													$c_type = $c['type'];
												}else{
													$c_type = $c['type_id'];
												}
												if(isset($c['module'])){
													if($c['module'] != 'media'){
														if(($c['module'] == 'shop') && ($c_type == 'category')){
															$link = $menu['link'].'/'.$c['module'].'/product/'.$c['link'];
														}else{
															$link = $menu['link'].'/'.$c['module'].'/'.$c['link'];
														}
													}else{
														$link = $menu['link'].'/'.$c['link'];
													}
												}else{
													$link = $menu['link'].'/'.$c['link'];
												}
												;?>
												<li>
													<ul class="dropdown-menu c-menu-type-inline">
														<li>
															<a href="<?php echo $link;?>"><?php echo $c['title'];?></a>
														</li>
													</ul>
												</li>
											<?php };?>
											</ul>
										</li>
										<?php }else{;?>
										<li class="<?php echo $p_active;?>">
											<a href="#" class="c-link dropdown-toggle"><?php echo $m['title'];?>
											<span class="c-arrow c-toggler"></span>
											</a>
											<ul class="dropdown-menu c-menu-type-classic c-pull-left" style="min-width: auto">
											<?php foreach($m['children'] as $c){
												if(isset($c['type'])){
													$c_type = $c['type'];
												}else{
													$c_type = $c['type_id'];
												}
												if(isset($c['module'])){
													if($c['module'] != 'media'){
														if(($c['module'] == 'shop') && ($c_type == 'category')){
															$link = $menu['link'].'/'.$c['module'].'/product/'.$c['link'];
														}else{
															$link = $menu['link'].'/'.$c['module'].'/'.$c['link'];
														}
														if(in_array($c['link'],$this->uri->segment_array())){
															$active = 'c-active';
														}else{
															$active = '';
														}
													}else{
														$link = $menu['link'].'/'.$c['link'];
														if(in_array($c['link'],$this->uri->segment_array())){
															$active = 'c-active';
														}else{
															$active = '';
														}
													}
												}else{
													$link = $menu['link'].'/'.$c['link'];
													if(in_array($c['link'],$this->uri->segment_array())){
														$active = 'c-active';
													}else{
														$active = '';
													}
												}
												;?>
												<?php if(isset($c['children'])){;?>
												
												<li class="dropdown-submenu <?php echo $active;?>">
                                                    <?php if(!empty($c['children'])){;?>
													<a href="#" class="c-link dropdown-toggle"><?php echo $c['title'];?>
                                                        <span class="c-arrow c-toggler"></span>
                                                    </a>
                                                    <ul class="dropdown-menu c-pull-left" style="min-width: auto">
													<?php foreach($c['children'] as $gc){
														$gc_link = $link.'/'.$gc['link'];
														if(in_array($gc['link'],$this->uri->segment_array())){
															$g_active = 'c-active';
														}else{
															$g_active = '';
														}
														;?>
                                                        <li class="<?php echo $g_active;?>">
                                                            <a href="<?php echo $gc_link;?>"><?php echo $gc['title'];?></a>
                                                        </li>
													<?php };?>
													</ul>
													<?php }else{;?>
													<a href="<?php echo $link = $menu['link'].'/'.$c['module'].'/'.$c['link'];?>" class="c-link dropdown-toggle"><?php echo $c['title'];?>
                                                        <span class="c-arrow c-toggler"></span>
                                                    </a>
													<?php };?>
												</li>	
												<?php }else{;?>
												<li class="<?php echo $active;?>">
													<a href="<?php echo $link;?>"><?php echo $c['title'];?></a>
												</li>
												<?php };?>
												
											<?php };?>
											</ul>
										</li>
										<?php };?>
										
									<?php };?>
								<?php };?>
						<?php };};?>
						<li class="c-search-toggler-wrapper">
							<a href="#" class="c-btn-icon c-search-toggler">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<?php if($zone_type == 3){;?>
						<li class="c-cart-toggler-wrapper">
							<a href="#" class="c-btn-icon c-cart-toggler">
								<i class="icon-handbag c-cart-icon"></i>
								<span class="c-cart-number c-theme-bg" id="cart_number" <?php if ($this->cart->total_items() == '0') : echo 'style="display : none"'; endif;?>><?php if ($this->cart->total_items() != '0') : echo $this->cart->total_items(); endif;?></span>
							</a>
						</li>
						<?php };?>
						<?php //if($menu['login'] === true){;?>
						<?php if($is_login !== true){;?>
						<li>
							<a href="#" data-toggle="modal" data-target="#login-form" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-theme-btn c-btn-circle c-btn-uppercase c-btn-sbold">
								<i class="icon-user"></i> Sign In</a>
						</li>
						<?php }else{;?>
						<li>
							<a href="<?php echo base_url($dashboard_url);?>" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-theme-btn c-btn-circle c-btn-uppercase c-btn-sbold" style="margin-right: 0;margin-left: 0;">
							<?php if($zone == 'shisha'){;?>
								<i class="fa fa-th-large"></i> My Account</a>
							<?php }else{;?>
								<i class="fa fa-th-large"></i> Dashboard</a>
							<?php };?>
						</li>
						<li>
							<?php if($is_mobile){;?>
							<a href="<?php echo base_url($logout_url);?>" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-theme-btn c-btn-circle c-btn-uppercase c-btn-sbold" style="margin-right: 0;margin-left: 0;">
								<i class="icon-logout"></i> Log Out
							</a>							
							<?php }else{;?>
							<a href="<?php echo base_url($logout_url);?>" class="c-btn-icon" title="Log Out">	
								<i class="icon-logout"></i>
							</a>	
							<?php };?>
						</li>
						<?php };?>
						<?php //};?>
					</ul>					
					<?php };?>
				</nav>
				<!-- END: MEGA MENU -->
				<!-- END: LAYOUT/HEADERS/MEGA-MENU -->
				<!-- END: HOR NAV -->
			</div>
			<!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->
			<!-- BEGIN: CART MENU -->
			<?php if($zone_type == 3) : ?>
			<a data-url="cart" data-url2="cart" data-param="add" data-id="view" class="add_to_cart" style="display:none"></a>
			<div class="c-cart-menu">
					<ul id ="none-item" class="c-cart-menu-items" <?php if($this->cart->contents()) : echo 'style = "display : none;"'; endif;?>>
						<li>
							<div style="text-align:center">
								You don't have any items yet.
							</div>
						</li>
					</ul>
					<div id="cartbox" <?php if(!$this->cart->contents()) : echo 'style = "display : none;"'; endif;?>>	
						<div class="c-cart-menu-title">
							<?php if($this->cart->total_items() == 1){;?>
							<p class="c-cart-menu-float-l c-font-sbold" id="total_item"><?php echo $this->cart->total_items();?> item</p>
							<?php }else{;?>
							<p class="c-cart-menu-float-l c-font-sbold" id="total_item"><?php echo $this->cart->total_items();?> item(s)</p>
							<?php };?>
							<p class="c-cart-menu-float-r c-theme-font c-font-sbold" id="total_price"><?php echo 'IDR '.number_format($this->cart->total(),2,',','.'); ?></p>
						</div>
						<ul class="c-cart-menu-items" id="list_item">
							<?php foreach($this->cart->contents() as $items): ?>
							<li>
								<div class="c-cart-menu-close">
									<a href="#" data-url="cart" data-url2="cart" data-param="remove" data-id="<?php echo $items['rowid'];?>" class="add_to_cart c-theme-link">×</a>
								</div>
								<img src="<?php echo $items['options']['img'];?>" />
								<div class="c-cart-menu-content">
									<p><?php echo $items['qty']; ?> x
										<span class="c-item-price c-theme-font"><?php echo 'IDR '.number_format($items['price'],2,',','.'); ?></span>
									</p>
									<a href="<?php echo $items['options']['url'];?>" class="c-item-name c-font-sbold"><?php echo $items['name']; ?></a>
									<?php if(!empty($items['options']['variant'])){;?>
										<p style="margin-top: 10px;"><?php echo $items['options']['variant'];?> </p>
									<?php };?>
								</div>
							</li>
							<?php endforeach; ?>
						</ul>
						<div class="c-cart-menu-footer">
							<a href="<?php echo $menu['link'].'/cart';?>" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase"><?php echo $text['view_cart'];?></a>
							<a href="<?php echo $menu['link'].'/checkout';?>" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
						</div>
					</div>
			</div>
			<?php endif;?>
			<!-- END: CART MENU -->
			<!-- END: LAYOUT/HEADERS/QUICK-CART -->
		</div>
	</div>
	<?php };?>
	<?php };?>
</header>
<?php };?>