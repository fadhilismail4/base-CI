<meta charset="utf-8" />
<title><?php echo $title ?></title>
<meta name="Description" content="<?php if(isset($metadescription)){ echo $metadescription;};?>" />
<meta name="Keywords" content="<?php if(isset($metakeyword)){ echo $metakeyword;};?>" />
<meta name="author" content="Fadhil I, Hasbi A, Budhi S"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">