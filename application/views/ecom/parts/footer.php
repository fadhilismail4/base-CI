<?php if(isset($style_footer['footer'])){
$bg_image = base_url('assets/img/content/backgrounds/bg-38.jpg');
$bg = $style_footer['footer'];//trigger dark or white
$c_bg = 'c-bg-'.$bg;
$c_border = 'c-border-'.$style_footer['footer'].'-'.$bg;
$c_content = 'c-content-'.$style_footer['footer'];
if($bg == 'dark' || $bg == 'slim'){
	$c_font = 'c-font-white';
}else{
	$c_font = ' ';
};
$c = 'c-'.$style_footer['footer'];
$c_line = 'c-line-'.$style_footer['footer'];
$custom_footer = 'footer.png';
$url_footer = './assets/'.$zone.'/'.trim($custom_footer);
if(file_exists($url_footer)){
	$ccs_m_f_custom = 'background-image:url('.base_url($url_footer).')';
}else{
	$ccs_m_f_custom = '';
}
;?>
<?php if(($style_footer['footer'] != 'none') && ($style_footer['footer'] != 'slim')){;?>
<?php if($bg == 'dark'){;?>
<style>
.c-layout-footer.c-layout-footer-3 .c-prefooter .c-container .c-blog > .c-post{
	border-bottom: 1px solid #fff;
}
</style>
<?php };?>
<?php if(in_array($zone,array('shisha','ilovekb'),true)){;?>
<footer class="c-layout-footer c-layout-footer-3 <?php echo $c_bg;?>" style="<?php echo $ccs_m_f_custom;?>">
	<div class="c-prefooter" style="padding-bottom:0">
		<div class="container">
			<div class="row">
<?php $sk=1;foreach($skeleton_footer as $key => $sk_footer){
	$sk_first = '';
	if($sk == 1){
		$sk_first = 'c-first';
	}
	;?>
	<div id="<?php echo 'footer_column_'.$key;?>" class="<?php echo $sk_footer->column;?>">
		<div class="c-container <?php echo $sk_first;?>">
			<div class="c-content-title-1">
				<h3 id="<?php echo 'footer_title_'.$key;?>" class="c-font-uppercase c-font-bold <?php echo $c_font;?>">
					<?php echo $sk_footer->title;?>
					<span class="c-theme-font"></span>
				</h3>
				<div class="c-line-left hide">
				</div>
				<p id="<?php echo 'footer_text_'.$key;?>" class="c-text <?php echo $c_font;?>">
					<?php echo $sk_footer->text;?>
				</p>
				<?php if($sk_footer->data_type == 'link'){;?>
					<?php if(!empty($menu['footer'])){;?>
					<ul id="<?php echo 'footer_datatype_'.$key;?>" data-type="<?php echo $sk_footer->data_type;?>" class="c-links">
						<?php foreach($menu['footer'] as $f){;?>
							<?php if($f->metadescription == 'page' || $f->metadescription == 'module'){;?>
							<li>
								<a class="c-theme-on-hover <?php echo $c_font;?>" href="<?php echo $menu['link'].'/'.$f->tagline;?>"><?php echo $f->title;?></a>
							</li>
							<?php };?>
						<?php };?>
					</ul>
					<?php };?>
				<?php }elseif($sk_footer->data_type == 'blog'){;?>
				<div id="<?php echo 'footer_datatype_'.$key;?>" data-type="<?php echo $sk_footer->data_type;?>" class="c-blog">
					<?php if(is_array($sk_footer->data)){;?>
						<?php $sk_data_last = count($sk_footer->data);$sk_dt = 1;
						foreach($sk_footer->data as $sk_data){
							$m_footer = $sk_footer->data_module;
							if(isset($sk_data->img)){
								$sk_data_img = $sk_data->img;
							}else{
								$sk_data_img = base_url('assets').'/'.$zone.'/'.$m_footer.'/'.$sk_data->image_square;
							}
							if(isset($sk_data->url)){
								$sk_data_url = $sk_data->url;
							}else{
								$sk_data_url = $menu['link'].'/'.$m_footer.'/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
							}
							$sk_last = '';
							if($sk_last == $sk_data_last){
								$sk_last = 'c-last';
							}
							
							;?>
							<div class="c-post <?php echo $sk_last;?>">
								<div class="c-post-img">
									<img src="<?php echo $sk_data_img;?>" alt="" class="img-responsive"/>
								</div>
								<div class="c-post-content">
									<h4 class="c-post-title">
										<a class="c-theme-on-hover <?php echo $c_font;?>" href="<?php echo $sk_data_url;?>" target="_blank">
											<?php echo $sk_data->title;?>
										</a>
									</h4>
									<p class="c-text <?php echo $c_font;?>">
										<?php echo $sk_data->category;?>
									</p>
								</div>
							</div>
						<?php $sk_dt++;};?>
					<?php };?>
				</div>
				<?php }elseif($sk_footer->data_type == 'socmed'){;?>
					<?php if(!empty($menu['footer'])){;?>
					<ul id="<?php echo 'footer_datatype_'.$key;?>" data-type="<?php echo $sk_footer->data_type;?>" class="c-socials">
						<?php foreach($menu['footer'] as $f){;?>
							<?php if($f->metadescription == 'socmed'){;?>
							<li>
								<a href="<?php echo $f->tagline;?>" target="_blank"><i class="socicon-btn socicon-<?php echo $f->title;?> tooltips c-theme-on-hover"></i></a>
							</li>
							<?php };?>
						<?php };?>
					</ul>
					<?php };?>
				<?php }elseif($sk_footer->data_type == 'image'){;?>
					<?php if(is_array($sk_footer->data)){;?>
						<?php $di = 0;foreach($sk_footer->data as $sk_data){
							if($sk_footer->data_module == 'event'){
								$sk_data_img = base_url('assets').'/'.$zone.'/event/'.$sk_data->image_square;
								$sk_data_url = $menu['link'].'/event#cbp='.$menu['link'].'/event/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
								if($zone == 'shisha'){
									$sk_data_url = $menu['link'].'/event/'.strtolower(str_replace(' ','_',$sk_data->category)).'#cbp='.$menu['link'].'/event/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
								}
							}else{
								$m_footer = $sk_footer->data_module;
								if(isset($sk_data->img)){
									$sk_data_img = $sk_data->img;
								}else{
									$sk_data_img = base_url('assets').'/'.$zone.'/'.$m_footer.'/'.$sk_data->image_square;
								}
								if(isset($sk_data->url)){
									$sk_data_url = $sk_data->url;
								}else{
									$sk_data_url = $menu['link'].'/'.$m_footer.'/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
								}
							}
							;?>
							<?php if(($di%3) == 0){;?>
							<ul id="<?php echo 'footer_datatype_'.$key;?>" data-type="<?php echo $sk_footer->data_type;?>" class="c-works">
							<?php };?>
								<li class="">
									<a href="<?php echo $sk_data_url;?>">
										<img src="<?php echo $sk_data_img;?>" alt="" class="img-responsive"/>
									</a>
								</li>
							<?php if((($di%3) == 2) && ($di > 0)){;?>
							</ul>
							<?php };?>
						<?php $di++;};?>
					<?php };?>
				<?php };?>
			</div>	
		</div>	
	</div>
<?php $sk++;};?>
		</div>
	</div>
	<div class="c-postfooter c-bg-dark-2">
		<div class="container">
			<p class="c-font-oswald c-font-14 c-font-grey">
				Copyright &copy; <?php echo $footer['copyright'];?>
			</p>
		</div>
	</div>
</footer>
<?php }else{;?>

<footer class="c-layout-footer c-layout-footer-3 <?php echo $c_bg;?>" style="<?php echo $ccs_m_f_custom;?>">
	<div class="c-prefooter">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="c-container c-first">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>"><?php echo $footer['copyright'];?><span class="c-theme-font"></span></h3>
							<div class="c-line-left hide">
							</div>
							<p class="c-text <?php echo $c_font;?>">
								<?php if($zone == 'shisha'){;?>
								Leave your worries and weariness outside, and step into the exotic land of 1001 nights, right here at Shisha Café.
								<?php }else{;?>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed elit diam nonummy ad minim.
								<?php };?>								
							</p>
						</div>
						<?php if(!empty($menu['footer'])){;?>
						<ul class="c-links">
							<?php foreach($menu['footer'] as $f){;?>
								<?php if($f->metadescription == 'page' || $f->metadescription == 'module'){;?>
								<li>
									<a class="c-theme-on-hover <?php echo $c_font;?>" href="<?php echo $menu['link'].'/'.$f->tagline;?>"><?php echo $f->title;?></a>
								</li>
								<?php };?>
							<?php };?>
						</ul>
						<?php };?>
					</div>
				</div>
				<div class="col-md-3">
					<?php if($zone == 'shisha'){;?>
					<div class="c-container">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>">Testimonial</h3>
							<div class="c-line-left hide">
							</div>
						</div>
						<div class="c-blog">
							<div class="c-post">
								<div class="c-post-img">
									<img src="http://media-cdn.tripadvisor.com/media/photo-l/09/d6/13/85/triana-s.jpg" alt="" class="img-responsive"/>
								</div>
								<div class="c-post-content">
									<h4 class="c-post-title"><a class="c-theme-on-hover <?php echo $c_font;?>" href="http://www.tripadvisor.com/Restaurant_Review-g294229-d2068862-Reviews-Shisha_Cafe-Jakarta_Java.html">“Best middle eastern restaurant”</a></h4>
									<p class="c-text <?php echo $c_font;?>">
										They have big menu for middle eastern foods and so many choices. Taste beautiful, original taste, they have lovely mix juices, sisha, cozy place and unique interior...
									</p>
								</div>
							</div>
							<div class="c-post c-last">
								<div class="c-post-img">
									<img src="http://media-cdn.tripadvisor.com/media/photo-l/03/79/be/07/facebook-avatar.jpg" alt="" class="img-responsive"/>
								</div>
								<div class="c-post-content">
									<h4 class="c-post-title"><a class="c-theme-on-hover <?php echo $c_font;?>" href="http://www.tripadvisor.com/Restaurant_Review-g294229-d2068862-Reviews-Shisha_Cafe-Jakarta_Java.html">“Good food and good shisha”</a></h4>
									<p class="c-text <?php echo $c_font;?>">
										Good food and good shisha. Thats all you need to know guys. Staff is helpful and better than average
									</p>
								</div>
							</div>
							<a href="#" class="btn btn-md c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-square c-btn-bold c-read-more hide">Read More</a>
						</div>
					</div>
					<?php }else{;?>
					<div class="c-container">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>">Latest Posts</h3>
							<div class="c-line-left hide">
							</div>
						</div>
						<div class="c-blog">
							<div class="c-post">
								<div class="c-post-img">
									<img src="<?php echo site_url();?>assets/img/content/stock/9.jpg" alt="" class="img-responsive"/>
								</div>
								<div class="c-post-content">
									<h4 class="c-post-title"><a class="c-theme-on-hover <?php echo $c_font;?>" href="#">Ready to Launch</a></h4>
									<p class="c-text <?php echo $c_font;?>">
										Lorem ipsum dolor sit amet ipsum sit, consectetuer adipiscing elit sit amet
									</p>
								</div>
							</div>
							<div class="c-post c-last">
								<div class="c-post-img">
									<img src="<?php echo site_url();?>assets/img/content/stock/14.jpg" alt="" class="img-responsive"/>
								</div>
								<div class="c-post-content">
									<h4 class="c-post-title"><a class="c-theme-on-hover <?php echo $c_font;?>" href="#">Dedicated Support</a></h4>
									<p class="c-text <?php echo $c_font;?>">
										Lorem ipsum dolor ipsum sit ipsum amet, consectetuer sit adipiscing elit ipsum elit elit ipsum elit
									</p>
								</div>
							</div>
							<a href="#" class="btn btn-md c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-square c-btn-bold c-read-more hide">Read More</a>
						</div>
					</div>
					<?php };?>
				</div>
				<div class="col-md-3">
					<?php if($zone == 'shisha'){;?>
					<div class="c-container">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>">Nearest Event</h3>
							<div class="c-line-left hide">
							</div>
						</div>
						<?php if(isset($data_custom_footer)){;?>
							<?php $di = 0;foreach($data_custom_footer as $d_c_f){;?>
								<?php if(($di%3) == 0){;?>
								<ul class="c-works">
								<?php };?>
									<li class="c-first">
										<a href="<?php echo base_url($zone);?>/event#cbp=<?php echo base_url($zone);?>/event/<?php echo str_replace(' ','_',strtolower($d_c_f->category));?>/<?php echo str_replace(' ','_',strtolower($d_c_f->title));?>"><img src="<?php echo base_url();?>assets/<?php echo $zone;?>/event/<?php echo $d_c_f->image_square;?>" alt="" class="img-responsive"/></a>
									</li>
								<?php if((($di%3) == 0) && ($di > 0)){;?>
								</ul>
								<?php };?>
							<?php $di++;};?>
						<?php };?>
						<a href="#" class="btn btn-md c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-square c-btn-bold c-read-more hide">View More</a>
					</div>
					<?php }else{;?>
					<div class="c-container">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>">Latest Works</h3>
							<div class="c-line-left hide">
							</div>
						</div>
						<ul class="c-works">
							<li class="c-first">
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/015.jpg" alt="" class="img-responsive"/></a>
							</li>
							<li>
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/012.jpg" class="img-responsive" alt=""/></a>
							</li>
							<li class="c-last">
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/12.jpg" alt="" class="img-responsive"/></a>
							</li>
						</ul>
						<ul class="c-works">
							<li class="c-first">
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/014.jpg" class="img-responsive" alt=""/></a>
							</li>
							<li>
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/011.jpg" class="img-responsive" alt=""/></a>
							</li>
							<li class="c-last">
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/15.jpg" class="img-responsive" alt=""/></a>
							</li>
						</ul>
						<ul class="c-works">
							<li class="c-first">
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/015.jpg" class="img-responsive" alt=""/></a>
							</li>
							<li>
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/013.jpg" class="img-responsive" alt=""/></a>
							</li>
							<li class="c-last">
								<a href="#"><img src="<?php echo site_url();?>assets/img/content/stock/13.jpg" class="img-responsive" alt=""/></a>
							</li>
						</ul>
						<a href="#" class="btn btn-md c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-square c-btn-bold c-read-more hide">View More</a>
					</div>
					<?php };?>	
				</div>
				<div class="col-md-3">
					<div class="c-container c-last">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>">Find us</h3>
							<div class="c-line-left hide">
							</div>
							<p class="c-text <?php echo $c_font;?>">
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed elit diam nonummy ad minim.
							</p>
						</div>
						<?php if(!empty($menu['footer'])){;?>
						<ul class="c-socials">
							<?php foreach($menu['footer'] as $f){;?>
								<?php if($f->metadescription == 'socmed'){;?>
								<li>
									<a href="<?php echo $f->tagline;?>" target="_blank"><i class="socicon-btn socicon-<?php echo $f->title;?> tooltips c-theme-on-hover"></i></a>
								</li>
								<?php };?>
							<?php };?>
						</ul>
						<?php };?>
						<!--<ul class="c-address">
							<li>
								<i class="icon-pointer c-theme-font"></i> One Boulevard, Beverly Hills
							</li>
							<li>
								<i class="icon-call-end c-theme-font"></i> +1800 1234 5678
							</li>
							<li>
								<i class="icon-envelope c-theme-font"></i> email@example.com
							</li>
						</ul>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-postfooter c-bg-dark-2">
		<div class="container">
			<p class="c-font-oswald c-font-14 c-font-grey">
				Copyright &copy; <?php echo $footer['copyright'];?>
			</p>
		</div>
	</div>
</footer>
<?php };?>
<!-- END: LAYOUT/FOOTERS/FOOTER-5 -->
<?php }elseif($style_footer['footer'] == 'slim'){;?>

<footer class="c-layout-footer c-layout-footer-3 <?php echo $c_bg;?>" style="<?php echo $ccs_m_f_custom;?>">
<?php if(isset($skeleton_footer)){;?>
	<div class="c-prefooter" style="background-color: #394048;padding:0;padding-top:20px">
		<div class="container">
			<div class="row">
<?php $sk=1;foreach($skeleton_footer as $sk_footer){
	$sk_first = '';
	if($sk == 1){
		$sk_first = 'c-first';
	}
	;?>
	<div class="<?php echo $sk_footer->column;?>">
		<div class="c-container <?php echo $sk_first;?>">
			<div class="c-content-title-1">
				<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>">
					<?php echo $sk_footer->title;?>
					<span class="c-theme-font"></span>
				</h3>
				<div class="c-line-left hide">
				</div>
				<p class="c-text <?php echo $c_font;?>">
					<?php echo $sk_footer->text;?>
				</p>
				<?php if($sk_footer->data_type == 'link'){;?>
					<?php if(!empty($menu['footer'])){;?>
					<ul class="c-links">
						<?php foreach($menu['footer'] as $f){;?>
							<?php if($f->metadescription == 'page' || $f->metadescription == 'module'){;?>
							<li>
								<a class="c-theme-on-hover <?php echo $c_font;?>" href="<?php echo $menu['link'].'/'.$f->tagline;?>"><?php echo $f->title;?></a>
							</li>
							<?php };?>
						<?php };?>
					</ul>
					<?php };?>
				<?php }elseif($sk_footer->data_type == 'blog'){;?>
				<div class="c-blog">
					<?php if(is_array($sk_footer->data)){;?>
						<?php $sk_data_last = count($sk_footer->data);$sk_dt = 1;
						foreach($sk_footer->data as $sk_data){
							$m_footer = $sk_footer->data_module;
							if(isset($sk_data->img)){
								$sk_data_img = $sk_data->img;
							}else{
								$sk_data_img = base_url('assets').'/'.$zone.'/'.$m_footer.'/'.$sk_data->image_square;
							}
							if(isset($sk_data->url)){
								$sk_data_url = $sk_data->url;
							}else{
								$sk_data_url = $menu['link'].'/'.$m_footer.'/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
							}
							$sk_last = '';
							if($sk_last == $sk_data_last){
								$sk_last = 'c-last';
							}
							
							;?>
							<div class="c-post <?php echo $sk_last;?>">
								<div class="c-post-img">
									<img src="<?php echo $sk_data_img;?>" alt="" class="img-responsive"/>
								</div>
								<div class="c-post-content">
									<h4 class="c-post-title">
										<a class="c-theme-on-hover <?php echo $c_font;?>" href="<?php echo $sk_data_url;?>" target="_blank">
											<?php echo $sk_data->title;?>
										</a>
									</h4>
									<p class="c-text <?php echo $c_font;?>">
										<?php echo $sk_data->category;?>
									</p>
								</div>
							</div>
						<?php $sk_dt++;};?>
					<?php };?>
				</div>
				<?php }elseif($sk_footer->data_type == 'socmed'){;?>
					<?php if(!empty($menu['footer'])){;?>
					<ul class="c-socials">
						<?php foreach($menu['footer'] as $f){;?>
							<?php if($f->metadescription == 'socmed'){;?>
							<li>
								<a href="<?php echo $f->tagline;?>" target="_blank"><i class="socicon-btn socicon-<?php echo $f->title;?> tooltips c-theme-on-hover"></i></a>
							</li>
							<?php };?>
						<?php };?>
					</ul>
					<?php };?>
				<?php }elseif($sk_footer->data_type == 'image'){;?>
					<?php if(is_array($sk_footer->data)){;?>
						<?php $di = 0;foreach($sk_footer->data as $sk_data){
							if($sk_footer->data_module == 'event'){
								$sk_data_img = base_url('assets').'/'.$zone.'/event/'.$sk_data->image_square;
								$sk_data_url = $menu['link'].'/event#cbp='.$menu['link'].'/event/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
							}else{
								$m_footer = $sk_footer->data_module;
								if(isset($sk_data->img)){
									$sk_data_img = $sk_data->img;
								}else{
									$sk_data_img = base_url('assets').'/'.$zone.'/'.$m_footer.'/'.$sk_data->image_square;
								}
								if(isset($sk_data->url)){
									$sk_data_url = $sk_data->url;
								}else{
									$sk_data_url = $menu['link'].'/'.$m_footer.'/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
								}
							}
							;?>
							<?php if(($di%3) == 0){;?>
							<ul class="c-works">
							<?php };?>
								<li class="">
									<a href="<?php echo $sk_data_url;?>">
										<img src="<?php echo $sk_data_img;?>" alt="" class="img-responsive"/>
									</a>
								</li>
							<?php if((($di%3) == 2) && ($di > 0)){;?>
							</ul>
							<?php };?>
						<?php $di++;};?>
					<?php };?>
				<?php };?>
			</div>	
		</div>	
	</div>
<?php $sk++;};?>
		</div>
	</div>
<?php };?>
	
	<div class="c-postfooter c-bg-dark-2" style="background-color:#000 !important;padding: 15px 0px 5px;">
		<div class="container">
			<p class="c-font-oswald c-font-14 c-font-grey" style="float:left">
				Copyright &copy; <?php echo $footer['copyright'];?>
			</p>
			<span style="float:right">
			<?php if(!empty($menu['footer'])){;?>
				<?php foreach($menu['footer'] as $f){;?>
					<?php if($f->metadescription == 'socmed'){;?>
						<a href="<?php echo $f->tagline;?>" target="_blank"><i class="socicon-btn socicon-<?php echo $f->title;?> tooltips c-theme-on-hover" style="color:#fff;border: 0;padding: 5px;font-size:20px"></i></a>
					<?php };?>
				<?php };?>
			<?php };?>
			</span>
		</div>
	</div>
</footer>
<?php };?>
<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
	<i class="icon-arrow-up"></i>
</div>
<!-- END: LAYOUT/FOOTERS/GO2TOP -->
<?php };?>