<style type="text/css">
@-moz-keyframes throbber-loader {
  0% {
    background: #dde2e7;
  }
  10% {
    background: #6b9dc8;
  }
  40% {
    background: #dde2e7;
  }
}
@-webkit-keyframes throbber-loader {
  0% {
    background: #dde2e7;
  }
  10% {
    background: #6b9dc8;
  }
  40% {
    background: #dde2e7;
  }
}
@keyframes throbber-loader {
  0% {
    background: #dde2e7;
  }
  10% {
    background: #6b9dc8;
  }
  40% {
    background: #dde2e7;
  }
}
/* :not(:required) hides these rules from IE9 and below */
.throbber-loader:not(:required) {
  -moz-animation: throbber-loader 2000ms 300ms infinite ease-out;
  -webkit-animation: throbber-loader 2000ms 300ms infinite ease-out;
  animation: throbber-loader 2000ms 300ms infinite ease-out;
  background: #dde2e7;
  display: inline-block;
  position:relative;
  top: 50%;
  left: 50%;
  text-indent: -9999px;
  width: 0.9em;
  height: 1.5em;
  margin: 0 1.6em;
}
.throbber-loader:not(:required):before, .throbber-loader:not(:required):after {
  background: #dde2e7;
  content: '\x200B';
  display: inline-block;
  width: 0.9em;
  height: 1.5em;
  position: absolute;
  top: 0;
}
.throbber-loader:not(:required):before {
  -moz-animation: throbber-loader 2000ms 150ms infinite ease-out;
  -webkit-animation: throbber-loader 2000ms 150ms infinite ease-out;
  animation: throbber-loader 2000ms 150ms infinite ease-out;
  left: -1.6em;
}
.throbber-loader:not(:required):after {
  -moz-animation: throbber-loader 2000ms 450ms infinite ease-out;
  -webkit-animation: throbber-loader 2000ms 450ms infinite ease-out;
  animation: throbber-loader 2000ms 450ms infinite ease-out;
  right: -1.6em;
}
</style>
<!--<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>-->
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
<script src="<?php echo base_url();?>assets/frontend/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/jquery.easing.min.js" type="text/javascript"></script>
<!-- END: CORE PLUGINS -->
<!-- BEGIN: LAYOUT PLUGINS -->
<script src="<?php echo base_url();?>assets/frontend/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->
<!-- BEGIN: THEME SCRIPTS -->
<script src="<?php echo base_url();?>assets/frontend/js/components.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/js/components-shop.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/js/app.js" type="text/javascript"></script>
<?php if(!empty($custom_js)){ echo $custom_js;}?>
<script>
	<?php if(($style_header['navbar'] == 'light-transparent')){;?>
	$(document).ready(function()
	{
		var slider = $('.c-layout-revo-slider .tp-banner');
		var cont = $('.c-layout-revo-slider .tp-banner-container');
		var api = slider.show().revolution(
		{
			delay: 5000,
			startwidth: 1170,
			startheight: 620,
			navigationType: "hide",
			navigationArrows: "solo",
			touchenabled: "on",
			onHoverStop: "on",
			keyboardNavigation: "off",
			/* navigationStyle: "circle",
			navigationHAlign: "center",
			navigationVAlign: "bottom", */
			navigationType:"bullet",
	        //navigationArrows:"",
	        navigationStyle:"round c-tparrows-hide c-theme",
	        navigationHAlign: "right",
	        navigationVAlign: "bottom", 
	        navigationHOffset:60,
	        navigationVOffset:60,
			spinner: "spinner2",
			fullScreen: "on",
			fullScreenAlignForce: "on",
			fullScreenOffsetContainer: '',
			shadow: 0,
			fullWidth: "off",
			forceFullWidth: "off",
			hideTimerBar: "on",
			hideThumbsOnMobile: "on",
			hideNavDelayOnMobile: 1500,
			hideBulletsOnMobile: "on",
			hideArrowsOnMobile: "on",
			hideThumbsUnderResolution: 0
		});
		api.bind("revolution.slide.onchange", function(e, data)
		{
			$('.c-layout-header').removeClass('hide');
			setTimeout(function()
			{
				$('.c-singup-form').fadeIn();
			}, 1500);
		});
	}); //ready
	<?php }else{;?>
	
		<?php if($zone == 'shisha'){;?>
		$(document).ready(function() { 
			winWid = $(window).width();
			if(winWid < 768) {
				$('.img-parallax-effect').each(function(ii){
					h = $(this).height()+120;
					$(this).css('background-size','auto '+h+'px');
					$(this).css('background-attachment','scroll');
					var target = $(this).attr('id');
				});
				(function(){
					var speed = 0.02;

					window.onscroll = function(){
						$(".img-parallax-effect").each(function(el){
							var windowYOffset = window.pageYOffset;
							var windowYOffset = windowYOffset - this.offsetTop;
							var elBackgrounPos = "50% " + (windowYOffset * speed) + "px";
							this.style.backgroundPosition = elBackgrounPos;
						});
					};
				})();
				App.init(); // init core    
				// init main slider
				var slider = $('.c-layout-revo-slider .tp-banner');
				var cont = $('.c-layout-revo-slider .tp-banner-container');
				var api = slider.show().revolution({
					delay: 3000,    
					startwidth:1170,
					startheight: 900,
					navigationType: "hide",
					navigationArrows: "solo",
					touchenabled: "on",
					onHoverStop: "on",
					keyboardNavigation: "off",
					navigationType:"bullet",
					navigationArrows:"",
					navigationStyle:"round c-tparrows-hide c-theme",
					navigationHAlign: "right",
					navigationVAlign: "bottom", 
					navigationHOffset:60,
					navigationVOffset:60,
					spinner: "spinner2",
					fullScreen: 'on',   
					fullScreenAlignForce: 'on', 
					fullScreenOffsetContainer: '.c-layout-header',
					shadow: 0,
					fullWidth: "off",
					forceFullWidth: "off",
					hideTimerBar:"on",
					hideThumbsOnMobile: "on",
					hideNavDelayOnMobile: 1500,
					hideBulletsOnMobile: "on",
					hideArrowsOnMobile: "on",
					hideThumbsUnderResolution: 0
				});
			}else{
				App.init(); // init core    

				// init main slider
				var slider = $('.c-layout-revo-slider .tp-banner');
				var cont = $('.c-layout-revo-slider .tp-banner-container');
				var api = slider.show().revolution({
					delay: 3000,    
					startwidth:1170,
					startheight: 900,
					navigationType: "hide",
					navigationArrows: "solo",
					touchenabled: "on",
					onHoverStop: "on",
					keyboardNavigation: "off",
					navigationType:"bullet",
					navigationArrows:"",
					navigationStyle:"round c-tparrows-hide c-theme",
					navigationHAlign: "right",
					navigationVAlign: "bottom", 
					navigationHOffset:60,
					navigationVOffset:60,
					spinner: "spinner2",
					fullScreen: 'on',   
					fullScreenAlignForce: 'on', 
					fullScreenOffsetContainer: '.c-layout-header',
					shadow: 0,
					fullWidth: "off",
					forceFullWidth: "off",
					hideTimerBar:"on",
					hideThumbsOnMobile: "on",
					hideNavDelayOnMobile: 1500,
					hideBulletsOnMobile: "on",
					hideArrowsOnMobile: "on",
					hideThumbsUnderResolution: 0
				});
				(function(){
					var speed = 0.02;

					window.onscroll = function(){
						$(".img-parallax-effect").each(function(el){
							var windowYOffset = window.pageYOffset;
							var windowYOffset = windowYOffset - this.offsetTop;
							var elBackgrounPos = "50% " + (windowYOffset * speed) + "px";
							this.style.backgroundPosition = elBackgrounPos;
						});
					};

				})();
			}
			
		});
		
		<?php }else{;?>
	$(document).ready(function() { 	
		App.init(); // init core    

		// init main slider
		var slider = $('.c-layout-revo-slider .tp-banner');
		var cont = $('.c-layout-revo-slider .tp-banner-container');
		var api = slider.show().revolution({
			delay: 3000,    
			startwidth:1170,
			startheight: 900,
			navigationType: "hide",
			navigationArrows: "solo",
			touchenabled: "on",
			onHoverStop: "on",
			keyboardNavigation: "off",
			navigationType:"bullet",
			navigationArrows:"",
			navigationStyle:"round c-tparrows-hide c-theme",
			navigationHAlign: "right",
			navigationVAlign: "bottom", 
			navigationHOffset:60,
			navigationVOffset:60,
			spinner: "spinner2",
			fullScreen: 'on',   
			fullScreenAlignForce: 'on', 
			fullScreenOffsetContainer: '.c-layout-header',
			shadow: 0,
			fullWidth: "off",
			forceFullWidth: "off",
			hideTimerBar:"on",
			hideThumbsOnMobile: "on",
			hideNavDelayOnMobile: 1500,
			hideBulletsOnMobile: "on",
			hideArrowsOnMobile: "on",
			hideThumbsUnderResolution: 0
		});
	});		
		<?php };?>

	<?php };?>
	</script>
<script type="text/javascript">
	function scrolltonote(selector){
		 var sel = selector
		 $('html, body').animate({
			scrollTop: $(sel).offset().top
		}, 1000);
	}
	$(document).ready(function(){
		var base_url = '<?php echo base_url();?>';
		$('#f-login').submit(function(event){ 
			event.preventDefault();  
			var login_url = $(this).attr('action');
			$('#login-button').attr('disabled','disabled');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				u: $('#login-username').val(),
				p: $('#login-password').val(),
				type: 1,
			};
			$.ajax({
				type: "POST",
				url: base_url+login_url,
				data: data,
				cache: false,
				dataType: 'json',
				success: function(data){
					if (data.status == "success"){
						$('<p>'+data.m+'</p>').appendTo('#success');
						$('#success').show();scrolltonote('#login-form');
						$('#success').fadeTo(3000, 500).slideUp(500);
						setTimeout(function(){location.reload(true);}, 4000);
					}else{
						$('<p>'+data.m+'</p>').appendTo('#fail');
						$('#login-button').removeAttr('disabled');
						$('#fail').show();
						scrolltonote('#login-form');
						$('#fail').fadeTo(3000, 500).slideUp(500);
					}
				},
				/* error : function(jqXHR){
					alert('Maaf! Terdapat kesalahan pada sistem'); location.reload(true);
				}	 */
			});
			$('#fail').empty(); $('#success').empty();
		});
		$('#member-area').submit(function(event){ 
			event.preventDefault();  
			var login_url = $(this).attr('action');
			$('#ma').attr('disabled','disabled');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				reg_id: $('#member-email').val(),
				name: $('#member-name').val(),
				type: 1,
			};
			$.ajax({
				type: "POST",
				url: base_url+login_url,
				data: data,
				cache: false,
				dataType: 'json',
				success: function(data){
					if (data.status == "success"){
						$('<p>'+data.m+'</p>').appendTo('#success-member');
						$('#success-member').show();scrolltonote('#member-area');
						$('#success-member').fadeTo(3000, 500).slideUp(500);
						setTimeout(function(){
							window.location = base_url+"dashboard";
						}, 4000);
					}else{
						$('<p>'+data.m+'</p>').appendTo('#fail-member');
						$('#ma').removeAttr('disabled');
						$('#fail-member').show();
						scrolltonote('#member-area');
						$('#fail-member').fadeTo(3000, 500).slideUp(500);
					}
				},
				/* error : function(jqXHR){
					alert('Maaf! Terdapat kesalahan pada sistem'); location.reload(true);
				}	 */
			});
			$('#fail-member').empty(); $('#success-member').empty();
		});
		$('#forget-password').submit(function(event){ 
			event.preventDefault();  
			var url = $(this).attr('action');
			$('#reset').attr('disabled','disabled');
			$('#fp').attr('disabled','disabled');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				e : $('#forget-email').val(),
			};
			var fp_url = $(this).attr('action');
			$.ajax({
				type: "POST",
				url: base_url+fp_url,
				data: data,
				cache: false,
				dataType: 'json',
				success: function(data){
					if (data.status == "success"){
						$('<p>'+data.m+'</p>').appendTo('#success-reset');
						$('#success-reset').show();scrolltonote('#forget-password-form');
						$('#success-reset').fadeTo(3000, 500).slideUp(500);
						setTimeout(function(){location.reload(true);}, 4000);
					}else{
						$('<p>'+data.m+'</p>').appendTo('#fail-reset');
						$('#fp').removeAttr('disabled');
						$('#fail-reset').show();
						scrolltonote('#forget-password-form');
						$('#fail-reset').fadeTo(3000, 500).slideUp(500);
					}
				}
			});
			$('#fail-reset').empty(); $('#success-reset').empty();
		});
	});
	$(document).ready(function(){
		var base_url = '<?php echo base_url();?>';
		$('#s-login').submit(function(event){ 
			event.preventDefault();  
			var login_url = $(this).attr('action');
			$('#login-button').attr('disabled','disabled');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				e: $('#signup-email').val(),
				u: $('#signup-username').val(),
				name: $('#signup-fullname').val(),
				p: $('#signup-password').val(),
				p2: $('#signup-password2').val()
			};
			$.ajax({
				type: "POST",
				url: base_url+login_url,
				data: data,
				cache: false,
				dataType: 'json',
				success: function(data){
					if (data.status == "success"){
						$('<p>'+data.m+'</p>').appendTo('#s-success');
						$('#s-success').show();scrolltonote('#login-form');
						$('#s-success').fadeTo(3000, 500).slideUp(500);
						setTimeout(function(){location.reload(true);}, 4000);
					}else{
						$('<p>'+data.m+'</p>').appendTo('#s-fail');
						$('#login-button').removeAttr('disabled');
						$('#s-fail').show();
						scrolltonote('#login-form');
						$('#s-fail').fadeTo(3000, 500).slideUp(500);
					}
				},
				/* error : function(jqXHR){
					alert('Maaf! Terdapat kesalahan pada sistem'); location.reload(true);
				}	 */
			});
			$('#s-fail').empty(); $('#s-success').empty();
		});
		$('#s2-login').submit(function(event){ 
			event.preventDefault();  
			var login_url = $(this).attr('action');
			$('#login-button').attr('disabled','disabled');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				rt_id: $('#type_reseller').val(),
				type_id: $('#type_id').val(),
				e: $('#signup2-email').val(),
				u: $('#signup2-username').val(),
				name: $('#signup2-fullname').val(),
				mobile: $('#signup2-mobile').val()
			};
			$.ajax({
				type: "POST",
				url: base_url+login_url,
				data: data,
				cache: false,
				dataType: 'json',
				success: function(data){
					if (data.status == "success"){
						$('<p>'+data.m+'</p>').appendTo('#s2-success');
						$('#s2-success').show();scrolltonote('#login-form');
						$('#s2-success').fadeTo(3000, 500).slideUp(500);
						setTimeout(function(){location.reload(true);}, 4000);
					}else{
						$('<p>'+data.m+'</p>').appendTo('#s2-fail');
						$('#login-button').removeAttr('disabled');
						$('#s2-fail').show();
						scrolltonote('#login-form');
						$('#s2-fail').fadeTo(3000, 500).slideUp(500);
					}
				},
				/* error : function(jqXHR){
					alert('Maaf! Terdapat kesalahan pada sistem'); location.reload(true);
				}	 */
			});
			$('#s2-fail').empty(); $('#s2-success').empty();
		});
	});
</script>
<script id="crud_script">
// ajax to call view inside page
$('.detail').on("click", function(e) {
	e.preventDefault();
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	var id = $(this).attr('id');
	var obj = $(this).data('url');
	var part = $(this).data('url2');
	var lang = $(this).data('lang');
	var param = $(this).data('param');
	var param2 = $(this).data('param2');
	var param3 = $(this).data('param3');
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		id : id,
		param : param,
		param2 : param2,
		param3 : param3,
		language_id : lang
	};
	$('.detail_content').empty();
	$('.detail_content').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
	var url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
	$.ajax({
		url : url,
		type: "POST",
		dataType: 'html',
		data: data,
		success: function(html){
			$('.detail_content').empty();
			$('.detail_content').append(html);
			$('.btn').each(function(){$(this).removeAttr('disabled');});
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
	function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script');
});// ajax to call view inside page
$('.detail2').on("click", function(e) {
	e.preventDefault();
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	var id = $(this).attr('id');
	var obj = $(this).data('url');
	var part = $(this).data('url2');
	var lang = $(this).data('lang');
	var param = $(this).data('param');
	var param2 = $(this).data('param2');
	var url3 = $(this).data('url3');
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		id : id,
		param : param,
		param2 : param2,
		url3 : url3,
		language_id : lang
	};
	$('.detail_content2').empty();
	$('.detail_content2').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
	var url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
	$.ajax({
		url : url,
		type: "POST",
		dataType: 'html',
		data: data,
		success: function(html){
			$('.detail_content2').empty();
			$('.detail_content2').append(html);
			$('.btn').each(function(){$(this).removeAttr('disabled');});
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
	function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script');
});
$('.detail3').on("click", function(e) {
	e.preventDefault();
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	var id = $(this).attr('id');
	var obj = $(this).data('url');
	var part = $(this).data('url2');
	var lang = $(this).data('lang');
	var param = $(this).data('param');
	var param2 = $(this).data('param2');
	var view = $(this).data('view');
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		id : id,
		param : param,
		param2 : param2,
		view : view,
		language_id : lang
	};
	$('.detail_content3').empty();
	$('.detail_content3').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
	var url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
	$.ajax({
		url : url,
		type: "POST",
		dataType: 'html',
		data: data,
		success: function(html){
			$('.detail_content3').empty();
			$('.detail_content3').append(html);
			$('.btn').each(function(){$(this).removeAttr('disabled');});
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
	function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script');
});
// end ajax to call view inside page

$(document).ready(function(){
	$(".menu_type").on("click", function(e) {
		$(".menu_type").removeClass("c-active");
		$(this).addClass("c-active");
	});
});
</script>
<script id="cart_script">
function ajax_post($type, $url, $data, functtoexec)
{	
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	$.ajax({
			url : $url,
			type: "POST",
			dataType: $type,
			data: $data,
			success: function(data){
				$('.btn').each(function(){$(this).removeAttr('disabled');});
				functtoexec(data);
			}
	});
}

function striptags(string)
{
	var regex = /(<([^>]+)>)/ig;
	var text = string.replace(regex, "");
	return text 
}

function limitword(textToLimit, wordLimit)
{	
	var finalText = "";
	var text2 = textToLimit.replace(/\s+/g, ' ');
	var text3 = text2.split(' ');
	var numberOfWords = text3.length;
	var i=0;
	if(numberOfWords > wordLimit)
	{
		for(i=0; i< wordLimit; i++)
		finalText = finalText+" "+ text3[i]+" ";
		return finalText+"...";
	}
	else 
	{
		return textToLimit;
	}
}

function currencyformat(input)
{
	var output = input.toString().replace('.',',');
		output = output.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
	return output;
}

function count_bill(item)
{
	var qty = $('#qty-'+item).val();
		price = $('#'+item).data('value');
		subtotal = qty * price;
		total = 0;
	$('#subtotal-'+item).empty();
	$('#subtotal-'+item).append('IDR '+currencyformat(subtotal)+',00');
	$('#subtotal-'+item).data('subtotal', subtotal);
	$('.subtotal').each(function(){
		total += Number($(this).data('subtotal'));
	});
	$('#total-bill').empty();
	$('#total-bill').append('IDR '+currencyformat(total)+',00')
}

function convertmonth(string) {
    var d = string.split(/[-:\s]/);
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    
    return months[parseInt(d[1], 10) - 1];
}

function convertday(string) {
	var t = string.split(/[- :]/);

	// Apply each element to the Date function
	var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    var d = new Date(d);
	var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var result = days[parseInt(d.getDay())];
    return result;
}

function convertdate(string) {
	var t = string.split(/[- :]/);

	// Apply each element to the Date function
	var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    var d = new Date(d);
	var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var result = days[parseInt(d.getDay())]+', '+d.getDate()+' '+months[parseInt(d.getMonth(), 10)]+' '+d.getFullYear();
    return result;
}

function get_data_eventgal(event)
{
	var var_view = '#event-gall';
	var var_data = '#data-eventgal';
	if (($(var_data).length != 0) && ($(var_view).length != 0))
	{
		$('#year > .cbp-filter-item').each(function(){
			$(this).removeClass('cbp-filter-item-active');
		});
		$('[data-year="'+event+'"]').addClass('cbp-filter-item-active');
		$('#grid-container').empty();
		var obj = $(var_data).data('url-first');
			part = $(var_data).data('url-second');
			base_url = $(var_data).data('base-url');
			base_modul = $(var_data).data('base-modul');
			url_zone = $(var_data).data('url-zone');
			cat = $(var_data).data('id');
			month = $('#year > .cbp-filter-item-active').data('month');
			zone = $(var_data).data('zone');
			data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : $('#year > .cbp-filter-item-active').data('year'),
				param : $(var_view).data('param'),
				sort : 'DESC',
				order : $('.month > .cbp-filter-item-active').data('filter'),
				limit : '8',
				category: cat,
				month: month,
				offset : $(var_view).data('offset')
			};
			url = "<?php echo $menu['link'];?>/list/view_detail/list";
			$('#loader').fadeIn("slow");
			success_fun = function(json){
				$('#grid-container').empty();
				$('#loader').fadeOut("slow");
				if (json.data.length === 0) {
					console.log('empty');
				}else{
					$.each(json.data, function(key, val) {
						view_grid = [];
						$.each(val.datas, function(key2, val2) {
							if(val.section.title.toLowerCase().indexOf("weekly") >= 0){
								var month = convertday(val2.datecreated);
							}else{
								if(convertmonth(val2.datecreated) != convertmonth(val2.dateupdated)){
									var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
									var start = val2.datecreated.split(/[-:\s]/),
										start = start[1];
									var end = val2.dateupdated.split(/[-:\s]/),
										end = end[1];
									var month = '';	
									if(start < end){	
										for (var i = start; i <= end; i++) {
											month += months[parseInt(i, 10) - 1]+' ';
										}
									}else{//sampai 12 desember
										for (var i = start; i <= 12; i++) {
											month += months[parseInt(i, 10) - 1]+' ';
										}
									}
								}else{
									var month = convertday(val2.datecreated);
								}
							}
							
							view_grid[key2] = '<div class="cbp-item '+month+'">'
							+'<div class="cbp-caption">'
								+'<div class="cbp-caption-defaultWrap">'
									+'<img src="'+base_url+'assets/'+zone+'/'+base_modul+'/'+val2.image_square+'" alt=""></div>'
								+'<div class="cbp-caption-activeWrap">'
									+'<div class="c-masonry-border"></div>'
									+'<div class="cbp-l-caption-alignCenter">'
										+'<div class="cbp-l-caption-body">'
											+'<a href="'+url_zone+'/'+base_modul+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'/'+val2.title.toLowerCase().replace(/\s+/g, '_')+'" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>'
											+'<a href="'+base_url+'assets/'+zone+'/'+base_modul+'/'+val2.image_square+'" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="'+val2.title+'">Zoom</a>'
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>'
							+'<a href="'+url_zone+'/'+base_modul+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'/'+val2.title.toLowerCase().replace(/\s+/g, '_')+'" class="cbp-singlePage cbp-l-grid-masonry-projects-title">'+val2.title+'</a>'
							+'<div class="cbp-l-grid-masonry-projects-desc">'+val2.tagline.replace('_', ' ')+'</div>'
						+'</div>';
						});
						//var date_created = val.datas[0].datecreated;
						$('#grid-container').append(view_grid);
					});
				}
				if(event){
					jQuery('#grid-container').cubeportfolio('destroy');
					$.getScript(base_url+"assets/frontend/js/scripts/pages/masonry-gallery.js");
				}else{
					var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
					var dt = new Date();
					$.getScript(base_url+"assets/frontend/js/scripts/pages/masonry-gallery.js").done(function( script, textStatus ) {
						var cek = $('[data-month="'+(dt.getMonth()+1)+'"]');
						if(cek.length){
							$('[data-month="'+(dt.getMonth()+1)+'"]').addClass('cbp-filter-item-active');
						}else{
							$('[data-year="'+dt.getFullYear()+'"]').addClass('cbp-filter-item-active');
						}
						/* $('#filters-container > .cbp-filter-item').each(function(){
							$(this).removeClass('cbp-filter-item-active');
						});
						$('[data-filter=".'+monthNames[parseInt(dt.getMonth(), 10)]+'"]').addClass('cbp-filter-item-active'); */
					});
				}
			};
			ajax_post('json', url, data, success_fun);
	}
}

function view_pagination($numpage, $active)
{
	var pagination = '';
	var class_active = '';
	var start_number = $active-2;
	
	var next = '';

	if ($active < 3)
	{
		start_number = 1;
	}

	var end_number = $numpage - start_number;
	number_list = $numpage;
	if (end_number >= 4)
	{
		number_list = start_number + 4;
		var next = '<li class="c-next"><a class="upage" href="#" data-offset="'+(number_list)+'"><i class="fa fa-angle-right"></i></a></li>';
	}

	var prev = '';
	if (start_number > 1)
	{
		var prev = '<li class="c-prev"><a class="upage" href="#" data-offset="'+(start_number-2)+'"><i class="fa fa-angle-left"></i></a></li>';
	}

	for(i=start_number; i <= number_list; i++)
	{
		var class_active = '';
		if (i == $active)
		{
			class_active = 'class="c-active"';
		}
		pagination = pagination+'<li '+class_active+'><a class="upage" href="#" data-offset="'+(i-1)+'">'+i+'</a></li>';
	}
	return prev+pagination+next;
}

function scrolltonote(selector){
	 var sel = selector
	 $('html, body').animate({
		scrollTop: $(sel).offset().top
	}, 1000);
}

function get_data_news($offset)
{	
	var var_data = '#data-newslist';
	var var_view = '#view-newslist';

	if(($(var_data).length != 0) && ($(var_data).length != 0)) {
		var obj = $(var_data).data('url-first');
			view_product = $(var_data).val();
			part = $(var_data).data('url-second');
			base_url = $(var_data).data('base-url');
			base_modul = $(var_data).data('base-modul');
			url_zone = $(var_data).data('url-zone');
			zone = $(var_data).data('zone');
			data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : $(var_data).data('id'),
				param : $(var_data).data('param'),
				sort : 'desc',
				order : 'year',
				limit : 20,
				offset : $offset
			};
			url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
			$('#loader').fadeIn("slow");
			success_fun = function(json){
				$(var_view).empty();
				$('.c-content-pagination').empty();
				if(json.data.length){
					pagination = view_pagination(json.numpage, json.ap);
					$.each(json.data, function(key, val) {
						view_grid = [];
						$.each(val.datas, function(key2, val2) {
							if(val2.image_square.length){
								view_grid[key2] = '<div class="c-content-blog-post-1" style="margin-bottom:0px">'+
									'<div class="col-md-3" style="padding: 0;">'+
										'<img src="'+base_url+'assets/'+zone+'/'+base_modul+'/'+val2.image_square+'" style="width:100%">'+
									'</div>'+
									'<div class="col-md-9">'+
										'<div class="c-title c-font-bold c-font-uppercase" style="margin-top:0;margin-bottom:0;">'+
											'<a href="'+url_zone+'/'+base_modul+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'/'+val2.title.toLowerCase().replace(/\s+/g, '_')+'">'+val2.title+'</a>'+
										'</div>'+
										'<div class="c-panel" style="margin-bottom:10px;">'+
											'<div class="c-author">'+
												'<a href="#">By'+
												'	<span class="">Admin</span>'+
												'</a>'+
											'</div>'+
											'<div class="c-date">on'+
												'<span class="c-font-uppercase"> '+convertdate(val2.datecreated)+'</span>'+
											'</div>'+
											'<ul class="c-tags c-theme-ul-bg">'+
												'<li>'+val.section.title.toLowerCase()+'</li>'+
											'</ul>'+
										'</div>'+
										'<div class="c-desc" style="margin-bottom: 45px;">'+
											''+limitword($.trim(val2.description.replace(/(<([^>]+)>)/ig,"")),40)+''+
											'<br>'+
											'<a href="'+url_zone+'/'+base_modul+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'/'+val2.title.toLowerCase().replace(/\s+/g, '_')+'" class="btn c-theme-btn c-btn-square" style="float: right;">'+
											'	Read More'+
											'</a>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<hr>';
							}else{
								view_grid[key2] = '<div class="c-content-blog-post-1" style="margin-bottom:0px">'+
									'<div class="col-md-12" style="padding-left:0px">'+
										'<div class="c-title c-font-bold c-font-uppercase" style="margin-top:0;margin-bottom:0;">'+
											'<a href="'+url_zone+'/'+base_modul+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'/'+val2.title.toLowerCase().replace(/\s+/g, '_')+'">'+val2.title+'</a>'+
										'</div>'+
										'<div class="c-panel" style="margin-bottom:10px;">'+
											'<div class="c-author">'+
												'<a href="#">By'+
												'	<span class="">Admin</span>'+
												'</a>'+
											'</div>'+
											'<div class="c-date">on'+
												'<span class="c-font-uppercase"> '+convertdate(val2.datecreated)+'</span>'+
											'</div>'+
											'<ul class="c-tags c-theme-ul-bg">'+
												'<li>'+val.section.title.toLowerCase()+'</li>'+
											'</ul>'+
										'</div>'+
										'<div class="c-desc" style="margin-bottom: 45px;">'+
											''+limitword($.trim(val2.description.replace(/(<([^>]+)>)/ig,"")),40)+''+
											'<br>'+
											'<a href="'+url_zone+'/'+base_modul+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'/'+val2.title.toLowerCase().replace(/\s+/g, '_')+'" class="btn c-theme-btn c-btn-square" style="float: right;">'+
											'	Read More'+
											'</a>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<hr>';
							}
						});
						//var date_created = val.datas[0].datecreated;
						$('#view-newslist').append(view_grid);
						$('.c-content-pagination').append(pagination);
						scrolltonote('body');
						$('#loader').fadeOut("slow");
					});
				}else{
					$('#loader').fadeOut("slow");
					view_grid = '<h1 style="text-align:center">Sorry, No News With This Category</h1>';
					$('#view-ewslist').append(view_grid);
				}
				
				
			};
		ajax_post('json', url,data, success_fun);
	}
}

function get_data_checkout()
{
	var var_data = '#data-checkout';
	
}

function add_data(type)
{
	if(type.length != 0){
		var var_data = '.add_data',
			data_id = $(var_data).attr('id'),
			var_view = '';
			if(type == 'wishlist'){
				var obj = 'cart',
					part = 'cart',
					param = 'wishlist';
			}
		var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : data_id,
			param : param
		}
		var url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
		success_fun =  function(json){
				if(typeof json.is_login != 'undefined'){
					if(json.is_login){
						view_add = $(var_data).text('Add to Wishlist').css();
						$(this).text('Remove Wishlist');
						$(this).css('background','#E7505A');
					}else{
						$('#login-form').modal('show');
					}
				}else{
					//for other
				}
			};
		ajax_post('json', url, data, success_fun);
	}
}

function get_variant_id(type)
{
	$('#variant').change(function() {
		if ($(this).find(':selected').val() != '') {
			var val = $(this).find(':selected').val(),
				item_id = $(this).find(':selected').data('id');
				prc = $(this).find(':selected').data('publish');
				discount = $(this).find(':selected').data('discount');
			$('.var_'+item_id+'').remove();
			if(discount != 'undefined'){
				real_price = prc;
				view_disc = '<br><span class="c-font-18 c-font-line-through c-font-red" id="cross-price"> IDR '+currencyformat(real_price)+'</span>';
				prc = (((100-discount)*prc)/100)+'.00';
				if(type == 'grid'){
					view_disc = '<br><span class="c-font-14 c-font-line-through c-font-red" id="cross-price"> IDR '+currencyformat(real_price)+'</span>';
				}
			}else{
				view_disc = '';
			}
			if(type == null){
				$('.add_to_cart[data-id="'+item_id+'"]').attr('data-variant',val);
				$('.add_to_cart[data-id="'+item_id+'"]').attr('data-price',prc);
				$('.add_to_cart[data-id="'+item_id+'"]').parent().prev().append('<p class="var_'+item_id+' c-price c-font-18 c-font-thin">IDR '+currencyformat(prc)+' &nbsp;										'+view_disc+'									</p>');
				$('.add_to_cart[data-page="'+item_id+'"]').attr('data-variant',val);
				$('.add_to_cart[data-page="'+item_id+'"]').attr('data-price',prc);
				$('.add_to_cart[data-page="'+item_id+'"]').parent().prev().append('<p class="var_'+item_id+' c-price c-font-18 c-font-thin">IDR '+currencyformat(prc)+' &nbsp;										'+view_disc+'									</p>');
			}else if(type == 'grid'){
				$('.add_to_cart[data-id="'+item_id+'"]').attr('data-variant',val);
				$('.add_to_cart[data-id="'+item_id+'"]').attr('data-price',prc);
				$('.add_to_cart[data-id="'+item_id+'"]').parent().parent().prev().append('<p class="var_'+item_id+' c-price c-font-14 c-font-thin">IDR '+currencyformat(prc)+' &nbsp;										'+view_disc+'									</p>');
				$('.add_to_cart[data-page="'+item_id+'"]').attr('data-variant',val);
				$('.add_to_cart[data-page="'+item_id+'"]').attr('data-price',prc);
				$('.add_to_cart[data-page="'+item_id+'"]').parent().parent().prev().append('<p class="var_'+item_id+' c-price c-font-14 c-font-thin">IDR '+currencyformat(prc)+' &nbsp;										'+view_disc+'									</p>');
			}
		}
	});
}

function slice_array_from_array_by_key(collection,key){
	// if the collections is an array
	if(collection instanceof Array) {
		if($.inArray(key, collection) != -1) {
			collection.splice($.inArray(key, collection), 1);
		}
	}
	// it's an object
	else if(collection.hasOwnProperty(key)) {
		delete collection.key;
	}

	return collection;
}

function get_data_product($offset)
{	
	var var_data = '#data-product';
	var var_view = '#view-product';
	var offset = $offset;

	if ($offset == 'page')
	{
		var offset = $(var_data+'-go-to-page').val();
	}

	if(($(var_data).length != 0) && ($(var_data).length != 0)) {
		var obj = $(var_data).data('url-first');
			view_product = $(var_data).val();
			part = $(var_data).data('url-second');
			base_url = $(var_data).data('base-url');
			base_modul = $(var_data).data('base-modul');
			url_zone = $(var_data).data('url-zone');
			zone = $(var_data).data('zone');
			tags = $('[name="tags"]:checked').val();
			data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : $(var_data).data('id'),
				param : $(var_data).data('param'),
				sort : $(var_data+'-show-sort :selected').data('sort'),
				order : $(var_data+'-show-sort').val(),
				limit : $(var_data+'-show-number').val(),
				range : $(var_data+'-range').val(),
				tags : tags,
				query : $('#keywords').val(),
				offset : offset
			};
			url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
			$('#loader').fadeIn("slow");
			success_fun = function(json){
				$(var_view).empty();
				$(var_data+'-go-to-page').empty();
				for (i = 1; i <= json.numpage; i++) { 
					$(var_data+'-go-to-page').append('<option value="'+(i-1)+'">'+i+'</option>');
				}
				$(var_data+'-go-to-page').val(offset);
				$('#loader').fadeOut("slow");
				if(json.data.length != 0){
					$.each(json.data, function(keys, vals) {
						$.each(vals.datas, function(key, val) {
							var product_title = val.title;
								cek_stock = true;
								stock = 0;
								product_id = val.object_id;
								discount = val.discount;
								price_publish = val.publish;
								product_img = val.image_square;
								product_description = val.description;
								product_section_title = vals.section.title;
								product_url = url_zone+'/'+base_modul+'/'+product_section_title.toLowerCase().replace(/\s/g, '_')+'/'+product_title.toLowerCase().replace(/\s/g,'_');
								real_price = ((100-discount)*price_publish)/100;
								if (discount == 0)
								{
									var real_price = price_publish;
								}
								if (view_product == 'grid')
								{
									var add_class = '';
								}
								else
								{
									var add_class = 'col-md-6';
								}
								view_option = [];
								view_option2 = [];
								v_v = [];
								if(val.vstatus == 1){
									variant_dsc_max = 0;
									c_i = 0;
									stock = 0;
									if(val.variant){
										
										
										variant_dsc_min = val.variant[0].discount;
										cek_category = val.variant[0].category.toLowerCase();
										
										$.each(val.variant, function(key, val){
											cek_stock = true;
											if(val.category.toLowerCase() != cek_category){
												cek_category = val.category.toLowerCase();
												v_v[c_i] = '<p class="c-title c-font-16 c-font-slim col-md-6" style="padding:0">'+cek_category+':</p>';
												c_i++;
											}
											if(cek_category == 'size'){
												view_option[key] = '<option value="'+val.settings_id+'" data-publish="'+val.publish+'" data-id="'+product_id+'" data-discount="'+val.discount+'">'+val.title+'</option>';
											}else{
												view_option2[key] = '<option value="'+val.settings_id+'" data-publish="'+val.publish+'" data-id="'+product_id+'" data-discount="'+val.discount+'">'+val.title+'</option>';
											}
											if(val.discount > variant_dsc_max){
												variant_dsc_max = val.discount;
											}
											if(val.discount < variant_dsc_min){
												variant_dsc_min = val.discount;
											}
											stock += parseInt(val.stock);
											if(stock < 1){
												cek_stock = false;
												overlay = '<div class="c-overlay-wrapper active">'+
															'<div class="c-overlay-content">'+
																'<a href="#" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Sold Out</a>'+
															'</div>'+
														'</div>';
												product_url = '#';		
											}else{
												overlay = '<div class="c-overlay-wrapper">'+
																'<div class="c-overlay-content">'+
																	'<a href="'+product_url+'" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>'+
																'</div>'+
															'</div>';
											}
										});
										variant = val.variant[0].settings_id;	
										rp = val.variant[0].publish;
										dsc = val.variant[0].discount;
										if(dsc){
											rp_dsc = rp;
											if (view_product == 'grid')
											{ 
												view_disc = '<span class="c-font-14 c-font-line-through c-font-red" id="cross-price"> IDR '+currencyformat(rp_dsc)+'</span>';
											}else{
												view_disc = '<br><span class="c-font-18 c-font-line-through c-font-red" id="cross-price"> IDR '+currencyformat(rp_dsc)+'</span>';
											}
											rp = ((100-dsc)*rp)/100
										}else{
											view_disc = '';
										}
										if (view_product == 'grid')
										{
											v_1 = '<div class="c-product-size"><select id="variant" onClick="get_variant_id('+"'"+'grid'+"'"+');">';
											v_2 = '</select></div>';
											variant1 = '<p class="c-title c-font-16 c-font-slim col-md-6" style="padding:0">Size:</p>'+
												v_1+view_option+v_2;
											variant2 = '<p class="c-title c-font-16 c-font-slim col-md-6" style="padding:0">Color:</p>'+
												v_1+view_option2+v_2;
											if(view_option2.length != 0){
												if(view_option.length != 0){
													v_variant = variant1+'<br>'+variant2;
												}else{
													v_variant = variant2;
												}
												
											}else{
												v_variant = variant1;
											}
											view_variant = '<div class="col-md-6" style="padding:0">'+
												v_variant+
											'</div>';	
											view_variant_prc = '<p class="var_'+product_id+' c-price c-font-14 c-font-thin">IDR '+currencyformat(rp)+' &nbsp;					<br>					'+view_disc+'									</p>';
										}else{
											v_1 = '<div class="c-product-size"><select id="variant" onClick="get_variant_id();">';
											v_2 = '</select></div>';
											variant1 = '<p class="c-product-meta-label c-product-margin-1 c-font-uppercase c-font-bold col-md-6" style="padding:0">Size:</p>'+
												v_1+view_option+v_2;
											variant2 = '<p class="c-product-meta-label c-product-margin-1 c-font-uppercase c-font-bold col-md-6" style="padding:0">Color:</p>'+
												v_1+view_option2+v_2;
											if(view_option2.length != 0){
												if(view_option.length != 0){
													v_variant = variant1+'<br>'+variant2;
												}else{
													v_variant = variant2;
												}
												
											}else{
												v_variant = variant1;
											}
											view_variant = '<div class="col-md-6" style="padding:0">'+v_variant+'</div>';	
											view_variant_prc = '<p class="var_'+product_id+' c-price c-font-18 c-font-thin">IDR '+currencyformat(rp)+' &nbsp;										'+view_disc+'									</p>';
										}
									}else{//out of stock
										cek_stock = false;
										overlay = '<div class="c-overlay-wrapper active">'+
															'<div class="c-overlay-content">'+
																'<a href="#" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Sold Out</a>'+
															'</div>'+
														'</div>';
										product_url = '#';				
										rp = real_price;
										view_variant = '';
										variant = '';
										view_variant_prc = '';
									}
								}else{
									rp = real_price;
									view_variant = '';
									variant = '';
									view_variant_prc = '';
									stock = val.stock;
									if(stock < 1){
										cek_stock = false;
										overlay = '<div class="c-overlay-wrapper active">'+
														'<div class="c-overlay-content">'+
															'<a href="#" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Sold Out</a>'+
														'</div>'+
													'</div>';
										product_url = '#';			
									}else{
										overlay = '<div class="c-overlay-wrapper">'+
														'<div class="c-overlay-content">'+
															'<a href="'+product_url+'" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>'+
														'</div>'+
													'</div>';
									}
								}
								if(val.cek === false)//add
								{
									if (view_product == 'grid')
									{
										var btn_cart = '<button type="submit" data-url="cart" data-url2="cart" data-param="add" data-id="'+product_id+'" data-qty="1" data-price="'+rp+'" data-name="'+product_title+'" data-variant="'+variant+'" class="add_to_cart btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold '+add_class+'" style="padding:6px">\
													<?php echo $text['add_cart'];?> </button>';
										var btn_wishlist = '<div class="btn-group c-border-top" role="group">\
											<a href="#" id="'+product_id+'" class="add_data btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" onClick="add_data('+"'"+'wishlist'+"'"+')">Wishlist</a>\
										</div>';
										if(cek_stock === false){
											var btn_cart = '<button type="submit" data-url="cart" data-url2="cart" data-param="add" data-id="'+product_id+'" data-qty="1" data-price="'+rp+'" data-name="'+product_title+'" data-variant="'+variant+'" class="btn btn-sm c-btn-square c-btn-uppercase c-btn-bold '+add_class+'" style="padding:6px">\
													Sold Out </button>';
										}
									}else{
										var btn_cart = '<button type="submit" data-url="cart" data-url2="cart" data-param="add" data-id="'+product_id+'" data-qty="1" data-price="'+rp+'" data-name="'+product_title+'" data-variant="'+variant+'" class="add_to_cart btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold '+add_class+'">\
													<?php echo $text['add_cart'];?> </button>';
										var btn_wishlist = '<button id="'+product_id+'" type="submit" class="add_data col-md-6 btn btn-sm btn-default c-btn-square c-btn-uppercase c-btn-bold" onClick="add_data('+"'"+'wishlist'+"'"+')" style="margin:0">\
													<i class="fa fa-heart-o"></i>Wishlist </button>';
										if(cek_stock === false){
											var btn_cart = '<button type="submit" data-url="cart" data-url2="cart" data-param="add" data-id="'+product_id+'" data-qty="1" data-price="'+rp+'" data-name="'+product_title+'" data-variant="'+variant+'" class="btn btn-sm c-btn-square c-btn-uppercase c-btn-bold '+add_class+'">\
													Sold Out </button>';
										}			
									}
									
								}
								else//remove
								{
									var btn_cart = '<button type="submit" data-url="cart" data-url2="cart" data-param="remove" data-id="'+val.rowid+'" data-page="'+product_id+'" data-price="'+rp+'" data-name="'+product_title+'"  data-variant="'+variant+'" class="add_to_cart btn btn-sm btn-default c-btn-square c-btn-uppercase c-btn-bold '+add_class+'" style="padding-bottom:6px">											<i class="fa fa-remove"></i> Cancel </button>';
									var btn_wishlist = '<div class="btn-group c-border-top" role="group">\
											<a href="#" id="'+product_id+'" class="add_data btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" onClick="add_data('+"'"+'wishlist'+"'"+')">Wishlist</a>\
										</div>';
								}
								if (discount != 0)
								{
									if (view_product == 'grid')
									{
										view_disc = '<span class="c-font-14 c-font-line-through c-font-red" id="cross-price"> IDR '+currencyformat(price_publish)+'</span>';
									}else{
										view_disc = '<span class="c-font-20 c-font-line-through c-font-red" id="cross-price"> IDR '+currencyformat(price_publish)+'</span>';
									}
									view_badge = '<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold c-label-right" id="product-sale">-'+discount+'%</div>';
								}else{
									view_disc = '';
									if(val.vstatus == 1){
										view_badge = '';
										if(variant_dsc_max != 0){
											view_badge = '<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold c-label-right" id="product-sale">-'+variant_dsc_max+'%</div>';
										}
									}else{
										view_badge = '';
									}
								}
								if(cek_stock === false){
									view_badge = '';
								}
								view_badge_new = '<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>';
								var view_grid = '<div class="col-md-4 col-sm-6 c-margin-b-20">\
							<div class="c-content-product-2 c-bg-white c-border">\
								<div class="c-content-overlay">\
									'+view_badge+'\
									'+overlay+'\
									<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: <?php if($zone == 'shisha'){
										/* if(isset($bc)){ 
											if(isset($bc['category'])){
												if($bc['category'] == 'member_card'){
													echo '180';
												}else{
													echo '380';
												};
											}else{
												echo '380';
											}; 
										}else{
											echo '380';
										}; */
										echo '380';
									}else{
										echo '250';};?>px; background-image: url('+product_img+');"></div>\
								</div>\
								<div class="c-info" style="min-height:133px">\
									<a href="'+product_url+'"><p class="c-title c-font-16 c-font-slim">'+product_title+'</p></a>\
									<p class="c-font-14 c-font-slim">IDR '+currencyformat(real_price)+'  &nbsp;\
										'+view_disc+'\
										'+view_variant+'\
										'+view_variant_prc+'\
									</p>\
								</div>\
								<div class="btn-group btn-group-justified" role="group">\
									'+btn_wishlist+'\
									<div class="btn-group c-border-left c-border-top" role="group">'+btn_cart+'</div>\
								</div>\
							</div>\
						</div>';
							var view_list = '<div class="row c-margin-b-40">\
									<div class="c-content-product-2 c-bg-white">\
										<div class="col-md-3">\
											<div class="c-content-overlay">\
												'+view_badge+'\
												'+overlay+'\
												<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 250px; background-image: url('+product_img+');"></div>\
											</div>\
										</div>\
										<div class="col-md-5">\
											<div class="c-info-list">\
												<h3 class="c-title c-font-bold c-font-22 c-font-dark">\
													<a class="c-theme-link" href="'+product_url+'">'+product_title+'</a>\
												</h3>\
												<p class="c-desc c-font-16 c-font-thin" id="product-desc" style="text-align:justify !important;">'+limitword(striptags(product_description), 40)+'</p>\
											</div>\
										</div>\
										<div class="col-md-4">\
											<p class="c-price c-font-20 c-font-thin">IDR '+currencyformat(real_price)+' &nbsp;\
												'+view_disc+'\
												'+view_variant+'\
											</p>\
												'+view_variant_prc+'\
											<div class="">'+btn_cart+'	'+btn_wishlist+'\
											</div>\
										</div>\
									</div>\
								</div>';
							
							if (view_product == 'grid')
							{
								view = view_grid;
								$('.select-grid').show();$('.select-list').hide();
							}
							else
							{
								view = view_list;
								$('.select-grid').hide();$('.select-list').show();
							}

							$(var_view).append(view);
						});	
					});	
				}else{
					$(var_view).append('<div class="c-shop-cart-page-1 c-center">'+
						'<i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i>'+
						'<h2 class="c-font-thin c-center">Not Found</h2>'+
					'</div>');
				}
				
			};
		ajax_post('json', url,data, success_fun);
	}
}

$(document).ready(function(){
	get_data_news(0);
	get_data_product(0);
	get_data_eventgal();
	$('a.upage').live('click', function(event){event.preventDefault(); get_data_news($(this).data('offset'))});
	$('#view-grid').on('click', function(){
		$('#data-product').val('grid');
		get_data_product(0);
	});
	$('#view-list').on('click', function(){
		$('#data-product').val('list');
		get_data_product(0);
	});
	$('.checkout').on('click', function(){
		var data_id = [];
		var data_qty = [];
		var redirect_url = $(this).data('redirect');
		$('.price').each(function(){
			id = $(this).attr('id');
			qty = $('#qty-'+id).val();
			if(id){
				data_id.push(id);
				data_qty.push(qty);
			}
		});
		var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			cartid : data_id,
			qty : data_qty,
			param: 'checkout',
			coupon : $('#coupon').val()
		}
		var url = $(this).data('url');
		success_fun =  
			function(json){
				if(json.s == "success"){
					setTimeout(function () {
						window.location.replace(redirect_url);
					}, 1500); //30s
				}else{
					
				}
			}
		ajax_post('json', url, data, success_fun);
	});
	$('.add_to_cart').live("click", function() {
		$('.btn').each(function(){
			$(this).attr('disabled', 'disabled');
		});
		var id = $(this).data('id');
		var obj = $(this).data('url');
		var part = $(this).data('url2');
		var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : $(this).data('id'),
			qty : $(this).data('qty'),
			price : $(this).data('price'),
			variant : $(this).data('variant'),
			name : $(this).data('name'),
			param : $(this).data('param')
		};
		var url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
		if ($(this).data('param') == 'remove')
		{
			if ($('#row-cart-'+id).length != 0)
			{
				$('#row-cart-'+id).remove();
			}

			if ($('.c-cart-table-row').length == 0 && $('#content-box-cart').length != 0)
			{
				$('#content-box-cart').empty();
				$('#content-box-cart').append('<div class="c-shop-cart-page-1 c-center">\
											   		<i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i>\
													<h2 class="c-font-thin c-center">Your Shopping Cart is Empty</h2>\
													<a href="#" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a>\
												</div>');
			}
			var total = 0;
			$('.subtotal').each(function(){
				total += Number($(this).data('subtotal'));
			});
			$('#total-bill').empty();
			$('#total-bill').append('IDR '+currencyformat(total)+'');
			if($(this).data('page')){
				$(this).replaceWith('<button type="submit" data-url="cart" data-url2="cart" data-param="add" data-id="'+$(this).data('page')+'" data-qty="1" data-price="'+$(this).data('price')+'" data-name="'+$(this).data('name')+'" data-variant="'+$(this).data('variant')+'" class="add_to_cart btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold" style="padding-bottom:6px">											<?php echo $text['add_cart'];?> </button>');
			}
		}
		$.ajax({
			url : url,
			type: "POST",
			dataType: 'json',
			data: data,
			success: function(data){
				$('.btn').each(function(){$(this).removeAttr('disabled');});
				$('#cart_number').empty();
				$('#cart_number_mobile').empty();
				if (data.total_item == '0')
				{
					$('#cartbox').hide();$('#none-item').show();$('#cart_number').hide();$('#cart_number_mobile').hide();
				}
				else
				{
					$('#cart_number').append(data.total_item);$('#cart_number_mobile').append(data.total_item);
					$('#cartbox').show();$('#none-item').hide();$('#cart_number').show();$('#cart_number_mobile').show();
				}
				if(data.total_item == '1'){
					$('#total_item').empty();$('#total_item').append($.parseJSON(data.total_item)+' item');
				}else{
					$('#total_item').empty();$('#total_item').append($.parseJSON(data.total_item)+' item(s)');
				}
				$('#total_price').empty();$('#total_price').append(data.total_price);
				$('#list_item').empty();
				$.each(data.list_item, function(key, val) {
					if(data.list_item[key].options.variant){
						variant = '<p style="margin-top: 10px;">'+data.list_item[key].options.variant+'</p>';
						variant_id = data.list_item[key].options.variant_id;
					}else{
						variant = '';
						variant_id = '';
					}
					$('#list_item').append('<li>\
						<div class="c-cart-menu-close">\
							<a href="#" data-url="cart" data-url2="cart" data-param="remove" data-id="'+key+'" class="add_to_cart c-theme-link">&#x2716;</a>\
						</div>\
						<img src="'+data.list_item[key].options.img+'" />\
						<div class="c-cart-menu-content">\
							<p>'+data.list_item[key].qty+' x<span class="c-item-price c-theme-font">'+currencyformat(data.list_item[key].price)+'</span></p>\
							<a href="'+data.list_item[key].options.url+'" class="c-item-name c-font-sbold">'+data.list_item[key].name+'</a>\
							'+variant+'\
						</div>\
						</li>');
					$('.add_to_cart[data-id="'+data.list_item[key].id+'"]').replaceWith('<button type="submit" data-url="cart" data-url2="cart" data-param="remove" data-id="'+data.list_item[key].rowid+'" data-page="'+data.list_item[key].id+'" data-price="'+data.list_item[key].price+'" data-name="'+data.list_item[key].name+'" data-variant="'+variant_id+'" class="add_to_cart btn btn-sm btn-default c-btn-square c-btn-uppercase c-btn-bold" style="padding-bottom:6px">											<i class="fa fa-remove"></i> Cancel </button>');
			    });				
			}
		});
	});
	$('.btn_post').on('click', function(){
		var obj = $(this).attr('url');
			part = $(this).attr('url2');
			file = $(".file").attr('id');
			param = $(this).data('param');
			url = "<?php echo $menu['link'];?>/"+obj+"/view_detail/"+part;
		function get_val(){
			var item_obj = {};
				$('[name="inputan"]').each(function(){
					item_obj[this.id] = this.value;
				});
			var item_array = {};
				$('[name="inputan_array"]').each(function(){
					item_array[this.id] = $(this).chosen().val();
				});
			if($(".file").val()){
				var item_summer = {};
				$('[name="inputan_summer"]').each(function(){
					item_summer[this.id] = $(this).code().replace(/"/g, "&quot;");
				});
			}else{
				var item_summer = {};
				$('[name="inputan_summer"]').each(function(){
					item_summer[this.id] = $(this).code();
				});
			}
			$.extend($.extend($.extend($.extend(item_obj, item_array), item_summer),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param});
			return item_obj;
		}
		var data = get_val();
		success_fun =  
			function(json){
				if(json.status == "success"){
					var target1 = $('.btn_post');
					var target = target1.closest('.form-group').find('#success');
					target.empty();
					target.append('<p>'+json.m+'</p>');
					scrolltonote(target1);
					target.show();
					target.fadeTo(2000, 500).slideUp(500);
					target1.each(function(){$(this).removeAttr('disabled');});
				}else{
					var target1 = $('.btn_post');
					var target = target1.closest('.form-group').find('#fail');
					target.empty();
					target.append('<p>'+json.m+'</p>');
					scrolltonote(target1);
					target.show();
					target.fadeTo(4000, 500).slideUp(500); 
					target1.each(function(){$(this).removeAttr('disabled');});
				};
			};
		ajax_post('json', url, data, success_fun);
	});
});

</script>
<?php echo $g_analytic;?>