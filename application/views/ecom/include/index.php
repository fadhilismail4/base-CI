<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-9 -->
		<section class="c-layout-revo-slider c-layout-revo-slider-2">
		<div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
			<div class="tp-banner">
				<ul>
					<!--BEGIN: SLIDE #1 -->
					<li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
						<img alt="" src="<?php echo site_url();?>assets/img/content/backgrounds/bg-60.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<!--BEGIN: MAIN TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-280" data-voffset="-100" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							Incredibly<br>
							 robust for any concept </h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-345" data-voffset="30" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 Lorem ipsum dolor sit amet, consectet<br>
								 Dolor sit amet, consectetuer nibh<br>
								 elit sed diam nonummy<br>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-445" data-voffset="140" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-btn-white c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
					</li>
					<!--END -->
					<!--BEGIN: SLIDE #2 -->
					<li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
						<img alt="" src="<?php echo site_url();?>assets/img/content/backgrounds/bg-61.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<!--BEGIN: MAIN TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-300" data-voffset="-130" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							HTML Theme Of 2015 </h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-290" data-voffset="-20" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 Lorem ipsum dolor sit amet, consectetuer et<br>
								 Dolor sit amet, consectetuer et nibh<br>
								 elit sed diam nonummy et nibh<br>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-415" data-voffset="100" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
					</li>
					<!--END -->
				</ul>
			</div>
		</div>
		</section>
		<!-- END: LAYOUT/SLIDERS/REVO-SLIDER-9 -->
		<!-- BEGIN: CONTENT/BARS/BAR-3 -->
		<div class="c-content-box c-size-md c-bg-dark">
			<div class="container">
				<div class="c-content-bar-3">
					<div class="row">
						<div class="col-md-7">
							<div class="c-content-title-1">
								<h3 class="c-font-uppercase c-font-bold">DEDICATED SUPPORT</h3>
								<p class="c-font-uppercase">
									JANGO comes with top-of-the-line support teams to ensure that we provide the best experience for our customers
								</p>
							</div>
						</div>
						<div class="col-md-3 col-md-offset-2">
							<div class="c-content-v-center" style="height: 90px;">
								<div class="c-wrapper">
									<div class="c-body">
										<button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-theme-btn c-btn-uppercase c-btn-bold">Get Support</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END: CONTENT/BARS/BAR-3 -->
		<!-- BEGIN: CONTENT/PRODUCTS/PRODUCT-1 -->
		<div class="c-content-box c-size-md c-bg-white c-no-bottom-padding">
			<div class="container">
				<div class="c-content-product-1 c-opt-1">
					<div class="c-content-title-1">
						<h3 class="c-center c-font-uppercase c-font-bold">Learn More About JANGO</h3>
						<div class="c-line-center">
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="c-media">
								<img src="<?php echo site_url();?>assets/img/content/misc/jango-intro-3.png" alt=""/>
							</div>
						</div>
						<div class="col-md-8">
							<div class="c-body">
								<ul class="c-row">
									<li>
										<h4>Code Quality</h4>
										<p>
											Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod
										</p>
									</li>
									<li>
										<h4>Angular JS Support</h4>
										<p>
											Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod
										</p>
									</li>
								</ul>
								<ul class="c-row">
									<li>
										<h4>Every Growing Unique Layouts</h4>
										<p>
											Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod
										</p>
									</li>
									<li>
										<h4>Fully Mobile Optimized</h4>
										<p>
											Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod
										</p>
									</li>
								</ul>
								<button type="button" class="btn btn-md c-btn-border-2x c-btn-square c-btn-regular c-btn-uppercase c-btn-bold c-margin-b-100">Learn More</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END: CONTENT/PRODUCTS/PRODUCT-1 -->
		<!-- BEGIN: CONTENT/BARS/BAR-2 -->
		<div class="c-content-box c-size-md c-bg-grey-1">
			<div class="container">
				<div class="c-content-bar-2 c-opt-1">
					<div class="row" data-auto-height="true">
						<div class="col-md-6">
							<!-- Begin: Title 1 component -->
							<div class="c-content-title-1" data-height="height">
								<h3 class="c-font-uppercase c-font-bold">Meet JANGO. The Theme Of 2015</h3>
								<p class="c-font-uppercase c-font-sbold">
									 The Ever growing Multipurpose Theme. Ultra responsive, Clean Coding with top quality modern design trends.
								</p>
								<button type="button" class="btn btn-md c-btn-border-2x c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Purchase</button>
							</div>
							<!-- End-->
						</div>
						<div class="col-md-6">
							<div class="c-content-v-center c-bg-red" data-height="height">
								<div class="c-wrapper">
									<div class="c-body">
										<h3 class="c-font-white c-font-bold">Just another option from an endless list of choices within JANGO.</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END: CONTENT/BARS/BAR-2 -->
		<!-- BEGIN: CONTENT/PORTFOLIO/LATEST-WORKS-2 -->
		<div class="c-content-box c-size-md c-bg-white">
			<div class="container">
				<div class="c-content-title-1">
					<h3 class="c-center c-font-uppercase c-font-bold">Latest Portfolio</h3>
					<div class="c-line-center c-theme-bg">
					</div>
					<p class="c-center c-font-uppercase">
						Showcasing your latest designs, sketches, photographs or videos.
					</p>
				</div>
				<div class="cbp-panel">
					<!-- SEE: components.js:ContentCubeLatestPortfolio -->
					<div id="grid-container" class="c-content-latest-works cbp cbp-l-grid-masonry-projects">
						<div class="cbp-item graphic">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo site_url();?>assets/img/content/stock/08-long_grey.jpg" alt="">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="c-masonry-border">
									</div>
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="ajax/projects/project1.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
											<a href="<?php echo site_url();?>assets/img/content/stock/08_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">zoom</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cbp-item web-design logos">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo site_url();?>assets/img/content/stock/07_grey.jpg" alt="">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="c-masonry-border">
									</div>
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="ajax/projects/project2.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
											<a href="<?php echo site_url();?>assets/img/content/stock/07_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="World clock widget<br>by Paul Flavius Nechita">zoom</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cbp-item graphic logos">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo site_url();?>assets/img/content/stock/09_grey.jpg" alt="">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="c-masonry-border">
									</div>
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="ajax/projects/project3.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
											<a href="http://vimeo.com/14912890" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="To-Do dashboard<br>by Tiberiu Neamu">view video</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cbp-item identity web-design">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo site_url();?>assets/img/content/stock/014_grey.jpg" alt="">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="c-masonry-border">
									</div>
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="ajax/projects/project4.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
											<a href="<?php echo site_url();?>assets/img/content/stock/014_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="WhereTO app<br>by Tiberiu Neamu">zoom</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cbp-item web-design graphic">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo site_url();?>assets/img/content/stock/34_grey.jpg" alt="">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="c-masonry-border">
									</div>
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="ajax/projects/project5.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
											<a href="<?php echo site_url();?>assets/img/content/stock/34_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Events and more<br>by Tiberiu Neamu">zoom</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cbp-item identity web-design">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo site_url();?>assets/img/content/stock/53_grey.jpg" alt="">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="c-masonry-border">
									</div>
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="ajax/projects/project6.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
											<a href="<?php echo site_url();?>assets/img/content/stock/53_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Ski * buddy<br>by Tiberiu Neamu">zoom</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cbp-item graphic logos">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo site_url();?>assets/img/content/stock/39_grey.jpg" alt="">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="c-masonry-border">
									</div>
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="ajax/projects/project7.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
											<a href="<?php echo site_url();?>assets/img/content/stock/39_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Seemple* music for ipad<br>by Tiberiu Neamu">zoom</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END: CONTENT/PORTFOLIO/LATEST-WORKS-2 -->
		<!-- BEGIN: CONTENT/BLOG/RECENT-POSTS -->
		<div class="c-content-box c-size-md c-bg-grey-1">
			<div class="container">
				<!-- Begin: Testimonals 1 component -->
				<div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
					<!-- Begin: Title 1 component -->
					<div class="c-content-title-1">
						<h3 class="c-center c-font-uppercase c-font-bold">Recent Blog Posts</h3>
						<div class="c-line-center c-theme-bg">
						</div>
					</div>
					<!-- End-->
					<!-- Begin: Owlcarousel -->
					<div class="owl-carousel owl-theme c-theme">
						<div class="item">
							<div class="c-content-blog-post-card-1 c-option-2">
								<div class="c-media c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/stock2/06.jpg" data-lightbox="fancybox" data-fancybox-group="gallery">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img class="c-overlay-object img-responsive" src="<?php echo site_url();?>assets/img/content/stock2/06.jpg" alt="">
								</div>
								<div class="c-body">
									<div class="c-title c-font-uppercase c-font-bold">
										<a href="#">Web & Mobile Development</a>
									</div>
									<div class="c-author">
										 By <a href="#"><span class="c-font-uppercase">Nick Strong</span></a>
										on <span class="c-font-uppercase">20 May 2015, 10:30AM</span>
									</div>
									<div class="c-panel">
										<ul class="c-tags c-theme-ul-bg">
											<li>
												ux
											</li>
											<li>
												web
											</li>
											<li>
												events
											</li>
										</ul>
										<div class="c-comments">
											<a href="#"><i class="icon-speech"></i> 30 comments</a>
										</div>
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor adipisicing elit. Nulla nemo ad sapiente officia amet.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-blog-post-card-1 c-option-2">
								<div class="c-media c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/stock2/01.jpg" data-lightbox="fancybox" data-fancybox-group="gallery">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img class="c-overlay-object img-responsive" src="<?php echo site_url();?>assets/img/content/stock2/01.jpg" alt="">
								</div>
								<div class="c-body">
									<div class="c-title c-font-uppercase c-font-bold">
										<a href="#">Modern Design Trends</a>
									</div>
									<div class="c-author">
										 By <a href="#"><span class="c-font-uppercase">Penny Baker</span></a>
										on <span class="c-font-uppercase">25 May 2015, 10:30AM</span>
									</div>
									<div class="c-panel">
										<ul class="c-tags c-theme-ul-bg">
											<li>
												design
											</li>
											<li>
												art
											</li>
											<li>
												trends
											</li>
										</ul>
										<div class="c-comments">
											<a href="#"><i class="icon-speech"></i> 18 comments</a>
										</div>
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor adipisicing elit. Nulla nemo ad sapiente officia amet.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-blog-post-card-1 c-option-2">
								<div class="c-media c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/stock2/03.jpg" data-lightbox="fancybox" data-fancybox-group="gallery">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img class="c-overlay-object img-responsive" src="<?php echo site_url();?>assets/img/content/stock2/03.jpg" alt="">
								</div>
								<div class="c-body">
									<div class="c-title c-font-uppercase c-font-bold">
										<a href="#">Beatifully crafted Code</a>
									</div>
									<div class="c-author">
										 By <a href="#"><span class="c-font-uppercase">Jim Raynor</span></a>
										on <span class="c-font-uppercase">26 May 2015, 10:30AM</span>
									</div>
									<div class="c-panel">
										<ul class="c-tags c-theme-ul-bg">
											<li>
												HTML
											</li>
											<li>
												CSS
											</li>
											<li>
												web
											</li>
										</ul>
										<div class="c-comments">
											<a href="#"><i class="icon-speech"></i> 34 comments</a>
										</div>
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor adipisicing elit. Nulla nemo ad sapiente officia amet.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-blog-post-card-1 c-option-2">
								<div class="c-media c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/stock2/04.jpg" data-lightbox="fancybox" data-fancybox-group="gallery">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img class="c-overlay-object img-responsive" src="<?php echo site_url();?>assets/img/content/stock2/04.jpg" alt="">
								</div>
								<div class="c-body">
									<div class="c-title c-font-uppercase c-font-bold">
										<a href="#">Optimized for all Devices</a>
									</div>
									<div class="c-author">
										 By <a href="#"><span class="c-font-uppercase">Sara Conner</span></a>
										on <span class="c-font-uppercase">29 May 2015, 10:30AM</span>
									</div>
									<div class="c-panel">
										<ul class="c-tags c-theme-ul-bg">
											<li>
												Mobile
											</li>
											<li>
												web
											</li>
											<li>
												ux
											</li>
										</ul>
										<div class="c-comments">
											<a href="#"><i class="icon-speech"></i> 25 comments</a>
										</div>
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor adipisicing elit. Nulla nemo ad sapiente officia amet.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-blog-post-card-1 c-option-2">
								<div class="c-media c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/stock2/05.jpg" data-lightbox="fancybox" data-fancybox-group="gallery">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img class="c-overlay-object img-responsive" src="<?php echo site_url();?>assets/img/content/stock2/05.jpg" alt="">
								</div>
								<div class="c-body">
									<div class="c-title c-font-uppercase c-font-bold">
										<a href="#">Compatible to all browsers</a>
									</div>
									<div class="c-author">
										 By <a href="#"><span class="c-font-uppercase">Mary Jane</span></a>
										on <span class="c-font-uppercase">30 May 2015, 10:30AM</span>
									</div>
									<div class="c-panel">
										<ul class="c-tags c-theme-ul-bg">
											<li>
												chrome
											</li>
											<li>
												firefox
											</li>
											<li>
												ie
											</li>
										</ul>
										<div class="c-comments">
											<a href="#"><i class="icon-speech"></i> 28 comments</a>
										</div>
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor adipisicing elit. Nulla nemo ad sapiente officia amet.
									</p>
								</div>
							</div>
						</div>
					</div>
					<!-- End-->
				</div>
				<!-- End-->
			</div>
		</div>
		<!-- END: CONTENT/BLOG/RECENT-POSTS -->
		<!-- BEGIN: CONTENT/MISC/ABOUT-3 -->
		<div class="c-content-box c-size-md c-bg-white">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<!-- Begin: Title 1 component -->
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold">About us</h3>
							<div class="c-line-left c-theme-bg">
							</div>
						</div>
						<!-- End-->
						<p>
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed elit diam nonummy nibh euismod tincidunt ut laoreet dolore magna aluam erat volutpat. Ut wisi enim ad minim veniam quis nostrud exerci et tation diam nisl ut aliquip ex ea commodo consequat euismod tincidunt ut laoreet dolore magna aluam.
						</p>
						<ul class="c-content-list-1 c-theme c-font-uppercase">
							<li>
								Perfect Design interface
							</li>
							<li>
								Huge Community
							</li>
							<li>
								Support for Everyone
							</li>
						</ul>
						<p>
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed elit diam nonummy nibh euismod tincidunt ut laoreet dolore.
						</p>
					</div>
					<div class="col-sm-6">
						<div class="c-content-tab-4 c-opt-3" role="tabpanel">
							<ul class="nav nav-justified" role="tablist">
								<li role="presentation" class="active">
									<a href="#tab-31" role="tab" data-toggle="tab" class="c-font-16">POPULAR</a>
								</li>
								<li role="presentation">
									<a href="#tab-32" role="tab" data-toggle="tab" class="c-font-16">COMMENTS</a>
								</li>
								<li role="presentation">
									<a href="#tab-33" role="tab" data-toggle="tab" class="c-font-16">RECENT</a>
								</li>
							</ul>
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane fade in active" id="tab-31">
									<ul class="c-tab-items">
										<li class="row">
											<div class="col-sm-4 col-xs-5">
												<div class="c-photo">
													<img class="img-responsive" width="150" height="100" src="<?php echo site_url();?>assets/img/content/stock/6.jpg" alt=""/>
												</div>
											</div>
											<div class="col-sm-8 col-xs-7">
												<h4 class="c-font-20">Lorem Ipsum</h4>
												<p class="c-font-16">
													Lorem ipsum dolor sit amet et consectetuer elit sed nonummy nib esuismod tincid unt laoreet dolore sit amet conectetuer ed dolore sit amet esmod conctetuer adipiscing lorem ipsum eit noummy tinid laoreet dolore.
												</p>
											</div>
										</li>
										<li class="row">
											<div class="col-sm-4 col-xs-5">
												<div class="c-photo">
													<img class="img-responsive" width="150" height="100" src="<?php echo site_url();?>assets/img/content/stock/5.jpg" alt=""/>
												</div>
											</div>
											<div class="col-sm-8 col-xs-7">
												<h4 class="c-font-20">Dolore Sit Amet</h4>
												<p class="c-font-16">
													Lorem ipsum dolor sit amet et consectetuer elit sed nonummy nib esuismod tincid unt laoreet dolore sit amet conectetuer ed dolore sit amet esmod conctetuer adipiscing lorem ipsum eit noummy tinid laoreet dolore.
												</p>
											</div>
										</li>
									</ul>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab-32">
									<ul class="c-tab-items">
										<li class="row">
											<div class="col-sm-4 col-xs-5">
												<div class="c-photo">
													<img class="img-responsive" width="150" height="100" src="<?php echo site_url();?>assets/img/content/stock/013.jpg" alt=""/>
												</div>
											</div>
											<div class="col-sm-8 col-xs-7">
												<h4 class="c-font-20">Lorem Ipsum</h4>
												<p class="c-font-16">
													Lorem ipsum dolor sit amet et consectetuer elit sed nonummy nib esuismod tincid unt laoreet dolore sit amet conectetuer ed dolore sit amet esmod conctetuer adipiscing lorem ipsum eit noummy tinid laoreet dolore.
												</p>
											</div>
										</li>
										<li class="row">
											<div class="col-sm-4 col-xs-5">
												<div class="c-photo">
													<img class="img-responsive" width="150" height="100" src="<?php echo site_url();?>assets/img/content/stock/18.jpg" alt=""/>
												</div>
											</div>
											<div class="col-sm-8 col-xs-7">
												<h4 class="c-font-20">Dolore Sit Amet</h4>
												<p class="c-font-16">
													Lorem ipsum dolor sit amet et consectetuer elit sed nonummy nib esuismod tincid unt laoreet dolore sit amet conectetuer ed dolore sit amet esmod conctetuer adipiscing lorem ipsum eit noummy tinid laoreet dolore.
												</p>
											</div>
										</li>
									</ul>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab-33">
									<ul class="c-tab-items">
										<li class="row">
											<div class="col-sm-4 col-xs-5">
												<div class="c-photo">
													<img class="img-responsive" width="150" height="100" src="<?php echo site_url();?>assets/img/content/stock/76.jpg" alt=""/>
												</div>
											</div>
											<div class="col-sm-8 col-xs-7">
												<h4 class="c-font-20">Lorem Ipsum</h4>
												<p class="c-font-16">
													Lorem ipsum dolor sit amet et consectetuer elit sed nonummy nib esuismod tincid unt laoreet dolore sit amet conectetuer ed dolore sit amet esmod conctetuer adipiscing lorem ipsum eit noummy tinid laoreet dolore.
												</p>
											</div>
										</li>
										<li class="row">
											<div class="col-sm-4 col-xs-5">
												<div class="c-photo">
													<img class="img-responsive" width="150" height="100" src="<?php echo site_url();?>assets/img/content/stock/75.jpg" alt=""/>
												</div>
											</div>
											<div class="col-sm-8 col-xs-7">
												<h4 class="c-font-20">Dolore Sit Amet</h4>
												<p class="c-font-16">
													Lorem ipsum dolor sit amet et consectetuer elit sed nonummy nib esuismod tincid unt laoreet dolore sit amet conectetuer ed dolore sit amet esmod conctetuer adipiscing lorem ipsum eit noummy tinid laoreet dolore.
												</p>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END: CONTENT/MISC/ABOUT-3 -->
		<!-- BEGIN: CONTENT/SLIDERS/TEAM-2 -->
		<div class="c-content-box c-size-md c-bg-grey-1">
			<div class="container">
				<!-- Begin: Testimonals 1 component -->
				<div class="c-content-person-1-slider" data-slider="owl" data-items="3">
					<!-- Begin: Title 1 component -->
					<div class="c-content-title-1">
						<h3 class="c-center c-font-uppercase c-font-bold">Meet The Team</h3>
						<div class="c-line-center c-theme-bg">
						</div>
					</div>
					<!-- End-->
					<!-- Begin: Owlcarousel -->
					<div class="owl-carousel owl-theme c-theme">
						<div class="item">
							<div class="c-content-person-1 c-option-2">
								<div class="c-caption c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/team/team10.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img src="<?php echo site_url();?>assets/img/content/team/team10.jpg" class="img-responsive c-overlay-object" alt="">
								</div>
								<div class="c-body">
									<div class="c-head">
										<div class="c-name c-font-uppercase c-font-bold">
											Randy JANGO
										</div>
										<ul class="c-socials c-theme-ul">
											<li>
												<a href="#"><i class="icon-social-twitter"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-facebook"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-dribbble"></i></a>
											</li>
										</ul>
									</div>
									<div class="c-position">
										 CEO, JANGO Inc.
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor nemo amet elit. Nulla nemo consequuntur.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-person-1 c-option-2">
								<div class="c-caption c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/team/team9.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img src="<?php echo site_url();?>assets/img/content/team/team9.jpg" class="img-responsive c-overlay-object" alt="">
								</div>
								<div class="c-body">
									<div class="c-head">
										<div class="c-name c-font-uppercase c-font-bold">
											Mary Jane
										</div>
										<ul class="c-socials c-theme-ul">
											<li>
												<a href="#"><i class="icon-social-twitter"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-facebook"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-dribbble"></i></a>
											</li>
										</ul>
									</div>
									<div class="c-position">
										 CFO, JANGO Inc.
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor nemo amet elit. Nulla nemo consequuntur.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-person-1 c-option-2">
								<div class="c-caption c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/team/team7.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img src="<?php echo site_url();?>assets/img/content/team/team7.jpg" class="img-responsive c-overlay-object" alt="">
								</div>
								<div class="c-body">
									<div class="c-head">
										<div class="c-name c-font-uppercase c-font-bold">
											Beard Mcbeardson
										</div>
										<ul class="c-socials c-theme-ul">
											<li>
												<a href="#"><i class="icon-social-twitter"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-facebook"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-dribbble"></i></a>
											</li>
										</ul>
									</div>
									<div class="c-position">
										 CTO, JANGO Inc.
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor nemo amet elit. Nulla nemo consequuntur.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-person-1 c-option-2">
								<div class="c-caption c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/team/team11.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img src="<?php echo site_url();?>assets/img/content/team/team11.jpg" class="img-responsive c-overlay-object" alt="">
								</div>
								<div class="c-body">
									<div class="c-head">
										<div class="c-name c-font-uppercase c-font-bold">
											Sara Conner
										</div>
										<ul class="c-socials c-theme-ul">
											<li>
												<a href="#"><i class="icon-social-twitter"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-facebook"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-dribbble"></i></a>
											</li>
										</ul>
									</div>
									<div class="c-position">
										 Director, JANGO Inc.
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor nemo amet elit. Nulla nemo consequuntur.
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-person-1 c-option-2">
								<div class="c-caption c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="#"><i class="icon-link"></i></a>
											<a href="<?php echo site_url();?>assets/img/content/team/team12.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
											<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img src="<?php echo site_url();?>assets/img/content/team/team12.jpg" class="img-responsive c-overlay-object" alt="">
								</div>
								<div class="c-body">
									<div class="c-head">
										<div class="c-name c-font-uppercase c-font-bold">
											Jim Book
										</div>
										<ul class="c-socials c-theme-ul">
											<li>
												<a href="#"><i class="icon-social-twitter"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-facebook"></i></a>
											</li>
											<li>
												<a href="#"><i class="icon-social-dribbble"></i></a>
											</li>
										</ul>
									</div>
									<div class="c-position">
										 Director, JANGO Inc.
									</div>
									<p>
										 Lorem ipsum dolor sit amet, dolor nemo amet elit. Nulla nemo consequuntur.
									</p>
								</div>
							</div>
						</div>
					</div>
					<!-- End-->
				</div>
				<!-- End-->
			</div>
		</div>
		<!-- END: CONTENT/SLIDERS/TEAM-2 -->
		<!-- BEGIN: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
		<div class="c-content-box c-size-md c-bg-white">
			<div class="container">
				<!-- Begin: Testimonals 1 component -->
				<div class="c-content-client-logos-slider-1" data-slider="owl" data-items="5" data-desktop-items="4" data-desktop-small-items="3" data-tablet-items="3" data-mobile-small-items="1" data-auto-play="5000">
					<!-- Begin: Title 1 component -->
					<div class="c-content-title-1">
						<h3 class="c-center c-font-uppercase c-font-bold">Our Clients</h3>
						<div class="c-line-center c-theme-bg">
						</div>
					</div>
					<!-- End-->
					<!-- Begin: Owlcarousel -->
					<div class="owl-carousel owl-theme c-theme">
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo1.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo2.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo3.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo4.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo5.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo6.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo7.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo8.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo9.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo10.jpg" alt=""/></a>
						</div>
						<div class="item">
							<a href="#"><img src="<?php echo site_url();?>assets/img/content/client-logos/logo11.jpg" alt=""/></a>
						</div>
					</div>
					<!-- End-->
				</div>
				<!-- End-->
			</div>
		</div>
		<!-- END: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->
	