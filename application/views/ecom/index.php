<!DOCTYPE html>

<html lang="en">
	<head>
		<?php $this->load->view('ecom/parts/meta')?>		
		<?php $this->load->view('ecom/parts/css-bundle')?>
	</head>   
	<?php if($style_header['navbar'] != 'none'){
		if($style_header['navbar'] == 'transparent'){
			$ccs_m_body = 'c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-fullscreen';
		}elseif($style_header['navbar'] == 'dark'){
			$ccs_m_body = 'c-layout-header-fixed c-layout-header-mobile-fixed c-page-on-scroll';
		}elseif($style_header['navbar'] == 'light'){
			$ccs_m_body = 'c-layout-header-fixed c-layout-header-mobile-fixed c-page-on-scroll';
		}elseif($style_header['navbar'] == 'light-transparent'){
			$ccs_m_body = 'c-layout-header-fixed c-layout-header-mobile-fixed c-page-on-scroll';
		}else{
			$ccs_m_body = 'c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-fullscreen';
		}
	}else{
		$ccs_m_body = '';
	}
	
	;?>
	<body class="<?php echo $ccs_m_body;?> <?php if($style_header['topbar'] != 'none'){ echo 'c-layout-header-topbar c-layout-header-topbar-collapse';};?>">  
		<?php $this->load->view('ecom/parts/header')?>
		<?php $this->load->view('ecom/parts/modal')?>	
		<div class="c-layout-page">
			<?php foreach($content as $c){;?>
				<?php $this->load->view($c)?>
			<?php };?>
		</div>	
		<?php $this->load->view('ecom/parts/footer')?>
		<?php $this->load->view('ecom/parts/js-bundle')?>
    </body>
</html>