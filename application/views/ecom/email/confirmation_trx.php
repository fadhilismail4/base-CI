<html lang="en">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>
      <?php echo $zone;?>
    </title>
<?php if(isset($style)){
	$bg_color = $style;
}else{
	$bg_color = '#74BEE5';
};?> 	
	<style type="text/css">
	a:hover { text-decoration: none !important; }
	.header h1 {color: #fff !important; font: normal 33px 'Source Sans Pro', sans-serif; margin: 0; padding: 0; line-height: 33px;}
	.header p {color: #dfa575; font: normal 20px 'Source Sans Pro', sans-serif; margin: 0; padding: 0; line-height: 11px; letter-spacing: 2px}
	.content h2 {color:<?php echo $bg_color;?> !important; font-weight: normal; margin: 0; padding: 0; font-style: normal; line-height: 35px; font-size: 20px;font-family: 'Source Sans Pro', sans-serif; }
	.content p {color:#767676; font-weight: normal; margin: 0; padding: 0; line-height: 35px; font-size: 20px;font-family: 'Source Sans Pro', sans-serif;}
	.content a {color: <?php echo $bg_color;?>; text-decoration: none;}
	.footer p {padding: 0; font-size: 20px; color:#fff; margin: 0; font-family: 'Source Sans Pro', sans-serif;}
	.footer a {color: <?php echo $bg_color;?>; text-decoration: none;}
	 h4{ font-size: 24px !important; }
	</style>
  </head>
  <body>
  	<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
		  <tr>
		  	<td align="center" style="margin: 0; padding: 0; background:<?php echo $bg_color;?>;padding: 35px 0">
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="650" style="font-family: 'Source Sans Pro', sans-serif;" class="header">
			      <tr>
			        <td bgcolor="#fff" height="115" align="center">
						<img src="<?php if(file_exists('./assets/'.$zone.'/email/logo_email.png')) {
							    echo base_url('assets').'/'.$zone.'/email/logo_email.png';
							} else {
							    echo $logo;
							};?>" width="auto" height="75px" alt="Logo">
			        </td>
			      </tr>
				  <tr>
					  <td style="font-size: 1px; height: 5px; line-height: 1px;" height="5">&nbsp;</td>
				  </tr>	
				</table>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="650" style="font-family: 'Source Sans Pro', sans-serif; background: #eaeaec;" bgcolor="#eaeaec">
			      <tr>
			        <td width="14" style="font-size: 0px;" bgcolor="#fff">&nbsp;</td>
					<td width="620" valign="top" align="left" bgcolor="#fff"style="font-family: 'Source Sans Pro', sans-serif; background: #fff;">
						<table cellpadding="0" cellspacing="0" border="0"  style="color: #717171; font: normal 11px 'Source Sans Pro', sans-serif; margin: 0; padding: 0;" width="620" class="content">
						<tr>
							<td style="padding: 25px 0;" align="center">
								<h4>Confirmation</h4>
								<p>Kode Transaksi: <span style="font-size: 20px !important; color: #1ab394;">#INV<?php echo $datas[0]->trx_code;?></span></p>
							</td>
						</tr>
						<tr>
							<td style="padding: 25px 0 0;" align="left">
								<p>Name: <?php echo $user->name;?></p>
								<p>Address: <?php echo $user->address_2;?></p>
								<p>Notes: <?php echo $datas[0]->description;?></p>
							</td>
						</tr>
						<tr>
							<td style="padding: 25px 0 0;" align="left">			
								<b style="font-weight:bold;font-size: 20px">List Products</b>
								<table style="margin-top: 15px;min-height: .01%;overflow-x: auto;max-width: 100%;margin-bottom: 20px;">
									<thead>
									<tr>
										<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">No</th>
										<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Product</th>
										<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">QTY</th>
										<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Price</th>
										<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Sub Total</th>
									</tr>
									</thead>
									<tbody>
									<?php $i=1;$total=0;foreach($datas as $ds){
										if($ds->object_id != 0){;?>
									<tr>
										<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;"><div><?php echo $i;?></div>
										</td>
										<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center"><p><?php echo $ds->title;?> <?php if($ds->varian){;?>(<?php echo $ds->varian;?>)<?php };?></p>
										<img src="<?php echo $ds->image;?>" width="75px" height="auto"></td>
										<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center"><?php echo $ds->total;?></td>
										<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center"><?php echo 'IDR '.number_format(($ds->price/$ds->total),2,',','.');?></td>
										<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center"><?php echo 'IDR '.number_format($ds->price,2,',','.');?></td>
									</tr>
									<?php $i++;$total=$total+$ds->price;};?>
									<?php };?>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding: 25px 0 0;" align="left">
								<p>Buy on <?php echo date('D, d M Y H:i:s',strtotime($datas[0]->datecreated));?></p>
								<p>Payment Method: <?php if($datas[0]->code == 'TRXON'){ echo ' Direct Bank Transfer';}else{ echo 'OFFLINE';};?></p>
								<p>Shipment: <?php echo $datas[0]->title.' (IDR '.number_format($datas[0]->price,2,',','.').')';?></p>
								<p>Total + Shipping: <?php echo 'IDR '.number_format(($datas[0]->price+$total),2,',','.');?></p>
							</td>
						</tr>
						<tr>
							<td style="padding: 25px 0 0;" align="left">			
								<p>Confirmation file on the attachment.</p>
								<a href="<?php echo $link;?>/admin" style="text-decoration:none;vertical-align:top;display:block;padding-top:10px" target="_blank"><span style="font-size:14px!important;line-height:54px;display:inline-block;color:#ffffff;border-radius:1px;text-align:center;padding:0 30px 0 30px;background-color:<?php echo $bg_color;?>">Go to Dashboard</span></a>
							</td>
						</tr>
						</table>	
					</td>
					<td width="16" bgcolor="#fff" style="font-size: 0px;font-family: 'Source Sans Pro', sans-serif; background: #fff;">&nbsp;</td>
			      </tr>
				</table><!-- body -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="650" style="font-family: 'Source Sans Pro', sans-serif; line-height: 10px;" bgcolor="#698291" class="footer">
			      <tr>
			        <td bgcolor="#fff"  align="center" style="padding: 15px 0 10px; font-size: 11px; color:#fff; margin: 0; line-height: 1.2;font-family: 'Source Sans Pro', sans-serif;" valign="top">
						<p style="padding: 0; font-size: 11px; color:<?php echo $bg_color;?>; margin: 0; font-family: 'Source Sans Pro', sans-serif;">Copyright &copy; <?php echo $zone;?> <?php echo date('Y');?></p>
					</td>
			      </tr> 
				</table><!-- footer-->
		  	</td>
		</tr>
    </table>
  </body>
</html>