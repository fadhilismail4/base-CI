<div class="loginColumns animated fadeInDown">
    <div class="row">

        <div class="col-md-6">
            <h2 class="font-bold">Welcome to KB<span style="color: #f39c12">Indonesia</span></h2>

            <p>
                Lorem ipsum dolor sit amet, ex has tantas epicurei facilisis. Augue dicit menandri ea sea, sit nemore utamur ea. Fastidii molestie quo cu, et vel dolor placerat, ne vim amet brute nostrud. Ius quod dicant admodum eu, et forensibus maiestatis has. Sea quem platonem eu, vix propriae pertinacia ad. Dicat vocent voluptaria mei ad, nihil postea scribentur pro no.
            </p>
            <p>
                Sed alii iisque ne. Utamur interpretaris id pri, cu vis idque postulant dissentiet, pri argumentum reprimique at. Ut mel case luptatum, pro cu mundi civibus suscipit. Duo ei simul legimus, denique electram te nec. Evertitur conclusionemque at pro, no his platonem maluisset. Choro semper tamquam vix ad. Nullam numquam ne sit, has ei ludus clita sententiae, postea vocibus instructior ius in.
            </p>

        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" role="form" action="<?php echo base_url()?>dashboard">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Username" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" required="">
                    </div>
                    <button href="<?php echo base_url()?>/dashboard" type="submit" class="btn btn-primary block full-width m-b">Login</button>

                    <a href="#">
                        <small>Forgot password?</small>
                    </a>

                    <p class="text-muted text-center">
                        <small>Do not have an account?</small>
                    </p>
                    <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
                </form>
                <p class="m-t">
                    <small>KBIndonesia Content Management System &copy; 2015</small>
                </p>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Copyright @ KBINDONESIA
        </div>
        <div class="col-md-6 text-right">
           <small>© 2015</small>
        </div>
    </div>
</div>
