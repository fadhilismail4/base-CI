<script src="<?php echo base_url();?>assets/admin/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/metisMenu/jquery.metisMenu.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/admin/js/inspinia.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/pace/pace.min.js" type="text/javascript"></script>

<?php if($js){admin_load_js($js, base_url());}?>
<?php if(isset($js_page)){admin_load_js($js_page, base_url());}?>
<?php if(!empty($custom_js)){ echo $custom_js;}?>
