
<link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">

<?php if ($css){admin_load_css($css, base_url());} ?>
<?php if (isset($css_page)){admin_load_css($css_page, base_url());} ?>