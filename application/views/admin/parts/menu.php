<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element"> 
					<span>
						<img alt="image" class="img-circle" src="<?php echo base_url();?><?php echo $user['profpic'];?>" style="width: 48px;"/>
					</span>
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<span class="clear"> 
							<span class="block m-t-xs"> <strong class="font-bold"><?php echo $user['nama'];?></strong>
							</span> 
							<span class="text-muted text-xs block"><?php echo $user['tipe'];?><b class="caret"></b></span> 
						</span> 
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<!--<li><a id="profile" class="menu" href="#">Profile</a></li>
						<!--<li><a href="#">Contacts</a></li>-->
						<li><a href="<?php echo $menu[0]['link'];?>/messages">Messages</a></li>
						<li class="divider"></li>
						<li><a href="<?php echo base_url();?><?php echo $logout;?>">Logout</a></li>
					</ul>
				</div>
				<div class="logo-element">
					MARS
				</div>
			</li>
			<?php if($zone == 'binamarga'){;?>
			<li class="<?php if($parent == 'dasbor'){ echo 'active';};?>">
				<a id="dasbor" class="" href="<?php echo $menu[0]['link'];?>/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dasbor</span></a>
			</li>
			<?php }else{;?>
			<li class="<?php if($parent == 'dashboard'){ echo 'active';};?>">
				<a id="dashboard" class="" href="<?php echo $menu[0]['link'];?>/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
			</li>
			<li class="hide <?php if($parent == 'frontend'){ echo 'active';};?>">
				<a id="frontend" class="" href="<?php echo $menu[0]['link'];?>/frontend"><i class="fa fa-eye"></i> <span class="nav-label">Frontend</span></a>
			</li>
			<?php };?>
			<?php if(isset($menu)){
					if(!empty($menu[0]['menu'])){
				;?>
			<?php foreach($menu as $m){
					if(isset($m['menu'])){
					if(count($m['child']) == 0){
			;?>
				<li class="<?php if($parent == $m['menu']->link){ echo 'active';};?>">
					<a href="
					<?php if(($zone_id == 1) || ($m['menu']->link == 'messages')){ ;?>
						<?php echo $m['link'];?>/<?php echo str_replace(' ','_',$m['menu']->link);?>
					<?php }else{ ;?>						
						<?php echo $m['link'];?>/<?php echo str_replace(' ','_',$m['menu']->link);?>/1
					<?php } ;?>
					" class="" id="#"><i class="fa fa-<?php if($m['menu']->icon){ echo $m['menu']->icon;};?>"></i> <span class="nav-label"><?php echo $m['menu']->name;?></span></a>
				</li>
			<?php ;} else { ?>
				<li class="<?php if($parent == $m['menu']->link){ echo 'active';};?>">
					<a href="#"><i class="fa fa-<?php if($m['menu']->icon){ echo $m['menu']->icon;};?>"></i> <span class="nav-label"><?php echo $m['menu']->name;?></span><span class="fa arrow"></span></a>
					<?php if(str_replace(' ','_',strtolower($m['menu']->name)) == 'static_page'){?>
					<ul class="nav nav-second-level">
						<?php foreach($m['child'] as $c){?>
						<li class="<?php if($child == $c->link){ echo 'active';};?>"><a href="<?php echo $m['link'];?>/static/<?php echo str_replace(' ','_',$c->link);?>" class="" id="#"><?php echo $c->name;?></a></li>
						<?php }?>
					</ul>
					<?php }else{ ?>
					<ul class="nav nav-second-level">
						<?php if(!empty($m['child'])){ foreach($m['child'] as $c){?>
						<li class="<?php if($child == $c->link){ echo 'active';};?>"><a href="<?php echo $m['link'];?>/<?php echo str_replace(' ','_',$c->link);?>/2" class="" id="#"><?php echo $c->name;?></a></li>
						<?php };}?>
					</ul>
					<?php } ?>
				</li>
			<?php ;} ;} ;}?>
			<?php };}; ?>
		</ul>

	</div>
</nav>