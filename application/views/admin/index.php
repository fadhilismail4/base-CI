<!DOCTYPE html>

<html>
    <head>        
		<?php $this->load->view('admin/parts/meta')?>		
		<?php $this->load->view('admin/parts/css-bundle')?>
	</head>   
	<body id="">        
		<div id="wrapper">
			<?php $this->load->view('admin/parts/menu')?>
			<div id="page-wrapper" class="gray-bg">
				<?php $this->load->view('admin/parts/navbar')?>
				<?php if($zone_id != 2){;?>
				<div class="row wrapper border-bottom white-bg page-heading">
					<div id="bread" class="col-lg-12">
						<h2 class="pull-left"> <?php echo $ch['title'];?> <!--<small><?php echo ' - '.$ch['small'];?></small>--></h2>
						<ol class="breadcrumb pull-right" style="margin-top: 25px;">
							<?php if($breadcrumb){foreach ($breadcrumb as $bc) :?>
							<li class="<?php if($bc['active']){ echo 'active';};?>"><a href="<?php echo base_url();?><?php echo $bc['url'];?>"><?php echo $bc['bcrumb'];?></a></li>
							<?php endforeach;} ?>
						</ol>
					</div>
					<!--<div id="button-action" class="col-lg-12 form-control btn-group" style="border:0px">
						<?php if(!empty($button)){foreach ($button as $btn) :?>
							<a href="<?php echo base_url();?><?php echo $btn['url'];?>"><button class="btn btn btn-<?php if($btn['color']){ echo $btn['color'];};?>"><?php echo $btn['name'];?></button></a>
						<?php endforeach;} ?>
					</div>-->
				</div>
				<?php };?>
				<div id="" class="wrapper wrapper-content">
					<?php if($content){$this->load->view($content);}?>
				</div>
				<div class="footer">
					<div class="pull-right">
						Copyright by <strong><?php echo $footer['provided'];?></strong>.
					</div>
					<div>
						Delivered to you by <strong style="text-transform: uppercase;"><?php echo $footer['copyright'] ;?> </strong>
					</div>
				</div>
			</div>
			<?php if($sidebar){ $this->load->view('admin/parts/sidebar'); }?>
		</div>
		<?php $this->load->view('admin/parts/js-bundle')?>
		<script type="text/javascript">
			document.addEventListener("DOMContentLoaded", function(event) {
				<?php foreach($notif as $n){;
				if($n[1]){?>
				var view = '<span class="badge badge-warning pull-right" style="display: block;left: 3px;position: absolute;top: 16px;"><?php echo $n[1];?></span>';
				$('li > a > .nav-label:contains("<?php echo $n[0];?>")').parent().append(view);
				<?php };};?>
			});
		</script>		
		<script type="text/javascript" id="crud_script">			
			function scrolltonote(selector){
				 var sel = selector
				 $('html, body').animate({
					scrollTop: $(sel).offset().top
				}, 1000);
			}
			// ajax to call view
			$('.menu').on("click", function(e) {
				e.preventDefault();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var obj = $(this).attr('id');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					lang : 1
				};
				$('.nav li').removeClass('active');
				$(this).parent('li').addClass('active');
				$(this).parent('li').parent('ul').parent('li').addClass('active');
				$('.wrapper-content').empty();
				$('#button-action').empty();
				$('.wrapper-content').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
				var url = "<?php echo $menu[0]['link'];?>/"+obj+"/update";
				$.ajax({
					url : url,
					type: "POST",
					dataType: 'html',
					data: data,
					success: function(html){
						$('.wrapper-content').empty();
						$('.wrapper-content').append(html);
						$('.btn').each(function(){$(this).removeAttr('disabled');});
						eval(document.getElementById("crud_script").innerHTML);
					},
					error: function(jqXHR){ location.reload(true); }
				});
				(function($){
					$( document ).ajaxComplete(function() {
						$('.create').each(function(){
							$(this).html('Create');
						});
						$('[name="inputan"]').each(function(){
							$(this).val('');
						});
						$('[name="inputan_array"]').each(function(){
							$(this).chosen().val('');
						});
					});
				})(jQuery);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			// end ajax to call view
			// ajax to call view inside page
			$('.select_to_select').change(function() {
				var obj = $(this).data('url'),
					part = $(this).data('url2'),
					param = $(this).data('param');
				if ($(this).find(':selected').val() != '') {
					var val = $(this).find(':selected').val(),
						item_d = $(this).find(':selected').data();
					function get_val(){
						var item_obj = {};
							for(var i in item_d){
								item_obj[i] = item_d[i];
							}
						$.extend($.extend($.extend(item_obj,{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param}),{id : val});
						return item_obj;
					}
					var data = get_val();
					$('.select_content').empty();
					var url = "<?php echo $menu[0]['link'];?>/"+obj+"/view_detail/"+part;
					$.ajax({
						url : url,
						type: "POST",
						dataType: 'html',
						data: data,
						success: function(datas){
							$('.select_content').html(datas);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					});
				}
			});
			$('.select_to_select2').change(function() {
				var obj = $(this).data('url'),
					part = $(this).data('url2'),
					param = $(this).data('param');
				if ($(this).find(':selected').val() != '') {
					var val = $(this).find(':selected').val(),
						item_d = $(this).find(':selected').data();
					function get_val(){
						var item_obj = {};
							for(var i in item_d){
								item_obj[i] = item_d[i];
							}
						$.extend($.extend($.extend(item_obj,{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param}),{id : val});
						return item_obj;
					}
					var data = get_val();
					$('.select_content2').empty();
					var url = "<?php echo $menu[0]['link'];?>/"+obj+"/view_detail/"+part;
					$.ajax({
						url : url,
						type: "POST",
						dataType: 'html',
						data: data,
						success: function(datas){
							$('.select_content2').html(datas);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					});
				}
			});
			$('.select_to_select3').change(function() {
				var obj = $(this).data('url'),
					part = $(this).data('url2'),
					param = $(this).data('param');
				if ($(this).find(':selected').val() != '') {
					var val = $(this).find(':selected').val(),
						item_d = $(this).find(':selected').data();
					function get_val(){
						var item_obj = {};
							for(var i in item_d){
								item_obj[i] = item_d[i];
							}
						$.extend($.extend($.extend(item_obj,{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param}),{id : val});
						return item_obj;
					}
					var data = get_val();
					$('.select_content3').empty();
					var url = "<?php echo $menu[0]['link'];?>/"+obj+"/view_detail/"+part;
					$.ajax({
						url : url,
						type: "POST",
						dataType: 'html',
						data: data,
						success: function(datas){
							$('.select_content3').html(datas);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					});
				}
			});
			$('.detail').on("click", function(e) {
				e.preventDefault();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var id = $(this).attr('id');
				var obj = $(this).data('url');
				var part = $(this).data('url2');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var param2 = $(this).data('param2');
				var view = $(this).data('view');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id : id,
					param : param,
					param2 : param2,
					view : view,
					language_id : lang
				};
				$('.detail_content').empty();
				$('.detail_content').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
				var url = "<?php echo $menu[0]['link'];?>/"+obj+"/view_detail/"+part;
				$.ajax({
					url : url,
					type: "POST",
					dataType: 'html',
					data: data,
					success: function(html){
						$('.detail_content').empty();
						$('.detail_content').append(html);
						view_btn_find_image = '<button id="gal" class="find_image btn btn-md btn-success col-md-12" style="margin-top: 10px;">Pick From File Manager</button>';
						$(view_btn_find_image).insertAfter('span.file-input');
						$('.btn').each(function(){$(this).removeAttr('disabled');});
						eval(document.getElementById("crud_script").innerHTML);
					}
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			$('.detail2').on("click", function(e) {
				e.preventDefault();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var id = $(this).attr('id');
				var obj = $(this).data('url');
				var part = $(this).data('url2');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var param2 = $(this).data('param2');
				var url3 = $(this).data('url3');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id : id,
					param : param,
					param2 : param2,
					url3 : url3,
					language_id : lang
				};
				$('.detail_content2').empty();
				$('.detail_content2').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
				var url = "<?php echo $menu[0]['link'];?>/"+obj+"/view_detail/"+part;
				$.ajax({
					url : url,
					type: "POST",
					dataType: 'html',
					data: data,
					success: function(html){
						$('.detail_content2').empty();
						$('.detail_content2').append(html);
						view_btn_find_image = '<button id="gal" class="find_image btn btn-md btn-success col-md-12" style="margin-top: 10px;">Pick From File Manager</button>';
						$(view_btn_find_image).insertAfter('span.file-input');
						$('.btn').each(function(){$(this).removeAttr('disabled');});
						eval(document.getElementById("crud_script").innerHTML);
					}
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			
			$('.dataTables-example').on("click", ".dtl", function(e) {
				e.preventDefault();
				$('[id*="dtl"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var obj = $(this).data('url');
				var part = $(this).data('url2');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var url3 = $(this).data('url3');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id : id,
					param : param,
					url3 : url3,
					language_id : lang
				};
				$('.detail_content2').empty();
				$('.detail_content2').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
				var url = "<?php echo $menu[0]['link'];?>/"+obj+"/view_detail/"+part;
				$.ajax({
					url : url,
					type: "POST",
					dataType: 'html',
					data: data,
					success: function(html){
						$('.detail_content2').empty();
						$('.detail_content2').append(html);
						$('.btn').each(function(){$(this).removeAttr('disabled');});
						eval(document.getElementById("crud_script").innerHTML);
					}
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			$('.detail3').on("click", function(e) {
				e.preventDefault();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var id = $(this).attr('id');
				var obj = $(this).data('url');
				var part = $(this).data('url2');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var view = $(this).data('view');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id : id,
					param : param,
					view : view,
					language_id : lang
				};
				$('.detail_content3').empty();
				$('.detail_content3').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
				var url = "<?php echo $menu[0]['link'];?>/"+obj+"/view_detail/"+part;
				$.ajax({
					url : url,
					type: "POST",
					dataType: 'html',
					data: data,
					success: function(html){
						$('.detail_content3').empty();
						$('.detail_content3').append(html);
						$('.btn').each(function(){$(this).removeAttr('disabled');});
						eval(document.getElementById("crud_script").innerHTML);
					}
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			// end ajax to call view inside page
			// ajax to view update
			$('.view_update').on("click", function(e) {
				e.preventDefault();
				var obj = $(this).attr('id');
					lang = $(this).data('lang');
					id = $(this).data('id');
				function get_val(){
					var item_obj = {};
						$('[name="inputan"]').each(function(){
							item_obj[this.id] = $(this).html();
						});
					var item_array = {};
						$('[name="inputan_array"]').each(function(){
							item_array[this.id] = $(this).chosen().html();
						});
					var item_summer = {};
						$('[name="inputan_summer"]').each(function(){
							item_summer[this.id] = $(this).html();
						});
					$.extend($.extend(item_obj, item_array), item_summer);
					return item_obj;
				}
				var data = get_val();
				$("#"+obj+"")[0].click();
				(function($){
					$( document ).ajaxComplete(function() {
						$.each(data, function(i, val) {
							$('[name="inputan"]').each(function(){
								if( $(this).attr('id') == i ){
									$('#'+i).val(val.trim());
								}
							});
							$('[name="inputan_array"]').each(function(){
								if( $(this).attr('id') == i ){
									$('#'+i).chosen().val(val.trim());
								}
							});
							$('[name="inputan_summer"]').each(function(){
								if( $(this).attr('id') == i ){
									$('#'+i).code(val.trim());
								}
							});
						});
						$('.create').html('Update');
					});
				})(jQuery);
			});
			// end ajax to view update
			// ajax to create
			$('.create').on("click", function(e) {
				e.preventDefault();
				$('#success').empty();
				$('#fail').empty();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var obj = $(this).attr('id');
					file = $(".file").attr('id');
					param = $(this).data('param');
				function get_val(){
					var item_obj = {};
						$('[name="inputan"]').each(function(){
							item_obj[this.id] = this.value;
						});
					var item_array = {};
						$('[name="inputan_array"]').each(function(){
							item_array[this.id] = $(this).chosen().val();
						});
					if($(".file").val()){
						var item_summer = {};
						$('[name="inputan_summer"]').each(function(){
							item_summer[this.id] = $(this).code().replace(/"/g, "&quot;");
						});
					}else{
						var item_summer = {};
						$('[name="inputan_summer"]').each(function(){
							item_summer[this.id] = $(this).code();
						});
					}
					$.extend($.extend($.extend($.extend(item_obj, item_array), item_summer),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param});
					return item_obj;
				}
				var data = get_val();
				if($(".file").val()){
					$.ajaxFileUpload({ 
						url : "<?php echo $menu[0]['link'];?>/"+obj+"/create",
						secureuri: false,
						fileElementId: file,
						type: "POST",
						dataType: 'JSON',
						data: data,
						success: function(data){
							JSON.parse(JSON.stringify(data), function(k, v) {
								var cek = JSON.parse(v.replace(/(<([^>]+)>)/ig,""));
								if (cek.status == "success"){
									$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#success');
									scrolltonote('.ibox-content');
									$('#success').show();
									$('#success').fadeTo(2000, 500).slideUp(500);
									$('.btn').each(function(){$(this).removeAttr('disabled');});
									setTimeout(function(){$('ol.breadcrumb li.active').prev('li').children('a').click();}, 2500);
									//location.reload(true);
								}else{
									$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#fail');
									scrolltonote('.ibox-content');
									$('#fail').show();
									$('#fail').fadeTo(4000, 500).slideUp(500); 
									$('.btn').each(function(){$(this).removeAttr('disabled');});
								} 
							});      
						}
					});
				}else{
					$.ajax({
						url : "<?php echo $menu[0]['link'];?>/"+obj+"/create",
						secureuri: false,
						fileElementId: file,
						type: "POST",
						dataType: 'json',
						data: data,
						success: function(data){
							if (data.status == "success"){
								$('<p>'+data.m+'</p>').appendTo('#success');
								scrolltonote('.ibox-content');
								$('#success').show();
								$('#success').fadeTo(2000, 500).slideUp(500);
								$('.btn').each(function(){$(this).removeAttr('disabled');});
								setTimeout(function(){$('ol.breadcrumb li.active').prev('li').children('a').click();}, 2500);
								//location.reload(true);
							}else{
								$('<p>'+data.m+'</p>').appendTo('#fail');
								scrolltonote('.ibox-content');
								$('#fail').show();
								$('#fail').fadeTo(4000, 500).slideUp(500); 
								$('.btn').each(function(){$(this).removeAttr('disabled');});
							}
						}
					});
				}
				
			});
			// end ajax to create
			// ajax to create_mdl
			$('.create_mdl').on("click", function(e) {
				e.preventDefault();
				$('#success').empty();
				$('#fail').empty();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var obj = $(this).attr('id');
					file = $(".file").attr('id');
					param = $(this).data('param');
					param2 = $(this).data('param2');
				function get_val(){
					var item_obj = {};
						$('[name="inputan"]').each(function(){
							item_obj[this.id] = this.value;
						});
					var item_array = {};
						$('[name="inputan_array"]').each(function(){
							item_array[this.id] = $(this).chosen().val();
						});
					if($(".file").val()){
						var item_summer = {};
						$('[name="inputan_summer"]').each(function(){
							item_summer[this.id] = $(this).code().replace(/"/g, "&quot;");
						});
					}else{
						var item_summer = {};
						$('[name="inputan_summer"]').each(function(){
							item_summer[this.id] = $(this).code();
						});
					}
					$.extend($.extend($.extend($.extend($.extend(item_obj, item_array), item_summer),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param}),{param2 : param2});
					return item_obj;
				}
				var data = get_val();
				if($(".file").val()){
					$.ajaxFileUpload({ 
						url : "<?php echo $menu[0]['link'];?>/"+obj+"/create_mdl",
						secureuri: false,
						fileElementId: file,
						type: "POST",
						dataType: 'JSON',
						data: data,
						success: function(data){
							JSON.parse(JSON.stringify(data), function(k, v) {
								var cek = JSON.parse(v.replace(/(<([^>]+)>)/ig,""));
								if (cek.status == "success"){
									$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#success');
									scrolltonote('.ibox-content');
									$('#success').show();
									$('#success').fadeTo(2000, 500).slideUp(500);
									$('.btn').each(function(){$(this).removeAttr('disabled');});
									setTimeout(function(){$('ol.breadcrumb li.active').prev('li').children('a').click();}, 2500);
									//location.reload(true);
								}else{
									$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#fail');
									scrolltonote('.ibox-content');
									$('#fail').show();
									$('#fail').fadeTo(4000, 500).slideUp(500); 
									$('.btn').each(function(){$(this).removeAttr('disabled');});
								} 
							});      
						}
					});
				}else{
					$.ajax({
						url : "<?php echo $menu[0]['link'];?>/"+obj+"/create_mdl",
						secureuri: false,
						fileElementId: file,
						type: "POST",
						dataType: 'json',
						data: data,
						success: function(data){
							if (data.status == "success"){
								$('<p>'+data.m+'</p>').appendTo('#success');
								scrolltonote('.ibox-content');
								$('#success').show();
								$('#success').fadeTo(2000, 500).slideUp(500);
								$('.btn').each(function(){$(this).removeAttr('disabled');});
								setTimeout(function(){$('ol.breadcrumb li.active').prev('li').children('a').click();}, 2500);
								//location.reload(true);
							}else{
								$('<p>'+data.m+'</p>').appendTo('#fail');
								scrolltonote('.ibox-content');
								$('#fail').show();
								$('#fail').fadeTo(4000, 500).slideUp(500); 
								$('.btn').each(function(){$(this).removeAttr('disabled');});
							}
						}
					});
				}
				
			});
			// end ajax to create_mdl
			function sendFile(file, url, editor) {
				var data = new FormData();
				data.append("file", file);
				data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo trim($this->security->get_csrf_hash()); ?>');
				$.ajax({
					data: data,
					type: "POST",
					url: '<?php echo $menu[0]['link'];?>/upload_summernote',
					cache: false,
					contentType: false,
					processData: false,
					success: function(data) {
						$('.summernote').summernote("insertImage", data, 'filename');
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
					}
				});
			};
			// ajax to view status
			$('.modal_status').on("click", function() {
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var status = $(this).data('status');
				var text = $(this).data('title');
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-sm">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">'+status+'</h5>                                        </div>                                        <div class="modal-body">    <div class="alert alert-danger" id="status_fail" style="display:none;"></div>	<div class="alert alert-info" id="status_success" style="display:none;"></div>                                        <h4>Are you sure want to <strong style="text-transform:uppercase">'+status+'</strong></h4> <strong style="text-transform:uppercase"><h3><span style="color:#F39C12">'+text+'</span><span>  ?</span></h3></strong>                                         </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="'+url+'" data-id="'+id+'" data-lang="'+lang+'" data-param="'+param+'" data-status="'+status.toLowerCase().replace(/ /g,"_")+'" type="button" class="btn btn-info status">'+status+'</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				eval(document.getElementById("crud_script").innerHTML);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			
			$('.modal_stas').on("click", function() {
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var url2 = $(this).data('url3');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var status = $(this).data('status');
				var text = $(this).data('title');
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-sm">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">'+status+'</h5>                                        </div>                                        <div class="modal-body">    <div class="alert alert-danger" id="status_fail" style="display:none;"></div>	<div class="alert alert-info" id="status_success" style="display:none;"></div>                                        <h4>Are you sure want to <strong style="text-transform:uppercase">'+status+'</strong></h4> <strong style="text-transform:uppercase"><h3><span style="color:#F39C12">'+text+'</span><span>  ?</span></h3></strong>                                         </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="'+url+'" data-url="'+url2+'" data-id="'+id+'" data-lang="'+lang+'" data-param="'+param+'" data-status="'+status.toLowerCase().replace(/ /g,"_")+'" type="button" class="btn btn-info mod_status">'+status+'</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				eval(document.getElementById("crud_script").innerHTML);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});

			$('.dataTables-example').on("click", ".modal_sts", function() {
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var status = $(this).data('status');
				var text = $(this).data('title');
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-sm">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">'+status+'</h5>                                        </div>                                        <div class="modal-body">    <div class="alert alert-danger" id="status_fail" style="display:none;"></div>	<div class="alert alert-info" id="status_success" style="display:none;"></div>                                        <h4>Are you sure want to <strong style="text-transform:uppercase">'+status+'</strong></h4> <strong style="text-transform:uppercase"><h3><span style="color:#F39C12">'+text+'</span><span>  ?</span></h3></strong>                                         </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="'+url+'" data-id="'+id+'" data-lang="'+lang+'" data-param="'+param+'" data-status="'+status.toLowerCase().replace(/ /g,"_")+'" type="button" class="btn btn-info status">'+status+'</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				eval(document.getElementById("crud_script").innerHTML);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			
			$('.dataTables-example').on("click", ".modal_stat", function() {
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var url2 = $(this).data('url3');
				var lang = $(this).data('lang');
				var param = $(this).data('param');
				var status = $(this).data('status');
				var text = $(this).data('title');
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-sm">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">'+status+'</h5>                                        </div>                                        <div class="modal-body">    <div class="alert alert-danger" id="status_fail" style="display:none;"></div>	<div class="alert alert-info" id="status_success" style="display:none;"></div>                                        <h4>Are you sure want to <strong style="text-transform:uppercase">'+status+'</strong></h4> <strong style="text-transform:uppercase"><h3><span style="color:#F39C12">'+text+'</span><span>  ?</span></h3></strong>                                         </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="'+url+'" data-url="'+url2+'" data-id="'+id+'" data-lang="'+lang+'" data-param="'+param+'" data-status="'+status.toLowerCase().replace(/ /g,"_")+'" type="button" class="btn btn-info mod_status">'+status+'</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				eval(document.getElementById("crud_script").innerHTML);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			
			// end ajax to view status
			
			// ajax to status
			$('.status').on("click", function(e) {
				e.preventDefault();
				$('#status_success').empty();
				$('#status_fail').empty();
				var obj = $(this).attr('id');
					lang = $(this).data('lang');
					id = $(this).data('id');
					param = $(this).data('param');
					status = $(this).data('status');
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id: id, 
					status: status,
					param: param,
					lang: lang
				};
				$.ajax({
					url : "<?php echo $menu[0]['link'];?>/"+obj+"/status",
					type: 'POST',
					dataType: 'json',
					data : data,
					success : function(data){
						if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#status_success');
							$('#status_success').show();
							$('#status_success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							$('[id*="modal_"]').each(function(){
								$(this).remove();
							});
							$('.modal-backdrop').remove();
							//location.reload(true);
							$('body').removeClass('modal-open');
							$('body').css('padding-right',0);
							$('ol.breadcrumb li.active > a').click();
							eval(document.getElementById("crud_script").innerHTML);
							
						}else{
							$('<p>'+data.m+'</p>').appendTo('#status_fail');
							$('#status_fail').show();
							$('#status_fail').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					},
					/* error: function(jqXHR){ location.reload(true); } */
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
				//$('#fail4').empty();
			});
			
			$('.mod_status').on("click", function(e) {
				e.preventDefault();
				$('#status_success').empty();
				$('#status_fail').empty();
				var obj = $(this).attr('id');
					lang = $(this).data('lang');
					id = $(this).data('id');
					url = $(this).data('url');
					param = $(this).data('param');
					status = $(this).data('status');
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id: id, 
					url: url,
					status: status,
					param: param,
					lang: lang
				};
				$.ajax({
					url : "<?php echo $menu[0]['link'];?>/"+obj+"/zone_status",
					type: 'POST',
					dataType: 'json',
					data : data,
					success : function(data){
						if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#status_success');
							$('#status_success').show();
							$('#status_success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							$('[id*="modal_"]').each(function(){
								$(this).remove();
							});
							$('.modal-backdrop').remove();
							//location.reload(true);
							$('body').removeClass('modal-open');
							$('body').css('padding-right',0);
							$('ol.breadcrumb li.active > a').click();
							eval(document.getElementById("crud_script").innerHTML);
							
						}else{
							$('<p>'+data.m+'</p>').appendTo('#status_fail');
							$('#status_fail').show();
							$('#status_fail').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					},
					/* error: function(jqXHR){ location.reload(true); } */
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
				//$('#fail4').empty();
			});
			
			// end ajax to status
			// ajax to view delete
			$('.modal_delete').on("click", function(e) {
				e.preventDefault();
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var lang = $(this).data('lang');
				var text = $(this).data('title');
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-sm">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title">Delete</h5>                                        </div>                                        <div class="modal-body">    <div class="alert alert-danger" id="delete_fail" style="display:none;"></div>	<div class="alert alert-info" id="delete_success" style="display:none;"></div>                                        <p>'+text+'</p><br><p><strong>Are You Sure Want to Delete This?</strong></p>                                        </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="'+url+'" data-id="'+id+'" data-lang="'+lang+'" type="button" class="btn btn-danger delete">Delete</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				eval(document.getElementById("crud_script").innerHTML);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			// end ajax to view delete
			
			$('.dataTables-example').on("click", ".mod_delete", function() {
				e.preventDefault();
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var part = $(this).data('url2');				
				var param = $(this).data('param');
				var lang = $(this).data('lang');
				var text = $(this).data('title');
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-sm">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title">Delete</h5>                                        </div>                                        <div class="modal-body">    <div class="alert alert-danger" id="delete_fail" style="display:none;"></div>	<div class="alert alert-info" id="delete_success" style="display:none;"></div>                                        <p>'+text+'</p><br><p><strong>Are You Sure Want to Delete This?</strong></p>                                        </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="'+url+'" data-id="'+id+'" data-url="'+part+'" data-param="'+param+'" data-lang="'+lang+'" type="button" class="btn btn-danger dlt">Delete</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				eval(document.getElementById("crud_script").innerHTML);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			
			
			// ajax to delete
			$('.delete').on("click", function(e) {
				e.preventDefault();
				$('#delete_success').empty();
				$('#delete_fail').empty();
				var obj = $(this).attr('id');
					lang = $(this).data('lang');
					id = $(this).data('id');
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id: id, 
					lang: lang
				};
				$.ajax({
					url : "<?php echo $menu[0]['link'];?>/"+obj+"/delete",
					type: 'POST',
					dataType: 'json',
					data : data,
					success : function(data){
						if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#delete_success');
							$('#delete_success').show();
							$('#delete_success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							$('[id*="modal_"]').each(function(){
								$(this).remove();
							});
							$('.modal-backdrop').remove();
							//window.location.href = "<?php echo $menu[0]['link'];?>"+"/"+obj+"";
							setTimeout(function(){
								$('body').removeClass('modal-open');
								$('body').css('padding-right',0);
								$('ol.breadcrumb li.active').prev('li').children('a').click();
							}, 2500);
							eval(document.getElementById("crud_script").innerHTML);
						}else{
							$('<p>'+data.m+'</p>').appendTo('#delete_fail');
							$('#delete_fail').show();
							$('#delete_fail').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					},
					/* error: function(jqXHR){ location.reload(true); } */
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
				//$('#fail4').empty();
			});
			
			$('.modal_dlt').on("click", function(e) {
				e.preventDefault();
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var part = $(this).data('url2');				
				var param = $(this).data('param');
				var lang = $(this).data('lang');
				var text = $(this).data('title');
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-sm">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title">Delete</h5>                                        </div>                                        <div class="modal-body">    <div class="alert alert-danger" id="delete_fail" style="display:none;"></div>	<div class="alert alert-info" id="delete_success" style="display:none;"></div>                                        <p>'+text+'</p><br><p><strong>Are You Sure Want to Delete This?</strong></p>                                        </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="'+url+'" data-id="'+id+'" data-url="'+part+'" data-param="'+param+'" data-lang="'+lang+'" type="button" class="btn btn-danger dlt">Delete</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				eval(document.getElementById("crud_script").innerHTML);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			// end ajax to view delete
			// ajax to delete
			$('.dlt').on("click", function(e) {
				e.preventDefault();
				$('#delete_success').empty();
				$('#delete_fail').empty();
				var obj = $(this).attr('id');
					lang = $(this).data('lang');
					id = $(this).data('id');
					part = $(this).data('url');
					url3 = $(this).data('url3');
					param = $(this).data('param');
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id: id, 
					lang: lang, 
					part: part, 
					url3: url3, 
					param: param
				};
				$.ajax({
					url : "<?php echo $menu[0]['link'];?>/"+obj+"/zone_delete",
					type: 'POST',
					dataType: 'json',
					data : data,
					success : function(data){
						if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#delete_success');
							$('#delete_success').show();
							$('#delete_success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							$('[id*="modal_"]').each(function(){
								$(this).remove();
							});
							$('.modal-backdrop').remove();
							//window.location.href = "<?php echo $menu[0]['link'];?>"+"/"+part+"/1";
							setTimeout(function(){
								$('body').removeClass('modal-open');
								$('body').css('padding-right',0);
								$('ol.breadcrumb li.active').prev('li').children('a').click();
							}, 2500);
							eval(document.getElementById("crud_script").innerHTML);
						}else{
							$('<p>'+data.m+'</p>').appendTo('#delete_fail');
							$('#delete_fail').show();
							$('#delete_fail').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					},
					/* error: function(jqXHR){ location.reload(true); } */
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
				//$('#fail4').empty();
			});
			
			// end ajax to delete
			// ajax to view maps
			$('.modal_maps').on("click", function(e) {
				e.preventDefault();
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				$('[class*="maps_cek"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var param = $(this).data('param');
				var param2 = $(this).data('param2');
				$.getScript( "http://maps.googleapis.com/maps/api/js" ).done(function( script, textStatus ) {
					var $latitude = document.getElementById(param);
					var $longitude = document.getElementById(param2);
					var $isi = document.getElementById('maps_target');
					var myLatLng = {lat: -6.920867, lng: 107.609558};
					var mapOptions1 = {
						zoom: 11,
						center: myLatLng,
						// Style for Google Maps
						styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
					};
					
					var contentString = '<div id="content">'+
					  '</div>';

					var infowindow = new google.maps.InfoWindow({
						content: contentString
					});
					var mapElement1 = document.getElementById('map1');
					
					var map1 = new google.maps.Map(mapElement1, mapOptions1);
					
					var marker = new google.maps.Marker({
						position: myLatLng,
						map: map1,
						title: 'Hello World!',
						draggable: true
					  });
					marker.setMap(map1);
					marker.addListener('click', function() {
						infowindow.open(map1, marker);
						this.setOptions({scrollwheel: true});
					});
					google.maps.event.addListener(marker, 'dragend', function(marker){
					  var latLng = marker.latLng;
					  $latitude.value = latLng.lat();
					  $longitude.value = latLng.lng();
					  $isi.dataset.val = latLng.lat();
					  $isi.dataset.val2 = latLng.lng();
					});
				});
				$(this).parent().append('<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">                              <div class="modal-lg modal-dialog" style="    min-width: 80%;">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title">Set Location</h5>                                        </div>                                        <div class="modal-body">             <div id="map1" class="google-map" style="width:100%; height:350px;"></div><div class="col-md-12">latitude: <input type="text" id="'+param+'" placeholder="'+param+'" style="    border: none;"> longitude: <input type="text" id="'+param2+'" placeholder="'+param2+'" style="    border: none;"></div>                                        </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                            <button id="maps_target" data-id="'+id+'" data-param="'+param+'" data-val="" data-param2="'+param2+'" data-val2="" type="button" data-dismiss="modal" class="btn btn-info maps">Set Location</button>                                        </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
					eval(document.getElementById("maps_script").innerHTML);
				 function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('maps_script'); 
			});
			// end ajax to view maps
			$('.modal_upload_data').on("click", function(e) {
				e.preventDefault();
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var url2 = $(this).data('url2');
				$(this).parent().parent().parent().parent().append('<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">                              <div class="modal-lg modal-dialog">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title">Upload Image</h5>                                        </div>                                        <div class="modal-body">   <form id="my-awesome-dropzone" class="dropzone dz-clickable" action="#">                            <div class="dropzone-previews"></div>                     <div style="bottom: 0;position: absolute;right: 0;"><button type="button" class="btn btn-white" data-dismiss="modal">Close</button><button id="" data-id="'+id+'" data-param="'+url+'" data-val="" data-param2="'+url2+'" data-val2="" type="submit" class="btn btn-info">Upload</button></div>                     </form><div>                        </div></div>                                        <div class="modal-footer">                                                                                                                                </div>                                    </div>                               </div>                            </div>'); 
				$('<link/>', {
				   rel: 'stylesheet',
				   type: 'text/css',
				   href: '<?php echo base_url('assets');?>/admin/css/plugins/dropzone/basic.css'
				}).appendTo('head');
				$('<link/>', {
				   rel: 'stylesheet',
				   type: 'text/css',
				   href: '<?php echo base_url('assets');?>/admin/css/plugins/dropzone/dropzone.css'
				}).appendTo('head');
				$.getScript( "<?php echo base_url('assets');?>/admin/js/plugins/dropzone/dropzone.js" ).done(function(){
					 Dropzone.options.myAwesomeDropzone = {

						autoProcessQueue: false,
						uploadMultiple: true,
						parallelUploads: 10,
						maxFiles: 10,
						addRemoveLinks: true,

						// Dropzone settings
						init: function() {
							var myDropzone = this;
							this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
								e.preventDefault();
								e.stopPropagation();
								myDropzone.processQueue();
							});
							this.on("addedfile", function(file) {
							  file.previewElement.children[5].addEventListener("click", function() {
								input_title = file.previewElement.children[6];
								input_desc = file.previewElement.children[7];
								if($(input_title).is(':visible') && $(input_desc).is(':visible')) {
									$(input_title).hide();
									$(input_desc).hide();
								}
								else
								{
									$(input_title).show();
									$(input_desc).show();
								}
							  });
							});
							
							this.on("sendingmultiple", function(data, xhr, formData) {
								 formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo trim($this->security->get_csrf_hash()); ?>');
							});
							this.on("successmultiple", function(files, response) {
							});
							this.on("errormultiple", function(files, response) {
							});
						}

					}
					var myDropzone = new Dropzone("#my-awesome-dropzone", { url: "<?php echo $menu[0]['link'];?>/upload_gallery/"+id+"/"+url+"/"+url2, previewTemplate:'<div class="dz-preview dz-file-preview">  <div class="dz-details">    <div class="dz-filename"><span data-dz-name></span></div>    <div class="dz-size" data-dz-size></div>    <img data-dz-thumbnail />  </div>  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>  <div class="dz-success-mark"><span>?</span></div>  <div class="dz-error-mark"><span>?</span></div>  <div class="dz-error-message"><span data-dz-errormessage></span></div> <button type="button" id="addval" class="btn btn-info btn-xs" style="margin-top: -20px;position: absolute;right: 5px;"><i class="fa fa-pencil"></i></button><div id="input-title" style="padding-top: 5px !important; display:none;"><input type="text" id="title" name="title[]" class="form-control" value="" placeholder="Title" /></div><div id="input-desc" style="padding-top: 5px !important; display:none;"><input type="text" id="description" name="desc[]" class="form-control" value="" placeholder="Description" /></div></div>'});
				});
				$('#modal_'+id+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
					eval(document.getElementById("crud_script").innerHTML);
				 function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script'); 
			});
			function urlExists(testUrl) {
			 var http = jQuery.ajax({
				type:"HEAD",
				url: testUrl,
				async: false
			  })
			  return http.status;
				  // this will return 200 on success, and 0 or negative value on error, and 500 if error
			}
			$('.find_image').on("click", function(e) {
				e.preventDefault();
				$('[id*="modal_"]').each(function(){
					$(this).remove();
				});
				var id = $(this).attr('id');
				var url = $(this).data('url');
				var url2 = $(this).data('url2');
				$(this).parent().parent().parent().parent().append('<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">                              <div class="modal-lg modal-dialog">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title">File Manager</h5>                                        </div>                                        <div class="modal-body"><div>                        </div></div>                                        <div class="modal-footer">                                                                                                                                </div>                                    </div>                               </div>                            </div>'); 
				var obj = 'frontend/view_detail/gallery';
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id: 'o', 
					module:'',
					category:''
				};
				setTimeout(function(){
					$.ajax({
						url : "<?php echo $menu[0]['link'];?>/"+obj,
						type: 'POST',
						dataType: 'json',
						data : data,
						success : function(json){
							if (json.data.length === 0) {
								console.log('empty');
							}else{
								if(json.data){
									view = [];
									show = [];
									pagination = [];
									type = '';
									$.each(json.data, function(key, val) {
										if(val.gallery_id){
											type = 'gallery';
											img = '<?php echo base_url('assets').'/'.$zone;?>'+'/gallery/thumbnail/'+val.image_square;
											if(urlExists(img) === 500){
												new_img = val.image_square.split('.');
												img = '<?php echo base_url('assets').'/'.$zone;?>'+'/gallery/thumbnail/'+new_img[0]+'_thumb'+'.'+new_img[1];
												if(urlExists(img) === 500){
													img = '';
												}
											}
										}else{
											type = 'object';
											img = '<?php echo base_url('assets').'/'.$zone;?>'+'/'+val.image_square;
											if(urlExists(img) === 500){
												img = '';
											}
										}
										
										if(img.length){
											view[key] = '<a class="cek_image" data-title="'+val.image_square+'" data-type="'+val.gallery_id+'"><img class="col-md-3" src="'+img+'"></a>';
										}
									});
									
									if(json.numpage >= 5){
										if(json.ap >= 3){
											ap = parseInt(json.ap);
											numpage = parseInt(json.numpage);
											from = ap - 1;
											till = ap + 3;
											n_next = till;
											next = '<button type="button" data-offset='+n_next+' class="find_image_btn btn btn-white"><i class="fa fa-chevron-right"></i> </button>';
											
											if((ap + 3) >= numpage){
												from = numpage - 4;
												till = numpage;
												next = '';
											}
											n_prev = from - 2;
											prev = '<button type="button" data-offset='+n_prev+' class="find_image_btn btn btn-white"><i class="fa fa-chevron-left"></i></button>';
										}else{
											from = 1;
											till = 5;
											prev = '';
											n_next = till;
											next = '<button type="button" data-offset='+n_next+' class="find_image_btn btn btn-white"><i class="fa fa-chevron-right"></i> </button>';
										}
										for( var i = from; i <= till; i++ ) {
											offset = i-1;
											if(json.ap == offset){
												pagination[i] = '<button class="find_image_btn btn btn-white active" data-offset='+offset+'>'+i+'</button>';
											}else{
												pagination[i] = '<button class="find_image_btn btn btn-white" data-offset='+offset+'>'+i+'</button>';
											}
										}
									}else{
										for( var i = 1; i <= json.numpage; i++ ) {
											offset = i-1;
											if(json.ap == offset){
												pagination[i] = '<button class="find_image_btn btn btn-white active" data-offset='+offset+'>'+i+'</button>';
											}else{
												pagination[i] = '<button class="find_image_btn btn btn-white" data-offset='+offset+'>'+i+'</button>';
											}
										}
										prev = '';
										next = '';
									}
									show_val = json.data.length;
									o_till = show_val*json.numpage;
									for( var i = 8; i <= o_till; i+=8 ) {
										if(i <= 40){
											if(i == show_val){
												show[i] = '<option value="'+i+'">'+i+'</option>';
											}else{
												show[i] = '<option value="'+i+'">'+i+'</option>';
											}
										}
									};
									v_show = '<div class="col-md-12"><div class="pull-right col-md-2" style="padding-right:0"><select class="form-control input-md c-square" id="limit_image">'+show+'</select></div><div class="pull-right col-md-1"><p style="margin: 7px 0 0 10px;">Show:</p></div></div>';
									v_pagination = '<div class="col-md-12"><div class="pull-right btn-group">'+prev+pagination+next+'</div></div>';
									shell = '<div id="all_image" class="row">'+v_show+view+v_pagination+'</div>';
									$('#modal_gal').find('.modal-body').append(shell.replace(/,/g, ""));
									a = $('#all_image > a');
									for( var i = 0; i < a.length; i+=4 ) {
										a.slice(i, i+4).wrapAll('<div class="col-md-12" style="margin:10px 0;padding:0"></div>');
									}
								}
							}
							eval(document.getElementById("file_script").innerHTML);
						}
					});
				}, 500);
				$('#modal_'+id+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('file_script'); 
			});
			/* $('.collapse-link').on("click", function(e) {
				var ibox = $(this).closest('div.ibox');
				var button = $(this).find('i');
				var content = ibox.find('div.ibox-content');
				if(button.hasClass('fa-chevron-up')){
					content.slideUp(200);
					button.removeClass('fa-chevron-up').addClass('fa-chevron-down');
				}else if(button.hasClass('fa-chevron-down')){
					content.slideDown(200);
					button.addClass('fa-chevron-up').removeClass('fa-chevron-down');
				}				
				ibox.toggleClass('').toggleClass('border-bottom');
				setTimeout(function () {
					ibox.resize();
					ibox.find('[id^=map-]').resize();
				}, 50);
				e.stopPropagation(); e.preventDefault();
			}); */
		</script>
		<script id="file_script">
			$('.find_image_btn').on("click", function(e) {
				var obj = 'frontend/view_detail/gallery';
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					offset: $(this).attr('data-offset'), 
					limit: $('#limit_image').find(':selected').val(), 
					id: 'o', 
					module:'',
					category:''
				};
				$('#modal_gal').find('.modal-body').empty();
				$.ajax({
					url : "<?php echo $menu[0]['link'];?>/"+obj,
					type: 'POST',
					dataType: 'json',
					data : data,
					success : function(json){
						if (json.data.length === 0) {
							console.log('empty');
						}else{
							if(json.data){
								view = [];
								show = [];
								pagination = [];
								type = '';
								$.each(json.data, function(key, val) {
									if(val.gallery_id){
										type = 'gallery';
										img = '<?php echo base_url('assets').'/'.$zone;?>'+'/gallery/thumbnail/'+val.image_square;
										if(urlExists(img) === 500){
											new_img = val.image_square.split('.');
											img = '<?php echo base_url('assets').'/'.$zone;?>'+'/gallery/thumbnail/'+new_img[0]+'_thumb'+'.'+new_img[1];
											if(urlExists(img) === 500){
												img = '';
											}
										}
									}else{
										type = 'object';
										img = '<?php echo base_url('assets').'/'.$zone;?>'+'/'+val.image_square;
										if(urlExists(img) === 500){
											img = '';
										}
									}
									
									if(img.length){
										view[key] = '<a class="cek_image" data-title="'+val.image_square+'" data-type="'+val.gallery_id+'"><img class="col-md-3" src="'+img+'"></a>';
									}
								});
								
								if(json.numpage >= 5){
									if(json.ap >= 3){
										ap = parseInt(json.ap);
										numpage = parseInt(json.numpage);
										from = ap - 1;
										till = ap + 3;
										n_next = till;
										next = '<button type="button" data-offset='+n_next+' class="find_image_btn btn btn-white"><i class="fa fa-chevron-right"></i> </button>';
										
										if((ap + 3) >= numpage){
											from = numpage - 4;
											till = numpage;
											next = '';
										}
										n_prev = from - 2;
										prev = '<button type="button" data-offset='+n_prev+' class="find_image_btn btn btn-white"><i class="fa fa-chevron-left"></i></button>';
									}else{
										from = 1;
										till = 5;
										prev = '';
										n_next = till;
										next = '<button type="button" data-offset='+n_next+' class="find_image_btn btn btn-white"><i class="fa fa-chevron-right"></i> </button>';
									}
									for( var i = from; i <= till; i++ ) {
										offset = i-1;
										if(json.ap == offset){
											pagination[i] = '<button class="find_image_btn btn btn-white active" data-offset='+offset+'>'+i+'</button>';
										}else{
											pagination[i] = '<button class="find_image_btn btn btn-white" data-offset='+offset+'>'+i+'</button>';
										}
									}
								}else{
									for( var i = 1; i <= json.numpage; i++ ) {
										offset = i-1;
										if(json.ap == offset){
											pagination[i] = '<button class="find_image_btn btn btn-white active" data-offset='+offset+'>'+i+'</button>';
										}else{
											pagination[i] = '<button class="find_image_btn btn btn-white" data-offset='+offset+'>'+i+'</button>';
										}
									}
									prev = '';
									next = '';
								}
								show_val = json.data.length;
								o_till = show_val*json.numpage;
								for( var i = 8; i <= o_till; i+=8 ) {
									if(i <= 40){
										if(i == show_val){
											show[i] = '<option value="'+i+'" selected>'+i+'</option>';
										}else{
											show[i] = '<option value="'+i+'">'+i+'</option>';
										}
									}
								};
								v_show = '<div class="col-md-12"><div class="pull-right col-md-2" style="padding-right:0"><select class="form-control input-md c-square" id="limit_image">'+show+'</select></div><div class="pull-right col-md-1"><p style="margin: 7px 0 0 10px;">Show:</p></div></div>';
								v_pagination = '<div class="col-md-12"><div class="pull-right btn-group">'+prev+pagination+next+'</div></div>';
								shell = '<div id="all_image" class="row">'+v_show+view+v_pagination+'</div>';
								$('#modal_gal').find('.modal-body').append(shell.replace(/,/g, ""));
								a = $('#all_image > a');
								for( var i = 0; i < a.length; i+=4 ) {
									a.slice(i, i+4).wrapAll('<div class="col-md-12" style="margin:10px 0;padding:0"></div>');
								}
							}
						}
						eval(document.getElementById("file_script").innerHTML);
					}
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('file_script'); 
			});
			$('#all_image > div > .cek_image').on("click", function(e) {
				e.preventDefault;
				target = $(this);
				target.find('.active').remove();
				parent = target.closest('.col-md-12');
				text = 'are you sure want to use this image?';
				title = target.data('title');
				img = this.getElementsByTagName('img')[0];
				w = img.naturalWidth;   // Note: $(this).width() will not
				h = img.naturalHeight; // work for in memory images.
				img_w = '<p>width: '+w+'</p>';
				img_h = '<p>height: '+h+'</p>';
				cek = title.split('/');
				module = '';
				if(cek.length > 1){
					url = title;
					module = '<p>From Module: '+cek[0]+'</p>';
					title = cek[1];
					type = cek[0];
				}else{
					url = title;
					module = '<p>From Gallery</p>';
					type = 'gallery';
				}
				button = '<button id="'+title+'" data-type="'+type+'" type="button" class="pull-right btn btn-outline btn-primary">Use This</button>';
				parent.children('.cek_image').each(function(){
					$(this).slideUp();
				});
				if(target.hasClass('col-md-12')){
					target.slideUp();
					target.find('.active').remove();
					$('.cek_image').each(function(){
						$(this).slideDown();
						$(this).attr('style','display:block');
						$(this).removeClass('col-md-12');
					});
				}else{
					target.slideDown();
					target.attr('style','display:block;padding:0;color: black; cursor: auto;');
					target.addClass('col-md-12');
					view = '<div class="active col-md-9"><a class="pull-right" aria-hidden="true" style="font-size: 20px;">×</a><h3>'+title+'</h3>'+module+''+img_w+''+img_h+''+button+'</div>';
					target.append(view);
					
					$('.cek_image > .active > button').on("click", function(e) {
						if(type == 'gallery'){
							url = '<?php echo base_url('assets').'/'.$zone;?>'+'/gallery/'+url;
						}else{
							url = '<?php echo base_url('assets').'/'.$zone;?>'+'/'+url;
						}
						v_input = 	'<input id="image_square2" type="hidden" name="inputan" value="'+title+'">'+
									'<input id="image_from" type="hidden" name="inputan" value="'+type+'">';
						view_after = '<div class="after_select col-md-12"><img class="img-responsive col-md-11" width="150px" height="auto" src="'+url+'" style="padding:0"/>'+v_input+'<a aria-hidden="true" style="font-size: 20px;">x</a></div>';
						$(view_after).insertAfter('.find_image');
						$('.find_image').hide();
						$('.file-input').hide();
						$('[id*="modal_"]').each(function(){
							$(this).remove();
						});
						$('.modal-backdrop').remove();
						setTimeout(function(){
							$('body').removeClass('modal-open');
							$('body').css('padding-right',0);
						}, 500);
						
						$('.after_select').parent().delegate(".after_select > a",'click',function(e){
							$(this).parent().remove();
							$('.find_image').show();
							$('.file-input').show();
						});
					});
				}
			});
		</script>
		<script id="maps_script">
			// ajax to get location
			$('.maps').on("click", function(e) {
				var	id = $(this).data('id'),
					param = $(this).data('param'),
					param_val = $(this).data('val'),
					param2 = $(this).data('param2'),
					param_val2 = $(this).data('val2');
				$('.modal_maps').parent().append('<div class="form-group maps_cek">					<div class="input-group col-md-12">				<br>	<label class="col-sm-12 control-label" style="text-align: left !important">'+param+'</label>					<input id="'+param+'" name="inputan" type="text" class="form-control" value="'+param_val+'">					</div>				</div><div class="form-group maps_cek">					<div class="input-group col-md-12"><label class="col-sm-12 control-label" style="text-align: left !important">'+param2+'</label>					<input id="'+param2+'" name="inputan" type="text" class="form-control" value="'+param_val2+'">					</div>				</div>');	
			});
		</script>
    </body>
</html>