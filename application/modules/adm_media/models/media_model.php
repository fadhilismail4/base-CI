<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Media_model extends CI_Model { 
	
		function get_category($type, $id){
			$q="
				SELECT distinct(c.object_id), c.category_id, c.type_id, t.title, t.parent_id
				FROM ccs_o c, ccs_oct t
				WHERE  t.category_id = ".$id."
				and c.category_id=t.category_id
				and t.type_id = ".$type."
				and t.language_id=2
				UNION 
				SELECT distinct(c.object_id), c.category_id, c.type_id, t.title, t.parent_id
				FROM ccs_o c, ccs_oct t
				WHERE  t.parent_id = ".$id."
				and c.category_id=t.category_id
				and t.type_id= ".$type."
				and t.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_list($ccs_id,$type){
			$q="			
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.category_id=d.category_id								
				and c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.ccs_id = m.ccs_id
				and c.ccs_id = d.ccs_id
				and c.ccs_id = a.ccs_id
				and c.ccs_id = ".$ccs_id."
				and c.ccs_key = ".$type."
				and c.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_listcat($type, $id){
			$q="
				SELECT distinct(m.object_id), m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.type_id = m.type_id 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = m.category_id 
				and a.admin_id = d.createdby
				and c.type_id = ".$type."
				and c.language_id=2
				and c.category_id = ".$id."
				UNION 				
				SELECT distinct(m.object_id), m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.category_id=m.category_id								
				and c.type_id = m.type_id 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = m.category_id 
				and a.admin_id = d.createdby
				and c.type_id = ".$type."
				and c.language_id=2
				and c.parent_id = ".$id."
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
}
	