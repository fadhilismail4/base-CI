<div class="ibox float-e-margins">
	<div class="ibox-title">
		<ol class="breadcrumb col-md-7">
			<li class="active">
				<h5>Add new Gallery</h5>
			</li>
		</ol>
	</div>
	<div class="ibox-content">
		<div class="form-horizontal">
			<div class="form-group">
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<div class="col-md-4">
					<input id="image_square" name="image_square" class="file" type="file" value="">
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Title</label>
						<div class="col-sm-10">
							<input id="title" name="inputan" type="text" class="form-control" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Description</label>
						<div class="col-sm-10">
							<textarea id="description" name="inputan" type="text" class="form-control" value=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<select class="form-control m-b" name="inputan" id="category_id">
								<?php foreach($category as $cat){?>
								<option value="<?php echo $cat->category_id ;?>"><?php echo $cat->title ;?></option>
								<?php } ;?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<option value="0">Draft</option>
								<option value="1">Publish</option>
								<option value="2">Pending</option>
							</select>
						</div>
					</div>
					<button id="gallery" class="create btn btn-block btn-primary">Create</button>
					<div class="space-25"></div>
					<a class="btn btn-block btn-warning" href="">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>	