<div class="text-center article-title">
	<div style="margin-bottom: -80px !important;">
		<h1 id="title" name="inputan">
			SEO Settings
		</h1>
		<span>
			On <strong><?php echo $news->title;?></strong>
		</span>
	</div>	
</div>

<div class="row" id="" name="">
	<div class="col-md-2">
		Meta Keyword  
	</div> 
	<div class="col-md-10">
		: <?php echo trim($news->metakeyword);?>
	</div>
</div>
<div class="row" id="" name="">
	<div class="col-md-2">
		Meta Description  
	</div> 
	<div class="col-md-10">
		: <?php echo trim($news->metadescription);?>
	</div>
</div>

<hr>
<div class="row">
	<div class="col-md-6">
		<h5>Category:</h5>
		<p id="" name="" class=""><?php echo $news->category;?></p>
	</div>
	<!--
	<div class="col-md-6">
		<div class="small text-right">
			<h5>Stats:</h5>
			<div> <i class="fa fa-comments-o"> </i> 56 comments </div>
			<i class="fa fa-eye"> </i> 144 views
		</div>
	</div>
	-->
</div>