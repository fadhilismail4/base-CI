<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Category</h5>
	</div>
	<div class="ibox-content">
		<div class="row" style="margin: 0px;">
			<a id="<?php echo $part;?>" data-url="category" data-url2="add_category" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-primary col-md-6">Add Category</a>
			<a id="<?php echo $part;?>" data-url="category" data-url2="view_category" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-success col-md-6">View Category</a>
			<div class="space-15"></div>
		</div>	
		<?php if($child == 'gallery'){;?>
		<div class="row" style="margin: 0px;">
			<a id="3" data-url="gallery" data-url2="create" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-info btn-block">Upload Image</a>
		</div>	
		<?php ;};?>
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="1" data-url="<?php echo $child;?>" data-url2="list" data-lang="2" class="detail2">
							<span class="label label-info"></span> <strong>View All</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<?php if(!empty($category)){;?>
					<?php foreach($category as $c){;?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<a id="<?php echo $c['parent']->category_id;?>" data-url="<?php echo $child;?>" data-url2="listcat" data-lang="<?php echo $category[0]['parent']->language_id;?>" class="detail2">
							<span class="label label-info"></span> <?php echo $c['parent']->title;?>
							</a>
							<p class="pull-right"><?php echo '('.$c['count'].')' ;?></p> 
						</div>
						<ol class="dd-list">
						<?php foreach($c['child'] as $cc){;?>
							<li class="dd-item">
								<div class="dd-handle dd-nodrag">
									<a id="<?php echo $cc['child']->category_id;?>" data-url="<?php echo $child;?>" data-url2="listcat" data-lang="<?php echo $category[0]['parent']->language_id;?>" class="detail2">
									<span class="label label-info"></span> <?php echo $cc['child']->title;?>
									</a>
									<p class="pull-right"><?php echo '('.$cc['count'].')' ;?></p> 
								</div>
							</li>		
						<?php ;} ;?>
						</ol>				
					</li>		
					<?php ;};?>
					<?php ;}else{;?>
					<br>No Category Added
					<?php };?>
				</ol>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>	