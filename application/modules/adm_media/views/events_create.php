<style>
	.modal-backdrop.in{
		display: none !important;
	}
	.fileinput-upload-button{
		display:none;
	}
	/* .input-group-btn > .btn-file{
		display:none;
	} */
	.checkbox label{
		padding-left:0px;
	}
</style>
<div class="row">
	<div class="col-lg-9">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Basic Content<small> - All basic information on your event.</small></h5>
			</div>
			<div class="ibox-content">
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
				<form class="form-horizontal" method="get">
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Title</label>
						<div class="col-sm-10">
							<input id="title" name="inputan" type="text" class="form-control" value="">
						</div>
					</div>
					<div class="form-group" id="data_5">
						<label class="col-sm-2 control-label" style="text-align: left !important">Date Start</label>
						<div class="col-sm-10">
							<div class="input-daterange input-group" id="datepicker">
								<input id="datestart" name="inputan"type="text" class="input-sm form-control"/>
								<span class="input-group-addon">to</span>
								<input id="dateend" name="inputan" type="text" class="input-sm form-control" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Content</label>
					</div>
				</form>
				
				<div class="mail-box">
					<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
						<div id="description" name="inputan_summer" class="summernote">
							
						</div>
					</div>
				
					<div class="mail-body text-right tooltip-demo">
					</div>
				</div>
			</div>
		</div>
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>SEO SETTINGS</h5>
			</div>
			<div class="ibox-content">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Meta Keywords</label>
						<div class="col-sm-10">
							<textarea id="metakeyword" name="inputan" type="text" class="form-control" value=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Meta Description</label>
						<div class="col-sm-10">
							<textarea id="metadescription" name="inputan" type="text" class="form-control" value=""></textarea>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Featured Image</h5>
			</div>
			<div class="ibox-content">
				<input id="image_square" name="image_square" class="file" type="file" value="">
			</div>
		</div>
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Event Category</h5>
			</div>
			<div class="ibox-content">
				<div class="input-group col-md-12">
					<select class="form-control m-b" name="inputan" id="category_id">						
						<option value="">Select Category</option>
						<?php foreach($category as $cat){?>
						<option value="<?php echo $cat->category_id ;?>"><?php echo $cat->title ;?></option>
						<?php } ;?>
					</select>
				</div>
			</div>
		</div>
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Publish</h5>
			</div>
			<div class="ibox-content">
				<div class="form-group">
					<div class="input-group col-md-12">
						<select class="form-control m-b" name="inputan" id="status">
							<option value="">Select Status</option>
							<option value="0">Draft</option>
							<option value="1">Publish</option>
							<option value="2">Pending</option>
						</select>
					</div>
				</div>
				<div class="clearfix" id="data_2">
					<label class="control-label">Publish on</label>
					<div class="">
						<div class="input-group date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input id="datecreated" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="space-25"></div>
				<button id="event" class="create btn btn-block btn-primary compose-mail">Create</button>
				<div class="space-25"></div>
				<button class="btn btn-block btn-warning compose-mail" href="history.back();">Cancel</button>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
