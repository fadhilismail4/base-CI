  <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Events Table</h5>
                        <div class="ibox-tools">
                            <a href="event/new" class="btn btn-primary ">Add New</a>
                        </div>
                    </div>
                    <div class="ibox-content">

                    <table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
                   <thead>
						<tr>
							<th>No</th>
							<th>Image </th>
							<th>Title </th>
							<th>Category </th>							
							<th>Event Date </th>
							<th>Createdby</th>
							<th>Date Created </th>
							<th>Viewer </th>
							<th>Status</th>
						</tr>
					</thead>
                    <tbody>
					<?php if(!empty($event)){ ?>
						<?php $i = 1; foreach($event as $n){ ?>
						<tr class="gradeX">
							<td><?php echo $i ?></td>
							<td><?php echo $n->image ?></td>
							<td><a href="<?php echo base_url('admin') ;?>/event/detail/<?php echo $n->media_id?>"><?php echo $n->title ?></a></td>
							<td><?php echo $n->category ?></td>
							<td><?php echo date("l, d M Y", strtotime($n->datestart)) ?> - <?php echo date("l, d M Y", strtotime($n->dateend)) ?></td>
							<td><?php echo $n->createdby ?></td>
							<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
							<td><?php echo $n->viewer ?></td>
							<td><?php echo $n->status ;?></td>
						</tr>	
						<?php $i++;} ?>
					<?php }else{ echo '<tr><td style="text-align: center">There is no data. Please create a new one... </td></tr>' ;} ;?>	
					</tbody>
                    <!--
					<tfoot>	
						<tr>
							<th>No</th>
							<th>Image </th>
							<th>Title </th>
							<th>Category </th>
							<th>Createdby</th>
							<th>Date Created </th>
							<th>Viewer </th>
							<th>Status</th>
						</tr>
                    </tfoot>
                    -->
					</table>

                    </div>
                </div>
            </div>
            </div>
            