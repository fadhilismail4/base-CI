<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Create Category test</h5>
	</div>
	<div class="ibox-content">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10"><input id="title" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Choose Parent</label>
				<div class="col-sm-10">
					<select class="form-control m-b" name="inputan" id="parent_id">
						<option value="0">Set as Parent Category</option>
						<?php foreach($category as $c){?>
						
						<option value="<?php echo $c->category_id ;?>"><?php echo $c->title ;?></option>
						<?php } ;?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" >Description</label>
				<div class="col-sm-10">
					<textarea id="description" name="inputan" type="text" class="form-control" value=""></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" >Meta Keywords</label>
				<div class="col-sm-10">
					<textarea id="metakeyword" name="inputan" type="text" class="form-control" value=""></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" >Meta Description</label>
				<div class="col-sm-10">
					<textarea id="metadescription" name="inputan" type="text" class="form-control" value=""></textarea>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-6 col-sm-offset-2">				
					<button id="all" data-url="<?php echo $part;?>" data-url2="list" data-lang="<?php if(!empty($category)){ echo $category[0]->language_id;}else{ echo $language_id;};?>" class="detail2 btn btn-white" class="btn btn-white" type="submit">Cancel</button>
					<button id="<?php echo $link_create;?>" class="create btn btn-success">Create</button>
				</div>
			</div>
		</form>
	</div>
</div>