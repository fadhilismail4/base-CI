<div class="row animated fadeInRight">
	<div class="col-md-3">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Event Detail</h5>
			</div>
			<div>
				<div class="ibox-content profile-content" >
					<div class="row">
						<img alt="image" class="col-md-12 img-responsive" src="<?php if($news->image){ echo base_url('assets').'/img/media/event/'.$news->image;}else{ echo base_url('assets').'/img/media/event/events_empty.png';}?>" style="display: block; margin-right: auto; margin-left: auto">
					</div>
					<div style="margin: 10px 0px !important">
						<h3 ><strong><?php echo $news->title;?></strong></h4>
						<h4 >Status : 
							<?php if($news->status == 1){ 
								echo "Published" ;
							}elseif($news->status == 0){
								echo "Draft" ;
							}else{ 
							echo "Scheduled" ;
							} ;?>
						</h5>					
					</div>
					<div class="row user-button" >			
						<div class="col-md-1">
						</div>
						<a href="<?php echo base_url('admin').'/event/update/'.$news->media_id;?>" ><button type="button" style="margin:0 1px"  class="col-md-5 btn btn-warning btn-md" ><i class="fa fa-edit" ></i> Edit</button></a>
						<button style="margin:0 1px" type="button" id="<?php echo $news->media_id;?>" data-lang="<?php echo $news->language_id;?>" data-url="event" data-title="<?php echo trim($news->title);?>" class="col-md-5 btn btn-danger btn-md modal_delete" ><i class="fa fa-minus-square-o"></i> Delete</button>
						
					</div>
				</div>				
			</div>
		</div>
	</div>
	<div class="col-lg-9">
		<div class="ibox">
			<div class="ibox-content">
				<div class="pull-right">
					<button id="<?php echo $news->media_id;?>" data-url="event" data-url2="content" data-lang="<?php echo $news->language_id;?>" class="detail btn btn-white btn-xs" type="button">Content</button>
					<button id="<?php echo $news->media_id;?>" data-url="event" data-url2="seo" data-lang="<?php echo $news->language_id;?>" class="detail btn btn-white btn-xs" type="button">SEO</button>
					<?php if($news->status == 1){ ;?>
						<button id="<?php echo $news->media_id;?>" data-url="event" data-lang="2" data-status="Draft" data-title="<?php echo $news->title;?>" class="modal_status btn btn-danger btn-xs" type="button"> Draft</button>
					<?php }elseif($news->status == 0){?>
						<button id="<?php echo $news->media_id;?>" data-url="event" data-lang="2" data-status="Publish" data-title="<?php echo $news->title;?>" class="modal_status btn btn-primary btn-xs" type="button"> Publish</button>
					<?php }else{?>									
						<button class="modal_status btn btn-xs btn-warning ">Pending</button>
					<?php } ;?>
				</div>
				<div class="pull-left">
					<button class="disabled btn btn-white btn-xs" type="button"><?php echo $language->code;?></button>
				</div>
				<div class="detail_content">
					<div class="text-center article-title">
						<div style="margin-bottom: -80px !important;">
							<h1 id="title" name="inputan" >
							<?php echo $news->title;?>	
							</h1>
							<span>
								Created at <?php echo date('H:i , D d M Y', strtotime($news->datecreated));?> by <strong><?php echo $createdby->name;?></strong>
								<br>
								<i class="fa fa-eye"></i>  <?php echo $news->viewer;?>  views
							</span>
						</div>
					</div>
					<div id="description" name="inputan_summer">
						<?php echo trim($news->description);?>
					</div>
					
					<hr>
					<div class="row">
						<div class="col-md-6">
							<h5>Category:</h5>
							<p id="" name="" class=""><?php echo $news->category;?></p>
						</div>
						<!--
						<div class="col-md-6">
							<div class="small text-right">
								<h5>Stats:</h5>
								<div> <i class="fa fa-comments-o"> </i> 56 comments </div>
								<i class="fa fa-eye"> </i> 144 views
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>