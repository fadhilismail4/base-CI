<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Update <?php echo $detail->title ;?> Category</h5>
		
		<div class="ibox-tools">
			<button id="3" data-url="category" data-url2="view_category" data-lang="2" class="detail2 btn btn-warning ">Back</button>
		</div>
	</div>
	<div class="ibox-content">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $detail->category_id ;?>"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10"><input id="title" name="inputan" type="text" class="form-control" value="<?php echo $detail->title ;?>"></input></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Choose Parent</label>
				<div class="col-sm-10">
					<select class="form-control m-b" name="inputan" id="parent_id">
						<option value="<?php echo $detail->category_id?>"><?php if($detail->parent_id != 0){echo $detail->title ;}else{ echo $detail->title ;} ;?></option>
						<?php if($detail->parent_id != 0){ ;?>		
							<option value="0">Set as Parent Category</option>		
							<?php foreach($category as $c){?>							
								<option value="<?php echo $c->category_id?>"><?php echo $c->title?></option>
							<?php ;}?>
						<?php ;}else{ ;?>															
							<option value="0">Set as Parent Category</option>
							<?php foreach($parent as $p){?>							
								<option value="<?php echo $p->category_id?>"><?php echo $p->title?></option>
							<?php ;}?>
						<?php } ;?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label" >Description</label>
				<div class="col-sm-10">
					<textarea id="description" name="inputan" type="text" class="form-control" value="<?php echo $detail->description ;?>"><?php echo $detail->description ;?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" >Meta Keywords</label>
				<div class="col-sm-10">
					<textarea id="metakeyword" name="inputan" type="text" class="form-control" value="<?php echo $detail->metakeyword ;?>"><?php echo $detail->metakeyword ;?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" >Meta Description</label>
				<div class="col-sm-10">
					<textarea id="metadescription" name="inputan" type="text" class="form-control" value="<?php echo $detail->metadescription ;?>"><?php echo $detail->metadescription ;?></textarea>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-6 col-sm-offset-2">				
					<button id="<?php echo $id ?>" data-url="category" data-url2="view_category" data-lang="2" class="detail2 btn btn-white" class="btn btn-white" type="submit">Cancel</button>
					<button id="<?php echo $link_create;?>" class="create btn btn-success">Update</button>
				</div>
			</div>
		</form>
	</div>
</div>