<div class="ibox float-e-margins">
	<div class="ibox-title">
		<ol class="breadcrumb col-md-7">
			<li class="active">
				<a id="<?php echo $id?>" data-url="category" data-url2="view_category" data-lang="2" class="detail2">
					<h5><?php echo $cat;?> Category</h5>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $id?>" data-url="category" data-url2="add_category" data-lang="2" class="detail2 btn btn-primary ">Add New</a>
		</div>
	</div>
	<div class="ibox-content">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Title </th>
					<th>Parent Category </th>
					<th>Createdby</th>
					<th>Date Created </th>
					<th>Status</th>
					<th>Activation</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($category)){ ?>
				<?php $i = 1; foreach($category as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->category_id?>" data-url="<?php echo strtolower($cat);?>" data-url2="listcat" data-lang="2" class="detail2">
							<?php echo $n->title ?>
						</a>
					</td>
					
					<td>
						<a id="<?php echo $n->parent_id?>" data-url="<?php echo strtolower($cat);?>" data-url2="listcat" data-lang="2" class="detail2">
							<?php if($n->parent_id != 0){ echo $n->parent_title ;} ;?>
						</a>
					</td>
					<td><?php echo $n->name ?></td>
					<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
					<td><?php if($n->status == 1){ echo "Active" ;}else{ echo "Inactive" ;};?></td>
					<td>
						<?php if($n->status == 1){ ;?>
							<button id="<?php echo $n->category_id;?>" data-url="category_media" data-lang="2" data-status="Inactivate" data-param="<?php echo strtolower($cat)?>" data-title="<?php echo $n->title;?>" class="modal_status btn btn-danger btn-xs" type="button"> Inactivate</button>
							<button id="<?php echo $n->category_id?>" data-url="category" data-url2="update_category" data-lang="2" data-param="<?php echo strtolower($cat)?>"class="detail2 btn btn-warning btn-xs" type="button"> Edit</button>
						<?php }else{?>
							<button  id="<?php echo $n->category_id;?>" data-url="category_media" data-lang="2" data-status="Activate" data-param="<?php echo strtolower($cat)?>" data-title="<?php echo $n->title;?>" class="modal_status btn btn-primary btn-xs" type="button"> Activate</button>
						<?php } ;?>
					</td>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
	</div>
</div>