<div class="ibox float-e-margins">
	<div class="ibox-title">
		<?php if($categories->parent_id == 0){ ;?>
		<ol class="breadcrumb col-md-7">
			<li class="active">
				<a  id="<?php echo $categories->category_id ;?>" data-url="event" data-url2="listcat" data-lang="2" class="detail2">
					<h5><?php echo $categories->title?> Category (<?php echo count($news)?>)</h5>
				</a>
			</li>
		</ol>
		<?php ;}else{ ;?>
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding: 6px 0px">
			<li class="">
				<a id="<?php echo $categories->parent_id ;?>" data-url="event" data-url2="listcat" data-lang="2" class="detail2">
					<?php echo $categories->parent_title?>
				</a>
			</li>
			<li class="active">
				<a  id="<?php echo $categories->category_id ;?>" data-url="event" data-url2="listcat" data-lang="2" class="detail2">
					<?php echo $categories->title?> Category (<?php echo count($news)?>)
				</a>
			</li>
		</ol>
		<?php ;} ;?>
		<div class="ibox-tools">
			<a href="event/new" class="btn btn-primary ">Add New</a>
		</div>
	</div>
	<div class="ibox-content">
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Title </th>
					<th>Category </th>
					<th>Createdby</th>
					<th>Date Created </th>
					<th>Viewer </th>
					<th>Status</th>
					<th>Activation</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($news)){ ?>
				<?php $i = 1; foreach($news as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td><a href="<?php echo base_url('admin') ;?>/event/detail/<?php echo $n->media_id?>"><?php echo $n->title ?></a></td>
					<td><?php if($n->parent_id != 0){ echo$n->pcat .', ' ;} ;?><?php echo $n->category ?></td>
					<td><?php echo $n->name ?></td>
					<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
					<td><?php echo $n->viewer ?></td>
					<td><?php if($n->status == 1){ echo "Published" ;}elseif($n->status == 0){ echo "Draft" ;}else{ echo "Scheduled" ;} ;?></td>
					<td>
						<?php if($n->status == 1){ ;?>
							<button id="<?php echo $n->media_id;?>" data-url="event" data-lang="2" data-status="Draft" data-title="<?php echo $n->title;?>" class="modal_status btn btn-danger btn-xs" type="button"> Unpublish</button>
						<?php }elseif($n->status == 0){ ;?>
							<button id="<?php echo $n->media_id;?>" data-url="event" data-lang="2" data-status="Publish" data-title="<?php echo $n->title;?>"class="modal_status btn btn-primary btn-xs" type="button"> Publish</button>
						<?php }else{ ;?>
							<button id="" data-url="" data-lang="2" data-status="" data-title="<?php echo $n->title;?>"class=" btn btn-warning btn-xs" type="button"> Pending</button>
						<?php } ;?>
					</td>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
	</div>
</div>