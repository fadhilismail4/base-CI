<div class="text-center article-title">
	<div style="margin-bottom: -80px !important;">
		<h1 id="title" name="inputan" >
		<?php echo $news->title;?>	
		</h1>
		<span>
			Created at <?php echo date('H:i , D d M Y', strtotime($news->datecreated));?> by <strong><?php echo $createdby->name;?></strong>
			<br>
			<i class="fa fa-eye"></i>  <?php echo $news->viewer;?>  views
		</span>
	</div>
</div>
<div id="description" name="inputan_summer">
	<?php echo trim($news->description);?>
</div>

<hr>
<div class="row">
	<div class="col-md-6">
		<h5>Category:</h5>
		<p id="" name="" class=""><?php echo $news->category;?></p>
	</div>
	<!--
	<div class="col-md-6">
		<div class="small text-right">
			<h5>Stats:</h5>
			<div> <i class="fa fa-comments-o"> </i> 56 comments </div>
			<i class="fa fa-eye"> </i> 144 views
		</div>
	</div>
	-->
</div>