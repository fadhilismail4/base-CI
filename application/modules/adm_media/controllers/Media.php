<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media extends Admin {
	
	public function view($type){	
		if ($this->s_login && !empty($type))
		{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['zone'] 		= $this->zone;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['is_mobile']	= $this->is_mobile();
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				'css/plugins/dataTables/dataTables.bootstrap.css',
				'css/plugins/dataTables/dataTables.responsive.css',
				'css/plugins/dataTables/dataTables.tableTools.min.css'
			);
			$data['js'] = array(
				'js/plugins/dataTables/jquery.dataTables.js',
				'js/plugins/dataTables/dataTables.bootstrap.js',
				'js/plugins/dataTables/dataTables.responsive.js',
				'js/plugins/dataTables/dataTables.tableTools.min.js',
				'js/modul/datatables.js',
				'js/plugins/nestable/jquery.nestable.js'
			);
			$data['parent'] = 'media';
			//End set parameter view
			
			if($type == 'news')
			{
				$data['child'] = 'news';
				$data['ch'] = array(
					'title'=> $this->module_name, 
					'small'=> 'Manage your '.$this->module_name.' '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => $this->zone.'/admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => $this->zone.'/admin/'.$this->module_link, 
						'bcrumb' => $this->module_name, 
						'active' => true
					)
				);
				$data['custom_js'] = '<script>
						$(document).ready(function(){
							$("[data-url2='. "'" .'news'. "'" .']")[0].click();
							$("[data-url2='. "'" .'list'. "'" .']")[0].click();
						});
					</script>';		
				
				$data['content'] = 'news.php';
			}
			elseif($type == 'event')
			{
				$data['child'] = 'event';
				$data['ch'] = array(
					'title'=>'Events', 
					'small'=>'Manage your events '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/event', 
						'bcrumb' => 'Events', 
						'active' => true
					)
				);
				$data['custom_js'] = '<script>
						$(document).ready(function(){
							$("[data-url2='. "'" .'event'. "'" .']")[0].click();
							$("[data-url2='. "'" .'list'. "'" .']")[0].click();
						});
					</script>';		
				
				$data['content'] = 'events.php';
			}
			elseif($type == 'gallery')
			{
				$data['child'] = 'gallery';
				$data['ch'] = array(
					'title'=>'Gallery', 
					'small'=>'Manage your gallery '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/gallery', 
						'bcrumb' => 'Gallery', 
						'active' => true
					)
				);
				$data['custom_js'] = '<script>
						$(document).ready(function(){
							$("[data-url2='. "'" .'gallery'. "'" .']")[0].click();
							$("[data-url2='. "'" .'list'. "'" .']")[0].click();
						});
					</script>';	
				$data['content'] = 'gallery.php';
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail($zone,$type,$id){	
		if ($this->s_login && !empty($type))
		{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				''
			);
			$data['js'] = array(
				''
			);
			$data['parent'] = 'media';
			//End set parameter view
			
			if($type == 'news')
			{	
				
				$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 1 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
				$data['child'] = 'news';
				$data['ch'] = array(
					'title'=>'News', 
					'small'=>'Manage your news '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/news', 
						'bcrumb' => 'News', 
						'active' => ''
					),
					array(
						'url' => 'admin/news/detail/'.$id.'', 
						'bcrumb' => $data['news']->title, 
						'active' => true
					)
				);
				$data['createdby'] = $this->dbfun_model->get_data_bys('name','admin_id = '.$data['news']->createdby.'','base_admin');
				$data['language'] = $this->dbfun_model->get_data_bys('language_id, code, name','language_id = '.$data['news']->language_id.'','base_language');
				$data['content'] = 'news_detail.php';
			}
			elseif($type == 'event')
			{					
				$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 2 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
				$data['child'] = 'event';
				$data['ch'] = array(
					'title'=>'Events', 
					'small'=>'Manage your events '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/event', 
						'bcrumb' => 'Events', 
						'active' => ''
					),
					array(
						'url' => 'admin/event/detail/'.$id.'', 
						'bcrumb' => $data['news']->title, 
						'active' => true
					)
				);
				$data['createdby'] = $this->dbfun_model->get_data_bys('name','admin_id = '.$data['news']->createdby.'','base_admin');
				$data['language'] = $this->dbfun_model->get_data_bys('language_id, code, name','language_id = '.$data['news']->language_id.'','base_language');
				$data['content'] = 'events_detail.php';
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}
	
	public function detail_ajax($zone,$type,$part){	
		if ($this->s_login && !empty($type)){
			
			$this->load->model('Media_model');
			$data = array();
			$data['child'] = $part;
			if($type == 'news'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');

				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					
					if($part == 'list'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$type = 1;
						$data['news'] = $this->Media_model->get_list($type);
						$view = $this->load->view('news_alllist', $data, TRUE);
					}elseif($part == 'listcat'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$type = 1;
						$parent = $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' ','base_mediacat')->parent_id;
						$cid = $this->dbfun_model->get_data_bys('category_id','category_id = '.$id.' ','base_mediacat')->category_id;
						
						if($parent != 0 ){
							$data['news'] = $this->dbfun_model->get_all_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name','c.type_id = 1 and c.type_id = 1 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id and c.category_id = '.$id.' and a.admin_id = d.createdby ','base_mediacat c, base_media m, base_mediadesc d, base_admin a');
							
						}else{
							
							$data['news'] = $this->Media_model->get_listcat($type, $cid);
						}
						$data['categories'] = $this->dbfun_model->get_data_bys('category_id, parent_id, parent_title, title','category_id = '.$id.' ','base_mediacat');;
						//echo $data['news'];
						$view = $this->load->view('news_list', $data, TRUE);
					}elseif($part == 'content'){
						$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 1 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
						$data['createdby'] = $this->dbfun_model->get_data_bys('name','admin_id = '.$data['news']->createdby.'','base_admin');
						$view = $this->load->view('news_detail_content', $data, TRUE);
					}elseif($part == 'seo'){
						$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 1 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
						$view = $this->load->view('news_detail_seo', $data, TRUE);
					}
					$view = $view;
					echo $view;
				}
			}elseif($type == 'event'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');

				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					
					if($part == 'list'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$type = 2;
						$data['news'] = $this->Media_model->get_list($type);
						$view = $this->load->view('events_alllist', $data, TRUE);
					}elseif($part == 'listcat'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$type = 2;
						$parent = $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' ','base_mediacat')->parent_id;
						$cid = $this->dbfun_model->get_data_bys('category_id','category_id = '.$id.' ','base_mediacat')->category_id;
						
						if($parent != 0 ){
							$data['news'] = $this->dbfun_model->get_all_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name','c.type_id = '.$type.' and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id and c.category_id = '.$id.' and a.admin_id = d.createdby ','base_mediacat c, base_media m, base_mediadesc d, base_admin a');
							
						}else{
							
							$data['news'] = $this->Media_model->get_listcat($type, $cid);
						}
						$data['categories'] = $this->dbfun_model->get_data_bys('category_id, parent_id, parent_title, title','category_id = '.$id.' ','base_mediacat');;
						//echo $data['news'];
						$view = $this->load->view('events_list', $data, TRUE);
					}elseif($part == 'content'){
						$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 2 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
						$data['createdby'] = $this->dbfun_model->get_data_bys('name','admin_id = '.$data['news']->createdby.'','base_admin');
						$view = $this->load->view('events_detail_content', $data, TRUE);
					}elseif($part == 'seo'){
						$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 2 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
						$view = $this->load->view('events_detail_seo', $data, TRUE);
					}
					$view = $view;
					echo $view;
				}
			}elseif($type == 'category'){
				$this->form_validation->set_rules('id','id page', 'trim|required|numeric|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');

				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					
					if($part == 'news'){
						$data['part'] = 1;
						$data['language_id'] = 2;
						$category = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id,c.type_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = 0 order by c.title ASC','oct c');
						foreach($category as $c){
							
							$data['count'] = $this->Media_model->get_category($id, $c->category_id);
							$count = count($data['count']);
							
							$child = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = '.$c->category_id.' order by c.title ASC','oct c');
							$childs = array();
							foreach($child as $cd){
								$c_count = $this->dbfun_model->count_result(array('category_id'=> $cd->category_id ),'base_media'); 
								$childs[] =  array('child'=> $cd,'count'=> $c_count);
							}
							$datas[] = array('parent' => $c,'count' => $count,'child' => $childs);
						}
						$data['category'] = $datas;
						echo "<script>
								 $(document).ready(function(){
									 $('#nestable2').nestable();
								 });
							</script>";
						$view = $this->load->view('category', $data, TRUE);
					}elseif($part == 'event'){
						$data['part'] = 2;
						$data['language_id'] = 2;
						$category = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id,c.type_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = 0 order by c.title ASC','base_mediacat c');
						foreach($category as $c){
							
							$data['count'] = $this->Media_model->get_category($id, $c->category_id);
							$count = count($data['count']);
							
							$child = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = '.$c->category_id.' order by c.title ASC','base_mediacat c');
							
							$childs = array();
							foreach($child as $cd){
								$c_count = $this->dbfun_model->count_result(array('category_id'=> $cd->category_id ),'base_media'); 
								$childs[] =  array('child'=> $cd,'count'=> $c_count);
							}
							$datas[] = array('parent' => $c,'count' => $count,'child' => $childs);
						}
						$data['category'] = $datas;
						echo "<script>
								 $(document).ready(function(){
									 $('#nestable2').nestable();
								 });
							</script>";
						$view = $this->load->view('category', $data, TRUE);
					}elseif($part == 'gallery'){
						$data['part'] = 5;
						$data['language_id'] = 2;
						$category = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id,c.type_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = 0 order by c.title ASC','base_mediacat c');
						$datas = array();
						foreach($category as $c){
							
							$data['count'] = $this->Media_model->get_category($id, $c->category_id);
							$count = count($data['count']);
							
							$child = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = '.$c->category_id.' order by c.title ASC','base_mediacat c');
							
							$childs = array();
							foreach($child as $cd){
								$c_count = $this->dbfun_model->count_result(array('category_id'=> $cd->category_id ),'base_media'); 
								$childs[] =  array('child'=> $cd,'count'=> $c_count);
							}
							$datas[] = array('parent' => $c,'count' => $count,'child' => $childs);
						}
						$data['category'] = $datas;
						echo "<script>
								 $(document).ready(function(){
									 $('#nestable2').nestable();
								 });
							</script>";
						$view = $this->load->view('category', $data, TRUE);
					}elseif($part == 'add_category'){
						if($id == 1){
							$data['language_id'] = 2;
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id,c.type_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = 0 order by c.title ASC','base_mediacat c');
							$data['part'] = 'news';
							$data['link_create'] = 'category_news';
							$view = $this->load->view('category_create', $data, TRUE);
						}elseif($id == 2){
							$data['language_id'] = 2;
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id,c.type_id','c.type_id = '.$id.' and c.language_id = '.$lang.' and c.parent_id = 0 order by c.title ASC','base_mediacat c');
							$data['part'] = 'event';
							$data['link_create'] = 'category_event';
							$view = $this->load->view('category_create', $data, TRUE);
						}elseif($id == 3){
							$data['language_id'] = 2;
							$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id, language_id, parent_id','parent_id = 0 and language_id = 2','com_ideacat');
							$data['part'] = 'youth';
							$data['link_create'] = 'category_ideas';
							$view = $this->load->view('category_create2', $data, TRUE);
						}elseif($id == 4){
							$data['language_id'] = 2;
							$data['part'] = 'development';
							$data['link_create'] = 'category_news';
							$view = $this->load->view('category_create2', $data, TRUE);
						}elseif($id == 5){
							$data['language_id'] = 2;
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.title, c.language_id, c.parent_id, c.category_id,c.type_id','c.type_id = 3 and c.language_id = '.$lang.' and c.parent_id = 0 order by c.title ASC','base_mediacat c');
							$data['part'] = 'gallery';
							$data['link_create'] = 'category_gallery';
							$view = $this->load->view('category_create', $data, TRUE);
						}
					}elseif($part == 'view_category'){
						if($id == 1){
							$lang = 2; 
							echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = '.$id.' and c.language_id = '.$lang.' and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['cat'] = 'News';
							$data['id'] = $id; 
							$data['count_c'] = count($data['category']);
							//$data['link_create'] = 'category_nupdate';
							$view = $this->load->view('category_view', $data, TRUE);
						}elseif($id == 2){
							$lang = 2; 
							echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = '.$id.' and c.language_id = '.$lang.' and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['cat'] = 'Event';
							$data['id'] = $id; 
							$data['count_c'] = count($data['category']);
							$view = $this->load->view('category_view', $data, TRUE);
						}elseif($id == 3){
							$lang = 2; 
							echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.language_id = '.$lang.' and a.admin_id=c.createdby order by c.title ASC','com_ideacat c, base_admin a');
							$data['cat'] = 'Ideas';
							$data['id'] = $id; 
							$data['count_c'] = count($data['category']);
							$view = $this->load->view('category_ideas', $data, TRUE);
						}elseif($id == 4){
							$lang = 2; 
							echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.language_id = '.$lang.' and a.admin_id=c.createdby order by c.title ASC','com_deveopmentcat c, base_admin a');
							$data['cat'] = 'Development';
							$data['id'] = $id; 
							$data['count_c'] = count($data['category']);
							$view = $this->load->view('category_dev', $data, TRUE);
						}elseif($id == 5){
							$lang = 2; 
							echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = 3 and c.language_id = '.$lang.' and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['cat'] = 'Gallery';
							$data['id'] = 3; 
							$data['count_c'] = count($data['category']);
							$view = $this->load->view('category_view', $data, TRUE);
						}
					}elseif($part == 'update_category'){
						$param = $this->input->post('param');
						if($id && $param == 'news'){
							$lang = 2; 
							$data['detail'] = $this->dbfun_model->get_data_bys('c.title, c.language_id, c.parent_id, c.parent_title, c.category_id,c.type_id','c.type_id = 1 and c.category_id = '.$id.' and c.language_id = '.$lang.' ','base_mediacat c');
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = 1 and c.language_id = '.$lang.' and c.parent_id = 0 and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['parent'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = 1 and c.language_id = '.$lang.' and c.parent_id = 0 and c.category_id != '.$id.' and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['id'] = 1; 
							$data['link_create'] = 'category_news';
							$view = $this->load->view('category_update', $data, TRUE);
						}elseif($id && $param == 'event'){
							$data['detail'] = $this->dbfun_model->get_data_bys('c.title, c.language_id, c.parent_id, c.parent_title, c.category_id,c.type_id','c.type_id = 2 and c.category_id = '.$id.' and c.language_id = '.$lang.' ','base_mediacat c');
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = 2 and c.language_id = '.$lang.' and c.parent_id = 0 and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['parent'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = 2 and c.language_id = '.$lang.' and c.parent_id = 0 and c.category_id != '.$id.' and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['id'] = 2; 
							$data['link_create'] = 'category_event';
							$view = $this->load->view('category_update', $data, TRUE);
						}elseif($id && $param == 'ideas'){
							$data['detail'] = $this->dbfun_model->get_data_bys('c.description, c.metadescription, c.metakeyword, c.title, c.language_id, c.parent_id, c.parent_title, c.category_id','c.category_id = '.$id.' and c.language_id = '.$lang.' ','com_ideacat c');
							
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.language_id = '.$lang.' and c.parent_id = 0 and a.admin_id=c.createdby order by c.title ASC','com_ideacat c, base_admin a');
							
							$data['parent'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.language_id = '.$lang.' and c.parent_id = 0 and c.category_id != '.$id.' and a.admin_id=c.createdby order by c.title ASC','com_ideacat c, base_admin a');
							$data['id'] = $id; 
							$data['link_create'] = 'category_ideas';
							$view = $this->load->view('category_update2', $data, TRUE);
						}elseif($id && $param == 'development'){
							$data['detail'] = $this->dbfun_model->get_data_bys('c.title, c.language_id, c.parent_id, c.parent_title, c.category_id','c.category_id = '.$id.' and c.language_id = '.$lang.' ','com_deveopmentcat c');
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.language_id = '.$lang.' and c.parent_id = 0 and a.admin_id=c.createdby order by c.title ASC','com_deveopmentcat c, base_admin a');
							$data['parent'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.language_id = '.$lang.' and c.parent_id = 0 and c.category_id != '.$id.' and a.admin_id=c.createdby order by c.title ASC','com_deveopmentcat c, base_admin a');
							$data['id'] = 2; 
							$data['link_create'] = 'category_event';
							$view = $this->load->view('category_update2', $data, TRUE);
						}elseif($id && $param == 'gallery'){
							$data['detail'] = $this->dbfun_model->get_data_bys('c.title, c.language_id, c.parent_id, c.parent_title, c.category_id,c.type_id','c.type_id = 3 and c.category_id = '.$id.' and c.language_id = '.$lang.' ','base_mediacat c');
							$data['category'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = 3 and c.language_id = '.$lang.' and c.parent_id = 0 and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['parent'] = $this->dbfun_model->get_all_data_bys('c.language_id, c.parent_id, c.category_id,c.type_id, c.title, c.parent_title, c.datecreated, c.createdby, c.status, a.name','c.type_id = 3 and c.language_id = '.$lang.' and c.parent_id = 0 and c.category_id != '.$id.' and a.admin_id=c.createdby order by c.title ASC','base_mediacat c, base_admin a');
							$data['id'] = 3; 
							$data['link_create'] = 'category_gallery';
							$view = $this->load->view('category_update', $data, TRUE);
						}
					}
					$view = $view;
					echo $view;
				}
			}elseif($type == 'gallery'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					
					if($part == 'list'){
						echo '';
						$type = 3;
						$data['gallery'] = $this->Media_model->get_list($type);
						$view = $this->load->view('gallery_list', $data, TRUE);
					}elseif($part == 'create'){
						$type = 3;
						echo '<link href="'.base_url().'assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet"><script src="'.base_url().'assets/admin/js/plugins/fileinput/fileinput.min.js" type="text/javascript"></script><script src="'.base_url().'assets/admin/js/ajaxfileupload.js" type="text/javascript"></script>';
						$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','type_id = 3 and language_id = 2','base_mediacat');
						$view = $this->load->view('gallery_create', $data, TRUE);
					}elseif($part == 'listcat'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$type = 3;
						$parent = $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' ','base_mediacat')->parent_id;
						$cid = $this->dbfun_model->get_data_bys('category_id','category_id = '.$id.' ','base_mediacat')->category_id;
						
						if($parent != 0 ){
							$data['news'] = $this->dbfun_model->get_all_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name','c.type_id = 3 and c.type_id = 3 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id and c.category_id = '.$id.' and a.admin_id = d.createdby ','base_mediacat c, base_media m, base_mediadesc d, base_admin a');
							
						}else{
							
							$data['news'] = $this->Media_model->get_listcat($type, $cid);
						}
						$data['categories'] = $this->dbfun_model->get_data_bys('category_id, parent_id, parent_title, title','category_id = '.$id.' ','base_mediacat');
						$view = $this->load->view('news_list', $data, TRUE);
					}elseif($part == 'content'){
						$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 1 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
						$data['createdby'] = $this->dbfun_model->get_data_bys('name','admin_id = '.$data['news']->createdby.'','base_admin');
						$view = $this->load->view('news_detail_content', $data, TRUE);
					}elseif($part == 'seo'){
						$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 1 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
						$view = $this->load->view('news_detail_seo', $data, TRUE);
					}
					$view = $view;
					echo $view;
				}
			}
		}else{
			redirect($this->login);
		}
	}
	
	public function view_add_new($type){	
		if ($this->s_login && !empty($type))
		{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				'css/plugins/datapicker/datepicker3.css',
				'css/plugins/iCheck/custom.css',
				'js/plugins/fileinput/fileinput.min.css',
				'css/plugins/chosen/chosen.css',
				'css/plugins/summernote/summernote-bs3.css',
				'css/plugins/summernote/summernote.css'
			);
			$data['js'] = array(
					'js/plugins/iCheck/icheck.min.js',
					'js/plugins/datapicker/bootstrap-datepicker.js',
					'js/plugins/summernote/summernote.min.js',
					'js/plugins/chosen/chosen.jquery.js',
					'js/plugins/fileinput/fileinput.min.js',
					'js/ajaxfileupload.js',
					'js/modul/new.js'
				);
			$data['custom_js'] = 	"<script>
					$(document).ready(function(){
						$('#data_2').hide();
						$('#status').change(function() {
							if ($(this).find(':selected').val() === '2') {
								$('#data_2').slideDown('slow');
							} else {
								$('#data_2').slideUp('slow');
							}
						});
					});</script>";
			$data['parent'] = 'media';
			//End set parameter view
			
			if($type == 'news')
			{
				$data['child'] = 'news';
				$data['ch'] = array(
					'title'=>' Add News', 
					'small'=>'Create your news '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/news', 
						'bcrumb' => 'News', 
						'active' => false
					),
					array(
						'url' => 'admin/news/new', 
						'bcrumb' => 'New', 
						'active' => true
					)
				);
				$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','type_id = 1 and language_id = 2','base_mediacat');
				
				$data['content'] = 'news_create.php';
			}
			elseif($type == 'event')
			{
				$data['child'] = 'event';
				$data['ch'] = array(
					'title'=>' Add Event', 
					'small'=>'Create your event '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/event', 
						'bcrumb' => 'Events', 
						'active' => false
					),
					array(
						'url' => 'admin/event/new', 
						'bcrumb' => 'New', 
						'active' => true
					)
				);
				$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','type_id = 2 and language_id = 2','base_mediacat');
				
				$data['content'] = 'events_create.php';
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function view_update($zone,$type,$id){
		if ($this->s_login && !empty($type))
		{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				'css/plugins/datapicker/datepicker3.css',
				'css/plugins/iCheck/custom.css',
				'js/plugins/fileinput/fileinput.min.css',
				'css/plugins/chosen/chosen.css',
				'css/plugins/summernote/summernote-bs3.css',
				'css/plugins/summernote/summernote.css'
			);
			$data['js'] = array(
				'js/plugins/iCheck/icheck.min.js',
				'js/plugins/datapicker/bootstrap-datepicker.js',
				'js/plugins/summernote/summernote.min.js',
				'js/plugins/chosen/chosen.jquery.js',
				'js/plugins/fileinput/fileinput.min.js',
				'js/ajaxfileupload.js',
				'js/modul/new.js'
			);
			$data['parent'] = 'media';
			//End set parameter view
			
			if($type == 'news')
			{
				$data['child'] = 'news';
				$data['ch'] = array(
					'title'=>' Add News', 
					'small'=>'Create your news '
				);
				
				$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and m.media_id = d.media_id and d.language_id = 2','base_mediacat c, base_media m, base_mediadesc d');
				
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/news', 
						'bcrumb' => 'News', 
						'active' => ''
					),
					array(
						'url' => 'admin/news/update/'.$id.'', 
						'bcrumb' => 'Update '.$data['news']->title.'', 
						'active' => true
					)
				);
				
				$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','type_id = 1 and language_id = 2','base_mediacat');
				
				$data['content'] = 'news_update.php';
				if(!empty($data['news']->image)){
					$data['custom_js'] = '<script>
						$(document).ready(function(){
							$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
							$("#status").val('.trim($data['news']->status).');
							$("#category_id").val('.trim($data['news']->category_id).');
							$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
							$(".file").fileinput("refresh",{
								initialPreview: [
									"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/news/'.$data['news']->image.'" class="file-preview-image">').'"
								]
							});
						});</script>';
				}else{
					$data['custom_js'] = '<script>
					$(document).ready(function(){
						$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
						$("#status").val('.trim($data['news']->status).');
						$("#category_id").val('.trim($data['news']->category_id).');
						$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
						$(".file").fileinput("refresh",{
							initialPreview: [
								"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/news/news_empty.png'.'" class="file-preview-image">').'"
							]
						});
					});</script>';
				}
			}
			elseif($type == 'event')
			{
				$data['child'] = 'Event';
				$data['ch'] = array(
					'title'=>' Add Event', 
					'small'=>'Create your event '
				);
				
				$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, m.media_id, m.datestart, m.dateend, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and m.media_id = d.media_id and d.language_id = 2','base_mediacat c, base_media m, base_mediadesc d');
				
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/event', 
						'bcrumb' => 'Events', 
						'active' => ''
					),
					array(
						'url' => 'admin/event/update/'.$id.'', 
						'bcrumb' => 'Update '.$data['news']->title.'', 
						'active' => true
					)
				);
				
				$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','type_id = 2 and language_id = 2','base_mediacat');
				
				$data['content'] = 'events_update.php';
				if(!empty($data['news']->image)){
					$data['custom_js'] = '<script>
						$(document).ready(function(){
							$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
							$("#status").val('.trim($data['news']->status).');
							$("#category_id").val('.trim($data['news']->category_id).');
							$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
							$("#datestart").val("'.trim(date('Y-m-d', strtotime($data['news']->datestart))).'");
							$("#dateend").val("'.trim(date('Y-m-d', strtotime($data['news']->dateend))).'");
							$(".file").fileinput("refresh",{
								initialPreview: [
									"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/event/'.$data['news']->image.'" class="file-preview-image">').'"
								]
							});
						});</script>';
				}else{
					$data['custom_js'] = '<script>
					$(document).ready(function(){
						$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
						$("#status").val('.trim($data['news']->status).');
						$("#category_id").val('.trim($data['news']->category_id).');
						$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
						$("#datestart").val("'.trim(date('Y-m-d', strtotime($data['news']->datestart))).'");
						$("#dateend").val("'.trim(date('Y-m-d', strtotime($data['news']->dateend))).'");
						$(".file").fileinput("refresh",{
							initialPreview: [
								"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/event/events_empty.png'.'" class="file-preview-image">').'"
							]
						});
					});</script>';
				}
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}
	
	public function create($type){
    	if ($this->s_login && !empty($type))
    	{
			$data = array();
			$status = '';
			$message = '';
			if($type == 'news')
			{
				$id = $this->input->post('media_id');
				$lang = $this->input->post('language_id');
				
					if($id) //update
					{
						if ($this->form_validation->run('base_mediadesc') === TRUE)
						{
							if(isset($_FILES['image_square'])){
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','image_square','img/media/news','image_square','jpeg|jpg|png');
							}else{
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','','','','');
							}
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{
								$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_media');
								$status = 'success';
								$message = 'Data updated successfully';
							}
							if ($this->form_validation->run('base_mediadesc') === TRUE)
							{
								$update2 = $this->update_data_bys('*', array('media_id' => $id), 'base_mediadesc','','','','');
								if($update2)
								{
									$this->dbfun_model->update_table(array('media_id' => $id, 'language_id' => $lang), $update2, 'base_mediadesc');
									$status = 'success';
									$message = 'Data updated successfully';
								}
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else  // create
					{
						if ($this->form_validation->run('base_mediadesc') === TRUE)
						{
							$image = $this->input->post('image_square');
							$custom_primary = array(
										'type_id' => 1
									);
							$create_new = $this->new_data('base_media',$custom_primary, 'image_square','image_square', 'img/media/news','jpeg|jpg|png');
							
							$value = $this->dbfun_model->ins_return_id('base_media', $create_new);
							
							if ($this->form_validation->run('base_mediadesc') === TRUE)
							{
								$status = $this->input->post('status');
								$admin_id = $this->session->userdata('adm_admin_id');
								if($status == 0){
									$custom = array(
												'media_id' => $value,
												'createdby' => $admin_id
											);
								}elseif($status == 1){
									$custom = array(
												'media_id' => $value,
												'createdby' => $admin_id,
												'datecreated' => date('Y-m-d')
											);
								}elseif($status == 2){
									$custom = array(
												'media_id' => $value,
												'createdby' => $admin_id,
												'datecreated' => $this->input->post('datecreated'),
												'datepublish' => $this->input->post('datecreated')
											);
								}
								$create_new_secondary = $this->new_data_secondary('base_mediadesc',$custom,'','','','');	
								$this->dbfun_model->ins_to('base_mediadesc', $create_new_secondary);
								
								$status = 'success';
								$message = 'Data created successfully';
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					
				echo json_encode(array('status' => $status, 'm' => $message));
				
			}
			elseif($type == 'event')
			{
				$id = $this->input->post('media_id');
				$lang = $this->input->post('language_id');
				
					if($id) //update
					{
						if ($this->form_validation->run('base_mediadesc') === TRUE)
						{
							if(isset($_FILES['image_square'])){
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','image_square','img/media/event','image_square','jpeg|jpg|png');
							}else{
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','','','','');
							}
							
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{
								$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_media');
								$status = 'success';
								$message = 'Data updated successfully';
							}
							if ($this->form_validation->run('base_mediadesc') === TRUE)
							{
								$update2 = $this->update_data_bys('*', array('media_id' => $id), 'base_mediadesc','','','','');
								if($update2)
								{
									$this->dbfun_model->update_table(array('media_id' => $id, 'language_id' => $lang), $update2, 'base_mediadesc');
									$status = 'success';
									$message = 'Data updated successfully';
								}
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else  // create
					{
						if ($this->form_validation->run('base_mediadesc') === TRUE)
						{
							$image = $this->input->post('image_square');
							$custom_primary = array(
										'type_id' => 2
									);
							$create_new = $this->new_data('base_media',$custom_primary, 'image_square','image_square', 'img/media/event','jpeg|jpg|png');
							
							$value = $this->dbfun_model->ins_return_id('base_media', $create_new);
							
							if ($this->form_validation->run('base_mediadesc') === TRUE)
							{
								$status = $this->input->post('status');
								$admin_id = $this->session->userdata('adm_admin_id');
								if($status == 0){
									$custom = array(
												'media_id' => $value,
												'createdby' => $admin_id
											);
								}elseif($status == 1){
									$custom = array(
												'media_id' => $value,
												'createdby' => $admin_id,
												'datecreated' => date('Y-m-d')
											);
								}elseif($status == 2){
									$custom = array(
												'media_id' => $value,
												'createdby' => $admin_id,
												'datecreated' => $this->input->post('datecreated'),
												'datepublish' => $this->input->post('datecreated')
											);
								}
								$create_new_secondary = $this->new_data_secondary('base_mediadesc',$custom,'','','','');	
								$this->dbfun_model->ins_to('base_mediadesc', $create_new_secondary);
								
								$status = 'success';
								$message = 'Data created successfully';
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					
				echo json_encode(array('status' => $status, 'm' => $message));
			
				
			}
			elseif($type == 'gallery')
			{
				$id = $this->input->post('media_id');
				$lang = $this->input->post('language_id');
				
					if($id) //update
					{
						if ($this->form_validation->run('base_mediadesc') === TRUE)
						{
							if(isset($_FILES['image_square'])){
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','image_square','img/media/event','image_square','jpeg|jpg|png');
							}else{
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','','','','');
							}
							
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{
								$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_media');
								$status = 'success';
								$message = 'Data updated successfully';
							}
							if ($this->form_validation->run('base_mediadesc') === TRUE)
							{
								$update2 = $this->update_data_bys('*', array('media_id' => $id), 'base_mediadesc','','','','');
								if($update2)
								{
									$this->dbfun_model->update_table(array('media_id' => $id, 'language_id' => $lang), $update2, 'base_mediadesc');
									$status = 'success';
									$message = 'Data updated successfully';
								}
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else  // create
					{
						$this->form_validation->set_rules('name', 'Name', 'trim|required');
						if (empty($_FILES['image_square']))
						{
							$this->form_validation->set_rules('image_square', 'Image', 'required');
						}
						if ($this->form_validation->run() === TRUE)
						{
							$image = $this->input->post('image_square');
							$custom_primary = array(
										'type_id' => 3
									);
								
							$create_new = $this->new_data('base_media',$custom_primary, 'image_square','image_square', 'img/media/event','jpeg|jpg|png');
							
							$value = $this->dbfun_model->ins_return_id('base_media', $create_new);
							
							if ($this->form_validation->run('base_mediadesc') === TRUE)
							{
								$status = $this->input->post('status');
								$admin_id = $this->session->userdata('adm_admin_id');
								$custom = array(
												'media_id' => $value,
												'createdby' => $admin_id,
												'datecreated' => date('Y-m-d'),
												'status' => 1
											);
								$create_new_secondary = $this->new_data_secondary('base_mediadesc',$custom,'','','','');	
								$this->dbfun_model->ins_to('base_mediadesc', $create_new_secondary);
								
								$status = 'success';
								$message = 'Data created successfully';
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					
				echo json_encode(array('status' => $status, 'm' => $message));
			
			}
			elseif($type == 'category_news' || $type == 'category_event' || $type == 'category_gallery')
			{
				$id = $this->input->post('category_id');
				$lang = $this->input->post('language_id');
				$admin_id = $this->session->userdata('adm_admin_id');
				
					if($id) //update
					{
						if ($this->form_validation->run('base_mediacat') === TRUE)
						{
							$update = $this->update_data_bys('*', array('category_id' => $id), 'base_mediacat','','','','');
							
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{
								if($type == 'category_news'){
									$type_id = 1;
								}elseif($type == 'category_event'){
									$type_id = 2;
								}elseif($type == 'category_gallery'){
									$type_id = 3;
								}
								$parent = $this->input->post('parent_id');
								if($parent != 0){								
									$update['parent_title'] = $this->dbfun_model->get_data_bys('title','category_id = '.$parent.' and type_id = '.$type_id.' ','base_mediacat')->title;
								}else{								
									$update['parent_title'] = '';
								}
								$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang), $update, 'base_mediacat');
								$status = 'success';
								$message = 'Data updated successfully';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else  // create
					{
						if ($this->form_validation->run('base_mediacat') === TRUE)
						{	
							if($type == 'category_news'){
								$type_id = 1;
								
							}elseif($type == 'category_event'){
								$type_id = 2;
							}elseif($type == 'category_gallery'){
								$type_id = 3;
							}
							
							$parent = $this->input->post('parent_id');
							if($parent != 0){								
								$title = $this->dbfun_model->get_data_bys('title','category_id = '.$parent.' and type_id = '.$type_id.' ','base_mediacat')->title;
							}else{								
								$title = '';
							}
							
							$category_id = $this->dbfun_model->get_data_bys('MAX(category_id) as category_id','type_id = '.$type_id.' ','base_mediacat')->category_id + 1;
							$custom_primary = array(
										'type_id' => $type_id,
										'status' => 1,
										'createdby' => $admin_id,
										'parent_title' => $title,
										'category_id' => $category_id
									);	
							$create_new = $this->new_data('base_mediacat',$custom_primary, '','', '','');
							
							$this->dbfun_model->ins_to('base_mediacat', $create_new);
							$status = 'success';
							$message = 'Data created successfully';
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					
				echo json_encode(array('status' => $status, 'm' => $message));
			}
			elseif($type == 'category_ideas')
			{
				$id = $this->input->post('category_id');
				$lang = $this->input->post('language_id');
				$admin_id = $this->session->userdata('adm_admin_id');
				
					if($id) //update
					{
						if ($this->form_validation->run('com_ideacat') === TRUE)
						{
							$update = $this->update_data_bys('*', array('category_id' => $id), 'com_ideacat','','','','');
							
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{	
								$parent = $this->input->post('parent_id');
								if($parent != 0){								
									$update['parent_title'] = $this->dbfun_model->get_data_bys('title','category_id = '.$parent.'','com_ideacat')->title;
								}else{										
									$update['parent_title'] = '';
								}
								$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang), $update, 'com_ideacat');
								$status = 'success';
								$message = 'Data updated successfully';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					
				echo json_encode(array('status' => $status, 'm' => $message));
			}
		}else{
			redirect($this->login);
		}
    }
	
	public function status($type){
		if ($this->s_login && !empty($type))
    	{
			$data = array();
    		$status = '';
    		$message = '';
			
			$this->form_validation->set_rules('id', 'page', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('lang', 'language', 'trim|numeric|xss_clean');
			$id = $this->input->post('id');
			$action = $this->input->post('status');
			$lang = $this->input->post('lang');
			if ($this->form_validation->run() === TRUE)
			{
				if($type == 'news')
				{
					if($action == 'draft')
					{
						$update = array
						(
							'status' => 0
						);
					}
					elseif($action == 'publish')
					{
						$update = array
						(
							'status' => 1
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('media_id' => $id,'language_id' => $lang), $update, 'base_mediadesc');
					}
					else
					{
						$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_mediadesc');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}
				elseif($type == 'event')
				{
					if($action == 'draft')
					{
						$update = array
						(
							'status' => 0
						);
					}
					elseif($action == 'publish')
					{
						$update = array
						(
							'status' => 1
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('media_id' => $id,'language_id' => $lang), $update, 'base_mediadesc');
					}
					else
					{
						$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_mediadesc');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}elseif($type == 'category_media')
				{
					$part = $this->input->post('param');
					if($part == 'news'){
						$type_id = 1;
					}elseif($part == 'event'){
						$type_id = 2;
					}elseif($part == 'gallery'){
						$type_id = 3;
					}
					if($action == 'inactivate')
					{
						$update = array
						(
							'status' => 0,
							'type_id' => $type_id
						);
					}
					elseif($action == 'activate')
					{
						$update = array
						(
							'status' => 1,
							'type_id' => $type_id
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang, 'type_id' => $type_id), $update, 'base_mediacat');
					}
					else
					{
						$this->dbfun_model->update_table(array('category_id' => $id, 'type_id' => $type_id), $update, 'base_mediacat');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}elseif($type == 'category_ideas')
				{
					if($action == 'inactivate')
					{
						$update = array
						(
							'status' => 0
						);
					}
					elseif($action == 'activate')
					{
						$update = array
						(
							'status' => 1
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang), $update, 'com_ideacat');
					}
					else
					{
						$this->dbfun_model->update_table(array('category_id' => $id), $update, 'com_ideacat');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}
			}else{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}
	}
	
	public function delete($type){
    	if ($this->s_login && !empty($type))
    	{
			$data = array();
    		$status = '';
    		$message = '';
			
			if($type == 'news'){

				$this->form_validation->set_rules('id', 'news id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{
					$id = $this->input->post('id');
					$lang = $this->input->post('lang');
					
					$this->cek_n_delete_data('media_id,image_square', array('media_id' => $id), 'base_media', 'image_square','img/media/news',array('media_id' => $id),'base_mediadesc',array('media_id' => $id, 'language_id' => $lang));
			
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
			}elseif($type == 'event'){

				$this->form_validation->set_rules('id', 'event id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{
					$id = $this->input->post('id');
					$lang = $this->input->post('lang');
					
					$this->cek_n_delete_data('media_id,image_square', array('media_id' => $id), 'base_media', 'image_square','img/media/event',array('media_id' => $id),'base_mediadesc',array('media_id' => $id, 'language_id' => $lang));
			
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
			}elseif(($type == 'category_event') || ($type == 'category_event')){

				$this->form_validation->set_rules('id', 'event id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{
					$id = $this->input->post('id');
					$lang = $this->input->post('lang');
					
					$this->dbfun_model->del_tables('category_id = '.$id.' and language_id = '.$lang.' ','base_mediacat');
			
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
			}
		}else{
			redirect($this->login);
		}
    }

}