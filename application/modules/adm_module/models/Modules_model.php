<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Modules_model extends CI_Model { 
	
		function get_promo($ccs_id, $ccs_key, $cat_id, $id, $url_id){
			$q="
				SELECT t . * , d.title as object, c.category_id as catid, c.title as category, s.title as varian, p.publish, p.basic
				FROM ccs_tkt t, ccs_ocs s, ccs_odc d, ccs_prv p, ccs_oct c
				WHERE t.ccs_id = s.ccs_id
				AND s.ccs_id = d.ccs_id
				AND d.ccs_id = p.ccs_id
				AND p.ccs_id = c.ccs_id
				AND p.ccs_id = ".$ccs_id."
				AND t.ccs_key = ".$ccs_key."
				AND t.url_id = p.ccs_key
				AND t.url_id = d.ccs_key
				AND t.url_id = c.ccs_key
				AND t.url_id = ".$url_id."
				AND t.category_id = ".$cat_id."
				AND t.object_id = ".$id."
				AND t.oid = p.object_id
				AND t.oid = d.object_id
				AND t.varian_id = p.varian_id
				AND p.url_id = s.ccs_key
				AND p.settings_id = s.settings_id
				AND d.category_id = c.category_id
				and t.vstatus = 1
				UNION
				SELECT t . * , d.title as object, c.category_id as catid, c.title as category, p.vstatus as varian, p.publish, p.basic
				FROM ccs_tkt t, ccs_odc d, ccs_prc p, ccs_oct c
				WHERE t.ccs_id = d.ccs_id
				AND d.ccs_id = p.ccs_id
				AND p.ccs_id = c.ccs_id
				AND p.ccs_id = ".$ccs_id."
				AND t.ccs_key = ".$ccs_key."
				AND t.url_id = p.ccs_key
				AND t.url_id = d.ccs_key
				AND t.url_id = c.ccs_key
				AND t.url_id = ".$url_id."
				AND t.category_id = ".$cat_id."
				AND t.object_id = ".$id."
				AND t.oid = p.object_id
				AND t.oid = d.object_id
				AND d.category_id = c.category_id
				AND t.vstatus = p.vstatus
				AND t.vstatus = 0
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_all_promo($ccs_id, $ccs_key, $url_id){
			$q="
				select  ccs_odc.title, ccs_odc.object_id, ccs_odc.category_id, ccs_tkt.discount, ccs_tkt.stock, ccs_tkt.sold, ccs_tkt.status, ccs_tkt.type_id, ccs_tkt.number, ccs_tkt.number_cat, ccs_tkt.ticket_id, ccs_tkt.datecreated, ccs_tkt.datestart, ccs_tkt.dateend, ccs_adm.name
				from ccs_odc, ccs_tkt, ccs_adm
				where ccs_tkt.ccs_key = ".$ccs_key."
				and ccs_odc.ccs_id = ".$ccs_id."
				and ccs_tkt.url_id = ".$url_id." 
				and ccs_odc.ccs_key = ccs_tkt.url_id
				and ccs_odc.ccs_id = ccs_tkt.ccs_id
				and ccs_odc.object_id = ccs_tkt.object_id 
				and ccs_tkt.type_id = 0
				and ccs_tkt.ccs_id = ccs_adm.ccs_id
				and ccs_tkt.createdby = ccs_adm.admin_id
				UNION
				select ccs_oct.title, ccs_oct.parent_id as object_id , ccs_oct.category_id , ccs_tkt.discount, ccs_tkt.stock, ccs_tkt.sold, ccs_tkt.status, ccs_tkt.type_id, ccs_tkt.number, ccs_tkt.number_cat, ccs_tkt.ticket_id, ccs_tkt.datecreated, ccs_tkt.datestart, ccs_tkt.dateend, ccs_adm.name
				from ccs_oct, ccs_tkt, ccs_adm
				where ccs_oct.ccs_id = ".$ccs_id."
				and ccs_oct.ccs_key = ".$url_id." 
				and ccs_tkt.ccs_key = ".$ccs_key."
				and ccs_oct.category_id = ccs_tkt.object_id
				and ccs_oct.ccs_id = ccs_tkt.ccs_id
				and ccs_oct.ccs_key = ccs_tkt.url_id
				and ccs_tkt.type_id =1
				and ccs_tkt.ccs_id = ccs_adm.ccs_id
				and ccs_tkt.createdby = ccs_adm.admin_id
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_category($ccs_key,$ccs_id, $id){
			$q="
				SELECT distinct(c.object_id), c.category_id, c.ccs_key, t.title, t.parent_id
				FROM ccs_odc c, ccs_oct t
				WHERE  t.category_id = ".$id."
				and c.category_id=t.category_id
				and t.ccs_key= c.ccs_key
				and t.ccs_key= ".$ccs_key."
				and t.ccs_id= c.ccs_id
				and t.ccs_id= ".$ccs_id."
				and t.language_id=2
				UNION 
				SELECT distinct(c.object_id), c.category_id, c.ccs_key, t.title, t.parent_id
				FROM ccs_odc c, ccs_oct t
				WHERE  t.parent_id = ".$id."
				and c.category_id=t.category_id
				and t.ccs_key= c.ccs_key
				and t.ccs_key= ".$ccs_key."
				and t.ccs_id= c.ccs_id
				and t.ccs_id= ".$ccs_id."
				and t.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_cat($ccs_key,$ccs_id, $id){
			$q="
				SELECT distinct(c.category_id), c.category_id, c.ccs_key, c.title, c.parent_id, c.parent_title
				FROM ccs_oct as c
				WHERE  c.category_id = ".$id."
				and c.ccs_key= ".$ccs_key."
				and c.ccs_id= ".$ccs_id."
				and c.language_id=2
				UNION 
				SELECT distinct(c.category_id), c.category_id, c.ccs_key, c.title, c.parent_id, c.parent_title
				FROM ccs_oct as c
				WHERE  c.parent_id = ".$id."
				and c.ccs_key= ".$ccs_key."
				and c.ccs_id= ".$ccs_id."
				and c.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_set($ccs_key,$ccs_id, $id){
			$q="
				SELECT distinct(c.settings_id), c.category_id, c.ccs_key, t.title, t.parent_id
				FROM ccs_ocs c, ccs_oct t
				WHERE  t.category_id = ".$id."
				and c.category_id=t.category_id
				and t.ccs_key= c.ccs_key
				and t.ccs_key= ".$ccs_key."
				and t.ccs_id= c.ccs_id
				and t.ccs_id= ".$ccs_id."
				and t.language_id=2
				UNION 
				SELECT distinct(c.settings_id), c.category_id, c.ccs_key, t.title, t.parent_id
				FROM ccs_ocs c, ccs_oct t
				WHERE  t.parent_id = ".$id."
				and c.category_id=t.category_id
				and t.ccs_key= c.ccs_key
				and t.ccs_key= ".$ccs_key."
				and t.ccs_id= c.ccs_id
				and t.ccs_id= ".$ccs_id."
				and t.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_list($ccs_id,$type){
			$q="			
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.category_id=d.category_id								
				and c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and a.admin_id = d.createdby
				and c.ccs_id = m.ccs_id
				and c.ccs_id = d.ccs_id
				and c.ccs_id = a.ccs_id
				and c.ccs_id = ".$ccs_id."
				and c.ccs_key = ".$type."
				and c.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_listcat($ccs_key, $id){
			$q="
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id=2
				and c.category_id = ".$id."
				UNION 				
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.category_id=d.category_id								
				and c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id=2
				and c.parent_id = ".$id."
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_listevent($ccs_key, $id){
			$q="
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name, m.datecreated as datestart, m.dateupdated as dateend
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id=2
				and c.category_id = ".$id."
				UNION 				
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name, m.datecreated as datestart, m.dateupdated as dateend
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.category_id=d.category_id								
				and c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id=2
				and c.parent_id = ".$id."
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_listset($ccs_key, $id){
			$q="
				SELECT distinct(m.settings_id), m.category_id, m.language_id,c.title as category, m.settings_id as object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, m.title, m.status, m.createdby, m.datecreated, a.name
				FROM ccs_oct c, ccs_ocs m, ccs_adm a
				WHERE  c.ccs_key = m.ccs_key 
				and m.language_id = c.language_id 
				and c.category_id = m.category_id 
				and a.admin_id = m.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id= 2
				and c.category_id = ".$id."
				UNION 				
				SELECT distinct(m.settings_id), m.category_id, m.language_id,c.title as category, m.settings_id as object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, m.title, m.status, m.createdby, m.datecreated, a.name
				FROM ccs_oct c, ccs_ocs m, ccs_adm a
				WHERE  c.ccs_key = m.ccs_key 
				and m.language_id = c.language_id 
				and c.category_id = m.category_id 
				and a.admin_id = m.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id= 2
				and c.parent_id = ".$id."
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}

		function get_trx($ccs_id, $ccs_key, $url){
			$q="SELECT * from (
					SELECT t.*, o.title, c.title as category, p.publish as dprice, a.name , p.vstatus as varian
					FROM ccs_trx t, ccs_oct c, ccs_prc p, ccs_adm a, ccs_odc o
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key = ".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id = 0
					and p.vstatus = 0
					and t.createdby = a.admin_id 
					and o.status = p.status
					and o.status = 1
					UNION
					SELECT t.*, o.title, c.title as category, p.publish as dprice, a.name , s.title as varian
					FROM ccs_trx t, ccs_oct c, ccs_prv p, ccs_adm a, ccs_odc o, ccs_ocs s
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and s.ccs_id = t.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key =".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id != 0
					and t.createdby = a.admin_id 
					and t.varian_id = p.varian_id
					and s.settings_id = p.settings_id
					and o.status = p.status
					and o.status = 1
				) as trxs order by trxs.datecreated DESC
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}

		function get_trx_order($ccs_id, $ccs_key, $url){
			$q="SELECT * from (
					SELECT t.*, o.title, c.title as category, p.publish as dprice, a.name , p.vstatus as varian
					FROM ccs_trx t, ccs_oct c, ccs_prc p, ccs_adm a, ccs_odc o
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key = ".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id = 0
					and p.vstatus = 0
					and t.createdby = a.admin_id 
					UNION
					SELECT t.*, o.title, c.title as category, p.publish as basic, a.name , s.title as varian
					FROM ccs_trx t, ccs_oct c, ccs_prv p, ccs_adm a, ccs_odc o, ccs_ocs s
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and s.ccs_id = t.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key =".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id != 0
					and t.createdby = a.admin_id 
					and t.varian_id = p.varian_id
					and s.settings_id = p.settings_id
				) as trxs order by trxs.datecreated DESC
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_trx_by_id($ccs_id, $ccs_key, $url, $id){
			$q="Select * from (
					SELECT t.*, o.title, c.title as category, p.publish as dprice, a.name , p.vstatus as varian
					FROM ccs_trx t, ccs_oct c, ccs_prc p, ccs_adm a, ccs_odc o
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.category_id = ".$id."
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key = ".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id = 0
					and p.vstatus = 0
					and t.createdby = a.admin_id 
					UNION
					SELECT t.*, o.title, c.title as category, p.publish as basic, a.name , s.title as varian
					FROM ccs_trx t, ccs_oct c, ccs_prv p, ccs_adm a, ccs_odc o, ccs_ocs s
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and s.ccs_id = t.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.category_id = ".$id."
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key =".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id != 0
					and t.createdby = a.admin_id 
					and t.varian_id = p.varian_id
					and s.settings_id = p.settings_id
				) as trxs order by trxs.datecreated DESC
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_product($ccs_id, $ccs_key){
			$q="SELECT * from (
					SELECT c.title as category, d.category_id, d.object_id, d.title, p.vstatus as varian_id, p.publish, p.stock, p.vstatus as varian  
					FROM ccs_oct c, ccs_odc d, ccs_prc p
					WHERE c.ccs_id = d.ccs_id
					and c.ccs_id = p.ccs_id
					and c.ccs_id = ".$ccs_id."
					and c.ccs_key = d.ccs_key
					and c.ccs_key = p.ccs_key
					and c.ccs_key = ".$ccs_key."
					and c.category_id = d.category_id
					and c.category_id = p.category_id
					and p.object_id = d.object_id
					and c.language_id = d.language_id
					and d.language_id = 2
					and c.status = d.status
					and c.status = 1
					and p.vstatus = 0
					UNION
					SELECT c.title as category, d.category_id, d.object_id, d.title, p.varian_id, p.publish, p.stock, s.title as varian
					FROM ccs_oct c, ccs_odc d, ccs_prv p, ccs_ocs s
					WHERE c.ccs_id = d.ccs_id
					and c.ccs_id = p.ccs_id
					and c.ccs_id = s.ccs_id
					and c.ccs_id = ".$ccs_id."
					and c.ccs_key = d.ccs_key
					and c.ccs_key = p.ccs_key
					and s.ccs_key = p.url_id
					and s.settings_id = p.settings_id
					and c.ccs_key = ".$ccs_key."
					and c.category_id = d.category_id
					and c.category_id = p.category_id
					and p.object_id = d.object_id
					and c.language_id = d.language_id
					and d.language_id = 2
					and c.status = d.status
					and c.status = 1					
				) as product order by product.object_id DESC
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_product_by_trx($ccs_id, $ccs_key, $url, $id){
			$q="Select * from (
					SELECT t.*, o.title, c.title as category, p.publish as dprice, a.name , p.vstatus as varian
					FROM ccs_trx t, ccs_oct c, ccs_prc p, ccs_adm a, ccs_odc o
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.trx_code = '".$id."'
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key = ".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id = 0
					and p.vstatus = 0
					and t.createdby = a.admin_id 
					UNION
					SELECT t.*, o.title, c.title as category, p.publish as basic, a.name , s.title as varian
					FROM ccs_trx t, ccs_oct c, ccs_prv p, ccs_adm a, ccs_odc o, ccs_ocs s
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and s.ccs_id = t.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.trx_code = '".$id."'
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key =".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id != 0
					and t.createdby = a.admin_id 
					and t.varian_id = p.varian_id
					and s.settings_id = p.settings_id
				) as trxs order by trxs.datecreated DESC
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_total_by_trx($ccs_id, $ccs_key, $url, $id){
			$q="Select sum(trxs.price) as total from (
					SELECT t.price
					FROM ccs_trx t, ccs_oct c, ccs_prc p, ccs_adm a, ccs_odc o
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.trx_code = '".$id."'
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key = ".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id = 0
					and p.vstatus = 0
					and t.createdby = a.admin_id 
					UNION
					SELECT t.price
					FROM ccs_trx t, ccs_oct c, ccs_prv p, ccs_adm a, ccs_odc o, ccs_ocs s
					where t.ccs_id = o.ccs_id
					and t.ccs_id = c.ccs_id
					and t.ccs_id = a.ccs_id
					and t.ccs_id = p.ccs_id
					and s.ccs_id = t.ccs_id
					and t.ccs_id = ".$ccs_id."
					and t.category_id = c.category_id
					and t.trx_code = '".$id."'
					and t.ccs_key = c.ccs_key
					and t.ccs_key = ".$ccs_key."
					and o.ccs_key = p.ccs_key
					and o.ccs_key =".$url."
					and o.object_id = t.object_id 
					and o.object_id = p.object_id
					and t.varian_id != 0
					and t.createdby = a.admin_id 
					and t.varian_id = p.varian_id
					and s.settings_id = p.settings_id
				) as trxs 
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_role($ccs_id, $ccs_key){
			$q="
				SELECT a.admin_id as object_id, a.email, a.name, a.createdby, a.role_id as type_id,  o.title as category, a.datecreated, a.status, o.category_id, o.parent_id
				FROM ccs_adm a, ccs_oct o 
				WHERE a.ccs_id = o.ccs_id
				and a.ccs_id = ".$ccs_id."
				and a.role_id = o.category_id
				and o.language_id = 2
				and o.ccs_key = ".$ccs_key."
				and a.status != 5
				and o.status = 1
				UNION
				SELECT u.user_id as object_id, u.email, u.name, a.name as createdby, u.type_id, o.title as category, u.datecreated, u.status, o.category_id, o.parent_id
				FROM ccs_usr u, ccs_adm a, ccs_oct o
				WHERE u.ccs_id = o.ccs_id
				and u.ccs_id = a.ccs_id
				and u.ccs_id = ".$ccs_id."
				and u.type_id = 2
				and o.language_id = 2
				and o.category_id = 5
				and u.reg_id = ''
				and o.ccs_key = ".$ccs_key."
				and u.createdby = a.admin_id
				and u.status != 5	
				and o.status = 1
				UNION
				SELECT u.user_id as object_id, u.email, u.name, a.name as createdby, u.type_id, o.title as category, u.datecreated, u.status, o.category_id, o.parent_id
				FROM ccs_usr u, ccs_adm a, ccs_oct o
				WHERE u.ccs_id = o.ccs_id
				and u.ccs_id = a.ccs_id
				and u.ccs_id = ".$ccs_id."
				and u.type_id = 2
				and o.language_id = 2
				and o.category_id = 6
				and u.reg_id != ''
				and o.ccs_key = ".$ccs_key."
				and u.createdby = a.admin_id
				and u.status != 5		
				and o.status = 1			
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
}
	