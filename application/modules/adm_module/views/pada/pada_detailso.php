
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn-sm btn btn-warning ">Back</button>
		</div>		
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		
		<div class="col-sm-12 " style="padding-bottom: 10px">
			<button class="btn btn-white btn-xs pull-left disabled" type="button" style="margin: 0 5px"><?php if($objects->language_id == 2){echo "ID"; }else{echo "EN";};?></button>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 0 5px">Content</button>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="seo_object" data-lang="2" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 0 5px">SEO</button>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="price_object" data-lang="2" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 0 5px">Price list</button>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="promo_object" data-lang="2" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 0 5px">Promotion</button>
			<?php foreach($status as $s){ ;?>
				<?php if($objects->status != $s->value){ ;?>
					<button id="<?php echo $objects->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $objects->title ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 0 5px"> <?php echo $s->name ;?></button>
				<?php ;} ;?>
				
			<?php ;} ;?>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" class="detail2 btn btn-info ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Edit</button>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="objects" data-title="<?php echo trim($objects->title);?>"data-lang="2" class="modal_dlt btn btn-white ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Delete</button>
		</div>
		<div class="row" style="margin-top: 25px !important">
			<div class="col-sm-3">
				<img alt="image" class="col-md-12 img-responsive" src="
				<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-right: auto; margin-left: auto">
			</div>			
			<div class="col-sm-9 col-xs-12">
				<div class="col-sm-12 col-xs-12">
					<h4 class="col-sm-3 col-xs-3">Name</h4>
					<div class="col-sm-9 col-xs-9"><h4><strong><?php echo $objects->title?></strong></h4></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<h4 class="col-sm-3 col-xs-3">Category</h4>
					<div class="col-sm-9 col-xs-9"><h4><strong><?php echo $category->title?><?php if($category->parent_id !=0){echo ", ".$category->parent_title; }?></strong></h4></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Status</p>
					<div class="col-sm-9 col-xs-3"><p><strong><?php if($objects->status == 1){ echo "Active" ;}elseif($objects->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Date Created</p>
					<div class="col-sm-9 col-xs-9"><p><?php echo date("l, d M Y", strtotime($objects->datecreated))  ;?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Creator</p>
					<div class="col-sm-9 col-xs-9"><p><?php echo $objects->name ;?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<?php if( strtolower($category->title) == 'banner'){ ;?>
						<p class="col-sm-3 col-xs-3">Description</p>
						<div class="col-sm-9 col-xs-9"><p><?php echo $objects->description ;?></p></div>
					<?php }else{ ;?>
						<p class="col-sm-12 col-xs-12">Description</p>
						<div class="col-sm-12 col-xs-12"><p><?php echo  $objects->description ;?></p></div>
					<?php } ;?>			
				</div>
			</div>
		</div>
	</div>
</div>