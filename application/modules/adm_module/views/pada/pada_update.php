<?php if($chiave == 'konten'){ ;?>
<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<?php if($objects->title != 'Banner'){ ;?>
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
	<?php } ;?>
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/chosen/chosen.css" rel="stylesheet">


<style>
	.modal-backdrop.in{
		display: none !important;
	}
	.fileinput-upload-button{
		display:none;
	}
	/* .input-group-btn > .btn-file{
		display:none;
	} */
	.checkbox label{
		padding-left:0px;
	}
</style>
<?php } ;?>
<div class="ibox">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-md-10 col-xs-10"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
			<?php if($chiave == 'konten'){ ;?>
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_object" data-lang="2" data-url3="konten" class="detail2">
					Edit Content
				</a>
			</li>
			<?php }else{ ;?>
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_object" data-lang="2" data-url3="seo" class="detail2">
					Edit SEO
				</a>
			</li>
			<?php } ;?>
		</ol>
		<?php if($chiave == 'konten'){ ;?>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php }else{ ;?>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php } ;?>
	</div>
	
	<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
	<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
	<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
	<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
	<?php if($chiave == 'seo'){ ;?>
	<div class="ibox-title row">
		<h5>SEO SETTINGS</h5>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>		
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Keywords</label>
				<div class="col-sm-10">
					<textarea id="metakeyword" name="inputan" type="text" class="form-control" value=""><?php echo $objects->metakeyword ;?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Description</label>
				<div class="col-sm-10">
					<textarea id="metadescription" name="inputan" type="text" class="form-control" value=""><?php echo $objects->metadescription ;?></textarea>
				</div>
			</div>
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="object" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Update</button>
				<div class="space-25"></div>		
					<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
			</div>
		</form>
	</div>
	<?php }else{  ;?>
	<div class="ibox-title row">
		<h5>Main Content</h5>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<div class="col-sm-4">
			<input id="image_square" name="image_square" class="file " type="file" value="">
		</div>
		<div class="col-sm-8">		
			<div class="form-horizontal">	
				<div class="form-group" >
					<br>
					<label class="col-sm-2 col-xs-4  control-label" style="text-align: left !important";>Category</label>
					<div class="col-sm-6 col-xs-8" >
						<div class="input-group col-sm-12 col-xs-12 " >
							<select class="form-control m-b" name="inputan" id="cat_id">		
								<option value="">Select Category</option>								
								<?php foreach($category as $cat){ ;?>
								<option value="<?php echo $cat->category_id ;?>" data-title="<?php echo strtolower($cat->title) ;?>"><?php echo $cat->title ;?></option>
									<?php if(isset($cat->child)){;?>
										<?php if(!empty($cat->child)){;?>
											<?php foreach($cat->child as $ct){;?>
											<option value="<?php echo $ct->category_id ;?>">- <?php echo $ct->title ;?> <?php echo "( ".$cat->title." )" ;?>  </option>
											<?php };?>
										<?php };?>
									<?php };?>								
								<?php } ;?>
							</select>
						</div>
					</div>
				</div>		
				<div class="form-group">
					<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Title</label>
					<div class="col-sm-10 col-xs-12 ">
						<input id="title" name="inputan" type="text" class="form-control" value="<?php echo $objects->title?>">
					</div>
				</div>		
				<div class="form-group">
					<label class="col-sm-2 col-xs-12  control-label" style="text-align: left !important">Tagline</label>
					<div class="col-sm-10 col-xs-12 " >
						<textarea id="tagline" name="inputan" type="text" class="form-control"><?php echo $objects->tagline?></textarea>
					</div>
				</div>
				<!-- FADHIL WAS HERE-->
				<div class="form-group">
					<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Tags</label>
					<div class="col-sm-10 col-xs-12 "> 
						<div class="input-group" id="create_tags">
							
						</div>
					</div>
				</div>
				<?php if( strtolower($objects->category) == 'banner'){?>
				<div class="form-group">
					<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Content</label>
					<div class="col-sm-10 col-xs-12 " >
						<textarea id="description" name="inputan" type="text" class="form-control"><?php echo trim($objects->description)?></textarea>
					</div>
				</div>
				<?php } ;?>
				<div class="form-group">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Weight</label>
					<div class="col-sm-3 col-xs-6 ">
						<input id="tu" name="inputan" type="text" class="form-control" value="<?php echo $objects->tu?>">
					</div>
					<p class="col-sm-1 col-xs-2" style="margin-top: 6px; text-align: left">Kg</p>
				</div>		
				<div class="form-group" >
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Publish</label>
					<div class="col-sm-4 col-xs-8" >
						<div class="input-group col-sm-12 col-xs-12 " >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="clearfix" id="data_3">
						<?php if($objects->status == 2){ ;?>
							<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Publish on</label>
							<div class="col-sm-4 col-xs-12">
								<div class="input-group date input-group col-sm-12 col-xs-12 ">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input id="date" name="inputan" data-date-format="dd/mm/yyyy" type="text" value ="<?php echo $objects->datepublish ;?>"class="form-control">
								</div>
							</div>
						<?php } ;?>
					</div>
					<div class="clearfix" id="data_2">
						<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Publish on</label>
						<div class="col-sm-4 col-xs-12">
							<div class="input-group date input-group col-sm-12 col-xs-12 ">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input id="datepublish" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if( strtolower($objects->category) != 'banner'){?>	
		<form class="form-horizontal" method="get">
			<div class="col-sm-12  col-xs-12">
				<div class="form-group">
					<label class="control-label" style="margin: 20px 0">Content</label>
					<div class="mail-box ">
						<div class="mail-text h-200" style="display: inline-block; width: 100%;">
							<div id="description" name="inputan_summer" class="summernote">
								<?php echo trim($objects->description)?>
							</div>
						</div>
				
						<div class="mail-body text-right tooltip-demo">
						</div>
					</div>
				</div>
			</div>
		</form>
		<?php } ;?>		
		<form class="form-horizontal" method="get">
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="object" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Update</button>
				<div class="space-25"></div>		
					<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
			</div>
			
		</form>
	</div>
	<?php } ;?>
</div>
<?php if($chiave == 'konten'){ ;?>
<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
	<?php if($objects->title != 'Banner'){ ;?>
	<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>
	<?php };?>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/chosen/chosen.jquery.js" type='text/javascript'></script>
<script>
$(document).ready(function(){
	$('#cat_id').val(<?php echo $objects->category_id;?>);
	$('#status').val(<?php echo $objects->status;?>);
	$('#data_2').hide();
	$('#data_2').hide();
	$('#status').change(function() {
		if ($(this).find(':selected').val() === '2') {
			$('#data_2').slideDown('slow');
			$('#data_3').slideUp('slow');
		} else {
			$('#data_2').slideUp('slow');
		}
		if ($(this).find(':selected').val() === '1') {
			$('#data_3').slideUp('slow');
		} else {
			$('#data_3').slideUp('slow');
		}
	});
	
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
	
	$('#datepicker').datepicker({
		format: 'yyyy-mm-dd',
		keyboardNavigation: false,
		forceParse: false
	});

	$('.date').datepicker({
		keyboardNavigation: false,
		todayBtn: 'linked',
		format: 'yyyy-mm-dd',
		forceParse: false
	});
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>' width='100%' class='file-preview-image'>"
		],			
		showRemove: false,
		showUpload: false
	});
	function f_create_tags(){
		var config = {
			".chosen-select"           : {},
			".chosen-select-deselect"  : {allow_single_deselect:true},
			".chosen-select-no-single" : {disable_search_threshold:10},
			".chosen-select-no-results": {no_results_text:"Oops, nothing found!"},
			".chosen-select-width"     : {width:"95%"}
		}
		for (var selector in config) {
			$(selector).chosen(config[selector]);
		}
	};
	v_tags_multiple = '<select id="tags" name="inputan" data-placeholder="Choose Tags..." class="chosen-select" multiple style="width:350px;" tabindex="4">'+
							'<option value="">Select</option><?php foreach($tags as $t){;?><option value="<?php echo $t->tags;?>"><?php echo $t->tags;?></option><?php };?>'+
						'</select>'+'<button class="create_tags btn btn-primary btn-sm"><i class="fa fa-edit"></i> create</button>';
	v_tags_manual = '<input id="tags" name="inputan" type="text" class="form-control">'+'<button class="c_create_tags btn btn-danger btn-sm form-control"><i class="fa fa-times-circle"></i> cancel</button>';
	$('#create_tags').append(v_tags_multiple);
	f_create_tags();
	$('#create_tags').delegate(".create_tags",'click',function(e){
		$('#create_tags').empty();
		$('#create_tags').append(v_tags_manual);
	});
	$('#create_tags').delegate(".c_create_tags",'click',function(e){
		$('#create_tags').empty();
		$('#create_tags').append(v_tags_multiple);
		f_create_tags();
	});
});
</script>

<?php } ;?>