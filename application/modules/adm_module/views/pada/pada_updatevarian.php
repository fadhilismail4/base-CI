<div class="ibox ">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-8"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="<?php echo $objects->ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->parent_title;?> 
				</a>
			</li>	
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
		</ol>		
		<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn-sm btn btn-warning pull-right">Back</button>			
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		<input id="url_id" name="inputan" type="text" class="form-control hide" value="<?php echo $trx->url_id ?>"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->ccs_key ?>"></input>
		<input id="varian_id" name="inputan" type="text" class="form-control hide" value="<?php echo $trx->varian_id ?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			
			<div class="col-sm-7 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Konten</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Price List</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Gallery</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="trx" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Transaction</button>
			</div>
			<div class="col-sm-5 col-xs-12 pull-right">
				<button id="<?php echo $trx->varian_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-lang="2" data-url3="pnp" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>	
			</div>
		</div>
		<div class="row " style="margin-top: 25px !important">
			<div class="col-sm-5 col-xs-12">
				<div class="col-sm-12 col-xs-12" style="margin-bottom: 20px">
					<img alt="image" class="img-responsive" src="
					<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;">
				</div>	
				<p class="col-sm-4 col-xs-3">Name</p>
				<div class="col-sm-8 col-xs-9"><p><strong><?php echo $objects->title?></strong> </p></div>
				<p class="col-sm-4 col-xs-3">Total Stock</p>
				<div class="col-sm-8 col-xs-9"><p><strong><?php echo ($price->stock - $price->sold)?> pc</p></strong> </div>
				<p class="col-sm-4 col-xs-3">Sold</p>
				<div class="col-sm-8 col-xs-9"><p><?php echo $price->sold ?> pc</p> </div>
				<p class="col-sm-4 col-xs-3">Basic Price</p>
				<div class="col-sm-8 col-xs-9"><p><?php echo  number_format($price->basic,2,',','.') ;?> Rupiah</p></div>
				<p class="col-sm-4 col-xs-3">Publish Price</p>
				<div class="col-sm-8 col-xs-9"><p><?php echo  number_format($price->publish,2,',','.');?> Rupiah</p></div>
			</div>
			<div class="col-sm-7 col-xs-12">
				<div class="form-horizontal">				
					<div class="form-group ">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important";>Product Varian</label>
						<div class="col-sm-8 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="select_to_select2 form-control m-b" name="inputan" data-url="module" data-url2="<?php echo $key_link?>" data-param="list_url" id="set">										
									<option value="">Select Product Varian</option>										
									<?php foreach($settings as $ct){ ;?>
									<option value="<?php echo $ct->varian_id ;?>"  data-url2="<?php echo $ct->ccs_key?>" data-url="<?php echo $key_link?>" >
										<?php echo $ct->varian ;?> ( <?php echo $ct->parent_title?> )
									</option>
									<?php  } ;?>
								</select>
							</div>
						</div>
					</div>
					
					<div class="clearfix" id="data_old">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Varian</label>
							<div class="col-sm-8 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="form-control m-b" name="inputan" id="settings_id">
										<option value="">Select Varian</option>										
										<?php foreach($varian as $v){ ;?>
										<option value="<?php echo $v->settings_id ;?>"  >
											<?php echo $v->title ;?></option>
										<?php  } ;?>
									</select>
								</div>
							</div>
						</div>	
					</div>
					<div class="clearfix" id="data_opr">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Basic Price</label>
							<div class="col-sm-8 col-xs-8">
								<input type="text" class="form-control" value="" disabled placeholder="Rp. <?php echo  number_format($price->basic,2,',','.');?>">	
							</div>
						</div>						
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Varian Price</label>
							<div class="col-sm-1 col-xs-2">
								<p style="font-size: 14px; padding-top: 8px">Rp.</p> 
							</div>
							<div class="col-sm-7 col-xs-6">
								<input id="oldprc" name="inputan"  type="text" class="form-control" value="<?php echo  number_format($trx->publish,2,',','.');?>">	
							</div>
						</div>				
					</div>							
					<div class="clearfix" id="data_stck">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Stock</label>
							<div class="col-sm-3 col-xs-6">
								<input id="stock" name="inputan"  type="text" class="form-control" value="<?php echo $trx->stock?>">
							</div>
							<div class="col-sm-1 col-xs-2">
								<p style="font-size: 14px; padding-top: 8px">pcs</p> 
							</div>
						</div>
					</div>
					<div class="clearfix" id="data_2">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Varian</label>
							<div class="col-sm-8 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="select_content2 form-control m-b" name="inputan" id="new">
									</select>
								</div>
							</div>
						</div>
					</div>	
					<div class="clearfix" id="data_new">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Price List</label>
							<div class="col-sm-8 col-xs-8">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="form-control m-b" name="inputan" id="prc">
										<option value="">Select Price</option>
										<option value="0">Default Price</option>
										<option value="1">New Price</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix" id="data_prc">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Default Price</label>
							<div class="col-sm-8 col-xs-8">
								<input type="text" class="form-control" value="" disabled placeholder="Rp. <?php echo  number_format($price->publish,2,',','.');?>">		
								<input id="basic" name="inputan" type="text" class="form-control hide" value="<?php echo $price->basic ?>"></input>			
							</div>
						</div>						
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Varian Price</label>
							<div class="col-sm-8 col-xs-8">
								<input id="publish2" name="inputan"  type="text" class="form-control" value="" placeholder="Publish price for this varian.">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Stock</label>
							<div class="col-sm-8 col-xs-8">
								<input id="newstock2" name="inputan"  type="text" class="form-control" value="" placeholder="Stock for this varian.">
							</div>
						</div>
					</div>
					<div class="clearfix" id="data_dflt">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Default Price</label>
							<div class="col-sm-8 col-xs-8">
								<input type="text" class="form-control" value="" disabled placeholder="Rp. <?php echo  number_format($price->basic,2,',','.');?>">	
							</div>
						</div>						
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Varian Price</label>
							<div class="col-sm-8 col-xs-8">
								<input type="text" class="form-control" value="" disabled placeholder="Rp. <?php echo  number_format($price->publish,2,',','.');?>">		
								<input id="publish" name="inputan" type="text" class="form-control hide" value="<?php echo $price->publish ?>"></input>		
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Stock</label>
							<div class="col-sm-8 col-xs-8">
								<input id="newstock" name="inputan"  type="text" class="form-control" value="" placeholder="Stock for this varian.">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; ">Status</label>
						<div class="col-sm-8 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Status</option>
									<?php foreach($status as $s){if($s->value != 2){ ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php };}?>
								</select>
							</div>
						</div>	
					</div>	
					<div class="form-group">	
						<div class="col-sm-12 col-xs-12">
							<button id="<?php echo $key_link;?>"  data-param="settings" data-param2="pnp" class="create_mdl btn btn-primary btn-md col-sm-3 col-xs-12 pull-right"><i class="fa fa-check-square"></i>  Update</button>	
							<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>	
	$('#set').val(<?php echo $trx->category;?>);
	$('#settings_id').val(<?php echo $trx->settings_id;?>);
	$('#status').val(<?php echo $trx->status;?>);
	$('#data_2').hide();
	$('#data_new').hide();		
	$('#data_prc').hide();	
	$('#data_dflt').hide();			
	$(document).ready(function(){
		$('#settings_id').change(function() {
			if ($(this).find(':selected').val() != '<?php echo $trx->settings_id;?>') {
				$('#data_new').slideDown('slow');
				$('#data_opr').slideUp('slow');
				$('#data_stck').slideUp('slow');
				$('#data_prc').slideUp('slow');
				$('#data_dflt').slideUp('slow');
			} else {
				$('#data_new').slideUp('slow');
				$('#data_old').slideDown('slow');
				$('#data_opr').slideDown('slow');
				$('#data_stck').slideDown('slow');
			}
			if ($(this).find(':selected').val() == '<?php echo $trx->settings_id;?>' ) {
				$('#data_new').slideUp('slow');
				$('#data_old').slideDown('slow');
				$('#data_opr').slideDown('slow');
				$('#data_stck').slideDown('slow');
				$('#data_prc').slideUp('slow');
				$('#data_dflt').slideUp('slow');
			}
			if ($(this).find(':selected').val() == '' ) {
				$('#var').val(<?php echo $trx->settings_id;?>);
				$('#data_new').slideUp('slow');
				$('#data_old').slideDown('slow');
				$('#data_opr').slideDown('slow');
				$('#data_stck').slideDown('slow');
				$('#data_prc').slideUp('slow');
				$('#data_dflt').slideUp('slow');
			}
		});
		$('#prc').change(function() {
			if ($(this).find(':selected').val() == '0') {
				$('#data_dflt').slideDown('slow');
			} else {		
				$('#data_dflt').slideUp('slow');		
			}
			if ($(this).find(':selected').val() == '1') {
				$('#data_prc').slideDown('slow');
			} else {
				$('#data_prc').slideUp('slow');
			}
			if ($(this).find(':selected').val() == '') {
				$('#data_prc').slideUp('slow');
				$('#data_dflt').slideUp('slow');
				$('#prc').val('');	
			} 
		});
		$('#set').change(function() {
			if ($(this).find(':selected').val() != '<?php echo $trx->category?>') {
				$('#data_2').slideDown('slow');
				$('#data_new').slideDown('slow');
				$('#data_opr').slideUp('slow');
				$('#data_old').slideUp('slow');
				$('#data_stck').slideUp('slow');
				$('#data_prc').slideUp('slow');
				$('#data_dflt').slideUp('slow');
			} else {
				$('#data_2').slideUp('slow');
				$('#data_new').slideUp('slow');
				$('#data_opr').slideDown('slow');
				$('#data_old').slideDown('slow');
				$('#data_stck').slideDown('slow');
			}
			if ($(this).find(':selected').val() == '') {
				$('#data_2').slideUp('slow');
				$('#settings_id').val(<?php echo $trx->settings_id;?>);
				$('#data_new').slideUp('slow');
				$('#data_opr').slideDown('slow');
				$('#data_old').slideDown('slow');
				$('#data_stck').slideDown('slow');
			} 
			if ($(this).find(':selected').val() == '<?php echo $trx->category?>') {
				$('#data_2').slideUp('slow');
				$('#settings_id').val(<?php echo $trx->settings_id;?>);
				$('#data_new').slideUp('slow');
				$('#data_opr').slideDown('slow');
				$('#data_old').slideDown('slow');
				$('#data_stck').slideDown('slow');
			}
		});
	});
</script>