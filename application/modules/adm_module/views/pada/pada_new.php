<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-9" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_name);?>				
				</a>
			</li>
			<?php if($id != 0){ ;?>
				<?php if($cat->parent_id != 0){ ;?>
				<li class="">
					<a id="<?php echo $cat->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
						<?php echo $cat->parent_title ;?>
					</a>
				</li>
				<?php } ;?>
				<li class="">
					<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
						<?php echo $cat->title ;?>
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
						New
					</a>
				</li>
			<?php }else{ ;?>
				<li class="active">
					<a id="0" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
						New
					</a>
				</li>
			<?php } ;?>
		</ol>
		<?php if ($id == 0){ ;?>
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
		<?php }else{ ;?>
			<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
		<?php } ;?>
	</div>
	<div class="ibox-content row">
		<div class=" col-sm-4 col-xs-12" style="margin-bottom: 20px">
			<input id="image_square" name="image_square" class="file " type="file" value="">
		</div>
		<div class=" col-sm-8 col-xs-12">		
			<div class="form-horizontal">
				<?php if(isset($is_page)){;?>
					<?php if($is_page){;?>
						<div class="form-group">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Type</label>
							<div class="col-sm-10 col-xs-8">
								<div class="input-group col-sm-6 col-xs-12" >
									<select class="form-control m-b" name="inputan" id="type">	
										<option value="0">Select Type</option>
										<option value="1">Page</option>
										<option value="2">Product Item</option>
									</select>
								</div>
							</div>
						</div>
						<script>
						$(document).ready(function(){
							$('#price').hide();
							$('#stock_1').hide();
							$('#stock_2').hide();
							$('#weight').hide();
							$('#weight2').hide();
							$('#type').change(function() {
								if ($(this).find(':selected').val() === '2') {
									$('#price').slideDown();
									$('#stock_1').slideDown();
									$('#stock_2').slideDown();
									$('#weight').slideDown();
									$('#weight2').slideDown();
								} else {
									$('#price').slideUp();
									$('#stock_1').slideUp();
									$('#stock_2').slideUp();
									$('#weight').slideUp();
									$('#weight2').slideUp();
								}
							});
						});
						</script>
					<?php };?>
				<?php };?>
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Category</label>
					<div class="col-sm-10 col-xs-8">
						<div class="input-group col-sm-6 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="category_id">	
								<?php if(isset($cat->parent_id)){ ;?>
									<?php if(($id == 0)&&($cat->parent_id == 0)){ ;?>
										<option value="">Select Category</option>	
									<?php }elseif(($id != 0)&&($cat->parent_id == 0)){ ;?>
										<option value="">Select Category</option>	
									<?php  } ;?>
								<?php }else{ ;?>
								<option value="">Select Category</option>	
								<?php };?>
								<?php foreach($category as $ct){ ;?>
								<option value="<?php echo $ct->category_id ;?>"><?php echo $ct->title ;?> <?php if($ct->parent_id != 0){ echo "( ".$ct->parent_title." )" ;} ;?>  </option>
								<?php  } ;?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Title</label>
					<div class="col-sm-10 col-xs-8">
						<input id="title" name="inputan" type="text" class="form-control" value="" placeholder="A short title but full of information [Only 100 Characters]">
					</div>
				</div>				
				<div class="form-group">
					<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Tagline</label>
					<div class="col-sm-10 col-xs-12 "><textarea id="tagline" name="inputan" type="text" class="form-control" placeholder="This is your promotion tagline, make sure to use a better tagline to engage your visitor."></textarea></div>
				</div>		
				<!-- FADHIL WAS HERE-->
				<div class="form-group">
					<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Tags</label>
					<div class="col-sm-10 col-xs-12 "> 
						<div class="input-group" id="create_tags">
							
						</div>
					</div>
				</div>
			
				<div id="price" class="form-group">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Basic Price</label>
					<div class="col-sm-4 col-xs-8" >
						<input id="basic" name="inputan" type="text" class="form-control" value="" placeholder="Rupiah">
					</div>
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Publish Price</label>
					<div class="col-sm-4 col-xs-8">
						<input id="publish" name="inputan" type="text" class="form-control" value="" placeholder="Rupiah">
					</div>
				</div>
				<div class="form-group ">
					<label id="stock_1" class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Stock</label>
					<div id="stock_2" class="col-sm-4 col-xs-8">
						<input id="stock" name="inputan" type="text" class="form-control" value="" placeholder="Total Stock">
					</div>		
					<label id="weight" class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Weight</label>
					<div id="weight2" class="col-sm-4 col-xs-8">
						<input id="tu" name="inputan" type="text" class="form-control" value="" placeholder="Weight ( Ex: 0.2 )">
					</div>		
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Publish</label>
					<div class="col-sm-4 col-xs-8"  >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="clearfix" id="data_2">	
						<label class="control-label col-sm-2" style="text-align: left !important; ">Publish On</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group date input-group col-sm-12 col-xs-12">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input id="datecreated" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-sm-12 col-xs-12">			
					<br>
					<button id="<?php echo $key_link;?>" data-param="object" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Create</button>				
					<?php if ($id == 0){ ;?>
						<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					<?php }else{ ;?>
						<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					<?php } ;?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ibox float-e-margins">
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
		<form class="form-horizontal" method="get">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Content</label>
			</div>
			<div class="mail-box">
				<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
					<div id="description" name="inputan_summer" class="summernote">
						<span style="font-family: 'Courier New';">This is an example for your description page, a lot of space here. Feel free to write your content . <span style="font-weight: bold;">Cheers</span></span>											
					</div>
				</div>
			
				<div class="mail-body text-right tooltip-demo">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<h5>SEO SETTINGS</h5>
	</div>
	<div class="ibox-content row">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Keywords</label>
				<div class="col-sm-10">
					<textarea id="metakeyword" name="inputan" type="text" class="form-control" value="" placeholder="Example : Author: J. K. Rowling, Illustrator: Mary GrandPré, Category: Books, Price: $17.99, Length: 784 pages [ Only 100 characters ]"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Description</label>
				<div class="col-sm-10">
					<textarea id="metadescription" name="inputan" type="text" class="form-control" value="" placeholder="Example : Make sure that every page on your site has a meta description. [ Only 200 characters ]"></textarea>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/chosen/chosen.jquery.js" type='text/javascript'></script>
<script>
	$(document).ready(function(){
		$('#data_2').hide();
		$('#status').change(function() {
			if ($(this).find(':selected').val() === '2') {
				$('#data_2').slideDown('slow');
			} else {
				$('#data_2').slideUp('slow');
			}
		});
		function f_create_tags(){
			var config = {
				".chosen-select"           : {},
				".chosen-select-deselect"  : {allow_single_deselect:true},
				".chosen-select-no-single" : {disable_search_threshold:10},
				".chosen-select-no-results": {no_results_text:"Oops, nothing found!"},
				".chosen-select-width"     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}
		};
		v_tags_multiple = '<select id="tags" name="inputan" data-placeholder="Choose Tags..." class="chosen-select" multiple style="width:350px;" tabindex="4">'+
								'<option value="">Select</option><?php foreach($tags as $t){;?><option value="<?php echo $t->tags;?>"><?php echo $t->tags;?></option><?php };?>'+
							'</select>'+'<button class="create_tags btn btn-primary btn-sm"><i class="fa fa-edit"></i> create</button>';
		v_tags_manual = '<input id="tags" name="inputan" type="text" class="form-control">'+'<button class="c_create_tags btn btn-danger btn-sm form-control"><i class="fa fa-times-circle"></i> cancel</button>';
		$('#create_tags').append(v_tags_multiple);
		f_create_tags();
		$('#create_tags').delegate(".create_tags",'click',function(e){
			$('#create_tags').empty();
			$('#create_tags').append(v_tags_manual);
		});
		$('#create_tags').delegate(".c_create_tags",'click',function(e){
			$('#create_tags').empty();
			$('#create_tags').append(v_tags_multiple);
			f_create_tags();
		});
	});
</script>
<script>
$(document).ready(function(){
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
	
	$('.date').datepicker({
		keyboardNavigation: false,
		todayBtn: 'linked',
		format: 'yyyy-mm-dd',
		forceParse: false
	});
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php echo base_url('assets').'/img/image_empty.png' ;?>' class='file-preview-image'>"
		],			
		showRemove: false,
		showUpload: false
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>