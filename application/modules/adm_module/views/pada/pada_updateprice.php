
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-8"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->parent_title;?> 
				</a>
			</li>	
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button  id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
		</div>		
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->ccs_key?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			
			<div class="col-sm-7 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Basic Info</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Price List</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Gallery</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="trx" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Transaction</button>
			</div>
			<div class="col-sm-5 col-xs-12 pull-right">
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="price" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
			</div>
		</div>
		<div class="row" style="margin-top: 25px !important">
			<div class="col-sm-3">
				<img alt="image" class="col-sm-12 img-responsive" src="
				<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-right: auto; margin-left: auto">
			</div>			
			<div class="col-sm-9 col-xs-12">
				<div class="form-horizontal">					
					<div class="form-group">
						<p class="col-sm-2 col-xs-3" style="text-align: left !important">Name</p>
						<div class="col-sm-10 col-xs-9"><p ><strong><?php echo $objects->title?></strong> </p></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important">Basic Price</label>
						<div class="col-sm-1 col-xs-2">
							<p style="font-size: 14px; padding-top: 8px">Rp.</p> 
						</div>
						<div class="col-sm-3 col-xs-7">
							<input id="basic" name="inputan" type="text" class="form-control" value="<?php echo  number_format($price->basic,2,',','.');?>">
						</div>
						<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important">Publish Price</label>
						<div class="col-sm-1 col-xs-2">
							<p style="font-size: 14px; padding-top: 8px">Rp.</p> 
						</div>
						<div class="col-sm-3 col-xs-7">
							<input id="publish" name="inputan" type="text" class="form-control" value="<?php echo  number_format($price->publish,2,',','.');?>">
						</div>
					</div>
					<?php if($var ==1 ){ ;?>
						<div class="form-group">
							<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important">Stock</label>
							<div class="col-sm-3 col-xs-7">
								<input type="text" class="form-control" value="<?php echo $stock ;?>" disabled />	
							</div>
							<div class="col-sm-1 col-xs-2">
								<p style="font-size: 14px; padding-top: 8px">pcs</p> 	
								<input id="stock" name="inputan" type="text" class="form-control hide" value="0"></input>		
							</div>							
							<div class="col-sm-6 col-xs-12">
								<p style="font-size: 14px; padding-top: 8px; color: #f8ac59">You can edit from varian stock.</p> 
							</div>
						</div>				
					<?php }else{ ;?>
						<div class="form-group">
							<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important">Stock</label>
							<div class="col-sm-3 col-xs-7">
								<input id="stock" name="inputan" type="text" class="form-control" value="<?php echo $stock ;?>">	
							</div>
							<div class="col-sm-1 col-xs-2">
								<p style="font-size: 14px; padding-top: 8px">pcs</p> 
							</div>
						</div>		
					<?php } ;?>
					<div class="form-group">	
						<div class="col-sm-12 col-xs-12">
							<button id="<?php echo $key_link;?>"  data-param="object" data-param2="price" class="create_mdl btn btn-primary btn-md col-sm-3 col-xs-12 pull-right"><i class="fa fa-check-square"></i>  Update</button>	
							<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>