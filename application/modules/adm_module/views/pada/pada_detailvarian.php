
<div class="ibox">
	<div class="ibox-title row" >
		<ol class="breadcrumb col-md-7 col-xs-8"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->parent_title;?> 
				</a>
			</li>	
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
		</ol>
		<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn-sm btn btn-warning pull-right">Back</button>		
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		<input id="url_id" name="inputan" type="text" class="form-control hide" value="<?php echo $key ?>"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key ?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			
			<div class="col-sm-7 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Basic Info</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Price List</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Gallery</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="trx" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Transaction</button>
			</div>
			<div class="col-sm-5 col-xs-12 pull-right">
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="pnp" class="detail2 btn btn-primary btn-xs pull-right" type="button" style="margin: 5px">Add Varian Price</button>
			</div>
		</div>
		<div class="row " style="margin-top: 10px !important">
			<div class="col-sm-5 col-xs-12">
				<div class="col-sm-12 col-xs-12" style="margin-bottom: 20px">
					<img alt="image" class="img-responsive" src="
					<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block;max-height: 200px;width: auto;">
				</div>	
				<h4 class="col-sm-9 col-xs-12" style="text-align: center">BASIC INFORMATION</h4>
				<p class="col-sm-4 col-xs-3">Name</p>
				<div class="col-sm-8 col-xs-9"><p><strong><?php echo $objects->title?></strong> </p></div>
				<p class="col-sm-4 col-xs-3">Total Stock</p>
				<div class="col-sm-8 col-xs-9"><p><strong><?php echo ($price->stock - $price->sold)?> pc</p></strong> </div>
				<p class="col-sm-4 col-xs-3">Sold</p>
				<div class="col-sm-8 col-xs-9"><p><?php echo $price->sold ?> pc</p> </div>
				<p class="col-sm-4 col-xs-3">Basic Price</p>
				<div class="col-sm-8 col-xs-9"><p><?php echo  number_format($price->basic,2,',','.') ;?> Rupiah</p></div>
				<p class="col-sm-4 col-xs-3">Publish Price</p>
				<div class="col-sm-8 col-xs-9"><p><?php echo  number_format($price->publish,2,',','.');?> Rupiah</p></div>
			</div>
			<div class="col-sm-7 col-xs-12">
				<div class="form-horizontal">				
					<div class="form-group ">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important";>Product Varian</label>
						<div class="col-sm-8 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="select_to_select2 form-control m-b" name="inputan" data-url="module" data-url2="<?php echo $key_link?>" data-param="list_url" id="var">										
									<option value="">Select Product Varian</option>										
									<?php foreach($varian as $ct){ ;?>
									<option value="<?php echo $ct->varian_id ;?>"  data-url2="<?php echo $ct->ccs_key?>" data-url="<?php echo $key_link?>" >
										<?php echo $ct->varian ;?> ( <?php echo $ct->parent_title?> )
									</option>
									<?php  } ;?>
								</select>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="clearfix" id="data_2">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Varian</label>
							<div class="col-sm-8 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="select_content2 form-control m-b" name="inputan" id="settings_id">
									</select>
								</div>
							</div>
						</div>
					</div>				
					<div class="clearfix" id="data_3">	
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Price List</label>
							<div class="col-sm-8 col-xs-8">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="form-control m-b" name="inputan" id="prc">
										<option value="">Select Price</option>
										<option value="0">Default Price</option>
										<option value="1">New Price</option>
									</select>
								</div>
							</div>
						</div>
					</div>
						
					<div class="clearfix" id="data_4">	
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Basic Price</label>
							<div class="col-sm-8 col-xs-8">
								<input type="text" class="form-control" value="" disabled placeholder="Rp. <?php echo  number_format($price->basic,2,',','.');?>">			
								<input id="basic" name="inputan" type="text" class="form-control hide" value="<?php echo $price->basic ?>"></input>		
							</div>
						</div>						
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Publish Price</label>
							<div class="col-sm-8 col-xs-8">
								<input type="text" class="form-control" value="" disabled placeholder="Rp. <?php echo  number_format($price->publish,2,',','.');?>">			
								<input id="publish" name="inputan" type="text" class="form-control hide" value="<?php echo $price->publish ?>"></input>		
							</div>
						</div>
					</div>
					<div class="clearfix" id="data_5">	
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Default Price</label>
							<div class="col-sm-8 col-xs-8">
								<input type="text" class="form-control" value="" disabled placeholder="Rp. <?php echo  number_format($price->publish,2,',','.');?>">	
							</div>
						</div>						
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Varian Price</label>
							<div class="col-sm-8 col-xs-8">
								<input id="publish2" name="inputan"  type="text" class="form-control" value="" placeholder="Publish price for this varian.">
							</div>
						</div>
					</div>
					
					<div class="clearfix" id="data_6">	
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Stock</label>
							<div class="col-sm-8 col-xs-8">
								<input id="stock" name="inputan"  type="text" class="form-control" value="" placeholder="Stock for this varian.">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; ">Status</label>
						<div class="col-sm-8 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Status</option>
									<?php foreach($status as $s){if($s->value != 2){ ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php };}?>
								</select>
							</div>
						</div>	
					</div>	
					<div class="form-group">	
						<div class="col-sm-12 col-xs-12">
							<button id="<?php echo $key_link;?>" data-param="settings" data-param2="pnp" class="create_mdl btn btn-primary btn-md col-sm-3 col-xs-12 pull-right"><i class="fa fa-check-square"></i>  Create</button>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#data_2').hide();
		$('#data_3').hide();
		$('#data_4').hide();
		$('#data_5').hide();		
		$('#data_6').hide();
		$('#var').change(function() {
			if ($(this).find(':selected').val() != '') {
				$('#data_2').slideDown('slow');
				$('#data_3').slideDown('slow');
			} else {
				$('#data_2').slideUp('slow');
				$('#data_3').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#data_4').slideUp('slow');
				$('#var').val('');	
				$('#prc').val('');	
			}
			if ($(this).find(':selected').val() == '') {
				$('#data_2').slideUp('slow');
				$('#data_3').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#data_4').slideUp('slow');
				$('#var').val('');	
				$('#prc').val('');	
			} 
		});
		$('#prc').change(function() {
			if ($(this).find(':selected').val() == '0') {
				$('#data_4').slideDown('slow');
				$('#data_6').slideDown('slow');
			} else {		
				$('#data_4').slideUp('slow');		
			}
			if ($(this).find(':selected').val() == '1') {
				$('#data_5').slideDown('slow');
				$('#data_6').slideDown('slow');
			} else {
				$('#data_5').slideUp('slow');
			}
			if ($(this).find(':selected').val() == '') {
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#prc').val('');	
			} 
		});
	});
</script>