
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-8"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<?php if($category->parent_id != 0){ ;?>
			<li class="">
				<a id="<?php echo $category->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->parent_title;?> 
				</a>
			</li>	
			<?php } ;?>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>	
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="price" class="detail2">
					Price List
				</a>
			</li>
		</ol>
		<button  id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
			
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			
			<div class="col-sm-7 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Basic Info</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="price" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Price List</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Gallery</button>
				<!--
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="trx" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Transaction</button>
				-->
			</div>
			<div class="col-sm-5 col-xs-12 pull-right">
				<?php if($key == 1){ ;?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="pnp" class="detail2 btn btn-primary btn-xs pull-right" type="button" style="margin: 5px">Add Varian Price</button>
				<?php };?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="price" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
			</div>
		</div>
		<div class="row" style="margin-top: 25px !important">
			<div class="col-sm-3">
				<img alt="image" class="col-sm-12 img-responsive" src="
				<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-right: auto; margin-left: auto; margin-bottom: 20px">
			</div>			
			<div class="col-sm-9 col-xs-12">
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Name</p>
					<div class="col-sm-9 col-xs-9"><p>: <strong><?php echo $objects->title?></strong> </p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Total Stock</p>
					<div class="col-sm-9 col-xs-9"><p>: <strong><?php echo ($stock - $sold)?> pc</p></strong> </div>
				</div>
					<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Sold</p>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo $sold ?> pc</p> </div>
				</div>
				
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Basic Price</p>
					<div class="col-sm-9 col-xs-9"><p>: Rp.<?php echo  number_format($price->basic,2,',','.') ;?></p></div>
				</div>
				<?php if($price->dstatus == 0){ ;?>
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Publish Price</p>
					<div class="col-sm-9 col-xs-9"><p>: Rp.<?php echo  number_format($price->publish,2,',','.');?></p></div>
				</div>
				<?php }elseif(($price->dstatus == 1) && ($price->vstatus == 0) ){ ;?>
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Discount</p>
					<div class="col-sm-9 col-xs-9">
						<p>: 
							<?php echo $discount->discount?>% 
							<strong><?php if($discount->stock == 0){ echo "Stock in promo already sold out. " ;} ;?></strong>
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<p class="col-sm-3 col-xs-3">Publish Price</p>
					<div class="col-sm-9 col-xs-9">
						: 
						<?php if($discount->stock != 0){ ;?>
						<span style="color:#f39c12;text-decoration:line-through; margin-right: 10px">
							<span style="color:#f39c12">Rp.<?php echo  number_format($price->publish,2,',','.');?>  </span>
						</span>
						<span>
							<strong>
								  Rp.<?php echo number_format(($price->publish -($price->publish *($discount->discount / 100))),2,',','.') ;?>
							</strong>
						</span>
						<?php }else{ ;?>
							 Rp.<?php echo  number_format($price->publish,2,',','.');?>
						<?php };?>						
					</div>
				</div>
				<?php };?>				
			</div>
		</div>
		<div class="row col-sm-12 col-xs-12" style="margin-top: 20px">
		<?php if($c_trx != 0){  ;?>
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th  style="text-align: center">Varian </th>
						<th>Stock</th>
						<th>Sold</th>
						<th  style="text-align: center">Basic</th>
						<th  style="text-align: center">Publish</th>
						<th>Discount</th>
						<!-- <th>Date Created </th>-->
						<th>Status</th>
						<th>Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($trx as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<strong><?php echo $n->title ?> Varian </strong>( <?php echo $n->category ;?> Varian on <?php echo $n->pcat ;?> )
						</td>	
						<td><?php echo ($n->stock - $n->sold) ?> pcs</td>
						<td><?php echo $n->sold ?> pcs</td>
						<td>Rp.<?php echo number_format($n->basic,0, ',','.') ?>,-</td>
						<?php if($n->dstatus == 1){ ;?>
							<td>
								<?php if($n->tstock != 0){ ;?>
								<span>
									<strong>
										  Rp.<?php echo number_format(($n->publish -($n->publish *($n->discount / 100))),2,',','.') ;?>
									</strong>
								</span>
								<span style='color:#f39c12;text-decoration:line-through'>
								  <span style='color:#f39c12'>Rp.<?php echo  number_format($n->publish,2,',','.');?></span>
								</span>
								<?php }else{ ;?>
									Rp.<?php echo number_format($n->publish,2, ',','.') ?>
								<?php } ;?>
							</td>
							<td>
								<?php echo $n->discount ?>%<br> Stock: <?php if($n->tstock != 0){ echo $n->tstock." pcs more" ;}else{ echo " Sold out.";}?>
							</td>
						<?php }else{ ;?>
							<td>Rp.<?php echo number_format($n->publish,2, ',','.') ?></td>
							<td>No Discount </td>
						<?php } ;?>
						<!--<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>-->
						<td>
							<?php foreach($status as $s){ if($s->value == $n->status){ echo $s->info;} ; } unset($s);?>
						</td>
						<td>
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ if($s->value != 2){  ;?>
									<button id="<?php echo $n->varian_id ?>" data-url="varian" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?> Varian" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 2px 1px"> <?php echo $s->name ;?></button>
								<?php ;} ;};?>
							<?php ;} ;?>
							<button id="<?php echo $n->varian_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-url3="pnp" data-lang="2" class="dtl btn btn-info btn-xs pull-right" type="button" style="margin: 2px 1px">Edit</button>
							<button  id="<?php echo $n->varian_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="varian" data-title="<?php echo trim($n->title);?> Varian"data-lang="2" class="modal_dlt btn btn-white btn-xs pull-right" type="button" style="margin: 2px 1px">Delete</button>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		<?php };?>
		</div>
	</div>
</div>