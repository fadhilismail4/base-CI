<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<?php if($category->category_id == 1){ ;?>	
				<?php if($category->parent_id == 0){ ;?>
				<li class="">
					<a id="<?php echo $category->category_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-url3="admin"  data-lang="2" class="detail2">
						<?php echo $category->title?>
					</a>
				</li>
				<?php }else{ ;?>
				<li class="">
					<a id="<?php echo $category->parent_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-url3="admin"  data-lang="2" class="detail2">
						<?php echo $category->parent_title?>
					</a>
				</li>
				<li class="">
					<a id="<?php echo $category->category_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-url3="admin"  data-lang="2" class="detail2">
						<?php echo $category->title?>
					</a>
				</li>
				<?php } ;?>
			<?php }else{ ;?>
				<?php if($category->parent_id == 0){ ;?>
				<li class="">
					<a id="<?php echo $category->category_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-url3="member"  data-lang="2" class="detail2">
						<?php echo $category->title?>
					</a>
				</li>
				<?php }else{ ;?>
				<li class="">
					<a id="<?php echo $category->parent_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-url3="member"  data-lang="2" class="detail2">
						<?php echo $category->parent_title?>
					</a>
				</li>
				<li class="">
					<a id="<?php echo $category->category_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-url3="member"  data-lang="2" class="detail2">
						<?php echo $category->title?>
					</a>
				</li>
				<?php } ;?>
			<?php } ;?>
			<li class="active">
				<a id="<?php echo $category->category_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_category" data-url3="<?php if(($category->parent_id == 1)||($category->category_id == 1)){echo "admin";}else{echo "member";} ;?>"  data-lang="2" class="detail2">
					Update
				</a>
			</li>
		</ol>
		<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_category" data-url3="<?php if(($category->parent_id == 1)||($category->category_id == 1)){echo "admin";}else{echo "member";} ;?>"  data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right">Back</button>
			
	</div>
	<div class="ibox-content row">
		<?php if($chiave == 'child'){ ;?>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 col-xs-4 control-label">Role Name</label>
				<div class="col-sm-4 col-xs-8"><input id="ttl" name="inputan" type="text" class="form-control" placeholder="<?php echo $category->title;?>" disabled></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label">Description</label>
				<div class="col-sm-10 col-xs-12"><textarea id="description" name="inputan" type="text" class="form-control"><?php echo $category->description?></textarea></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>		
			<input id="module_id" name="inputan" type="text" class="form-control hide" value=""></input>
			<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $category->ccs_key?>"></input>
			<input id="name" name="inputan" type="text" class="form-control hide" value="<?php echo strtolower(str_replace(' ','',$category->title)) ?>"></input>
			<input id="type_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
			<div id="language" class="form-group col-sm-12">	
				<?php 
				foreach($module as $c){;?>
					<div class="col-sm-3" style="padding: 10px 0">
						<div class="col-sm-2">
							<input id="<?php echo $c->module_id ;?>" type="checkbox" class="i-checks">
						</div>
						<div class="col-sm-9" style="">
							<i class="fa fa-<?php echo $c->icon;?>"></i> <?php echo $c->name?>
						</div>
					</div>
				<?php };?>
			</div>			
			<div class="form-group col-sm-12">
				<div class="hr-line-dashed"></div>			
				<div class="col-md-12">
					<button id="<?php echo $key_link;?>" data-param="<?php echo $key_link;?>" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Update</button>
					<button id="all" data-url="module" data-url2="role" data-param="list" data-lang="2" class="detail2 btn btn-md btn-white pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
		<?php }elseif($chiave == 'parent'){ ;?>
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>		
			<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $category->ccs_key?>"></input>
			<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
			<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
			<input id="parent_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->parent_id?>"></input>
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 col-xs-4 control-label">Category Name</label>
					<div class="col-sm-4 col-xs-8"><input id="title" name="inputan" type="text" class="form-control" value="<?php echo $category->title;?>" ></input></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-xs-12 control-label">Description</label>
					<div class="col-sm-10 col-xs-12"><textarea id="description" name="inputan" type="text" class="form-control"><?php echo $category->description?></textarea></div>
				</div>	
				<div class="col-md-12 col-xs-12">
					<button id="<?php echo $key_link;?>" data-param="category" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Update</button>
					<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_category" data-lang="2" data-url3="<?php if(($category->parent_id == 1)||($category->category_id == 1)){echo "admin";}else{echo "member";} ;?>"  class="detail2 btn btn-md btn-white pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
			</form>
		<?php };?>
	</div>
</div>
<?php if($chiave == 'child'){ ;?>
<script>
$(document).ready(function(){
	$('#language input').each(function(){
		if(jQuery.inArray($(this).attr('id'), [<?php foreach($categories as $c){ foreach($c['module'] as $cc){ echo '"'.$cc->module_id.'",';};};?>]) !== -1){
			$(this).parent().addClass('checked');
			$(this).prop('checked', true);
			brands();
		}
	});
	
	
	$("#language input").on("ifToggled", function (event) {
		$(".create_mdl").slideDown();
		brands();
	});
	
	function brands() {
		var brands = [];
		$("#language input").each(function (index, value) {
			if ($(this).is(":checked")) {
				brands.push($(this).attr("id"));
			}
		});
		console.log(brands);
		$("#module_id").val(brands);
		if(!brands.length){
			$(".create_mdl").slideUp();
		}
	}
});
</script>
<?php };?>