
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-10 col-xs-10"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<?php if($chiave == 'admin'){ ;?>
			<li class="">
				<a id="<?php echo $objects->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->parent_title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list"  data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>	
			<li class="active">
				<a id="<?php echo $objects->admin_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave ;?>" class="detail2">
					<?php echo $objects->name;?> 
				</a>
			</li>
			<?php }else{ ;?>
			<li class="">
				<a id="<?php echo $category->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->parent_title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list"  data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>	
			<li class="active">
				<a id="<?php echo $objects->user_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave ;?>" class="detail2">
					<?php echo $objects->name;?> 
				</a>
			</li>
			<?php } ;?>		
			
		</ol>
		<?php if($chiave == 'admin'){ ;?>
		<button id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" data-url3="<?php echo $chiave ;?>" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php }else{ ;?>
		<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" data-url3="<?php echo $chiave ;?>" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php } ;?>
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			<div class="col-sm-6 col-xs-12">
			</div>
			<div class="col-sm-6 col-xs-12">
			<?php if($chiave == 'admin'){ ;?>
				<button id="<?php echo $objects->admin_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="user" data-title="<?php echo trim($objects->name);?>"data-lang="<?php echo $chiave ;?>" class="modal_dlt btn btn-white  btn-xs pull-right" type="button" style="margin: 5px">Delete</button>
				<button id="<?php echo $objects->admin_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-lang="1" data-url3="role_edit" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
				<?php foreach($status as $s){ ;?>
					<?php if($objects->status != $s->value){ ;?>
						<button id="<?php echo $objects->admin_id ?>" data-url="user" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="<?php echo $chiave ;?>" data-status="<?php echo $s->name ;?>" data-title="<?php echo $objects->name ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 5px"> <?php echo $s->name ;?></button>
					<?php ;} unset($s) ;?>				
				<?php ;} ;?>			
			<?php }else{ ;?>
				<button id="<?php echo $objects->user_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="user" data-title="<?php echo trim($objects->name);?>"data-lang="<?php echo $chiave ;?>" class="modal_dlt btn btn-white  btn-xs pull-right" type="button" style="margin: 5px">Delete</button>
				<button id="<?php echo $objects->user_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-lang="2" data-url3="role_edit" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
				<?php if($objects->status != 3){ ;?>
				<?php foreach($status as $s){ ;?>
					<?php if($objects->status != $s->value){ ;?>
						<button id="<?php echo $objects->user_id ?>" data-url="user" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="<?php echo $chiave ;?>" data-status="<?php echo $s->name ;?>" data-title="<?php echo $objects->name ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 5px"> <?php echo $s->name ;?></button>
					<?php ;} unset($s) ;?>				
				<?php ;} ;?>
				<?php }else{ ;?>
				<?php } ;?>
			<?php } ;?>
			</div>
		</div>
		<div class="row" >
			<?php if($chiave == 'admin'){ ;?>
				<div class="col-sm-3 col-xs-12">
					<img alt="image" class="img-responsive" src="
					<?php if($objects->main_image){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/admin/<?php echo trim($objects->main_image)?>
					<?php }else{ echo base_url('assets').'/img/image_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto;">
				</div>		
				<div class="col-sm-9 col-xs-12" style="padding-left: 0px">
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Name</p>
						<div class="col-sm-5 col-xs-8"><p><strong>: <?php echo $objects->name?></strong></p></div>	
						<p class="col-sm-2 col-xs-4">Username</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <strong><?php echo $objects->username  ;?></strong></p>
						</div>						
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Category</p>
						<div class="col-sm-5 col-xs-8">
							<p>: 
								<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
									<?php echo $objects->title?>
								</a>, 
								<a id="<?php echo $objects->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-url3="<?php echo $chiave ;?>"data-lang="2" class="detail2">
									<?php echo $objects->parent_title?>
								</a>
							</p>
						</div>
						<p class="col-sm-2 col-xs-4">Status</p>
						<div class="col-sm-3 col-xs-8">	
							<p><strong>: 
								<?php foreach($status as $s){ ;?>
									<?php if($objects->status == $s->value){ ;?>
										<?php echo $s->info?>
								<?php ;} unset($s) ;?>				
							<?php ;} ;?>	
							</strong></p>
						</div>
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Created at</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <?php echo date("l, d M Y", strtotime($objects->datecreated));?> at <?php echo date("H:i:s", strtotime($objects->datecreated));?></p>
						</div>
						<p class="col-sm-2 col-xs-4">Created By</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <?php if($objects->createdby == $admin->admin_id){ echo $admin->name ;} ;?>
							</p>
						</div>
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">						
						<p class="col-sm-2 col-xs-4">Email</p>
						<div class="col-sm-10 col-xs-8">
							<p>: <strong><?php if($objects->email != 0){ echo $objects->email ;}else{ echo "No Email" ;}  ;?></strong></p>
						</div>
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">						
						<p class="col-sm-2 col-xs-4">Birthday</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <?php echo date("l, d M Y", strtotime($objects->birthday));?> </p>
						</div>
						<p class="col-sm-2 col-xs-4">Gender</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <?php if($objects->gender == 1){ echo "Male" ;}else{ echo "Female" ;}  ;?></p>
						</div>	
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Phone</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <?php if($objects->phone){ echo $objects->phone ;}else{ echo "No phone number" ;} ;?></p>
						</div>
						<p class="col-sm-2 col-xs-4">Mobile</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <?php if($objects->mobile){ echo $objects->mobile ;}else{ echo "No mobile phone number" ;} ;?></p>
						</div>
					</div>					
					<div class="col-sm-12 col-xs-12 " style="padding-left: 0px">
						<p class="col-sm-2 col-xs-12">Address</p>
						<div class="col-sm-10 col-xs-12"><p>: <?php if($objects->address ){ echo $objects->address ;}else{echo "Empty data of address information.";} ;?></p></div>					
					</div>
					<div class="col-sm-12 col-xs-12 " style="padding-left: 0px">
						<p class="col-sm-2 col-xs-12">Short Info</p>
						<div class="col-sm-10 col-xs-12"><p>: <?php if(trim($objects->description) ){ echo $objects->description ;}else{echo "Empty data of short description.";} ;?></p></div>					
					</div>
				</div>				
			<?php }elseif($chiave == 'member'){ ;?>
				
				<div class="col-sm-3 col-xs-12">
					<img alt="image" class="img-responsive" src="
					<?php if($objects->avatar){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/user/member/<?php echo trim($objects->avatar)?>
					<?php }else{ echo base_url('assets').'/img/image_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto;">
				</div>		
				<div class="col-sm-9 col-xs-12" style="padding-left: 0px">
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Name</p>
						<div class="col-sm-5 col-xs-8"><p><strong>: <?php echo $objects->name?></strong></p></div>	
						<p class="col-sm-2 col-xs-4">Username</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <strong><?php echo $objects->username  ;?></strong></p>
						</div>						
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Category</p>
						<div class="col-sm-5 col-xs-8">
							<p>: 
								<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
									<?php echo $category->title?>
								</a>
							</p>
						</div>
						<p class="col-sm-2 col-xs-4">Status</p>
						<div class="col-sm-3 col-xs-8">	
							<p><strong>: 
								<?php if($objects->status != 3){ ?>
									<?php foreach($status as $s){ ;?>
										<?php if($objects->status == $s->value){ ;?>
											<?php echo $s->info?>
										<?php ;} unset($s) ;?>	
									<?php ;} ;?>		
								<?php }else{ ;?>
									Invited
								<?php } ;?>
							</strong></p>
						</div>
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">						
						<p class="col-sm-2 col-xs-4">Email</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <strong><?php if($objects->email != 0){ echo $objects->email ;}else{ echo "No Email" ;}  ;?></strong></p>
						</div>
						<?php if($category->title == 'Verified Member'){ ;?>
						<p class="col-sm-2 col-xs-4">Member ID</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <strong><?php echo $objects->reg_id ;?></strong>
							</p>
						</div>
						<?php } ;?>
					</div>					
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Created at</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <?php echo date("l, d M Y H:i:s", strtotime($objects->datecreated));?> </p>
						</div>
						<?php if($category->title == "Verified Member"){?>
						<p class="col-sm-2 col-xs-4">Creator</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <?php echo $objects->creator ;?>
							</p>
						</div>
						<?php }elseif($category->title == "Registered Member"){ ;?>
						<p class="col-sm-2 col-xs-4">Activation</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <?php if($objects->approvedby == 0){ echo "By systems" ;}else{ echo $objects->creator ;} ;?>
							</p>
						</div>
						<?php } ;?>
					</div>
					<?php if($category->title == 'Verified Member'){ ;?>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Valid Until</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <?php echo date("l, d M Y", strtotime($objects->dateend));?></p>
						</div>
					</div>
					<?php } ;?>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Birthday</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <?php if($objects->birthday != '0000-00-00'){ echo date("l, d M Y", strtotime($objects->birthday)) ;}else{ echo "No birthday data" ;};?> </p>
						</div>
						<p class="col-sm-2 col-xs-4">Gender</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <?php if($objects->gender == 1){ echo "Male" ;}else{ echo "Female" ;}  ;?></p>
						</div>	
					</div>
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p class="col-sm-2 col-xs-4">Phone</p>
						<div class="col-sm-5 col-xs-8">
							<p>: <?php if($objects->phone){ echo $objects->phone ;}else{ echo "No phone number" ;} ;?></p>
						</div>
						<p class="col-sm-2 col-xs-4">Mobile</p>
						<div class="col-sm-3 col-xs-8">
							<p>: <?php if($objects->mobile){ echo $objects->mobile ;}else{ echo "No mobile phone number" ;} ;?></p>
						</div>						
					</div>
				</div>				
				<div class="col-sm-12 col-xs-12 " style="padding-left: 0px">
					<p class="col-sm-2 col-xs-12">Address</p>
					<div class="col-sm-10 col-xs-12"><p>: <?php if($objects->address ){ echo ucfirst($objects->address) ;}else{echo "Empty data of address information.";} ;?></p></div>					
				</div>
				<div class="col-sm-12 col-xs-12 " style="padding-left: 0px">
					<p class="col-sm-2 col-xs-12">Description</p>
					<div class="col-sm-10 col-xs-12"><p>: <?php if($objects->description ){ echo $objects->description ;}else{echo "Empty data of short description.";} ;?></p></div>	
			<?php }else{ ;?>
				
			<?php } ;?>
		</div>
		<?php if($chiave == 'admin'){ ;?>
		<div class="row">
			<div class="col-sm-12 col-xs-12" style="padding-right: 0px">
				<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
					<thead>
						<tr>
							<th>No</th>
							<th>Module </th>
							<th style="text-align: center">Description </th>
							<th>Createdby</th>
							<th>Date Created </th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; foreach($module as $n){ ?>
						<tr class="gradeX">
							<td><?php echo $i ?></td>
							<td>
								<?php echo $n->name ?>
							</td>
							<td>							
								<?php echo word_limiter($n->description,15) ;?>
							</td>
							<td><?php echo $admin->name ?></td>
							<td><?php echo date("l, d M Y H:i:s", strtotime($n->datecreated)) ?></td>
							<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></td>
							
						</tr>	
						<?php $i++;} ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php } ;?>
	</div>
</div>