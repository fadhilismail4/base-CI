<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-8"  style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<?php if($category->parent_id == 0){ ;?>
				<li class="active">
					<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" class="detail2">
						<?php echo $category->title;?> 
					</a>
				</li>
			<?php }else{ ;?>
				<li class="">
					<a id="<?php echo $category->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" class="detail2">
						<?php echo $category->parent_title;?> 
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" class="detail2">
						<?php echo $category->title;?> 
					</a>
				</li>
			<?php }?>
		</ol>
		<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
			
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<div class="col-sm-12 col-xs-12 " style="padding-right: 10px">
			<?php if(($category->category_id == 1)&&($category->parent_id != 0)){ ;?>
				<?php foreach($status as $s){if($s->value != 2){ ;?>
					<?php if($category->status != $s->value){ ;?>
						<button id="<?php echo $category->category_id ?>" data-url="category" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $category->title ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 2px"> <?php echo $s->name ;?></button>
					<?php ;};} ;?>					
				<?php ;} ;?>
			<?php ;} ;?>
			<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_category" data-lang="2" class="detail2 btn btn-info btn-xs pull-right" data-url3="<?php if(($category->parent_id == 1)||($category->category_id == 1)){echo "admin";}else{echo "member";} ;?>"  type="button" style="margin: 2px">Edit</button>		
		</div>		
		<div class="row ">
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-5 pull-left"><p>Name</p></div>
				<div class="col-sm-4 col-xs-7"><p><strong>: <?php echo $category->title?></strong></p></div>
				<div class="col-sm-2 col-xs-5 pull-left"><p>Status</p></div>
				<div class="col-sm-4 col-xs-7"><p><strong>: <?php if($category->status == 1){ echo "Active" ;}elseif($category->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></strong></p></div>
			</div>			
			<?php if($category->parent_id != 0){ ;?>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-5 pull-left"><p>Parent Category</p></div>
				<div class="col-sm-4 col-xs-7"><p>: <?php echo $category->parent_title?></p></div>
			</div>
			<?php ;} ;?>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-5 pull-left"><p>Date Created</p></div>
				<div class="col-sm-10 col-xs-7"><p>: <?php echo date("l, d M Y H:i:s", strtotime($category->datecreated))  ;?></p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-5 pull-left"><p>Creator</p></div>
				<div class="col-sm-10 col-xs-7"><p>: <?php echo $category->name ;?></p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-12 pull-left"><p>Description</p></div>
				<div class="col-sm-10 col-xs-12"><p>: <?php echo $category->description?></p></div>
			</div>			
		</div>
		<?php if($chiave == 'child'){?>
		<div class="row col-sm-12 col-xs-12" style="padding-right: 0px">
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th>Module </th>
						<th>Description </th>
						<th>Createdby</th>
						<th>Date Created </th>
						<th>Status</th>
						<th>Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($module as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<?php echo $n->module ?>
						</td>
						<td>							
							<?php echo word_limiter($n->description,15) ;?>
						</td>
						<td><?php echo $n->name ?></td>
						<td><?php echo date("l, d M Y H:i:s", strtotime($n->datecreated)) ?></td>
						<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></td>
						<td>
							<?php foreach($status as $s){if($s->value != 2){ ;?>
								<?php if($n->status != $s->value){ ;?>
									<button id="<?php echo $n->module_id ?>" data-url="module" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="<?php echo $category->category_id?>" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->module ;?> Module" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
								<?php ;} ;?>
							<?php ;};} ;?>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
		<?php } ;?>
	</div>
</div>