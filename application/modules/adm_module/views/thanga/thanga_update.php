<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-10 col-xs-10"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<?php if($chiave == 'admin'){ ;?>
			<li class="">
				<a id="<?php echo $objects->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->parent_title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list"  data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>	
			<li class="">
				<a id="<?php echo $objects->admin_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave?>" class="detail2">
					<?php echo $objects->name;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->admin_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_custom" data-lang="<?php echo $objects->parent_id?>" data-url3="role_edit" class="detail2">
					Update
				</a>
			</li>
			<?php }else{ ;?>
			<li class="">
				<a id="<?php echo $category->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->parent_title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list"  data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>	
			<li class="">
				<a id="<?php echo $objects->user_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave ;?>" class="detail2">
					<?php echo $objects->name;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->user_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_custom" data-lang="<?php echo $category->parent_id?>" data-url3="role_edit" class="detail2">
					Update
				</a>
			</li>
			<?php } ;?>		
			
		</ol>
		<?php if($chiave == 'admin'){ ;?>
		<button id="<?php echo $objects->admin_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave ;?>" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php }else{ ;?>
		<button id="<?php echo $objects->user_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave ;?>" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php } ;?>
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>	
		<?php if($chiave == 'admin'){ ?>
			<input id="role" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->admin_id ;?>"></input>	
		<?php }elseif($chiave == 'member'){ ;?>
			<input id="role" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->user_id ;?>"></input>	
		<?php } ;?>
		<div class="row">
			<div class="col-sm-3 col-xs-12" style="margin-bottom: 20px">
				<img alt="image" class="img-responsive" src="
					<?php if($objects->main_image){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php if($chiave == 'admin'){ echo "admin" ;}else{ echo "user/member" ;} ?>/<?php echo trim($objects->main_image)?>
					<?php }else{ echo base_url('assets').'/img/image_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto;">
			</div>
			<div class="col-sm-9 col-xs-12">	
				<div class="form-horizontal">	
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";><?php if($chiave == 'admin'){echo "Role";}else{echo "Membership Type";} ;?></label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="<?php if($chiave == 'admin'){echo "role_id";}else{echo "type_id";} ;?>">	
									
									<?php if($chiave == 'admin'){ ;?>
									<option value="">Select Role</option>	
										<?php foreach($category as $ct){ ;?>
											<option value="<?php echo $ct->category_id ;?>">
												<?php echo $ct->title ;?> 
											</option>
										<?php  } ;?>
									
									<?php }elseif($chiave == 'member'){?>
										<?php if($category->title == 'Verified Member'){ ;?>
											<option value="<?php echo $category->category_id ;?>">
												<?php echo $category->title ;?>
											</option>
										<?php }elseif($category->title == 'Registered Member'){ ;?>
											<option value="">Select Type</option>	
											<option value="<?php echo $category->category_id ;?>">
												<?php echo $category->title ;?>
											</option>
											<option value="<?php echo $verified->category_id ;?>" data-title="<?php echo strtolower(str_replace(' ','',$verified->title)) ;?>"> 
												<?php echo $verified->title ;?>
											</option>
										<?php } ;?>
									<?php  } ;?>
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Name</label>
						<div class="col-sm-4 col-xs-8">
						<?php if($chiave == 'member'){ ;?>
							<?php if($category->title == 'Registered Member'){ ?>
								<input id="name" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->name) ;?>" disabled>
							<?php }else{ ;?>
								<input id="name" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->name) ;?>" placeholder="Full name [ 50 Char ]">
							<?php } ;?>	
						<?php }else{ ;?>	
							<input id="name" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->name) ;?>" placeholder="Full name [ 50 Char ]">
						<?php } ;?>	
						</div>
					</div>	
					
					<div class="form-group ">
						<?php if($chiave == 'member'){ ;?>
						<div class="" id="data_member">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Member ID</label>
							<div class="col-sm-4 col-xs-8">
								<input id="reg_id" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->reg_id) ;?>" placeholder="Member ID [ 15 Char ]">
							</div>	
						</div>		
						<?php } ;?>						
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Reset Password</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="pwd">	
									<option value="0">Select Option</option>
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>
						</div>
					</div>
					<div class="clearfix" id="data_new">
						<div class="form-group">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">New Password</label>
							<div class="col-sm-4 col-xs-8 " >
								<input id="pass" name="inputan" type="password" class="form-control"  placeholder="New Password">
							</div>
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Confirmation Password</label>
							<div class="col-sm-4 col-xs-8 " >
								<input id="confirm" name="inputan" type="password" class="form-control"  placeholder="Password Confirmation">
							</div>
						</div>
					</div>
					<?php if($chiave == 'member'){ ;?>
						<input id="key" name="inputan" type="hidden" class="form-control" value="<?php echo strtolower(str_replace(' ','',$category->title)) ;?>">
						
						
						<?php if($category->title == 'Registered Member'){ ?>
						<div class="form-group ">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Username</label>
							<div class="col-sm-4 col-xs-8">
								<input id="username" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->username) ;?>" disabled>
							</div>
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Email</label>
							<div class="col-sm-4 col-xs-8">
								<input id="email" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->email) ;?>" disabled>
							</div>
						</div>
						<?php }elseif($category->title  == 'Verified Member'){ ;?>
						<div class="form-group ">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Username</label>
							<div class="col-sm-4 col-xs-8">
								<input id="username" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->username) ;?>" placeholder="Username">
							</div>
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Email</label>
							<div class="col-sm-4 col-xs-8">
								<input id="email" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->email) ;?>" placeholder="Email">
							</div>
						</div>
						<?php } ;?>	
					<?php }elseif($chiave == 'admin'){ ;?>
						<div class="form-group ">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Username</label>
							<div class="col-sm-4 col-xs-8">
								<input id="username" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->username) ;?>" placeholder="Username">
							</div>
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Email</label>
							<div class="col-sm-4 col-xs-8">
								<input id="email" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->email) ;?>" placeholder="Email">
							</div>
						</div>
					<?php } ;?>
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Phone</label>
						<div class="col-sm-4 col-xs-8">
							<input id="phone" name="inputan" type="text" class="form-control" value="<?php if(trim($objects->phone)){ echo trim($objects->phone);} ;?>" placeholder="Empty fix phone number">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Mobile</label>
						<div class="col-sm-4 col-xs-8">
							<input id="mobile" name="inputan" type="text" class="form-control" placeholder="Empty mobile phone number"  value="<?php if(trim($objects->mobile)){ echo trim($objects->mobile);} ;?>">
						</div>
					</div>	
								
				</div>
			</div>
			<div class="col-sm-12 col-xs-12">
				<div class="form-horizontal"  method="get">	
					<div class="form-group" >
						<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Birthday</label>
						<div class="col-sm-3 col-xs-8">
							<div class="input-group date input-group col-sm-12 col-xs-12">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input id="birthday" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control" value="<?php if($objects->birthday != '0000-00-00'){ echo date(" d-m-Y",strtotime($objects->birthday)) ;};?>" placeholder="Empty birthday data">
							</div>
						</div>	
						<label class="col-sm-1 col-xs-4 control-label" style="text-align: left !important; ">Gender</label>
						<div class="col-sm-2 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="gender">
									<option value="">Select Gender</option>
									<option value="1">Male</option>
									<option value="0">Female</option>
								</select>
							</div>
						</div>
						<?php if($chiave == 'member'){ ;?>
							<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Postal Code</label>
							<div class="col-sm-2 col-xs-8">
								<input id="zip" name="inputan" type="text" class="form-control"  placeholder="Code [ Only 5 Characters ]" value="<?php echo $objects->zip?>">
							</div>
						<?php } ;?>		
					</div>	
					
					<?php if($chiave == 'member'){ ;?>
						<div class="clearfix" id="data_date">
							<div class="form-group" >				
								<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Date Start</label>
								<div class="col-sm-4 col-xs-8">
									<div class="input-group date input-group col-sm-12 col-xs-12">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="datestart" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control" value="<?php if($objects->datestart != '0000-00-00 00:00:00'){ echo date(" d-m-Y",strtotime($objects->datestart)) ;};?>" placeholder="Start Date">
									</div>
								</div>
								<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Date End</label>
								<div class="col-sm-4 col-xs-8">
									<div class="input-group date input-group col-sm-12 col-xs-12">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input id="dateend" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control" value="<?php if($objects->dateend != '0000-00-00 00:00:00'){ echo date(" d-m-Y",strtotime($objects->dateend)) ;};?>" placeholder="Valid Until">
									</div>
								</div>
							</div>
						</div>
					<?php } ;?>
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Address</label>
						<div class="col-sm-10 col-xs-12 "><textarea id="address" name="inputan" type="text" class="form-control"  placeholder="Empty address data"><?php echo trim($objects->address)?></textarea></div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Description</label>
						<div class="col-sm-10">
							<textarea id="description" name="inputan" type="text" class="form-control" value=""  placeholder="Empty short description data."><?php echo trim($objects->description)?></textarea>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Activation</option>
									<?php if($chiave == 'admin'){ ;?>
										<?php foreach($status as $s){if($s->value != 2){ ;?>
										<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
										<?php ;};} unset($s) ;?>
									<?php }else{ ;?>
										<?php foreach($status as $s){ ;?>
										<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
										<?php ;} unset($s) ;?>
										<option value="3">Invite</option>
									<?php } ;?>
								</select>
							</div>
						</div>
					</div>	
					<form class="form-group"  method="get">			
						<button id="<?php echo $key_link ;?>" data-param="user" data-param2="<?php echo $chiave?>" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Update</button>		
						<button id="<?php if($chiave == 'admin'){ echo $objects->admin_id ;}else{ echo $objects->user_id ;} ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom"  data-url3="<?php echo $chiave ;?>"  data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script>
$('#status').val(<?php echo $objects->status ;?>);
$('#gender').val(<?php echo $objects->gender ;?>);
$('#data_old').hide();
$('#data_new').hide();
<?php if($chiave == 'admin'){ ;?>
$('#role_id').val(<?php echo $objects->category_id ;?>);
<?php }else{ ;?>
	<?php if($category->title == 'Registered Member'){ ;?>
		$('#data_date').hide();
		$('#data_member').hide();	
		$('#type_id').val(<?php echo $category->category_id ;?>);
	<?php } ;?>
<?php } ;?>
$(document).ready(function(){
	$('#pwd').change(function() {	
		if ($(this).find(':selected').val() == 1) {
			$('#data_old').slideDown('slow');		
			$('#data_new').slideDown('slow');					
		}else{
			$('#data_old').slideUp('slow');
			$('#data_new').slideUp('slow');	
			$('#pwd').val('0');		
			$('#pass').val('');		
			$('#confirm').val('');		
		}
	});
	
	$('#type_id').change(function() {	
		if ($(this).find(':selected').data('title') == 'verifiedmember') {
			$('#data_date').slideDown('slow');		
			$('#data_member').slideDown('slow');								
		}else{
			$('#data_date').slideUp('slow');
			$('#data_member').slideUp('slow');	
			$('#reg_id').val('');
			$('#datestart').val('');
			$('#dateend').val('');
		}
		if ($(this).find(':selected').val() == '') {
			$('#data_date').slideUp('slow');	
			$('#data_member').slideUp('slow');		
			$('#reg_id').val('');
			$('#datestart').val('');
			$('#dateend').val('');
		}
	});
	$('.date').datepicker({
		keyboardNavigation: false,
		todayBtn: 'linked',
		format: 'yyyy-mm-dd',
		forceParse: false
	});
});
</script>
