<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">

<style>
	.modal-backdrop.in{
		display: none !important;
	}
	.fileinput-upload-button{
		display:none;
	}
	/* .input-group-btn > .btn-file{
		display:none;
	} */
	.checkbox label{
		padding-left:0px;
	}
</style>
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-8" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo $key_name;?>				
				</a>
			</li>
			<li class="">
				<a id="<?php echo $cat->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
					<?php echo $cat->parent_title ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
					<?php echo $cat->title?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-url3="<?php echo $chiave ;?>" data-lang="2" class="detail2">
					New
				</a>
			</li>
		</ol>
		<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-url3="<?php echo $chiave ;?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
	</div>
	<div class="ibox-content row">
		<div class="row">
			<div class="col-sm-3 col-xs-12" style="margin-bottom: 20px">
				<input id="<?php if($chiave == 'admin'){ echo "main_image" ;}else{echo "avatar" ;} ;?>" name="<?php if($chiave == 'admin'){ echo "main_image" ;}else{echo "avatar" ;} ;?>" class="file " type="file" value="">
			</div>
			<div class="col-sm-9 col-xs-12">			
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
				<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
				<input id="category_id" name="inputan" type="hidden" class="form-control" value="<?php echo $cat->parent_id?>">
				<div class="form-horizontal">	
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";><?php if($chiave == 'admin'){echo "Role";}else{echo "Membership Type";} ;?></label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="<?php if($chiave == 'admin'){echo "role_id";}else{echo "type_id";} ;?>">	
									
									<?php if($chiave == 'admin'){ ;?>
									<option value="">Select Role</option>	
										<?php foreach($category as $ct){ ;?>
											<option value="<?php echo $ct->category_id ;?>">
												<?php echo $ct->title ;?> 
											</option>
										<?php  } ;?>
									
									<?php }elseif(($cat->category_id == 6)&&($chiave == 'member')){?>
										<option value="<?php echo $cat->category_id ;?>">
											<?php echo $cat->title ;?>
										</option>
									<?php  } ;?>
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Name</label>
						<div class="col-sm-4 col-xs-8">
							<input id="name" name="inputan" type="text" class="form-control" value="" placeholder="Full name [ 50 Char ]">
						</div>
					</div>	
					<?php if($chiave == 'member'){ ;?>
					<input id="key" name="inputan" type="hidden" class="form-control" value="<?php echo strtolower(str_replace(' ','',$cat->title)) ;?>">
					<div class="clearfix" id="data_member">
						<div class="form-group ">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Member ID</label>
							<div class="col-sm-4 col-xs-8">
								<input id="reg_id" name="inputan" type="text" class="form-control" value="" placeholder="Member ID [ 15 Char ]">
							</div>	
						</div>
					</div>
					<?php } ;?>
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Username</label>
						<div class="col-sm-4 col-xs-8">
							<input id="username" name="inputan" type="text" class="form-control" value="" placeholder="Username">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Email</label>
						<div class="col-sm-4 col-xs-8">
							<input id="email" name="inputan" type="text" class="form-control" value="" placeholder="Email">
						</div>
					</div>
					<?php if($chiave == 'admin'){?>
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Password</label>
						<div class="col-sm-4 col-xs-8">
							<input id="password" name="inputan" type="password" class="form-control" value="" placeholder="Password">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Confirm Password</label>
						<div class="col-sm-4 col-xs-8">
							<input id="confirm" name="inputan" type="password" class="form-control" value="" placeholder="Confirm Password">
						</div>
					</div>	
					<?php } ;?>
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Phone</label>
						<div class="col-sm-4 col-xs-8">
							<input id="phone" name="inputan" type="text" class="form-control" value="" placeholder="Fix Phone">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Mobile</label>
						<div class="col-sm-4 col-xs-8">
							<input id="mobile" name="inputan" type="text" class="form-control" value="" placeholder="Mobile Phone">
						</div>
					</div>	
					<div class="form-group" >
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Gender</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="gender">
									<option value="">Select Gender</option>
									<option value="1">Male</option>
									<option value="0">Female</option>
								</select>
							</div>
						</div>
						<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Birthday</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group date input-group col-sm-12 col-xs-12">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input id="birthday" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
							</div>
						</div>	
					</div>	
					<?php if($chiave == 'member'){ ;?>
					<div class="form-group" >	
						<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Postal Code</label>
						<div class="col-sm-4 col-xs-8">
							<input id="zip" name="inputan" type="text" class="form-control" value="" placeholder="Code [ Only 5 Characters ]">
						</div>
					</div>
					<?php } ;?>					
				</div>
			</div>
			<div class="col-sm-12 col-xs-12">
				<div class="form-horizontal"  method="get">		
					<?php if($chiave == 'member'){ ;?>
						<div class="form-group" >				
							<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Date Start</label>
							<div class="col-sm-4 col-xs-8">
								<div class="input-group date input-group col-sm-12 col-xs-12">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input id="datestart" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
								</div>
							</div>
							<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Date End</label>
							<div class="col-sm-4 col-xs-8">
								<div class="input-group date input-group col-sm-12 col-xs-12">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input id="dateend" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
								</div>
							</div>
						</div>
					<?php } ;?>
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Address</label>
						<div class="col-sm-10 col-xs-12 "><textarea id="address" name="inputan" type="text" class="form-control"  placeholder="Address information for your user. [ Only 250 Characters]"></textarea></div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 control-label" style="text-align: left !important">Description</label>
						<div class="col-sm-10">
							<textarea id="description" name="inputan" type="text" class="form-control" value=""  placeholder="You can add a short description about user [ Only 250 Characters ]"></textarea>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Activation</option>
									<?php if($chiave == 'admin'){ ;?>
										<?php foreach($status as $s){if($s->value != 2){ ;?>
										<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
										<?php ;};} unset($s) ;?>
									<?php }else{ ;?>
										<?php foreach($status as $s){ ;?>
										<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
										<?php ;} unset($s) ;?>
										<?php if($cat->category_id == 6){ ;?>
										<option value="3">Invite</option>
										<?php } ;?>
									<?php } ;?>
								</select>
							</div>
						</div>
					</div>	
					<form class="form-group"  method="get">			
						<button id="<?php echo $key_link ;?>" data-param="user" data-param2="<?php echo $chiave?>" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Create</button>		
						<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list"  data-url3="<?php echo $chiave ;?>"  data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>

<script>
$(document).ready(function(){
	$('.date').datepicker({
		keyboardNavigation: false,
		todayBtn: 'linked',
		format: 'yyyy-mm-dd',
		forceParse: false
	});
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php echo base_url('assets').'/img/image_empty.png' ;?>' class='file-preview-image' style='width: 100%; height: auto'>"
		],			
		showRemove: false,
		showUpload: false
	});
});
</script>
