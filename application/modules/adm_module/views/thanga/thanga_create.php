<link href="http://buzcon.com/project/assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet" type="text/css">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="1" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					Admin
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="new_category" data-url3="admin" data-lang="2" class="detail2">
					Create
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Back</button>
		</div>		
	</div>
	<div class="ibox-content row">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Role</label>
				<div class="col-sm-4 col-xs-8">
					<div class="input-group col-sm-12 col-xs-12" >
						<select class="form-control m-b" name="inputan" id="parent_id">	
							<option value="1">Admin</option>	
						</select>
					</div>
				</div>
				<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Role Name</label>
				<div class="col-sm-4 col-xs-8 "><input id="title" name="inputan" type="text" class="form-control" placeholder="Make a short but informative name"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Description</label>
				<div class="col-sm-10 col-xs-12"><textarea id="description" name="inputan" type="text" class="form-control" placeholder="This is the description about role you created. [ Only 250 characters allowed]"></textarea></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>		
			<input id="module_id" name="inputan" type="text" class="form-control hide" value=""></input>
			<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
			<div id="language" class="form-group col-sm-12">	
				<?php 
				foreach($category as $c){;?>
					<div class="col-sm-3" style="padding: 10px 0">
						<div class="col-sm-2">
							<input id="<?php echo $c->module_id ;?>" type="checkbox" class="i-checks">
						</div>
						<div class="col-sm-9" style="">
							<i class="fa fa-<?php echo $c->icon;?>"></i> <?php echo $c->name?>
						</div>
					</div>
				<?php };?>
			</div>
			<div class="form-group col-sm-12">
				<div class="hr-line-dashed"></div>			
				<div class="col-md-12">
					<button id="<?php echo $key_link;?>" data-param="<?php echo $key_link;?>" data-param2="admin" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Create</button>
					<button id="all" data-url="module" data-url2="role" data-param="list" data-lang="2" class="detail2 btn btn-md btn-white pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
		
	</div>
</div>
<script>
$(document).ready(function(){
	$(".create_mdl").hide();
	$.getScript( "http://buzcon.com/project/assets/admin/js/plugins/iCheck/icheck.min.js" ).done(function( script, textStatus ) {
		$(".i-checks").iCheck({
			checkboxClass: "icheckbox_square-green",
			radioClass: "iradio_square-green",
		});
	});
	$("#language input").on("ifToggled", function (event) {
		$(".create_mdl").slideDown();
		brands();
	});
	
	function brands() {
		var brands = [];
		$("#language input").each(function (index, value) {
			if ($(this).is(":checked")) {
				brands.push($(this).attr("id"));
			}
		});
		console.log(brands);
		$("#module_id").val(brands);
		if(!brands.length){
			$(".create_mdl").slideUp();
		}
	}
});
</script>