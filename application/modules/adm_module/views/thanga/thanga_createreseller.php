<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-12"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="2" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					Reseller
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $categories->category_id ?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="new_category" data-url3="reseller" data-lang="2" class="detail2">
					Create
				</a>
			</li>
		</ol>
		<button id="3" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn btn-sm btn-warning pull-right">Back</button>
			
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>		
		<div class="col-sm-4 col-xs-12" style="padding-left: 0px" >
			<input id="image_square" name="image_square" class="file " type="file" value="">
		</div>
		<div class="col-sm-8 col-xs-12" >
			<form class="form-horizontal" style="margin-top: 10px">
				<div class="form-group">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Category</label>
					<div class="col-sm-5 col-xs-8">
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="parent_id">
								<option value="<?php echo $categories->category_id?>"><?php echo $categories->title?></option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Name</label>
					<div class="col-sm-10 col-xs-12 "><input id="title" name="inputan" type="text" class="form-control" placeholder="Give a common name for your reseller category that easily remembered."></input></div>
				</div>
				<div class="form-group" >
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
					<div class="col-sm-5 col-xs-8" >
						<div class="input-group col-sm-12 col-xs-12 " >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<option value="0">Inactivate</option>
								<option value="1">Activate</option>
							</select>
						</div>
					</div>
				</div>
			</form>
		</div>
		<form class="form-horizontal" method="get">			
			<div class="form-group">
				<label class="col-sm-12 col-xs-12 control-label" style="text-align: left !important">Content</label>
			</div>
			<div class="mail-box">
				<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
					<div id="description" name="inputan_summer" class="summernote">
						<span style="font-family: 'Courier New';">This is an example for your description page, a lot of space here. Feel free to write your content . <span style="font-weight: bold;">Cheers</span></span>
					</div>
				</div>			
				<div class="mail-body text-right tooltip-demo"  >
				</div>
			</div>
		</form>	
	</div>
</div>
<div class="ibox ">
	<div class="ibox-title row">
		<h5>SEO SETTINGS</h5>
	</div>
	<div class="ibox-content row">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Keywords</label>
				<div class="col-sm-10">
					<textarea id="metakeyword" name="inputan" type="text" class="form-control" value="" placeholder="Example : Author: J. K. Rowling, Illustrator: Mary GrandPré, Category: Books, Price: $17.99, Length: 784 pages [ Only 100 characters ]"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Description</label>
				<div class="col-sm-10">
					<textarea id="metadescription" name="inputan" type="text" class="form-control" value="Example: " placeholder="Example : Make sure that every page on your site has a meta description. [ Only 200 characters ]"></textarea>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-2 pull-right">		
					<button id="<?php echo $key_link;?>" data-param="<?php echo $key_link?>" data-param2="reseller" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square"></i>  Create</button>		
					<button id="3" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2 btn btn-md btn-white pull-right" type="submit"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>

<script>
$(document).ready(function(){
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php echo base_url('assets').'/img/image_empty.png' ;?>' class='file-preview-image'>"
		],			
		showRemove: false,
		showUpload: false
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>
