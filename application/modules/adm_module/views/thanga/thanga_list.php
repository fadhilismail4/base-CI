<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					All <?php echo $key_name?>
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-6 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<?php if($categories->parent_id == 0){ ;?>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php }else{ ;?>
			<li class="">
				<a id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->parent_title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php }; ?>
		</ol>
		<?php } ;?>
		<?php if($id != 'all'){ ;?>
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
			<?php if(($categories->parent_id != 0)&&($categories->title != 'Registered Member')){ ;?>
			<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-url3="<?php if(($categories->parent_id == 1)||($categories->category_id == 1)){echo "admin";}else{echo "member";} ;?>" data-lang="2" class="detail2 btn-sm btn btn-primary pull-right" style="margin-right: 10px">Add New</button>
			<?php }elseif(($categories->parent_id == 0)&&($categories->title == 'Reseller')){ ;?>
			<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new_category" data-url3="reseller" data-lang="2" class="detail2 btn-sm btn btn-primary pull-right" style="margin-right: 10px">Add Category</button>
			<?php } ;?>
		<?php } ;?>
		<?php if($zone == 'shisha'){;?>
			<button data-toggle="modal" data-target="#import_user" class="btn-sm btn btn-primary pull-right" style="margin-right: 10px">Import User</button>
		<?php };?>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<div class="table-responsive" style="border: 0;">
			<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th style="text-align: center">Title </th>
						<th style="text-align: center">Role </th>
						<th style="text-align: center">Email </th>
						<th>Created By</th>
						<th style="text-align: center">Date Created</th>
						<th>Status</th>
						<th style="text-align: center">Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php if($id == 'all'){ ;?>
					<tr class="gradeX">
						<td>1</td>
						<td>
							<?php echo $admin->name ?>
						</td>					
						<td>
							Super Admin
						</td><td>
							<?php echo $admin->email ?>
						</td>	
						<td>COBOLTS Tech</td>
						
						<td> <?php echo date("l, d M Y H:i:s", strtotime($admin->datecreated))  ;?></td>
						<td>Active</td>
						<td>
							
						</td>
					<?php }elseif($id == 1){ ;?>
					<tr class="gradeX">
						<td>1</td>
						<td>
							<?php echo $admin->name ?>
						</td>					
						<td>
							Super Admin
						</td><td>
							<?php echo $admin->email ?>
						</td>	
						<td>COBOLTS Tech</td>
						
						<td> <?php echo date("l, d M Y H:i:s", strtotime($admin->datecreated))  ;?></td>
						<td>Active</td>
						<td>
							
						</td>
					<?php } ;?>
					</tr>	
					<?php if($id == 'all' ){ $i = 2;}elseif($id == 1){ $i = 2;}else{ $i = 1;}; foreach($objects as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<?php if($id == 'all'){ ;?>
							<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_custom"  data-url3="<?php if((($categories->category_id == $n->category_id) || ($categories->category_id == $n->parent_id)) || (($n->parent_id == 1)||($n->category_id == 1))){echo "admin";}else{echo "member";} ;?>" class="detail2">
								<?php echo $n->name ?>
							</a>
							<?php }else{ ;?>
							<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_custom"  data-url3="<?php if(( $categories->category_id == 1) || ($categories->parent_id == 1)){echo "admin";}else{echo "member";} ;?>" class="detail2">
								<?php echo $n->name ?>
							</a>
							<?php } ;?>
						</td>
						
						<td>
							<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_category" class="detail2">
								<?php echo $n->category ;?>
							</a>
						</td><td>
							<?php if(!is_numeric($n->email)){ echo $n->email ;}else{ echo "No Email" ;};?>
						</td>	
						<td><?php if($n->createdby == $admin->object_id){ echo $admin->name ;}else{ echo $n->createdby ;} ?></td>					
						<td> <?php echo date("l, d M Y H:i:s", strtotime($n->datecreated))  ;?></td>
						<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}elseif($n->status == 2){echo "On Hold" ;}else{ echo "Invited";};?></td>
						<td>
							<?php if($id == 'all'){ ;?>
							<?php if(($n->status != 3)&&($n->object_id != $admin->object_id)){;?>						
								<?php foreach($status as $s){ ;?>
									<?php if($n->status != $s->value){ ;?>
										<button id="<?php echo $n->object_id ?>" data-url="user" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="<?php if((($categories->category_id == $n->category_id) || ($categories->category_id == $n->parent_id)) || (($n->parent_id == 1)||($n->category_id == 1))){echo "admin";}else{echo "member";} ;?>" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->name ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 2px"> <?php echo $s->name ;?></button>
									<?php ;} ;?>
								<?php ;} ;?>				
							<?php };?>
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-url3="role_edit" data-lang="<?php if((($categories->category_id == $n->category_id) || ($categories->category_id == $n->parent_id)) || (($n->parent_id == 1)||($n->category_id == 1))){echo "1";}else{echo "2";} ;?>" class="dtl btn btn-info btn-xs pull-right" type="button" style="margin: 2px">Edit</button>
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_custom" data-url3="<?php if((($categories->category_id == $n->category_id) || ($categories->category_id == $n->parent_id)) || (($n->parent_id == 1)||($n->category_id == 1))){echo "admin";}else{echo "member";} ;?>" data-lang="2" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 2px">View</button>
							<?php }else{ ;?>
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-url3="role_edit" data-lang="<?php if(( $categories->category_id == 1) || ($categories->parent_id == 1)){echo "1";}else{echo "2";} ;?>" class="dtl btn btn-info btn-xs pull-right" type="button" style="margin: 2px">Edit</button>
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_custom" data-url3="<?php if(( $categories->category_id == 1) || ($categories->parent_id == 1)){echo "admin";}elseif(( $categories->category_id == 3) || ($categories->parent_id == 3)){ echo 'reseller' ;}else{echo "member";} ;?>" data-lang="2" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 2px">View</button>
							<?php } ;?>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php if($zone == 'shisha'){;?>
<!-- Modal -->
<div class="modal fade" id="import_user" tabindex="-1" role="dialog" aria-labelledby="import_user_label">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="import_user_label">Import Member</h4>
      </div>
      <div class="modal-body">
		<div class="row">
			<div class="col-md-2">
				<p>*Needed</p>
				<hr>
				<p>Name*</p>
				<p>Member ID *</p>
				<p>Username</p>
				<p>Email *</p>
				<p>Phone</p>
				<p>Mobile</p>
				<p>Gender</p>
				<p>Birthday</p>
				<p>Postal Code</p>
				<p>Date Start</p>
				<p>Date End</p>
				<p>Address</p>
			</div>
			<div class="col-md-10 table-responsive">
				<div class="alert alert-danger" id="fail_export" style="display:none;"></div>
				<div class="alert alert-info" id="success_export" style="display:none;"></div>
				<div class="row">
					<div class="col-md-8">
						<input id="export_user" name="export_user" type="file" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
					</div>
					<div class="col-md-4">
						<button id="extract_user" type="button" class="btn btn-sm btn-info pull-right">Extract</button>
					</div>
				</div>
				<hr style="margin-top:9px">
				<div id="body_import_excel" class="col-md-12">
				</div>
			</div>
		</div>
      </div>
     <div id="btn_create_user" class="modal-footer" style="display:none">
        <button type="button" data-filename="" class="btn btn-primary">Create User</button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type="text/javascript"></script>
<script>
$('#extract_user').on("click", function(e) {
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	$('#success_export').empty();
	$('#fail_export').empty();
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		lang : 1,
	};
	$.ajaxFileUpload({ 
		url : "<?php echo $menu['link'];?>/export_user/extract",
		fileElementId: 'export_user',
		secureuri: false,
		type: "POST",
		dataType: 'JSON',
		data: data,
		success: function(data){
			$('.btn').each(function(){$(this).removeAttr('disabled');});
			JSON.parse(JSON.stringify(data), function(k, v) {
				var cek = JSON.parse(v.replace(/(<([^>]+)>)/ig,""));
				if (cek.status == "success"){
					$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#success_export');
					$('#success_export').show();
					$('#btn_create_user').show();
					$('#btn_create_user').data('filename',cek.filename);
					$('#success_export').fadeTo(2000, 500).slideUp(500);
					view = '<table id="table_user" class="table table-responsive table-striped table-bordered table-hover">';
					
					if(cek.datas.excels.length != 0){
						var i = 1;
						$.each(cek.datas.excels, function(k, excel) {
							if(k == 0){
								view +=
								'<thead>'+
										'<tr>'+
											'<th>#</th>';
											$.each(excel, function(k_h, val) {
												if(k_h != 'is_inserted'){
													view += '<th>'+k_h+'</th>';
												}
											});
								view +=	'</tr>'+
									'</thead>'+
								'<tbody>'; 
							}
							if(excel['is_inserted'] == 0){
								color = 'ready_to_insert';
							}else if(excel['is_inserted'] == 1){
								color = 'success';
							}else if(excel['is_inserted'] == 2){
								color = 'danger';
							}else{
								color = '';
							}
							view += 
										'<tr class="'+color+'">'+ 
											'<td scope="row">'+i+'</td>';
											$.each(excel, function(key, e) {
												if(key != 'is_inserted'){
													view += '<td>'+e+'</td>';
												}	
											});
							view += 	'</tr>';
							i++;
						});
						view += '</tbody></table>';
						$('#body_import_excel').empty();
						$('#body_import_excel').append(view);
						$('#table_user').DataTable();
					};
				}else{
					$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#fail_export');
					$('#fail_export').show();
					$('#fail_export').fadeTo(4000, 500).slideUp(500); 
				} 
			});     
		}
	});
});

$('#btn_create_user').on("click", function(e) {
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	$('#success_export').empty();
	$('#fail_export').empty();
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		lang : 1,
		data : $(this).data('filename')
	};
	$.ajax({
		url : "<?php echo $menu['link'];?>/export_user/create",
		type: "POST",
		dataType: 'JSON',
		data: data,
		success: function(data){
			$('.btn').each(function(){$(this).removeAttr('disabled');});
			if (data.status == "success"){
				$('<p>'+data.m+'</p>').appendTo('#success_export');
				$('#success_export').show();
				$('.ready_to_insert').each(function(){
					$(this).addClass('green');
					$(this).removeClass('ready_to_insert');
				});
				$('#success_export').fadeTo(2000, 500).slideUp(500);
			}else{
				$('<p>'+data.m+'</p>').appendTo('#fail_export');
				$('#fail_export').show();
				$('#fail_export').fadeTo(4000, 500).slideUp(500); 
			}
		}
	});
});
</script>
<?php };?>