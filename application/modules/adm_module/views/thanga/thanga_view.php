<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-5">
			<li class="active">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					All <?php echo $key_name;?>
				</a>
			</li>
		</ol>
		<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right" style="margin-left: 2px">Back</button>	
		<button id="<?php echo $ccs_key;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new_category"  data-url3="admin" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-primary pull-right" style="margin-left: 2px">Add Role</button>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>		
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Title </th>
					<th>Description </th>
					<th>Createdby</th>
					<th>Date Created </th>
					<th>Status</th>
					<th>Activation</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($category)){ ?>
				<?php $i = 1; foreach($category as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-url3="<?php if(($n->parent_id == 1)||($n->category_id == 1)){echo "admin";}else{echo "member";} ;?>" data-lang="2" data-param="detail_category" class="dtl">
							<?php echo $n->title ?>
						</a>
					</td>
					<td>
						<?php echo word_limiter($n->description, 15) ;?>
					</td>
					<td><?php echo $n->name ?></td>
					<td><?php echo date("l, d M Y H:i:s", strtotime($n->datecreated)) ?></td>
					<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></td>
					<td>
						<button id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-url3="<?php if(($n->parent_id == 1)||($n->category_id == 1)){echo "admin";}else{echo "member";} ;?>" data-param="detail_category" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 2px">View</button>
						<button id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_category" data-url3="<?php if(($n->parent_id == 1)||($n->category_id == 1)){echo "admin";}else{echo "member";} ;?>" data-lang="2" class="dtl btn btn-info btn-xs pull-right" type="button"  style="margin: 2px">Edit</button>
						<?php if(($n->parent_id != 0)&&($n->parent_id != 2)){ ;?>
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){if($s->value != 2){ ;?>
									<button id="<?php echo $n->category_id ?>" data-url="category" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button"  style="margin: 2px"> <?php echo $s->name ;?></button>
								<?php ;};} ;?>
							<?php ;} ;?>
						<?php ;} ;?>
					</td>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
	</div>
</div>