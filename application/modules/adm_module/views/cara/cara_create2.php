<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-sm-7 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="detail2">
					<?php echo $category->title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="new_categories" data-lang="2" class="detail2">
					Create
				</a>
			</li>
		</ol>
		<button id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="listcat" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right">Back</button>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id ;?>"></input>
		<form class="form-horizontal">
			
			<div class="form-group" >
				<div class="col-sm-6 col-xs-12 ">
					<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; padding-left: 0px">Menu Type</label>
					<div class="col-sm-8 col-xs-8" style=" padding-left: 10px" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="menu_type">
								<option value="">Select Type</option>
								<option value="paralax">Paralax Menu</option>
								<option value="multisite">Multi Site Menu</option>
							</select>
						</div>
					</div>
				</div>
					
				<div class="col-sm-6 col-xs-12">
					<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; padding-left: 0px ">Activation</label>
					<div class="col-sm-8 col-xs-8" style=" padding-left: 10px" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ if($s->value != 2){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php ;};}?>
							</select>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important; ">Description</label>
				<div class="col-sm-10 col-xs-12"><textarea id="description" name="inputan" type="text" class="form-control" placeholder="A short description about the menu that you create can help you to understand their function."></textarea></div>
			</div>
			<div class="form-group" >
				<div class="" id="data_2">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Select Type</label>
					<div class="col-sm-4 col-xs-8" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="type_id">
								<option value="">Select Type</option>
								<option value="module">From Module</option>
								<option value="page">From Page</option>
							</select>
						</div>
					</div>
				</div>				
				<div class="" id="data_3">
					<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Select Section</label>
					<div class="col-sm-4 col-xs-8">
						<div class="input-group col-sm-12 col-xs-12" >
						<select class="form-control m-b" name="inputan" id="section">
							<option value="">Select Section</option>
							<?php foreach($section as $s){ ;?>
								<option value="<?php echo $s->title?>"><?php echo $s->title?></option>
							<?php } ;?>
						</select>
					</div>
					</div>
				</div>
			</div>	
			<div class="form-group" >				
				<div class="" id="data_4">
					<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Select Module</label>
					<div class="col-sm-4 col-xs-8">
						<div class="input-group col-sm-12 col-xs-12" >
						<select class="form-control m-b" name="inputan" id="module">
							<option value="">Select Module</option>
							<?php foreach($module as $m){ ;?>
								<option value="<?php echo $m->name?>"><?php echo $m->name?> Module</option>
							<?php } ;?>
						</select>
					</div>
					</div>
				</div>				
				<div class="" id="data_5">
					<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Select Page</label>
					<div class="col-sm-4 col-xs-8">
						<div class="input-group col-sm-12 col-xs-12" >
						<select class="form-control m-b" name="inputan" id="page">
							<option value="">Select Page</option>
							<?php foreach($page as $p){ ;?>
								<option value="<?php echo $p->title?>"><?php echo $p->title?> Page</option>
							<?php } ;?>
						</select>
					</div>
					</div>
				</div>
			</div>	
			<div class="form-group">
				<div class="clearfix" id="data_6">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Select Category</label>
					<div class="col-sm-4 col-xs-8" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="category">
								<option value="">Select Category</option>
								<option value="1">All Category</option>
								<option value="0">Only Module</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-2 pull-right">		
					<button id="<?php echo $key_link;?>" data-param="menu" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square"></i>  Create</button>		
					<button id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="listcat" data-lang="2" class="detail2 btn btn-md btn-white pull-right" type="submit"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#data_2').hide();
		$('#data_3').hide();
		$('#data_4').hide();
		$('#data_5').hide();		
		$('#data_6').hide();
		$('#menu_type').change(function() {
			if ($(this).find(':selected').val() === 'multisite') {
				$('#data_2').slideDown('slow');
			}else {
				$('#data_2').slideUp('slow');
			}
			if ($(this).find(':selected').val() === 'paralax') {
				$('#data_3').slideDown('slow');
			}else {
				$('#data_3').slideUp('slow');
			}
		});
		$('#type_id').change(function() {
			if ($(this).find(':selected').val() === 'module') {
				$('#data_4').slideDown('slow');
			}else {
				$('#data_4').slideUp('slow');
				$('#data_6').slideUp('slow');
			}
			if ($(this).find(':selected').val() === 'page') {
				$('#data_5').slideDown('slow');
			}else {
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
			}
			
		});
		$('#module').change(function() {
			if ($(this).find(':selected').val()!= '') {
				$('#data_6').slideDown('slow');
			}else {
				$('#data_6').slideUp('slow');
			}
			
		});
	});
</script>