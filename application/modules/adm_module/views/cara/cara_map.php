<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-sm-12 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
		</ol>
	</div>
	<div class="ibox-content row">
		<div class="wrapper wrapper-content animated fadeInRight col-sm-12 col-xs-12" style="padding-top: 0px !important">
			<div class="col-sm-6 col-xs-12" style="padding: 10px 0px;">
				<img alt="image" class="img-responsive" src="<?php echo base_url('assets')?>/img/manage-boss.jpg">
			</div>
			<div class="col-sm-6 col-xs-12" style="margin: 0px ">
				<a id="<?php echo $sitemap[0]->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-lang="2" data-param="list" class="detail2 col-sm-12 col-xs-6" style="padding: 1px 5px ">				
					<div class="widget style1 lazur-bg" style="margin: 9px 0px !important">
						<div class="row">
							<div class="col-sm-4 col-xs-12">
								<i class="fa fa-edit fa-5x"></i>
							</div>
							<div class="col-sm-8 col-xs-12 text-right">
								<span>  </span>
								<h3 class="font-bold"><?php echo $sitemap[0]->title?></h3>
							</div>
						</div>
					</div>
				</a>
				<a id="<?php echo $sitemap[1]->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-lang="2" data-param="list" class="detail2 col-sm-12 col-xs-6" style="padding: 1px 5px ">
					<div class="widget style1 navy-bg" style="margin: 9px 0px !important">
						<div class="row">
							<div class="col-sm-4 col-xs-12">
								<i class="fa fa-bookmark-o fa-5x"></i>
							</div>
							<div class="col-sm-8 col-xs-12 text-right">
								<span>  </span>
								<h3 class="font-bold"><?php echo $sitemap[1]->title?></h3>
							</div>
						</div>
					</div>
				</a>
				<a id="<?php echo $sitemap[2]->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-lang="2" data-param="listcat" class="detail2 col-sm-12 col-xs-6" style="padding: 1px 5px ">
					<div class="widget style1 yellow-bg" style="margin: 9px 0px !important">
						<div class="row">
							<div class="col-sm-4 col-xs-12">
								<i class="fa fa-desktop fa-5x"></i>
							</div>
							<div class="col-sm-8 col-xs-12 text-right">
								<span>  </span>
								<h3 class="font-bold"><?php echo $sitemap[2]->title?></h3>
							</div>
						</div>
					</div>
				</a>
				<a id="<?php echo $sitemap[3]->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-lang="2" data-param="list" class="detail2 col-sm-12 col-xs-6" style="padding: 1px 5px ">
					<div class="widget style1 red-bg" style="margin: 9px 0px !important">
						<div class="row">
							<div class="col-sm-4 col-xs-12">
								<i class="fa fa-circle-o-notch fa-5x"></i>
							</div>
							<div class="col-sm-8 col-xs-12 text-right">
								<span>  </span>
								<h3 class="font-bold"><?php echo $sitemap[3]->title?></h3>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>