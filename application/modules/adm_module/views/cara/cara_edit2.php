<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="detail2">
					<?php echo $category->title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_category" data-lang="2" class="detail2">
					Edit
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Back</button>
		</div>		
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="parent_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10"><input id="title" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
			</div>
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Menu Type</label>
				<div class="col-sm-10" >
					<div class="input-group col-sm-6" >
						<select class="form-control m-b" name="inputan" id="metakeyword">
							<option value="">Select Type</option>
							<option value="paralax">Paralax Menu</option>
							<option value="multisite">Multi Site Menu</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="clearfix" id="data_2">
					<label class="control-label col-sm-2" style="text-align: left !important; ">Modul Type</label>
					<div class="col-sm-10">
						<div class="input-group col-sm-6" >
						<select class="form-control m-b" name="inputan" id="metadescriptions">
							<option value="">Select Modul</option>
							<option value="post">Single Post</option>
							<?php foreach($module as $m){ ;?>
								<option value="<?php echo $m->link?>"><?php echo $m->name?></option>
							<?php } ;?>
						</select>
					</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="clearfix" id="data_3">
					<label class="control-label col-sm-2" style="text-align: left !important; ">Homepage Type</label>
					<div class="col-sm-10">
						<div class="input-group col-sm-6" >
						<select class="form-control m-b" name="inputan" id="metadescription">
							<option value="">Select Section</option>
							<?php foreach($section as $s){ ;?>
								<option value="<?php echo $s->title?>"><?php echo $s->title?></option>
							<?php } ;?>
						</select>
					</div>
					</div>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-2 pull-right">		
					<button id="<?php echo $key_link;?>" data-param="category" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square"></i>  Create</button>		
					<button id="<?php echo $ccs_key?>" data-url="<?php echo $key_link;?>" data-url2="list" data-lang="<?php if(!empty($category)){  echo $category->language_id;}else{ $language_id;};?>" class="detail2 btn btn-md btn-white pull-right" type="submit"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#data_2').hide();
		$('#data_3').hide();
		$('#metakeyword').change(function() {
			if ($(this).find(':selected').val() === 'multisite') {
				$('#data_2').slideDown('slow');
			}else {
				$('#data_2').slideUp('slow');
			}
			if ($(this).find(':selected').val() === 'paralax') {
				$('#data_3').slideDown('slow');
			}else {
				$('#data_3').slideUp('slow');
			}
			
		});
	});
</script>