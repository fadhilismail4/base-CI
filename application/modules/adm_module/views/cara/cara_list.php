<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-sm-7 col-xs-7" style="font-size: 14px; padding-top: 6px">
			<?php if($id != 'all'){ ?>
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php }else{ ;?>
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<?php } ;?>
		</ol>
		<?php if($id != 'all'){ ;?>
		<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right" style="margin-left: 5px">Back</button>
		<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary pull-right">Add New</a>
		<?php };?>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>It's empty here, please create your sitemap content... </h3>
		<?php }else{ ;?>
		<div class="table-responsive" style="border: 0;">
			<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th style="text-align:center">Title </th>
						<th style="text-align: center">Category </th>
						<th>Creator</th>
						<th style="text-align:center">Date Created </th>
						<th>Status</th>
						<th style="text-align:center">Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($objects as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_object" data-url3="<?php if($n->category == 'Page'){ echo 'konten';}else{echo $n->metakeyword ;} ;?>" class="dtl">
								<?php echo ucwords($n->title) ?>
							</a>
						</td>					
						<td style="text-align: center">
							<?php if(($n->category == 'Header')&&($id != 'all')){ ?>
							<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="list" class="dtl">
								<?php if($n->metadescription == 'socmed'){ echo "Socmed" ;}else{ echo "Page" ;};?> Type <br>( <?php echo $n->category ;?> )
							</a>
							<?php }else{ ;?>
							<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="list" class="dtl">
								<?php echo $n->category ;?>
							</a>
							<?php } ;?>
						</td>
						<td><?php echo $n->name ?></td>
						<td><?php echo date("l, d M Y H:i:s", strtotime($n->datecreated)) ?></td>
						<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
						<td>
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ ;?>
									<button id="<?php echo $n->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 2px"> <?php echo $s->name ;?></button>
								<?php ;} ;?>
							<?php ;} ;?>
							<?php if(($n->category == 'Header') && ($id != 'all')){?>
								<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="<?php if($n->category == 'Page'){ echo 'konten';}else{echo $n->metakeyword ;} ;?>"class="dtl btn btn-info  btn-xs pull-right" type="button" style="margin: 2px">Edit</button>
							<?php } ;?>
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_object" data-url3="<?php if($n->category == 'Page'){ echo 'konten';}else{echo $n->metakeyword ;} ;?>" data-lang="2" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 2px">View</button>
							<?php if(($n->category == 'Footer') && ($id != 'all')){ ;?>						
								<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="objects" data-title="<?php echo trim($n->title);?>"data-lang="2" class="modal_dlt btn btn-white btn-xs pull-right" type="button" style="margin: 2px">Delete</button>
							<?php } ;?>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
		<?php };?>
	</div>
</div>