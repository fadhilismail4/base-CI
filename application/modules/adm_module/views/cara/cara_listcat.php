<div class="ibox">
	<div class="ibox-title row">
		<?php if($categories->title != "Menu"){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-5" style="font-size: 14px; padding-top: 6px">
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-8" style="font-size: 14px; padding-top: 6px">
		<?php }?>
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<?php if($categories->parent_id != 0){ ;?>
			<li class="">
				<a id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="detail2">
					<?php echo $categories->parent_title ;?>
				</a>
			</li>
			<?php } ;?>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
		</ol>
		<?php if($categories->parent_id != 0){ ;?>
		<button id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right">Back</button>
		<button id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new_categories" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-primary pull-right" style="margin-right:5px">Add New</button>
		<?php }else{ ;?>
		<button id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right">Back</button>
		<?php } ;?>
		
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $categories->category_id?>"></input>
		<?php if($categories->title != "Menu"){ ;?>
		<div class="col-sm-12 col-xs-12 pull-right" style="padding-bottom: 10px">
			<?php foreach($status as $s){ ;?>
				<?php if($categories->status != $s->value){if($s->value != 2){ ;?>
					<button id="<?php echo $categories->category_id ?>" data-url="category" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $categories->title ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 0 5px"> <?php echo $s->name ;?></button>
				<?php };} ;?>
				
			<?php ;} ;?>
			<button id="<?php echo $categories->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_category" data-lang="2" class="detail2 btn btn-info ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Edit</button>	
			<?php if( count($page) == 0){ ;?>
				<button id="<?php echo $categories->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="objects" data-title="<?php echo trim($categories->title);?>"data-lang="2" class="modal_dlt btn btn-white ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Delete</button>
			<?php } ;?>
		</div>
		<?php } ;?>
		<div class="row">
			<div class="col-sm-2 col-xs-5 pull-left"><p>Name</p></div>
			<div class="col-sm-10 col-xs-7"><p>: <strong><?php echo $categories->title?></strong></p></div>
			<div class="col-sm-2 col-xs-5"><p>Status</p></div>
			<div class="col-sm-10 col-xs-7"><p>: <strong><?php if($categories->status == 1){ echo "Active" ;}elseif($categories->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></strong></p></div>
			<div class="col-sm-2 col-xs-5"><p>Date Created</p></div>
			<div class="col-sm-10 col-xs-7"><p>: <?php echo date("l, d M Y H:i:s", strtotime($categories->datecreated))  ;?></p></div>
			<div class="col-sm-2 col-xs-5"><p>Creator</p></div>
			<div class="col-sm-10 col-xs-7"><p> : <?php echo $categories->name ;?></p></div>		
			<div class="col-sm-2 col-xs-12 pull-left"><p>Description </p></div>
			<div class="col-sm-10 col-xs-12"><p>: <?php echo $categories->description?></p></div>
		</div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		
		<?php if($c_page == 0){ ;?>	
			<h3>It's empty here, please create your sitemap content... </h3>
		<?php }else{ ;?>
		<div class="table-responsive" style="border: 0;">
			<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th style="text-align: center">Menu </th>
						<?php if($categories->title == 'Branch'){ ;?>
						<th>Order </th>
						<?php } ;?>
						<th>Category </th>
						<th>Creator</th>
						<th style="text-align: center">Date Created </th>
						<th>Status</th>
						<th>Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($page as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>				
							<strong><?php echo $n->title ?></strong> 
							<br>
							From <?php echo ucfirst($n->type_id) ?>						
						</td>
						<?php if($categories->title == 'Branch'){ ;?>
							<td style="text-align: center"><?php echo $n->orders ;?></td>
						<?php } ;?>
						<td>									
							<?php if($categories->parent_id != 0){ ;?>
								<strong>
									<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="dtl">
										<?php echo $categories->title ;?>
									</a> 
								</strong>
								<br>
								 On <a id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="dtl">
									<?php echo $categories->parent_title ;?>
								</a>
							<?php }else{ ;?>
								<strong>
									<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="listcat" data-lang="2" class="dtl">
										<?php echo $categories->title ;?>
									</a>
								</strong>
							<?php } ;?>
						</td>
						<td><?php echo $n->name ?></td>
						<td style="width: 19%"><?php echo date("l, d M Y H:i:s", strtotime($n->datecreated)) ?></td>
						<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
						<td style="width: 20%">
							<!--
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="<?php if(strtolower($menu) == 'menu'){ echo 'edit_category' ;}else{ echo 'edit_object';} ?>" data-lang="2" class="dtl btn btn-info btn-xs pull-right" type="button" style="margin: 2px">Edit</button>
							-->
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ if($s->value != 2){ ;?>
									<button id="<?php echo $n->menu_id ?>" data-url="menu" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 2px"> <?php echo $s->name ;?></button>
								<?php ;} ;} ;?>
							<?php ;} ;?>
							<button id="<?php echo $n->menu_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="menu" data-title="<?php echo trim($n->title);?> from Website Menu"data-lang="2" class="modal_dlt btn btn-white btn-xs pull-right" type="button" style="margin: 2px">Delete</button>
							<?php if(($n->orders != 1)&&($c_page != $n->orders)){ ;?>
								<button id="<?php echo $n->menu_id ?>" data-url="sort" data-param="up<?php echo $n->orders?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Move Up" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-success btn-xs pull-right" type="button" style="margin: 2px"> Move Up</button>
								<button id="<?php echo $n->menu_id ?>" data-url="sort" data-param="dn<?php echo $n->orders?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Move Down" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-warning btn-xs pull-right" type="button" style="margin: 2px"> Move Down</button>
							<?php }elseif(($n->orders == 1)&&($c_page != $n->orders)){ ;?>							
								<button id="<?php echo $n->menu_id ?>" data-url="sort" data-param="dn<?php echo $n->orders?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Move Down" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-warning btn-xs pull-right" type="button" style="margin: 2px"> Move Down</button>
							<?php }elseif(($n->orders != 1)&&($c_page == $n->orders)){ ;?>
								<button id="<?php echo $n->menu_id ?>" data-url="sort" data-param="up<?php echo $n->orders?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Move Up" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-success btn-xs pull-right" type="button" style="margin: 2px"> Move Up</button>
							<?php } ;?>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
		<?php };?>
	</div>
</div>