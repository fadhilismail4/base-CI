
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-sm-7 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2">
					<?php echo ucwords($objects->title) ;?> 
				</a>
			</li>
		</ol>
		<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>		
	</div>
	<div class="ibox-content row" >	
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		<div class="row">
			<?php foreach($status as $s){ ;?>
				<?php if($objects->status != $s->value){ ;?>
					<button id="<?php echo $objects->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $objects->title ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 0 5px"> <?php echo $s->name ;?></button>
				<?php ;} ;?>
				
			<?php ;} ;?>
			<?php if($category->title == 'Header'){ ;?>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" class="detail2 btn btn-info ;?> btn-xs pull-right" data-url3="<?php echo $objects->metadescription?>" type="button" style="margin: 0 5px">Edit</button>
			<?php } ;?>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="objects" data-title="<?php echo trim($objects->title);?>"data-lang="2" class="modal_dlt btn btn-white ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Delete</button>
		</div>
		<div class="row" style="margin-top: 20px">	
			<p class="col-sm-2 col-xs-5">Category </p>
			<div class="col-sm-4 col-xs-7"><p>: <?php echo $category->title ;?></p></div>	
			<p class="col-sm-2 col-xs-5"><?php echo $category->title?> Source </p>
			<div class="col-sm-4 col-xs-7"><p>: <?php echo ucwords( $objects->metadescription) ;?></p></div>
			<?php if($objects->metadescription == 'socmed'){?>
			<p class="col-sm-2 col-xs-5">Socmed Type</p>
			<?php }else{ ;?>
			<p class="col-sm-2 col-xs-5">Name</p>
			<?php } ;?>
			<div class="col-sm-4 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>	
			<p class="col-sm-2 col-xs-5">Status</p>
			<div class="col-sm-4 col-xs-7"><p><strong>: <?php if($objects->status == 1){ echo "Active" ;}elseif($objects->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></strong></p></div>
			<p class="col-sm-2 col-xs-5">Date Created</p>
			<div class="col-sm-4 col-xs-7"><p>: <?php echo date("l, d M Y", strtotime($objects->datecreated))  ;?></p></div>
			<p class="col-sm-2 col-xs-5">Creator</p>
			<div class="col-sm-4 col-xs-7"><p>: <?php echo $objects->name ;?></p></div>
			
			<?php if($objects->metadescription == 'socmed'){?>
			<p class="col-sm-2 col-xs-12">Link </p>
			<div class="col-sm-10 col-xs-12">
				<p>: 
					<a href="<?php echo $objects->tagline ;?>" target="_blank" >
						<?php echo $objects->tagline ;?>
					</a>
				</p>
			</div>
			<p class="col-sm-2 col-xs-12" >Description</p>
				<div class="col-sm-10 col-xs-12"><p>: <?php echo $objects->description  ;?></p></div>
			<?php }elseif($objects->metadescription == 'page'){ ;?>
				<p class="col-sm-2 col-xs-12" >Description</p>
				<div class="col-sm-10 col-xs-12"><p>: <?php echo $objects->description  ;?></p></div>
			<?php }?>
		</div>
	</div>
</div>
<?php if($objects->metadescription == 'page'){ ;?>
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<h5 >Related <?php echo ucwords($objects->metadescription)?></h5>		
		<button id="<?php echo $relation->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_object" data-url3="<?php if($objects->metadescription == 'page'){ echo 'konten';}else{echo $objects->metakeyword ;} ;?>" class="detail2 btn-sm btn btn-success pull-right">View</button>	
	</div>
	<div class="ibox-content row" >
		<div class="row">
			<div class="col-sm-4 col-xs-12" style="padding-left: 0px">
				<img alt="image" class="col-md-12 img-responsive" src="
				<?php if($relation->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($relation->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-left: auto; margin-right: auto ">
			</div>	
			<div class="col-sm-8 col-xs-12" style="padding-top: 20px">
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5">Name</p>
					<div class="col-sm-4 col-xs-7">
						<p>
							<strong>: 
								<a id="<?php echo $relation->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_object" data-url3="page" class="detail2">
								<?php echo $relation->title?>
								</a>
							</strong>
						</p>
					</div>	
					<p class="col-sm-2 col-xs-5">Status</p>
					<div class="col-sm-3 col-xs-7"><p><strong>: <?php if($relation->status == 1){ echo "Active" ;}elseif($relation->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5">Creator</p>
					<div class="col-sm-4 col-xs-7"><p>: <?php echo $relation->name ;?></p></div>
					<p class="col-sm-2 col-xs-5">Type</p>
					<div class="col-sm-3 col-xs-7"><p><strong>: Single Page</strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-12" >Date Created</p>
					<div class="col-sm-9 col-xs-12"><p>: <?php echo date("l, d M Y", strtotime($relation->datecreated))  ;?></p></div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding-top: 20px">
				<p class="col-sm-12 col-xs-12" >Content</p>
				<div class="col-sm-12 col-xs-12"><p><?php echo $relation->description  ;?></p></div>
			</div>
		</div>
	</div>
</div>
<?php }elseif($objects->metadescription == 'module'){ ;?>
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<h5 >Related <?php echo ucwords($objects->metadescription)?></h5>	
		<button id="all" data-url="module" data-url2="<?php echo $relation->link?>" data-lang="2" data-param="list" data-url3="page" class="detail2 btn-sm btn btn-success pull-right">View</button>	
	</div>
	<div class="ibox-content row">
		<div class="row" style="margin-top: 20px">	
			<p class="col-sm-2 col-xs-5">Module </p>
			<div class="col-sm-4 col-xs-7"><p><strong>: <?php echo $relation->name ;?></strong></p></div>	
			<p class="col-sm-2 col-xs-5">Status</p>
			<div class="col-sm-4 col-xs-7"><p><strong>: <?php if($relation->status == 1){ echo "Active" ;}elseif($relation->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></strong></p></div>
			<p class="col-sm-2 col-xs-5">Creator</p>
			<div class="col-sm-4 col-xs-7"><p>: <?php echo $relation->name ;?></p></div>
			<p class="col-sm-2 col-xs-5">Date Created</p>
			<div class="col-sm-4 col-xs-7"><p>: <?php echo date("l, d M Y", strtotime($relation->datecreated))  ;?></p></div>
			
			<p class="col-sm-2 col-xs-12">Description</p>
			<div class="col-sm-10 col-xs-12"><p>: <?php echo $relation->description ;?></p></div>
		</div>
	</div>
</div>
<?php } ;?>