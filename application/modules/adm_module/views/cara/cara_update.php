
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-sm-7 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_object" data-lang="2" data-url3="<?php echo $objects->metadescription?>" class="detail2">
					Update
				</a>
			</li>
		</ol>
		<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>		
	</div>
	
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">		
		<input id="object_id" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->object_id ;?>">
		<input id="category_id" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->category_id ;?>">
		<input id="chiave" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->metadescription?>">
		<input id="simo" name="inputan" type="hidden" class="form-control" value="simo">		
		<input id="description" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->metakeyword?>">		
		<input id="title" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->title?>">
		<div class="col-sm-12 col-xs-12">		
			<div class="form-horizontal">	
				<div class="form-group" >
					<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important";><?php echo $category->title;?> Type</label>
					<div class="col-sm-4 col-xs-9" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="header_type">		
								<option value="">Select Type</option>	
								<option value="socmed" data-param="socmed">Socmed Type</option>			
								<option value="page" data-param="page">Page Type</option>				
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group" >					
					<div class="" id="data_4">
					<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important";>Socmed Type</label>
						<div class="col-sm-4 col-xs-9" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="socmed">		
									<option value="">Select Type</option>	
									<option value="facebook" data-param="facebook">Facebook</option>
									<option value="tripadvisor" data-param="tripadvisor">Tripadvisor</option>
									<option value="twitter" data-param="twitter">Twitter</option>
									<option value="google" data-param="google">Google +</option>			
									<option value="instagram" data-param="instagram">Instagram</option>		
									<option value="pinterest" data-param="pinterest">Pinterest</option>			
									<option value="youtube" data-param="youtube">Youtube</option>						
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" >
					<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important; ">Activation</label>
					<div class="col-sm-4 col-xs-9" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ if($s->value != 2){ ;?>
								<option value="<?php echo $s->value?>" data-title="<?php echo $s->name?>"><?php echo $s->name?></option>
								<?php ;};}?>
							</select>
						</div>
					</div>
				</div>
				<div class="" id="data_3">
					<div class="form-group">
						<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important; ">Page</label>
						<div class="col-sm-4 col-xs-12" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="page_id">
									<option value="">Select Page</option>
									<?php foreach($page as $p){ ;?>
										<option value="<?php echo $p->object_id?>" data-param="<?php echo $p->title?>"><?php echo $p->title?> Page</option>
									<?php } ;?>
								</select>
							</div>
						</div>
					</div>					
				</div>	
				<div class="" id="data_desc">
					<div class="form-group">					
						<label class="col-sm-2 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10">
							<textarea id="descs" name="inputan" type="text" class="form-control" value="" placeholder="A short description about your social media"></textarea>
						</div>
					</div>	
				</div>
				<div class="" id="data_soc">
					<div class="form-group">					
						<label class="col-sm-2 control-label" style="text-align: left !important">Link</label>
						<div class="col-sm-10">
							<input id="link" name="inputan" type="text" class="form-control" value="" placeholder="Copy link from your social media account to this field">
						</div>
					</div>				
					<div class="form-group">					
						<label class="col-sm-2 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10">
							<textarea id="desc" name="inputan" type="text" class="form-control" value="" placeholder="A short description about your social media">
							</textarea>
						</div>
					</div>	
				</div>
					
				<?php if($objects->metadescription == 'socmed'){ ;?>
				<div class="" id="data_5">
					<div class="form-group">					
						<label class="col-sm-2 col-xs-5 control-label" style="text-align: left !important">Account</label>
						<div class="col-sm-4 col-xs-7">
							<input  type="text" class="form-control" disabled value="<?php echo $objects->title ?>" >
						</div>
					</div>
					<div class="form-group">					
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Link</label>
						<div class="col-sm-10 col-xs-12 ">
							<input id="oldlink" name="inputan" type="text" class="form-control" value="<?php echo $objects->tagline ?>" >
						</div>
					</div>
					<div class="form-group">					
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10 col-xs-12 ">
							<textarea id="olddescs" name="inputan" type="text" class="form-control" ><?php echo trim($objects->description) ;?></textarea>
						</div>
					</div>
				</div>			
				<?php } ;?>
				<?php if($objects->metadescription == 'page'){ ;?>
				<div class="" id="data_2">
					<div class="form-group">					
						<label class="col-sm-2 col-xs-5 control-label" style="text-align: left !important">Title</label>
						<div class="col-sm-4 col-xs-7">
							<input  type="text" class="form-control " disabled value="<?php echo $objects->title ?>" ></div>
					</div>
					<div class="form-group">					
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10 col-xs-12 ">
							<textarea id="olddesc" name="inputan" type="text" class="form-control"  ><?php echo trim($objects->description) ;?></textarea>
						</div>
					</div>
				</div>
				<?php } ;?>
			</div>		
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="sitemap" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Update</button>
				<div class="space-25"></div>	
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>					
			</div>
		</div>
	</div>
</div>
<script>
$('#header_type').val('<?php echo $objects->metadescription;?>');
$('#status').val('<?php echo $objects->status;?>');
$(document).ready(function(){
	$('#data_3').hide();
	$('#data_4').hide();
	$('#data_desc').hide();
	$('#data_soc').hide();
	$('#header_type').change(function() {
		if ($(this).find(':selected').val() === 'socmed') {
			$('#data_4').slideDown('slow');
			$('#data_5').slideUp('slow');	
			$('#page_id').val('');
			$('#data_2').slideUp('slow');
		}else{
			$('#data_4').slideUp('slow');
			$('#data_soc').slideUp('slow');
			$('#data_desc').slideUp('slow');
		}
	
		if($(this).find(':selected').val() === 'page') {
			$('#data_3').slideDown('slow');
			$('#data_5').slideUp('slow');	
			$('#socmed').val('');
		}else{
			$('#data_3').slideUp('slow');
			$('#data_soc').slideUp('slow');
			$('#data_desc').slideUp('slow');
		}
		
		if($(this).find(':selected').val() == '') {
			$('#data_2').slideDown('slow');		
			$('#data_5').slideDown('slow');				
			$('#socmed').val('');
			$('#page_id').val('');
		}else{
			$('#data_2').slideUp('slow');
			$('#data_soc').slideUp('slow');
			$('#data_desc').slideUp('slow');
		}
	});
	$('#page_id').change(function() {
		if($(this).find(':selected').val() != '') {
			var key = $(this).find(':selected').data('param');
			if(key != '<?php echo $objects->title ;?>'){
				$('#data_desc').slideDown('slow');
				$('#data_2').slideUp('slow');	
			}else{
				$('#data_desc').slideUp('slow');
				$('#data_2').slideDown('slow');	
			};
			$('#simo').val('');	
			$('#simo').val(''+key+'');	
		}else{
			$('#simo').val('');	
		}		
	});
	
	$('#socmed').change(function() {
		if($(this).find(':selected').val() != '') {
			var key = $(this).find(':selected').data('param');
			if(key != '<?php echo $objects->title ;?>'){
				$('#data_soc').slideDown('slow');
				$('#data_5').slideUp('slow');	
			}else{
				$('#data_soc').slideUp('slow');
				$('#data_5').slideDown('slow');	
			};
		}else{	
			$('#data_soc').slideUp('slow');
		}		
	});
});
</script>