<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					All <?php echo $key_name?> Content
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
		</ol>
		<?php } ;?>
		<div class="ibox-tools">
			<?php if(($id) && ( strtolower($categories->title) == 'menu')){ ;?>
				
				<button id="<?php echo $ccs_key;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new_catego" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-primary">Add New</button>
				<button id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Back</button>
			<?php }elseif(($id) && ( strtolower($categories->title) == 'header')){?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Add New</a>
				<button id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Back</button>
			<?php }elseif(($id) && ( strtolower($categories->title) == 'page')){?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Add New</a>
				<button id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Back</button>
			<?php }elseif(($id) && ( strtolower($categories->title) == 'footer')){?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Add New</a>
				<button id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Back</button>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>It's empty here, please create your sitemap content... </h3>
		<?php }else{ ;?>
		<div class="table-responsive" style="border: 0;">
			<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th>Title </th>
						<th>Parent Category </th>
						<th>Createdby</th>
						<th>Date Created </th>
						<th>Status</th>
						<th>Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($objects as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_object" data-url3="<?php echo strtolower($n->pcat)?>"  class="dtl">
								<?php echo $n->title ?>
							</a>
						</td>
						
						<td>
							<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_category" class="dtl">
								<?php if($n->category_id != 0){ echo $n->pcat ;} ;?>
							</a>
						</td>
						<td><?php echo $n->name ?></td>
						<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
						<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
						<td>
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ ;?>
									<button id="<?php echo $n->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
								<?php ;} ;?>
							<?php ;} ;?>
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="<?php if(strtolower($menu) == 'menu'){ echo 'edit_category' ;}else{ echo 'edit_object';} ?>" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Edit</button>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
		<?php };?>
	</div>
</div>