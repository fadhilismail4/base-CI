<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">


<style>
	.modal-backdrop.in{
		display: none !important;
	}
	.fileinput-upload-button{
		display:none;
	}
	/* .input-group-btn > .btn-file{
		display:none;
	} */
	.checkbox label{
		padding-left:0px;
	}
</style>

<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-sm-8 col-xs-5" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_link);?>				
				</a>
			</li>
			<li class="">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $cat->title ;?>
				</a>
			</li>
			<li class="active">
				<a id="0" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
					New
				</a>
			</li>
		</ol>
		<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
	</div>
	<div class="ibox-content row">
		<div class="col-sm-4 col-xs-12" style="margin-bottom: 20px">
			<input id="image_square" name="image_square" class="file " type="file" value="">
		</div>
		<div class="col-sm-8 col-xs-12">		
			<div class="form-horizontal">	
				<div class="form-group">
					<label class="col-sm-2 control-label" style="text-align: left !important">Title</label>
					<div class="col-sm-10">
						<input id="title" name="inputan" type="text" class="form-control" value="">
					</div>
				</div>
				<div class="form-group" >
					<label class="col-sm-2 control-label" style="text-align: left !important";>Category</label>
					<div class="col-sm-10" >
						<div class="input-group col-sm-8" >
							<select class="form-control m-b" name="inputan" id="category_id">	
								<option value="">Select Category</option>								
								<?php foreach($category as $cat){
									if($cat->title != 'Home'){
								?>
								<option value="<?php echo $cat->category_id ;?>"><?php echo $cat->title ;?></option>
									<?php ;} ;} ;?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group" >
					<label class="col-sm-2 control-label" style="text-align: left !important; ">Publish</label>
					<div class="col-sm-10" >
						<div class="input-group col-sm-8" >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="clearfix" id="data_2">
						<label class="control-label col-sm-2" style="text-align: left !important; ">Publish on</label>
						<div class="col-sm-10">
							<div class="input-group date input-group col-sm-6">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input id="datecreated" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">					
					<div class="space-25"></div>
					<button id="<?php echo $key_link;?>" data-param="sitemap" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Create</button>
					<div class="space-25"></div>	
					<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ibox float-e-margins">
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
		<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $zone_id?>">
		<input id="counter" name="inputan" type="hidden" class="form-control" value="<?php echo $c_o?>">
		<input id="object_id" name="inputan" type="hidden" class="form-control" value="">
		<form class="form-horizontal" method="get">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Content</label>
			</div>
		</form>
		<?php if( strtolower($cat->title) != 'banner'){?>
		<div class="mail-box">
			<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
				<div id="description" name="inputan_summer" class="summernote">
					
				</div>
			</div>
		
			<div class="mail-body text-right tooltip-demo">
			</div>
		</div>
		<?php }else{ ;?>
			<div class="col-sm-12" style="padding: 0"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
		<?php } ;?>
	</div>
</div>
<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<h5>SEO SETTINGS</h5>
	</div>
	<div class="ibox-content row">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Keywords</label>
				<div class="col-sm-10">
					<textarea id="metakeyword" name="inputan" type="text" class="form-control" value=""></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Meta Description</label>
				<div class="col-sm-10">
					<textarea id="metadescription" name="inputan" type="text" class="form-control" value=""></textarea>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>
<script>
	$(document).ready(function(){
		$('#data_2').hide();
		$('#status').change(function() {
			if ($(this).find(':selected').val() === '2') {
				$('#data_2').slideDown('slow');
			} else {
				$('#data_2').slideUp('slow');
			}
		});
	});
</script>
<script>
$(document).ready(function(){
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
	
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php echo base_url('assets').'/img/image_empty.png' ;?>' class='file-preview-image'>"
		]
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>
