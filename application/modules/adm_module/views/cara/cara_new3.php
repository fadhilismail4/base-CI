<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-8" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_link);?>				
				</a>
			</li>
			<li class="">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $cat->title?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
					New
				</a>
			</li>
		</ol>
		<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right ">Back</button>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
		<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $zone_id?>">
		<input id="image_square" name="inputan" type="hidden" class="form-control" value="no-footer.jpg">
		<input id="description" name="inputan" type="hidden" class="form-control" value="footer">
		<input id="title" name="inputan" type="hidden" class="form-control" value="title">
		<input id="category_id" name="inputan" type="hidden" class="form-control" value="<?php echo $cat->category_id ;?>">
		
		<div class="form-horizontal" method="get">	
			<div class="form-group" >
				<div class="col-sm-6 col-xs-12 ">
					<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; padding-left: 0px">Footer Type</label>
					<div class="col-sm-8 col-xs-8" style=" padding-left: 10px" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="footer_type">
								<option value="">Select Type</option>	
								<option value="header">From Header</option>			
								<option value="menu">From Menu</option>	
							</select>
						</div>
					</div>
				</div>
					
				<div class="col-sm-6 col-xs-12">
					<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; padding-left: 0px ">Activation</label>
					<div class="col-sm-8 col-xs-8" style=" padding-left: 10px" >
						<div class="input-group col-sm-10 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ if($s->value != 2){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php ;};}?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group" >
				<div class="" id="data_1">
					<label class="col-sm-2 control-label" style="text-align: left !important";>Header Type</label>
					<div class="col-sm-4" >
						<div class="input-group col-sm-11" >
							<select class="form-control m-b" name="inputan" id="objects_id">		
								<option value="">Select Header</option>									
								<?php if($category){?>
								<?php foreach($category as $c){ ;?>
									<option value="<?php echo $c->object_id?>"><?php echo $c->title ?> ( <?php echo ucwords($c->metadescription) ;?> )</option>			
								<?php } ;?>						
								<?php } ;?>					
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group" >
				<div class="" id="data_2">
					<label class="col-sm-2 control-label" style="text-align: left !important";>Menu Type</label>
					<div class="col-sm-4" >
						<div class="input-group col-sm-11" >
							<select class="form-control m-b" name="inputan" id="cat_id">		
								<option value="">Select Menu</option>
								<?php if($menu){?>
								<?php foreach($menu as $m){ ;?>
									<option value="<?php echo $m->object_id?>"><?php echo $m->title ?> ( <?php echo $m->type_id?> )</option>			
								<?php } ;?>
								<?php } ;?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="" id="data_3">
					<label class="col-sm-2 control-label" style="text-align: left !important">Title</label>
					<div class="col-sm-8">
						<input id="ttl" name="inputan" type="text" class="form-control" value="">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="" id="data_4">
					<label class="col-sm-2 control-label" style="text-align: left !important">Description</label>
					<div class="col-sm-8">
						<textarea id="desc" name="inputan" type="text" class="form-control" value=""></textarea>
					</div>
				</div>
			</div>
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="sitemap" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Create</button>
				<div class="space-25"></div>					
				<?php if ($id == 0){ ;?>
					<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
				<?php }else{ ;?>
					<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
				<?php } ;?>
			</div>
		</div>

	</div>
</div>
<script>
	$(document).ready(function(){
		$('#data_1').hide();
		$('#data_2').hide();
		$('#data_3').hide();
		$('#data_4').hide();
		$('#footer_type').change(function() {
			if ($(this).find(':selected').val() === 'header') {
				$('#data_1').slideDown('slow');
			} else {
				$('#data_1').slideUp('slow');
			}
			if ($(this).find(':selected').val() === 'menu') {
				$('#data_2').slideDown('slow');
			} else {
				$('#data_2').slideUp('slow');
			}
		});
		
	});
</script>