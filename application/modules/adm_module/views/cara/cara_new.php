<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-8 col-xs-8" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_link);?>				
				</a>
			</li>
			<li class="">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $cat->title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $cat->category_id ?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
					New
				</a>
			</li>
		</ol>
		<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right ">Back</button>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">		
		<input id="description" name="inputan" type="hidden" class="form-control" value="socmed">		
		<input id="title" name="inputan" type="hidden" class="form-control" value="socmed">
		<input id="category_id" name="inputan" type="hidden" class="form-control" value="<?php echo $cat->category_id ;?>">
		<div class="col-sm-12 col-xs-12">		
			<div class="form-horizontal">	
				<div class="form-group" >
					<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important";>Header Type</label>
					<div class="col-sm-4 col-xs-9" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="header_type">		
								<option value="">Select Type</option>	
								<option value="socmed">Socmed Type</option>			
								<option value="page">Page Type</option>				
							</select>
						</div>
					</div>
				</div>
				<div class="form-group" >					
					<div class="" id="data_4">
					<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important";>Socmed Type</label>
						<div class="col-sm-4 col-xs-9" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="socmed">		
									<option value="">Select Type</option>	
									<option value="facebook">Facebook</option>			
									<option value="tripadvisor">Tripadvisor</option>
									<option value="twitter">Twitter</option>
									<option value="google">Google +</option>			
									<option value="instagram">Instagram</option>		
									<option value="pinterest">Pinterest</option>			
									<option value="youtube">Youtube</option>						
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" >
					<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important; ">Activation</label>
					<div class="col-sm-4 col-xs-12" >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ if($s->value != 2){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php ;};}?>
							</select>
						</div>
					</div>
				</div>
				<div class="" id="data_3">
					<div class="form-group">
						<label class="col-sm-2 col-xs-3 control-label" style="text-align: left !important; ">Page</label>
						<div class="col-sm-4 col-xs-12" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="page_id">
									<option value="">Select Page</option>
									<?php foreach($page as $p){ ;?>
										<option value="<?php echo $p->object_id?>"><?php echo $p->title?> Page</option>
									<?php } ;?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">					
						<label class="col-sm-2 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10">
							<textarea id="desc" name="inputan" type="text" class="form-control" value="" placeholder="A short description about your social media">
							</textarea>
						</div>
					</div>	
				</div>
				<div class="" id="data_1">
					<div class="form-group">					
						<label class="col-sm-2 control-label" style="text-align: left !important">Link</label>
						<div class="col-sm-10">
							<input id="link" name="inputan" type="text" class="form-control" value="" placeholder="Copy link from your social media account to this field">
						</div>
					</div>
					<div class="form-group">					
						<label class="col-sm-2 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10">
							<textarea id="descriptions" name="inputan" type="text" class="form-control" value="" placeholder="A short description about your social media">
							</textarea>
						</div>
					</div>
				</div>
			</div>		
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="sitemap" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Create</button>
				<div class="space-25"></div>	
				<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>					
			</div>
		</div>
	</div>
</div>
<div class="ibox float-e-margins" id="data_content2">
	<div class="ibox-title row">
		<h5>SEO SETTINGS</h5>
	</div>
	<div class="ibox-content row">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Meta Keywords</label>
				<div class="col-sm-10 col-xs-12">
					<textarea id="metakeyword" name="inputan" type="text" class="form-control" value="" placeholder="Example : Author: J. K. Rowling, Illustrator: Mary GrandPré, Category: Books, Price: $17.99, Length: 784 pages [ Only 100 characters ]"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important" >Meta Description</label>
				<div class="col-sm-10 col-xs-12">
					<textarea id="metadescription" name="inputan" type="text" class="form-control" value="" placeholder="Example : Make sure that every page on your site has a meta description. [ Only 200 characters ]"></textarea>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#data_content2').hide();
		$('#data_1').hide();
		$('#data_3').hide();
		$('#data_4').hide();
		$('#header_type').change(function() {
			if ($(this).find(':selected').val() === 'socmed') {
				$('#data_1').slideDown('slow');
				$('#data_4').slideDown('slow');
			}else{
				$('#data_1').slideUp('slow');
				$('#data_4').slideUp('slow');
			}
			
			if($(this).find(':selected').val() === 'page') {
				$('#data_3').slideDown('slow');
			}else{
				$('#data_3').slideUp('slow');
			}
		});
	});
</script>