<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-10 col-xs-10"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
		</ol>
		<?php if($chiave == 'konten'){ ;?>
			<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php }else{ ;?>
			<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php } ;?>
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			<div class="col-sm-6 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Konten</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Gallery</button>
				<!--<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="relation" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Relation</button>
				-->
			</div>
			<div class="col-sm-6 col-xs-12">
			<?php if($chiave == 'konten'){ ;?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="objects" data-title="<?php echo trim($objects->title);?>"data-lang="2" class="modal_dlt btn btn-white  btn-xs pull-right" type="button" style="margin: 5px">Delete</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="konten" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
				<?php foreach($status as $s){ ;?>
					<?php if($objects->status != $s->value){ ;?>
						<button id="<?php echo $objects->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $objects->title ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 5px"> <?php echo $s->name ;?></button>
					<?php ;} unset($s) ;?>				
				<?php ;} ;?>				
			<?php }else{ ;?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="seo" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
			<?php } ;?>
			</div>
		</div>
		<div class="row" >
			<?php if($chiave == 'konten'){ ;?>
				<div class="col-sm-4 col-xs-12">
					<img alt="image" class="img-responsive" src="
					<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto; margin-bottom: 20px">
				</div>		
				<div class="col-sm-8 col-xs-12" style="padding-left: 0px; ">
					<p class="col-sm-4 col-xs-5">Name</p>
					<div class="col-sm-8 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
					<p class="col-sm-4 col-xs-5">Category</p>
					<div class="col-sm-8 col-xs-7">
						<p>: 
							<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $category->title?>
							</a>
						</p>
					</div>
					<p class="col-sm-4 col-xs-5">Status</p>
					<div class="col-sm-8 col-xs-7">	
						<p><strong>: 
							<?php foreach($status as $s){ ;?>
								<?php if($objects->status == $s->value){ ;?>
									<?php echo $s->info?>
							<?php ;} unset($s) ;?>				
						<?php ;} ;?>	
						</strong></p>
					</div>
					<p class="col-sm-4 col-xs-5">Date Created</p>
					<div class="col-sm-8 col-xs-7"><p>: <?php echo date("l, d M Y", strtotime($objects->datecreated))  ;?></p></div>
					<?php if($objects->status == 2){ ;?>
						<p class="col-sm-4 col-xs-5">Publish Scheduled</p>
						<div class="col-sm-8 col-xs-7"><p style="color: #f39c12"><strong>: <?php echo date("l, d M Y", strtotime($objects->datepublish))  ;?></strong></p></div>
					<?php };?>
					<p class="col-sm-4 col-xs-5">Creator</p>
					<div class="col-sm-8 col-xs-7"><p>: <?php echo $objects->name ;?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<?php if( strtolower($category->title) == 'banner'){ ;?>
						<p class="col-sm-2 col-xs-12">Content</p>
						<div class="col-sm-10"><p>: <?php echo $objects->description ;?></p></div>
					<?php }else{ ;?>
						<p class="col-sm-12 col-xs-12">Content :</p>
						<div class="col-sm-12 col-xs-12"><p><?php echo  $objects->description ;?></p></div>					
					<?php } ;?>		
				</div>
			<?php }elseif($chiave == 'seo'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5">Name</p>
					<div class="col-sm-10 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
					<p class="col-sm-2 col-xs-5">Meta Keyword</p>
					<div class="col-sm-10 col-xs-7">
						<p>: 
							 <?php echo $objects->metakeyword?>
						</p>
					</div>
					<p class="col-sm-2 col-xs-5">Meta Description</p>
					<div class="col-sm-10 col-xs-7">
						<p>: 
							 <?php echo $objects->metadescription?>
						</p>
					</div>
					<p class="col-sm-2 col-xs-5">Viewer</p>
					<div class="col-sm-10 col-xs-7">
						<p>: 
							 <?php echo $objects->viewer?> views
						</p>
					</div>
				</div>
			<?php }else{ ;?>
				<?php if($rcount == 0){ ;?>
				<?php }else{ ;?>
				<?php }?>
			<?php } ;?>
		</div>
	</div>
</div>