<div class="ibox ">
	<div class="ibox-title row" >
		<ol class="breadcrumb col-md-10 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<?php if($category->parent_id != 0){ ;?>
			<li class="">
				<a id="<?php echo $category->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->parent_title;?> 
				</a>
			</li>	
			<?php } ;?>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" data-url3="gallery" class="detail2">
					Gallery
				</a>
			</li>
		</ol>
		<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_category" data-lang="2" data-url3="konten" class="detail2  pull-right btn-sm btn btn-warning pull-right">Back</button>
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			<div class="col-sm-8 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_category" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Gallery</button>
				<!--
				<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_category" data-lang="2" data-url3="trx" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Transaction</button>-->
			</div>
			<div class="col-sm-4 col-xs-12">				
				<button id="0" data-url="<?php echo $category->category_id ;?>" data-url2="<?php echo $category->ccs_key ;?>"  data-lang="2" class="modal_upload_data btn-xs btn btn-primary pull-right" style="margin: 0 5px">Add Gallery</button>
			</div>
		</div>
		<div class="row" >
			<?php if($chiave == 'gallery'){ ;?>		
				<div class="col-sm-12 col-xs-12" style ="padding-top: 20px;">
					<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
						<thead>
							<tr>
								<th>No</th>
								<th style="text-align: center">Image </th>
								<th style="text-align: center">Caption </th>
								<th style="text-align: center">Category</th>
								<th style="text-align: center">Date Created </th>
								<th style="text-align: center">Creator </th>
								<th style="text-align: center">Status </th>
								<th style="text-align: center">Activation</th>
							</tr>
						</thead>
						<tbody>
							<?php if($category->image_square){;?>
							<tr class="gradeX">
								<td>1</td>
								<td style="width: 15%; ">
									<img src="<?php echo base_url()?>assets/<?php echo $zone?>/category/<?php echo $category->image_square?>" style="width: 100%">
								</td>
								<td>
									<?php if($category->title){ echo $category->title;}else{echo "No caption";} ;?>
								</td>
								<td>
									<?php if($category->istatus == 0){ echo "Main Image";}elseif($category->istatus == 1){ echo "Banner Image" ;}else{ echo "Gallery" ;} ;?>
								</td>
								<td>
									<?php echo date("l, d M Y", strtotime($category->datecreated)) ?> at
									<?php echo date("H:i:s", strtotime($category->datecreated)) ?>
								</td>
								<td>
									<?php echo $category->name?>
								</td>
								
								<td>
									<?php foreach($gstatus as $g){ ;?>
										<?php if($category->status == $g->value){ ;?>
											<?php echo $g->info ;?>
										<?php ;};?>
									<?php ;} unset($g) ;?>
									
								</td>
								<td style="width: 25%">
									<?php if($category->istatus == 2){ ;?>
									<button id="<?php echo $category->category_id ?>" data-url="gallery" data-param="1" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Banner" data-title="<?php if($category->title){ echo $category->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Banner</button>
									<button id="<?php echo $category->category_id ?>" data-url="gallery" data-param="0" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Main Image" data-title="<?php if($category->title){ echo $category->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Main</button>
									<?php } ;?>
									<?php if(isset($category_image)){;?>
										<a class="fancybox btn-xs btn btn-success pull-right" title="<?php if($category->title){ echo $category->title;}else{echo "No caption";} ;?>"  rel="gallery" href="<?php echo base_url()?>assets/<?php echo $zone?>/category/<?php echo $category->image_square?>" style="margin: 1px">View</a>
										<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_category" data-lang="2" class="detail2 btn-xs btn btn-info pull-right" style="margin: 1px" >Edit</button>
									<?php };?>
									
									<?php if($category->istatus != 0){ ;?>
									<?php } ;?>
								</td>
							</tr>
							<?php };?>
							<?php if($gcount != 0){ ;?>			
							<?php $i = 2; foreach($gallery as $gal){ if(file_exists('assets/'.$zone.'/gallery/'.$gal->image_square)){ ?>								
								<tr class="gradeX">
									<td><?php echo $i ?></td>
									<td style="width: 15%; ">
										<img src="<?php echo base_url()?>assets/<?php echo $zone?>/gallery/thumbnail/<?php $path = 'assets/'.$zone.'/gallery/thumbnail/'.$gal->image_square ;if(file_exists($path)){ echo $gal->image_square ;}else{ echo str_replace('.','_thumb.', $gal->image_square) ;} ;?>" style="width: 100%">
									</td>
									<td>
										<?php if($gal->title){ echo $gal->title;}else{echo "No caption";} ;?>
									</td>
									<td>
										<?php if($gal->istatus == 0){ echo "Main Image";}elseif($gal->istatus == 1){ echo "Banner Image" ;}else{ echo "Gallery" ;} ;?>
									</td>
									<td>
										<?php echo date("l, d M Y", strtotime($gal->datecreated)) ?> at
										<?php echo date("H:i:s", strtotime($gal->datecreated)) ?>
									</td>
									<td>
										<?php echo $gal->name?>
									</td>
									<td>
										<?php foreach($gstatus as $g){ ;?>
											<?php if($gal->status == $g->value){ ;?>
												<?php echo $g->info ;?>
											<?php ;};?>
										<?php ;} unset($g) ;?>
										
									</td>
									<td>
										<a class="fancybox btn-xs btn btn-success pull-right" title="<?php if($gal->title){ echo $gal->title;}else{echo "No caption";} ;?>"  rel="gallery" href="<?php echo base_url()?>assets/<?php echo $zone?>/gallery/<?php echo $gal->image_square?>" style="margin: 1px">View</a>
										<button id="<?php echo $gal->gallery_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="gallery" data-title="<?php if($gal->title){ echo ucwords($gal->title." image");}else{echo "No caption";} ;?>"data-lang="2" class="modal_dlt btn-xs btn btn-danger pull-right" style="margin: 1px">Delete</button>
										<button id="<?php echo $gal->gallery_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-lang="2" data-url3="gallery" class="dtl btn-xs btn btn-info pull-right" style="margin: 1px" >Edit</button>
										<?php if($gal->istatus == 2){ ;?>
										<button id="<?php echo $gal->gallery_id ?>" data-url="setup" data-param="1" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Banner" data-title="<?php if($gal->title){ echo $gal->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Banner</button>
										<button id="<?php echo $gal->gallery_id ?>" data-url="setup" data-param="0" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Main Image" data-title="<?php if($gal->title){ echo $gal->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Main</button>
										<button id="<?php echo $gal->gallery_id ?>" data-url="setup" data-param="3" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Main Image" data-title="<?php if($gal->title){ echo $gal->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Mobile</button>
										<?php }elseif($gal->istatus == 3){ ;?>
										<button id="<?php echo $gal->gallery_id ?>" data-url="setup" data-param="1" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Banner" data-title="<?php if($gal->title){ echo $gal->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Banner</button>
										<button id="<?php echo $gal->gallery_id ?>" data-url="setup" data-param="0" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Main Image" data-title="<?php if($gal->title){ echo $gal->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Main</button>
										<button id="<?php echo $gal->gallery_id ?>" data-url="setup" data-param="2" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="Set as Main Image" data-title="<?php if($gal->title){ echo $gal->title." image";}else{echo "No caption";} ;?>" class="modal_stat btn-xs btn btn-primary pull-right" style="margin: 1px" >Set as Gallery</button>
										<?php } ;?>
										<?php foreach($gstatus as $g){ ;?>
											<?php if($gal->status != $g->value){ ;?>
												<button id="<?php echo $gal->gallery_id ?>" data-url="gallery" data-param="<?php echo $g->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $g->name ;?>" data-title="<?php if($gal->title){ echo $gal->title;}else{echo "No caption";} ;?>" class="modal_stat btn btn-<?php echo $g->color ;?> btn-xs pull-right" type="button" style="margin: 2px"> <?php echo $g->name ;?></button>
											<?php ;} ;?>
										<?php ;} ;?>
									</td>
								</tr>
								<?php ;}$i++;} unset($gal);?>
								<?php } ;?>
						</tbody>
					</table>
				</div>
			<?php } ;?>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox-thumbs.js"></script>
<script id="fancy_script" type="text/javascript">
if (!$("link[href='<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css']").length){
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">').appendTo("head");
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox-thumbs.css" rel="stylesheet">').appendTo("head");
	$(document).ready(function() {
		$(".fancybox")
			.fancybox({
				prevEffect	: 'none',
				nextEffect	: 'none',
				helpers : {
					title : {
						type: 'inside',
						margin: 'auto'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					},
					buttons	: {}
				}  
			});
	});
}
</script>   