<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5><?php echo $key_name ;?> Category</h5>
	</div>
	<div class="ibox-content">
		<div class="row" style="margin: 0px;">
			<?php if ( !(count($category) > 3)){ ;?>
			<a id="<?php echo $ccs_key;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new_category" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-primary col-md-6">Add Category</a>
			
			<a id="<?php echo $ccs_key;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="<?php echo $language_id;?>" data-param="view_category" class="detail2 btn btn-sm btn-success col-md-6">View Category</a>
			<div class="space-15"></div>
			<?php }else { ;?>
				<a id="<?php echo $ccs_key;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="<?php echo $language_id;?>" data-param="view_category" class="detail2 btn btn-sm btn-success col-md-12">View Category</a>
				<div class="space-15"></div>
			<?php } ;?>
		</div>	
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
							<span class="label label-info"></span> <strong>View All</strong>
							</a>
							<p class="pull-right">(<?php echo $cat_count?>)</p> 
						</div>
					</li>
					<?php if(!empty($category)){;?>
					<?php foreach($category as $c){;?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<span class="label label-info"></span> <?php echo $c['parent']->title;?>							
							<p class="pull-right"><?php echo '('.$c['count'].')' ;?></p> 
						</div>
						<ol class="dd-list">
						<?php foreach($c['child'] as $cc){;?>
							<li class="dd-item">
								<div class="dd-handle dd-nodrag">
									<a id="<?php echo $cc['child']->category_id;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="<?php echo $category[0]['parent']->language_id;?>" data-param="list" class="detail2">
									<span class="label label-info"></span> <?php echo word_limiter($cc['child']->title, 2);?>
									</a>
									<p class="pull-right"><?php echo '('.$cc['count'].')' ;?></p> 
								</div>
							</li>		
						<?php ;} ;?>
						</ol>				
					</li>		
					<?php ;};?>
					<?php ;}else{;?>
					<br>No Category Added
					<?php };?>
				</ol>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>	