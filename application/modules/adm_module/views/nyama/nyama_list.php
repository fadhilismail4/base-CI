<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					All <?php echo $key_name?> Content
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<?php if($categories->parent_id == 0){ ;?>
			<li class="active">
				<a id="<?php echo $categories->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php }else{ ;?>
			<li class="">
				<a id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="detail_category" data-lang="2" class="detail2">
					<?php echo $categories->parent_title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php } ;?>
		</ol>
		<?php } ;?>
		<div class="ibox-tools">
			<?php if ($id == 'all'){ ;?>
				
				<a id="0" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Add New</a>
			<?php }else{ ;?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Add New</a>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>It's empty here, please create your sitemap content... </h3>
		<?php }else{ ;?>
		<div class="table-responsive" style="border: 0;">
			<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th>Title </th>
						<th>Category </th>
						<th>Createdby</th>
						<th>Date Created </th>
						<th>Status</th>
						<th>Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($objects as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_object" class="detail2">
								<?php echo $n->title ?>
							</a>
						</td>
						
						<td>
							<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="list" class="detail2">
								<?php echo $n->pcat ?>
							</a>
						</td>
						<td><?php echo $n->name ?></td>
						<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
						<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
						<td>
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ ;?>
									<button id="<?php echo $n->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
								<?php ;} ;?>
							<?php ;} ;?>
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Edit</button>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
		<?php };?>
	</div>
</div>