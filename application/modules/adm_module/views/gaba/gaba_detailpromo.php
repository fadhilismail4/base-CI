
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="promo" class="detail2">
					Promo List
				</a>
			</li>
		</ol>		
		<button id="<?php echo $category->category_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right" style="margin-left: 5px">Back</button>	
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		
		<div class="row " style="padding-bottom: 10px">
			<div class="col-sm-6 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Basic Info</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="promo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Promo List</button>
				
			</div>
			<div class="col-sm-6 col-xs-12">
				<button id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="product" class="detail2 btn-xs btn btn-primary pull-right">Add Product</button>	
			</div>
		</div>
		<div class="row" style="margin-top: 10px !important">
			<div class="col-sm-4 col-xs-12" >
				<img alt="image" class="img-responsive" src="
				<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto;" >
			</div>
			<div class="col-sm-8 col-xs-12">
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px; padding-top: 10px">
					<p class="col-sm-4 col-xs-4" style="padding-left: 0px">Name</p>
					<div class="col-sm-8 col-xs-8"><p><strong>: <?php echo $objects->title?></strong></p></div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-4" style="padding-left: 0px">Promo Code</p>
					<div class="col-sm-8 col-xs-8"><p><strong>: <?php echo $objects->tagline?></strong></p></div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-4" style="padding-left: 0px">Category</p>
					<div class="col-sm-8 col-xs-8">
						<p>: 
							<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $category->title?>
							</a>
						</p>
					</div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-4" style="padding-left: 0px">Discount</p>
					<div class="col-sm-8 col-xs-8"><p><strong>: <?php echo $objects->love?> %</strong></p></div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-4" style="padding-left: 0px">Promo Date</p>
					<div class="col-sm-8 col-xs-8">
						<p>: <?php echo date("d M Y", strtotime($objects->datestart))  ;?> - 
						<?php echo date("d M Y", strtotime($objects->dateend))  ;?>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="margin-top: 20px">
				<?php if($pcount == 0){ ;?>	
					<h3>It's empty here, please create your promo from your product list... </h3>
				<?php }else{ ;?>
				<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
					<thead>
						<tr>
							<th>No</th>
							<th style="text-align: center">Product </th>
							<th>Varian </th>
							<th style="text-align: center">Price </th>
							<th>Discount</th>
							<th>Stock</th>
							<th>Sold</th>
							<th style="text-align: center">Date Created</th>
							<th>Status</th>
							<th style="text-align: center">Activation</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; foreach($promo as $n){ ?>
						<tr class="gradeX">
							<td><?php echo $i ?></td>
							<td>
								<a id="<?php echo $n->ticket_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-lang="2" data-param="detail_custom" data-url3="<?php if($n->varian_id != 0){ echo 'varian' ;}else{ echo 'price' ;} ;?>" class="dtl">
									<p style="font-weight: bold"><?php echo $n->object ?></p> 
								</a>
								<a id="<?php echo $n->catid?>" data-url="module" data-url2="product" data-lang="2" data-param="list" data-url3="list" class="dtl">
									<p>on <?php echo $n->category ?> Category</p>
								</a>
							</td>
							
							<td>
								<?php if( $n->vstatus == 1){echo $n->varian." Varian"  ; }else{ echo "All Varian" ;} ;?>
							</td>
							<?php if($n->status == 1){ ;?>
							<td> Rp.<?php echo number_format(($n->publish -($n->publish *($n->discount / 100))),2,',','.') ;?>
								<br>
								<span style='color:#f39c12;text-decoration:line-through'>
								  <span style='color:#f39c12'>Rp.<?php echo  number_format($n->publish,2,',','.');?></span>
								</span>
								
							</td>
							<td><?php echo $n->discount ?>%</td>
							<?php }else{ ;?>
							
							<td> Rp.<?php echo number_format($n->publish ,2,',','.') ;?>								
							</td>
							<td>Discount is inactive</td>
							<?php } ;?>
							<td><?php echo $n->stock ?></td>
							<td><?php echo $n->sold ?></td>
							<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
							<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
							<td>				
								<?php foreach($status as $s){ ;?>
									<?php if($n->status != $s->value){if($s->value != 2){ ;?>
										<button id="<?php echo $n->ticket_id ?>" data-url="promo" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->object ;?> promo" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 2px"> <?php echo $s->name ;?></button>
									<?php } ;} ;?>
								<?php ;} ;?>	
								<button id="<?php echo $n->ticket_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_custom" data-url3="<?php if($n->varian_id != 0){ echo 'varian' ;}else{ echo 'price' ;} ;?>" data-lang="2" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 2px">View</button>		
								<button id="<?php echo $n->ticket_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_custom" data-url3="<?php if($n->varian_id != 0){ echo 'varian' ;}else{ echo 'price' ;} ;?>" data-lang="2" class="dtl btn btn-info btn-xs pull-right" type="button"  type="button" style="margin: 2px">Edit</button>					
							</td>
						</tr>	
						<?php $i++;} ?>
					</tbody>
				</table>				
				<?php } ;?>
			</div>
		</div>
	</div>
</div>