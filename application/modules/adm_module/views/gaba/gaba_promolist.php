
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="active">
				<a id="0" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="promo_list" data-lang="2" class="detail2">
					Promo List
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<!--
			<button id="0" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new_promos" data-lang="2" class="detail2 btn-sm btn btn-primary ">Add New</button>
			-->
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Back</button>
		</div>		
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
				
		<div class="row col-sm-12" style="margin : 0px">
		
		<?php if($c_trx == 0){ ;?>			
			<div class="row pull-left" style="padding-bottom: 0px !important">
				<h4>There is no product that used this promo code.</h4>
			</div>
		<?php }else{ ;?>
			<div class="row">
				<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
					<thead>
						<tr>
							<th>No</th>
							<th>Title </th>
							<th>Category</th>
							<th>Discount</th>
							<th>Stock</th>						
							<th>Promo Date</th>
							<th>Creator</th>
							<th>Date Created</th>
							<th>Status</th>
							<th>Activation</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; foreach($promo as $n){ ?>
						<tr class="gradeX">
							<td><?php echo $i ?></td>
							<td>
								<?php if($n->type_id == 0){ ;?>
									<a id="<?php echo $n->object_id?>" data-url="module" data-url2="product" data-lang="2" data-param="detail_object" class="detail2">
										<?php echo $n->title ?> Product
									</a>
									<br>
									<p><strong>(<?php if($n->number == ''){echo $n->number_cat;}else{echo $n->number ;}?> )</strong></p>
								<?php }else{ ;?>
									<a id="<?php echo $n->category_id?>" data-url="module" data-url2="product" data-lang="2" data-param="detail_category" class="detail2">
										<?php echo $n->title ?> Category
									</a>									
									<p><strong>(<?php if($n->number == ''){echo $n->number_cat;}else{echo $n->number ;}?> )</strong></p>
								<?php } ;?>
							</td>
							<td>
								<?php foreach($category as $c){ if($c->category_id == $n->category_id){ ;?>
									<a id="<?php echo $c->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-lang="2" data-param="detail_object" class="detail2"><?php echo $c->title?></a> , 
									<a id="<?php echo $c->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-lang="2" data-param="list" class="detail2"><?php echo $c->parent?></a>
								<?php ;} ;} ;?>
							</td>
							<td>
								<?php echo $n->discount ?> %
							</td>
							<td>
								<?php echo $n->stock ?> pcs <br> <p style="color: #f39c12">(<?php echo $n->sold?> pcs sold)</p>
							</td>
							<td>
								<?php echo date("d M Y", strtotime($n->datestart)) ?> - 
								<br><?php echo date("d M Y", strtotime($n->dateend)) ?> 
							</td>
							<td><?php echo $n->name ?></td>
							<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
							<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
							<td>
								<?php foreach($status as $s){ ;?>
									<?php if($n->status != $s->value){ ;?>
										<button id="<?php echo $n->ticket_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
									<?php ;} ;?>
								<?php ;} ;?>
								<button id="<?php echo $n->ticket_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Edit</button>
							</td>
						</tr>	
						<?php $i++;} ?>
					</tbody>
				</table>
				
			</div>
		<?php };?>
		</div>
	</div>
</div>