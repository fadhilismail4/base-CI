
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="promo" class="detail2">
					Promo List
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->ticket_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="edit_custom" data-lang="2" data-url3="<?php echo $chiave?>" class="detail2">
					Update Product
				</a>
			</li>
		</ol>		
		<button id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="promo" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right" style="margin-left: 5px">Back</button>	
	</div>
	<div class="ibox-content row" >
		<div class="row" style="margin-top: 10px !important">
			<div class="col-sm-4 col-xs-12" >
				<img alt="image" class="img-responsive" src="
				<?php if($product->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/product/<?php echo trim($product->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto; margin-bottom: 20px" >
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<h3 style="text-align: center">Basic Info</h3>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5" style="padding-left: 0px">Promo</p>
					<div class="col-sm-9 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5" style="padding-left: 0px">Category</p>
					<div class="col-sm-9 col-xs-7"><p>: <?php echo $objects->category ;?></p></div>
				</div>
				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5" style="padding-left: 0px">Code</p>
					<div class="col-sm-9 col-xs-7"><p><strong>: <?php echo $objects->code?></strong></p></div>
				</div>
				<!--
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5" style="padding-left: 0px">Used</p>
					<div class="col-sm-9 col-xs-7"><p>: <?php echo $objects->counter ;?>x</p></div>
				</div>
				-->
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5" style="padding-left: 0px">Start</p>
					<div class="col-sm-9 col-xs-7">
						<p>: 
							<?php echo date("l, d-m-Y ", strtotime($objects->datestart));?> 
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-3 col-xs-5" style="padding-left: 0px">End</p>
					<div class="col-sm-9 col-xs-7">
						<p>:
							<?php echo date("l, d-m-Y ", strtotime($objects->dateend));?>
						</p>
					</div>
				</div>
			</div>
			<div class="row col-sm-8 col-xs-12"  >			
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
				<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->ccs_key?>"></input>
				<input id="oid" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->oid?>"></input>
				<input id="ticket_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->ticket_id?>"></input>
					<input id="varian_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->varian_id?>"></input>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label" style="text-align: left !important";>Category</label>
							<div class="col-sm-4">
								<input id="category_id" name="inputan" type="text" class="form-control" placeholder="<?php echo $objects->category?>" disabled>
							</div>	
							<label class="col-sm-2 control-label" style="text-align: left !important">Promo</label>
							<div class="col-sm-4">
								<input id="object_id" name="inputan" type="text" class="form-control" placeholder="<?php echo $objects->title?>" disabled>
							</div>	
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" style="text-align: left !important";>Product</label>
							<div class="col-sm-4">
								<input id="prodtitle" name="inputan" type="text" class="form-control" placeholder="<?php echo $product->title?>" disabled>
							</div>	
							<?php if($chiave == 'varian'){ ;?>
							<label class="col-sm-2 control-label" style="text-align: left !important">Varian</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="<?php echo $objects->varian?> Varian" disabled>
							</div>	
							<?php } ;?>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" style="text-align: left !important";>Product Stock</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="<?php echo $objects->pstock?> pcs" disabled>
							</div>	
							<label class="col-sm-2 control-label" style="text-align: left !important">Product Sold</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="<?php echo $objects->psold?> pcs" disabled>
							</div>	
						</div>
						<div id="data_old">
							<div class="form-group">															
								<label class="col-sm-2 control-label" style="text-align: left !important">Old Discount</label>
								<div class="col-sm-2">
									<input id="dsc" type="text" class="form-control" value="<?php echo $objects->discount?>" disabled>
								</div>	
								<div class="col-sm-2">
									<p style="font-size: 14px; padding-top: 6px">%</p>
								</div>
								<label class="col-sm-2 control-label" style="text-align: left !important">Old Price</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" placeholder="Rp.<?php echo number_format(($objects->publish -($objects->publish *($objects->discount / 100))),2,',','.') ;?>" disabled>
								</div>	
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label" style="text-align: left !important">Publish Price</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Rp.<?php echo number_format($objects->publish,2,',','.') ;?>" disabled>
							</div>	
							<div class="" id="data_price">
								<label class="col-sm-2 control-label" style="text-align: left !important">Discount Price</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" placeholder="Rp.<?php echo number_format(($objects->publish -($objects->publish *($objects->discount / 100))),2,',','.') ;?>" disabled>
								</div>	
							</div>
							<div class="" id="data_new">
								<label class="col-sm-2 control-label" style="text-align: left !important";>New Price</label>
								<div class="col-sm-4">
									<input id="new" type="text" class="form-control" value="" disabled>
								</div>	
							</div>	
						</div>
						<div class="form-group">							
							<label class="col-sm-2 control-label" style="text-align: left !important">Discount</label>
							<div class="col-sm-2">
								<input id="discount" name="inputan" type="text" class="form-control" value="<?php echo $objects->discount?>">
							</div>	
							<div class="col-sm-2">
								<p style="font-size: 14px; padding-top: 6px">%</p>
							</div>
							<label class="col-sm-2 control-label" style="text-align: left !important";>Discount Stock</label>
							<div class="col-sm-2">
								<input id="stock" name="inputan" type="text" class="form-control" value="<?php echo $objects->stock?>">
							</div>	
							<div class="col-sm-2">
								<p style="font-size: 14px; padding-top: 6px">pcs</p>
							</div>
						</div>
						<div class="form-group" >
							<label class="col-sm-2 control-label" style="text-align: left !important; ">Publish</label>
							<div class="col-sm-4" >
								<div class="input-group col-sm-12" >
									<select class="form-control m-b" name="inputan" id="status">
										<option value="">Select Status</option>
										<?php foreach($status as $s){if($s->value != 2){ ;?>
										<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
										<?php ;} ;} ;?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">					
							<div class="space-25"></div>
							<button id="<?php echo $key_link;?>" data-param="settings" data-param2="edit_product" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Update</button>
							<div class="space-25"></div>	
							<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-url3="promo" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('#status').val(<?php echo $objects->status ;?>);
	$(document).ready(function(){
		$('#data_new').hide();
		$('#data_old').hide();
		$('#discount').change(function() {
			if ($(this).val() != <?php echo $objects->discount ;?>) {
				$('#data_new').slideDown('slow');
				$('#data_old').slideDown('slow');
				$('#data_price').slideUp('slow');
				var percent = $(this).val(),
					pph = <?php echo $objects->publish ;?> - (($('#discount').val()/100)*<?php echo $objects->publish ;?>),
					publish = "Rp. "+pph.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
				$('#new').val(''+publish+',00');
				
			}else{
				$('#data_price').slideDown('slow');
				$('#data_new').slideUp('slow');	
				$('#data_old').slideUp('slow');	
				$('$discount').val('<?php echo $objects->discount ;?>');
			}
		});
	});
</script>