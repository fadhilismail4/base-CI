
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="promo" class="detail2">
					Promo List
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->ticket_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave?>" class="detail2">
					Detail Product
				</a>
			</li>
		</ol>		
		<button id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="promo" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right" style="margin-left: 5px">Back</button>	
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		<div class="row" style="margin-top: 10px !important">
			<div class="col-sm-3 col-xs-12" >
				<img alt="image" class="img-responsive" src="
				<?php if($product->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/product/<?php echo trim($product->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto; margin-bottom: 20px" >
			</div>
			<div class="row col-sm-9 col-xs-12"  >
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Promo</p>
					<div class="col-sm-5 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Category</p>
					<div class="col-sm-3 col-xs-7"><p>: <?php echo $objects->category ;?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Promo Code</p>
					<div class="col-sm-5 col-xs-7"><p><strong>: <?php echo $objects->code?></strong></p></div>
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Used</p>
					<div class="col-sm-3 col-xs-7"><p>: <?php echo $objects->counter ;?>x</p></div>
				</div>
				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Promo Date</p>
					<div class="col-sm-10 col-xs-7">
						<p>: 
							<?php echo date("l, d M y H:i:s", strtotime($objects->datestart));?> - 
							<?php echo date("l, d M y H:i:s", strtotime($objects->dateend));?>
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Prod. Name</p>
					<div class="col-sm-5 col-xs-7"><p><strong>: <?php echo $product->title?> </strong>on <?php echo $product->category ;?> Category</p></div>
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Varian</p>
					<div class="col-sm-3 col-xs-7"><p>: <?php if($objects->vstatus != 0){ echo $objects->varian." Varian" ;}else{ "All Varian" ;}?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Basic Price</p>
					<div class="col-sm-5 col-xs-7"><p>: Rp.<?php echo number_format($objects->basic,2,',','.') ;?></p></div>
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Pub. Price</p>
					<div class="col-sm-3 col-xs-7"><p>: <strong>Rp.<?php echo number_format($objects->publish,2,',','.') ;?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">					
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Disc. Price</p>
					<div class="col-sm-5 col-xs-7">
						<p>: <strong>
							<?php if($objects->stock == 0){ ;?>
								<?php echo number_format($objects->publish,2,',','.') ;?>
							<?php }else{ ;?>
								 Rp.<?php echo number_format(($objects->publish -($objects->publish *($objects->discount / 100))),2,',','.') ;?> 
								<span style='color:#f39c12;text-decoration:line-through'>
								  <span style='color:#f39c12'>Rp.<?php echo  number_format($objects->publish,2,',','.');?></span>
								</span>
							<?php } ;?>
						</strong></p>
					</div>
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Discount</p>
					<div class="col-sm-3 col-xs-7"><p>: <?php echo $objects->discount ;?>%</p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">					
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Prod. Stock</p>
					<div class="col-sm-5 col-xs-7">
						<p>: <?php echo $objects->pstock ;?> pcs</p>
					</div>
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Prod. Sold</p>
					<div class="col-sm-3 col-xs-7"><p>: <strong> <?php echo $objects->psold ;?> pcs</strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">					
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Disc. Stock</p>
					<div class="col-sm-5 col-xs-7">
						<p>: <?php echo $objects->stock ;?> pcs</p>
					</div>
					<p class="col-sm-2 col-xs-5" style="padding-left: 0px">Disc. Sold</p>
					<div class="col-sm-3 col-xs-7"><p>: <strong> <?php echo $objects->sold ;?> pcs</strong></p></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-xs-12" style="margin-top: 10px">
			<h1>Transactions</h1>
			<br>
			<?php if($ctrx == 0){ ;?>
				<p>Sorry, there is still no transactions on this product. Cheers</p>
			<?php }else{ ;?>
				<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
					<thead>
						<tr>
							<th>No</th>
							<th style="text-align: center">TRX Code </th>
							<th>Total </th>
							<th>Discount</th>
							<th style="text-align: center">Unit Price </th>
							<th>Total Price</th>
							<th style="text-align: center">Date Created</th>
							<th>Status</th>
							<th style="text-align: center">Activation</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; foreach($trx as $n){ ?>
							<tr class="gradeX">
								<td><?php echo $i ?></td>
								<td>
									<strong><?php echo $n->code ;?><?php echo $n->trx_code ;?></strong> on <?php echo $n->title ;?>
								</td>							
								<td>
									<?php echo $n->total ;?>pcs
								</td>					
								<td>
									<?php echo $objects->discount ;?>%
								</td>						
								<td>
									Rp<?php echo number_format($n->price,1,',','.') ;?>
								</td>
								<td>
									Rp<?php echo number_format(($n->total*$n->price),1,',','.') ;?>
								</td>
								<td>
									<?php echo date("l, d M y H:i:s", strtotime($n->datesales));?>
								</td>
								<td>
									<?php if($n->status == 1){ ;?>
										<?php foreach($status as $s){if($s->value == $n->c_status){ ;?>
											<?php echo $s->name?>
										<?php ;};} ;?>
									<?php };?>
								</td>
								<td>								
									<button id="<?php echo $n->trx_code?>" data-url="module" data-url2="transactions" data-param="detail_custom" data-url3="detail" data-lang="2" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 2px">View</button>	
								</td>
								
							</tr>	
							<?php $i++;} ?>
					</tbody>
				</table>
			<?php };?>
			</div>
		</div>
	</div>
</div>