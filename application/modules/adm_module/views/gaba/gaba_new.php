<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->category;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $category->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $category->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="new_promo" data-lang="2" class="detail2">
					New Promo
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Back</button>
		</div>		
	</div>
	<div class="ibox-content row">	
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
		<input id="url_id" name="inputan" type="hidden" class="form-control" value="url_id">	
		<input id="type_id" name="inputan" type="hidden" class="form-control" value="0">
		<input id="number_cat"  value="<?php echo $category->tagline?>" name="inputan" type="hidden" class="form-control">	
		<div class="form-horizontal">				
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important";>Product</label>
				<div class="col-sm-7" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="object_id">
							<option value="">Select Product</option>								
							<?php foreach($product as $p){ ;?>
							<option value="<?php echo $p->object_id ;?>" 
								data-publish="Rp. <?php echo number_format($p->publish,2,',','.');?>" 
								data-discount="<?php echo $p->publish?>" 
								data-stock="<?php echo $p->stock?>" data-key="<?php echo $p->ccs_key?>"
							>
								<?php echo $p->title ;?> ( <?php echo $p->category ;?> Product)
							</option>
							<?php unset($p);} ;?>
						</select>
					</div>
				</div>
				<div class="col-sm-3" >
					<div class="" id="data_object">
						<button id="" data-url="module" data-url2="product" data-lang="2" data-param="price_object" class="mdl_object btn-sm btn btn-success pull-right "><i class="fa fa-search-plus"></i>  Detail Product</button>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important";>Category</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="category_id">	
							<option value="<?php echo $category->object_id ;?>"><?php echo $category->title ;?></option>
						</select>
					</div>
				</div>	
				<label class="col-sm-2 control-label" style="text-align: left !important">Promo Code</label>
				<div class="col-sm-4">
					<input id="number" name="inputan" type="text" class="form-control" placeholder="<?php echo $category->tagline?>">
				</div>	
			</div>
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Description</label>
				<div class="col-sm-10" >
					<div class="input-group col-sm-12" >
						<textarea id="description" name="inputan" type="text" class="form-control" placeholder="A short description about your promotion product (250 characters only)"></textarea>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-2" style="text-align: left !important; ">Promo Start</label>
				<div class="col-sm-4">
					<div class="input-group date input-group col-sm-12">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="datestart" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
					</div>
				</div>
				<label class="control-label col-sm-2" style="text-align: left !important; ">Promo End</label>
				<div class="col-sm-4">
					<div class="input-group date input-group col-sm-12">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="dateend" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Publish</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="status">
							<option value="">Select Status</option>
							<?php foreach($status as $s){ ;?>
							<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="" id="data_3">
					<label class="col-sm-2 control-label" style="text-align: left !important">Publish Price</label>
					<div class="col-sm-4" >
						<input  id="price" name="inputan" type="text" class="form-control " disabled=""/>							
						<input  id="basic" name="inputan" type="hidden" class="form-control" value="basic">
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="" id="data_4">
					<label class="col-sm-2 control-label" style="text-align: left !important">Discount</label>
					<div class="col-sm-2" >
						<input  id="discount" name="inputan" type="text" class="form-control" value="<?php echo $category->love?>" >
					</div> 
					<div class="col-sm-2" >						
						<p style="padding-top: 5px; margin-left: -15px; font-size: 16px"><strong>%</strong></p>
					</div>
				</div>
				<div class="" id="data_5">
					<label class="col-sm-2 control-label" style="text-align: left !important">Discount Price</label>
					<div class="col-sm-4" >
						<input  id="dcp" type="text" class="form-control" disabled="" >
						<input  id="publish" name="inputan" type="hidden" class="form-control" value="publish">
					</div> 
				</div>
			</div>
			<div class="form-group">
				<div class="" id="data_6">
					<label class="col-sm-2 control-label" style="text-align: left !important">On Stock</label>
					<div class="col-sm-2" >
						<input  id="stck"type="text" class="form-control" disabled=""  >
					</div> 
					<div class="col-sm-2" >						
						<p style="color: #f39c12; padding-top: 6px; margin-left: -15px;">On stock</p>
					</div>
					<label class="col-sm-2 control-label" style="text-align: left !important">Promo Stock</label>
					<div class="col-sm-2" >
						<input  id="stock" name="inputan" type="text" class="form-control" >
					</div> 
					<div class="col-sm-2" >						
						<p style="color: #f39c12; padding-top: 6px; margin-left: -15px;">From stock</p>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="clearfix" id="data_2">
					<label class="control-label col-sm-2" style="text-align: left !important; ">Publish on</label>
					<div class="col-sm-4">
						<div class="input-group datepicker input-group col-sm-12">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input id="datecreated" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="ticket" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Create</button>
				<div class="space-25"></div>					
				<?php if ($id == 0){ ;?>
					<button id="<?php echo $category->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
				<?php }else{ ;?>
					<button id="<?php echo $category->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
				<?php } ;?>
			</div>
		</div>	
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script>
	$(document).ready(function(){
		
		$('#datepicker').datepicker({
			format: 'yyyy-mm-dd',
			keyboardNavigation: false,
			forceParse: false
		});
		$('.date').datepicker({
			keyboardNavigation: false,
			todayBtn: 'linked',
			format: 'yyyy-mm-dd',
			forceParse: false
		});
		$('#data_2').hide();
		$('#data_3').hide();
		$('#data_4').hide();
		$('#data_5').hide();
		$('#data_6').hide();
		$('#data_object').hide();
		$('#status').change(function() {
			if ($(this).find(':selected').val() === '2') {
				$('#data_2').slideDown('slow');
			} else {
				$('#data_2').slideUp('slow');
			}
		});
		$('#object_id').change(function() {
			if ($(this).find(':selected').val() != '') {
				var prc = $(this).find(':selected').data('publish');
				var disc = $(this).find(':selected').data('discount');
				var stock = $(this).find(':selected').data('stock');
				var key = $(this).find(':selected').data('key');
				var percent = $('#discount').val(),
					pph = disc - ((disc/100)*percent),
					publish = "Rp. "+pph.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
				$('#dcp').val(''+publish+',00');
				$('#publish').val(''+pph+'');
				$('#price').val('');
				$('#url_id').val('');
				$('#data_object').children('.mdl_object').attr('id',$(this).val());				
				$('#price').val(''+prc+'');				
				$('#basic').val(''+disc+'');				
				$('#url_id').val(''+key+'');	
				$('#stck').val(''+stock+' pcs');
				$('#data_3').slideDown('slow');
				$('#data_4').slideDown('slow');
				$('#data_5').slideDown('slow');
				$('#data_6').slideDown('slow');
				$('#data_object').slideDown('slow');
				$('#discount').change(function() {
					var percent = $(this).val(),
						pph = disc - ((disc/100)*percent),
						publish = "Rp. "+pph.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
					$('#dcp').val(''+publish+',00');
					$('#publish').val(''+pph+'');
				});
			} else {				
				$('#price').val('');
				$('#data_3').slideUp('slow');
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#data_object').slideUp('slow');
			}
		});
	});
</script>
<script id="object_script">
$(document).ready(function(){
	$('.mdl_object').on("click", function(e) {
			e.preventDefault();
			
			$('.pace-done').css('padding-right','');
			$('.btn').each(function(){
				$(this).attr('disabled', 'disabled');
			});
			var id = $(this).attr('id');
			var obj = $(this).data('url');
			var part = $(this).data('url2');
			var lang = $(this).data('lang');
			var param = $(this).data('param');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : id,
				param : param,
				language_id : lang
			};
			$('[id*="modal_"]').each(function(){
				$(this).remove();
			});
			var url = "<?php echo base_url('shisha');?>/admin/"+obj+"/view_detail/"+part;
			$.ajax({
				url : url,
				type: "POST",
				dataType: 'html',
				data: data,
				success: function(html){
					$('#data_object').parent().append('<div class="modal inmodal  animated fadeInRight" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-lg">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">PRODUCT DETAIL</h5>                                        </div>                                        <div class="modal-body">    '+html.replace(/detail2/g, 'mdl_object')+'                                      </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                       </div>                                    </div>                               </div>                            </div>');
					$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
					$('.modal-backdrop').css('display','none');
					$('.btn').each(function(){$(this).removeAttr('disabled');});
					eval(document.getElementById("object_script").innerHTML);
					removeCrud('object_script');
				}
			});
			function removeCrud( itemid ) {
				var element = document.getElementById(itemid); // will return element
				elemen.parentNode.removeChild(element); // will remove the element from DOM
			}
			removeCrud('object_script');
		});
	});
	
</script>
