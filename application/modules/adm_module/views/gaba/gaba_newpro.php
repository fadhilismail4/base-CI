<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>	
			<li class="active">
				<a id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="product" class="detail2">
					Add Product
				</a>
			</li>
		</ol>		
		<button id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="promo"class="detail2 btn-sm btn btn-warning pull-right" style="margin-left: 5px">Back</button>	
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		<input id="url_id" name="inputan" type="text" class="form-control hide" value="<?php echo $parole ?>"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key ?>"></input>
		
		<div class="row ">
			<div class="col-sm-6 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Basic Info</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="promo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Promo List</button>
				
			</div>
			<div class="col-sm-6 col-xs-12">
			</div>
		</div>
		<div class="row" style="margin-top: 10px !important">
			<div class="col-sm-5 col-xs-12" >
				<img alt="image" class="img-responsive" src="
				<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
				<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block;max-height: 214px;width: auto;margin-left: auto;margin-right: auto;" >
			
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px; padding-top: 10px">
					<p class="col-sm-4 col-xs-5" style="padding-left: 0px">Name</p>
					<div class="col-sm-8 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-5" style="padding-left: 0px">Promo Code</p>
					<div class="col-sm-8 col-xs-7"><p><strong>: <?php echo $objects->tagline?></strong></p></div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-5" style="padding-left: 0px">Category</p>
					<div class="col-sm-8 col-xs-7">
						<p>: 
							<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $category->title?>
							</a>
						</p>
					</div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-5" style="padding-left: 0px">Discount</p>
					<div class="col-sm-8 col-xs-7"><p><strong>: <?php echo $objects->love?> %</strong></p></div>
				</div>
				<div class=" col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-12" style="padding-left: 0px">Promo Date</p>
					<div class="col-sm-8 col-xs-12">
						<p>: <?php echo date("d M Y", strtotime($objects->datestart))  ;?> - 
						<?php echo date("d M Y", strtotime($objects->dateend))  ;?>
						</p>
					</div>
				</div>
			</div>
			<div class="row col-sm-7 col-xs-12">
				<div class="form-horizontal">				
					<div class="form-group ">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important";>Product Category</label>
						<div class="col-sm-8 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="select_to_select2 form-control m-b" name="inputan" data-url="module" data-url2="<?php echo $key_link?>" data-param="list_url" id="cat_id">										
									<option value="">Select Product Category</option>										
									<?php foreach($product as $ct){ ;?>
									<option value="<?php echo $ct->category_id ;?>"  data-url2="<?php echo $parole?>" data-url="product" >
										<?php echo $ct->title ;?> <?php if($ct->parent_id != 0){ echo "on ".$ct->parent_title ;} ;?>
									</option>
									<?php  } ;?>
								</select>
							</div>
						</div>
					</div>					
					<div class="clearfix" id="data_2">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Product</label>
							<div class="col-sm-8 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="select_content2 form-control m-b" name="inputan" id="oid">
									</select>
								</div>
							</div>
						</div>
					</div>	
					<div class="clearfix" id="data_3">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Discount Type</label>
							<div class="col-sm-8 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="form-control m-b" name="inputan" id="dsc">
										<option value="">Select Discount Type</option>		
										<option value="0">Default Discount</option>		
										<option value="1">New Discount</option>		
									</select>
								</div>
							</div>
						</div>
					</div>	
					<div class="clearfix" id="data_4">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Discount</label>
							<div class="col-sm-2 col-xs-6">
								<input id="default" name="inputan"  type="text" class="form-control" value="<?php echo $objects->love?>" disabled>
							</div>
							<p class="col-sm-1 col-xs-2" style="font-size: 14px; padding-top: 6px; padding-left: 0px">%</p>
							<p class="col-sm-5 col-xs-12" style="font-size: 14px; padding: 6px 0px 0px 0px; color: #f39c12">Will be used on each product.</p>
						</div>
					</div>	
					<div class="clearfix" id="data_5">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Discount</label>
							<div class="col-sm-2 col-xs-6">
								<input id="discount" name="inputan"  type="text" class="form-control" value="">
							</div>
							<p class="col-sm-1 col-xs-2" style="font-size: 14px; padding-top: 6px; padding-left: 0px">%</p>
							<p class="col-sm-5 col-xs-12" style="font-size: 14px; padding: 6px 0px 0px 0px; color: #f39c12">Will be used on each product.</p>
						</div>
					</div>	
					<div class="clearfix" id="data_6">
						<div class="form-group">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Max. Use</label>
							<div class="col-sm-3 col-xs-6">
								<input id="stock" name="inputan"  type="text" class="form-control" value="">
							</div>
							<p class="col-sm-1 col-xs-2" style="font-size: 14px; padding-top: 6px; padding-left: 0px">X</p>
							<p class="col-sm-4 col-xs-12" style="font-size: 14px; padding: 0px 0px; color: #f39c12">Max. number for being used.</p>
						</div>
					</div>	
					
					<div class="form-group">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; ">Status</label>
						<div class="col-sm-8 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Status</option>
									<?php foreach($status as $s){;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php }?>
								</select>
							</div>
						</div>	
					</div>	
					<div class="clearfix" id="data_date">
						<div class="form-group">
							<label class="control-label col-sm-4 col-xs-12" style="text-align: left !important; ">Publish on</label>
							<div class="col-sm-8 col-xs-8">
								<div class="input-group date input-group col-sm-12 col-xs-12">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input id="datecreated" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">					
						<div class="space-25"></div>
						<button id="<?php echo $key_link;?>" data-param="settings" data-param2="product" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Create</button>
						<div class="space-25"></div>
						<button id="<?php echo $objects->object_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="promo" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script>
	$(document).ready(function(){
		
		$('#datepicker').datepicker({
			format: 'yyyy-mm-dd',
			keyboardNavigation: false,
			forceParse: false
		});

		$('.date').datepicker({
			keyboardNavigation: false,
			todayBtn: 'linked',
			format: 'yyyy-mm-dd',
			forceParse: false
		});
		$('#data_date').hide();
		$('#data_2').hide();
		$('#data_3').hide();
		$('#data_4').hide();
		$('#data_5').hide();		
		$('#data_6').hide();
		
		$('#status').change(function() {
			if ($(this).find(':selected').val() === '2') {
				$('#data_date').slideDown('slow');
			} else {
				$('#data_date').slideUp('slow');
			}
		});
		$('#cat_id').change(function() {
			if ($(this).find(':selected').val() != '') {
				$('#data_2').slideDown('slow');
			} else {
				$('#data_2').slideUp('slow');
				$('#data_3').slideUp('slow');
				$('#var').val('');	
				$('#dsc').val('');	
				$('#oid').val('');	
			}
			if ($(this).find(':selected').val() != 0) {
				$('#data_2').slideDown('slow');
				$('#data_3').slideUp('slow');
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#var').val('');	
				$('#discount').val('');	
				$('#stock').val('');	
				$('#dsc').val('');	
				$('#oid').val('');	
			} 
			if ($(this).find(':selected').val() == '') {
				$('#data_2').slideUp('slow');
				$('#data_3').slideUp('slow');
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#var').val('');	
				$('#discount').val('');	
				$('#stock').val('');	
				$('#dsc').val('');	
				$('#oid').val('');	
			} 
		});
		$('#oid').change(function() {
			if ($(this).find(':selected').val() != 0) {
				$('#data_3').slideDown('slow');		
			} else {
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#dsc').val('');	
				$('#object').val('');	
				$('#discount').val('');	
				$('#stock').val('');	
			}
			if ($(this).find(':selected').val() == 0) {
				$('#data_3').slideDown('slow');
			} else {
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');	
				$('#dsc').val('');	
				$('#discount').val('');	
				$('#stock').val('');	
			}
			if ($(this).find(':selected').val() == '') {
				$('#data_3').slideUp('slow');
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#dsc').val('');	
				$('#var').val('');	
				$('#discount').val('');	
				$('#stock').val('');	
			} 
		});
		
		$('#dsc').change(function() {
			if ($(this).find(':selected').val() == 0) {
				$('#data_4').slideDown('slow');
				$('#data_6').slideDown('slow');
			} else {
				$('#data_4').slideUp('slow');
				$('#stock').val('');	
			}
			if ($(this).find(':selected').val() == 1) {
				$('#data_5').slideDown('slow');
				$('#data_6').slideDown('slow');
			} else {
				$('#data_5').slideUp('slow');
				$('#discount').val('');	
				$('#stock').val('');	
			}
			if ($(this).find(':selected').val() == '') {
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#dsc').val('');	
				$('#discount').val('');	
				$('#stock').val('');	
			} 
		});
	});
</script>