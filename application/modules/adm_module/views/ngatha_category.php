<div class="ibox float-e-margins">
	<div class="ibox-title">
		<ol class="breadcrumb " style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a href="">
					<?php echo $key_name ?> Module
				</a>
			</li>
		</ol>
	</div>
	<div class="ibox-content">
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
							<span class="label label-info"></span> <strong>View All Category</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<?php if(!empty($category)){;?>
					<?php foreach($category as $c){;?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<a id="<?php echo $c->module_id;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="" data-param="list" class="detail2">
							<span class="label label-info"></span> <?php echo $c->name;?>
							</a>
							<p class="pull-right"></p> 
						</div>			
					</li>		
					<?php ;};?>
					<?php ;}else{;?>
					<br>No Category Added
					<?php };?>
				</ol>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>	