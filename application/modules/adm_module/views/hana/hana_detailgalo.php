
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-md-10 col-xs-10"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
		</ol>
		<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		
		<div class="row" style="padding-bottom: 10px">
			<div class="col-sm-6 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Konten</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="gallery_object" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Image</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="relation" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Relation</button>
			</div>
			<div class="col-sm-6 col-xs-12">				
				<button id="<?php echo $objects->object_id ;?>" data-url="<?php echo $category->category_id ;?>" data-url2="<?php echo $objects->ccs_key ;?>"  data-lang="2" class="modal_upload_data btn-xs btn btn-primary pull-right" style="margin: 0 5px">Tambah Foto</button>
			</div>
		</div>
		<div class="row" >
			<?php if($chiave == 'gallery'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5">Name</p>
					<div class="col-sm-10 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>					
				</div>
				<div class="col-sm-12 col-xs-12" style ="padding-top: 20px">
					<a class="fancybox" title="<?php echo $objects->title?> ( Main Image )"  rel="gallery" href="<?php echo base_url()?>assets/<?php echo $zone?>/<?php echo strtolower($key_name)?>/<?php echo $objects->image_square?>">
						<img src="<?php echo base_url()?>assets/<?php echo $zone?>/<?php echo strtolower($key_name)?>/<?php echo $objects->image_square?>" width="" height="" alt="chimp-1-th">
					</a>
					<?php if($objects->image_landscape){?>
					<a class="fancybox" title="<?php echo $objects->title?> ( Banner Image )"  rel="gallery" href="<?php echo base_url()?>assets/<?php echo $zone?>/<?php echo strtolower($key_name)?>/<?php echo $objects->image_landscape?>">
						<img src="<?php echo base_url()?>assets/<?php echo $zone?>/<?php echo strtolower($key_name)?>/<?php echo $objects->image_landscape?>" width="" height="" alt="chimp-1-th">
					</a>
					<?php } ;?>
					<?php if($gcount != 0){ ;?>
					
						<?php foreach($gallery as $gal){ if(file_exists('assets/shisha/gallery/'.$gal->image_square)){ ?>
							<a class="fancybox" title="<?php echo $gal->title?> "  rel="gallery" href="<?php echo base_url()?>assets/<?php echo $zone?>/gallery/<?php echo $gal->image_square?>">
								<img src="<?php echo base_url()?>assets/<?php echo $zone?>/gallery/thumbnail/<?php echo $gal->image_square?>" width=""alt="chimp-1-th">
							</a>
						<?php ;};} unset($gal);?>
					<?php } ;?>
				</div>
			<?php } ;?>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox-thumbs.js"></script>
<script id="fancy_script" type="text/javascript">
if (!$("link[href='<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css']").length){
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">').appendTo("head");
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox-thumbs.css" rel="stylesheet">').appendTo("head");
	$(document).ready(function() {
		$(".fancybox")
			.fancybox({
				prevEffect	: 'none',
				nextEffect	: 'none',
				helpers : {
					title : {
						type: 'inside',
						margin: 'auto'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					},
					buttons	: {}
				}  
			});
	});
}
</script>   