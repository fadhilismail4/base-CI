<?php if(($chiave == 'add_image')||($chiave == 'edit_gallery')){ ;?>	
	<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<?php } ;?>
<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-md-10 col-xs-10"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>	
			<?php if($chiave == 'konten'){ ;?>
				<li class="active">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
						<?php echo $objects->title;?> 
					</a>
				</li>
			<?php }elseif($chiave == 'seo'){ ;?>
				<li class="">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
						<?php echo $objects->title;?> 
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="seo" class="detail2">
						SEO Settings
					</a>
				</li>
			<?php }elseif($chiave == 'relation'){ ;?>
				<li class="">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
						<?php echo $objects->title;?> 
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="relation" class="detail2">
						Relation 
					</a>
				</li>	
			<?php }elseif($chiave == 'add_rel'){ ;?>
				<li class="">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
						<?php echo $objects->title;?> 
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="add_rel" class="detail2">
						Add Relation 
					</a>
				</li>
			<?php }elseif($chiave == 'add_image'){ ;?>
				<li class="">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
						<?php echo $objects->title;?> 
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="add_image" class="detail2">
						Add Responsive Image 
					</a>
				</li>
			<?php }elseif($chiave == 'gallery'){ ;?>
				<li class="">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
						<?php echo $objects->title;?> 
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2">
						Gallery
					</a>
				</li>
			<?php }elseif($chiave == 'edit_gallery'){ ;?>
				<li class="">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
						<?php echo $objects->title;?> 
					</a>
				</li>
				<li class="">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2">
						Gallery
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="edit_gallery" class="detail2">
						Edit Image
					</a>
				</li>
			<?php } ;?>
		</ol>
		<?php if($chiave == 'konten'){ ;?>
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php }else{ ;?>
			<?php if($chiave != 'edit_gallery'){ ;?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
			<?php }else{ ;?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
			<?php } ;?>
		<?php } ;?>
	</div>
	<div class="ibox-content row" >		
		<div class="row" style="padding-bottom: 10px">
			<div class="col-sm-6 col-xs-12">
				<button class="btn btn-white btn-xs pull-left disabled" style="margin: 5px 0" type="button" >ID</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="konten" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Konten</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="seo" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">SEO Settings</button>				
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Gallery</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="relation" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 5px">Relation</button>
			</div>
			<div class="col-sm-6 col-xs-12">
			<?php if($chiave == 'konten'){ ;?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="objects" data-title="<?php echo trim($objects->title);?>"data-lang="2" class="modal_dlt btn btn-white  btn-xs pull-right" type="button" style="margin: 5px">Delete</button>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="konten" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
				<?php foreach($status as $s){ ;?>
					<?php if($objects->status != $s->value){ ;?>
						<button id="<?php echo $objects->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $objects->title ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 5px"> <?php echo $s->name ;?></button>
					<?php ;} unset($s) ;?>				
				<?php ;} ;?>				
			<?php }elseif($chiave == 'seo'){ ;?>
				<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="seo" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px">Edit</button>
			<?php }elseif($chiave == 'relation'){ ;?>
				<?php if($rcount == 0){ ;?>
					<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="add_rel" class="detail2 btn btn-primary btn-xs pull-right" type="button" style="margin: 5px"> Add Relation </button>
				<?php }else{ ;?>
					<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="relation" class="detail2 btn btn-info btn-xs pull-right" type="button" style="margin: 5px"> Edit </button>
				<?php } ;?>
			<?php } ;?>
			</div>
			<div class="col-sm-6 col-xs-12">
				<?php if($chiave == 'gallery'){ ;?>
					<?php if($gcount == 0 ){ ;?>
							<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="add_image" class="detail2 btn btn-primary btn-xs pull-right" type="button" style="margin: 5px">Add Image</button>
					<?php } ;?>
				<?php } ;?>
			</div>
		</div>
			
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		<div class="row" >
			<?php if($chiave == 'konten'){ ;?>
				<div class="col-sm-4 col-xs-12" style="padding-left: 0px">
					<img alt="image" class="col-md-12 img-responsive" src="
					<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; ">
				</div>		
				<div class="col-sm-8 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-4 col-xs-5">Name</p>
					<div class="col-sm-8 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
					<p class="col-sm-4 col-xs-5">Category</p>
					<div class="col-sm-8 col-xs-7">
						<p>: 
							<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $category->title?>
							</a>
						</p>
					</div>
					<p class="col-sm-4 col-xs-5">Status</p>
					<div class="col-sm-8 col-xs-7">	
						<p><strong>: 
							<?php foreach($status as $s){ ;?>
								<?php if($objects->status == $s->value){ ;?>
									<?php echo $s->info?>
							<?php ;} unset($s) ;?>				
						<?php ;} ;?>	
						</strong></p>
					</div>
					<p class="col-sm-4 col-xs-5">Date Created</p>
					<div class="col-sm-8 col-xs-7"><p>: <?php echo date("l, d M Y", strtotime($objects->datecreated))  ;?></p></div>
					<?php if($objects->status == 2){ ;?>
						<p class="col-sm-4 col-xs-5">Publish Scheduled</p>
						<div class="col-sm-8 col-xs-7"><p style="color: #f39c12"><strong>: <?php echo date("l, d M Y", strtotime($objects->datepublish))  ;?></strong></p></div>
					<?php };?>
					<p class="col-sm-4 col-xs-5">Creator</p>
					<div class="col-sm-8 col-xs-7"><p>: <?php echo $objects->name ;?></p></div>
					
				</div>
				<?php if($objects->tagline != 'video'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px; padding-top: 15px">
					<?php if($image){ ;?>
							<p class="col-sm-2 col-xs-12">Responsive Image</p>
							<div class="col-sm-10 col-xs-12"><p>: <strong>Activated</strong> <br>Your website view in mobile devices ( smartphone, tablet, etc ) <strong>has been optimized</strong>.</p></div>
					<?php }else{ ;?>
						<p class="col-sm-2 col-xs-12">Responsive Image</p>
						<div class="col-sm-10 col-xs-12">
							<p>: <strong>Sorry, there is no image for mobile devices</strong>. 
							<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="add_image" class="detail2 btn btn-primary btn-xs pull-right" type="button" style="margin: 5px">Add Image</button>
							</p><p>Please upload an image to optimize your website view in mobile devices ( smartphone, tablet, etc )</p>
						</div>
					<?php } ;?>
				</div>
				<?php } ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px;">
					<?php if( strtolower($category->title) == 'banner'){ ;?>
						<p class="col-sm-2 col-xs-12">Description</p>
						<div class="col-sm-10"><p>: <?php echo $objects->description ;?></p></div>
					<?php }else{ ;?>
						<p class="col-sm-12 col-xs-12">Description :</p>
						<div class="col-sm-12 col-xs-12"><p><?php echo  $objects->description ;?></p></div>					
					<?php } ;?>		
				</div>
			<?php }elseif($chiave == 'seo'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5">Name</p>
					<div class="col-sm-10 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
					<p class="col-sm-2 col-xs-5">Meta Keyword</p>
					<div class="col-sm-10 col-xs-7">
						<p>: 
							 <?php echo $objects->metakeyword?>
						</p>
					</div>
					<p class="col-sm-2 col-xs-5">Meta Description</p>
					<div class="col-sm-10 col-xs-7">
						<p>: 
							 <?php echo $objects->metadescription?>
						</p>
					</div>
					<p class="col-sm-2 col-xs-5">Viewer</p>
					<div class="col-sm-10 col-xs-7">
						<p>: 
							 <?php echo $objects->viewer?> views
						</p>
					</div>
				</div>
			<?php }elseif($chiave == 'relation'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-5">Name</p>
					<div class="col-sm-10 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
					<?php if($rcount > 0){ ;?>
					<p class="col-sm-2 col-xs-5">Relation To</p>
					<div class="col-sm-10 col-xs-7"><p><strong>: <?php echo $relation->url?></strong></p></div>
					<?php };?>
				</div>				
			<?php }elseif($chiave == 'add_rel'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<div class="alert alert-danger" id="fail" style="display:none;"></div>
					<div class="alert alert-info" id="success" style="display:none;"></div>
					<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
					<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
					<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
					<p class="col-sm-2 col-xs-5">Name</p>
					<div class="col-sm-10 col-xs-7"><p><strong>: <?php echo $objects->title?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="form-horizontal">				
						<div class="form-group ">
							<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important";>Select Module</label>
							<div class="col-sm-4 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="select_to_select2 form-control m-b" name="inputan" data-url="module" data-url2="<?php echo $key_link?>" data-param="list_url" id="url_key">										
										<option value="">Select Module</option>										
										<?php foreach($modul as $mdl){ if( $mdl->name != "Shop"){ ;?>
											<option value="<?php echo $mdl->module_id ;?>"  data-url2="" data-url="module" >
												<?php echo $mdl->name ;?> Module
											</option>
										<?php  ;} ;} ;?>
									</select>
								</div>
							</div>			
							<div id="data_2">								
								<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Category</label>
								<div class="col-sm-4 col-xs-12">
									<div class="input-group col-sm-12 col-xs-12" >
										<select class="select_to_select select_content2 form-control m-b" data-url="module" data-url2="<?php echo $key_link?>" data-param="list_url" name="inputan" id="catid">
										</select>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-group">		
							<div id="data_3">								
								<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Content</label>
								<div class="col-sm-4 col-xs-12">
									<div class="input-group col-sm-12 col-xs-12" >
										<select class="select_content form-control m-b" name="inputan" id="oid">
										</select>
									</div>
								</div>
							</div>	
							<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Activation</label>
							<div class="col-sm-4 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class="form-control m-b" name="inputan" id="status">
										<option value="">Select Status</option>
										<option value="0">Inactivate</option>
										<option value="1">Activate </option>
									</select>
								</div>
							</div>		
						</div>						
						<div class="form-group">	
							<div class="col-sm-12 col-xs-12">
								<button id="<?php echo $key_link;?>"  data-param="relation" class="create_mdl btn btn-primary btn-md col-sm-3 col-xs-12 pull-right"><i class="fa fa-check-square"></i>  Add</button>	
								<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="relation" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
							</div>
						</div>
					</div>
				</div>
			<?php }elseif($chiave == 'add_image'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<div class="col-sm-12 col-xs-12">
						<div class="alert alert-danger" id="fail" style="display:none;"></div>
						<div class="alert alert-info" id="success" style="display:none;"></div>
						<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
						<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
						<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
						<div class="col-sm-4 col-xs-12">
							<input id="image_square" name="image_square" class="file " type="file" value="">
						</div>
						<div class="col-sm-8 col-xs-12">		
							<div class="form-horizontal">				
								<div class="form-group" >
									<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Caption</label>
									<div class="col-sm-10 col-xs-12 ">
										<textarea id="title" name="inputan" type="text" class="form-control" value="" placeholder="Caption for your image. [Only 100 Characters]"></textarea>
									</div>
								</div>
								<div class="form-group" >
									<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Description</label>
									<div class="col-sm-10 col-xs-12 ">
										<textarea id="description" name="inputan" type="text" class="form-control" value="" placeholder="A short description about your image but full of information [Only 200 Characters]"></textarea>
									</div>
								</div>
								<div class="form-group">			
									<button id="<?php echo $key_link;?>" data-param="gallery" data-param2="portrait" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Create</button>													
									<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="konten" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>									
								</div>
								<div class="form-group">
									<div class="col-sm-12 col-xs-12" >
										<h4>Notes</h4>
										<p>Your image, should be on <strong>portrait view for mobile devices</strong>. Optimum size is based on each devices resolution. </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php }elseif($chiave == 'gallery'){ ;?>		
			<div class="col-sm-12 col-xs-12" style ="padding-top: 20px;">
					<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
						<thead>
							<tr>
								<th>No</th>
								<th style="text-align: center">Image </th>
								<th style="text-align: center">Caption </th>
								<th style="text-align: center">Category</th>
								<th style="text-align: center">Date Created </th>
								<th style="text-align: center">Creator </th>
								<th style="text-align: center">Status </th>
								<th style="text-align: center">Activation</th>
							</tr>
						</thead>
						<tbody>
							<tr class="gradeX">
								<td>1</td>
								<td style="width: 15%; ">
									<img src="<?php echo base_url()?>assets/<?php echo $zone?>/<?php echo $key_link;?>/<?php echo $objects->image_square?>" style="width: 100%">
								</td>
								<td>
									<?php if($objects->title){ echo $objects->title;}else{echo "No caption";} ;?>
								</td>
								<td>
									<?php if($objects->istatus == 0){ echo "Main Image";}elseif($objects->istatus == 1){ echo "Banner Image" ;}else{ echo "Gallery" ;} ;?>
								</td>
								<td>
									<?php echo date("l, d M Y", strtotime($objects->datecreated)) ?> at
									<?php echo date("H:i:s", strtotime($objects->datecreated)) ?>
								</td>
								<td>
									<?php echo $objects->name?>
								</td>
								
								<td>
									<?php foreach($gstatus as $g){ ;?>
										<?php if($objects->status == $g->value){ ;?>
											<?php echo $g->info ;?>
										<?php ;};?>
									<?php ;} unset($g) ;?>
									
								</td>
								<td style="width: 25%">
									<a class="fancybox btn-xs btn btn-success pull-right" title="<?php if($objects->title){ echo $objects->title;}else{echo "No caption";} ;?>"  rel="gallery" href="<?php echo base_url()?>assets/<?php echo $zone?>/<?php echo $key_link;?>/<?php echo $objects->image_square?>" style="margin: 1px">View</a>
									<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" data-url3="konten" class="dtl btn-xs btn btn-info pull-right" style="margin: 1px" >Edit</button>
								</td>
							</tr>
							<?php if($gcount != 0){ ;?>			
							<?php $i = 2; foreach($gallery as $gal){ if(file_exists('assets/'.$zone.'/'.$modul.'/'.$gal->image_square)){ ?>								
								<tr class="gradeX">
									<td><?php echo $i ?></td>
									<td style="width: 15%; ">
										<img src="<?php echo base_url()?>assets/<?php echo $zone.'/'.$modul.'/' ?><?php $path = 'assets/'.$zone.'/'.$modul.'/'.$gal->image_square ;if(file_exists($path)){ echo $gal->image_square ;}else{ echo str_replace('.','_thumb.', $gal->image_square) ;} ;?>" style="width: 100%">
									</td>
									<td>
										<?php if($gal->title){ echo $gal->title;}else{echo "No caption";} ;?>
									</td>
									<td>
										<?php if($gal->istatus == 0){ echo "Main Image";}elseif($gal->istatus == 1){ echo "Banner Image" ;}else{ echo "Gallery" ;} ;?>
									</td>
									<td>
										<?php echo date("l, d M Y", strtotime($gal->datecreated)) ?> at
										<?php echo date("H:i:s", strtotime($gal->datecreated)) ?>
									</td>
									<td>
										<?php echo $gal->name?>
									</td>
									<td>
										<?php foreach($gstatus as $g){ ;?>
											<?php if($gal->status == $g->value){ ;?>
												<?php echo $g->info ;?>
											<?php ;};?>
										<?php ;} unset($g) ;?>
										
									</td>
									<td>
										<a class="fancybox btn-xs btn btn-success pull-right" title="<?php if($gal->title){ echo $gal->title;}else{echo "No caption";} ;?>"  rel="gallery" href="<?php echo base_url()?>assets/<?php echo $zone.'/'.$modul.'/'?><?php echo $gal->image_square?>" style="margin: 1px">View</a>
										<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_object" data-lang="2" data-url3="edit_gallery" class="dtl btn-xs btn btn-info pull-right" style="margin: 1px" >Edit</button>
										<button id="<?php echo $gal->gallery_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="gallery" data-title="<?php if($gal->title){ echo ucwords($gal->title." image");}else{echo "No caption";} ;?>"data-lang="2" class="modal_dlt btn-xs btn btn-danger pull-right" style="margin: 1px">Delete</button>
									</td>
								</tr>
								<?php ;}$i++;} unset($gal);?>
								<?php } ;?>
						</tbody>
					</table>
				</div>
			<?php }elseif($chiave == 'edit_gallery'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<div class="col-sm-12 col-xs-12">
						<div class="alert alert-danger" id="fail" style="display:none;"></div>
						<div class="alert alert-info" id="success" style="display:none;"></div>
						<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
						<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
						<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
						<input id="img_id" name="inputan" type="text" class="form-control hide" value="<?php echo $image->gallery_id?>"></input>
						<div class="col-sm-4 col-xs-12">
							<input id="image_square" name="image_square" class="file " type="file" value="">
						</div>
						<div class="col-sm-8 col-xs-12">		
							<div class="form-horizontal">				
								<div class="form-group" >
									<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Caption</label>
									<div class="col-sm-10 col-xs-12 ">
										<textarea id="title" name="inputan" type="text" class="form-control" value="" placeholder="Caption for your image. [Only 100 Characters]"><?php if(!empty($image->title)){ echo trim($image->title) ;} ;?></textarea>
									</div>
								</div>
								<div class="form-group" >
									<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Description</label>
									<div class="col-sm-10 col-xs-12 ">
										<textarea id="description" name="inputan" type="text" class="form-control" value="" placeholder="A short description about your image but full of information [Only 200 Characters]"><?php if(!empty($image->description)){ echo trim($image->description) ;} ;?></textarea>
									</div>
								</div>
								<div class="form-group">			
									<button id="<?php echo $key_link;?>" data-param="gallery" data-param2="portrait" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Update</button>													
									<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="konten" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>									
								</div>
								<div class="form-group">
									<div class="col-sm-12 col-xs-12" >
										<h4>Notes</h4>
										<p>Your image, should be on <strong>portrait view for mobile devices</strong>. Optimum size is based on each devices resolution. </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ;?>
		</div>
	</div>
</div>
<?php if($chiave == 'add_rel'){ ;?>
	<script>	
		$('#data_2').hide();
		$('#data_3').hide();
		$(document).ready(function(){
			$('#url_key').change(function() {
				if (($(this).find(':selected').val() != '') && ($(this).find(':selected').val() != 0)) {
					$('#data_2').slideDown('slow');
				} else {
					$('#data_2').slideUp('slow');
					$('#data_3').slideUp('slow');
				}
			});
			$('#catid').change(function() {
				if (($(this).find(':selected').val() != '') && ($(this).find(':selected').val() != 0)) {
					$('#data_3').slideDown('slow');
				} else {
					$('#data_3').slideUp('slow');
				}
			});
		});
	</script>
<?php }elseif(($chiave == 'add_image')||($chiave == 'edit_gallery')){ ;?>
	<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>
	<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
	<?php if($chiave == 'add_image'){ ;?>
	<script>
		$(document).ready(function(){
			$(".file").fileinput("refresh",{
				initialPreview: [
					"<img src='<?php echo base_url('assets').'/img/image_empty.png' ;?>' class='file-preview-image'>"
				],			
				showRemove: false,
				showUpload: false
			});
		});
	</script>
	<?php }else{ ;?>
	<script>
		$(document).ready(function(){
			$(".file").fileinput("refresh",{
				initialPreview: [
					"<img src='<?php echo base_url('assets').'/'.$zone.'/'.$modul.'/'.$image->image_square ;?>' class='file-preview-image' style='width: 100%' >"
				],			
				showRemove: false,
				showUpload: false
			});
		});
	</script>
	<?php } ;?>
<?php }elseif($chiave == 'gallery'){ ;?>

<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox-thumbs.js"></script>
<script id="fancy_script" type="text/javascript">
if (!$("link[href='<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css']").length){
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">').appendTo("head");
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox-thumbs.css" rel="stylesheet">').appendTo("head");
	$(document).ready(function() {
		$(".fancybox")
			.fancybox({
				prevEffect	: 'none',
				nextEffect	: 'none',
				helpers : {
					title : {
						type: 'inside',
						margin: 'auto'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					},
					buttons	: {}
				}  
			});
	});
}
</script>   
<?php } ;?>