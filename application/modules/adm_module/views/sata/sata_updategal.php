<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<div class="ibox ">
	<div class="ibox-title row" >
		<ol class="breadcrumb col-md-10 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<?php if($objects->parent_id != 0){ ;?>
			<li class="">
				<a id="<?php echo $objects->parent_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->pcat;?> 
				</a>
			</li>	
			<?php } ;?>
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>		
			<?php if($chiave == 'main_gallery'){ ;?>	
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
			<?php }elseif($chiave == 'gallery'){ ;?>
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->object;?> 
				</a>
			</li>
			<?php } ;?>
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2">
					Gallery
				</a>
			</li>
			<?php if($chiave == 'main_gallery'){ ;?>			
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_custom" data-lang="2" data-url3="main_gallery" class="detail2">
					Update
				</a>
			</li>
			<?php }elseif($chiave == 'gallery'){ ;?>
			<li class="active">
				<a id="<?php echo $objects->gallery_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_custom" data-lang="2" data-url3="gallery" class="detail2">
					Update
				</a>
			</li>
			<?php } ;?>
		</ol>
		<button id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2  pull-right btn-sm btn btn-warning pull-right">Back</button>
	</div>
	<div class="ibox-content row" >
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id ;?>"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key ;?>"></input>
		<?php if($chiave == 'gallery'){ ;?>
		<input id="gallery_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->gallery_id ;?>"></input>
		<?php } ;?>
		<div class="row col-sm-12 col-xs-12" >			
			<div class="col-sm-4 col-xs-12">
				<input id="image_square" name="image_square" class="file " type="file" value="">
			</div>
			<div class="col-sm-8">		
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<div class="form-horizontal">	
					<div class="form-group" >
						<br>
						<label class="col-sm-3 col-xs-4  control-label" style="text-align: left !important";>Image Category</label>
						<div class="col-sm-6 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12 " >
								<?php if($chiave == 'main_gallery'){ ;?>
								<select class="form-control m-b" name="inputan" id="istatus">		
									<option value="">Select Category</option>											
									<option value="0" >Main Image</option>											
									<option value="1" >Banner Image</option>								
									<option value="2" >Gallery Image</option>	
								</select>
								<?php }elseif($chiave == 'gallery'){ ;?>
								<select class="form-control m-b" name="inputan" id="istatus">		
									<option value="">Select Category</option>											
									<option value="0" >Main Image</option>											
									<option value="1" >Banner Image</option>								
									<option value="2" >Gallery Image</option>	
								</select>
								<?php } ;?>
							</div>
						</div>
					</div>	
					<?php if($chiave == 'main_gallery'){ ;?>					
					<div class="form-group">
						<label class="col-sm-3 col-xs-4 control-label" style="text-align: left !important">Caption</label>
						<div class="col-sm-6 col-xs-8 ">
							<input id="title" name="inputan" type="text" class="form-control" value="<?php echo $objects->title?>" disabled>
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-3 col-xs-4 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-9 col-xs-8 ">
							<textarea id="tagline" name="inputan" type="text" class="form-control" placeholder="Empty short descriptions data."  disabled><?php echo $objects->tagline?></textarea>
						</div>
					</div>	
					<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
						<p style="font-size: 14px; color: #f39c12"><strong>Notes </strong></p>
						<p style="font-size: 14px">The title and short description can't be edited, because it were your title and tagline product. </p>
					</div>
					<div class="form-group">					
						<div class="space-25"></div>
						<button id="<?php echo $key_link;?>" data-param="gallery" data-param2="main" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Update</button>
						<div class="space-25"></div>		
						<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</div>
					<?php }elseif($chiave == 'gallery'){ ;?>
					<div class="form-group">
						<label class="col-sm-3 col-xs-4 control-label" style="text-align: left !important">Caption</label>
						<div class="col-sm-6 col-xs-8 ">
							<input id="title" name="inputan" type="text" class="form-control" value="<?php echo $objects->title?>" >
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-3 col-xs-4 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-9 col-xs-8 ">
							<textarea id="description" name="inputan" type="text" class="form-control" placeholder="Empty short descriptions data."><?php echo $objects->description?></textarea>
						</div>
					</div>	
					<div class="form-group" >
						<label class="col-sm-3 col-xs-4 control-label" style="text-align: left !important; ">Publish</label>
						<div class="col-sm-6 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12 " >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Status</option>
									<?php foreach($status as $s){if($s->value != 2){  ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php ;} ;} ;?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">					
						<div class="space-25"></div>
						<button id="<?php echo $key_link;?>" data-param="gallery" data-param2="gallery" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Update</button>
						<div class="space-25"></div>		
						<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" data-url3="gallery" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</div>
					<?php } ;?>					
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>
<script>
$('#istatus').val(<?php echo $objects->istatus ;?>);
<?php if($chiave == 'gallery'){ ;?>
$('#status').val(<?php echo $objects->status ;?>);
<?php } ;?>
$(document).ready(function(){
	<?php if($chiave == 'main_gallery'){ ;?>
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $key_link ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>' width='100%' height='auto' class='file-preview-image'>"
		],			
		showRemove: false,
		showUpload: false
	});
	<?php }elseif($chiave == 'gallery'){ ;?>
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/gallery/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>' width='100%' height='auto' class='file-preview-image'>"
		],			
		showRemove: false,
		showUpload: false
	});
	<?php } ;?>
});
</script>