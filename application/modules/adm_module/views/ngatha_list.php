<div class="ibox">
	<div class="ibox-title row">
	<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					All <?php echo $key_name?> Content
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
		</ol>
		<?php } ;?>
		<div class="ibox-tools">
			<?php if (($id == 'all') && ($c_objects != 24) && ($theme != 2)){ ;?>
				
				<a id="0" data-url="0" data-url2="<?php echo $ccs_key ?>" class="modal_upload_data btn-sm btn btn-primary ">Add New</a>
			<?php }else{ ;?>
				<!--
				<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-info ">Add From Content</button>
				-->
				<?php if($c_objects != 4){?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Add New</a>
				<?php } ;?>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">
		<?php if(!empty($objects)){ ?>
			<?php foreach($objects as $n){ ?>
			<div class="file-box">	
				<div class="file">
					<a id="<?php echo $n->object_id;?>" name="" data-url="" data-url2="" class="">
						<span class="corner"></span>
						<div class="">
							<img alt="image" class="img-responsive" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo strtolower($n->name);?>/<?php echo $n->image_square;?>">
						</div>
						<div class="file-name">
							<?php echo $n->title;?>
							<br/>
							<small><?php echo $n->name;?></small>
						</div>
					</a>
				</div>
			</div>
			<?php };?>
		<?php }else{;?>
		<h1><a href=""><i class="fa fa-plus" style="border: 5px dotted #666;padding: 8px 10px;"></i></a></h1>
		<h1>There is no <?php echo ucfirst($key_name);?>. Please create a new one...</h1>
		<?php };?>
	</div>
</div>