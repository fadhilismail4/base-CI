<html lang="en">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>
      <?php echo $from[0]->char_1;?>
    </title>
<?php if(isset($style)){
	$bg_color = $style;
}else{
	$bg_color = '#74BEE5';
};?> 	
	<style type="text/css">
	a:hover { text-decoration: none !important; }
	.header h1 {color: #fff !important; font: normal 33px 'Source Sans Pro', sans-serif; margin: 0; padding: 0; line-height: 33px;}
	.header p {color: #dfa575; font: normal 20px 'Source Sans Pro', sans-serif; margin: 0; padding: 0; line-height: 11px; letter-spacing: 2px}
	.content h2 {color:<?php echo $bg_color;?> !important; font-weight: normal; margin: 0; padding: 0; font-style: normal; line-height: 35px; font-size: 20px;font-family: 'Source Sans Pro', sans-serif; }
	.content p {color:#767676; font-weight: normal; margin: 0; padding: 0; line-height: 35px; font-size: 20px;font-family: 'Source Sans Pro', sans-serif;}
	.content a {color: <?php echo $bg_color;?>; text-decoration: none;}
	.footer p {padding: 0; font-size: 20px; color:#fff; margin: 0; font-family: 'Source Sans Pro', sans-serif;}
	.footer a {color: <?php echo $bg_color;?>; text-decoration: none;}
	 h4{ font-size: 24px !important; }
	</style>
  </head>
  <body>
  	<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
		  <tr>
		  	<td align="center" style="margin: 0; padding: 0; background:<?php echo $bg_color;?>;padding: 35px 0">
			    <table cellpadding="0" cellspacing="0" border="0" align="center" width="650" style="font-family: 'Source Sans Pro', sans-serif;" class="header">
			      <tr>
			        <td bgcolor="#fff" height="115" align="center">
						<img src="<?php if(file_exists('./assets/'.$zone.'/email/logo_email.png')) {
							echo base_url('assets').'/'.$zone.'/email/logo_email.png';
						} else {
							echo $logo;
						};?>" width="auto" height="75px" alt="Logo">
			        </td>
			      </tr>
				  <tr>
					  <td style="font-size: 1px; height: 5px; line-height: 1px;" height="5">&nbsp;</td>
				  </tr>	
				</table><!-- header-->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="650" style="font-family: 'Source Sans Pro', sans-serif; background: #eaeaec;" bgcolor="#eaeaec">
			      <tr>
			        <td width="14" style="font-size: 0px;" bgcolor="#fff">&nbsp;</td>
					<td width="620" valign="top" align="left" bgcolor="#fff"style="font-family: 'Source Sans Pro', sans-serif; background: #fff;">
						<table cellpadding="0" cellspacing="0" border="0"  style="color: #717171; font: normal 11px 'Source Sans Pro', sans-serif; margin: 0; padding: 0;" width="620" class="content">
						<tr>
							<td style="padding: 25px 0;" align="center">
								<h4><?php echo $m_status;?></h4>
							</td>
						</tr>
						<tr>
							<td style="padding: 25px 0 0;" align="left">
								<p>Receipt No: <span style="font-size: 20px !important; color: #1ab394;"><?php echo $code[0];?></span></p>
								<p>Invoice No: <?php echo $code[1];?></p>
								<p>Invoice Date: <?php echo $total_trx['date'];?></p>
								<p>Receipt Date: <?php echo $code[2];?></p>
								<br>
								<p>Total: IDR <?php echo number_format($total_trx['count_prc'],2,',','.');?></p>
								<p>Shipping: IDR <?php echo number_format($expedisi['price'],2,',','.');?> ( <?php echo $expedisi['type'];?> )</p>
								<p>Total + Shipping: IDR <?php echo number_format(($total_trx['count_prc'] + $expedisi['price']),2,',','.');?></p>
							</td>
						</tr>
						<tr>
							<td style="padding: 25px 0 0;" align="left">			
								<p><strong>Notes</strong></p>
								<p><?php echo $notes;?></p>
							</td>
						</tr>
						<tr>
							<td style="padding: 25px 0 0;" align="left">			
								<p>For more detail please see the attachment.</p>
							</td>
						</tr>
						</table>	
					</td>
					<td width="16" bgcolor="#fff" style="font-size: 0px;font-family: 'Source Sans Pro', sans-serif; background: #fff;">&nbsp;</td>
			      </tr>
				</table><!-- body -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="650" style="font-family: 'Source Sans Pro', sans-serif; line-height: 10px;" bgcolor="#698291" class="footer">
			      <tr>
			        <td bgcolor="#fff"  align="center" style="padding: 15px 0 10px; font-size: 11px; color:#fff; margin: 0; line-height: 1.2;font-family: 'Source Sans Pro', sans-serif;" valign="top">
						<p style="padding: 0; font-size: 11px; color:<?php echo $bg_color;?>; margin: 0; font-family: 'Source Sans Pro', sans-serif;">Copyright &copy; <?php echo $zone;?> <?php echo date('Y');?></p>
					</td>
			      </tr> 
				</table><!-- footer-->
		  	</td>
		</tr>
    </table>
  </body>
</html>