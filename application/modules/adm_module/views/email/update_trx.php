<html lang="en">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>
      Classical
    </title>
<?php if(isset($style)){
	if(!empty($style)){
		$bg_color = $style;
	}else{
		$bg_color = '#74BEE5';
	}
}else{
	$bg_color = '#74BEE5';
};?> 		
	<style type="text/css">
	address {
		margin-bottom: 20px;
		font-style: normal;
		line-height: 1.42857143;
	}
	table {
		border-spacing: 0;
		border-collapse: collapse;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
		border-top: 1px solid #e7eaec;
		line-height: 1.42857;
		padding: 8px;
		vertical-align: top;
	}
	.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
		border-top: 0;
	}
	body {
		font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		border-top: 1px solid #e7eaec;
		line-height: 1.42857;
		padding: 8px;
		vertical-align: top;
	}
	.invoice-table tbody > tr > td:last-child, .invoice-table tbody > tr > td:nth-child(4), .invoice-table tbody > tr > td:nth-child(3), .invoice-table tbody > tr > td:nth-child(2) {
		text-align: right;
	}
	span{font-weight: normal !important;}
	</style>
  </head>
  <body>
  	<div style="background-color: #ffffff;	color: inherit;	padding: 15px 20px 20px 20px;	border-color: #e7eaec;	border-image: none;	border-style: solid solid none;	border-width: 1px 0px;padding: 40px;border:5px solid <?php echo $bg_color;?>;">
		<table style="margin-right: -15px;	margin-left: -15px;width: 100%;max-width: 100%;margin-bottom: 20px;">
			<thead>
				<tr>
					<th style="width:50%;text-align:left; position: absolute;">
						<img src="<?php if(file_exists('./assets/'.$zone.'/email/logo_email.png')) {
    echo base_url('assets').'/'.$zone.'/email/logo_email.png';
} else {
    echo $logo;
};?>" width="120px" height="auto" alt="Logo">
						<br>
						<span>From:</span>
						<address>
							<strong><?php echo $from[0]->char_1;?></strong><br>
							<abbr title="Phone">P:</abbr> <?php echo $from[0]->char_3;?><br/>
							<abbr title="Phone">M:</abbr> <?php echo $from[0]->char_4;?><br/>
							<abbr title="Phone">E:</abbr> <?php echo $from[0]->char_2;?><br/>
						</address>
					</th>
					<th style="width:50%;text-align:right">
						<?php echo $m_status;?>
						<h4>Receipt No.<span style="color: #1ab394;"><?php echo $code[0];?></span></h4>
						<span>To:</span>
						<address>
							<strong><?php echo $customer['name'];?></strong><br>
							<?php echo $customer['address'];?><br>
							<abbr title="Phone">P:</abbr> <?php echo $customer['phone'];?><br>
							<abbr title="Email">E:</abbr> <a href="mailto:<?php echo $customer['email'];?>" class="c-theme-color"><?php echo $customer['email'];?></a>
						</address>
						<span><strong>Invoice No:</strong> <?php echo $code[1];?></span><br/>
						<span><strong>Invoice Date:</strong> <?php echo $total_trx['date'];?></span><br/>
						<span><strong>Receipt Date:</strong> <?php echo $code[2];?></span>
					</th>
				</tr>
			</thead>
		</table>
		
		<table align="center" style="margin-top: 15px;min-height: .01%;overflow-x: auto;width: 100%;max-width: 100%;margin-bottom: 20px;">
			<thead>
			<tr>
				<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Item List</th>
				<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Quantity</th>
				<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Unit Price</th>
				<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Tax</th>
				<th style="border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;	vertical-align: top;">Total Price</th>
			</tr>
			</thead>
			<tbody>
			<?php $i=1;foreach($data as $val){;?>
			<tr>
				<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;"><div><strong><?php echo $val->title;?></strong></div>
					<?php if(!empty($val->varian)){;?>
					<small>Size : <?php echo $val->varian;?></small>
					<?php };?>
				</td>
				<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center"><?php echo $val->total;?></td>
				<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center">IDR <?php echo number_format(($val->price/$val->total),2,',','.');?></td>
				<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center">-</td>
				<td style="border-top: 1px solid #e7eaec;border-bottom: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;vertical-align: top;text-align:center">IDR <?php echo number_format($val->price,2,',','.');?></td>
			</tr>
			<?php $i++;};?>
			</tbody>
		</table>

		<table class="invoice-total" style="width: 100%;max-width: 100%;margin-bottom: 20px;">
			<tbody>
			<tr>
				<td style="text-align: right;width:80%"><strong>Subtotal :</strong></td>
				<td style="text-align:right">IDR <?php echo number_format($total_trx['count_prc'],2,',','.');?></td>
			</tr>
			<tr>
				<td style="border: 0 none;text-align: right;width:80%">
					<strong style="float: left;">Shipping : <?php echo $expedisi['type'];?></strong>
					<strong>Shipping Fee :</strong>
				</td>
				<td style="text-align:right">IDR <?php echo number_format($expedisi['price'],2,',','.');?></td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid #DDDDDD;text-align: right;width: 15%;width:80%;padding-bottom: 20px;"><strong>Grand Total :</strong></td>
				<td style="border-bottom: 1px solid #dddddd;padding-bottom: 20px; text-align:right">IDR <?php echo number_format(($total_trx['count_prc'] + $expedisi['price']),2,',','.');?></td>
			</tr>
			</tbody>
		</table>
		<!--<div style="text-align: right;">
			<button class="btn" style="display: inline-block;padding: 6px 12px;	margin-bottom: 0;	font-size: 14px;	font-weight: 400;	line-height: 1.42857143;	text-align: center;	white-space: nowrap;vertical-align: middle;	-ms-touch-action: manipulation;	touch-action: manipulation;	cursor: pointer;	-webkit-user-select: none;	-moz-user-select: none;	-ms-user-select: none;	user-select: none;	background-image: none;	border: 1px solid transparent;	border-radius: 3px;background-color: #1ab394;border-color: #1ab394;	color: #FFFFFF;">Make A Payment</button>
		</div>-->
		<div style="min-height: 20px;padding: 19px;	margin-bottom: 20px;background-color: #fbfbfb;	border: 1px solid #e3e3e3;	border-radius: 4px;	-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);box-shadow: inset 0 1px 1px rgba(0,0,0,.05);margin-top: 15px;">
			<strong>Notes:</strong><br/>
			<p><?php echo $notes;?></p>
			<br/>
			<?php if(isset($bank)){;?>
			<strong>OUR BANK DETAILS</strong>
			<ul style="padding-left: 10px;">
				<?php foreach($bank as $bank){;?>
				<li>
					<p><b><?php echo $bank->char_1;?></b> <?php echo $bank->char_3;?> <?php echo $bank->char_2;?></p>
				</li>
				<?php };?>
			</ul>
			<?php };?>
		</div>
	</div>
  </body>
</html>  