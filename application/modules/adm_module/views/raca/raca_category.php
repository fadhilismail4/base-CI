<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<h5><?php echo $key_name ;?> Category</h5>
	</div>
	<div class="ibox-content row">
		<div class="row" style="margin: 0px;">
			<?php if( !(count($category) > 1)){ ;?>
			<a id="<?php echo $ccs_key;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new_category" data-lang="<?php echo $language_id;?>" class="detail2 btn btn-sm btn-primary col-md-6 col-xs-12">Add Category</a>
			<?php }elseif((count($category) == 3)){ ?>
			<a id="<?php echo $ccs_key;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="<?php echo $language_id;?>" data-param="view_category" class="detail2 btn btn-sm btn-success col-md-12 col-xs-12">View Category</a>
			<?php } ;?>
		</div>	
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
				<?php if( !(count($category) > 1)){ ;?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
							<span class="label label-info"></span> <strong>View All Category</strong>
							</a>
							<p class="pull-right">(<?php echo $pcount?>)</p> 
						</div>
					</li>
				<?php } ;?>
					<?php if(!empty($category)){;?>
					<?php foreach($category as $c){ if($c['parent']->title != 'Offline Sales'){ ;?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<a id="<?php echo $c['parent']->category_id;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="<?php echo $category[0]['parent']->language_id;?>" data-param="list" class="detail2">
							<span class="label label-info"></span> <?php echo $c['parent']->title;?>
							</a>
							<p class="pull-right"><?php echo '('.$c['count'].')' ;?></p> 
						</div>		
					</li>		
					<?php ;} ;};?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<a id="timeout" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="<?php echo $category[0]['parent']->language_id;?>" data-param="list" class="detail2">
							<span class="label label-info"></span> Time Out
							</a>
							<p class="pull-right"></p> 
						</div>		
					</li>	
					<?php ;}else{;?>
					<br>No Category Added
					<?php };?>
				</ol>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>	