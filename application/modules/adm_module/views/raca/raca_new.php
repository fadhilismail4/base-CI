<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-9" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_name);?>				
				</a>
			</li>
			<?php if($id != 0){ ;?>
				<?php if($cat->parent_id != 0){ ;?>
				<li class="">
					<a id="<?php echo $cat->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
						<?php echo $cat->parent_title ;?>
					</a>
				</li>
				<?php } ;?>
				<li class="">
					<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
						<?php echo $cat->title ;?>
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new"  data-url3="single" data-lang="2" class="detail2">
						New Single Product
					</a>
				</li>
			<?php }else{ ;?>
				<li class="active">
					<a id="0" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
						New
					</a>
				</li>
			<?php } ;?>
		</ol>
		<?php if ($id == 0){ ;?>
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
		<?php }else{ ;?>
			<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
			<button id="<?php echo  $cat->category_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" data-url3="multiple" class="detail2 btn-sm btn btn-primary pull-right" style="margin-right : 5px">Add Multiple Product</button>
		<?php } ;?>
	</div>
	<div class="ibox-content row">		
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		<input id="info_id" name="inputan" type="text" class="form-control hide" value="info_id"></input>
		<div class=" col-sm-12 col-xs-12" style="margin-bottom: 20px">
			<div class="form-horizontal">					
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Sales Type</label>
					<div class="col-sm-4 col-xs-8">
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="category_id">	
								<?php if(($id == 0)&&($cat->parent_id == 0)){ ;?>
									<option value="">Select Category</option>	
								<?php }elseif(($id != 0)&&($cat->parent_id == 0)){ ;?>
									<option value="">Select Category</option>	
								<?php  } ;?>
								<?php foreach($category as $ct){ if( $ct->title != "Online Sales"){  ;?>
								<option value="<?php echo $ct->category_id ;?>"><?php echo $ct->title ;?> <?php if($ct->parent_id != 0){ echo "( ".$ct->parent_title." )" ;} ;?>  </option>
								<?php  ;} ;} ;?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Product Category</label>
					<div class="col-sm-4 col-xs-8">
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="select_to_select2 form-control m-b" name="inputan" data-url="module" data-url2="<?php echo $key_link?>" data-param="list_url" id="cat_id">										
								<option value="">Select Product Category</option>										
								<?php foreach($product as $ct){ ;?>
								<option value="<?php echo $ct->category_id ;?>"  data-url2="<?php echo $parole ;?>" data-url="trx" >
									<?php echo $ct->title ;?> <?php if($ct->parent_id != 0){ echo "on ".$ct->parent_title ;} ;?>
								</option>
								<?php  } ;?>
							</select>
						</div>
					</div>			
					<div class="clearfix" id="data_2">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Product</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="select_content2 select_to_select form-control m-b" name="inputan" id="object_id" data-url="module" data-url2="<?php echo $key_link?>" data-param="list_url">
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix" id="data_4">
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Varian</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="select_content form-control m-b" name="inputan" id="var">
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Total</label>
					<div class="col-sm-4 col-xs-8">
						<input id="total" name="inputan" type="text" class="form-control" value="" placeholder="Total Product">
					</div>						
					<div class="clearfix" id="data_5">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Price</label>
						<div class="col-sm-1 col-xs-2">
							<p style="font-size: 14px; margin-top: 6px">Rp.</p>
						</div>
						<div class="col-sm-3 col-xs-6">
							<input id="prc" name="inputan" type="text" class="form-control" value="" disabled>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Status</label>
					<div class="col-sm-4 col-xs-8"  >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ if($s->value < 2){;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php };}?>
							</select>
						</div>
					</div>
					<div class="clearfix" id="data_6">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Total Price</label>
						<div class="col-sm-1 col-xs-2">
							<p style="font-size: 14px; margin-top: 6px">Rp.</p>
						</div>
						<div class="col-sm-3 col-xs-6">
							<input id="price" name="inputan" type="text" class="form-control" value="" disabled>
						</div>
					</div>
				</div>	
				<div class="form-group ">					
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Code</label>
					<div class="col-sm-4 col-xs-8">
						<input id="ticket" name="inputan" type="text" class="form-control" value="" placeholder="Code for discount price">
					</div>		
					<label class="control-label col-sm-2  col-xs-4 " style="text-align: left !important; ">Selling Date</label>
					<div class="col-sm-4  col-xs-8" >
						<div class="input-group date input-group col-sm-12 col-xs-12">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input id="datecreated" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Customer Type</label>
					<div class="col-sm-4 col-xs-8"  >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="customer">
								<option value="">Select Type</option>
								<option value="0">New Customer</option>
								<option value="1">Find on Customer Database</option>
							</select>
						</div>
					</div>
					<div class="clearfix" id="data_old">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Customer Name</label>
						<div class="col-sm-4 col-xs-8"  >
							<div class="input-group col-sm-12 col-xs-12" >
								<select id="user_id" name="inputan" data-placeholder="Select Customer" class="chosen-select col-sm-12"  tabindex="2">
									<option value="">Select Customer</option>
									<?php foreach($user as $p){?>
										<option value="<?php echo $p->user_id;?>" data-info="0" data-address="<?php echo trim($p->address)?>" >
											<?php echo $p->name;?> ( <?php if($p->type_id == 2){ echo "Member" ;}else{ echo "Reseller" ;} ;?> )
										</option>
									<?php } ?>
									<?php foreach($customer as $c){?>
										<option value="0" data-info="<?php echo $c->info_id;?>" data-address="<?php echo trim($c->address)?>" >
											<?php echo $c->name;?> ( Shipping )
										</option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix" id="data_new">
					<div class="form-group ">					
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Customer Name</label>
						<div class="col-sm-4 col-xs-8">
							<input id="name" name="inputan" type="text" class="form-control" value="" placeholder="Customer name">
						</div>				
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Phone</label>
						<div class="col-sm-4 col-xs-8">
							<input id="phone" name="inputan" type="text" class="form-control" value="" placeholder="Customer phone">
						</div>		
					</div>
					<div class="form-group ">					
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Email</label>
						<div class="col-sm-4 col-xs-8">
							<input id="email" name="inputan" type="text" class="form-control" value="" placeholder="Customer email">
						</div>				
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Mobile</label>
						<div class="col-sm-4 col-xs-8">
							<input id="mobile" name="inputan" type="text" class="form-control" value="" placeholder="Customer mobile phone">
						</div>		
					</div>
				</div>
				<div class="clearfix" id="data_daddress">
					<div class="form-group">
						<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Address</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="address" name="inputan" type="text" class="form-control" placeholder="This is customer address. [Only 250 characters]"></textarea>
						</div>
					</div>
				</div>
				<div class="clearfix" id="data_shipping">
					<div class="form-group">
						<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Shipping To</label>
						<div class="col-sm-4 col-xs-8"  >
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="sia">
								<option value="">Select Type</option>
								<option value="0">Default Address</option>
								<option value="1">Different Address</option>
							</select>
						</div>
					</div>
					</div>
				</div>
				<div class="clearfix" id="data_address">
					<div class="form-group">
						<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Shipping Address</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="shipping" name="inputan" type="text" class="form-control" placeholder="This is shipping address, can be different from customer address. [Only 250 characters]"></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2 col-xs-4" style="text-align: left !important; ">Notes</label>
					<div class="col-sm-10 col-xs-12 " >
						<textarea id="desc" name="inputan" type="text" class="form-control" placeholder="This is notes for offline sales. Please start by your customer info and then your sales notes. [Only 250 characters]"></textarea>
					</div>
				</div>
				<div class="form-group">			
					<button id="<?php echo $key_link;?>" data-param="trx" data-param2="single" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Create</button>				
					<?php if ($id == 0){ ;?>
						<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					<?php }else{ ;?>
						<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					<?php } ;?>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script>
	$(document).ready(function(){
		$('#data_2').hide();
		$('#data_4').hide();
		$('#data_5').hide();
		$('#data_6').hide();
		$('#data_new').hide();
		$('#data_address').hide();
		$('#data_daddress').hide();
		$('#data_shipping').hide();
		$('#data_old').hide();
		$('#category_id').val(<?php echo $cat->category_id?>);
		
		$('#sia').change(function() {
			if ($(this).find(':selected').val() == 1) {
				$('#data_address').slideDown('slow');
			} else {
				$('#data_address').slideUp('slow');
				$('#shipping').val('');
			}
			
			if ($(this).find(':selected').val() == '') {
				$('#data-address').slideUp('slow');
				$('#shipping').val('');
			}
		});
		$('#customer').change(function() {
			if ($(this).find(':selected').val() == 0) {
				$('#data_new').slideDown('slow');				
				$('#data_shipping').slideDown('slow');
				$('#data_daddress').slideDown('slow');
			} else {
				$('#data_new').slideUp('slow');
				$('#name').val('');
				$('#phone').val('');
				$('#email').val('');
				$('#mobile').val('');
				$('#address').val('');
				$('#info_id').val('');
				$('#shipping').val('');				
			}
			if ($(this).find(':selected').val() == 1) {
				$('#data_old').slideDown('slow');
				$('#data_shipping').slideDown('slow');
				$('#data_daddress').slideDown('slow');
				$('#user_id').change(function() {
					if ($(this).find(':selected').val() != '') {
						var address = $(this).find(':selected').data('address');
						var info = $(this).find(':selected').data('info');
						$('#address').val(''+address+'');
						$('#info_id').val(''+info+'');
					}else{
						$('#address').val('');
						$('#info_id').val('');
						$('#user_id').val('');
						//$("#user_id").("#chosen-select").val('');
					}
				});
			} else {
				$('#data_old').slideUp('slow');
				$('#address').val('');
				$('#info_id').val('');
				$('#user_id').val('');
			}
			if ($(this).find(':selected').val() == '') {
				$('#data_new').slideUp('slow');
				$('#data_old').slideUp('slow');
				$('#data_daddress').slideUp('slow');
				$('#data_shipping').slideUp('slow');
				$('#name').val('');
				$('#phone').val('');
				$('#mobile').val('');
				$('#address').val('');
				$('#info_id').val('');
				$('#email').val('');
				$('#shipping').val('');		
				$('#user_id').val('');
			}			
		});
		$('#cat_id').change(function() {
			if ($(this).find(':selected').val() != 0) {
				$('#data_2').slideDown('slow');
				$('#data_4').slideUp('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
			} else {
				$('#data_2').slideUp('slow');
			}
		});
		$('#object_id').change(function() {
			if ($(this).find(':selected').data('status') == 1) {
				$('#data_4').slideDown('slow');
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
			} else {
				$('#data_4').slideUp('slow');
				$('#total').val('');	
			}
			if ($(this).find(':selected').data('status') == 0) {
				$('#data_4').slideUp('slow');
				$('#data_5').slideDown('slow');
				//$('#data_6').slideDown('slow');
				var prc = $(this).find(':selected').data('price');
				var ttl = $(this).find(':selected').data('ttl');
				$('#prc').val('');	
				$('#prc').val(''+prc+'');	
				$('#price').val('');	
				$('#price').val(''+ttl+'');	
			} else {
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#prc').val('');	
				$('#ttl').val('');	
				$('#total').val('');	
			}
		});
		$('#var').change(function() {
			var total = $(this).val();
			if ($(this).find(':selected').val() != 0) {
				$('#data_5').slideDown('slow');
				//$('#data_6').slideDown('slow');
				var prc = $(this).find(':selected').data('price');
				var ttl = $(this).find(':selected').data('ttl');
				$('#prc').val('');	
				$('#price').val('');	
				$('#prc').val(''+prc+'');	
				var jumlah = ttl*total;
				var price = jumlah.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
				$('#price').val(''+price+'');
			} else {
				$('#data_5').slideUp('slow');
				$('#data_6').slideUp('slow');
				$('#prc').val('');	
				$('#price').val('');
				$('#total').val('');		
			}
		});
		$('#total').change(function() {
			var total = $(this).val();
			if ($(this).val() != 0) {
				$('#data_6').slideDown('slow');
				var ttl = $('#object_id').find(':selected').data('ttl');
				var jumlah = ttl*total;
				var price = jumlah.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
				$('#price').val(''+price+'');	
				
			} else {
				$('#data_6').slideUp('slow');
				$('#price').val('');	
				$('#total').val('');	
			}
		});
		$('#datepicker').datepicker({
			format: 'yyyy-mm-dd',
			keyboardNavigation: false,
			forceParse: false
		});

		$('.date').datepicker({
			keyboardNavigation: false,
			todayBtn: 'linked',
			format: 'yyyy-mm-dd',
			forceParse: false
		});
	});
</script>
