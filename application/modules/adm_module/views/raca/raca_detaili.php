<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-10 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<?php if($dod == 1){ ;?>
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>		
			<li class="">
				<a id="<?php echo $objects->trx_code?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="detail" class="detail2">
					<?php echo $objects->code ?><?php echo $objects->trx_code?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->trx_code?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave?>" class="detail2">			
					<?php echo ucwords($chiave) ;?>
				</a>
			</li>					
			<?php }else{ ;?>	
			<li class="">
				<a id="<?php echo $objects[0]->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects[0]->category;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects[0]->trx_code?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="detail" class="detail2">
					<?php echo $objects[0]->code ?><?php echo $objects[0]->trx_code?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects[0]->trx_code?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php echo $chiave?>" class="detail2">					
					<?php echo ucwords($chiave) ;?>
				</a>
			</li>	
			<?php } ;?>
		</ol>
		<?php if($dod == 1){ ;?>
				<button id="<?php echo $objects->trx_code?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-lang="2" data-url3="detail" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php }else{ ;?>	
				<button id="<?php echo $objects[0]->trx_code?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-lang="2" data-url3="detail" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>			
		<?php } ;?>
	</div>
	<?php if($chiave == 'invoice'){ ;?>
	<div class="ibox-content row" >	
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		<div class="row col-sm-12 col-xs-12" style="padding-left: 0px">	
			<div class="row wrapper wrapper-content animated fadeInRight">
				<div class="ibox-content p-xl" style="border-color: white; padding-top: 0px">
					<div class="row">
						<?php if($dod == 1){ ;?>
							<div class="pull-right btn-lg btn btn-<?php if($objects->status == 0){ echo "warning";}elseif( $objects->status == 1){echo "primary" ;}else{ echo "danger" ;} ?> "><?php if($objects->status == 0){ echo "UNPAID";}elseif( $objects->status == 1){echo "PAID" ;}else{ echo "CANCELLED" ;} ?></div>
						<?php }else{ ;?>
								<div class="pull-right btn-lg btn btn-<?php if($objects[0]->status == 0){ echo "warning";}elseif( $objects[0]->status == 1){echo "primary" ;}else{ echo "danger" ;} ?> "><?php if($objects[0]->status == 0){ echo "UNPAID";}elseif( $objects[0]->status == 1){echo "PAID" ;}else{ echo "CANCELLED" ;} ?></div>
						<?php } ;?>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<h5>From:</h5>
							<address>
								<strong><?php echo trim($owner->char_1) ;?></strong><br>
								<?php echo trim($owner->char_5) ;?>							
								<br><i class="fa fa-phone"></i>     <?php echo $owner->char_3?>
								<br><i class="fa fa-envelope"></i> <?php echo $owner->char_2?>
							</address>
						</div>
						<div class="col-sm-6 text-right">
							<h4>Invoice No.</h4>
							<h4 class="text-navy">
								<?php if($dod == 1){ ;?>
									#<?php echo $objects->icode?><?php echo $objects->inv_code?>
								<?php }else{ ;?>
									#<?php echo $invoice->icode?><?php echo $invoice->inv_code?>
								<?php } ;?>
							</h4>
							<span>To:</span>
							<address>
								<strong>
									<?php echo $shipping->name?>
								</strong>
								<br>
								<?php echo $shipping->address_2?>
								<abbr title="Phone" />
								<br><i class="fa fa-phone"></i> : <?php echo $shipping->phone?>
								<br><i class="fa fa-envelope"></i> : <?php echo $shipping->email?>
							</address>
							<p>
								<span><strong>Invoice Date:</strong> 
									<?php if($dod == 1){ ;?>
										<?php echo date("l, d M Y H:i:s", strtotime($objects->datecreated)) ?>
									<?php }else{ ;?>
										<?php echo date("l, d M Y H:i:s", strtotime($objects[0]->datecreated)) ?>
									<?php } ;?>
								</span><br/>
								<span><strong>Due Date:</strong> 
									<?php if($dod == 1){ ;?>
										<?php echo date("l, d M Y H:i:s", strtotime($objects->datesales)) ?>
									<?php }else{ ;?>
										<?php echo date("l, d M Y H:i:s", strtotime($objects[0]->datevalid)) ?>
									<?php } ;?>
								</span>
							</p>
						</div>
					</div>
					<?php echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>' ;?>
					
					<?php if($dod == 1){ ;?>
					<div class="table-responsive m-t">
						<table class="table invoice-table">
							<thead>
							<tr>
								<th>No</th>
								<th style="text-align: center">Item List</th>
								<th style="text-align: center">Quantity</th>
								<th style="text-align: center">Unit Price</th>
								<th style="text-align: center">Discount</th>
								<th>Total Price</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1</td>
								<td style="text-align: left"><div><strong><?php echo $objects->title?></strong></div>
									<small><?php if($varian != ''){ echo $varian->title." Varian" ;}else{ echo "All Varian";} ;?> 
									</small>
								</td>
								<td style="text-align: center"><?php echo $objects->total?>pcs</td>
								<td style="text-align: center">
									<?php if($stock->publish != ($objects->price/$objects->total)){ ;?>
									<strong>Rp.<?php echo  number_format(($objects->price/$objects->total),2,',','.');?></strong>
										   <br>from <span style='color:#f39c12;text-decoration:line-through'>
										  <span style='color:#f39c12'>Rp.<?php echo  number_format($stock->publish,2,',','.');?></span>
										</span>
										
									<?php }else{ ;?>							
										<strong>Rp.<?php echo  number_format($objects->price,2,',','.');?></strong>
									<?php } ;?>
								</td>
								<td style="text-align: center">
									<?php if($stock->publish != ($objects->price/$objects->total)){ echo (100-(($objects->price/$objects->total)/$stock->publish*100))."%" ;}else{ echo "-" ;} ;?> 
								</td>
								<td>Rp.<?php echo  number_format($objects->price,2,',','.');?></td>
							</tr>
							</tbody>
						</table>
					</div><!-- /table-responsive -->
					<table class="table invoice-total">
						
						<tbody>
						<tr>
							<td><strong>Tax :</strong></td>
							<td>-</td>
						</tr>
						<tr>
							<td><strong>Shipping Fee :</strong></td>
							<td>Rp.<?php echo  number_format($shipment->price,2,',','.');?></td>
						</tr>
						<tr>
							<td><strong>Total :</strong></td>
							<td>Rp.<?php echo  number_format(($total->total+$shipment->price),2,',','.');?></td>
						</tr>
						</tbody>
					</table>
					<?php }else{ ;?>
					<div class="table-responsive m-t">
						<table class="table invoice-table">
							<thead>
							<tr>
								<th>No</th>
								<th style="text-align: center">Item List</th>
								<th style="text-align: center">Quantity</th>
								<th style="text-align: center">Unit Price</th>
								<th style="text-align: center">Discount</th>
								<th>Total Price</th>
							</tr>
							</thead>
							<tbody>
							<?php $i=1;foreach($objects as $o){ ;?>
							<tr>
								<td><?php echo $i?></td>
								<td style="text-align: left"><div><strong><?php echo $o->title?></strong></div>
									<small><?php if($o->varian != '0'){ echo $o->varian." Varian" ;}else{ echo "All Varian";} ;?> </small></td>
								<td style="text-align: center"><?php echo $o->total?>pcs</td>
								<td style="text-align: center">
									<?php if($o->dprice != ($o->price/$o->total)){ ;?>
									<strong>Rp.<?php echo  number_format(($o->price/$o->total),2,',','.');?></strong>
										   <br>from <span style='color:#f39c12;text-decoration:line-through'>
										  <span style='color:#f39c12'>Rp.<?php echo  number_format($o->dprice,2,',','.');?></span>
										</span>
										
									<?php }else{ ;?>							
										<strong>Rp.<?php echo  number_format($o->price,2,',','.');?></strong>
									<?php } ;?>
								</td>
								<td style="text-align: center">
									<?php if($o->dprice != ($o->price/$o->total)){ echo (100-(($o->price/$o->total)/$o->dprice*100))."%" ;}else{ echo "-" ;} ;?> 
								</td>
								<td>Rp.<?php echo  number_format($o->price,2,',','.');?></td>
							</tr>
							<?php $i++;} ;?>
							</tbody>
						</table>
					</div><!-- /table-responsive -->
					
					<table class="table invoice-total">
						<tbody>
						<tr>
							<td><strong>Tax :</strong></td>
							<td>-</td>
						</tr>
						<tr>
							<td><strong>Shipping Fee :</strong></td>
							<td>Rp.<?php echo  number_format($shipment->price,2,',','.');?></td>
						</tr>
						<tr>
							<td><strong>Total :</strong></td>
							<td>Rp.<?php echo  number_format(($total[0]->total),2,',','.');?></td>
						</tr>
						</tbody>
					</table>
					<?php } ;?>
					<!--
					<div class="text-right">
						<button class="btn btn-primary"><i class="fa fa-dollar"></i> Make A Payment</button>
					</div>
					-->
					<div class="well m-t"><strong>Notes</strong>
						<br>
						<ol style="padding-left: 15px">
							<li>Shipping provider using <strong><?php echo $shipment->ticket; ?></strong></li>
							<?php if($dod == 1){ ;?>
								<li><?php echo $objects->description?></li>
							<?php }else{ ;?>
								<li><?php echo $objects[0]->description?></li>
							<?php } ;?>
						</ol>
					</div>
				</div>
			</div>           
		</div>	
	</div>
	<?php }else{?>
		<div class="ibox-content row" >	
			<div class="row col-sm-12 col-xs-12">	
				<?php if($rcp){ ;?>
				<div class="row wrapper wrapper-content animated fadeInRight">
					<div class="ibox-content p-xl" style="border-color: white; padding-top: 0px">
						<div class="row">
							<?php if($dod == 1){ ;?>
								<div class="pull-right btn-lg btn btn-<?php if($objects->status == 0){ echo "warning";}elseif( $objects->status == 1){echo "primary" ;}else{ echo "danger" ;} ?> "><?php if($objects->status == 0){ echo "UNPAID";}elseif( $objects->status == 1){echo "PAID" ;}else{ echo "CANCELLED" ;} ?></div>
							<?php }else{ ;?>
									<div class="pull-right btn-lg btn btn-<?php if($objects[0]->status == 0){ echo "warning";}elseif( $objects[0]->status == 1){echo "primary" ;}else{ echo "danger" ;} ?> "><?php if($objects[0]->status == 0){ echo "UNPAID";}elseif( $objects[0]->status == 1){echo "PAID" ;}else{ echo "CANCELLED" ;} ?></div>
							<?php } ;?>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<h5>From:</h5>
								<address>
									<strong><?php echo trim($owner->char_1) ;?></strong><br>
									<?php echo trim($owner->char_5) ;?>							
									<br><i class="fa fa-phone"></i>     <?php echo $owner->char_3?>
									<br><i class="fa fa-envelope"></i> <?php echo $owner->char_2?>
								</address>
							</div>
							<div class="col-sm-6 text-right">
								<h4>Receipt No.</h4>
								<h4 class="text-navy">
										#<?php echo $receipt->code?><?php echo $receipt->codex?>
								</h4>
								<span>To:</span>
								<address>
									<strong>
										<?php echo $shipping->name?>
									</strong>
									<br>
									<?php echo $shipping->address_2?>
									<abbr title="Phone" />
									<br><i class="fa fa-phone"></i> : <?php echo $shipping->phone?>
									<br><i class="fa fa-envelope"></i> : <?php echo $shipping->email?>
								</address>
								<p>	
									<span><strong>Invoice No:</strong> 
										<?php if($dod == 1){ ;?>
											#INV<?php echo $objects->trx_code ?>
										<?php }else{ ;?>
											#INV<?php echo $objects[0]->trx_code ?>
										<?php } ;?>
									</span><br/>
									<span><strong>Invoice Date:</strong> 
										<?php if($dod == 1){ ;?>
											<?php echo date("l, d M Y H:i:s", strtotime($objects->datecreated)) ?>
										<?php }else{ ;?>
											<?php echo date("l, d M Y H:i:s", strtotime($objects[0]->datecreated)) ?>
										<?php } ;?>
									</span><br/>
									<span><strong>Receipt Date:</strong> 
											<?php echo date("l, d M Y H:i:s", strtotime($receipt->datecreated)) ?>
									</span>
								</p>
							</div>
						</div>
						<?php echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>' ;?>
						
						<?php if($dod == 1){ ;?>
						<div class="table-responsive m-t">
							<table class="table invoice-table">
								<thead>
								<tr>
									<th>No</th>
									<th style="text-align: center">Item List</th>
									<th style="text-align: center">Quantity</th>
									<th style="text-align: center">Unit Price</th>
									<th style="text-align: center">Discount</th>
									<th>Total Price</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>1</td>
									<td style="text-align: left"><div><strong><?php echo $objects->title?></strong></div>
										<small><?php if($varian != ''){ echo $varian->title." Varian" ;}else{ echo "All Varian";} ;?> 
										</small>
									</td>
									<td style="text-align: center"><?php echo $objects->total?>pcs</td>
									<td style="text-align: center">
										<?php if($stock->publish != ($objects->price/$objects->total)){ ;?>
										<strong>Rp.<?php echo  number_format(($objects->price/$objects->total),2,',','.');?></strong>
											   <br>from <span style='color:#f39c12;text-decoration:line-through'>
											  <span style='color:#f39c12'>Rp.<?php echo  number_format($stock->publish,2,',','.');?></span>
											</span>
											
										<?php }else{ ;?>							
											<strong>Rp.<?php echo  number_format($objects->price,2,',','.');?></strong>
										<?php } ;?>
									</td>
									<td style="text-align: center">
										<?php if($stock->publish != ($objects->price/$objects->total)){ echo (100-(($objects->price/$objects->total)/$stock->publish*100))."%" ;}else{ echo "-" ;} ;?> 
									</td>
									<td>Rp.<?php echo  number_format($objects->price,2,',','.');?></td>
								</tr>
								</tbody>
							</table>
						</div><!-- /table-responsive -->
						<table class="table invoice-total">
							
							<tbody>
							<tr>
								<td><strong>Tax :</strong></td>
								<td>-</td>
							</tr>
							<tr>
								<td><strong>Shipping Fee :</strong></td>
								<td>Rp.<?php echo  number_format($shipment->price,2,',','.');?></td>
							</tr>
							<tr>
								<td><strong>Total :</strong></td>
								<td>Rp.<?php echo  number_format(($total->total+$shipment->price),2,',','.');?></td>
							</tr>
							</tbody>
						</table>
						<?php }else{ ;?>
						<div class="table-responsive m-t">
							<table class="table invoice-table">
								<thead>
								<tr>
									<th>No</th>
									<th style="text-align: center">Item List</th>
									<th style="text-align: center">Quantity</th>
									<th style="text-align: center">Unit Price</th>
									<th style="text-align: center">Discount</th>
									<th>Total Price</th>
								</tr>
								</thead>
								<tbody>
								<?php $i=1;foreach($objects as $o){ ;?>
								<tr>
									<td><?php echo $i?></td>
									<td style="text-align: left"><div><strong><?php echo $o->title?></strong></div>
										<small><?php if($o->varian != '0'){ echo $o->varian." Varian" ;}else{ echo "All Varian";} ;?> </small></td>
									<td style="text-align: center"><?php echo $o->total?>pcs</td>
									<td style="text-align: center">
										<?php if($o->dprice != ($o->price/$o->total)){ ;?>
										<strong>Rp.<?php echo  number_format(($o->price/$o->total),2,',','.');?></strong>
											   <br>from <span style='color:#f39c12;text-decoration:line-through'>
											  <span style='color:#f39c12'>Rp.<?php echo  number_format($o->dprice,2,',','.');?></span>
											</span>
											
										<?php }else{ ;?>							
											<strong>Rp.<?php echo  number_format($o->price,2,',','.');?></strong>
										<?php } ;?>
									</td>
									<td style="text-align: center">
										<?php if($o->dprice != ($o->price/$o->total)){ echo (100-(($o->price/$o->total)/$o->dprice*100))."%" ;}else{ echo "-" ;} ;?> 
									</td>
									<td>Rp.<?php echo  number_format($o->price,2,',','.');?></td>
								</tr>
								<?php $i++;} ;?>
								</tbody>
							</table>
						</div><!-- /table-responsive -->
						
						<table class="table invoice-total">							
							<tbody>
							<tr>
								<td><strong>Tax :</strong></td>
								<td>-</td>
							</tr>
							<tr>
								<td><strong>Shipping Fee :</strong></td>
								<td>Rp.<?php echo  number_format($shipment->price,2,',','.');?></td>
							</tr>
							<tr>
								<td><strong>Total :</strong></td>
								<td>Rp.<?php echo  number_format(($total[0]->total+$shipment->price),2,',','.');?></td>
							</tr>
							</tbody>
						</table>
						<?php } ;?>
						<!--
						<div class="text-right">
							<button class="btn btn-primary"><i class="fa fa-dollar"></i> Make A Payment</button>
						</div>
						-->
						<div class="well m-t"><strong>Notes</strong>
							<br>
								<ol style="padding-left: 15px">
									<li>Shipping provider using <strong><?php echo $shipment->ticket; ?></strong></li>									
									<li><?php echo $receipt->description?></li>
								</ol>
						</div>
					</div>
				</div>           
				<?php }else{?>
					<p>There is <strong>no receipt</strong> for this transaction, please <strong>check payment confirmation</strong> tabs. Thank you.</p>
				<?php } ;?>
			</div>
		</div>
	<?php } ;?>
</div>