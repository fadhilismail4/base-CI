<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> Category
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="new_category" data-lang="2" class="detail2">
					Create
				</a>
			</li>
		</ol>
		<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right">Back</button>
				
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		<form class="form-horizontal">
			<?php if($segretissimo != 0){ ;?>
			<div class="form-group">
				<label class="col-sm-2 col-xs-4 control-label">Category</label>
				<div class="col-sm-4 col-xs-8 ">
					<select class="form-control m-b" name="inputan" id="parent_id">
						<option value="0">Set as Parent Category</option>
						<?php foreach($category as $c){if($c->count == 0){ ;?>
						<option value="<?php echo $c->category_id ;?>"><?php echo $c->title ;?></option>
						<?php } ;} ;?>
					</select>
				</div>
			</div>
			<?php }else{ ;?>
				<input id="parent_id" name="inputan" type="text" class="form-control hide" value="0"></input>
			<?php } ;?>
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label">Name</label>
				<div class="col-sm-10 col-xs-12 "><input id="title" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label">Description</label>
				<div class="col-sm-10 col-xs-12 "><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-12 col-xs-12  pull-right">		
					<button id="<?php echo $key_link;?>" data-param="category" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square"></i>  Create</button>		
					<button id="all" data-url="<?php echo $key_link;?>" data-url2="list" data-lang="<?php if(!empty($category)){  echo $category[0]->language_id;}else{ $language_id;};?>" class="detail2 btn btn-md btn-white pull-right" type="submit"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>