<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb  col-sm-8 col-xs-7" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					All <?php echo $key_name?> Content
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-6" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<?php if(isset($categories)){;?>
			<?php if($categories->parent_id == 0){ ;?>
			<li class="active">
				<a id="<?php echo $categories->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php }else{ ;?>
			<li class="">
				<a id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->parent_title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php } ;?>
			<?php } ;?>
		</ol>
		<?php } ;?>
		<?php if ($id != 'all'){ ;?>
			<?php if(isset($categories)){;?>
			<?php if($categories->parent_id != 0){ ?>
				<button id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right" style="margin-left: 5px">Back</button>
				<?php }else{ ;?>
				<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right" style="margin-left: 5px">Back</button>
			<?php } ;?>
			<?php if($keys != 'Online Sales'){ ;?>
				<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" data-url3="single" class="detail2 btn-sm btn btn-primary pull-right" style="margin-left: 5px">Add Single Product</button>	
				<!--
				<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" data-url3="multiple" class="detail2 btn-sm btn btn-primary pull-right">Add Multiple Product</button>
				-->
			<?php } ;?>
			<?php } ;?>
		<?php }else{ ;?>	
			<!--
			<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="invoice" data-lang="2" class="detail2 btn-sm btn btn-success pull-right" style="margin-left: 5px">Invoice</button>
			<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="receipt" data-lang="2" class="detail2 btn-sm btn btn-info pull-right">Sales Receipt</button>
			-->
		<?php } ;?>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>Sorry, it's still no sales from your product. Keep smile :) ... </h3>
		<?php }else{ ;?>
		<div class="table-responsive" style="border: 0;">
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th style="text-align: center">Status </th>
						<th style="text-align: center">Code </th>
						<th style="text-align: center">Product </th>
						<th style="text-align: center">Price </th>
						<!--<th style="text-align: center">Disc </th>-->
						<th style="text-align: center">Total </th>
						<th>Approval</th>
						<th style="text-align: center">Date Created</th>
						<th style="text-align: center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($objects as $n){ if( $n->code != 'TRXOF'){ ;?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td style="text-align: center">						
							<?php foreach($status as $s){ ;?>
								<?php if($n->status == $s->value){ ;?>
									<?php echo $s->name ;?>
								<?php ;} ;?>
							<?php ;} ;?>	
							<?php if(!empty($n->o_s)){;?>
							<br>
							(<?php echo $n->o_s;?>)
							<?php };?>
						</td>
						<td style="text-align: center">
							<a id="<?php echo $n->trx_code?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_custom" data-url3="detail" class="dtl">
								<?php echo $n->trx_code?><br><strong><?php echo $n->code ?> SALES</strong>
							</a>
						</td>
						<td style="width:15%">
							<?php foreach($n->lists as $l){;?>
								<a id="<?php echo $n->trx_code?>" class="showImage">
									<?php echo $l->title ?>
									<?php if(!empty($l->varian)){;?>
										(<?php echo $l->varian;?>)
									<?php };?>
								</a>
								<br>
								<?php if($l->image){;?>
								<img id="<?php echo $n->trx_code?>" src="<?php echo $l->image;?>" class="img-responsive" style="display: none;">
								<?php };?>
							<?php };?>
						</td>
						<td>
							Rp.<?php echo  number_format($n->price,2,',','.');?>
							<!--<?php if($n->dprice != ($n->price/$n->total)){ ;?>
								Rp.<?php echo  number_format($n->price,2,',','.');?>
									<br>
									<span style='color:#f39c12;text-decoration:line-through'>
									  <span style='color:#f39c12'>Rp.<?php echo  number_format(($n->dprice*$n->total),2,',','.');?></span>
									</span>
							<?php }else{ ;?>							
								Rp.<?php echo  number_format($n->price,2,',','.');?>
							<?php } ;?>-->
						</td>
						<!--<td style="text-align: center"><?php if($n->dprice != ($n->price/$n->total)){ echo (100-(($n->price/$n->total)/$n->dprice*100))."%" ;}else{ echo "-" ;} ;?> 
						</td>-->
						<td><?php echo $n->total ;?> pcs
						</td>
						<td><?php echo $n->name ?></td>
						<td><?php echo date("l, d M Y H:i:s", strtotime($n->datecreated)) ?></td>
						<td>
							<!--
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ ;?>
									<button class="btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 1px"> <?php echo $s->name ;?></button>
								<?php ;} ;?>
							<?php ;} ;?>
							-->
							<button id="<?php echo $n->trx_code?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_custom" data-url3="detail" data-lang="2" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 1px">View</button>
						</td>
					</tr>	
					<?php ;} $i++;} ?>
				</tbody>
			</table>
		</div>	
		<?php };?>
	</div>
</div>
<script>
$('.showImage').click(function(){
	id = $(this).attr('id');
	target = $(this).siblings('img');
	target.toggle('slow');
});
</script>