<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-9" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_name);?>				
				</a>
			</li>
			<?php if($id != 0){ ;?>
				<?php if($cat->parent_id != 0){ ;?>
				<li class="">
					<a id="<?php echo $cat->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
						<?php echo $cat->parent_title ;?>
					</a>
				</li>
				<?php } ;?>
				<li class="">
					<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
						<?php echo $cat->title ;?>
					</a>
				</li>
				<li class="active">
					<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new"  data-url3="<?php echo $segretissimo?>" data-lang="2" class="detail2">
						New Multiple Product
					</a>
				</li>
			<?php }else{ ;?>
				<li class="active">
					<a id="0" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
						New
					</a>
				</li>
			<?php } ;?>
		</ol>
			<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
			<button id="<?php echo  $cat->category_id ?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" data-url3="single" class="detail2 btn-sm btn btn-primary pull-right" style="margin-right : 5px">Add Single Product</button>
	</div>
	<div class="ibox-content row">		
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		<div class="row col-sm-12 col-xs-12" style="margin-bottom: 20px">
			<form class="form-horizontal" method="get">
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<div class="form-group">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left">Select Product</label>
					<div class="col-sm-6 col-xs-6">
						<div class="input-group  col-sm-12 col-xs-12">
							<select id="pdc" name="inputan" data-placeholder="Select" class="chosen-select col-sm-12"  tabindex="2">
							<option value="">Select Product</option>
							<?php foreach($product as $p){?>
								<option value="<?php echo $p->object_id;?>" data-var="<?php if($p->varian_id != 0){ echo $p->varian ;}else{ echo "-" ;};?>" data-price="<?php echo number_format($p->publish,2,'.',',')?>" data-stock="<?php echo $p->stock ;?>" data-cat="<?php echo $p->category_id?>" data-title="<?php echo $p->title?>">
									<?php echo $p->title;?><?php if($p->varian_id != 0){ echo ", ".$p->varian." Varian" ;};?> ( Rp.<?php echo "Rp.".number_format($p->publish, 2, '.',',')?>; <?php echo $p->stock?> pcs left )
								</option>
							<?php } ?>
							</select>
						</div>
					</div>					
					<button id="add_product" type="button" class="btn-sm btn btn-primary">Add Product</button>
					<button id="checkout" type="button" class="btn-sm btn btn-primary">Checkout</button>
					
				</div>
			</form>
		</div>
		<div class="col-sm-12 col-xs-12">	
			<div id="data_trx" class="row data_trx">
				<table id="datatrx" class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
					<thead>
						<tr>
							<th>No.</th>
							<th>Product Name</th>
							<th>Varian</th>
							<th>Stock</th>
							<th>Total</th>
							<th>Price</th>
						</tr>
					</thead>
					<tfoot>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){	
	$('#data_trx').hide();	
	var counter = 0;
	
	$('#pdc').change(function() {
		$('#data_trx').slideDown('slow');
		var prc = $(this).find(':selected').data('price');
		var varian = $(this).find(':selected').data('var');
		var stock = $(this).find(':selected').data('stock');
		var ttl = $(this).find(':selected').data('title');
		var id = $(this).val();
		
		var t = $('#datatrx').DataTable();
	 
		$('#add_product').on( 'click', function (e){
			e.preventDefault();
			t.row.add( [
				counter+1,
				''+ttl+'',
				''+varian+'',
				''+stock+'',
				1,
				''+prc+''
			] ).draw();
	 
			counter++;
		});
	});
});
</script>