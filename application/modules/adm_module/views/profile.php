<button id="profile_view_add_new" type="button" class="menu btn btn-primary col-md-1 col-xs-2 hide">Add New</button>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Profile Detail</h5>
	</div>
	<div>
		<div id="ava2" class="ibox-content no-padding border-left-right">
			<img id="ava" alt="image" class="img-responsive" src="<?php echo base_url();?><?php echo $this->session->userdata($zone.'_foto');?>" style="width: 100%;">
		</div>
		<div class="ibox-content profile-content">
			<p class="pull-right"><strong>Superadmin</strong></p>
			<h4><strong id="name" name="inputan"><?php echo $profile->name;?></strong></h4>
			<p><i class="fa fa-map-marker"></i> <?php if(!empty(trim($profile->address))){echo trim($profile->address);}else{echo '-';};?></p>
			<h5>
				About me
			</h5>
			<p>
				<?php if(!empty(trim($profile->description))){echo trim($profile->description);}else{echo '-';};?>
			</p>
			<div class="user-button">
				<div class="row">
						<button id="<?php echo $profile->admin_id;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="edit_category" type="button" class="detail2 btn btn-primary btn-sm pull-right" style="margin:0 5px"><i class="fa fa-pencil"></i> Edit</button>
						<button id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" type="button" class="detail2 btn btn-primary btn-sm pull-right" style="margin:0 5px"><i class="fa fa-clock-o"></i> Last Activity</button>
				</div>
			</div>
		</div>
	</div>
</div>