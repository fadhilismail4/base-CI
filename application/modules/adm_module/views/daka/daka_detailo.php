
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-10 col-xs-10"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; ">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>		
			<?php if($parole == 'detail'){ ;?>
			<li class="active">
				<a id="<?php echo $objects->rc_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" class="detail2">
					<?php if($objects->category_id == 2){ ;?>
						<?php echo ucwords($objects->char_4) ?>
					<?php }else{ ;?>
						<?php echo ucwords($objects->char_1) ?>
						
					<?php } ;?>
				</a>
			</li>
			<?php }else{ ;?>
			<li class="">
				<a id="<?php echo $objects->rc_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" class="detail2">
					<?php if($objects->category_id == 2){ ;?>
						<?php echo ucwords($objects->char_4) ?>
					<?php }else{ ;?>
						<?php echo ucwords($objects->char_1) ?>
						
					<?php } ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->rc_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_custom" data-lang="2" data-url3="edit_<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" class="detail2">
					Update 
				</a>
			</li>
			<?php }?>
		</ol>
		<?php if($parole == 'detail'){ ;?>
			<button id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php }else{ ;?>
			<button id="<?php echo $objects->rc_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-url3="<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" data-lang="2" class="detail2  pull-right btn-sm btn btn-warning ">Back</button>
		<?php } ;?>
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $ccs_key?>"></input>
		<input id="rc_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->rc_id?>"></input>
		<?php if($parole == 'detail'){ ;?>
		<div class="row">
			<?php foreach($status as $s){ ;?>
				<?php if($objects->status != $s->value){if($s->value != 2){ ;?>
					<button id="<?php echo $objects->rc_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php if($objects->category_id == 2){ ;?><?php echo ucwords($objects->char_4) ?> on <?php echo ucwords($objects->char_7)?> Email<?php }else{ ;?><?php echo ucwords($objects->char_1) ?> on <?php if($objects->rc_id == 0){echo "Company Info";}elseif($objects->rc_id == 1){ echo "Bank Account";}else{ echo "Time Interval" ;} ;?><?php } ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 0 5px"> <?php echo $s->name ;?></button>
				<?php ;};} ;?>
				
			<?php ;} ;?>
			<button id="<?php echo $objects->rc_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_custom" data-lang="2" class="detail2 btn btn-info btn-xs pull-right" data-url3="edit_<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" type="button" style="margin: 0 5px">Edit</button>
			<button id="<?php echo $objects->rc_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="web" data-title="<?php if($objects->category_id == 2){ ;?><?php echo ucwords($objects->char_4) ?> on <?php echo ucwords($objects->char_7)?> Email<?php }else{ ;?><?php echo ucwords($objects->char_1) ?> on <?php if($objects->rc_id == 0){echo "Company Info";}elseif($objects->rc_id == 1){ echo "Bank Account";}else{ echo "Time Interval" ;} ;?><?php } ;?>"data-lang="2" class="modal_dlt btn btn-white ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Delete</button>
		</div>
		<div class="row" style="margin-top: 20px">
			<?php if($chiave == 'email'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Name</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php echo $objects->char_4?></strong></p></div>
					<p class="col-sm-2 col-xs-3">Status</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php if($objects->status == 1){echo "Active";}else{ echo "Inactive";} ;?></strong></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Category</p>
					<div class="col-sm-4 col-xs-9">
						<p>: 
							<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $objects->category?>
							</a>
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Creator</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->name?></p></div>
					<p class="col-sm-2 col-xs-3">Date Created</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo date("l, d M Y H:i:s", strtotime($objects->datecreated)) ?></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">SMTP Port</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_1?></p></div>
					<p class="col-sm-2 col-xs-3">SMTP Host</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_2?></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Email Account</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_5?></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Short Description</p>
					<div class="col-sm-10 col-xs-9"><p>: <?php echo $objects->char_6?></p></div>
				</div>
			<?php }elseif($chiave == 'info'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Company Name</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php echo $objects->char_1?></strong></p></div>
					<p class="col-sm-2 col-xs-3">Status</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php if($objects->status == 1){echo "Active";}else{ echo "Inactive";} ;?></strong></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Category</p>
					<div class="col-sm-4 col-xs-9">
						<p>: 
							<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $objects->category?>
							</a>
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Creator</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->name?></p></div>
					<p class="col-sm-2 col-xs-3">Date Created</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo date("l, d M Y H:i:s", strtotime($objects->datecreated)) ?></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Official Email</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_2?></p></div>
					<p class="col-sm-2 col-xs-3">Postal Code</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->url_id?></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Phone</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_3?></p></div>
					<p class="col-sm-2 col-xs-3">Fax</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_4?></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Address</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_5?></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px"><p class="col-sm-2 col-xs-3">Short Description</p>
					<div class="col-sm-10 col-xs-9"><p>: <?php echo $objects->char_6?></p></div>
				</div>
			<?php }elseif($chiave == 'bank'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Bank Provider</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php echo $objects->char_1?></strong></p></div>
					<p class="col-sm-2 col-xs-3">Status</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php if($objects->status == 1){echo "Active";}else{ echo "Inactive";} ;?></strong></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Category</p>
					<div class="col-sm-4 col-xs-9">
						<p>: 
							<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $objects->category?>
							</a>
						</p>
					</div>
				</div>		
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Account Name</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_2?></p></div>
					<p class="col-sm-2 col-xs-3">Account No</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_3?></p></div>
				</div>		
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Creator</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->name?></p></div>
					<p class="col-sm-2 col-xs-3">Date Created</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo date("l, d M Y H:i:s", strtotime($objects->datecreated)) ?></p></div>
				</div>		
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px"><p class="col-sm-2 col-xs-3">Short Description</p>
					<div class="col-sm-10 col-xs-9"><p>: <?php echo $objects->char_4?></p></div>
				</div>			
			<?php }elseif($chiave == 'interval'){ ;?>
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Bank Provider</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php echo $objects->char_1?></strong></p></div>
					<p class="col-sm-2 col-xs-3">Status</p>
					<div class="col-sm-4 col-xs-9"><p><strong>: <?php if($objects->status == 1){echo "Active";}else{ echo "Inactive";} ;?></strong></p></div>
				</div>				
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Category</p>
					<div class="col-sm-4 col-xs-9">
						<p>: 
							<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
								<?php echo $objects->category?>
							</a>
						</p>
					</div>
					<p class="col-sm-2 col-xs-3">Interval </p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->char_3?> <?php echo $objects->char_2?> from transaction time</p></div>
				</div>		
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px">
					<p class="col-sm-2 col-xs-3">Creator</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo $objects->name?></p></div>
					<p class="col-sm-2 col-xs-3">Date Created</p>
					<div class="col-sm-4 col-xs-9"><p>: <?php echo date("l, d M Y H:i:s", strtotime($objects->datecreated)) ?></p></div>
				</div>		
				<div class="col-sm-12 col-xs-12" style="padding-left: 0px"><p class="col-sm-2 col-xs-3">Short Description</p>
					<div class="col-sm-10 col-xs-9"><p>: <?php echo $objects->char_4?></p></div>
				</div>
			<?php } ;?>
		</div>
		<div class="row" style="margin-top: 20px">
		<?php }elseif($parole == 'edit'){ ;?>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->category_id?>"></input>
		<input id="char_7" name="inputan" type="text" class="form-control hide" value="<?php echo trim($objects->char_7)?>"></input>
			<?php if($chiave == 'edit_email'){ ;?>
				<div class="form-horizontal">				
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Category</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="category_id">	
									<option value="<?php echo $objects->category_id ;?>"><?php echo $objects->category ;?></option>
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Payment Info</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="object_id">	
									<option value="<?php echo $objects->object_id?>">
										<?php if($objects->object_id == 0){ echo "Subscribe" ;}elseif($objects->object_id == 1){ echo "Online Transaction";}else{ echo "Email Blast ( Promo )";} ;?>
									</option>
								</select>
							</div>
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Outgoing Port</label>
						<div class="col-sm-4 col-xs-8 ">
							<input id="char_1" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_1) ;?>" placeholder="Port number [ numeric only ]" />
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">SMTP Host</label>
						<div class="col-sm-4 col-xs-8 " >
							<input id="char_2" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_2) ;?>" placeholder="Example : mail.buzcon.com" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Update Password</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="password">	
									<option value="0">Select Option</option>
									<option value="1">Update Password</option>
									<option value="0">Don't Update</option>
								</select>
							</div>
						</div>
						<div class="clearfix" id="data_old">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Old Password</label>
							<div class="col-sm-4 col-xs-8 ">
								<input id="char_3" name="inputan" type="password" class="form-control"  value="xxxxxxxx" placeholder="Old Password">
							</div>
						</div>
					</div>
					<div class="clearfix" id="data_new">
						<div class="form-group">
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">New Password</label>
							<div class="col-sm-4 col-xs-8 " >
								<input id="char_8" name="inputan" type="password" class="form-control"  placeholder="New Password">
							</div>
							<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Confirmation Password</label>
							<div class="col-sm-4 col-xs-8 " >
								<input id="char_9" name="inputan" type="password" class="form-control"  placeholder="Password Confirmation">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Name</label>
						<div class="col-sm-4 col-xs-8 ">
							<input id="char_4" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_4) ;?>" placeholder="Name for email sender.">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Email Account</label>
						<div class="col-sm-4 col-xs-8 " >
							<input id="char_5" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_5) ;?>" placeholder="Put your email account here.">
						</div>
					</div>		
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_6" name="inputan"  type="text" class="form-control" placeholder="This is your email description, a lot of space here, feel free to write your content. Cheers"><?php echo trim($objects->char_6) ;?></textarea>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12 " >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Activation</option>
									<?php foreach($status as $s){if($s->value != 2){ ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php ;};} ;?>
								</select>
							</div>
						</div>
					</div>	
					<div class="space-25"></div>
					<div class="form-group">			
						<button id="<?php echo $key_link;?>" data-param="settings" data-param2="web" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Update</button>		
						<button id="<?php echo $objects->rc_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-url3="<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</div>
				</div>
			<?php }elseif($chiave == 'edit_info'){ ;?>
				<div class="form-horizontal">				
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Category</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="category_id">	
									<option value="<?php echo $objects->category_id ;?>"><?php echo $objects->category ;?></option>
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Payment Info</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="object_id">	
									<option value="<?php echo $objects->object_id?>">
										<?php if($objects->object_id == 0){ echo "Company Information" ;}elseif($objects->object_id == 1){ echo "Bank Account";}else{ echo "Time Interval";} ;?>
									</option>
								</select>
							</div>
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Company Name</label>
						<div class="col-sm-4 col-xs-8 ">
							<input id="char_1" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_1)?>" placeholder="Your company name.">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Email</label>
						<div class="col-sm-4 col-xs-8 " >
							<input id="char_2" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_2)?>" placeholder="Official company email.">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Phone</label>
						<div class="col-sm-4 col-xs-8 ">
							<input id="char_3" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_3)?>" placeholder="Phone number">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Fax</label>
						<div class="col-sm-4 col-xs-8 " >
							<input id="char_4" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_4)?>" placeholder="Fax number">
						</div>
					</div>						
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Postal Code</label>
						<div class="col-sm-4 col-xs-8 ">
							<input id="url_id" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->url_id)?>" placeholder="Postal Code">
						</div>
					</div>		
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Address</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_5" name="inputan" type="text" class="form-control" placeholder="This is your address information, a lot of space here, feel free to write your content. Cheers"><?php echo trim($objects->char_5)?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Company Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_6" name="inputan"   type="text" class="form-control" placeholder="This is your short description, a lot of space here, feel free to write your content. Cheers"><?php echo trim($objects->char_6)?></textarea>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12 " >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Activation</option>
									<?php foreach($status as $s){if($s->value != 2){ ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php ;};} ;?>
								</select>
							</div>
						</div>
					</div>	
					<div class="space-25"></div>
					<div class="form-group">			
						<button id="<?php echo $key_link;?>" data-param="settings" data-param2="web" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Update</button>		
						<button id="<?php echo $objects->rc_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-url3="<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</div>
				</div>
			<?php }elseif($chiave == 'edit_interval'){ ;?>
				<div class="form-horizontal">				
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Category</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="category_id">	
									<option value="<?php echo $objects->category_id ;?>"><?php echo $objects->category ;?></option>
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Payment Info</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="object_id">	
									<option value="<?php echo $objects->object_id?>">
										<?php if($objects->object_id == 0){ echo "Company Information" ;}elseif($objects->object_id == 1){ echo "Bank Account";}else{ echo "Time Interval";} ;?>
									</option>
								</select>
							</div>
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Interval Type</label>
						<div class="col-sm-4 col-xs-8 " >
							<div class="input-group col-sm-12 col-xs-12 " >
								<select class="form-control m-b" name="inputan" id="char_2">
									<option value="<?php echo trim($objects->char_1)?>"><?php echo trim($objects->char_1);?></option>
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Interval</label>
						<div class="col-sm-2 col-xs-4 ">
							<input id="char_3" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_3)?>" placeholder="Only number, don't use text.">
						</div>
						<div class="col-sm-2 col-xs-4">
							<p style="font-size: 14px; margin-top: 6px; color: #f39c12"><?php echo trim($objects->char_2)?></p>
						</div>
					</div>		
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_4"  name="inputan" type="text" class="form-control" placeholder="This is your short description, a lot of space here, feel free to write your content. Cheers"><?php echo trim($objects->char_4)?></textarea>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12 " >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Activation</option>
									<?php foreach($status as $s){if($s->value != 2){ ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php ;};} ;?>
								</select>
							</div>
						</div>
					</div>	
					<div class="space-25"></div>
					<div class="form-group">			
						<button id="<?php echo $key_link;?>" data-param="settings" data-param2="web" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Update</button>		
						<button id="<?php echo $objects->rc_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-url3="<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</div>
				</div>
			<?php }elseif($chiave == 'edit_bank'){ ;?>
				<div class="form-horizontal">				
					<div class="form-group ">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Category</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="category_id">	
									<option value="<?php echo $objects->category_id ;?>"><?php echo $objects->category ;?></option>
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Payment Info</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="object_id">	
									<option value="<?php echo $objects->object_id?>">
										<?php if($objects->object_id == 0){ echo "Company Information" ;}elseif($objects->object_id == 1){ echo "Bank Account";}else{ echo "Time Interval";} ;?>
									</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Bank</label>
						<div class="col-sm-4 col-xs-8 " >
							<input id="char_1" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_1)?>" placeholder="Bank company provider.">
						</div>
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Account Name</label>
						<div class="col-sm-4 col-xs-8 ">
							<input id="char_2" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_2)?>" placeholder="Your bank account name.">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Account No.</label>
						<div class="col-sm-10 col-xs-8 ">
							<input id="char_3" name="inputan" type="text" class="form-control" value="<?php echo trim($objects->char_3)?>" placeholder="Your bank account number. Format : xxx-xx-xxxxxxxx">
						</div>
					</div>				
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_4" name="inputan" type="text" class="form-control" placeholder="This is your short description, a lot of space here, feel free to write your content. Cheers"><?php echo trim($objects->char_4)?></textarea>
						</div>
					</div>					
					<div class="form-group" >
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
						<div class="col-sm-4 col-xs-8" >
							<div class="input-group col-sm-12 col-xs-12 " >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Select Activation</option>
									<?php foreach($status as $s){if($s->value != 2){ ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php ;};} ;?>
								</select>
							</div>
						</div>
					</div>	
					<div class="space-25"></div>
					<div class="form-group">			
						<button id="<?php echo $key_link;?>" data-param="settings" data-param2="web" class="create_mdl btn btn-primary btn-md col-sm-2 pull-right"><i class="fa fa-check-square"></i>  Update</button>		
						<button id="<?php echo $objects->rc_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_custom" data-url3="<?php if($objects->category_id == 1){ echo $objects->char_7 ;}else{echo "email" ;}?>" data-lang="2" class="detail2 btn-md btn btn-white col-sm-2  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					</div>
				</div>
			<?php } ;?>
		<?php } ;?>
		</div>
	</div>
</div>
<?php if($parole == 'edit'){ ;?>
<script>
	$('#status').val(<?php echo $objects->status?>);
	$('#data_old').hide();
	$('#data_new').hide();
	$(document).ready(function(){
		$('#password').change(function() {	
			if ($(this).find(':selected').val() == 1) {
				$('#data_old').slideDown('slow');		
				$('#data_new').slideDown('slow');					
			}else{
				$('#data_old').slideUp('slow');
				$('#data_new').slideUp('slow');		
			}
		});
	});
</script>
<?php } ;?>