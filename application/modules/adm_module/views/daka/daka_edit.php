<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-9"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_category" data-lang="2" class="detail2">
					Update
				</a>
			</li>
		</ol>
		<button id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right ">Back</button>		
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $category->ccs_key?>"></input>
		<input id="parent_id" name="inputan" type="text" class="form-control hide" value="0"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label">Name</label>
				<div class="col-sm-10 col-xs-12"><input id="title" name="inputan" type="text" class="form-control" value="<?php echo $category->title?>" disabled></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-xs-12 control-label">Description</label>
				<div class="col-sm-10 col-xs-12"><textarea id="description" name="inputan" type="text" class="form-control"><?php echo $category->description?></textarea></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-12 col-xs-12 col-sm-offset-2 pull-right">				
					<button id="<?php echo $key_link;?>" data-param="category" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square"></i>  Update</button>
					<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="detail_category" data-lang="" class="detail2 btn btn-md btn-white pull-right" type="submit"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>