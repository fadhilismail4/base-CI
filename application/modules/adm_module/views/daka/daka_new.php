<div class="ibox row">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-10 col-xs-9" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_name);?>				
				</a>
			</li>
			<?php if($id != 0){ ;?>
			<li class="">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $cat->title ;?>
				</a>
			</li>
			<?php } ;?>
			<?php if($id != 0){ ;?>
			<li class="active">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
					New
				</a>
			</li>
			<?php }else{ ;?>
			<li class="active">
				<a id="0" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
					New
				</a>
			</li>
			<?php } ;?>
		</ol>
		<?php if ($id == 0){ ;?>
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
		<?php }else{ ;?>
			<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
		<?php } ;?>
	</div>
	<div class="ibox-content row">
		<div class="col-sm-12 col-xs-12">				
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
			<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
			<div class="form-horizontal">				
				<div class="form-group ">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Category</label>
					<div class="col-sm-4 col-xs-8">
						<div class="input-group col-sm-12 col-xs-12" >
							<select class="form-control m-b" name="inputan" id="category_id">	
								<option value="">Select Category</option>	
								<?php foreach($category as $ct){ ;?>
								<option value="<?php echo $ct->category_id ;?>"><?php echo $ct->title ;?></option>
								<?php  } ;?>
							</select>
						</div>
					</div>
					<div class="clearfix" id="data_4">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Payment Info</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="object_id">	
									<option value="">Select Payment Info</option>
									<option value="0">Company Information</option>		
									<option value="1">Bank Account</option>	
									<option value="2">Time Interval</option>	
								</select>
							</div>
						</div>
					</div>
					<div class="clearfix" id="data_3">
						<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important";>Email Type</label>
						<div class="col-sm-4 col-xs-8">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="object_id">	
									<option value="">Select Email Type</option>	
									<option value="0">Subscribe</option>	
									<option value="1">Online Transaction</option>	
									<option value="2">Email Blast ( Promo )</option>	
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix" id="data_info">
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Company Name</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="char_1" name="inputan" type="text" class="form-control" value="" placeholder="Your company name.">
						</div>
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Email</label>
						<div class="col-sm-4 col-xs-12 " >
							<input id="char_2" name="inputan" type="text" class="form-control" placeholder="Official company email.">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Phone</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="char_3" name="inputan" type="text" class="form-control" value="" placeholder="Phone number">
						</div>
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Fax</label>
						<div class="col-sm-4 col-xs-12 " >
							<input id="char_4" name="inputan" type="text" class="form-control" placeholder="Fax number">
						</div>
					</div>						
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Postal Code</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="url_id" name="inputan" type="text" class="form-control" value="" placeholder="Postal Code">
						</div>
					</div>		
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Address</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_5" name="inputan" type="text" class="form-control" placeholder="This is your address information, a lot of space here, feel free to write your content. Cheers"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Company Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_6" name="inputan"   type="text" class="form-control" placeholder="This is your short description, a lot of space here, feel free to write your content. Cheers"></textarea>
						</div>
					</div>
				</div>
				<div class="clearfix" id="data_account">
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Bank</label>
						<div class="col-sm-4 col-xs-12 " >
							<input id="char_1" name="inputan" type="text" class="form-control" placeholder="Bank company provider.">
						</div>
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Account Name</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="char_2" name="inputan" type="text" class="form-control" value="" placeholder="Your bank account name.">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Account No.</label>
						<div class="col-sm-10 col-xs-12 ">
							<input id="char_3" name="inputan" type="text" class="form-control" value="" placeholder="Your bank account number. Format : xxx-xx-xxxxxxxx">
						</div>
					</div>				
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_4"  type="text" class="form-control" placeholder="This is your short description, a lot of space here, feel free to write your content. Cheers"></textarea>
						</div>
					</div>
				</div>
				<div class="clearfix" id="data_interval">
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Interval Type</label>
						<div class="col-sm-4 col-xs-12 " >
							<div class="input-group col-sm-12 col-xs-12 " >
								<select class="form-control m-b" name="inputan" id="char_2">
									<option value="">Select Interval</option>
									<option value="days">Daily Interval</option>
									<option value="hours">Hourly Interval</option>								
								</select>
							</div>
						</div>
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Interval</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="char_3" name="inputan" type="text" class="form-control" value="" placeholder="Only number, don't use text.">
						</div>
					</div>		
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Short Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_4"  name="inputan" type="text" class="form-control" placeholder="This is your short description, a lot of space here, feel free to write your content. Cheers"></textarea>
						</div>
					</div>
				</div>
				<div class="clearfix" id="data_email">
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Outgoing Port</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="char_1" name="inputan" type="text" class="form-control" value="" placeholder="Port number [ numeric only ]">
						</div>
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">SMTP Host</label>
						<div class="col-sm-4 col-xs-12 " >
							<input id="char_2" name="inputan" type="text" class="form-control" placeholder="Example : mail.buzcon.com">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Password</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="char_3" name="inputan" type="password" class="form-control" value="" placeholder="Password">
						</div>
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Confirm Password</label>
						<div class="col-sm-4 col-xs-12 " >
							<input id="char_7" name="inputan" type="password" class="form-control" placeholder="Password Confirmation">
						</div>
					</div>						
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Name</label>
						<div class="col-sm-4 col-xs-12 ">
							<input id="char_4" name="inputan" type="text" class="form-control" value="" placeholder="Name for email sender.">
						</div>
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Email Account</label>
						<div class="col-sm-4 col-xs-12 " >
							<input id="char_5" name="inputan" type="text" class="form-control" placeholder="Put your email account here.">
						</div>
					</div>		
					<div class="form-group">
						<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Description</label>
						<div class="col-sm-10 col-xs-12 " >
							<textarea id="char_6" name="inputan"  type="text" class="form-control" placeholder="This is your email description, a lot of space here, feel free to write your content. Cheers"></textarea>
						</div>
					</div>
				</div>
				<div class="form-group" >
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Activation</label>
					<div class="col-sm-4 col-xs-8" >
						<div class="input-group col-sm-12 col-xs-12 " >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Activation</option>
								<?php foreach($status as $s){if($s->value != 2){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php ;};} ;?>
							</select>
						</div>
					</div>
				</div>					
				<div class="space-25"></div>
				<div class="form-group">			
					<button id="<?php echo $key_link;?>" data-param="settings" data-param2="web" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Create</button>				
					<?php if ($id == 0){ ;?>
						<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-param2="category" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					<?php }else{ ;?>
						<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					<?php } ;?>
				</div>
			</div>
		</div>
	</div>	
</div>
<script>
$(document).ready(function(){
	<?php if($id == 'all'){?>
	$('#data_3').hide();
	$('#data_4').hide();
	$('#data_email').hide();
	$('#data_info').hide();
	$('#data_account').hide();
	$('#data_interval').hide();
	<?php } ;?>
	//$('.form-horizontal').find('[name="inputan"]').attr('name','abc');
	$('#category_id').change(function() {
		if ($(this).find(':selected').val() == 1) {
			$('#data_4').slideDown('slow');	
			$('#data_4').find('[name="abc"]').attr('name','inputan');
			$('#object_id').change(function() {	
				if ($(this).find(':selected').val() == 0) {
					$('#data_info').slideDown('slow');
					$('#data_info').find('[name="abc"]').attr('name','inputan');					
				}else{
					$('#data_info').slideUp('slow');
					$('#data_info').find('[name="inputan"]').attr('name','abc');
				}
				if ($(this).find(':selected').val() == 1) {
					$('#data_account').slideDown('slow');					
					$('#data_account').find('[name="abc"]').attr('name','inputan');					
				}else{
					$('#data_account').slideUp('slow');
					$('#data_account').find('[name="inputan"]').attr('name','abc');
				}
				if ($(this).find(':selected').val() == 2) {	
					$('#data_interval').slideDown('slow');				
					$('#data_interval').find('[name="abc"]').attr('name','inputan');				
				}else{
					$('#data_interval').slideUp('slow');
					$('#data_interval').find('[name="inputan"]').attr('name','abc');
				}
				if($(this).find(':selected').val() == '') {
					$('#data_info').slideUp('slow');
					$('#data_account').slideUp('slow');
					$('#data_interval').slideUp('slow');
					$('#data_info').find('[name="inputan"]').attr('name','abc');
					$('#data_account').find('[name="inputan"]').attr('name','abc');
					$('#data_interval').find('[name="inputan"]').attr('name','abc');
				}
			});
		}else{
			$('#data_4').slideUp('slow');
			$('#data_info').slideUp('slow');
			$('#data_account').slideUp('slow');
			$('#data_interval').slideUp('slow');
			$('#data_interval').find('[name="inputan"]').attr('name','abc');
			$('#data_account').find('[name="inputan"]').attr('name','abc');
			$('#data_info').find('[name="inputan"]').attr('name','abc');
			$('#data_4').find('[name="inputan"]').attr('name','abc');
		}
	
		if($(this).find(':selected').val() == 2) {
			$('#data_3').slideDown('slow');
			$('#data_email').slideDown('slow');
			$('#data_3').find('[name="abc"]').attr('name','inputan');
			$('#data_email').find('[name="abc"]').attr('name','inputan');
		}else{
			$('#data_3').slideUp('slow');
			$('#data_email').slideUp('slow');
			$('#data_3').find('[name="inputan"]').attr('name','abc');
			$('#data_email').find('[name="inputan"]').attr('name','abc');
		}
		
		if($(this).find(':selected').val() == '') {
			$('#data_3').slideUp('slow');
			$('#data_4').slideUp('slow');
			$('#data_email').slideUp('slow');
			$('#data_3').find('[name="inputan"]').attr('name','abc');
			$('#data_4').find('[name="inputan"]').attr('name','abc');
			$('#data_email').find('[name="inputan"]').attr('name','abc');
		}
	});
});
</script>