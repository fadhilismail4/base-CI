<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb  col-sm-8 col-xs-7" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					All <?php echo $key_name?> Content
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name?>
				</a>
			</li>
			<?php if($categories->parent_id == 0){ ;?>
			<li class="active">
				<a id="<?php echo $categories->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php }else{ ;?>
			<li class="">
				<a id="<?php echo $categories->parent_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->parent_title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
			<?php } ;?>
		</ol>
		<?php } ;?>
		<?php if ($id != 'all'){ ;?>			
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn btn-sm  btn-warning pull-right" style="margin-left: 5px">Back</button>			
		<?php } ;?>
		<button id="0" data-url="module" data-url2="<?php echo $key_link ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary pull-right">Add New</button>	
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>It's empty here, please create your sitemap content... </h3>
		<?php }else{ ;?>
		<div class="table-responsive" style="border: 0;">
			<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>No</th>
						<th style="text-align: center">Title </th>
						<th style="text-align: center">Category </th>
						<th>Createdby</th>
						<th style="text-align: center">Date Created </th>
						<th>Status</th>
						<th style="text-align: center">Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($objects as $n){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<a id="<?php echo $n->rc_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="detail_custom" data-url3="<?php if($n->category_id == 1){ echo $n->char_7 ;}else{echo "email" ;}?>" class="dtl">
								<?php if($n->category_id == 2){ ;?>
									<strong><?php echo ucwords($n->char_4) ?></strong> 
									<br>
									on <?php echo ucwords($n->char_7)?> Email
								<?php }else{ ;?>
									<strong><?php echo ucwords($n->char_1) ?></strong> 
									<br>
									on <?php if($n->object_id == 0){echo "Company Info";}elseif($n->object_id == 1){ echo "Bank Account";}else{ echo "Time Interval" ;} ;?>
								<?php } ;?>
							</a>
						</td>
						
						<td style="text-align: center">
							<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>" data-lang="2" data-param="list" class="dtl">
								<?php echo $n->category ;?>
							</a> 
						</td>
						<td><?php echo $n->name ?></td>
						<td style="text-align: center"><?php echo date("l, d M Y", strtotime($n->datecreated)) ?><?php echo " at".date(" H:i:s", strtotime($n->datecreated)) ?></td>
						<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
						<td>
							<button id="<?php echo $n->rc_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_custom" data-url3="<?php if($n->category_id == 1){ echo $n->char_7 ;}else{echo "email" ;}?>" data-lang="2" class="dtl btn btn-success btn-xs pull-right" type="button" style="margin: 2px">View</button>
							<button id="<?php echo $n->rc_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="detail_custom" data-url3="edit_<?php if($n->category_id == 1){ echo $n->char_7 ;}else{echo "email" ;}?>" data-lang="2" class="dtl btn btn-info btn-xs pull-right" type="button" style="margin: 2px">Edit</button>
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ if($s->value != 2){  ;?>
									<button id="<?php echo $n->rc_id ?>" data-url="web" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php if($n->category_id == 2){ ;?><?php echo ucwords($n->char_4) ?> on <?php echo ucwords($n->char_7)?> Email<?php }else{ ;?><?php echo ucwords($n->char_1) ?> on <?php if($n->object_id == 0){echo "Company Info";}elseif($n->object_id == 1){ echo "Bank Account";}else{ echo "Time Interval" ;} ;?><?php } ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 2px"> <?php echo $s->name ;?></button>
								<?php ;} ;?>
								<?php ;} ;} ;?>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
		<?php };?>
	</div>
</div>