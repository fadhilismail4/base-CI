 <div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Last activity</h5>
	</div>
	<div class="ibox-content">
		
		<ul class="list-group clear-list">
		<?php if(!empty($objects)){;?>
			<?php $i=1;foreach($objects as $lg){
				if(isset($lg->title)){
					if($lg->type == 1){
						$info = '<span class="pull-right"> <span class="label label-warning">CREATE</span> </span>';
					}elseif($lg->type == 2){
						$info = '<span class="pull-right"> <span class="label label-primary">READ</span> </span>';
					}elseif($lg->type == 3){
						$info = '<span class="pull-right"> <span class="label label-success">UPDATE</span> </span>';
					}elseif($lg->type == 4){
						$info = '<span class="pull-right"> <span class="label label-danger">DELETE</span> </span>';
					}else{
						$info = '';
					}
				}else{
					$info = '';
				}
				?>
				<li class="list-group-item <?php if($i == 1){echo 'fist-item';}?>">
					<?php echo $info;?>
					<strong><?php echo $lg->module;?></strong><?php if(isset($lg->title)){ echo ' / '.$lg->category;};?><?php if(isset($lg->title)){ echo ' / '.$lg->title;};?><br><small><?php echo date('d-M-Y H:i:s', strtotime($lg->datecreated));?></small>
				</li>
			<?php ;$i++;}?>
		<?php }else{ ?>
		<i>Doesn't have any recent activity</i>	
		</ul>
		
		<?php } ?>
		
	</div>
</div>
<script>
$('.btn-file').hide();
$('.file-input').slideUp();
$('.file-input').remove();
$("[data-param='edit_category']").slideDown();
$('#ava').slideDown();
</script>