<div class="ibox float-e-margins">
	<div class="ibox-title row" style="padding-bottom: 0px !important">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" class="detail2">
					<?php echo $category->title;?> 
				</a>
			</li>
			
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn-sm btn btn-warning ">Back</button>
		</div>		
	</div>
	<div class="ibox-content row" >
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="category_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category->category_id?>"></input>
		<div class="col-sm-12 " style="padding-bottom: 10px">
			<button class="btn btn-white btn-xs pull-left disabled" type="button" style="margin: 0 5px"><?php if($category->language_id == 2){echo "ID"; }else{echo "EN";};?></button>
			<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2 btn btn-white btn-xs pull-left" type="button" style="margin: 0 5px">Content List</button>
			<?php foreach($status as $s){ ;?>
				<?php if($category->status != $s->value){ ;?>
					<button id="<?php echo $category->category_id ?>" data-url="category" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $category->title ;?>" class="modal_stas btn btn-<?php echo $s->color ;?> btn-xs pull-right" type="button" style="margin: 0 5px"> <?php echo $s->name ;?></button>
				<?php ;} ;?>
				
			<?php ;} ;?>
			<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_category" data-lang="2" class="detail2 btn btn-info ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Edit</button>
			<button id="<?php echo $category->category_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="category" data-title="<?php echo trim($category->title);?>"data-lang="2" class="modal_dlt btn btn-white ;?> btn-xs pull-right" type="button" style="margin: 0 5px">Delete</button>
		</div>		
		<div class="row">
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><h4>Name</h4></div>
				<div class="col-sm-10 col-xs-9"><h4><strong><?php echo $category->title?></strong></h4></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Description</p></div>
				<div class="col-sm-10 col-xs-9"><p><?php echo $category->description?></p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Status</p></div>
				<div class="col-sm-10 col-xs-9"><p><strong><?php if($category->status == 1){ echo "Active" ;}elseif($category->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></strong></p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Date Created</p></div>
				<div class="col-sm-10 col-xs-9"><p><?php echo date("l, d M Y", strtotime($category->datecreated))  ;?></p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Creator</p></div>
				<div class="col-sm-10 col-xs-9"><p><?php echo $category->name ;?></p></div>
			</div>
			
		</div>
	</div>
</div>