<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-9" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $key_name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->category;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->settings_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" data-url3="konten" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->settings_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_object" data-lang="2" data-url3="konten" class="detail2">
					Edit Content
				</a>
			</li>
		</ol>
			<button id="<?php echo $objects->settings_id?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="konten" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Back</button>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
		<input id="category_id" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->category_id?>">
		<input id="cat_id" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->category_id?>">
		<input id="settings_id" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->settings_id?>">
		<div class="col-sm-12 col-xs-12">	
			<div class="form-horizontal">				
				<div class="form-group">
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important">Transaction Type</label>
					<div class="col-sm-6 col-xs-8 ">
						<input id="title" name="inputan" type="text" class="form-control" value="<?php echo $objects->title?>" disabled>
					</div>
					<label class="col-sm-1 col-xs-4 control-label" style="text-align: left !important">Code</label>
					<div class="col-sm-3 col-xs-8 ">
						<input id="rule_1" name="inputan" type="text" class="form-control" value="<?php echo $objects->rule_1?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-xs-12 control-label" style="text-align: left !important">Description</label>
					<div class="col-sm-10 col-xs-12 " ><textarea id="description" name="inputan" type="text" class="form-control" value=""><?php echo $objects->description?></textarea></div>
				</div>
				<div class="form-group" >
					<label class="col-sm-2 col-xs-4 control-label" style="text-align: left !important; ">Publish</label>
					<div class="col-sm-4 col-xs-8" >
						<div class="input-group col-sm-12 col-xs-12 " >
							<select class="form-control m-b" name="inputan" id="status">
								<option value="">Select Status</option>
								<?php foreach($status as $s){ if($s->value != 2){ ;?>
								<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
								<?php };}?>
							</select>
						</div>
					</div>
				</div>						
				<div class="space-25"></div>
					
				<div class="form-group">			
					<button id="<?php echo $key_link;?>" data-param="settings" data-param2="settings" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Update</button>		
					<button id="<?php echo $objects->settings_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-url3="konten" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Cancel</button>
					
				</div>
			</div>
		</div>
	</div>	
</div>		
<script>
	$('#status').val('<?php echo $objects->status;?>');
</script>
