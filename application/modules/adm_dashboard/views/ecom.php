<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<span class="label label-info pull-right">Annual</span>
				<h5>Income</h5>
			</div>
			<div class="ibox-content">
				<h1 class="no-margins"><?php echo number_format($income[0]->totalprice,2,',','.');?></h1>
				<!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
				<small>Total income in IDR</small>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<span class="label label-info pull-right">Annual</span>
				<h5>Orders</h5>
			</div>
			<div class="ibox-content">
				<h1 class="no-margins"><?php echo $trx;?></h1>
				<!--<div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>-->
				<small>Total orders</small>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<!--<span class="label label-primary pull-right">Today</span>-->
				<h5>Product</h5>
			</div>
			<div class="ibox-content">
				<h1 class="no-margins"><?php echo $product;?></h1>
				<!--<div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>-->
				<small>Total product</small>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<!--<span class="label label-danger pull-right">Low value</span>-->
				<h5>Total Member</h5>
			</div>
			<div class="ibox-content">
				<h1 class="no-margins"><?php echo $member;?></h1>
				<!--<div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>-->
				<small>In first month</small>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Orders</h5>
				<div class="pull-right">
					<div id="button_active" class="btn-group">
						<button data-time="today" data-part="trx" type="button" class="dashboard btn btn-xs btn-white active">Today</button>
						<button data-time="monthly" data-part="trx" type="button" class="dashboard btn btn-xs btn-white">Monthly</button>
						<button data-time="annualy" data-part="trx" type="button" class="dashboard btn btn-xs btn-white">Annual</button>
					</div>
				</div>
			</div>
			<div class="ibox-content">
				<div class="row">
				<div class="col-lg-9">
					<div class="flot-chart">
						<div class="flot-chart-content" id="flot-dashboard-chart"></div>
					</div>
				</div>
				<div class="col-lg-3">
					<ul class="stat-list">
						<li>
							<h2 class="no-margins"><?php echo $inv_this_m;?></h2>
							<small>Total orders in this month</small>
							<div class="stat-percent"><?php echo $inv_this_m_paid;?> paid</div>
							<div class="progress progress-mini">
								<?php if($inv_this_m == 0){
									$o_t_m = 0;
								}else{
									$o_t_m = ($inv_this_m_paid/$inv_this_m)*100;
								};?>
								<div style="width: <?php echo $o_t_m;?>%;" class="progress-bar"></div>
							</div>
						</li>
						<li>
							<h2 class="no-margins "><?php echo $inv_last_m;?></h2>
							<small>Orders in last month</small>
							<div class="stat-percent"><?php echo $inv_last_m_paid;?> paid</div>
							<div class="progress progress-mini">
								<?php if($inv_last_m == 0){
									$o_l_m = 0;
								}else{
									$o_l_m = ($inv_last_m_paid/$inv_last_m)*100;
								};?>
								<div style="width: <?php echo $o_l_m;?>%;" class="progress-bar"></div>
							</div>
						</li>
						<li>
							<h2 class="no-margins ">IDR <?php echo number_format($income_this_m[0]->totalprice,2,',','.');?></h2>
							<small>Monthly income from orders</small>
							<!--<div class="stat-percent">22% <i class="fa fa-bolt text-navy"></i></div>
							<div class="progress progress-mini">
								<div style="width: 22%;" class="progress-bar"></div>
							</div>-->
						</li>
						</ul>
					</div>
				</div>
				</div>

		</div>
	</div>
</div>
<?php if(isset($ga_map) && !empty($ga_map)){;?>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Report Analytics</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-lg-6">
						<div id="ga_curve">
						
						</div>
					</div>
					<div class="col-lg-3">
						<div id="ga_pie_1">
						
						</div>
					</div>
					<div class="col-lg-3">
						<div id="ga_pie_2">
						
						</div>
					</div>
					<div class="col-lg-9">
						<div id="ga_map">
						
						</div>
					</div>
					<div class="col-lg-3">
						<div id="ga_visit">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type='text/javascript'>
  google.charts.load('current', {'packages':['geochart','corechart','table']});
  google.charts.setOnLoadCallback(drawRegionsMap);
  function drawRegionsMap() {

	var data = google.visualization.arrayToDataTable([<?php echo $ga_map;?>]);

	var options = {};

	var chart = new google.visualization.GeoChart(document.getElementById('ga_map'));

	chart.draw(data, options);
  }
  
  google.charts.setOnLoadCallback(drawChart1);

  function drawChart1() {
	var data = google.visualization.arrayToDataTable([
	  ['Date', 'Visitor'],
	  <?php echo $ga_time;?>
	]);

	var options = {
	  title: '<?php echo date('F');?>',
	  curveType: 'function',
	  legend: { position: 'bottom' }
	};

	var chart = new google.visualization.LineChart(document.getElementById('ga_curve'));

	chart.draw(data, options);
  }
   google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          <?php echo $ga_device;?>
        ]);

        var options = {
          title: 'Devices',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('ga_pie_1'));
        chart.draw(data, options);
      }
   google.charts.setOnLoadCallback(drawChart2);
      function drawChart2() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          <?php echo $ga_gender;?>
        ]);

        var options = {
          title: 'Gender',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('ga_pie_2'));
        chart.draw(data, options);
      }
  google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Link');
        data.addColumn('number', 'Visited');
        data.addRows([
          <?php echo $ga_visit;?>
        ]);

        var table = new google.visualization.Table(document.getElementById('ga_visit'));

        table.draw(data, {showRowNumber: true, width: '100%', height: '350px'});
      }
</script>

<?php }elseif(isset($g_analytics)){;?>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Report Analytics</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					<?php if($g_analytics['login']){;?>
					<div class="col-lg-12">
						Please Login First: <?php echo $g_analytics['login'];?>
					</div>
					<?php }else{;?>
					<div class="col-lg-12">
						Be patient, propagation take 24 hours.
					</div>
					<?php };?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php };?>
<script>
var newwindow;
	function poptastic(url)
	{
		newwindow=window.open(url,'name','height=400,width=500');
		if (window.focus) {newwindow.focus()}
	}
	
document.addEventListener("DOMContentLoaded", function(event) {
	
	var max_x;
	var min_x;
	var range;

	function get_data_stats(param, param2, time, part)
	{	
		var data = {
				gdctk_rand : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				param : param,
				param2 : param2,
				time : time
			};
		var url = "<?php echo base_url($zone);?>/admin/dashboard/view_detail/"+part;
		$.ajax({
			url : url,
			type: "POST",
			dataType: 'json',
			data: data,
			success: function(result){
				var dorder = [];
				var dpayment = [];
				$.each(result.data, function(key, val){
					dorder.push([val.time, val.total_order]);
					dpayment.push([val.time, val.payment]);
				});

				switch (result.time)
				{
					case 'today' :
						var max_x = 24; min_x = 1; 
						break;
					case 'monthly' :
						var max_x = 31; min_x = 1;
						break;
					case 'annualy' :
						var max_x = 12; min_x = 1;
						break;	
				}
				range = max_x + min_x;
				//console.log(dorder, dpayment);

				var dataset = [
				{
					label: 'Number of orders',
					data: dorder,
					color: '#1ab394',
					bars: {
						show: true,
						align: 'center',
						lineWidth:0
					}

				}, {
					label: 'Payments',
					data: dpayment,
					yaxis: 2,
					color: '#464f88',
					lines: {
						lineWidth:1,
							show: true,
							fill: true,
						fillColor: {
							colors: [{
								opacity: 0.2
							}, {
								opacity: 0.2
							}]
						}
					},
					splines: {
						show: false,
						tension: 0.6,
						lineWidth: 1,
						fill: 0.1
					},
				}
				];


				var options = {
					xaxis: {
						min: min_x,
						max: max_x,
						ticks: range,
						tickDecimals: 0,
						axisLabel: 'Date',
						axisLabelUseCanvas: true,
						axisLabelFontSizePixels: 12,
						axisLabelFontFamily: 'Arial',
						axisLabelPadding: 10,
						color: '#d5d5d5'
					},
					yaxes: [{
						min: 0,
						tickDecimals: 0,
						position: 'left',
						color: '#d5d5d5',
						axisLabelUseCanvas: true,
						axisLabelFontSizePixels: 12,
						axisLabelFontFamily: 'Arial',
						axisLabelPadding: 3
					}, {
						position: 'right',
						color: '#d5d5d5',
						axisLabelUseCanvas: true,
						axisLabelFontSizePixels: 12,
						axisLabelFontFamily: ' Arial',
						axisLabelPadding: 67
					}
					],
					legend: {
						noColumns: 1,
						labelBoxBorderColor: '#000000',
						position: 'nw'
					},
					grid: {
						hoverable: true,
						borderWidth: 0
					}
				};

				$.plot($('#flot-dashboard-chart'), dataset, options);
				$("<div id='tooltip'></div>").css({
					position: "absolute",
					display: "none",
					border: "1px solid #000",
					padding: "2px",
					"background-color": "#fff",
					opacity: 0.80
				}).appendTo("body");

				$("#flot-dashboard-chart").bind("plothover", function (event, pos, item) {
					if (item) {
						var x = item.datapoint[0];
							y = item.datapoint[1];

						$("#tooltip").html(item.series.label + " of " + x + " : " + y)
							.css({top: item.pageY+5, left: item.pageX+5})
							.fadeIn(200);
					}
				});
			}
		});
	}

	get_data_stats('', '', 'today', 'trx');
	$('.dashboard').on("click", function(e) {
		e.preventDefault();
		var part = $(this).data('part');
		var param = $(this).data('param');
		var param2 = $(this).data('param2');
		var time = $(this).data('time');
		var data = {
			gdctk_rand : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			param : param,
			param2 : param2,
			time : time
		};
		get_data_stats(param, param2, time, part);
		
		$("#button_active > button").removeClass("active");
		$(this).addClass("active");
	});
});
</script>