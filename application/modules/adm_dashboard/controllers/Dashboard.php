<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Dashboard extends Admin {
		
	public function index($zone)
	{	
		if ($this->s_login)
    	{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['is_mobile']	= $this->is_mobile();
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['notif']		= $this->notif;
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			$data['child'] = '';
			
			//End set parameter view
			$data['custom_module'] = $custom_module;
			$data['content'] = 'dashboard.php';
			
			$data['menu_active'] = 'dashboard';					
			$data['css'] = '';
			$data['js'] = '';
			$data['parent'] = 'dashboard';
			$data['ch'] = array(
				'title'=>'Dashboard', 
				'small'=>"Here's a brief information about your website."
			);
			if($this->is_active_domain === TRUE){
				$url = 'admin/dashboard';
			}else{
				$url = $this->zone.'/admin/dashboard';
			}
			$data['breadcrumb'] = array(
				array(
					'url' => $url, 
					'bcrumb' => 'Dashboard', 
					'active' => ''
				)
			);
			
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$part){	
		if ($this->s_login && !empty($part))
		{
			$data = array();
			$id = $this->input->post('id');
			$view = '';
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			if($custom_module == 3){
				$chiave = $this->input->post('time');
				
				if($chiave == 'today'){
					$select = "HOUR(datecreated) as time, count(trx_code) as total_order, sum(price) as payment";
					//$key = "AND DATE(datecreated) = ".date('Y-m-d')." group by HOUR(datecreated)";
					$key = "AND DATE(datecreated) = '".date('Y-m-d')."' group by HOUR(datecreated)";
				}elseif($chiave == 'monthly'){
					$select = "DAY(datecreated) as time, count(trx_code) as total_order, sum(price) as payment";
					$key = "AND YEAR(datecreated) = ".date('Y')." AND MONTH(datecreated) = ".date('m')." group by DAY(datecreated) ";
				}elseif($chiave == 'annualy'){
					$select = "MONTH(datecreated) as time, count(trx_code) as total_order, sum(price) as payment";
					$key = "AND YEAR(datecreated) = ".date('Y')." group by MONTH(datecreated)";
				}
				
				if($part == 'product'){
					$product = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$this->zone_id.' and link = "product"','mdl')->module_id;					
					$datas = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$product.' '.$key.'','odc'));	
				}elseif(in_array($part,array('trx','income'),true)){
					$trx = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$this->zone_id.' and link = "transactions"','mdl')->module_id;
					if($part == 'trx'){
						$datas = $this->dbfun_model->get_all_data_bys(''.$select.'','ccs_id = '.$this->zone_id.' and ccs_key = '.$trx.' '.$key.' ','trx');
					}elseif($part == 'income'){
						$datas = $this->dbfun_model->get_all_data_bys('sum(price) as totalprice','ccs_id = '.$this->zone_id.' and ccs_key = '.$trx.' and status = 1 '.$key.'','trx');	
					}
				}elseif($part == 'member'){
					$datas = count($this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$this->zone_id.' '.$key.'','usr'));
				}
				if(!empty($chiave)){
					echo json_encode(array('type' => $part,'data' => $datas,'time'=>$chiave));
				}else{
					echo json_encode(array('type' => $part,'data' => $datas));
				}
			}else{
				if($part == 'map_detail'){
					$data['ds'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.image_square, ccs_olc.latitude, ccs_olc.longitude','ccs_o.object_id = '.$id.' and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = 9 and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = 103 and ccs_o.object_id = ccs_odc.object_id and ccs_olc.ccs_id = ccs_odc.ccs_id and ccs_olc.ccs_key = ccs_odc.ccs_key and ccs_olc.object_id = ccs_odc.object_id','ccs_odc, ccs_o, ccs_olc');
					$data['status'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 9 and ccs_key = 1','sts');
					$gal = $this->dbfun_model->get_all_data_bys('obr_id','ccs_id = 9 and ccs_key = 105 and utl_id = '.$id.'','obr');
					$i = count($gal);
					$data['gal'] = $i; 
					if($i != 0){
						$data['isp'] = $this->dbfun_model->get_data_bys('ccs_olg.datecreated, ccs_olg.log_id, ccs_obr.metakeyword, ccs_olg.description, ccs_usr.name','ccs_obr.utl_id = '.$id.' and ccs_obr.obr_id = ccs_olg.url_id and ccs_obr.ccs_id = ccs_olg.ccs_id and ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_id = 9 and ccs_obr.ccs_key = ccs_olg.ccs_key and ccs_obr.ccs_key = 105 and ccs_olg.createdby = ccs_usr.user_id and ccs_olg.parent_id = 0 order by ccs_olg.datecreated desc','obr, olg, usr');
						$isp = $data['isp'];
						$log = $isp->log_id;
						$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id, title, image_square','ccs_id = 9 and ccs_key = 105 and object_id = '.$log.' and category_id = '.$id.' limit 4','ogl');
					}
					
					$view = $this->load->view('map_detail', $data, TRUE);
				}elseif($part == 'list'){				
					$inspeksi = $this->dbfun_model->get_data_bys('module_id','ccs_id = 9 and name = "Inspeksi"','mdl')->module_id;
					$jadwal = $this->dbfun_model->get_data_bys('module_id','ccs_id = 9 and name = "Penjadwalan"','mdl')->module_id;
					$data['status'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 9 and ccs_key = 2','sts');			
					$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_obr.*, ccs_usr.name, ccs_oct.title as tahun, ccs_odc.category_id as c_id','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.object_id = ccs_obr.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_key = ccs_odc.ccs_key order by ccs_obr.datestart desc limit 7','obr, usr, odc, oct');
										
					$view = $this->load->view('map_list', $data, TRUE);
				}
				echo $view;
			}
		}
	}
	
	private function get_map($poi, $zoom, $center){
		$data = array();
		$aww = null;
		if($poi && is_array($poi)){
			foreach($poi as $val){
				if((substr_count($val->latitude,'.') == 1) && (substr_count($val->longitude,'.') == 1)){
					$aww[] = ('[\'aa\','.$val->latitude.','.$val->longitude.','.$val->status.','.$val->object_id.',"'.$val->metakeyword.'"],');
				}else{
					$posisiX= trim($val->latitude);
					$posisiY= trim($val->longitude);
					if ($dot=strrpos($posisiY,".")) {
						$posisiY=trim(substr($posisiY,0,$dot)).substr(str_replace('.','',$posisiY),$dot);
					}
					if ($dot=strrpos($posisiX,".")) {
						$posisiX=trim(substr($posisiX,0,$dot)).substr(str_replace('.','',$posisiX),$dot);
					}
					$aww[] = ('[\'aa\','.$posisiY.','.$posisiX.','.$val->status.','.$val->object_id.',"'.$val->metakeyword.'"],');
				}
			}
			$data['peta_pointer'] = $aww;
		}else{
			$data['peta_pointer'] = '';
		}
		$data['zone'] = $this->zone;
		$data['zoom'] = $zoom;
		$data['center'] = $center;
		$view = $this->load->view('map_init', $data, TRUE);
		echo $view;
	}
	
	public function pointer($zone){
		$zoom = '13';
		$center = '-6.920867, 107.609558';
		$anchor = $this->input->post('cat');
		if (empty($anchor)){
			$poi = $this->dbfun_model->get_all_data_bys('ccs_odc.object_id,ccs_odc.status,ccs_odc.metakeyword,ccs_odc.status,ccs_olc.latitude, ccs_olc.longitude','ccs_o.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = 9 and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = 103 and ccs_o.object_id = ccs_odc.object_id and ccs_olc.ccs_id = ccs_odc.ccs_id and ccs_olc.ccs_key = ccs_odc.ccs_key and ccs_olc.object_id = ccs_odc.object_id','ccs_odc, ccs_o, ccs_olc');
			$this->get_map($poi, $zoom, $center);
		}else{
			$poi = '';
			$this->get_map($poi, $zoom, $center);
		}
	}
}