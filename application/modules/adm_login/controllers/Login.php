<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class login extends Admin {
		
	public function index($zone)
	{	
		$cek = $this->dbfun_model->get_data_bys('name','LOWER(name) like "'.strtolower($zone).'"','zone');
		if($cek){
			if (!$this->s_login)
			{
				$data = array();
				$data['footer'] = array(
									'provided' => $this->provided,
									'copyright' => $cek->name
								);
				$data['zone'] = $zone;				
				$data['title'] = ''.$cek->name.' | Login Page';
				$data['login_url'] = ''.strtolower($cek->name).'/admin/checking-account';
				$this->load->view('login', $data);
				//$this->output->cache(10);
			}
			else
			{
				redirect($this->dashboard);
			}
		}else{
			show_404();
		}
	}

	public function login_check($zone)
	{	
		if (!$this->s_login)
    	{
    		$status = '';
    		$message = '';

    		$this->form_validation->set_rules('u', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('p', 'Password', 'trim|required|xss_clean');

			if ($this->form_validation->run() === TRUE)
			{
				$username = trim($this->input->post('u'));
				$id = $this->dbfun_model->get_data_bys('ccs_id','LOWER(name) like "'.strtolower($zone).'"','zone')->ccs_id;
				$password = $this->input->post('p');
				
				$login_data = $this->dbfun_model->get_data_bys('*', array('username' => $username, 'ccs_id'=> $id), 'adm');
				$data['pass'] = MD5($password);
				$cek_user_is_exist = $this->dbfun_model->get_data_bys('status', array('username' => $username,'password' => $data['pass'], 'ccs_id'=> $id), 'adm');
				
				if ($login_data)
				{
					if($cek_user_is_exist){
					/* if (($login_data->first_log ==='0') || ($login_data->first_log ==='1'))
					{ */
						$type = $this->dbfun_model->get_data_bys('name', array('type_id' => $login_data->role_id, 'ccs_id' => $login_data->ccs_id), 'mdr');
						
						if($cek_user_is_exist->status === '1')
						{ 
							$foto = '';
							if ($login_data->main_image)
							{
								$foto = 'assets/'.$zone.'/admin/'.$login_data->admin_id.'/'.$login_data->main_image;
							}
							else
							{
								$foto = 'assets/admin/admin_empty.jpg';
							}
							$session = array(
								strtolower($zone).'_username' => $login_data->username,
								strtolower($zone).'_nama' => $login_data->name,
								strtolower($zone).'_type' => $type->name,
								strtolower($zone).'_zone' => strtolower($zone),
								strtolower($zone).'_role' => $login_data->role_id,
								strtolower($zone).'_admin_id' => $login_data->admin_id,
								strtolower($zone).'_status' => $login_data->status,
								strtolower($zone).'_foto' => $foto,
								strtolower($zone).'_logged_in' => TRUE
							);
							$this->session->set_userdata($session);
							$this->log_user($login_data->admin_id, 0, 1, 1);
							$status= 'success';
							$message = '<b>Login berhasil!</b> mohon tunggu ...';
							
						}
						elseif(($cek_user_is_exist->status === '2') || ($cek_user_is_exist->status === '0'))
						{
							$status = 'error';
							$message = 'Maaf! Akun Anda sudah tidak aktif';	
						}
						elseif($cek_user_is_exist->status === '3')
						{
							$status = 'error';
							$message = 'Maaf! Akun Anda sudah tidak aktif';	
						}
						else
						{
							$status = 'error';
							$message = 'Maaf! Password yang Anda masukan salah';	
						}	
					/*} 
					 else
					{
						$status = 'error';
						$message = 'Maaf! Anda Telah Login pada Perangkat yang Berbeda, Harap lakukan Log Out Terlebih Dahulu';
					} */ 
					}else{
						$status = 'error';
						$message = 'Maaf! Password yang Anda masukan salah';
					}
				}
				else
				{
					$status = 'error';
					$message = 'Maaf! Akun dengan username tersebut tidak ada';
				}
			}
			else
			{
				$status = 'error';
				$message = validation_errors();
			}

			if ($message)
			{
				echo json_encode(array('status' => $status, 'm' => $message));
			}
			else
			{
				redirect($this->login);
			}
    	}
    	else
    	{
    		redirect($this->dashboard);
    	}
	}

	public function logout($zone){
		$cek = $this->dbfun_model->get_data_bys('name','LOWER(name) like "'.strtolower($zone).'"','zone');
		if($cek){
			if ($this->s_login)
			{
				$this->log_user($this->admin_id, 0, 0, 1);
				$session = array(
						strtolower($zone).'_username',
						strtolower($zone).'_nama',
						strtolower($zone).'_admin_id',
						strtolower($zone).'_type',
						strtolower($zone).'_zone',
						strtolower($zone).'_role',
						strtolower($zone).'_status',
						strtolower($zone).'_foto',
						strtolower($zone).'_logged_in'
				); 
				$this->session->unset_userdata($session);
				redirect($this->login);
			}
			else
			{
				redirect($this->login);
			}
		}else{
			show_404();
		}
	}
	
	public function upload_summernote($zone){
		if($this->s_login){
			$allowed = array('png', 'jpg', 'jpeg', 'gif','zip');
			if(isset($_FILES['file']) && $_FILES['file']['error'] == 0){

				$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

				if(!in_array(strtolower($extension), $allowed)){
					echo '{"status":"error"}';
					exit;
				}
				if (!is_dir('assets/'.$this->zone.'/content'))
				{
					mkdir('./assets/'.$this->zone.'/content', 0777, true);
				}
				$dir_name = './assets/'.$this->zone.'/content/';
				if(move_uploaded_file($_FILES['file']['tmp_name'],
				$dir_name.$_FILES['file']['name'])){
					$tmp=$dir_name.$_FILES['file']['name'];
					echo base_url('assets/'.$this->zone.'/content').'/'.$_FILES['file']['name'];
					//echo '{"status":"success"}';
					exit;
				}
			}

			echo '{"status":"error"}';
			exit;
		}
	}

	private function _upload_files($path, $type, $uploadfiles)
	{
		$this->load->library('upload');
		$this->load->library('image_lib'); 

		$config['upload_path'] = $path;
		$config['allowed_types'] = $type;
		$config['max_size']	= '20480';	
		$config['overwrite'] = FALSE;

		$config_thumb['image_library'] = 'gd2';
		$config_thumb['create_thumb'] = FALSE;
		$config_thumb['maintain_ratio'] = TRUE;
		$config_thumb['width']	= 200;
		$config_thumb['height']	= 200;

		$path_thumb = $path.'/thumbnail';
		if (!is_dir($path_thumb))
		{
			mkdir($path_thumb, 0777, true);
		}

		$files = array();

        foreach ($uploadfiles['name'] as $key => $file) {
            $_FILES['file[]']['name']= $uploadfiles['name'][$key];
            $_FILES['file[]']['type']= $uploadfiles['type'][$key];
            $_FILES['file[]']['tmp_name']= $uploadfiles['tmp_name'][$key];
            $_FILES['file[]']['error']= $uploadfiles['error'][$key];
            $_FILES['file[]']['size']= $uploadfiles['size'][$key];

            $config['file_name'] = $file;
           

            $this->upload->initialize($config);
			if($_FILES['file[]']['type'] != 'mp4'){
				if ($this->upload->do_upload('file[]')) 
				{
					$upload_file = $this->upload->data();
					$files[] = $upload_file['file_name'];
					$config_thumb['source_image'] = $upload_file['full_path'];
					$config_thumb['new_image'] = $path_thumb.'/'.$upload_file['file_name'];
					$this->image_lib->initialize($config_thumb);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}
			}else{
				$this->upload->do_upload('file[]');
				$upload_file = $this->upload->data();
			}
            
        }

		return $files;
	}
	
	public function upload_gallery($zone,$object_id,$cat_id,$ccs_key){
		if($this->s_login){

			$this->form_validation->set_rules('title[]', 'Title', 'trim|max_length[255]');
			$this->form_validation->set_rules('desc[]', 'Description', 'trim|max_length[255]');

			$status = 'error';
			$message = 'Maaf! Terdapat kesalahan pada data yang Anda kirimkan';

			if ($this->form_validation->run() === TRUE)
			{
				$desc = $this->input->post('desc');
				$title = $this->input->post('title');

				$data_upload = array();

				foreach ($title as $key => $val) {
					$data_upload[] = array('title' => $val, 'desc' => $desc[$key]);
				}

				$allowed_types = 'jpg|jpeg|png|mp4';
				$path = './assets/'.$this->zone.'/gallery';
				$path_cek = './assets/'.$this->zone.'/gallery';
				if (!is_dir($path_cek))
				{
					mkdir('./assets/'.$this->zone.'/gallery', 0777, true);
				}

				
				if (!empty($_FILES['file']['name'][0])) {
					$files = $this->_upload_files($path, $allowed_types, $_FILES['file']);
	                if ($files) {
	                	foreach ($files as $key => $file) {
	                		$data = array(
								'ccs_id' => $this->zone_id,
								'ccs_key' => $ccs_key,
								'category_id' => $cat_id,
								'object_id' => $object_id,
								'language_id' => 2,
								'createdby' => $this->admin_id,
								'image_square' => $file,
								'title' => $data_upload[$key]['title'],
								'description' => $data_upload[$key]['desc']
							);
							$this->dbfun_model->ins_to('ccs_ogl',$data);
	                	}
	                }
	            }    

	            $status = 'success';
	            $message = 'File berhasil disimpan!';

			}

			echo json_encode(array('status' => $status, 'm' => $message));
		}
	}
}