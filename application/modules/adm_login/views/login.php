<!DOCTYPE html>
<html>


<!-- Site: HackForums.Ru | E-mail: abuse@hackforums.ru | Skype: h2osancho -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $title ;?></title>

    <link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/admin/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="loginColumns animated fadeInDown">
		<div class="row">

			<div class="col-md-6">
				<h2 class="font-bold">Welcome to <span style="color: black; text-transform:uppercase;"><?php echo $footer['copyright'] ;?></span></h2>
				<br/>
				<p>
					Be Silent and let the success bring the noise.
				</p>
			</div>
			<div class="col-md-6">
				<div class="ibox-content">
					<form class="m-t" role="form" action="<?php echo $login_url;?>" id="login-form" method="post">
						<div class="alert alert-danger" id="fail" style="display:none;"></div>
						<div class="alert alert-info" id="success" style="display:none;"></div>
						<div class="form-group">
							<input id="username" type="text" class="form-control" placeholder="Username" required="">
						</div>
						<div class="form-group">
							<input id="password" type="password" class="form-control" placeholder="Password" required="">
						</div>
						<button id="login-button" type="submit" class="btn btn-primary block full-width m-b">Login</button>

						<a id="show-forget" href="#fp-form">
							<small>Forgot password?</small>
						</a>
					</form>
					<form class="m-t" role="form" action="<?php echo $login_url;?>" id="fp-form" method="post" style="display:none">
						<div class="alert alert-danger" id="fail" style="display:none;"></div>
						<div class="alert alert-info" id="success" style="display:none;"></div>
						<div class="form-group">
							<input id="username" type="text" class="form-control" placeholder="Username" required="">
						</div>
						<div class="form-group">
							<input id="email" type="email" class="form-control" placeholder="Email" required="">
						</div>
						<button id="login-button" type="submit" class="btn btn-primary block full-width m-b">Submit</button>

						<a id="show-login" href="#login-form">
							<small>Have an account?</small>
						</a>
					</form>
				</div>
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-md-6">
				Delivered to you by <strong style="text-transform:uppercase;"><?php echo $footer['copyright'] ;?></strong>
			</div>
			<div class="col-md-6 text-right">
			   <small>© <?php echo date("Y"); ?></small>
			</div>
		</div>
	</div>
	
	<script src="<?php echo base_url();?>assets/admin/js/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		function scrolltonote(selector){
			 var sel = selector
			 $('html, body').animate({
				scrollTop: $(sel).offset().top
			}, 1000);
		}
		$(document).ready(function(){
			var base_url = '<?php echo base_url();?>';
			$('#login-form').submit(function(event){ 
				event.preventDefault();  
				var login_url = $(this).attr('action');
				$('#login-button').attr('disabled','disabled');
				$('#reset').attr('disabled','disabled');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					u: $('#username').val(),
					p: $('#password').val()
				};
				$.ajax({
					type: "POST",
					url: base_url+login_url,
					data: data,
					cache: false,
					dataType: 'json',
					success: function(data){
						if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#success');
							$('#success').show();scrolltonote('.ibox-content');
							$('#success').fadeTo(3000, 500).slideUp(500);
							setTimeout(function(){location.reload(true);}, 4000);
						}else{
							$('<p>'+data.m+'</p>').appendTo('#fail');
							$('#login-button').removeAttr('disabled');
							$('#reset').removeAttr('disabled');
							$('#fail').show();
							scrolltonote('.ibox-content');
							$('#fail').fadeTo(3000, 500).slideUp(500);
						}
					},
					//error : function(jqXHR){
						//alert('Maaf! Terdapat kesalahan pada sistem'); location.reload(true);
					//}	
				});
				$('#fail').empty(); $('#success').empty();
			});
			
			$('#show-forget').on('click', function(event){
				event.preventDefault();
				var sh = $(this).attr('href');
				$(sh).slideDown();$('#login-form').slideUp();
			});
			 $('#show-login').on('click', function(event){
				event.preventDefault();
				var sh = $(this).attr('href');
				$(sh).slideDown();$('#fp-form').slideUp();
			});
			 $('#fp-form').submit(function(event){ 
				event.preventDefault();  
				var url = $(this).attr('action');
				$('#login-button').attr('disabled','disabled');
				$('#reset').attr('disabled','disabled');
				$('#login-button').attr('disabled','disabled');
				var data = {
					u: $('#username').val(),
					z: '<?php echo $zone;?>',
					e : $('#email').val(),
					tcbtptk: tcbtptk
				};
				$.ajax({
					type: "POST",
					url: url,
					data: data,
					cache: false,
					dataType: 'json',
					success: function(data){if (data.status == "success"){$(data.m).appendTo('#success-reset');$('#success-reset').show();scrolltonote('.ibox-content');
								  $('#success-reset').fadeTo(3000, 500).slideUp(500);setTimeout(function(){location.reload(true);}, 4000);}else{$('<p>'+data.m+'</p>').appendTo('#fail-reset');$('#login-button').removeAttr('disabled');$('#reset').removeAttr('disabled');$('#fail-reset').show();
								  scrolltonote('.ibox-content');$('#fail-reset').fadeTo(3000, 500).slideUp(500);}},
					error : function(jqXHR){alert('Maaf! Terdapat kesalahan pada sistem'); location.reload(true);}	
				});
				$('#fail-reset').empty(); $('#success-reset').empty();
			});
		});
	</script>
</body>

</html>