<style>
	.ibox { margin: 1px 2px 0px 0px !important }
	.ibox.float-e-margins{ margin: 0px 2px !important}
</style>
<script>
	$(document).ready(function(){
		$('#nestable2').nestable();
	});
</script>
<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<h5>Calendar Module </h5>
	</div>
	<div class="ibox-content row">
		<div class="row" style="margin: 0px;">
			<a id="1067" data-url="module" data-url2="budget" data-param="new_category" data-lang="2" class="detail2 btn btn-sm btn-primary col-md-6 col-xs-12">Add Category</a>
			<a id="1067" data-url="module" data-url2="budget" data-lang="2" data-param="view_category" class="detail2 btn btn-sm btn-success col-md-6 col-xs-12">View Category</a>
		</div>	
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="all" data-url="module" data-url2="budget" data-param="list" data-lang="2" class="detail2">
								<span class="label label-info"></span> <strong>View All Category</strong>
							</a>
							<p class="pull-right">(0)</p> 
						</div>
					</li>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<a id="0" data-url="module" data-url2="budget" data-lang="2" data-param="list" class="detail2">
								<span class="label label-info"></span> uncategory
							</a>
							<p class="pull-right">(0)</p> 
						</div>
					</li>
				</ol>
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
</div>