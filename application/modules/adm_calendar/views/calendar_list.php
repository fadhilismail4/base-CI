<style>
	.ibox { margin: 1px 2px 0px 0px !important }
	.ibox.float-e-margins{ margin: 0px 2px !important}
</style>
<link href="<?php echo base_url('assets');?>/admin/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">

<div class="ibox">
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<div id="calendar"></div>

		<div class="clearfix"></div>
	</div>
</div>

<script src="<?php echo base_url('assets');?>/admin/js/plugins/fullcalendar/moment.min.js"></script>
<script src="<?php echo base_url('assets');?>/admin/js/plugins/fullcalendar/fullcalendar.min.js"></script>
<script>
var detailCalendar = function (id) {
	var url = "<?php echo $menu[0]['link'];?>/calendar/view_detail/detail";
	var data = {id: id};
	$.ajax({
		url : url,
		type: "POST",
		dataType: 'html',
		data: data,
		success: function(html){
			$('.detail_content2').empty();
			$('.detail_content2').append(html);
			view_btn_find_image = '<button id="gal" class="find_image btn btn-md btn-success col-md-12" style="margin-top: 10px;">Pick From File Manager</button>';
			$(view_btn_find_image).insertAfter('span.file-input');
			$('.btn').each(function(){$(this).removeAttr('disabled');});
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
}
$('#external-events div.external-event').each(function() {
	/* $(this).data('event', {
		title: $.trim($(this).text()), 
		stick: true 
	});

	$(this).draggable({
		zIndex: 1111999,
		revert: true,      
		revertDuration: 0  
	}); */
});

/* initialize the calendar
 -----------------------------------------------------------------*/
var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();

$('#calendar').fullCalendar({
	eventClick: function(calEvent, jsEvent, view) {
		var id = calEvent.id;
		detailCalendar(id);
	},
	header: {
		left: 'prev,next today',
		center: 'title',
		right: 'month,agendaWeek,agendaDay'
	},
	editable: false,
	droppable: false,
	drop: function() {
		if ($('#drop-remove').is(':checked')) {
			$(this).remove();
		}
	},
	events: [
		{
			title: 'All Day Event',
			start: new Date(y, m, 1)
		},
		{
			title: 'Long Event',
			start: new Date(y, m, d-5),
			end: new Date(y, m, d-2),
		},
		{
			id: 999,
			title: 'Repeating Event',
			start: new Date(y, m, d-3, 16, 0),
			allDay: false,
		},
		{
			id: 999,
			title: 'Repeating Event',
			start: new Date(y, m, d+4, 16, 0),
			allDay: false
		},
		{
			title: 'Meeting',
			start: new Date(y, m, d, 10, 30),
			allDay: false
		},
		{
			title: 'Lunch',
			start: new Date(y, m, d, 12, 0),
			end: new Date(y, m, d, 14, 0),
			allDay: false
		},
		{
			title: 'Birthday Party',
			start: new Date(y, m, d+1, 19, 0),
			end: new Date(y, m, d+1, 22, 30),
			allDay: false
		},
		{
			title: 'Click for Google',
			start: new Date(y, m, 28),
			end: new Date(y, m, 29),
			id: 10
		}
	],
});
</script>