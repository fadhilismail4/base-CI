<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Calendar extends Admin {
		
	public function view ($zone)
	{	
		if ($this->s_login)
    	{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['is_mobile']	= $this->is_mobile();
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['notif']		= $this->notif;
			$data['child'] 		= '';
			
			//End set parameter view
			$data['content'] = 'calendar.php';
			
			$data['menu_active'] = 'calendar';
			$data['css'] = '';
			$data['js'] = array(
				'js/plugins/nestable/jquery.nestable.js'
			);
			$data['parent'] = 'calendar';
			$data['ch'] = array(
				'title'=> 'Calendar', 
				'small'=> "Here's a about calendar."
			);

			if($this->is_active_domain === TRUE){
				$url = 'admin/calendar';
			}else{
				$url = $this->zone.'/admin/calendar/1';
			}

			$data['breadcrumb'] = array(
				array(
					'url' => $url, 
					'bcrumb' => 'Calendar', 
					'active' => true
				)
			);

			$data['custom_js'] = '<script>
				$(document).ready(function(){
					$("[data-param='. "'" .'list'. "'" .']")[0].click();
				});
			</script>';	
			
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$part){	
		(!$this->s_login) ? redirect($this->login) : '';

		$data = array();
		$data['menu'] = $this->menu;
		$id = $this->input->post('id');
		$view = '';

		if ($part == 'category') {
			$view = $this->load->view('calendar_category.php', $data, TRUE);
		}

		if ($part == 'list') {
			$view = $this->load->view('calendar_list.php', $data, TRUE);
		}

		if ($part == 'detail') {
			$data['detail'] = $this->get_o($id);
			$view = $this->load->view('calendar_detail.php', $data, TRUE);
		}

		echo $view;
	}
}