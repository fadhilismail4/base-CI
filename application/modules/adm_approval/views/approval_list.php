<style>
	.ibox { margin: 1px 2px 0px 0px !important }
	.ibox.float-e-margins{ margin: 0px 2px !important}
</style>

<div class="ibox">
	<div class="ibox-title row" style="border-width: 1px 0px 0;">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px; padding-left: 5px;">
			<li class="active">
				List Approval
			</li>
		</ol>
		<div class="ibox-tools col-md-5">
			<div class="input-group">
				<input type="text" placeholder="Search" class="input-sm form-control"> 
				<span class="input-group-btn">
					<button type="button" class="btn btn-sm btn-primary"> Search</button>
				</span>
			</div>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Event Name</th>
					<th>Event Date</th>
					<th>Status</th>
					<th>Budget</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>VI021</td>
					<td>Golf Tournament</td>
					<td>17/08/2017</td>
					<td><span class="text-default">TO BE REVIEWED</span></td>
					<td>Rp. <span class="pull-right">125.000,00</span></td>
					<td>
						<button class="btn btn-warning btn-xs">Detail</button>
						<button class="btn btn-primary btn-xs">Edit</button>
						<button class="btn btn-danger btn-xs">X</button>
					</td>
				</tr>
				<tr>
					<td>VI022</td>
					<td>Golf Tournament II</td>
					<td>22/08/2017</td>
					<td><span class="text-navy">APPROVED</span></td>
					<td>Rp. <span class="pull-right">20.000,00</span></td>
					<td>
						<button class="btn btn-warning btn-xs">Detail</button>
						<button class="btn btn-primary btn-xs">Edit</button>
						<button class="btn btn-danger btn-xs">X</button>
					</td>
				</tr>
				<tr>
					<td>VI023</td>
					<td>Golf Tournament III</td>
					<td>26/08/2017</td>
					<td><span class="text-danger">NEED TO BE EDITED</span></td>
					<td>Rp. <span class="pull-right">125.000,00</span></td>
					<td>
						<button class="btn btn-warning btn-xs">Detail</button>
						<button class="btn btn-primary btn-xs">Edit</button>
						<button class="btn btn-danger btn-xs">X</button>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="6">
						<small>showing 3 from 33</small>
						<div class="pull-right">
							<div class="btn-group">
                                <button type="button" class="btn btn-white btn-sm"><i class="fa fa-chevron-left"></i></button>
                                <button class="btn btn-white btn-sm">1</button>
                                <button class="btn btn-white btn-sm">2</button>
                                <button class="btn btn-white btn-sm">3</button>
                                <button class="btn btn-white btn-sm active">4</button>
                                <button type="button" class="btn btn-white btn-sm"><i class="fa fa-chevron-right"></i> </button>
                            </div>
						</div>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>