<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Frontend extends Front {

	private function _is_post()
	{
		if ($this->input->server('REQUEST_METHOD') != 'POST')
		{
			show_404();
		}
	}
	
		
	public function index($zone,$module,$category,$title,$additional)
	{
		$cek = $this->dbfun_model->get_data_bys('name','LOWER(name) like "'.strtolower($zone).'" and status = 1','zone');
		if($cek){
			$this->load->helper('date_indonesia');
			$data = array();
			$data = $this->data;
			//Set parameter view
			$data['title'] 		= $this->seo()['title'];
			$data['metadescription'] = $this->seo()['metadescription'];
			$data['metakeyword'] = $this->seo()['metakeyword'];
			$data['metakeyword'] = $this->seo()['metakeyword'];
			$data['g_analytic']	= $this->g_analytics();
			if($this->is_mobile()){
				$data['is_mobile'] = TRUE;
			}
			else {
				$data['is_mobile'] = FALSE;
			}
			if($zone == 'shisha'){
				$data['is_verified'] = TRUE;
				$data['card_name'] = 'Shisha Club Member';
				$data['card_price'] = '250000';
			}else{
				$data['is_verified'] = FALSE;
			}
			
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['menu'] 		= $this->menu;
			$data['is_login']	= $this->s_login;
			$data['reg_id']		= $this->user_reg_id;
			if($this->is_active_domain){
				if($this->session->userdata($this->zone.'_admin_id')){
					$data['sign_up_url']  = '';
					$data['forgot_url']  = $this->forgot;
					$data['login_url']  = 'admin/login';
					$data['logout_url']  = 'admin/logout';
					$data['dashboard_url']  = 'admin/dashboard';
				}else{
					$data['sign_up_url']  = $this->sign_up;
					$data['forgot_url']  = $this->forgot;
					$data['login_url']  = $this->login;
					$data['logout_url']  = $this->logout;
					$data['dashboard_url']  = $this->dashboard;
				}
			}else{
				if($this->session->userdata($this->zone.'_admin_id')){
					$data['sign_up_url']  = '';
					$data['forgot_url']  = $this->forgot;
					$data['login_url']  = $this->zone.'/admin/login';
					$data['logout_url']  = $this->zone.'/admin/logout';
					$data['dashboard_url']  = $this->zone.'/admin/dashboard';
				}else{
					$data['sign_up_url']  = $this->sign_up;
					$data['forgot_url']  = $this->forgot;
					$data['login_url']  = $this->login;
					$data['logout_url']  = $this->logout;
					$data['dashboard_url']  = $this->dashboard;
				}
			}
			
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => ucfirst($this->copyright)
								); 
			$data['css']= array('slider-for-bootstrap/css/slider.css','../css/themes/yellow3.css');
			$data['js']= array('revo-slider/js/jquery.themepunch.tools.min.js','revo-slider/js/jquery.themepunch.revolution.min.js','cubeportfolio/js/jquery.cubeportfolio.min.js','counterup/jquery.waypoints.min.js','counterup/jquery.counterup.min.js','fancybox/jquery.fancybox.pack.js','slider-for-bootstrap/js/bootstrap-slider.js');
			$data['style_header'] = $this->style['header'];
			$data['style'] = $this->style['homepage'];
			$data['style_footer'] = $this->style['footer'];
			$data['zone_type'] = $this->zone_type_id;
			if($this->zone == 'moonshine'){
				if($module != ''){
					$data['style_header'] = array("topbar"=> "none","navbar"=> "dark","menu"=>"classic");
				}
				$data['skeleton_footer'] = array(
					/* 0 => (object)array(
										'column'=> 'col-md-3',
										'title'=> 'KB INDONESIA',
										'text'=> 'Authorized Distributor of Kyusoku Bihaku product in Indonesia.',
										'data' => '',
										'data_type'=>'link'
									),
					1 => (object)array(
										'column'=> 'col-md-3',
										'title'=> 'Our Agents',
										'text' => 'Jakarta, Bekasi, Bogor, Bandung, Jogja, Bondowoso, Balikpapan, Bali, Riau, Mataram',
										'data_module' => '',
										'data' => '',
										'data_type'=>'blog'
									), */
					0 => (object)array(
										'column'=> 'col-md-6',
										'title'=> 'FIND US',
										'text'=> '<div class="form-group"><div class="alert alert-danger" id="fail" style="display:none;"></div><div class="alert alert-info" id="success" style="display:none;"></div><input id="emails" name="inputan" type="text" class="col-md-8 form-group input-lg c-square col-md-12" placeholder="E-mail"><button url="create" url2="create" class="btn_post col-md-4 c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-md c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-sbold">subscribe</button></div>',
										'data' => '',
										'data_type'=>''
									),
				);
			}elseif($this->zone == 'shisha'){
				$limit = 'LIMIT 6';
				$offset = 'OFFSET 0';
				//$order = 'and ccs_o.image_square != "" and ccs_o.dateupdated >= CURDATE() ORDER BY ccs_o.dateupdated DESC';
				$order = 'and ccs_o.image_square != "" and YEAR(ccs_o.dateupdated) >= YEAR(CURDATE()) and MONTH(ccs_o.dateupdated) >= MONTH(CURDATE()) ORDER BY ccs_o.datecreated ASC';
				$m_footer = 'event';
				$d_footer = $this->get_all_object_from_module_with_order_by_limit_no_cat($m_footer,$this->current_language,$order,$limit,$offset);
				$data['data_custom_footer'] = array();
				foreach($d_footer as $d_f){
					$d_f_c = $this->category_id_to_category($this->module_to_ccs_key($m_footer),$d_f->category_id);
					$data['data_custom_footer'][] = (object)array_merge((array)$d_f,array('category'=>$d_f_c));
				}
				$limit2 = 'LIMIT 2';
				$offset2 = 'OFFSET 0';
				$order2 = 'and ccs_o.image_square != "" and ccs_odc.status = 1';
				$m_footer2 = 'testimonial';
				$d_footer2 = $this->get_all_object_from_module_with_order_by_limit_no_cat($m_footer2,$this->current_language,$order2,$limit2,$offset2);
				foreach($d_footer2 as $d_f2){
					$d_f_c2 = $this->category_id_to_category($this->module_to_ccs_key($m_footer),$d_f2->category_id);
					$data2[] = (object)array_merge((array)$d_f2,array('category'=>$d_f_c2));
				}
				$data['skeleton_footer'] = array(
						0 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][0]['column'],
											'title'=> $data['menu']['dfooter']['datas'][0]['title'],
											'text'=> $data['menu']['dfooter']['datas'][0]['text'],
											'data' => '',
											'data_type'=>$data['menu']['dfooter']['datas'][0]['data_type']
										),
						1 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][1]['column'],
											'title'=> $data['menu']['dfooter']['datas'][1]['title'],
											'text'=> $data['menu']['dfooter']['datas'][1]['text'],
											'data_module' => '',
											'data' => array(
													0 => (object) array(
															'img' => base_url('assets').'/'.$zone.'/testimonial/'.$data2[0]->image_square,
															'url' => $data2[0]->tagline,
															'title' => $data2[0]->title,
															'category' => $data2[0]->description
														),
													1 => (object) array(
															'img' => base_url('assets').'/'.$zone.'/testimonial/'.$data2[1]->image_square,
															'url' => $data2[1]->tagline,
															'title' => $data2[1]->title,
															'category' => $data2[1]->description
														),	
												),
											'data_type'=>$data['menu']['dfooter']['datas'][1]['data_type']
										),
						2 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][2]['column'],
											'title'=> $data['menu']['dfooter']['datas'][2]['title'],
											'text'=> $data['menu']['dfooter']['datas'][2]['text'],
											'data_module' => 'event',
											'data' => $data['data_custom_footer'],
											'data_type'=>$data['menu']['dfooter']['datas'][2]['data_type']
										),
						3 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][3]['column'],
											'title'=> $data['menu']['dfooter']['datas'][3]['title'],
											'text'=> $data['menu']['dfooter']['datas'][3]['text'],
											'data' => '',
											'data_type'=>$data['menu']['dfooter']['datas'][3]['data_type']
										),
					);	
			}elseif($this->zone == 'ilovekb'){
				$data['user_type'] = $this->module_to_all_category_w_desc('role');
				$limit = 'LIMIT 9';
				$offset = 'OFFSET 0';
				$order = 'and ccs_o.image_square != "" and ccs_odc.status = 1 ORDER BY RAND()';
				$order2 = 'and ccs_o.image_square != "" and ccs_odc.status = 1 ORDER BY ccs_odc.datecreated DESC';
				$m_footer = 'testimonial';
				$d_footer = $this->get_all_object_from_module_with_order_by_limit_no_cat($m_footer,$this->current_language,$order,$limit,$offset);
				foreach($d_footer as $d_f){
					$d_f_c = $this->category_id_to_category($this->module_to_ccs_key($m_footer),$d_f->category_id);
					$data['data_custom_footer'][] = (object)array_merge((array)$d_f,array('category'=>$d_f_c));
				}
				$d_testi = $this->get_all_object_from_module_with_order_by_limit_no_cat($m_footer,$this->current_language,$order2,$limit,$offset);
				foreach($d_testi as $d_testi){
					$d_f_c2 = $this->category_id_to_category($this->module_to_ccs_key($m_footer),$d_testi->category_id);
					$data['d_testi'][] = (object)array_merge((array)$d_testi,array('category'=>$d_f_c2));
				}
				$data['testimonial'] = $data['d_testi'];
				$data['skeleton_footer'] = array(
						0 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][0]['column'],
											'title'=> $data['menu']['dfooter']['datas'][0]['title'],
											'text'=> $data['menu']['dfooter']['datas'][0]['text'],
											'data' => '',
											'data_type'=> $data['menu']['dfooter']['datas'][0]['data_type'],
										),
						1 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][1]['column'],
											'title'=> $data['menu']['dfooter']['datas'][1]['title'],
											'text'=> $data['menu']['dfooter']['datas'][1]['text'],
											'data_module' => '',
											'data' => '',
											'data_type'=> $data['menu']['dfooter']['datas'][1]['data_type'],
										),
						2 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][2]['column'],
											'title'=> $data['menu']['dfooter']['datas'][2]['title'],
											'text'=> $data['menu']['dfooter']['datas'][2]['text'],
											'data_module' => 'testimonial',
											'data' => $data['data_custom_footer'],
											'data_type'=> $data['menu']['dfooter']['datas'][2]['data_type'],
										),
						3 => (object)array(
											'column'=> $data['menu']['dfooter']['datas'][3]['column'],
											'title'=> $data['menu']['dfooter']['datas'][3]['title'],
											'text'=> '<div class="form-group"><div class="alert alert-danger" id="fail" style="display:none;"></div><div class="alert alert-info" id="success" style="display:none;"></div><input id="emails" name="inputan" type="text" class="form-group input-lg c-square col-md-12" placeholder="E-mail"><button url="create" url2="create" class="btn_post c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-sbold">subscribe</button></div>',
											'data' => '',
											'data_type'=> $data['menu']['dfooter']['datas'][3]['data_type']
										),
					);	
			}
			switch($this->zone_type_id){
				case '3':
					$this->load->library('cart');
					$data['text']['cart'] = 'bag';
					$data['text']['add_cart'] = 'add to bag';
					$data['text']['view_cart'] = 'view bag';
					break;
			}
			
			if($module == ''){
				$module = 'homepage';
				/* $data['title'] 			= $this->title.' | '.ucwords($module);
				$data['metadescription']= 'Officially '.$this->zone.' Website';
				$data['metakeyword']= ''.$this->zone.''; */
				$data['bc'] = array(
									'module' => $module
								);
				$ccs_key = $this->module_to_ccs_key($module);
				$homepage_category_id = $this->dbfun_model->get_all_data_bys('category_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
				$var2 = array_keys($this->style['homepage']);
				$i = 0; 
				$datas2 = array();
				foreach($homepage_category_id as $key => $hp){
					$datas = array();
					$datas = $this->get_all_object($module,$hp->category_id,$this->current_language); 
					$datas = $this->relation($ccs_key,$datas); 
					$data_frt = $this->glue($datas);
					if($data_frt){
						$datas = $data_frt;
					}
					if(!empty($var2[$i])){
						$datas2[$var2[$i]] = array('section'=>$hp,'datas'=>$datas);
					}else{
						$datas2['undefined'] = array('section'=>$hp,'datas'=>$datas);
					}
					$i++;
				}
				if(in_array($this->zone,array('shisha'),true)){
					/* $cek_category = $this->category_to_category_id(str_replace('-',' ','shoes'));
					$product = $this->get_all_object_with_order_by_limit('product', $cek_category,$this->current_language);
					$datas4 = array();
					foreach($product as $ds){
						$ds_prc = (object) array_merge((array) $ds, (array) $this->get_prc('product',$cek_category,$ds->object_id));
						$m = $this->dbfun_model->get_data_bys('name','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.module_id = '.$ds->ccs_key.'','mdl');
						$datas4[] = (object) array_merge((array) $ds_prc, array('module'=> $m->name,'category' => $this->category_id_to_category($ds->ccs_key,$ds->category_id)));
					} */
					$category_product = $this->module_to_category_id($module,'favourite menu');
					$datas4 = $this->relation($ccs_key,$this->get_all_object($module,$category_product,$this->current_language)); 
					$datas2['product'] = $datas4;
				}
				$data['datas'] = $datas2;
				$category_banner = $this->module_to_category_id($module,'banner');
				$data['banner'] = $this->get_all_object_order_by($module,$category_banner,$this->current_language,'order by ccs_odc.title ASC'); 
				$data_frt_banner = $this->glue_homepage($data['banner'],'banner');
				if($data_frt_banner){
					$data['banner'] = $data_frt_banner;
				}
				$category_divider = $this->module_to_category_id($module,'divider');
				$data['divider'] = $this->relation($ccs_key,$this->get_all_object($module,$category_divider,$this->current_language)); 
				$data_frt_divider = $this->glue_homepage($data['divider'],'divider');
				if($data_frt_divider){
					$data['divider'] = $data_frt_divider;
				}
				if($this->zone == 'moonshine'){
					$data['content'][] = 'moonshine';
				}elseif($this->zone == 'contribyouth'){
					$data['content'][] = 'contribyouth';
					//print_r($data);
				}elseif($this->zone == 'ilovekb'){
					$data['content'][] = 'ilovekb';
					//print_r($data);
				}else{
					if(!empty($data['banner'])){
						$data['content'][] = 'compro/big-slider';
					}	
					$data['content'][] = 'ecom/product_list';
					$data['content'][] = 'compro/divider';
					//$data['content'][] = 'compro/parallax-arrow';
					$data['content'][] = 'compro/tiles';
					$data['content'][] = 'compro/testimonials';
					//$data['content'][] = 'compro/porto';
				}
				if(in_array($this->zone,array('shisha'),true)){
					//print_r($data_frt_banner);
				};
			}elseif($category == ''){
				$ccs_key = $this->module_to_ccs_key($module);
				if($ccs_key){
					$data['bc'] = array(
									'module' => $module
								);
					$category_category_id = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
					$datas2 = array();
					foreach($category_category_id as $key => $hp){
						$datas = array();
						$datas = $this->get_all_object($module,$hp->category_id,$this->current_language); 
						$datas2[] = array('section'=>$hp,'datas'=>$datas);
					}
					$data['datas'] = $datas2;
					if($this->zone == 'moonshine'){
						
					}else{
						$data['content'][] = 'compro/breadcrumb';
					}
					if(!empty($datas2)){
						switch($module){
							case 'news':
								$data['category'] = $this->module_to_all_category($module);
								$data['content'][] = 'compro/blog-list';
								break;
							case 'event':
								$data['years'] = $this->dbfun_model->get_all_data_bys('DISTINCT(YEAR(dateupdated)) as year','ccs_key = '.$ccs_key.' and status != 0 order by dateupdated ASC','o');
								//$data['custom_js'] = '<script src="'.base_url().'assets/frontend/js/scripts/pages/masonry-gallery.js" type="text/javascript"></script>';
								$data['content'][] = 'compro/gallery';
								break;	
							case 'product':	
								if($this->zone == 'moonshine'){
									$c_parent = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,image_square','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = 0 order by category_id ASC','oct');
									foreach($c_parent as $c_p){
										$cc = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,image_square','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$c_p->category_id.' order by category_id ASC','oct');
										$c_child[] = (object) array_merge((array)$c_p, array('child'=>$cc));
									}
									$data['category'] = $c_child;
									$data['gallery'] = $this->dbfun_model->get_all_data_bys('category_id,image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = 0 and status = 1','ogl');
									if(!empty($data['datas'])){
										$data['content'][] = 'ecom/product_list_page_category_image';
									}else{
										$data['content'][] = '404';
									}
								}else{
									$c_parent = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = 0 order by category_id ASC','oct');
									foreach($c_parent as $c_p){
										$cc = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$c_p->category_id.' order by category_id ASC','oct');
										$c_child[] = (object) array_merge((array)$c_p, array('child'=>$cc));
									}
									$data['category'] = $c_child;
									
									if(!empty($data['datas'])){
										if($zone == 'moonshine'){
											$data['sidebar'] = array('price_range'=>array('min'=>'100000','max'=>'1000000','currency'=>'IDR'),'tags'=>'color');
											if(isset($data['sidebar']['tags'])){
												$data['tags'] = $this->dbfun_model->get_all_data_bys('tags','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and tags != ""','odc');
											}
										}
										$data['content'][] = 'ecom/product_list_page';
									}else{
										$data['content'][] = '404';
									}
								}
								
								break;
							case 'branch':
								$data['content'][] = 'compro/blog-grid';
								break;
							case 'testimonial':
								if((!empty($datas2))){
									$data['category'] = $this->module_to_all_category($module);
									$data['content'][] = 'compro/blog-list';
								}else{
									$data['content'][] = '404';
								}
								break;	
							default:
								$data['content'][] = 'compro/blog-grid';
								break;
						}
					}else{
						$data['content'][] = '404';
					}	
				}else{
					switch($module){
						case 'dashboard':
							if ($this->s_login && ($this->zone_type_id == 3))
							{
								if(!$this->session->userdata($this->zone.'_admin_id')){
									$data['bc'] = array(
													'module' => $module
												);
									$data['user'] = $this->dbfun_model->get_data_bys('password,reg_id,name,description,gender,birthday,email,phone,mobile,zip,address','user_id = '.$this->user_id.' and status = 1','usr');
									$data['type'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and status = 1','umr');
									$data['content'][] = 'compro/breadcrumb';
									$data['content'][] = 'ecom/dashboard';
								}else{
									$data['content'][] = 'double_login_error_page';
								}
							}else{
								redirect(base_url($this->zone));
							}
							break;
						case 'cart':
							$data['bc'] = array(
											'module' => $module
										);
							$data['custom_js'] = "<script>
								$(document).ready(function() {
									$('.cart_qty_max').change(function() {
									  var max = parseInt($(this).attr('max'));
									  var min = parseInt($(this).attr('min'));
									  if ($(this).val() > max)
									  {
										  $(this).val(max);
									  }
									  else if ($(this).val() < min)
									  {
										  $(this).val(min);
									  }       
									}); 
									$('.coupon').on('click', function(e) {
										$(this).slideUp();
										$('#coupon').slideDown();
										$('#btn-coupon').slideDown();
									});
									$('#cancel-coupon').on('click', function(e) {
										$('.coupon').slideDown();
										$('#coupon').slideUp();
										$('#btn-coupon').slideUp();
									});
									$('#cek-coupon').on('click', function(e) {
										$('#fail.alert').empty();
										$('#success.alert').empty();
										var data = {
											".$this->security->get_csrf_token_name()." : '".trim($this->security->get_csrf_hash())."',
											ticket : $('#coupon').val(),
											param : 'ticket'
										};
										var url = '".$this->menu['link']."/cart/view_detail/cart';
										$('.btn').each(function(){
											$(this).attr('disabled', 'disabled');
										});
										$.ajax({
											url : url,
											type: 'POST',
											dataType: 'JSON',
											data: data,
											success: function(data){	
												$('.btn').each(function(){
													$(this).removeAttr('disabled');
												});
												$('#coupon').attr('disabled','disabled');
												if(data.message[0] == 'success'){
													$('#cancel-coupon').hide();$('#cek-coupon').hide();
													$('#success.alert').append('<p>'+data.message[1]+'</p>');
													$('#success.alert').slideDown();
													$('#success.alert').fadeTo(3000, 500).slideUp(500);
													$.each(data.datas, function(key, val) {
														qty = $('#qty-'+val.id+'').val();
														subtotal_new = qty * val.price_new;
														$('#'+val.id+'').after('<p id='+val.id+' class=".'"'."c-cart-price c-font-bold price".'"'." data-value=".'"'."'+val.price_new+'".'"'.">IDR '+currencyformat(val.price_new)+',00</p>');
														$('#'+val.id).addClass('c-font-line-through').removeClass('c-font-bold').removeAttr('id');
														$('#subtotal-'+val.id+'').after('<p id=".'"'."subtotal-'+val.id+'".'"'." class=".'"'."c-cart-price c-font-bold subtotal".'"'." data-subtotal=".'"'."'+subtotal_new+'".'"'.">IDR '+currencyformat(subtotal_new)+',00</p>');
														$('#subtotal-'+val.id+'').addClass('c-font-line-through').removeClass('subtotal c-font-bold').data('subtotal', 0).removeAttr('id');
													});
													var total = 0;
													$('.subtotal').each(function(){
														total += Number($(this).data('subtotal'));
													});
													$('#total-bill').empty();
													$('#total-bill').append('IDR '+currencyformat(total)+',00');
													console.log(data);
												}else{
													$('#fail.alert').append('<p>'+data.message[1]+'</p>');
													$('#fail.alert').slideDown();
													$('#fail.alert').fadeTo(3000, 500).slideUp(500);
												}
											}
										});
									});
								});
								</script>";
							//print_r($this->session->all_userdata());			
							$data['content'][] = 'compro/breadcrumb';
							$data['content'][] = 'ecom/cart_view';
							break;
						case 'checkout':
							$data['bc'] = array(
											'module' => $module
										);
							$data['user'] = '';			
							if($this->user_id){
								$data['user'] = $this->dbfun_model->get_data_bys('name,description,email,phone,mobile,zip,address','user_id = '.$this->user_id.' and status = 1','usr');
							}
							$data['content'][] = 'compro/breadcrumb';
							$data['content'][] = 'ecom/checkout';
							break;
						case 'search':
							$data['bc'] = array(
											'module' => $module
										);
							$q = $this->input->post('query');
							$dq_result = array();
							$numpage = '';
							$active_page = '';
							if($this->zone_type_id == 3){
								if($zone == 'shisha'){
									$data['sidebar'] = array('product','event','branch','menu');
								}else{
									$data['sidebar'] = array('product');
								}
								$q_module = $data['sidebar'][0];
								$data['category'] = $this->module_to_all_category($q_module);
							}else{
								$q_module = 'news';
							}
							foreach($data['sidebar'] as $key => $s){
								$this->form_validation->set_rules('query', 'query', 'trim|required|xss_clean');
								if ($this->form_validation->run() === TRUE)
								{
									$ccs_key = $this->module_to_ccs_key($s);
									$offset = 0;
									$active_page = $offset + 1;
									$limit = '3';
									$total = '0';
									$dq_result = array();
									$dq1 = $this->dbfun_model->get_all_data_bys('object_id,title,description,ccs_key,category_id','ccs_id = '.$this->zone_id.' and title like "%'.$q.'%" and ccs_key = '.$ccs_key.' and status = 1','odc');
									$dq2 = $this->dbfun_model->get_all_data_bys('object_id,title,description,ccs_key,category_id','ccs_id = '.$this->zone_id.' and description like "%'.$q.'%" and ccs_key = '.$ccs_key.' and status = 1','odc');
									$dq = array_unique(array_merge($dq1,$dq2),SORT_REGULAR);
									if(!empty($dq)){
										
										foreach($dq as $dq){
											$image = $this->dbfun_model->get_data_bys('image_square','ccs_o.status != 0 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_o.ccs_key = '.$dq->ccs_key.' and ccs_o.object_id = '.$dq->object_id.'','o');
											$dq_module = $this->dbfun_model->get_data_bys('name','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.module_id = '.$dq->ccs_key.'','mdl');
											$cats = $this->category_id_to_category($dq->ccs_key,$dq->category_id);
											if(!empty($cats)){
												$dq_result[] = (object) array_merge((array) $dq, array('category' => $cats,'module'=> $dq_module->name,'image_square'=>$image->image_square));
											}
										}
										if(!empty($dq_result)){
											$status = 'success';
										}else{
											$status = 'error';
										}
										$numpage = ceil($total/$limit);
									}else{
										$status = 'error';
										$numpage = '';
									}
								}else{
									$status = 'error';
								}
								$data['datas'][$key] = array('last_query'=>(object)array('q'=>$q,'category'=>'all','from'=>0,'to'=>0, 'module'=>$s),'status'=>$status,'data' => $dq_result, 'numpage' => $numpage, 'ap' => $active_page);
							}
							$data['content'][] = 'ecom/search_product';
							break;
						case 'confirm':
							if (($this->zone_type_id == 3))
							{
								$data['bc'] = array(
												'module' => $module
											);
											
								$q_url = $this->get_hash($_SERVER['QUERY_STRING'],'de');
								if(!empty($q_url)){
									//print_r($q_url);
									$q_url = explode('-',$q_url);
									if(count($q_url) > 1){
										$info_id = $q_url[0];
										$trx_code = str_replace('INV','',$q_url[1]);
										$list = $this->dbfun_model->get_all_data_bys('trx_code,cat_id,object_id,datecreated,ticket,total,price,varian_id,status,c_status,code,description','info_id = '.$info_id.' and ccs_id = '.$this->zone_id.' and trx_code = '.$trx_code.' order by object_id','ccs_trx');
										$data['user'] = $this->dbfun_model->get_data_bys('*','info_id = '.$info_id.' and ccs_id = '.$this->zone_id.'','ccs_tri');
										$datas2 = '';
										foreach($list as $l){
											$o = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','o');
											$odc = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','odc');
											$cat = $this->dbfun_model->get_data_bys('title','ccs_oct.ccs_id = '.$this->zone_id.' and status != 0 and category_id = '.$l->cat_id.'','oct');
											if($l->varian_id != 0){
												$settings_id = $this->dbfun_model->get_data_bys('settings_id','ccs_prv.ccs_id = '.$this->zone_id.' and ccs_prv.object_id = '.$l->object_id.' and ccs_prv.varian_id = '.$l->varian_id.'','prv')->settings_id;
												$varian = $this->dbfun_model->get_data_bys('ccs_ocs.title','ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.settings_id = '.$settings_id.'','ocs,oct');
												if(!empty($varian)){
													$varian = $varian->title;
												}else{
													$varian = '';
												}
											}else{
												$varian = '';
											}
											$m = 'Waiting for Payment';
											$cnf_img = '';
											$rcp_status = '';
											$cnf_status = $this->dbfun_model->get_data_bys('status,description','code like "CNF" and ccs_id = '.$this->zone_id.' and trx_code = '.$l->trx_code.' order by inv_id DESC','ccs_inv');
											if(!empty($cnf_status)){
												$o_data = explode('.',$cnf_status->description);
												if(count($o_data) > 2){
													$cnf_img = $o_data[1].'.'.$o_data[2];	
												}
												$cnf_status = $cnf_status->status;
												if($cnf_status == 0){
													$m = 'Waiting for Review';
												}elseif($cnf_status == 1){
													$m = 'Confirmation Success';
													$o_s = $this->dbfun_model->get_data_bys('status,description','code like "DLV" and ccs_id = '.$this->zone_id.' and icodex = '.$l->trx_code.' and status = 2','ccs_rcp');
													if(!empty($o_s)){
														$desc = explode('<br>',trim($o_s->description));
														if(isset($desc[1])){
															$m = 'On Shipping<br>Your shipping code: '.$desc[0];
														}else{
															$m = 'On Shipping';
														}
													}else{
														$m = $m.'<br>Now on Packaging';
													}
												}elseif($cnf_status == 2){
													$m = 'Rejected, Please Confirm again';
												}
											}else{
												$cnf_status = '';
											}
											if(!empty($o)){
												$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>$odc->title,'image'=>base_url('assets/'.$zone.'/product/'.$o->image_square),'cnf_status'=>$cnf_status,'rcp_status'=>$rcp_status,'m'=>$m,'cnf_img'=>$cnf_img));
											}else{
												$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>strtoupper($l->ticket),'image'=>'','cnf_status'=>$cnf_status,'rcp_status'=>$rcp_status,'m'=>$m,'cnf_img'=>$cnf_img));
											}
										}		
										$data['datas'] = $datas2;
										$data['content'][] = 'ecom/confirmation';
									}else{
										$data['content'][] = '404';
									}
								}else{
									$data['content'][] = '404';
								}
							}else{
								$data['content'][] = '404';
							}
							break;
						default:
							$cek_category = $this->module_to_category_id('sitemap','page');
							$data['datas'] = $this->get_object_by_title($module,$cek_category,$this->current_language);
							if(empty($data['datas'])){
								$data['datas'] = $this->get_object_by_title(str_replace('-',' ',$module),$cek_category,$this->current_language);
							}
							if(!empty($data['datas'])){
								if($this->is_mobile()){
									$cek_mobile = $this->dbfun_model->get_data_bys('object_id,image_square,istatus','ccs_id = '.$this->zone_id.' and object_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' and istatus = 3','ogl');
									if(!empty($cek_mobile)){
										$data['datas']->image_square = $cek_mobile->image_square;
									}
								}
								$data['custom_js'] = '<script src="'.base_url().'assets/frontend/js/scripts/pages/faq.js" type="text/javascript"></script>';
								if($this->zone == 'moonshine'){
									$data['is_banner'] = FALSE;
								}else{
									$data['content'][] = 'compro/breadcrumb';
								}
								$data['content'][] = 'compro/about';
							}else{
								$data['content'][] = '404';
							}
							break;
					}
				}
			}elseif($title == ''){
				$ccs_key = $this->module_to_ccs_key($module);
				if($ccs_key){
					$data['bc'] = array(
									'module' => $module,
									'category' => $category
								);
					$cek_category = $this->module_to_category_id(str_replace('_',' ',$module),str_replace('_',' ',$category));
					if(empty($cek_category)){
						$cek_category = $this->module_to_category_id(str_replace('_',' ',$module),str_replace('-',' ',$category));
					}
					if($cek_category){
						$section[] = (object) array('title'=>$category);
						foreach($section as $key => $hp){
							$datas = array();
							$datas = $this->get_all_object($module,$cek_category,$this->current_language); 
							$datas2[] = array('section'=>$hp,'datas'=>$datas);
						}
						$data['datas'] = $datas2;
						$data['content'][] = 'compro/breadcrumb';
						switch($module){
							case 'news':
								$data['category'] = $this->module_to_all_category($module);
								$data['content'][] = 'compro/blog-list';
								break;
							case 'event':
								$data['years'] = $this->dbfun_model->get_all_data_bys('DISTINCT(YEAR(dateupdated)) as year','ccs_key = '.$ccs_key.' and status != 0 order by dateupdated ASC','o');
								$odc = $this->dbfun_model->get_all_data_bys('object_id','ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and ccs_id = '.$this->zone_id.' and status != 0 order by dateupdated ASC','odc');
								$data['month'] = array();
								foreach($odc as $odc){
									$cek_odc = $this->dbfun_model->get_data_bys('MONTH(dateupdated) as month, MONTH(datecreated) as frm','ccs_key = '.$ccs_key.' and ccs_id = '.$this->zone_id.' and object_id = '.$odc->object_id.' and status != 0 order by dateupdated ASC','o');
									if(!empty($cek_odc)){
										if($cek_odc->month != $cek_odc->frm){
											for($i=$cek_odc->frm; $i <= $cek_odc->month; $i++){
												$data['month'][$i] = '';
											}
										}else{
											$data['month'][$cek_odc->month] = '';
										}
									}
								}
								//$data['custom_js'] = '<script src="'.base_url().'assets/frontend/js/scripts/pages/masonry-gallery.js" type="text/javascript"></script>';
								$data['content'][] = 'compro/gallery';
								break;	
							case 'product':
								$c_parent = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = 0 order by category_id ASC','oct');
								foreach($c_parent as $c_p){
									$cc = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$c_p->category_id.' order by category_id ASC','oct');
									$c_child[] = (object) array_merge((array)$c_p, array('child'=>$cc));
								}
								$data['category'] = $c_child;
								if(!empty($datas2[0]['datas']) && (!in_array(strtolower(str_replace('_',' ',$category)), array('shisha merchandise','shisha shop')))){
									if($zone == 'moonshine'){
										$data['sidebar'] = array('price_range'=>array('min'=>'100000','max'=>'1000000','currency'=>'IDR'),'tags'=>'color');
										if(isset($data['sidebar']['tags'])){
											$data['tags'] = $this->dbfun_model->get_all_data_bys('tags','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and tags != ""','odc');
										}
									}elseif($zone == 'shisha'){
										//$data['gallery'] = $this->dbfun_model->get_all_data_bys('category_id,image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1','ogl');
										$istatus = 'istatus != 3';
										if($this->is_mobile()){
											$istatus = 'istatus = 3';
										}										
										$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id as object_id, category_id,image_square,title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1 and '.$istatus.'','ogl');
										$data_frt = $this->glue($data['gallery']);
										if($data_frt){
											$data['gallery'] = $data_frt;
										}
									}
									$data['content'][] = 'ecom/product_list_page';
								}elseif(!empty($datas2[0]['datas'])){
									if($this->zone == 'shisha'){
										$c_parent = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,image_square','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = 0 and category_id = '.$cek_category.' order by category_id ASC','oct');
										foreach($c_parent as $c_p){
											$cc = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,image_square','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$c_p->category_id.' order by category_id ASC','oct');
											foreach($cc as $c){
												$c_child2[] = (object) array_merge((array)$c);
											}
										}
										$data['category'] = $c_child2;
										//print_r($data['category']);
										//$data['gallery'] = $this->dbfun_model->get_all_data_bys('category_id,image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1','ogl');
										$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id as object_id, category_id,image_square,title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1','ogl');
										$data_frt = $this->glue($data['gallery']);
										if($data_frt){
											$data['gallery'] = $data_frt;
										}
										if(!empty($data['datas'])){
											$data['content'][] = 'ecom/product_list_page_category_image';
										}else{
											$data['content'][] = '404';
										}
									}else{
										$cek_section = $this->dbfun_model->get_all_data_bys('ccs_odc.tagline','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$cek_category.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$this->current_language.' order by ccs_odc.tagline ASC','o, odc');
										$datas2 = array();
										foreach($cek_section as $key2 => $hp2){
											$datas = array();
											$section = explode(" ",$hp2->tagline);
											if($section){
												$find = '';
												$end = '';
												if(count($section) > 0){
													$end = array_pop($section);
													if(count($section) > 0){
														$find = implode(' ', $section); 
													}
												}
											}
											$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$cek_category.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$this->current_language.' and ccs_odc.tagline like "'.$find.'%" order by ccs_odc.tagline ASC','o, odc');
											$datas2[] = array('section'=>$hp2,'datas'=>$datas);
										}
										$data['datas'] = $datas2;
										$data['content'][] = 'ecom/product_page';
									}
									//$data['content'][] = 'ecom/product_page';
								}else{
									if($zone == 'moonshine'){
										$data['sidebar'] = array('price_range'=>array('min'=>'100000','max'=>'1000000','currency'=>'IDR'),'tags'=>'color');
										if(isset($data['sidebar']['tags'])){
											$data['tags'] = $this->dbfun_model->get_all_data_bys('tags','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and tags != ""','odc');
										}
									}
									$data['content'][] = 'ecom/product_list_page';
								}
								break;
							case 'branch':
								//$cek_category = $this->module_to_category_id('sitemap','page');
								//$data['datas'] = $this->get_object_by_title($category,$cek_category,$this->current_language);
								/* $data['bc'] = array(
										'module' => $module,
									);
								$data['breadcrumb'] = (object) array(
										'image' => base_url('assets').'/'.$this->zone.'/sitemap/'.$data['datas']->image_square,
									); */
								//$data['content'][] = 'compro/breadcrumb';
								//print_r($datas2);
								if(!empty($data['datas'])){
									if(isset($datas2[0]['datas'][0]->object_id)){
										$data['loc'] = $this->dbfun_model->get_data_bys('*','object_id = '.$datas2[0]['datas'][0]->object_id.'','olc');
										$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$datas2[0]['datas'][0]->object_id);	
										$data['content'][] = 'compro/branch-list';
									}else{
										$data['content'][] = '404';
									}
								}else{
									$data['content'][] = '404';
								}
								break;	
							case 'menu':
								$cek_category = $this->category_to_category_id(str_replace('_',' ',$category));
								if($this->user_id){
									if(isset($data['datas']->object_id)){
										$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
									}
								}
								if($cek_category){
									//$section[] = (object) array('title'=>$title);
									$categories = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$cek_category.' order by category_id ASC','oct');
									foreach($categories as $key => $hp){
										$datas = array();
										$datas = $this->get_all_object($module,$hp->category_id,$this->current_language); 
										$datas2[] = array('section'=>$hp,'datas'=>$datas);
									}
									$data['datas'] = $datas2;
								}
								if((!empty($datas2[0]['datas']))||(!empty($datas2[1]['datas']))){
									$data['category'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$this->module_to_ccs_key('menu').' and category_id = '.$cek_category.'','ccs_oct');
									if(!empty($datas2[0]['datas'][0]->object_id)){
										$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$datas2[0]['datas'][0]->object_id);	
									}
									$data['content'][] = 'compro/menu-list';
								}else{
									$data['content'][] = '404';
								}
								break;
							case 'testimonial':
								if((!empty($datas2))){
									$data['category'] = $this->module_to_all_category($module);
									$data['content'][] = 'compro/blog-list';
								}else{
									$data['content'][] = '404';
								}
								break;	
							default:
								$data['content'][] = 'compro/blog-grid';
								break;
						}
					}else{
						switch($module){
							default:
								$cek_category = $this->module_to_category_id('sitemap','page');
								$data['datas'] = $this->get_object_by_title($category,$cek_category,$this->current_language);
								if(!empty($data['datas'])){
									if($this->is_mobile()){
										$cek_mobile = $this->dbfun_model->get_data_bys('object_id,image_square,istatus','ccs_id = '.$this->zone_id.' and object_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' and istatus = 3','ogl');
										if(!empty($cek_mobile)){
											$data['datas']->image_square = $cek_mobile->image_square;
										}
									}
									$data['content'][] = 'compro/breadcrumb';
									$data['content'][] = 'compro/about';
								}else{
									$data['content'][] = '404';
								}
								break;
						}
					}
				}else{
					$cek_category = $this->module_to_category_id('sitemap','page');
					$data['datas'] = $this->get_object_by_title($module,$cek_category,$this->current_language);
					if(empty($data['datas'])){
						$data['datas'] = $this->get_object_by_title(str_replace('-',' ',$category),$cek_category,$this->current_language);
					}
					if(!empty($data['datas'])){
						if($this->is_mobile()){
							$cek_mobile = $this->dbfun_model->get_data_bys('object_id,image_square,istatus','ccs_id = '.$this->zone_id.' and object_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' and istatus = 3','ogl');
							if(!empty($cek_mobile)){
								$data['datas']->image_square = $cek_mobile->image_square;
							}
						}
						$data['custom_js'] = '<script src="'.base_url().'assets/frontend/js/scripts/pages/faq.js" type="text/javascript"></script>';
						if($this->zone == 'moonshine'){
							$data['is_banner'] = FALSE;
						}else{
							$data['content'][] = 'compro/breadcrumb';
						}
						$data['content'][] = 'compro/about';
					}else{
						$data['content'][] = '404';
					}
				}
			}else{
				$ccs_key = $this->module_to_ccs_key($module);
				$data['bc'] = array(
								'module' => $module,
								'category' => $category,
								'title' => $title,
							);		
				switch($module){
					case 'branch':
						$cek_category = $this->module_to_category_id('sitemap','page');
						$data['datas'] = $this->get_object_by_title($title,$cek_category,$this->current_language);
						/* $data['bc'] = array(
								'module' => $module,
							);
						$data['breadcrumb'] = (object) array(
								'image' => base_url('assets').'/'.$this->zone.'/branch/'.$data['datas']->image_square,
							); */
						//$data['content'][] = 'compro/breadcrumb';
						//$data['content'][] = 'compro/about';
						if(!empty($data['datas'])){
							$data['content'][] = 'compro/branch';
						}else{
							$data['content'][] = '404';
						}
						break;
					case 'menu':
						if($additional == ''){
							$cek_category = $this->category_to_category_id(str_replace('-',' ',$title));
							if($this->user_id){
								if(isset($data['datas']->object_id)){
									$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
								}
							}
							if($cek_category){
								$section[] = (object) array('title'=>$title);
								foreach($section as $key => $hp){
									$datas = array();
									$datas = $this->get_all_object($module,$cek_category,$this->current_language); 
									$data_frt = $this->glue($datas);
									if($data_frt){
										$datas = $data_frt;
									}
									$datas2[] = array('section'=>$hp,'datas'=>$datas);
								}
								$data['datas'] = $datas2;
							}else{
								$cek_category2 = $this->category_to_category_id(str_replace('-',' ',$category));
								$section[] = (object) array('title'=>$category);
								foreach($section as $key => $hp){
									$datas = array();
									$datas4 = $this->get_object_by_title(str_replace('-',' ',$title),$cek_category2,$this->current_language); 
									$datas3 = $this->get_all_object($module,$cek_category2,$this->current_language); 
									if(!empty($datas4)){
										$datas = array_merge(array('0'=>$datas4),(array)$datas3); 
									}else{
										$datas = $datas3;
									}
									$datas2[] = array('section'=>$hp,'datas'=>$datas);
								}
								$data['datas'] = $datas2;
							}
							if(!empty($datas2[0]['datas'])){
								if($cek_category){
									$data['category'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$this->module_to_ccs_key('menu').' and category_id = '.$cek_category.'','ccs_oct');
									if(isset($datas2[0]['datas'][0]->object_id)){
										$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$datas2[0]['datas'][0]->object_id);	
									}else{
										//$data['gallery'] = array();
										$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id as object_id, category_id,image_square,title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1','ogl');
										$data_frt = $this->glue($data['gallery']);
										if($data_frt){
											$data['gallery'] = $data_frt;
										}										
										//$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,0);	
									}
									$data['content'][] = 'compro/menu-detail';
								}elseif($cek_category2){
									if(isset($datas2[0]['datas'][0]->object_id)){
										$data['gallery'] = $this->get_all_object_gallery($module,$cek_category2,$datas2[0]['datas'][0]->object_id);
									}else{
										//$data['gallery'] = array();
										$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id as object_id, category_id,image_square,title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1','ogl');
										$data_frt = $this->glue($data['gallery']);
										if($data_frt){
											$data['gallery'] = $data_frt;
										}
										//$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,0);										
									}
									$data['content'][] = 'compro/menu-detail';
								}else{
									$data['content'][] = '404';
								}
							}else{
								$data['content'][] = '404';
							}
						}elseif($additional != ''){
							$cek_category = $this->category_to_category_id(str_replace('-',' ',$title));
							if($this->user_id){
								if(isset($data['datas']->object_id)){
									$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
								}
							}
							if($cek_category){
								$section[] = (object) array('title'=>$title);
								foreach($section as $key => $hp){
									$datas = array();
									$datas4 = $this->get_object_by_title(str_replace('-',' ',$additional),$cek_category,$this->current_language); 
									$datas3 = $this->get_all_object($module,$cek_category,$this->current_language); 
									if(!empty($datas4)){
										$datas = array_merge(array('0'=>$datas4),(array)$datas3); 
									}else{
										$datas = $datas3;
									}
									$datas2[] = array('section'=>$hp,'datas'=>$datas);
								}
								$data['datas'] = $datas2;
							}
							if(!empty($datas2[0]['datas'])){
								$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$datas2[0]['datas'][0]->object_id);
								$data['content'][] = 'compro/menu-detail';
							}else{
								
								$data['content'][] = '404';
							}
						}else{
							$cek_category = $this->category_to_category_id(str_replace('_',' ',$title));
							$data['datas'] = $this->get_object_by_title($additional,$cek_category,$this->current_language);
							if($this->user_id){
								if(isset($data['datas']->object_id)){
									$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
								}
							}
							$data['content'][] = 'compro/breadcrumb';
							$data['content'][] = 'compro/blog';
						}
						break;
					default:
						/* if($this->category_to_category_id(str_replace('_',' ',$category))){
							$cek_category = $this->category_to_category_id(str_replace('_',' ',$category));
						}else{
							$cek_category = $this->category_to_category_id(str_replace('-',' ',$category));
						} */
						$cek_category = $this->module_to_category_id(str_replace('_',' ',$module),str_replace('_',' ',$category));
						$data['datas'] = $this->get_object_by_title(str_replace('_',' ',$title),$cek_category,$this->current_language);
						$data['category'] = $this->module_to_all_category($module);
						if($this->user_id){
							if(isset($data['datas']->object_id)){
								$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
							}
						}
						if(isset($data['datas']->object_id)){
							$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$data['datas']->object_id);	
						}
						if($module == 'product'){
							if(isset($data['datas']->object_id)){
								$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id,object_id,image_square,title,description,istatus','ccs_id = '.$this->zone_id.' and category_id = '.$cek_category.' and ccs_key = '.$ccs_key.' and object_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' and istatus != 3 ','ogl');
							}
							$data['datas'] = (object) array_merge((array) $data['datas'], (array) $this->get_prc($module,$cek_category,$data['datas']->object_id));
							if($data['datas']->vstatus == 1){
								$ds_variant = $this->get_variant($module,$cek_category,$data['datas']->object_id);
								$price_max = 0;
								if(!empty($ds_variant)){
									$price_min = $ds_variant[0]->publish;
								}
								foreach($ds_variant as $dsv){
									$variant1 = (object) array_merge((array) $dsv, (array) $this->get_ocs($dsv->settings_id));
									if($this->s_login){
										$variant[] = (object) array_merge((array) $variant1, (array) $this->discount($data['datas']->object_id,$variant1->varian_id,'',''));
									}else{
										$variant[] = $variant1;
									}
									
									if($dsv->publish > $price_max){//get max
										$price_max = $dsv->publish;
									}
									if($dsv->publish < $price_min){//get min
										$price_min = $dsv->publish;
									}
								}
								if(!empty($ds_variant)){
									if($price_min != $price_max){
										$price = str_replace('.',',',$price_min).' - '.str_replace('.',',',$price_max);
									}else{
										$price = str_replace('.',',',$price_min);
									}
									$data['datas'] = (object) array_merge((array) $data['datas'], array('publish'=> $price,'discount'=> 0,'variant'=> $variant));
								}else{
									$data['datas'] = (object) array_merge((array) $data['datas'], array('publish'=> '','discount'=> 0,'variant'=> array()));
								}
							}else{
								$dsc = $this->discount($data['datas']->object_id,'','');
								/* if($this->s_login){
									$dsc = $this->dbfun_model->get_data_bys('discount','ccs_id = '.$this->zone_id.' and oid = '.$data['datas']->object_id.' and stock != 0 and status = 1','ccs_tkt');
								}else{
									$dsc = 0;
								}
								
								if(!empty($dsc)){
									$dsc = $dsc->discount;
								}else{
									$dsc = 0;
								} */
								$data['datas'] = (object) array_merge((array) $data['datas'], array('discount'=> $dsc));
								if((strtolower($category) == 'member_card') && $this->is_verified_member()){
									$data['datas'] = (object) array_merge((array) $data['datas'], array('stock'=> 0));
								}
							}
							$data['rltd_o_w_oct'] = '';
							//print_r($data['datas']);
							$data['content'][] = 'compro/breadcrumb';
							$data['content'][] = 'ecom/product_detail';
							$data['custom_js'] = "
							<script>
									$('.c-spinner').click(function() {
										var qty = $('input[name=".'"'."qty".'"'."]').val();
										$(".'"'."[data-id='".$data['datas']->object_id."']".'"'.").data('qty',qty);
									});
									
								</script>
								<script src='".base_url()."assets/frontend/plugins/zoom-master/jquery.zoom.min.js' type='text/javascript'></script>
							";
						}elseif($module == 'event'){
							//$data['content'][] = 'compro/breadcrumb';
							$view = $this->load->view('compro/ajax-detail', $data, TRUE);
							echo $view;
						}else{
							$data['content'][] = 'compro/breadcrumb';
							$data['content'][] = 'compro/blog';
						}
						break;
				}			
			}
			if(($module == 'event') && ($title != '')){
				
			}else{
				$this->load->view('ecom/index',$data);
			}
		}else{
			show_404();
		} 
	}

	public function login($zone)
	{
		if (!$this->s_login)
    	{
    		$status = '';
    		$message = '';

    		$this->form_validation->set_rules('u', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('p', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');

			if ($this->form_validation->run() === TRUE)
			{
				$username = trim($this->input->post('u'));
				$id = $this->dbfun_model->get_data_bys('ccs_id','LOWER(name) like "'.strtolower($zone).'"','zone')->ccs_id;
				$password = $this->input->post('p');
				//$type_id = $this->input->post('type');
				
				$login_data = $this->dbfun_model->get_data_bys('*', array('LOWER(username)' => strtolower($username), 'ccs_id'=> $id), 'usr');
				$data['pass'] = MD5($password);
				$cek_user_is_exist = $this->dbfun_model->get_data_bys('status', array('username' => $username,'password' => $data['pass'], 'ccs_id'=> $id), 'usr');
				
				if ($login_data)
				{
					if($cek_user_is_exist){
					/* if (($login_data->first_log ==='0') || ($login_data->first_log ==='1'))
					{ */						
						if($cek_user_is_exist->status === '1')
						{ 
							$foto = '';
							if ($login_data->avatar)
							{
								$foto = $login_data->avatar;
							}
							else
							{
								$foto = 'admin_empty.jpg';
							}
							$session = array(
								strtolower($zone).'_username' => $login_data->username,
								strtolower($zone).'_nama' => $login_data->name,
								strtolower($zone).'_zone' => strtolower($zone),
								strtolower($zone).'_user_type' => $login_data->type_id,
								strtolower($zone).'_reg_id' => $login_data->reg_id,
								strtolower($zone).'_user_id' => $login_data->user_id,
								strtolower($zone).'_status' => $login_data->status,
								strtolower($zone).'_foto' => $foto,
								strtolower($zone).'_logged_in' => TRUE
							);
							$this->session->set_userdata($session);
							$this->log_user($login_data->user_id, 0, 1, 1);
							$this->refresh_cart_session();
							$status= 'success';
							$message = '<b>Login berhasil!</b> mohon tunggu ...';
						}
						elseif(($cek_user_is_exist->status === '2') || ($cek_user_is_exist->status === '0'))
						{
							$status = 'error';
							$message = 'Maaf! Akun Anda sudah tidak aktif';	
						}
						elseif($cek_user_is_exist->status === '3')
						{
							$status = 'error';
							$message = 'Maaf! Akun Anda sudah tidak aktif';	
						}
						else
						{
							$status = 'error';
							$message = 'Maaf! Password yang Anda masukan salah';	
						}	
					}else{
						$status = 'error';
						$message = 'Maaf! Password yang Anda masukan salah';
					}
				}
				else
				{
					$status = 'error';
					$message = 'Maaf! Akun dengan username tersebut tidak ada';
				}
			}
			else
			{
				$status = 'error';
				$message = validation_errors();
			}

			if ($message)
			{
				echo json_encode(array('status' => $status, 'm' => $message));
			}
			else
			{
				redirect($this->login);
			}
    	}
    	else
    	{
    		redirect($_SERVER['REQUEST_URI'], 'refresh'); 
    	}
	}
	
	public function sign_up($zone){
		if (!$this->s_login)
    	{
    		$status = '';
    		$message = '';
			$ceks = false;
			$cek = $this->input->post('rt_id');
			if(!empty($cek)){
				$this->form_validation->set_rules('u', 'Username', 'trim|required|xss_clean');
				$this->form_validation->set_rules('mobile', 'Mobile Phone', 'trim|numeric|required|xss_clean');
				$this->form_validation->set_rules('e', 'E-mail', 'trim|required|valid_email|xss_clean');
				$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean');
			}else{
				$this->form_validation->set_rules('u', 'Username', 'trim|required|xss_clean');
				$this->form_validation->set_rules('p', 'Password', 'trim|required|xss_clean');
				$this->form_validation->set_rules('p2', 'Confirmation Password', 'trim|required|xss_clean');
				$this->form_validation->set_rules('e', 'E-mail', 'trim|required|valid_email|xss_clean');
				$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean');
			}
    		
			if ($this->form_validation->run() === TRUE)
			{
				$username = trim($this->input->post('u'));
				$email = trim($this->input->post('e'));
				$password = $this->input->post('p');
				$password2 = $this->input->post('p2');
				$real_pass = MD5($password);
				$cek_username = $this->dbfun_model->get_data_bys('username', 'LOWER(username) like "'.strtolower($username).'" and ccs_id = '.$this->zone_id.'','usr');
				$cek_username2 = $this->dbfun_model->get_data_bys('username', 'LOWER(username) like "'.strtolower($username).'" and ccs_id = '.$this->zone_id.' and type_id = 3','usr');
				$cek_email = $this->dbfun_model->get_data_bys('email', 'LOWER(email) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.'','usr');
				$cek_email2 = $this->dbfun_model->get_data_bys('email', 'LOWER(email) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.' and type_id = 3','usr');
				
				if(!empty($cek))
				{
					if(empty($cek_username2)){
						if(empty($cek_email2)){
							$ceks = true;
						}else{
							$status = 'error';
							$message = ''.$email.' already exist, please choose another';
						}
					}else{
						$status = 'error';
						$message = ''.$username.' already exist, please choose another';
					}
				}
				else
				{
					if(($password == $password2)){
						if(empty($cek_username)){
							if(empty($cek_email)){
								$ceks = true;
							}else{
								$status = 'error';
								$message = ''.$email.' already exist, please choose another';
							}
						}else{
							$status = 'error';
							$message = 'username: '.$username.' already exist, please choose another';
						}
					}else{
						$status = 'error';
						$message = "Password doesn't match";
					}
				}
			}
			else
			{
				$status = 'error';
				$message = validation_errors();
			}
			
			if($ceks == true)
			{
				$admin_id = $this->dbfun_model->get_data_bys('admin_id','ccs_id = '.$this->zone_id.'','adm')->admin_id;
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				$activation_key = substr( str_shuffle( $chars ), 0, 10 );
				$user_type = $this->input->post('rt_id');
				if(!empty($user_type)){
					$type = $this->input->post('type_id');
					$user_type = $user_type;
					$real_pass = '';
					$need_validate = true;
				}else{
					$type = 2;
					$user_type = 0;
					$need_validate = false;
				}
				$custom_usr = array(
					'username' => $username,
					'name' => $this->input->post('name'),
					'email' => $email,
					'status' => 1,
					'createdby' => $admin_id,
					'datecreated' => date('Y-m-d H:i:s'),
					'type_id' => $type,
					'rt_id' => $user_type,
					'rw_id' => 0,
					'activation_key' => $activation_key,
					'ccs_id' => $this->zone_id,
					'password' => $real_pass
				);
				$user_id = $this->new_data('ccs_usr',$custom_usr,'','','','');
				if(!empty($user_id)){
					if($need_validate === true){
						$subject = '['.strtoupper($this->zone).'] Sign Up Success';
						$data = $this->dbfun_model->get_data_bys('name,username', 'LOWER(email) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.'','usr');
						$logo = $this->dbfun_model->get_data_bys('logo','LOWER(name) like "'.strtolower($this->zone).'"','zone')->logo;
						$content = array(
								'username' => $data->username,
								'full_name' => $data->name,
								'logo' => base_url('assets').'/'.$zone.'/'.$logo,
								'url' => $this->menu['link'],
								'name' => $zone,
								'title' => 'Welcome to '.ucfirst($zone),
								'style' => $this->menu['primaryc'],
							);
						$this->send_mail($email,$subject,$content,'ecom/email/sign_up_validate');
						$status = 'success';
						$message = 'Sign Up Success, Waiting for Approval';
					}else{
						$login_data = $this->dbfun_model->get_data_bys('*', 'LOWER(username) like "'.strtolower($username).'" and ccs_id = '.$this->zone_id.'', 'usr');
						$foto = '';
						if ($login_data->avatar)
						{
							$foto = $login_data->avatar;
						}
						else
						{
							$foto = 'admin_empty.jpg';
						}
						$session = array(
							strtolower($zone).'_username' => $login_data->username,
							strtolower($zone).'_nama' => $login_data->name,
							strtolower($zone).'_zone' => strtolower($zone),
							strtolower($zone).'_user_type' => $login_data->type_id,
							strtolower($zone).'_reg_id' => $login_data->reg_id,
							strtolower($zone).'_user_id' => $login_data->user_id,
							strtolower($zone).'_status' => $login_data->status,
							strtolower($zone).'_foto' => $foto,
							strtolower($zone).'_logged_in' => TRUE
						);
						$this->session->set_userdata($session);
						$this->log_user($login_data->user_id, 0, 1, 1);
						
						$subject = '['.strtoupper($this->zone).'] Sign Up Success';
						$data = $this->dbfun_model->get_data_bys('name,username', 'LOWER(email) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.'','usr');
						$logo = $this->dbfun_model->get_data_bys('logo','LOWER(name) like "'.strtolower($this->zone).'"','zone')->logo;
						$content = array(
								'username' => $data->username,
								'full_name' => $data->name,
								'logo' => base_url('assets').'/'.$zone.'/'.$logo,
								'url' => $this->menu['link'],
								'name' => $zone,
								'title' => 'Welcome to '.ucfirst($zone),
								'style' => $this->menu['primaryc'],
							);
						$this->send_mail($email,$subject,$content,'ecom/email/sign_up');
						
						if($this->zone_type_id == 3){
							/*update tri*/
							$cek_tri = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and LOWER(email) like "'.strtolower($email).'"','ccs_tri');
							if(!empty($cek_tri)){
								$custom_tri = array(
									'user_id'=> $login_data->user_id,
									'type_id'=> $login_data->type_id,
								);
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and LOWER(email) like "'.strtolower($email).'"', $custom_tri, 'ccs_tri');
								$custom_usr = array(
									'phone'=> $cek_tri->phone,
									'mobile'=> $cek_tri->mobile,
									'address'=> $cek_tri->address,
								);
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and user_id = '.$login_data->user_id.'', $custom_usr, 'ccs_usr');
								/*update trx*/
								$cek_trx = $this->dbfun_model->get_all_data_bys('*','ccs_id = '.$this->zone_id.' and info_id = '.$cek_tri->info_id.'','ccs_trx');
								if(!empty($cek_trx)){
									$custom_trx = array(
										'user_id'=> $login_data->user_id,
									);
									foreach($cek_trx as $trx){
										$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and trx_id = '.$trx->trx_id.'', $custom_trx, 'ccs_trx');
									}
								}
							}	
						}
						$status = 'success';
						$message = 'Sign Up Success';
					}
					
				}else{
					$status = 'error';
					$message = 'Sign Up failed';
				}
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}
		else
		{
			redirect($_SERVER['REQUEST_URI'], 'refresh'); 
		}
	}
	
	public function forgot_password($zone){
		if (!$this->s_login)
    	{
			$status = '';
    		$message = '';
			
			$this->form_validation->set_rules('e', 'E-mail', 'trim|required|valid_email|xss_clean');
			if ($this->form_validation->run() === TRUE)
			{
				$email = trim($this->input->post('e'));
				$cek_email = $this->dbfun_model->get_data_bys('email,username', 'LOWER(email) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.'','usr');
				if(!empty($cek_email)){
					
					$chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789";
					$password = substr( str_shuffle( $chars ), 0, 6 );
					$custom_usr = array(
						'password'=> md5($password),
						'dateupdated'=> date('Y-m-d H:i:s'),
					);
					$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and LOWER(email) like "'.strtolower($email).'"', $custom_usr, 'ccs_usr');
					
					$subject = '['.strtoupper($this->zone).'] Password Reset';
					$data = $this->dbfun_model->get_data_bys('username', 'LOWER(email) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.'','usr');
					$logo = $this->dbfun_model->get_data_bys('logo','LOWER(name) like "'.strtolower($this->zone).'"','zone')->logo;
					$content = array(
							'username' => $data->username,
							'password' => $password,
							'logo' => base_url('assets').'/'.$zone.'/'.$logo,
							'url' => $this->menu['link'],
							'name' => $zone,
							'title' => 'Password Reset',
							'style' => $this->menu['primaryc'],
						);
					$this->send_mail($email,$subject,$content,'ecom/email/forgot_password');
					$status = 'success';
					$message = 'Success, Please Check Your Email';
				}else{
					$status = 'error';
					$message = 'Email Not Valid';
				}
			}
			else
			{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}else{
			redirect($_SERVER['REQUEST_URI'], 'refresh'); 
		}
	}

	public function member_area($zone){
		if (!$this->s_login)
    	{
			$status = '';
    		$message = '';
			
			$this->form_validation->set_rules('reg_id', 'Member ID', 'trim|required|xss_clean');
			$this->form_validation->set_rules('name', 'Member Name', 'trim|required|xss_clean');
			if ($this->form_validation->run() === TRUE)
			{
				$email = trim($this->input->post('reg_id'));
				$name = strtolower(trim($this->input->post('name')));
				$cek_email = $this->dbfun_model->get_data_bys('*', 'LOWER(reg_id) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.' and LOWER(name) like "'.$name.'"','usr');
				if(!empty($cek_email)){
					$foto = '';
					if ($cek_email->avatar)
					{
						$foto = $cek_email->avatar;
					}
					else
					{
						$foto = 'admin_empty.jpg';
					}
					$session = array(
						strtolower($zone).'_username' => $cek_email->username,
						strtolower($zone).'_nama' => $cek_email->name,
						strtolower($zone).'_zone' => strtolower($zone),
						strtolower($zone).'_user_type' => $cek_email->type_id,
						strtolower($zone).'_reg_id' => $cek_email->reg_id,
						strtolower($zone).'_user_id' => $cek_email->user_id,
						strtolower($zone).'_status' => $cek_email->status,
						strtolower($zone).'_foto' => $foto,
						strtolower($zone).'_logged_in' => TRUE
					);
					$this->session->set_userdata($session);
					$this->log_user($cek_email->user_id, 0, 1, 1);
					$this->refresh_cart_session();
					$status= 'success';
					$message = '<b>Login berhasil!</b> mohon tunggu ...';
				}else{
					$status = 'error';
					$message = 'Data Member Not Found';
				}
			}
			else
			{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}else{
			redirect($_SERVER['REQUEST_URI'], 'refresh'); 
		}
	}
	
	public function logout($zone){
		$cek = $this->dbfun_model->get_data_bys('name','LOWER(name) like "'.strtolower($zone).'"','zone');
		if($cek){
			if ($this->s_login)
			{
				$this->log_user($this->user_id, 0, 0, 1);
				$session = array(
						strtolower($zone).'_username' => '',
						strtolower($zone).'_nama' => '',
						strtolower($zone).'_zone' => '',
						strtolower($zone).'_user_type' => '',
						strtolower($zone).'_reg_id' => '',
						strtolower($zone).'_user_id' => '',
						strtolower($zone).'_status' => '',
						strtolower($zone).'_foto' => '',
						strtolower($zone).'_logged_in' => FALSE
				); 
				$this->session->unset_userdata($session);
				$this->load->library('cart');
				$this->cart->destroy();
				if($this->is_active_domain){
					redirect(base_url());
				}else{
					redirect(base_url($this->zone));
				}
			}
			else
			{
				redirect(base_url($this->zone));
			}
		}else{
			show_404();
		}
	}
	
	public function detail_ajax($zone,$type){

		if ($this->s_login && !empty($type) && ($type != 'list') && ($type != 'checkout') && ($type != 'create')){
			
			$data = array();
			$data['zone'] = $zone;
			if($type == 'view'){
				echo header('Content-Type: text/html; charset=ISO-8859-1');
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$part = $this->input->post('param');
				if($this->form_validation->run() == TRUE ){
					if($part == 'edit_profile'){
						$data['menu']['link'] = $this->menu['link'];
						$data['user'] = $this->dbfun_model->get_data_bys('password, reg_id,name,description,gender,birthday,email,phone,mobile,zip,address','user_id = '.$this->user_id.' and status = 1','usr');
						$view = $this->load->view('ecom/edit_profile', $data, TRUE);
					}elseif($part == 'order_history'){
						//echo '<script src="'.base_url().'assets/admin/js/ajaxfileupload.js" type="text/javascript"></script>';
						$param2 = $this->input->post('param2');
						if($param2 == 'list'){
							$list = $this->dbfun_model->get_all_data_bys('trx_code,cat_id,object_id,datecreated,ticket,total,price,varian_id,status,c_status,code','user_id = '.$this->user_id.' and ccs_id = '.$this->zone_id.' and trx_code = '.$this->input->post('id').'','ccs_trx');
							$datas2 = '';
							foreach($list as $l){
								$o = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','o');
								$odc = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','odc');
								$cat = $this->dbfun_model->get_data_bys('title','ccs_oct.ccs_id = '.$this->zone_id.' and status != 0 and category_id = '.$l->cat_id.'','oct');
								if($l->varian_id != 0){
									$settings_id = $this->dbfun_model->get_data_bys('settings_id','ccs_prv.ccs_id = '.$this->zone_id.' and ccs_prv.object_id = '.$l->object_id.' and ccs_prv.varian_id = '.$l->varian_id.'','prv')->settings_id;
									$varian = $this->dbfun_model->get_data_bys('ccs_ocs.title','ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.settings_id = '.$settings_id.'','ocs,oct');
									if(!empty($varian)){
										$varian = $varian->title;
									}else{
										$varian = '';
									}
								}else{
									$varian = '';
								}
								$m = 'Waiting for Payment';
								$cnf_img = '';
								$rcp_status = '';
								$cnf_status = $this->dbfun_model->get_data_bys('status,description','code like "CNF" and ccs_id = '.$this->zone_id.' and trx_code = '.$l->trx_code.'','ccs_inv');
								if(!empty($cnf_status)){
									$o_data = explode('.',$cnf_status->description);
									$cnf_img = $o_data[1].'.'.$o_data[2];
									$cnf_status = $cnf_status->status;
									if($cnf_status == 0){
										$m = 'Waiting for Review';
									}elseif($cnf_status == 1){
										$m = 'Confirmation Success';
										$o_s = $this->dbfun_model->get_data_bys('status,description','code like "DLV" and ccs_id = '.$this->zone_id.' and icodex = '.$l->trx_code.' and status = 2','ccs_rcp');
										if(!empty($o_s)){
											$desc = explode('<br>',trim($o_s->description));
											if(isset($desc[1])){
												$m = 'On Shipping<br>Your shipping code: '.$desc[0];
											}else{
												$m = 'On Shipping';
											}
										}else{
											$m = $m.'<br>Now on Packaging';
										}
									}elseif($cnf_status == 2){
										$m = 'Rejected, Please Confirm again';
									}
								}else{
									$cnf_status = '';
								}
								if(!empty($o)){
									$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>$odc->title,'image'=>base_url('assets/'.$zone.'/product/'.$o->image_square),'cnf_status'=>$cnf_status,'rcp_status'=>$rcp_status,'m'=>$m,'cnf_img'=>$cnf_img));
								}else{
									$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>strtoupper($l->ticket),'image'=>'','cnf_status'=>$cnf_status,'rcp_status'=>$rcp_status,'m'=>$m,'cnf_img'=>$cnf_img));
								}
							}							
							echo json_encode($datas2);
							$view = '';
						}elseif($param2 == 'confirmation'){
							$status = 'error';
							$file = 'file';
							$param3 = trim($this->input->post('param3'));
							if(empty($this->input->post('id'))){
								$m = 'please choose invoice number';
							}elseif(!isset($_FILES[$file]) && (empty($param3))){
								$m = 'no file added';
							}else{
								$table = 'ccs_inv';
								$code = 'TRXON';
								$code2 = 'CNF';
								$path = $this->zone.'/trx/cnf';
								$where = 'ccs_id = '.$this->zone_id.' and trx_code like "'.trim($this->input->post('id')).'" and status = 0 and code like "'.$code.'"';
								$where2 = 'ccs_id = '.$this->zone_id.' and trx_code like "'.trim($this->input->post('id')).'" and status = 0 and code like "'.$code2.'"';
								$cek = $this->dbfun_model->get_data_bys('*',$where,$table);
								$cek2 = $this->dbfun_model->get_data_bys('*',$where2,$table);
								$m = 'please choose valid invoice number';
								if(!empty($cek)){
									if((empty($cek2)) && (empty($param3))){
										$status = 'success';
										$m = 'confirmation sent';
										
										if (!is_dir('assets/'.$this->zone.'/trx/'))
										{
											mkdir('./assets/'.$this->zone.'/trx/', 0777, true);
										}
										if (!is_dir('assets/'.$this->zone.'/trx/cnf/'))
										{
											mkdir('./assets/'.$this->zone.'/trx/cnf/', 0777, true);
										}
										$param3 = true;
										$param4 = 'new';
									}elseif(empty($param3)){
										$o_data = explode('.',$cek2->description);
										$old_data = $o_data[1].'.'.$o_data[2];
										$status = 'success';
										$m = 'confirmation file updated';
										$param3 = true;
										$param4 = 'update';
									}elseif($param3 == 'cancel'){
										$status = 'success';
										$m = 'Cancel Order Success';
										$param3 = false;
										$param4 = 'cancel';
									}
									
									if($param3 == true){
										$file = 'file';
										$type = 'jpg|jpeg|png';
										$nama_file = trim($file.'-'.date('Ymd_His'));
										$config = unggah_berkas($path, $nama_file, $type);
										$this->upload->initialize($config);
										
										if (!$this->upload->do_upload($file))
										{
											$status = 'error';
											$message = $this->upload->display_errors('', '');
										}else{
											$upload_data = $this->upload->data();
											$nama_file = $nama_file.$upload_data['file_ext'];
										}
										$desc = 'confirmation payment.'.$nama_file;
										$custom = array(
													'ccs_id'=> $this->zone_id,
													'ccs_key'=> $cek->ccs_key,
													'type'=> 2,
													'code'=> $code2,
													'icode'=> 'INV',
													'inv_code'=> $cek->inv_code,
													'trx_code'=> $cek->trx_code,
													'description'=> $desc,
													'dateinv'=> $cek->dateinv,
													'datecreated'=> $cek->datecreated,
													'dateupdated'=> $cek->dateupdated,
													'createdby'=> $cek->createdby,
													'updatedby'=> $cek->createdby,
													'status'=> 0,
												);
										if($param4 == 'update'){
											$file_path = './assets/'.$path.'/'.trim($old_data);
											if(!empty($old_data)){
												if (file_exists($file_path))
												{
													unlink($file_path);
												}
											}
											$this->dbfun_model->del_tables($where2, $table);
										}
										if(($param4 == 'update') ||($param4 == 'new')){
											$this->new_data($table,$custom,'','','','');
										}
									}elseif($param3 === false){
										if($param4 == 'cancel'){
											$custom = array(
													'status'=> 2,
												);
											$this->dbfun_model->update_table($where, $custom, 'ccs_trx');
											$this->dbfun_model->update_table($where, $custom, $table);
										}
									}
								}else{
									$m = 'please choose valid invoice number';
									if($param3 == 'cancel'){
										$status = 'success';
										$m = 'Cancel Order Success';
										$code = 'TRXON';
										$where = 'ccs_id = '.$this->zone_id.' and trx_code like "'.trim($this->input->post('id')).'" and status = 0 and code like "'.$code.'"';
										$custom = array(
												'status'=> 2,
											);
										$this->dbfun_model->update_table($where, $custom, 'ccs_inv');
										$this->dbfun_model->update_table($where, $custom, 'ccs_trx');
									}
								}
							}
							
							$datas2 = array('status'=>$status,'m'=>$m);
							echo json_encode($datas2);
							$view = '';
						}else{
							$datas = $this->dbfun_model->get_all_data_bys('distinct(ccs_inv.datecreated),ccs_inv.trx_code,ccs_inv.code,ccs_inv.status','ccs_trx.user_id = '.$this->user_id.' and ccs_trx.ccs_id = '.$this->zone_id.' and ccs_trx.ccs_id = ccs_inv.ccs_id and ccs_trx.trx_code = ccs_inv.trx_code and ccs_inv.status != 2 and ccs_trx.code = ccs_inv.code order by ccs_trx.datecreated DESC','ccs_trx,ccs_inv');
							$datas2 = '';
							$cek = $this->dbfun_model->get_all_data_bys('trx_code,status','code like "CNF" and ccs_id = '.$this->zone_id.'','ccs_inv');
							$datas3 = array();
							$a = array();
							if(!empty($cek)){
								$a = array_map(function($entry) { return $entry->trx_code; }, 
								$cek);
								foreach($datas as $ds4){
									if(!in_array($ds4->trx_code,$a,true)){
										$datas3[] = (object) array('trx_code'=>$ds4->trx_code);
									}
								}
							}else{
								foreach($datas as $ds4){
									$datas3[] = (object) array('trx_code'=>$ds4->trx_code);
								}
							}
							foreach($datas as $ds){
								$ds2 = $this->dbfun_model->get_all_data_bys('price,total,object_id','user_id = '.$this->user_id.' and ccs_id = '.$this->zone_id.' and trx_code = '.$ds->trx_code.'','ccs_trx');
								$count_prc = '';
								$total = '';
								if(!empty($ds2)){
									foreach($ds2 as $ds3){
										$count_prc += $ds3->price;
										if($ds3->object_id != 0){
											$total += $ds3->total;
										}
									}
								}
								$m = 'Waiting for Payment';
								$cnf_status = '';
								$rcp_status = '';
								if(in_array($ds->trx_code,$a,true)){
									$cnf_status = $this->dbfun_model->get_data_bys('status','code like "CNF" and ccs_id = '.$this->zone_id.' and trx_code = '.$ds->trx_code.'','ccs_inv')->status;
									if($cnf_status == 0){
										$m = 'Waiting for Review';
									}elseif($cnf_status == 1){
										$m = 'Confirmation Success';
										$rcp_status = $this->dbfun_model->get_data_bys('status','code like "RCP" and ccs_id = '.$this->zone_id.' and icodex = '.$ds->trx_code.'','ccs_rcp')->status;
										if($rcp_status == 0){
											$m = '';
										}elseif($rcp_status == 1){
											$m = '';
										}elseif($rcp_status == 2){
											$m = '';
										}
									}elseif($cnf_status == 2){
										$m = 'Rejected, Please Confirm again';
									}
								}
								$datas2[] = (object) array_merge((array)$ds, array('count_prc'=>$count_prc,'total'=>$total,'cnf_status'=>$cnf_status,'rcp_status'=>$rcp_status,'m'=>$m));
							}
							$data['datas2'] = $datas3;
							$data['is_pagination'] = false;
							$total = count($datas2);
							$limit = 5;
							$data['total'] = $total;
							$data['limit'] = $limit;
							if(empty($param2)){
								$start = 0;
							}else{
								$start = $param2*$limit;
							}
							$data['start'] = $start;
							if($total > $limit){
								$data['is_pagination'] = true;
								$data['offset'] = $param2;
							}
							$numpage = ceil($total/$limit);
							if($datas2){
								$datas2 = array_slice($datas2, $start, $limit, false);
							}else{
								$datas2 = $datas2;
							}
							$data['datas'] = $datas2;
							$view = $this->load->view('ecom/order_history', $data, TRUE);
						}
					}elseif($part == 'address'){
						$data['user'] = $this->dbfun_model->get_data_bys('name,description,gender,birthday,email,phone,mobile,zip,address','user_id = '.$this->user_id.' and status = 1','usr');
						$phone = $data['user']->phone;
						$mobile = $data['user']->mobile;
						if(empty($phone)){
							$phone = '-';
						};
						if(empty($mobile)){
							$mobile = '-';
						};
						$data['address_1'] = (object) array(
														'address'=>$data['user']->address,
														'phone'=>$phone,
														'mobile'=>$mobile,
														'zip'=>$data['user']->zip,
													);
						$data['address_2'] = (object) array(
														'address'=>$data['user']->address,
														'phone'=>$phone,
														'mobile'=>$mobile,
														'zip'=>$data['user']->zip,
													);
						$cek_tri = $this->dbfun_model->get_data_bys('address,address_2','user_id = '.$this->user_id.' and status = 1 and ccs_id = '.$this->zone_id.'','tri');
						if(!empty($cek_tri)){
							$tri1 = explode('<br>',$cek_tri->address);
							$tri2 = explode('<br>',$cek_tri->address_2);
							$data['address_1'] = (object) array(
														'address'=>$tri1[0].'<br>'.$tri1[1],
														'phone'=>$phone,
														'mobile'=>$mobile,
														'zip'=> str_replace('KODE POS: ','',$tri1[2]),
													);
							$data['address_2'] = (object) array(
														'address'=>$tri2[0].'<br>'.$tri2[1],
														'phone'=>$phone,
														'mobile'=>$mobile,
														'zip'=> str_replace('KODE POS: ','',$tri2[2]),
													);
						}
						$view = $this->load->view('ecom/address', $data, TRUE);
					}elseif($part == 'wishlist'){
						$view = $this->load->view('ecom/wishlist', $data, TRUE);
					}elseif($part == 'message'){
						$data['menu']['link'] = $this->menu['link'];
						$data['child'] = 'messages';
						$data['type_id'] = $this->user_type;
						$cek = $this->dbfun_model->get_data_bys('type_id,rt_id,rw_id','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and user_id = '.$this->user_id.'','usr');
						if($cek->rw_id != 0){
							$ck = array();
							//$ck[] = (object) array('type_id' => '0', 'name' => 'Admin');
							$cek2 = $this->dbfun_model->get_all_data_bys('distinct(type_id)','ccs_id = '.$this->zone_id.' and rt_id = '.$cek->rt_id.'','usr');
							foreach($cek2 as $cc){
								$ck[] = $this->dbfun_model->get_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and type_id = '.$cc->type_id.'','umr');
							}
							$data['type'] = $ck;
						}elseif($cek->rt_id != 0){
							$ck = array();
							$ck[] = (object) array('type_id' => '0', 'name' => 'Admin');
							$cek2 = $this->dbfun_model->get_all_data_bys('distinct(type_id)','ccs_id = '.$this->zone_id.' and rt_id = '.$cek->rt_id.'','usr');
							foreach($cek2 as $cc){
								$ck[] = $this->dbfun_model->get_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and type_id = '.$cc->type_id.'','umr');
							}
							$data['type'] = $ck;
						}elseif($cek->type_id != 0){
							$ck = array();
							$ck[] = (object) array('type_id' => '0', 'name' => 'Admin');
							$cek2 = $this->dbfun_model->get_all_data_bys('distinct(type_id)','ccs_id = '.$this->zone_id.' and rt_id = '.$cek->rt_id.'','usr');
							foreach($cek2 as $cc){
								$ck[] = $this->dbfun_model->get_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and type_id = '.$cc->type_id.'','umr');
							}
							$data['type'] = $ck;
						}else{
							$data['type'] = '';
						}	
						$admin_id = $this->user_id;
						$table = 'usr';
						$column = 'user_id';
						$user = "ccs_usr.type_id = ".$this->user_type."";
						$part2 = $this->input->post('param2');
						$id = $this->user_type;
						$param2 = 0;
						if(in_array($part2, array('inbox','sendmail','drafts'))){
							echo '<link href="'.base_url().'assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet">';
							echo '<script src="'.base_url().'assets/admin/js/plugins/iCheck/icheck.min.js"></script>';
							if($part2 == 'inbox'){
								$sender = 'tr_id = '.$id.' and receiver = '.$admin_id.'';
								$read_status = ''.$sender.' and ccs_msg.status = 1 and read_status >= 2 and read_status <= 3';
								$type_user = 'and ts_id = '.$param2.'';
							}elseif($part2 == 'sendmail'){
								$sender = 'ts_id = '.$id.' and sender = '.$admin_id.'';
								$read_status = ''.$sender.' and ccs_msg.status = 1';
								$type_user = 'and tr_id = '.$param2.'';
							}elseif($part2 == 'drafts'){
								$sender ='ts_id = '.$id.' and sender = '.$admin_id.'';
								$read_status = ''.$sender.' and ccs_msg.status = 0 and ccs_msg.read_status = 0';
								$type_user = 'and tr_id = '.$param2.'';
							}
							$ds2 = array();
							if($param2 == 'all'){
								$msg = $this->dbfun_model->get_all_data_bys('message_id, ts_id,tr_id, sender,receiver, subject, status,read_status,datecreated','ccs_id = '.$this->zone_id.' and '.$read_status.'','msg');
							}else{
								$msg = $this->dbfun_model->get_all_data_bys('message_id, ts_id,tr_id, sender,receiver, subject, status,read_status,datecreated','ccs_id = '.$this->zone_id.' and '.$read_status.' '.$type_user.'','msg');
							}
							$i= 0;
							foreach($msg as $m){
								if(in_array($part2, array('sendmail','drafts'))){
									$object = $m->receiver;
									if($m->tr_id == 0){
										$db = 'adm';
										$db_column = 'admin_id';
									}else{
										$db = 'usr';
										$db_column = 'user_id';
									}
								}else{
									$object = $m->sender;
									if($m->ts_id == 0){
										$db = 'adm';
										$db_column = 'admin_id';
									}else{
										$db = 'usr';
										$db_column = 'user_id';
									}
								}
								$ds = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and '.$db_column.' = '.$object.'',$db);
								$ds2[] = (object) array_merge((array) $m, array('name' => $ds->name));
								if(($m->status == 1) && ($m->read_status == 3)){
									$i++;
								}
							};
							$data['messages'] = $ds2;
							$data['m_total'] = $i;
							$data['title'] = $part2;
							
							echo '<script>
								$(".i-checks").iCheck({
									checkboxClass: "icheckbox_square-green",
									radioClass: "iradio_square-green",
								});</script>';
							$view = $this->load->view('ecom/messages', $data, TRUE);	
						}elseif(in_array($part2, array('detail','compose','reply','forward'))){
							$param = $this->input->post('language_id');
							$this->segretis = 0;
							$this->simo = 0;
							$data['part'] = $part2;
							$data['title'] = $param;
							if($part2 != 'detail'){
								echo '<link href="'.base_url().'assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">';
								echo '<link href="'.base_url().'assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">';
								echo '<link href="'.base_url().'assets/admin/css/plugins/chosen/chosen.css" rel="stylesheet">';
								echo '<script src="'.base_url().'assets/admin/js/plugins/chosen/chosen.jquery.js"></script>
								<script>
								var config = {
									".chosen-select"           : {},
									".chosen-select-deselect"  : {allow_single_deselect:true},
									".chosen-select-no-single" : {disable_search_threshold:10},
									".chosen-select-no-results": {no_results_text:"Oops, nothing found!"},
									".chosen-select-width"     : {width:"95%"}
								}
								for (var selector in config) {
									$(selector).chosen(config[selector]);
								}
								</script>
								';
								if($this->user_type == 1){//cek user to receive the message
									$this->load->model('Messages_model');
									if($this->simo != 0){
										$receivers = $this->Messages_model->get_all_user_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 2
									}else{
										$receivers = $this->Messages_model->get_all_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 1
									}
									
									$data['receiver'] = $receivers;
								}elseif($this->user_type == 2){//berarti ini user lgsg ke admin
									$this->load->model('Messages_model');
									$data['receiver'] = $this->Messages_model->get_all_admin($this->zone_id,$this->user_id,$this->segretis); 
								}else{//berarti ini admin
									$data['receiver'] = $this->dbfun_model->get_all_data_bys(''.$column.' as id, name',''.$column.' != '.$admin_id.' and ccs_id = '.$this->zone_id.' ',$table);
								}
							}
							if($part2 == 'compose'){
								$view = $this->load->view('ecom/messages_compose', $data, TRUE);
							}else{
								$id = $this->input->post('id');
								$data['messages'] = $this->dbfun_model->get_data_bys('msg.message_id, msg.parent_id,msg.ts_id,msg.tr_id, msg.status, msg.subject, msg.message, msg.datecreated, msg.read_status, msg.sender as id, msg.receiver as idr','msg.message_id = '.$id.' and ccs_msg.ccs_id = '.$this->zone_id.' ','ccs_msg');
								if(($data['messages']->tr_id != 0) && ($data['messages']->ts_id != 0)){ //penerima bukan admin dan pengirim bukan admin
									$data['r_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->tr_id.'','umr')->name);
									$data['s_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->ts_id.'','umr')->name);
									$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->id.' and type_id = '.$data['messages']->ts_id.'','ccs_usr')->name;
								}elseif(($data['messages']->tr_id == 0) && ($data['messages']->ts_id != 0)){ //penerima admin dan pengirim bukan admin
									$data['r_type'] = 'Admin';
									$data['s_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->ts_id.'','umr')->name);
									$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->id.' and type_id = '.$data['messages']->ts_id.'','ccs_usr')->name;
								}elseif(($data['messages']->tr_id != 0) && ($data['messages']->ts_id == 0)){ //penerima bukan admin dan pengirim admin
									$data['r_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->tr_id.'','umr')->name);
									$data['s_type'] = 'Admin';
									$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->id.'','ccs_adm')->name;
								}else{ //penerima admin dan pengirim admin
									$data['r_type'] = 'Admin';
									$data['s_type'] = 'Admin';
									$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->id.'','ccs_adm')->name;
								}
								if($part2 == 'detail'){
									if(($data['messages']->read_status == 3) && ($data['messages']->id != $admin_id)){
										$this->dbfun_model->update_table(array('message_id' => $id), array('read_status' => 2), 'ccs_msg');
									}
									if($param == 'drafts'){
										if(isset($this->simo)){//cek user to receive the message
											$this->load->model('Messages_model');
											if($this->simo != 0){
												$receivers = $this->Messages_model->get_all_user_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 2
											}else{
												$receivers = $this->Messages_model->get_all_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 1
											}
											
											$data['receiver'] = $receivers;
										}else{//berarti ini admin
											$this->load->model('Messages_model');
											$data['receiver'] = $this->Messages_model->get_all_user_receiver_from_admin($this->zone_id,$this->admin_id);
										}
										echo '
										<script>
										$(document).ready(function(){
											$(".summernote").summernote();
											$(".summernote").code("'.str_replace('"',"'", trim($data['messages']->message)).'");
											$("#message_id").val("'.$data['messages']->message_id.'");
											$("#subject").val("'.$data['messages']->subject.'");
											$("#receiver").chosen().val('.$data['messages']->idr.');
											$("#receiver").trigger("chosen:updated");
										});
										</script>';
										$view = $this->load->view('ecom/messages_update', $data, TRUE);
										
									}else{
										if(($data['messages']->tr_id != 0) && ($data['messages']->ts_id != 0)){ //penerima bukan admin dan pengirim bukan admin
											$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->idr.' and type_id = '.$data['messages']->tr_id.'','ccs_usr')->name;
										}elseif(($data['messages']->tr_id == 0) && ($data['messages']->ts_id != 0)){ //penerima admin dan pengirim bukan admin
											$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->idr.'','ccs_adm')->name;
										}elseif(($data['messages']->tr_id != 0) && ($data['messages']->ts_id == 0)){ //penerima bukan admin dan pengirim admin
											$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->idr.' and type_id = '.$data['messages']->tr_id.'','ccs_usr')->name;
										}else{ //penerima admin dan pengirim admin
											$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->idr.'','ccs_adm')->name;
										}
										$view = $this->load->view('ecom/messages_detail', $data, TRUE);
									}
								}elseif($part2 == 'reply'){
									echo '
									<script>
									$(document).ready(function(){
										$(".summernote").summernote();
										$(".summernote").code("'.str_replace('"',"'", '<br><div class="icon-reply reply"><hr><small>'.date('Y-m-d', strtotime($data['messages']->datecreated)).'</small><br><p>'.$data['sender'].':</p> <br><i style="color: #aaa;">'.trim($data['messages']->message)).'</i></div>'.'");
										$("#receiver").chosen().val('.$data['messages']->id.');
										$("#receiver").trigger("chosen:updated");
									});
									</script>';
									$view = $this->load->view('ecom/messages_reply', $data, TRUE);
								}elseif($part2 == 'forward'){
									echo '<script>
									$(document).ready(function(){
										$(".summernote").summernote();
										$(".summernote").code("'.str_replace('"',"'", '<br><div class="forward"><hr>---Forwarded Message---<br><small>'.date('Y-m-d', strtotime($data['messages']->datecreated)).'</small><br><p>'.$data['sender'].':</p> <br><i style="color: #aaa;">'.trim($data['messages']->message)).'</i></div>'.'");
										$("#receiver").val();
									});</script>';
									$view = $this->load->view('ecom/messages_reply', $data, TRUE);
								}
							}
						}
					}
					echo $view;
				}else{
					redirect(base_url($zone));
				}
			}elseif($type == 'cart'){
				$data = array();
				$active_page = 1;
				$this->form_validation->set_rules('id','id page', 'trim|xss_clean');
				$part = $this->input->post('param');
				if($this->zone_type_id == 3){
					echo header('Content-Type: text/html; charset=ISO-8859-1');
					$this->load->library('cart');
					if($this->form_validation->run() == TRUE ){
						$id = $this->input->post('id');
						$cat = $this->dbfun_model->get_data_bys('category_id,ccs_key,tu', array('ccs_id' => $this->zone_id, 'object_id' => $id), 'odc');
						if(in_array($part,array('add','remove','ticket','checkout'),true)){
							if($part == 'add'){
								$data_product = $this->dbfun_model->get_data_bys('image_square as img, istatus', array('ccs_id' => $this->zone_id, 'object_id' => $id), 'o');
								if($data_product->istatus != 0){
									$image = $this->get_object_gallery_main_image($id)->image_square;
									if(file_exists('./assets/'.$zone.'/gallery/thumbnail/'.trim($image))){
										$image = base_url('assets/'.$zone).'/gallery/thumbnail/'.$image;
									}else{
										$image = base_url('assets/'.$zone).'/gallery/thumbnail/'.str_replace('.','_thumb.',$image);
									}
								}else{
									$image = base_url('assets/'.$zone).'/product/'.$data_product->img;
								}
								$category = $this->category_id_to_category($cat->ccs_key,$cat->category_id);
								if($id != 'view'){
									$qty = $this->input->post('qty');
									$prc = $this->get_prc($cat->ccs_key,$cat->category_id,$id);
									$url = base_url($zone.'/product/'.strtolower(str_replace(' ','_',$category)).'/'.strtolower(str_replace(' ','_',$this->input->post('name'))));
									if($prc->vstatus == 1){
										$var_id = $this->input->post('variant');
										$variant = $this->get_ocs($var_id);
										$cek_qty = $this->dbfun_model->get_data_bys('stock','ccs_id = '.$this->zone_id.' and category_id = '.$cat->category_id.' and object_id = '.$id.' and settings_id = '.$var_id.' and ccs_prv.status = 1 and stock != 0','prv')->stock;
										if(strtolower($category) == 'member card'){
											$cek_qty = 1;
										}
										$options = array(
											'img' => $image,
											'variant' => $variant->title,
											'variant_id' => $var_id,
											'weight' => $cat->tu,
											'max_qty' => $cek_qty,
											'url'=> $url
										);
									}else{
										$cek_qty = $prc->stock;
										if(strtolower($category) == 'member card'){
											$cek_qty = 1;
										}
										$options = array(
											'img' => $image,
											'variant' => '',
											'variant_id' => '',
											'weight' => $cat->tu,
											'max_qty' => $cek_qty,
											'url'=> $url
										);
									}
									if($qty > $cek_qty){
										$qty = $cek_qty;
									}
									$data = array(
										'id'      => $id,
										'qty'     => $qty,
										'price'   => $this->input->post('price'),
										'name'    => $this->input->post('name'),
										'options' => $options
									);
									$this->cart->insert($data); 
								}
							}elseif($part == 'remove'){
								$data = array(
									'rowid'	=> $id,
									'qty'	=> 0
								);
								$this->cart->update($data); 
							}elseif($part == 'ticket'){
								$code = $this->input->post('ticket');
								if(!empty($code)){
									$cek_tkt = $this->dbfun_model->get_all_data_bys('object_id,oid,discount','ccs_id = '.$this->zone_id.' and LOWER(code) like "'.strtolower($code).'" and stock != 0 and status = 1','ccs_tkt');
									$cek_tkt2 = '';
									//$this->dbfun_model->get_data_bys('url_id,ccs_key,url_id,type_id,category_id,object_id,discount,publish','ccs_id = '.$this->zone_id.' and LOWER(number_cat) like "'.strtolower($code).'" and stock != 0 and status = 1','ccs_tkt');
									if(!empty($cek_tkt)){
										$datas = array();
										foreach($cek_tkt as $ds){
											$datas[] = $ds->oid;
											$dsc[$ds->oid] = $ds->discount;
										}
									}
									if(!empty($cek_tkt)){
										$cek_valid = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and object_id = '.$cek_tkt[0]->object_id.' and (CURDATE() between datecreated and dateupdated) and love != 0 and status = 1','ccs_o');
										if(!empty($cek_valid)){
											$cek = false;
											foreach($this->cart->contents() as $items):
												if(in_array($items['id'],$datas)){
													if($this->is_oprc_is_nprc($items['id'],$items['options']['variant_id'],$items['price'])){
														if(!empty($dsc[$items['id']])){
														$dsc = $dsc[$items['id']];
														}else{
															$dsc = 0;
														}
														$data = array(
															'rowid'	=> $items['rowid'],
															'qty'	=> $items['qty'],
															'price'	=> $items['price']*(1 - ($dsc / 100))
														);
														$this->cart->update($data); 
														$cek = true;
														$cart['datas'][] = array('id'=>$items['rowid'],'price_new'=>$data['price']);
													}
												}
											endforeach;	
											if($cek === true){
												$cart['message'] = (object) array('success','Coupon "'.$code.'" can be used');
											}else{
												$cart['message'] = (object) array('error','This coupon can not be used or The product already discount');
											}
										}else{
											$cart['message'] = (object) array('error','This coupon doesnt exist anymore');
										}
									}else{
										$cart['message'] = (object) array('error','Coupon "'.$code.'" does not exist!');
									}
								}else{
									$cart['message'] = (object) array('error','No Coupon Added');
								}							
							}elseif($part == 'checkout'){
								$this->form_validation->set_rules('cartid[]', 'Cart ID', 'trim|required|xss_clean');
								$this->form_validation->set_rules('qty[]', 'Quantity', 'trim|required|is_natural_no_zero|xss_clean');
								$this->form_validation->set_rules('coupon', 'Coupon', 'trim|max_length[255]|xss_clean');
								$status = 'error';
								if ($this->form_validation->run() === TRUE)
								{
									$data_cart = array();
									foreach ($_POST['cartid'] as $key => $value) {
										$data_cart[$key] = array('rowid' => $value, 'qty' => $_POST['qty'][$key]);
									}
									$this->cart->update($data_cart);
									$status = 'success';
								}

								echo json_encode(array('s' => $status));
							}
							if($part != 'ticket'){
								$cart = array(
									'total_item' => $this->cart->total_items(),
									'total_price'=>'IDR '.number_format($this->cart->total(),2,',','.'),
									'list_item' => $this->cart->contents()
								);
							}else{
								$cart = (object) array_merge((array) $cart, array(
									'total_item' => $this->cart->total_items(),
									'total_price'=>'IDR '.number_format($this->cart->total(),2,',','.'),
									'list_item' => $this->cart->contents()
								));
							}
						}elseif(in_array($part,array('wishlist'),true)){
							if(!empty($cat->ccs_key) && !empty($id)){
								$where = array('url_id' => $id,'user_id'=>$this->user_id,'ccs_id'=>$this->zone_id,'ccs_key'=>$cat->ccs_key);
								$cek_data = $this->dbfun_model->get_data_bys('type',$where,'ulg');
								if(empty($cek_data)){
									$type_remove = 5;
									$type = 'add';
									$data = array(
										'ccs_id' => $this->zone_id,
										'ccs_key' => $cat->ccs_key,
										'user_id' => $this->user_id,
										'url_id' => $id,
										'language_id'=> $this->current_language,
										'type'=> $type_remove,
									);
								}else{
									if($cek_data->type == 5){
										$type_remove = 6;
										$type = 'remove';
									}elseif($cek_data->type == 6){
										$type_remove = 5;
										$type = 'add';
									}
									
									$data = array(
										'type'=> $type_remove,
									);
									$this->dbfun_model->update_table($where, $data, 'ulg');
								}
								
								$cart = array(
									'status' => 'success',
									'data' => (object) array(
												'id' => $id,
												'ccs_key' => $cat->ccs_key,
												'type'=>$type
											),
									'is_login'=>TRUE
								);
							}else{
								$cart = array(
									'status' => 'error',
									'data' => '',
									'is_login'=>TRUE
								);
							}
						}
						if($part != 'checkout'){
							echo json_encode($cart);
						}
					}
				}
			}elseif($type == 'creates'){
				$part2 = $this->input->post('param2');
				if($part2 == 'messages'){
					$id = $this->input->post('message_id');
					$lang = 2;
					$admin_id = $this->user_id;
					$type_id = $this->user_type;
						if($id) //update
						{
							//if still draft or save as draft
							if ($this->form_validation->run('ccs_msg') === TRUE)
							{
								$status = 'success';
								$message = 'No data updated';
								$update = $this->update_data_bys('*', array('message_id' => $id), 'msg','','','','');
								$param = $this->input->post('param');
								if($param == 'draft'){
									$update['ts_id'] = $type_id;
									$update['ccs_id'] = $this->zone_id;
									$update['status'] = 0;
									$status = 'success';
									$message = 'Draft updated';
								}elseif($param == 'sent'){
									$update['ts_id'] = $type_id;
									$update['ccs_id'] = $this->zone_id;
									$update['status'] = 1;
									$update['read_status'] = 3;
									$update['datecreated'] = date('Y-m-d H:i:s');
									$status = 'success';
									$message = 'Message sent';
								}
								if($update) 
								{
									if($lang)
									{
										$this->dbfun_model->update_table(array('message_id' => $id,'language_id' => $lang), $update, 'msg');
									}
									else
									{
										$this->dbfun_model->update_table(array('message_id' => $id), $update, 'msg');
									}
								}
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}
						else  // create
						{
							if ($this->form_validation->run('ccs_msg') === TRUE)
							{
								if($this->input->post('param') == 'draft'){
									$custom_primary = array(
										'ts_id' => $type_id,
										'ccs_id' => $this->zone_id,
										'language_id' => 2,
										'sender' => $admin_id,
										'createdby' => $admin_id,
										'message' => str_replace('icon-reply reply',' ',$this->input->post('message')),
										'read_status' => 0,
										'status' => 0
									);	
								}else{
									$custom_primary = array(
										'ts_id' => $type_id,
										'ccs_id' => $this->zone_id,
										'language_id' => 2,
										'sender' => $admin_id,
										'createdby' => $admin_id,
										'message' => str_replace('icon-reply reply',' ',$this->input->post('message')),
										'read_status' => 3,
										'status' => 1
									);	
								}
								
								$this->new_data('ccs_msg',$custom_primary, '','', '','');
								$status = 'success';
								$message = 'Message sent';
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}
						
					echo json_encode(array('status' => $status, 'm' => $message));
				}elseif($part2 == 'edit_profile'){
					if($this->s_login){//if login
						$user_id = $this->user_id;
						$old_pass = $this->input->post('old');
						$cek_old = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$user_id.'','usr');
						$cek_data = FALSE;
						$cek_pass = FALSE;
						if(!empty($old_pass)){
							$cek_pass = FALSE;
							$this->form_validation->set_rules('old', 'Old Password', 'trim|required|xss_clean');
							$this->form_validation->set_rules('p', 'Password', 'trim|required|xss_clean');
							$this->form_validation->set_rules('p2', 'Password', 'trim|required|xss_clean');
							if ($this->form_validation->run() === TRUE)
							{
								if($cek_old->password == md5($old_pass)){
									if($this->input->post('p') == $this->input->post('p2')){
										$cek_pass = TRUE;
									}else{
										$status = 'error';
										$message = "Password Doesn't match";
									}
								}else{
									$status = 'error';
									$message = 'Wrong Old Password, Try Again';
								}
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}else{
							$cek_data = FALSE;
							foreach($_POST as $key => $val)  
							{
								$data1[$key] = $this->input->post($key); 
							}
							$data2 = get_object_vars($cek_old);
							$datas = array_intersect_key(array_diff_assoc($data1, $data2),$data2);
							if(!empty($datas)){
								$cek_data = TRUE;
							}
						}
						if($cek_pass == TRUE){
							$custom_usr = array(
								'name'=> $this->input->post('name'),
								'email'=> $this->input->post('email'),
								'phone'=> $this->input->post('phone'),
								'mobile'=> $this->input->post('mobile'),
								'password' => md5($this->input->post('p')),
								'zip'=> $this->input->post('zip'),
								'address'=> $this->input->post('address'),
								'dateupdated'=> date('Y-m-d H:i:s'),
							);
						}elseif($cek_data){
							$custom_usr = $datas;
						}
						if(!empty($custom_usr)){
							$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and user_id = '.$user_id.'', $custom_usr, 'ccs_usr');
							$status = 'success';
							$message = "Profile Updated";
						}else{
							$status = 'error';
							$message = 'No Data Updated';
						}
						echo json_encode(array('status' => $status, 'm' => $message));
					}
				}else{
					redirect(base_url($zone));
				}
			}elseif($type == 'delete'){
				
			}
		}elseif($type == 'cart'){
			$data = array();
			$active_page = 1;
			$this->form_validation->set_rules('id','id page', 'trim|xss_clean');
			$part = $this->input->post('param');
			if($this->zone_type_id == 3){
				echo header('Content-Type: text/html; charset=ISO-8859-1');
				$this->load->library('cart');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$cat = $this->dbfun_model->get_data_bys('category_id,ccs_key,tu', array('ccs_id' => $this->zone_id, 'object_id' => $id), 'odc');
					if(in_array($part,array('add','remove','ticket','checkout'),true)){
						if($part == 'add'){
							$data_product = $this->dbfun_model->get_data_bys('image_square as img, istatus', array('ccs_id' => $this->zone_id, 'object_id' => $id), 'o');
							if($data_product->istatus != 0){
								$image = $this->get_object_gallery_main_image($id)->image_square;
								if(file_exists('./assets/'.$zone.'/gallery/thumbnail/'.trim($image))){
									$image = base_url('assets/'.$zone).'/gallery/thumbnail/'.$image;
								}else{
									$image = base_url('assets/'.$zone).'/gallery/thumbnail/'.str_replace('.','_thumb.',$image);
								}
							}else{
								$image = base_url('assets/'.$zone).'/product/'.$data_product->img;
							}
							$category = $this->category_id_to_category($cat->ccs_key,$cat->category_id);
							if($id != 'view'){
								$qty = $this->input->post('qty');
								$prc = $this->get_prc($cat->ccs_key,$cat->category_id,$id);
								if($this->is_active_domain){
									$url = base_url('product/'.strtolower(str_replace(' ','_',$category)).'/'.strtolower(str_replace(' ','_',$this->input->post('name'))));
								}else{
									$url = base_url($zone.'/product/'.strtolower(str_replace(' ','_',$category)).'/'.strtolower(str_replace(' ','_',$this->input->post('name'))));
								}
								if($prc->vstatus == 1){
									$var_id = $this->input->post('variant');
									$variant = $this->get_ocs($var_id);
									$cek_qty = $this->dbfun_model->get_data_bys('stock','ccs_id = '.$this->zone_id.' and category_id = '.$cat->category_id.' and object_id = '.$id.' and settings_id = '.$var_id.' and ccs_prv.status = 1 and stock != 0','prv')->stock;
									if(strtolower($category) == 'member card'){
										$cek_qty = 1;
									}
									$options = array(
										'img' => $image,
										'variant' => $variant->title,
										'variant_id' => $var_id,
										'weight' => $cat->tu,
										'max_qty' => $cek_qty,
										'url'=> $url
									);
								}else{
									$cek_qty = $prc->stock;
									if(strtolower($category) == 'member card'){
										$cek_qty = 1;
									}
									$options = array(
										'img' => $image,
										'variant' => '',
										'variant_id' => '',
										'weight' => $cat->tu,
										'max_qty' => $cek_qty,
										'url'=> $url
									);
								}
								if($qty > $cek_qty){
									$qty = $cek_qty;
								}
								$data = array(
									'id'      => $id,
									'qty'     => $qty,
									'price'   => $this->input->post('price'),
									'name'    => $this->input->post('name'),
									'options' => $options
								);
								$this->cart->insert($data); 
							}
						}elseif($part == 'remove'){
							$data = array(
								'rowid'	=> $id,
								'qty'	=> 0
							);
							$this->cart->update($data); 
						}elseif($part == 'ticket'){
							$code = $this->input->post('ticket');
							if(!empty($code)){
								$cek_tkt = $this->dbfun_model->get_all_data_bys('object_id,oid,discount','ccs_id = '.$this->zone_id.' and LOWER(code) like "'.strtolower($code).'" and stock != 0 and status = 1','ccs_tkt');
								$cek_tkt2 = '';
								//$this->dbfun_model->get_data_bys('url_id,ccs_key,url_id,type_id,category_id,object_id,discount,publish','ccs_id = '.$this->zone_id.' and LOWER(number_cat) like "'.strtolower($code).'" and stock != 0 and status = 1','ccs_tkt');
								if(!empty($cek_tkt)){
									$datas = array();
									foreach($cek_tkt as $ds){
										$datas[] = $ds->oid;
										$dsc[$ds->oid] = $ds->discount;
									}
								}
								if(!empty($cek_tkt)){
									$cek_valid = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and object_id = '.$cek_tkt[0]->object_id.' and (CURDATE() between datecreated and dateupdated) and love != 0 and status = 1','ccs_o');
									if(!empty($cek_valid)){
										$cek = false;
										foreach($this->cart->contents() as $items):
											if(in_array($items['id'],$datas)){
												if($this->is_oprc_is_nprc($items['id'],$items['options']['variant_id'],$items['price'])){
													if(!empty($dsc[$items['id']])){
														$dsc = $dsc[$items['id']];
													}else{
														$dsc = 0;
													}
													$data = array(
														'rowid'	=> $items['rowid'],
														'qty'	=> $items['qty'],
														'price'	=> $items['price']*(1 - ($dsc / 100))
													);
													$this->cart->update($data); 
													$cek = true;
													$cart['datas'][] = array('id'=>$items['rowid'],'price_new'=>$data['price']);
												}
											}
										endforeach;	
										if($cek === true){
											$cart['message'] = (object) array('success','Coupon "'.$code.'" can be used');
										}else{
											$cart['message'] = (object) array('error','This coupon can not be used or The product already discount');
										}
									}else{
										$cart['message'] = (object) array('error','This coupon doesnt exist anymore');
									}
								}else{
									$cart['message'] = (object) array('error','Coupon "'.$code.'" does not exist!');
								}
							}else{
								$cart['message'] = (object) array('error','No Coupon Added');
							}
						}elseif($part == 'checkout'){
							$this->form_validation->set_rules('cartid[]', 'Cart ID', 'trim|required|xss_clean');
							$this->form_validation->set_rules('qty[]', 'Quantity', 'trim|required|is_natural_no_zero|xss_clean');
							$this->form_validation->set_rules('coupon', 'Coupon', 'trim|max_length[255]|xss_clean');

							$status = 'error';
							
							if ($this->form_validation->run() === TRUE)
							{
								$data_cart = array();
								$code = $this->input->post('coupon');
								$cek_valid = '';
								$cek_tkt = $this->dbfun_model->get_all_data_bys('object_id,oid,discount','ccs_id = '.$this->zone_id.' and LOWER(code) like "'.strtolower($code).'" and stock != 0 and status = 1','ccs_tkt');
								if(!empty($cek_tkt)){
									$cek_valid = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and object_id = '.$cek_tkt[0]->object_id.' and (CURDATE() between datecreated and dateupdated) and love != 0 and status = 1','ccs_o');
									$datas = array();
									foreach($cek_tkt as $ds){
										$datas[] = $ds->oid;
										$dsc[$ds->oid] = $ds->discount;
									}
								}
								if(((!empty($cek_tkt)) && (!empty($code)))&& (!empty($cek_valid))){
									$i = 0;
									foreach($this->cart->contents() as $key => $items):
										$qty = $_POST['qty'][$i];
										if(in_array($items['id'],$datas,true)){
											if($this->is_oprc_is_nprc($items['id'],$items['options']['variant_id'],$items['price'])){
												if(!empty($dsc[$items['id']])){
													$dsc = $dsc[$items['id']];
												}else{
													$dsc = 0;
												}
												$options = array(
													'img' => $items['options']['img'],
													'variant' => $items['options']['variant'],
													'variant_id' => $items['options']['variant_id'],
													'weight' => $items['options']['weight'],
													'code' => $code,
													'max_qty' => $items['options']['max_qty'],
													'url' => $items['options']['url']
												);
												if(isset($items['options']['discount'])){
													$price = $items['price'];
												}else{
													$options['discount'] = $dsc;
													$price = $items['price']*(1 - ($dsc / 100));
												}
												$data[] = array(
													'id'      => $items['id'],
													'qty'     => $qty,
													'price'   => $price,
													'name'    => $items['name'],
													'options' => $options
												);
											}else{
												$options = array(
													'img' => $items['options']['img'],
													'variant' => $items['options']['variant'],
													'variant_id' => $items['options']['variant_id'],
													'weight' => $items['options']['weight'],
													'max_qty' => $items['options']['max_qty'],
													'url'=> $items['options']['url']
												);
												$data[] = array(
													'id'      => $items['id'],
													'qty'     => $qty,
													'price'   => $items['price'],
													'name'    => $items['name'],
													'options' => $options
												);
											}
										}else{
											$options = array(
												'img' => $items['options']['img'],
												'variant' => $items['options']['variant'],
												'variant_id' => $items['options']['variant_id'],
												'weight' => $items['options']['weight'],
												'max_qty' => $items['options']['max_qty'],
												'url'=> $items['options']['url']
											);
											$data[] = array(
												'id'      => $items['id'],
												'qty'     => $qty,
												'price'   => $items['price'],
												'name'    => $items['name'],
												'options' => $options
											);
										}
										$i++;
										
										$remove = array(
											'rowid'	=> $items['rowid'],
											'qty'	=> 0
										);
										$this->cart->update($remove);
									endforeach;
									$this->cart->insert($data);
								}else{
									foreach ($_POST['cartid'] as $key => $value) {
										$data_cart[$key] = array('rowid' => $value, 'qty' => $_POST['qty'][$key]);
									}
									$this->cart->update($data_cart);
								}
								$status = 'success';
							}
							echo json_encode(array('s' => $status));
						}
						if($part == 'checkout'){
							$cart = '';
						}elseif($part != 'ticket'){
							$cart = array(
								'total_item' => $this->cart->total_items(),
								'total_price'=>'IDR '.number_format($this->cart->total(),2,',','.'),
								'list_item' => $this->cart->contents()
							);
						}else{
							$cart = (object) array_merge((array) $cart, array(
								'total_item' => $this->cart->total_items(),
								'total_price'=>'IDR '.number_format($this->cart->total(),2,',','.'),
								'list_item' => $this->cart->contents()
							));
						}
					}elseif(in_array($part,array('wishlist','rewishlist'),true)){
						if(!empty($cat->ccs_key) && !empty($id)){
							$cart = array(
								'status' => 'success',
								'is_login' => FALSE
							);
						}else{
							$cart = array(
								'status' => 'error',
								'is_login' => FALSE
							);
						}
					}	
					if(!empty($cart)){
						echo json_encode($cart);
					}
				}
			}
		}elseif($type == 'create'){
			$datas = array();
			$this->form_validation->set_rules('emails','E-mail', 'trim|xss_clean|valid_email|required');
			$status = 'error';
			$message = '';
			if($this->form_validation->run() == TRUE ){
				$status = 'success';
				$message = 'success';
				
				$email = $this->input->post('emails');
				$admin_id = $this->dbfun_model->get_data_bys('admin_id','ccs_id = '.$this->zone_id.'','adm')->admin_id;
				$cek_email = $this->dbfun_model->get_data_bys('email', 'LOWER(email) like "'.strtolower($email).'" and ccs_id = '.$this->zone_id.'','usr');
				if(empty($cek_email)){
					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
					$activation_key = substr( str_shuffle( $chars ), 0, 10 );
					$custom_usr = array(
						'email' => $email,
						'status' => 1,
						'createdby' => $admin_id,
						'datecreated' => date('Y-m-d H:i:s'),
						'rw_id' => 0,
						'activation_key' => $activation_key,
						'ccs_id' => $this->zone_id,
					);
					$user_id = $this->new_data('ccs_usr',$custom_usr,'','','','');
				}else{
					$status = 'error';
					$message = ''.$email.' already exist';
				}
			}else{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message,'data' => $datas));
		}elseif($type == 'list'){
			$data = array();

			$this->form_validation->set_rules('id','id page', 'trim|xss_clean');
			$this->form_validation->set_rules('param','parameter page', 'trim|xss_clean');
			$this->form_validation->set_rules('sort', 'Sort', 'trim|required|xss_clean');
			$this->form_validation->set_rules('order', 'Order By', 'trim|required|xss_clean');
			$this->form_validation->set_rules('limit', 'Show Number', 'trim|required|is_natural_no_zero|less_than[101]|xss_clean');
			$this->form_validation->set_rules('offset', 'Offset', 'trim|required|is_natural|xss_clean');

			$datas4 = array();
			$numpage = 0;
			$active_page = 1;
			if($this->zone_type_id == 3){
				echo header('Content-Type: text/html; charset=ISO-8859-1');
				if($this->form_validation->run() == TRUE ){
					$module = $this->input->post('param');
					$id = $this->input->post('id');
					$sort = $this->input->post('sort');
					$order = $this->input->post('order');
					$limit = $this->input->post('limit');
					$offset = $this->input->post('offset');
					$range = $this->input->post('range');
					$q_range = FALSE;
					$tags = $this->input->post('tags');
					$q_tags = '';
					if(!empty($range)){
						$range = explode(',',$range);
						$q_range = TRUE;
					}
					if(!empty($tags)){
						$q_tags = 'and tags like "'.$tags.'"';
					}
					$start = $offset*$limit;
					$datas4 = array();

					$sorting = SORT_ASC;

					if ($sort == 'desc')
					{
						$sorting = SORT_DESC;
					}

					$ccs_key = $this->module_to_ccs_key($module);
					if(in_array($module,array('product','search'),true))
					{
						$module = 'product';
						if($this->input->post('type')){
							$module_type = $this->input->post('type');
						}else{
							$module_type = $module;
						}
						$ccs_key = $this->module_to_ccs_key($module_type);
						$q = $this->input->post('query');
						$this->load->library('cart');
						if($id == 'all'){
							$category_category_id = $this->dbfun_model->get_all_data_bys('category_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
						}
						else
						{
							$category_category_id = $this->dbfun_model->get_all_data_bys('category_id,title,parent_id','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and LOWER(ccs_oct.title) like "'.str_replace('_',' ',strtolower($id)).'" and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
							
							if($category_category_id[0]->parent_id == 0){
								$cek = $this->dbfun_model->get_all_data_bys('category_id,title,parent_id','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.parent_id = '.$category_category_id[0]->category_id.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
								$category_category_id = (object) array_merge((array)$category_category_id,(array)$cek);
							}
						}
						$datas2 = array();

						foreach($category_category_id as $key => $hp){
							$datas = array();
							if(empty($q)){
								$datas = $this->get_all_object_with_order_by_limit_custom($module_type,$hp->category_id,$this->current_language,$q_tags); 
							}else{
								
								$dq1 = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_odc.ccs_key,ccs_odc.category_id,ccs_o.istatus','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_odc.object_id = ccs_o.object_id and ccs_odc.ccs_id = '.$this->zone_id.' and ccs_odc.title like "%'.$q.'%" and ccs_odc.ccs_key = '.$ccs_key.' and ccs_odc.category_id = '.$hp->category_id.' and ccs_odc.language_id = '.$this->current_language.' and ccs_odc.status != 0 '.$q_tags.'','odc,o');
								$dq2 = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_odc.ccs_key,ccs_odc.category_id,ccs_o.istatus','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_odc.object_id = ccs_o.object_id and ccs_odc.ccs_id = '.$this->zone_id.' and ccs_odc.description like "%'.$q.'%" and ccs_odc.ccs_key = '.$ccs_key.' and ccs_odc.category_id = '.$hp->category_id.' and ccs_odc.language_id = '.$this->current_language.' and ccs_odc.status != 0 '.$q_tags.'','odc,o');
								$datas = array_unique(array_merge($dq1,$dq2),SORT_REGULAR);
							}
							
							$datas2[] = array('section'=>$hp,'datas'=>$datas);
						}
						
						if($this->cart->contents()){
							foreach($this->cart->contents() as $items):
								$data_add_to_cart[] = $items['id'];
								$data_add_to_cart_row_id[$items['id']] = $items['rowid'];
							endforeach;	
						}
						foreach($datas2 as $d){

							$datas3 = array();
							foreach($d['datas'] as $ds){
								if($ds->istatus != 0){
									$image = $this->get_object_gallery_main_image($ds->object_id)->image_square;
									$ds = (object) array_merge((array) $ds, array('image_square'=>base_url('assets/'.$zone).'/gallery/'.$image));
								}else{
									$ds = (object) array_merge((array) $ds, array('image_square'=>base_url('assets/'.$zone).'/'.$module_type.'/'.$ds->image_square));
								}
								$aw = (object) array_merge((array) $ds, (array) $this->get_prc($module_type,$d['section']->category_id,$ds->object_id));
								$aw = (object) array_merge((array) $ds, (array) $this->get_prc($module_type,$d['section']->category_id,$ds->object_id));
								if(($id == 'member_card') && $this->is_verified_member()){
									$aw = (object) array_merge((array) $aw, array('stock'=>0));
								}
								
								if(isset($aw->vstatus)){
									if($aw->vstatus == 1){
										$ds_variant = $this->get_variant($module_type,$d['section']->category_id,$aw->object_id);
										$variant = '';
										$variant1 = '';
										$price_max = 0;
										if(!empty($ds_variant)){
											$price_min = $ds_variant[0]->publish;
										}
										foreach($ds_variant as $dsv){
											$variant1 = (object) array_merge((array) $dsv, (array) $this->get_ocs($dsv->settings_id));
											if(!empty($ds)){
												$variant[] = (object) array_merge((array) $variant1, (array) $this->discount($ds->object_id,$variant1->varian_id,''));
											}else{
												$variant[] = $variant1;
											}
											/* if($this->s_login){
												if(!empty($data['datas'])){
													$variant[] = (object) array_merge((array) $variant1, (array) $this->discount($data['datas']->object_id,$variant1->varian_id,''));
												}else{
													$variant[] = $variant1;
												}
											}else{
												$variant[] = $variant1;
											} */
											if($dsv->publish > $price_max){//get max
												$price_max = $dsv->publish;
											}
											if($dsv->publish < $price_min){//get min
												$price_min = $dsv->publish;
											}
										}
										if(!empty($ds_variant)){
											if($price_min != $price_max){
												$price = str_replace('.',',',$price_min).' - '.str_replace('.',',',$price_max);
											}else{
												$price = str_replace('.',',',$price_min);
											}
											$aw = (object) array_merge((array) $aw, array('publish'=> $price,'discount'=> 0,'variant'=> $variant));
										}else{
											//out of stock
											$aw = (object) array_merge((array) $aw, array('discount'=> 0,'variant'=> $variant));
										}
										
									}else{
										$dsc = $this->discount($ds->object_id,'','');
										/* if($this->s_login){
											$dsc = $this->dbfun_model->get_data_bys('discount','ccs_id = '.$this->zone_id.' and oid = '.$ds->object_id.' and stock != 0 and status = 1','ccs_tkt');
										}else{
											$dsc = 0;
										}
										if(!empty($dsc)){
											$dsc = $dsc->discount;
										}else{
											$dsc = 0;
										} */
										$aw = (object) array_merge((array) $aw, array('discount'=> $dsc));
									}
								}
								
								if(isset($data_add_to_cart)){
									if(in_array($ds->object_id,$data_add_to_cart,true)){
										$datas3[] = (object) array_merge((array) $aw, array('cek'=>true, 'rowid'=>$data_add_to_cart_row_id[$ds->object_id]) );
									}else{
										$datas3[] = (object) array_merge((array) $aw, array('cek' =>false));
									}
								}else{
									$datas3[] = (object) array_merge((array) $aw, array('cek' =>false));
								}
							}

							if(!empty($datas3)){
								if($q_range){
									if(($datas3[0]->publish >= $range[0]) && ($datas3[0]->publish <= $range[1])){
										$datas4[] = array('section'=> $d['section'],'datas'=>$datas3);
									}
								}else{
									$datas4[] = array('section'=> $d['section'],'datas'=>$datas3);
								}
							}
						}
					}
					elseif(in_array($module,array('event','news','testimonial'),true)) //id = year, order = month
					{

						$active_page = $offset + 1;
						$category = '';
						if(!empty($id)){
							if(is_numeric($id)){
								$year = $id;
								$months = $this->input->post('month');
								if(!empty($months)){//month
									$months = $months;
									$year = date('Y');
								}
								
							}else{
								$year = '';
								$category = str_replace('_',' ',$id);
							}
						}else{
							$year = date('Y');
						}
						if(($this->zone == 'shisha') && ($module == 'event')){
							$category = $this->input->post('category');
						}
						if(!empty($order) && $order != '*'){
							$month = $order;
						}else{
							$month = date('n');
						}
						if($module == 'event'){
							$limit_num = 8;
							$limit = 'LIMIT '.$limit_num.'';
							if(!empty($offset)){
								$offset = 'OFFSET '.$offset.'';
							}else{
								$offset = 'OFFSET 0';
							}
						}else{
							$limit_num = 3;
							$limit = 'LIMIT '.$limit_num.'';
							if(!empty($offset)){
								$offset = 'OFFSET '.$offset*$limit_num.'';
							}else{
								$offset = 'OFFSET 0';
							}
						}
						$datas4 = array();
						if(!empty($category) && ($category != 'all')){
							$category_category_id = $this->dbfun_model->get_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and title like "'.$category.'" and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
							$datas = array();
							if(!empty($year)){
								$order = 'and YEAR(ccs_o.datecreated) = '.$year.' ORDER BY ccs_o.datecreated DESC';
								if(!empty($months)){
									$order = 'and '.$months.' between MONTH(ccs_o.datecreated) and MONTH(ccs_o.dateupdated) and YEAR(ccs_o.datecreated) = '.$year.' ORDER BY ccs_o.datecreated ASC';
								}elseif(strpos($category, 'week') !== false){
									$order = 'and YEAR(ccs_o.datecreated) = '.$year.' ORDER BY ccs_o.datecreated ASC';
								}
							}else{
								$order = 'ORDER BY ccs_o.datecreated DESC';
							}
							/* if(strpos($hp->title,'month') !== false){
								$order = 'and YEAR(ccs_o.datecreated) = '.$year.' ORDER BY ccs_o.datecreated DESC';
							}else{
								$order = 'and YEAR(ccs_o.datecreated) = '.$year.' and MONTH(ccs_o.datecreated) = '.$month.' ORDER BY ccs_o.datecreated DESC';
							} */
							if($category_category_id->parent_id != 0){
								$datas = $this->get_all_object_from_module_with_order_by_limit($module,$category_category_id->category_id,$this->current_language,$order,$limit,$offset); 
								//$this->get_all_object_from_module_with_order_by_limit_no_cat_pagination($module,$this->current_language,$order,$limit,$offset); 
								if(!empty($datas)){
									$datas4[] = array('section'=>$category_category_id,'datas'=>$datas);
								}
								$total = count($this->get_all_object_with_order_by_limit($module,$this->module_to_category_id($module,$category),$this->current_language));
							}else{
								$total = '';
								$ceks = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and parent_id = '.$category_category_id->category_id.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
								$category_category_id = (object) array_merge(array('0'=>$category_category_id),(array)$ceks);
								foreach($category_category_id as $hp){
									$datas = array();
									$datas = $this->get_all_object_from_module_with_order_by_limit($module,$hp->category_id,$this->current_language,$order,$limit,$offset);
									if(!empty($datas)){
										$datas4[] = array('section'=>$hp,'datas'=>$datas);
									}
									$total += count($this->get_all_object_with_order_by_limit($module,$this->module_to_category_id($module,strtolower($hp->title)),$this->current_language));
								}
							}
							
						}else{
							$category_category_id = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
							foreach($category_category_id as $key => $hp){
								$datas = array();
								if(!empty($year)){
									$order = 'and YEAR(ccs_o.datecreated) = '.$year.' ORDER BY ccs_o.datecreated DESC';
								}else{
									$order = 'ORDER BY ccs_o.datecreated DESC';
								}
								/* if(strpos($hp->title,'month') !== false){
									$order = 'and YEAR(ccs_o.datecreated) = '.$year.' ORDER BY ccs_o.datecreated DESC';
								}else{
									$order = 'and YEAR(ccs_o.datecreated) = '.$year.' and MONTH(ccs_o.datecreated) = '.$month.' ORDER BY ccs_o.datecreated DESC';
								} */
								$datas = $this->get_all_object_from_module_with_order_by_limit($module,$hp->category_id,$this->current_language,$order,$limit,$offset); 
								if(!empty($datas)){
									$datas4[] = array('section'=>$hp,'datas'=>$datas);
								}
							}
							$total = count($this->get_all_object_from_module($module,$this->current_language));
						}
						$numpage = ceil($total/$limit_num);
					}
					if(in_array($module,array('product','search'),true))
					{
						$datas5 = array();
						foreach ($datas4 as $key => $row)
						{
							switch ($order) {
								case 'title':
									$datas5[$key] = $row['datas'][0]->title;
									break;
								case 'publish':
									$datas5[$key] = $row['datas'][0]->publish;
									break;
								case 'discount':
									$datas5[$key] = $row['datas'][0]->discount;
									break;
								case 'date':
									$datas5[$key] = $row['datas'][0]->datecreated;
									break;
								default:
									break;
							}
						}
						array_multisort($datas5, $sorting, $datas4);
						$total = count($datas4);
						$numpage = ceil($total/$limit);
						$datas4 = array_slice($datas4, $start, $limit, false);
					}
					elseif(in_array($module,array('event','news','testimonial'),true))
					{
						/* $datas5 = array();
						foreach ($datas4 as $key => $row)
						{
							switch ($order) {
								case 'year':
									$datas5[$key] = $row['datas'][0]->title;
									break;
								case 'month':
									$datas5[$key] = $row['datas'][0]->publish;
									break;
								default:
									break;
							}
						} */
					}
				}
				echo json_encode(array('data' => $datas4, 'numpage' => $numpage, 'ap' => $active_page,'total'=>$total));
			}
		}elseif($type == 'checkout'){
			if($this->zone_type_id == 3){
				$this->load->library('cart');
				$this->form_validation->set_rules('name','Full Name', 'trim|xss_clean|required');
				$this->form_validation->set_rules('expedisi','Shipping', 'trim|xss_clean|required');
				$this->form_validation->set_rules('address','Address', 'trim|xss_clean|required');
				$this->form_validation->set_rules('loc_id','Town / City', 'trim|xss_clean|required');
				$this->form_validation->set_rules('email','E-mail', 'trim|xss_clean|valid_email|required');
				if($this->form_validation->run() == TRUE ){
					$cek_expedisi = $this->input->post('expedisi');
					if($cek_expedisi != 'undefined'){
						$email = $this->input->post('email');
						$cek_usr = $this->dbfun_model->get_data_bys('email,ccs_id,type_id,name,phone,mobile,address,user_id','ccs_id = '.$this->zone_id.' and LOWER(email) like "'.strtolower($email).'"','usr');
						$admin_id = $this->dbfun_model->get_data_bys('admin_id','ccs_id = '.$this->zone_id.'','adm')->admin_id;
						$cek_tri = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and LOWER(email) like "'.strtolower($email).'"','ccs_tri');
						
						$name = $this->input->post('name');
						$phone = $this->input->post('phone');
						$mobile = $this->input->post('mobile');
						$zip = $this->input->post('zip');
						$zip_2 = $this->input->post('zip_2');
						$phone = $this->input->post('phone');
						$phone = $this->input->post('phone');
						$city = $this->input->post('city');
						$city_2 = $this->input->post('city_2');
						$loc_id = $this->input->post('loc_id');
						$loc_id2 = $this->input->post('loc_id2');
						
						$now = date('Y-m-d H:i:s');
						
						$address_1 = $this->input->post('address').'<br>'.$city.'<br>'.'KODE POS: '.$zip;
						
						if(!empty($loc_id2)){//cek shipment
							$address_2 = $this->input->post('address_2').'<br>'.$city_2.'<br>'.'KODE POS: '.$zip_2;
						}else{//brrti billing sama dengan shipment, shipment is address_2
							$address_2 = $address_1;
						}
						
						if(empty($zip_2)){
							$zip_2 = $zip;
						}
						
						$cek_error = array();
						$cek_error['address'] = $address_2;
						
						$custom_usr_update = array(
							'name'=> $name,
							'email'=> strtolower(trim($email)),
							'phone'=> $phone,
							'mobile'=> $mobile,
							'zip'=> $zip,
							'loc_id'=> $loc_id,
							'address'=> $this->input->post('address'),
							'dateupdated'=> $now,
						);
						
						if($this->zone == 'shisha'){
							if($this->s_login && $this->user_reg_id && empty($cek_usr)){
								$user_id = $this->user_id;
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and user_id = '.$user_id.'', $custom_usr_update, 'ccs_usr');
								$cek_error['cek_usr'] = 'empty';
								$cek_tri = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = "'.$user_id.'"','ccs_tri');
							}elseif($this->s_login && $this->user_reg_id && !empty($cek_usr)){
								$user_id = $cek_usr->user_id;
								$cek_email_this_user = $this->dbfun_model->get_data_bys('email,ccs_id,type_id,name,phone,mobile,address,user_id','ccs_id = '.$this->zone_id.' and user_id = '.$this->user_id.'','usr');
								
								if(!empty($cek_email_this_user)){
									if(!(strtolower(trim($cek_email_this_user->email)) == strtolower(trim($cek_usr->email)))){
										$status = 'error';
										$message = 'the email has been taken by other user, please try another one';
										echo json_encode(array('status' => $status, 'm' => $message));
										return;
									}else{
										$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and user_id = '.$user_id.'', $custom_usr_update, 'ccs_usr');
										$cek_error['cek_usr'] = 'not empty';
									}
								}
								$cek_tri = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = "'.$user_id.'"','ccs_tri');
							}
						}
						
						if(!empty($cek_usr)){//if not login but have same email
							$user_id = $cek_usr->user_id;
							$type_id = $cek_usr->type_id;
							if($this->s_login){//if login
								$custom_usr = array(
									'name'=> $this->input->post('name'),
									'phone'=> $this->input->post('phone'),
									'mobile'=> $this->input->post('mobile'),
									'zip'=> $this->input->post('zip'),
									'loc_id'=> $this->input->post('loc_id'),
									'address'=> $this->input->post('address'),
									'dateupdated'=> date('Y-m-d H:i:s'),
								);
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and user_id = '.$user_id.'', $custom_usr, 'ccs_usr');
							}
							if(!empty($this->input->post('username')) && !empty($this->input->post('password'))){
								//kalau email sama tapi mau daftar lg
							}
						}else{
							if(!empty($this->input->post('username')) && !empty($this->input->post('password'))){
								$custom_usr = array(
									'datecreated' => date('Y-m-d H:i:s'),
									'status' => 1,
									'createdby' => $admin_id,
									'type_id' => 2,
									'rt_id' => 0,
									'rw_id' => 0,
									'ccs_id' => $this->zone_id,
									'password' => MD5($this->input->post('password'))
								);
								$user_id = $this->new_data('ccs_usr',$custom_usr,'','','','');
							}else{
								$user_id = 0;
							}
							$type_id = 2;
						}
						
						if(!empty($this->input->post('loc_id'))){
							$cek_loc = $this->dbfun_model->get_data_bys('loc_id,parent_loc,counter','ccs_id = '.$this->zone_id.' and loc_id = '.$this->input->post('loc_id').'','ccs_loc');
							if(empty($cek_loc)){
								$this->load->library('rajaongkir');//api
								$loc = json_decode($this->rajaongkir->city('',$this->input->post('loc_id')));
								$cek_loc_province = $this->dbfun_model->get_data_bys('loc_id,counter','ccs_id = '.$this->zone_id.' and LOWER(title) like "'.strtolower($loc->province).'" and parent_loc = 0','ccs_loc');
								if(empty($cek_loc_province)){
									$cek_max_loc_id_n_add1 = $this->dbfun_model->get_data_bys('MAX(loc_id) as province','ccs_id = '.$this->zone_id.'','ccs_loc')->province + 1;
									$custom_loc2 = array(
										'datecreated' => date('Y-m-d H:i:s'),
										'status' => 1,
										'createdby' => $admin_id,
										'ccs_id' => $this->zone_id,
										'loc_id' => $cek_max_loc_id_n_add1,
										'parent_loc' => 0,
										'title' => $loc->province,
										'parent_title' => '',
										'country' => 'ID',
										'type' => 'Prov',
									);
									$this->new_data('ccs_loc',$custom_loc2,'','','','');
									$province_id = $cek_max_loc_id_n_add1;
								}else{
									$province_id = $cek_loc_province->loc_id;
								}
								if(strpos(strtolower($loc->type),'kabupaten') == TRUE){
									$loc_type = 'Kab';
								}else{
									$loc_type = $loc->type;
								}
								$custom_loc = array(
									'datecreated' => date('Y-m-d H:i:s'),
									'status' => 1,
									'createdby' => $admin_id,
									'ccs_id' => $this->zone_id,
									'loc_id' => $loc->city_id,
									'parent_loc' => $province_id,
									'title' => $loc->city_name,
									'parent_title' => $loc->province,
									'country' => 'ID',
									'type' => $loc_type,
								);
								$this->new_data('ccs_loc',$custom_loc,'','','','');
							}else{
								$custom_loc = array(
									'counter' => $cek_loc->counter + 1,
								);
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and loc_id = '.$this->input->post('loc_id').'', $custom_loc, 'ccs_loc');
								if($cek_loc->parent_loc != 0){
									$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and loc_id = '.$cek_loc->parent_loc.' and parent_loc = 0', $custom_loc, 'ccs_loc');
								}
							}
						}
						
						$custom_tri = array(
							'name'=> $name,
							'email'=> $email,
							'phone'=> $phone,
							'mobile'=> $mobile,
							'address'=> $address_1,
							'address_2'=> $address_2,
							'description'=> ''
						);
						
						if(empty($cek_tri)){//if not have transaction before
							$custom_tri['ccs_id'] = $this->zone_id;
							$custom_tri['ccs_key'] = $this->module_to_ccs_key('transactions');
							$custom_tri['type_id'] = $type_id;
							$custom_tri['user_id'] = $user_id;
							$custom_tri['datecreated'] = $now;
							$custom_tri['createdby'] = $admin_id;
							$custom_tri['status'] = 1;
							
							$info_id = $this->new_data('ccs_tri',$custom_tri,'','','','');	
							$cek_error['tri'] = 'create new';
						}else{//update if ever transaction before
							$custom_tri['dateupdated'] = $now;
							
							if($cek_tri->user_id == $user_id){
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and user_id = '.$user_id.'', $custom_tri, 'ccs_tri');
								$cek_error['tri_update'] = $custom_tri;
							}else if($cek_tri->user_id == 0){
								$custom_tri['user_id'] = $user_id;
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and LOWER(email) like "'.strtolower($email).'"', $custom_tri, 'ccs_tri');
								$cek_error['tri_update'] = 'user_id = 0';
							}else{
								$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and user_id = '.$user_id.'', $custom_tri, 'ccs_tri');
								$cek_error['tri_update'] = 'other';
							}
							$cek_error['tri'] = 'update';
							$info_id = $cek_tri->info_id;
						}
						//for trace only
						/* $cek_error['type_id'] = $type_id;
						$cek_error['info_id'] = $info_id;
						$cek_error['user_id'] = $user_id;
						echo json_encode(array('cek' => $cek_error));
						return; */
						//end
						
						$trx_code = date('ymdHis');
						$cek_trx = true;
						$cek_trx_v = true;
						$trx_settings = $this->dbfun_model->get_data_bys('char_2,char_3','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "interval" and status = 1','orc');
						if(!empty($trx_settings)){
							$datevalid = ''.$trx_settings->char_3.' '.$trx_settings->char_2.'';
						}else{
							$datevalid = '1 hours';
						}
						$code = 'TRXON';
						$expedisi_type = '';
						$expedisi_package = '';
						$expedisi_price = '';
						
						if(!empty($cek_expedisi)){
							$expedisi = explode('-',$cek_expedisi);
							$expedisi_type = strtoupper($expedisi[0]);
							$expedisi_package = strtoupper($expedisi[1]);
							$expedisi_price = $expedisi[2];
						}
						$i = 1;
						foreach($this->cart->contents() as $items):
							$qty = (int)$items['qty'];
							$where = 'ccs_id = '.$this->zone_id.' and object_id = '.$items['id'].'';
							$prc = $this->dbfun_model->get_data_bys('ccs_key,category_id,stock,sold,vstatus,dstatus,publish',$where,'prc');
							if(!empty($prc)){
								if($prc->vstatus == 1){//cek variant
									$prc_v = $this->dbfun_model->get_data_bys('settings_id,varian_id,publish,stock,sold','ccs_id = '.$this->zone_id.' and object_id = '.$items['id'].' and status = 1 and stock != 0 and settings_id = '.$items['options']['variant_id'].'','prv');
									$varian_id = $prc_v->varian_id;
									$where_dsc = 'ccs_id = '.$this->zone_id.' and oid = '.$items['id'].' and varian_id = '.$varian_id.' and stock != 0 and status = 1';
									
									if(!empty($prc_v)){//variant 
										if(isset($items['options']['code'])){
											$prc_dsc = $this->dbfun_model->get_data_bys('ticket_id,oid,discount,code,stock,sold,counter','ccs_id = '.$this->zone_id.' and oid = '.$items['id'].' and LOWER(code) like "'.$items['options']['code'].'" and varian_id = '.$varian_id.' and stock != 0 and status = 1','ccs_tkt');
										}else{
											$prc_dsc = false;
										}
										if($prc_dsc){//if have discount
											$ticket = $prc_dsc->code;
											$ticket_id = $prc_dsc->ticket_id;
											$price = ((100 - $prc_dsc->discount)*$prc_v->publish)/100;
											/* $stock_dsc = array(
												'stock' => $prc_dsc->stock - $qty,
												'sold' => $prc_dsc->sold + $qty,
												'counter' => $prc_dsc->counter +1,
											);
											$this->dbfun_model->update_table($where_dsc, $stock_dsc, 'ccs_tkt'); */
										}else{
											$ticket = '';
											$ticket_id = '';
											$price = $prc_v->publish;
										}
										$price = $items['price'];
										$cek_trx_v = true; 
									}else{//variant empty
										$cek_trx_v = false;
									}
								}else{//non variant
									if(isset($items['options']['code'])){
										$where_dsc = 'ccs_id = '.$this->zone_id.' and oid = '.$items['id'].' and varian_id = 0 and stock != 0 and LOWER(code) like "'.$items['options']['code'].'" and status = 1';
										$prc_dsc = $this->dbfun_model->get_data_bys('ticket_id,oid,discount,code,stock,sold,counter',$where_dsc,'ccs_tkt');
									}else{
										$prc_dsc = false;
									}
									if(!empty($prc_dsc)){//discount
										$ticket = $prc_dsc->code;
										$ticket_id = $prc_dsc->ticket_id;
										$price = ((100 - $prc_dsc->discount)*$prc->publish)/100;
										/* $stock_dsc = array(
												'stock' => $prc_dsc->stock - $qty,
												'sold' => $prc_dsc->sold + $qty,
												'counter' => $prc_dsc->counter +1,
											);
										$this->dbfun_model->update_table($where_dsc, $stock_dsc, 'ccs_tkt'); */
									}else{//not discount
										$ticket = '';
										$price = $prc->publish;
										$ticket_id = '';
									}
									$price = $items['price'];
									$varian_id = 0;
									$cek_trx = true;
								}
							}else{//stock prc empty
								$cek_trx = false;
							}
							if((($cek_trx === true)||($cek_trx_v === true))&&(!empty($prc))){
								$custom_trx = array(
										'ccs_id' => $this->zone_id,
										'ccs_key' => $this->module_to_ccs_key('transactions'),
										'category_id' => $this->category_to_category_id('online sales'),
										'cat_id' => $prc->category_id,
										'object_id' => $items['id'],
										'ticket_id' => $ticket_id,
										'varian_id' => $varian_id,
										'ticket' => $ticket,
										'code' => $code,
										'trx_code' => $trx_code,
										'price' => $price*$qty,
										'total' => $qty,
										'description' => $this->input->post('notes'),
										'datecreated' => date('Y-m-d H:i:s'),
										'datesales' => date('Y-m-d H:i:s'),
										'datevalid' => date("Y-m-d H:i:s", strtotime('+'.$datevalid.'')),
										'user_id' => $user_id,
										'info_id' => $info_id,
										'createdby' => $admin_id,
										'status' => 0,
										'cstatus' => 0,
									);
								$this->new_data('ccs_trx',$custom_trx,'','','','');
								if($i == 1){
									$custom_inv = array(
											'ccs_id' => $this->zone_id,
											'ccs_key' => $this->module_to_ccs_key('transactions'),
											'type' => 0,
											'code' => $code,
											'icode' => 'INV',
											'inv_code' => $trx_code,
											'trx_code' => $trx_code,
											'description' => $this->input->post('notes'),
											'dateinv' => date('Y-m-d H:i:s'),
											'datecreated' => date('Y-m-d H:i:s'),
											'createdby' => $admin_id,
											'status' => 0,
										);
									$this->new_data('ccs_inv',$custom_inv,'','','','');
									$i++;
								} 
								
								/* if($prc->vstatus == 1){//if have valid
									$stock = array(
										'stock' => $prc_v->stock - $qty,
										'sold' => $prc_v->sold + $qty,
									);
									$this->dbfun_model->update_table('ccs_id = '.$this->zone_id.' and object_id = '.$items['id'].' and status = 1 and stock != 0 and settings_id = '.$items['options']['variant_id'].'', $stock, 'ccs_prv');
								}else{
									$stock = array(
										'stock' => $prc->stock - $qty,
										'sold' => $prc->sold + $qty,
									);
									$this->dbfun_model->update_table($where, $stock, 'ccs_prc');
								}  */
								
								$status = 'success';
								$message = 'berhasil';
							}else{
								$status = 'error';
								$message = 'maaf stock habis';
							}
						endforeach;	
						if(!empty($expedisi_price)){
							$custom_trx2 = array(
									'ccs_id' => $this->zone_id,
									'ccs_key' => $this->module_to_ccs_key('transactions'),
									'category_id' => $this->category_to_category_id('online sales'),
									'cat_id' => 0,
									'object_id' => 0,
									'ticket_id' => 0,
									'varian_id' => 0,
									'ticket' => $expedisi_type.' '.$expedisi_package,
									'code' => $code,
									'trx_code' => $trx_code,
									'price' => $expedisi_price,
									'total' => 1,
									'description' => $this->input->post('notes'),
									'datecreated' => date('Y-m-d H:i:s'),
									'datesales' => date('Y-m-d H:i:s'),
									'datevalid' => date("Y-m-d H:i:s", strtotime('+'.$datevalid.'')),
									'user_id' => $user_id,
									'info_id' => $info_id,
									'createdby' => $admin_id,
									'status' => 0,
									'cstatus' => 0,
								);
							$this->new_data('ccs_trx',$custom_trx2,'','','','');
						}
						$datas2 = '';
						$bank = '';
						$total_trx = '';
						$customer = '';
						if($status == 'success'){ 
							$list = $this->dbfun_model->get_all_data_bys('trx_code,cat_id,object_id,datecreated,datevalid,total,price,varian_id','ccs_id = '.$this->zone_id.' and trx_code = '.$trx_code.'','ccs_trx');
							$count_prc = '';
							$total = '';
							foreach($list as $l){
								$o = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','o');
								$odc = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','odc');
								$cat = $this->dbfun_model->get_data_bys('title','ccs_oct.ccs_id = '.$this->zone_id.' and status != 0 and category_id = '.$l->cat_id.'','oct');
								if($l->varian_id != 0){
									$settings_id = $this->dbfun_model->get_data_bys('settings_id','ccs_prv.ccs_id = '.$this->zone_id.' and ccs_prv.object_id = '.$l->object_id.' and ccs_prv.varian_id = '.$l->varian_id.'','prv')->settings_id;
									$varian = $this->dbfun_model->get_data_bys('ccs_ocs.title','ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.settings_id = '.$settings_id.'','ocs,oct');
									if(!empty($varian)){
										$varian = $varian->title;
									}else{
										$varian = '';
									}
								}else{
									$varian = '';
								}
								if(!empty($l->object_id != 0)){//expedisi not listed here
									$count_prc += $l->price;
									$total += $l->total;
									$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>$odc->title,'image'=>base_url('assets/'.$zone.'/product/'.$o->image_square)));
								}
							}
							$total_trx = array(
								'total'=>$total,
								'count_prc'=> $count_prc,
								'date'=> date('l, d F Y H:i:s',strtotime($list[0]->datecreated)),
								'date_valid'=> date('l, d F Y H:i:s',strtotime($list[0]->datevalid)),
							);
							$bank = $this->dbfun_model->get_all_data_bys('char_1,char_2,char_3','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "bank" and status = 1','orc');
							$customer = array(
								'name' => $name,
								'phone' => $phone,
								'mobile' => $mobile,
								'email' => $email,
								'zip' => $zip,
								'zip_2' => $zip_2,
								'address' => $this->input->post('address'),
								'address_2' => $this->input->post('address_2'),
								'valid' => $datevalid,
								'uid' => $user_id,
								'iid' => $info_id,
							);
							$subject = '['.strtoupper($this->zone).'] Success Order';
							$from = $this->dbfun_model->get_all_data_bys('url_id,char_1,char_2,char_3,char_4,char_5,char_6,char_7','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "info" and status = 1','orc');
							$content = array(
									'type'=> 'order',
									'logo'=> $this->menu['logo'],
									'data'=> $datas2,
									'bank'=> $bank,
									'zone'=> $this->zone,
									'total_trx'=> $total_trx,
									'customer'=> $customer,
									'from'=> $from,
									'link_to_confirm'=> $this->menu['link'].'/confirm?'.$this->get_hash($info_id.'-INV'.$trx_code,'en'),
									'style'=> $this->menu['primaryc'],
									'expedisi'=> array('price'=>$expedisi_price,'type'=>$expedisi_type, 'package'=>$expedisi_package),
								);
							$notes = 'Make your payment directly into our account. Please use your <strong>INVOICE ID as the payment reference</strong>. Your order wont be shipped until the funds have cleared in our account.';
							$content_pdf = array(
									'm_status'=> '',
									'logo'=> $this->menu['logo'],
									'zone'=> $this->zone,
									'data'=> $datas2,
									'bank'=> $bank,
									'total_trx'=> $total_trx,
									'customer'=> $customer,
									'from'=> $from,
									'style'=> $this->menu['primaryc'],
									'expedisi'=> array('price'=>$expedisi_price,'type'=>$expedisi_type.' '.$expedisi_package),
									'notes' => $notes
								);
							if (!is_dir('assets/'.$this->zone.'/trx/'))
							{
								mkdir('./assets/'.$this->zone.'/trx/', 0777, true);
							}
							if (!is_dir('assets/'.$this->zone.'/trx/inv/'))
							{
								mkdir('./assets/'.$this->zone.'/trx/inv/', 0777, true);
							}
							$file_name = 'file-'.date('YmdHis').'.pdf';
							$path = './assets/'.$this->zone.'/trx/inv/'.$file_name;
							$content_pdf['code'][] = '#INV'.$trx_code;
							$this->create_pdf($path,$content_pdf);
							
							if($this->zone == 'shisha'){
								$this->send_mail_attach($email,$subject,$content,$path,'ecom/email/checkout_success_simple');
							}else{
								$this->send_mail_attach($email,$subject,$content,$path,'ecom/email/checkout_success');
							}
							$this->cart->destroy();
						}
					}else{
						$datas2 = '';
						$bank = '';
						$total_trx = '';
						$customer = '';
						$status = 'error';
						$message = 'No Shipping to the Town / City';
						$expedisi_price = '';
						$expedisi_type = '';
						$expedisi_package = '';
					}
				}else{
					$datas2 = '';
					$bank = '';
					$total_trx = '';
					$customer = '';
					$status = 'error';
					$message = validation_errors();
					$expedisi_price = '';
					$expedisi_type = '';
					$expedisi_package = '';
				}
				echo json_encode(array('status' => $status, 'm' => $message,'data' => $datas2,'bank'=>$bank,'total_trx'=>$total_trx,'customer'=>$customer,'expedisi'=> array('price'=>$expedisi_price,'type'=>$expedisi_type, 'package'=>$expedisi_package)));
			}
		}elseif($type == 'view'){
			echo header('Content-Type: text/html; charset=ISO-8859-1');
			$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
			$part = $this->input->post('param');
			$param2 = $this->input->post('param2');
			if($this->form_validation->run() == TRUE ){
				if($part == 'order_history'){
					if($param2 == 'confirmation'){
						$status = 'error';
						$file = 'file';
						$param3 = trim($this->input->post('param3'));
						$id = $this->input->post('id');
						if(empty($id)){
							$m = 'please choose invoice number';
						}elseif(!isset($_FILES[$file]) && (empty($param3))){
							$m = 'no file added';
						}else{
							$table = 'ccs_inv';
							$code = 'TRXON';
							$code2 = 'CNF';
							$path = $this->zone.'/trx/cnf';
							$where = 'ccs_id = '.$this->zone_id.' and trx_code like "'.trim($id).'" and status = 0 and code like "'.$code.'"';
							$where2 = 'ccs_id = '.$this->zone_id.' and trx_code like "'.trim($id).'" and status = 0 and code like "'.$code2.'"';
							$cek = $this->dbfun_model->get_data_bys('*',$where,$table);
							$cek2 = $this->dbfun_model->get_data_bys('*',$where2,$table);
							$m = 'please choose valid invoice number';
							if(!empty($cek)){
								if((empty($cek2)) && (empty($param3))){
									$status = 'success';
									$m = 'confirmation sent';
									
									if (!is_dir('assets/'.$this->zone.'/trx/'))
									{
										mkdir('./assets/'.$this->zone.'/trx/', 0777, true);
									}
									if (!is_dir('assets/'.$this->zone.'/trx/cnf/'))
									{
										mkdir('./assets/'.$this->zone.'/trx/cnf/', 0777, true);
									}
									$param3 = true;
									$param4 = 'new';
								}elseif(empty($param3)){
									$o_data = explode('.',$cek2->description);
									$old_data = $o_data[1].'.'.$o_data[2];
									$status = 'success';
									$m = 'confirmation file updated';
									$param3 = true;
									$param4 = 'update';
								}elseif($param3 == 'cancel'){
									$status = 'success';
									$m = 'Cancel Order Success';
									$param3 = false;
									$param4 = 'cancel';
								}
								
								if($param3 == true){
									$file = 'file';
									$type = 'jpg|jpeg|png';
									$nama_file = trim($file.'-'.date('Ymd_His'));
									$config = unggah_berkas($path, $nama_file, $type);
									$this->upload->initialize($config);
									
									if (!$this->upload->do_upload($file))
									{
										$status = 'error';
										$message = $this->upload->display_errors('', '');
									}else{
										$upload_data = $this->upload->data();
										$nama_file = $nama_file.$upload_data['file_ext'];
									}
									$desc = 'confirmation payment.'.$nama_file;
									$custom = array(
												'ccs_id'=> $this->zone_id,
												'ccs_key'=> $cek->ccs_key,
												'type'=> 2,
												'code'=> $code2,
												'icode'=> 'INV',
												'inv_code'=> $cek->inv_code,
												'trx_code'=> $cek->trx_code,
												'description'=> $desc,
												'dateinv'=> $cek->dateinv,
												'datecreated'=> $cek->datecreated,
												'dateupdated'=> $cek->dateupdated,
												'createdby'=> $cek->createdby,
												'updatedby'=> $cek->createdby,
												'status'=> 0,
											);
									if($param4 == 'update'){
										$file_path = './assets/'.$path.'/'.trim($old_data);
										if(!empty($old_data)){
											if (file_exists($file_path))
											{
												unlink($file_path);
											}
										}
										$this->dbfun_model->del_tables($where2, $table);
									}
									if(($param4 == 'update') ||($param4 == 'new')){
										$this->new_data($table,$custom,'','','','');
									}
									
									if($this->zone = 'shisha'){//notif to admin if have confirmation transaction send to admin email
										$content = array();
										$where_trx = 'ccs_id = '.$this->zone_id.' and trx_code like "'.trim($id).'" and code like "'.$code.'"';
										$info_id = $this->dbfun_model->get_data_bys('*',$where_trx,'ccs_trx')->info_id;
										$list = $this->dbfun_model->get_all_data_bys('trx_code,cat_id,object_id,datecreated,ticket,total,price,varian_id,status,c_status,code,description','info_id = '.$info_id.' and ccs_id = '.$this->zone_id.' and trx_code = '.$id.' order by object_id','ccs_trx');
										
										$content['user'] = $this->dbfun_model->get_data_bys('*','info_id = '.$info_id.' and ccs_id = '.$this->zone_id.'','ccs_tri');
										$datas2 = '';
										foreach($list as $l){
											$o = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','o');
											$odc = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','odc');
											$cat = $this->dbfun_model->get_data_bys('title','ccs_oct.ccs_id = '.$this->zone_id.' and status != 0 and category_id = '.$l->cat_id.'','oct');
											if($l->varian_id != 0){
												$settings_id = $this->dbfun_model->get_data_bys('settings_id','ccs_prv.ccs_id = '.$this->zone_id.' and ccs_prv.object_id = '.$l->object_id.' and ccs_prv.varian_id = '.$l->varian_id.'','prv')->settings_id;
												$varian = $this->dbfun_model->get_data_bys('ccs_ocs.title','ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.settings_id = '.$settings_id.'','ocs,oct');
												if(!empty($varian)){
													$varian = $varian->title;
												}else{
													$varian = '';
												}
											}else{
												$varian = '';
											}
											//$m = 'Waiting for Payment';
											$cnf_img = '';
											$rcp_status = '';
											$cnf_status = $this->dbfun_model->get_data_bys('status,description','code like "CNF" and ccs_id = '.$this->zone_id.' and trx_code = '.$l->trx_code.'','ccs_inv');
											if(!empty($cnf_status)){
												$o_data = explode('.',$cnf_status->description);
												$cnf_img = $o_data[1].'.'.$o_data[2];
												$cnf_status = $cnf_status->status;
												if($cnf_status == 0){
													//$m = 'Waiting for Review';
												}elseif($cnf_status == 1){
													//$m = 'Confirmation Success';
													$o_s = $this->dbfun_model->get_data_bys('status,description','code like "DLV" and ccs_id = '.$this->zone_id.' and icodex = '.$l->trx_code.' and status = 2','ccs_rcp');
													if(!empty($o_s)){
														$desc = explode('<br>',trim($o_s->description));
														if(isset($desc[1])){
															//$m = 'On Shipping<br>Your shipping code: '.$desc[0];
														}else{
															//$m = 'On Shipping';
														}
													}else{
														//$m = $m.'<br>Now on Packaging';
													}
												}elseif($cnf_status == 2){
													//$m = 'Rejected, Please Confirm again';
												}
											}else{
												$cnf_status = '';
											}
											if(!empty($o)){
												$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>$odc->title,'image'=>base_url('assets/'.$zone.'/product/'.$o->image_square),'cnf_status'=>$cnf_status,'rcp_status'=>$rcp_status,'cnf_img'=>$cnf_img));
											}else{
												$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>strtoupper($l->ticket),'image'=>'','cnf_status'=>$cnf_status,'rcp_status'=>$rcp_status,'cnf_img'=>$cnf_img));
											}
										}		
										$content['datas'] = $datas2;
										$content['style'] = $this->menu['primaryc'];
										$content['zone'] = $this->zone;
										$content['logo'] = $this->menu['logo'];
										$content['link'] = $this->menu['link'];
										
										$email = $this->dbfun_model->get_data_bys('char_1,char_2,char_3,char_4,char_5,char_6','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "subscribe" and status = 1','orc')->char_5;
										$subject = '[CONFIRMATION] '.$id;
										$path = './assets/'.$path.'/'.trim($nama_file);
										$this->send_mail_attach($email,$subject,$content,$path,'ecom/email/confirmation_trx');
									}
									
								}elseif($param3 === false){
									if($param4 == 'cancel'){
										$custom = array(
												'status'=> 2,
											);
										$this->dbfun_model->update_table($where, $custom, 'ccs_trx');
										$this->dbfun_model->update_table($where, $custom, $table);
									}
								}
							}else{
								$m = 'please choose valid invoice number';
								if($param3 == 'cancel'){
									$status = 'success';
									$m = 'Cancel Order Success';
									$code = 'TRXON';
									$where = 'ccs_id = '.$this->zone_id.' and trx_code like "'.trim($id).'" and status = 0 and code like "'.$code.'"';
									$custom = array(
											'status'=> 2,
										);
									$this->dbfun_model->update_table($where, $custom, 'ccs_inv');
									$this->dbfun_model->update_table($where, $custom, 'ccs_trx');
								}
							}
						}
						
						$datas2 = array('status'=>$status,'m'=>$m);
						echo json_encode($datas2);
						$view = '';
					}
				}
			}else{
				redirect(base_url($zone));
			}
			
		}else{
			redirect($this->login);
		}
	}
	
	public function cek_api($zone,$type){
		if($type == 'rajaongkir'){
			$data = array();
			$data['zone'] = $zone;
			$part = $this->input->post('param');
			$part2 = $this->input->post('param2');
			$part3 = $this->input->post('param3');
			$this->load->library('rajaongkir');
			if($part == 'city'){
				if($part2 == 'indonesia'){
					$data = $this->rajaongkir->city();
				}else{
					$data = $this->rajaongkir->internationalDestination();
				}
			}elseif($part == 'cost'){
				$origin = '152';//152 - jakarta pusat
				if(empty($part3)){
					$part3 = 1;
				}
				$weight = ceil($part3)*1000;//dalam gram
				$courier = 'jne';//jne, pos, tiki
				$data = $this->rajaongkir->cost($origin,$part2,$weight,$courier);
			}
			echo $data;
		}
	}
}