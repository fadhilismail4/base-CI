<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Messages_model extends CI_Model { 
	
		function get_all($zone_id,$read_status,$type_user){
			$q='select * from(
				SELECT distinct(ccs_msg.message_id), ccs_msg.ts_id, ccs_msg.subject, a.name, ccs_msg.status, ccs_msg.read_status, ccs_msg.datecreated, ccs_msg.sender
				FROM ccs_msg, ccs_usr a
				WHERE 
				'.$read_status.'
				'.$type_user.'.user_id
				and ccs_msg.ccs_id = '.$zone_id.' 
				group by ccs_msg.message_id 
				UNION 
				SELECT distinct(ccs_msg.message_id), ccs_msg.ts_id, ccs_msg.subject, a.name, ccs_msg.status, ccs_msg.read_status, ccs_msg.datecreated, ccs_msg.sender
				FROM ccs_msg, ccs_adm a
				WHERE 
				'.$read_status.'
				and ccs_msg.ccs_id = '.$zone_id.' 
				group by ccs_msg.message_id ) as log order by log.datecreated desc
				';
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_all_user_receiver($zone_id,$user_id,$rt_id){
			$q='SELECT distinct(ccs_usr.user_id) as id, ccs_usr.type_id,ccs_usr.name, ccs_usr.avatar, ccs_umr.name as role
				FROM ccs_umr, ccs_usr
				WHERE 
				ccs_usr.ccs_id = '.$zone_id.' 
				and ccs_usr.rt_id = '.$rt_id.'
				and ccs_usr.user_id != '.$user_id.'
				and ccs_usr.ccs_id = ccs_umr.ccs_id
				and ccs_usr.type_id = ccs_umr.type_id
				order by ccs_usr.user_id 
				';
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_all_receiver($zone_id,$user_id,$rt_id){
			$q='select * from(
				SELECT distinct(a.admin_id) as id, @s:= 0 type_id,a.name, a.main_image as avatar, ccs_mdr.name as role
				FROM ccs_adm as a,ccs_mdr
				WHERE 
				a.ccs_id = '.$zone_id.' 
				and a.ccs_id = ccs_mdr.ccs_id
				and a.role_id = ccs_mdr.type_id
				group by a.admin_id
				UNION
				SELECT distinct(a.user_id) as id, a.type_id,a.name, a.avatar, ccs_umr.name as role
				FROM ccs_umr, ccs_usr as a
				WHERE 
				a.ccs_id = '.$zone_id.' 
				and a.rt_id = '.$rt_id.'
				and a.user_id != '.$user_id.'
				and a.ccs_id = ccs_umr.ccs_id
				and a.type_id = ccs_umr.type_id
				group by a.user_id
				) as log order by log.name asc
				';
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_all_user_receiver_from_admin($zone_id,$admin_id){
			$q='select * from(
				SELECT distinct(a.admin_id) as id, @s:= 0 type_id,a.name, a.main_image as avatar, ccs_mdr.name as role
				FROM ccs_adm as a,ccs_mdr
				WHERE 
				a.ccs_id = '.$zone_id.' 
				and a.admin_id != '.$admin_id.'
				and a.ccs_id = ccs_mdr.ccs_id
				and a.role_id = ccs_mdr.type_id
				group by a.admin_id
				UNION
				SELECT distinct(a.user_id) as id, a.type_id,a.name, a.avatar, ccs_umr.name as role
				FROM ccs_umr, ccs_usr as a
				WHERE 
				a.ccs_id = '.$zone_id.' 
				and a.ccs_id = ccs_umr.ccs_id
				and a.type_id = ccs_umr.type_id
				group by a.user_id
				) as log order by log.name asc
				';
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_all_admin($zone_id,$admin_id){
			$q='select * from(
				SELECT distinct(a.admin_id) as id, @s:= 0 type_id,a.name, a.main_image as avatar, ccs_mdr.name as role
				FROM ccs_adm as a,ccs_mdr
				WHERE 
				a.ccs_id = '.$zone_id.' 
				and a.admin_id != '.$admin_id.'
				and a.ccs_id = ccs_mdr.ccs_id
				and a.role_id = ccs_mdr.type_id
				group by a.admin_id) as log order by log.name asc
				';
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
}
	