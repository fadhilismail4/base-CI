<?php if(in_array($zone,array('shisha'))){;?>
<div class="c-content-box c-size-md c-bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin:120px 0px">
				<h1 style="font-size:80px;font-weight:bold;text-align:center">
					<i class="fa fa-clock-o"></i>
				</h1>
				<br>
				<h1 style="text-align:center">
					Maintenance
				</h1>
				<div class="text-center">
					<a href="<?php echo $menu['link'];?>" class="c-btn-border-opacity-04 c-btn btn-no-focus btn btn-sm c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-sbold clearfix" style="margin-right: 0;margin-left: 0;">
						<i class="fa fa-paper-plane-o"></i> GO BACK TO HOMEPAGE
					</a>	
				</div>
			</div>
		</div>
	</div>
</div>
<?php }else{;?>
<div class="c-content-box c-size-md c-bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin:120px 0px">
				<h1 style="font-size:80px;font-weight:bold;text-align:center">
					404
				</h1>
				<br>
				<h1 style="text-align:center">
					THE PAGE COULD NOT FOUND
				</h1>
				<h1 style="text-align:center">
					<a href="<?php echo $menu['link'];?>" class="c-btn-border-opacity-04 c-btn btn-no-focus btn btn-sm c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-sbold clearfix" style="margin-right: 0;margin-left: 0;">
						<i class="fa fa-paper-plane-o"></i> GO BACK TO HOMEPAGE
					</a>
				</h1>	
			</div>
		</div>
	</div>
</div>
<?php };?>