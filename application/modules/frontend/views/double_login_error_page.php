<div class="c-content-box c-size-md c-bg-white" style="margin:120px">
	<div class="row">
		<div class="col-md-12">
			<h1 style="font-size:80px;font-weight:bold;text-align:center">
				404
			</h1>
			<br>
			<h1 style="text-align:center">
				Please Log Out From Admin First
			</h1>
			<h1 style="text-align:center">
				<a href="<?php echo base_url($zone);?>/admin/logout" class="c-btn-border-opacity-04 c-btn btn-no-focus btn btn-sm c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-sbold clearfix" style="margin-right: 0;margin-left: 0;">
					<i class="icon-logout"></i> Log Out
				</a>
			</h1>	
		</div>
	</div>
</div>