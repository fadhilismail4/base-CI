<?php //if(!empty($gallery)){;?>
<div class="c-media">
	<?php //foreach($gallery as $g){;?>
	<div class="c-content-media-2-slider" data-slider="owl" data-single-item="true" data-auto-play="4000">
		<div class="owl-carousel owl-theme c-theme owl-single">
			<div class="item">
				<div class="c-content-media-2" style="background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $datas->image_square;?>); min-height: 460px;"> </div>
			</div><div class="item">
				<div class="c-content-media-2" style="background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $datas->image_square;?>); min-height: 460px;"> </div>
			</div>
		</div>
	</div>
	<?php// };?>
</div>
<?php// };?>


<div class="c-content-box c-size-md c-bg-white">
	<div class="container">
		<div class="cbp-l-project-desc">
			<div class="cbp-l-project-desc-title"><span><?php echo $datas->title;?></span></div>
			<div class="cbp-l-project-desc-text"><?php echo $datas->description;?></div>
		</div>
		<div class="cbp-l-project-details">
			<div class="c-content-box c-overflow-hide c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
						   <div class="col-md-6 col-sm-6">
								<div class="c-content-product-5 c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
												<span class="c-font-thin">
												<?php echo str_replace('_',' ',$bc['category']);?></span>
											<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo str_replace(' ','-',strtolower($bc['category']));?>/<?php echo str_replace(' ','-',strtolower($datas->title));?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">EXPLORE</a>
										</div>
									</div>
									<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 200px; background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $datas->image_square;?>);"></div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="c-content-product-5 c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
												<span class="c-font-thin">
												<?php echo str_replace('_',' ',$bc['category']);?></span>
											<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo str_replace(' ','-',strtolower($bc['category']));?>/<?php echo str_replace(' ','-',strtolower($datas->title));?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">EXPLORE</a>
										</div>
									</div>
									<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 200px; background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $datas->image_square;?>);"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
