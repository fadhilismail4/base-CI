<?php if(isset($style['parallax'])){;?>
<section id="<?php echo str_replace('#','',$menu[0]['menu']->description);?>">
<?php if(($style['parallax'] == 'left') || ($style['parallax'] == 'right')){;?>
<?php 
$bg_image = base_url('assets/img/content/backgrounds/bg-38.jpg');
$bg = $style['parallax-color'];//trigger dark or grey or white
$c_bg = 'c-bg-'.$bg;
$c_border = 'c-border-'.$style['parallax'].'-'.$bg;
$c_content = 'c-content-'.$style['parallax'];
if($style['parallax'] == 'left'){
	$c_content_inverse = 'c-content-right';
}else{
	$c_content_inverse = 'c-content-left';
};
if($bg == 'dark'){
	$c_font = 'c-font-white';
}else{
	$c_font = ' ';
};
$c = 'c-'.$style['parallax'];
$c_line = 'c-line-'.$style['parallax'];

;?>
	<div class="c-content-box c-size-md c-no-padding <?php echo $c_bg;?>">
		<div class="c-content-feature-4">
			<div class="c-bg-parallax c-feature-bg <?php echo $c_content_inverse.' '.$c_border;?> c-arrow" style="background-image: url(<?php echo $bg_image;?>)"></div>
			<div class="c-content-area <?php echo $c_content;?>"></div>
			<div class="container">
				<div class="c-feature-content <?php echo $c;?>">
					<div class="c-content-v-center">
						<div class="c-wrapper">
							<div class="c-body">
								<div class="c-content-title-1">
									<h3 class="c-font-uppercase c-font-bold <?php echo $c.' '.$c_font;?>">
									
									<?php// echo $datas->title;?>
									
									</h3>
									<div class="<?php echo $c_line;?>"></div>
									<p class="<?php echo $c;?>">
									<?php// echo $datas->tagline;?>
									</p>
								</div>
								<div class="c-content">
									<p class="c-margin-b-30 <?php echo $c;?>"> 
									<?php// echo $datas->description;?>
									</p>
									<button class="btn btn-lg c-theme-btn c-btn-border-2x c-btn-square c-btn-bold">
									EXPLORE
									</button>
									<button class="btn btn-lg c-theme-btn c-btn-square c-btn-bold">
									PURCHASE
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php };?>
</section>
<?php };?>