<!-- BEGIN: BLOG LISTING -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="c-content-blog-post-card-1-grid">
					<div class="row">
					<?php foreach($datas as $d){;?>
						<?php $last_val = count($d['datas']);$mid_val = round($last_val/2);$i=1;foreach($d['datas'] as $ds){;?>
						<?php if($i == 1 || $i == 2){;?>
						<div class="col-md-6">
						<?php };?>
							<div class="c-content-blog-post-card-1 c-option-2 c-bordered">
								<div class="c-media c-content-overlay">
									<div class="c-overlay-wrapper">
										<div class="c-overlay-content">
											<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo str_replace(' ','-',strtolower($d['section']->title));?>/<?php echo str_replace(' ','_',strtolower($ds->title));?>">
												<i class="icon-link"></i>
											</a>
											<a href="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $ds->image_square;?>" data-lightbox="fancybox" data-fancybox-group="gallery">
												<i class="icon-magnifier"></i>
											</a>
										</div>
									</div>
									<img class="c-overlay-object img-responsive" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $ds->image_square;?>" alt="" style="width: 100%;"> 
								</div>
								<div class="c-body">
									<div class="c-title c-font-bold c-font-uppercase">
										<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo str_replace(' ','-',strtolower($d['section']->title));?>/<?php echo str_replace(' ','_',strtolower($ds->title));?>"><?php echo $ds->title;?></a>
									</div>
									<div class="c-author"> By
										<a href="#">
											<span class="c-font-uppercase">Admin</span>
										</a> on
										<span class="c-font-uppercase"><?php echo $ds->datecreated;?></span>
									</div>
									<div class="c-panel">
										<ul class="c-tags c-theme-ul-bg pull-left">
											<li><?php echo $d['section']->title;?></li>
										</ul>
										<!--<div class="c-comments">
											<a href="#">
												<i class="icon-speech"></i> 30 comments
											</a>
										</div>-->
									</div>
									<p> <?php echo word_limiter(strip_tags($ds->description),12);?></p>
								</div>
							</div>
						<?php if($i == $last_val || $i == $mid_val){;?>
						</div>
						<?php };?>
						<?php $i++;};?>
					<?php };?>
					</div>
					<div class="c-pagination">
						<ul class="c-content-pagination c-theme">
							<li class="c-prev">
								<a href="#">
									<i class="fa fa-angle-left"></i>
								</a>
							</li>
							<li>
								<a href="#">1</a>
							</li>
							<li class="c-active">
								<a href="#">2</a>
							</li>
							<li>
								<a href="#">3</a>
							</li>
							<li>
								<a href="#">4</a>
							</li>
							<li class="c-next">
								<a href="#">
									<i class="fa fa-angle-right"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<!-- sidbar -->
			</div>
		</div>
	</div>
</div>
<!-- END: BLOG LISTING  -->