<?php if(isset($style['tiles'])){;?>
<?php if($style['tiles'] == 'color'){;?>
<div class="c-content-box c-size-md c-bg-white">
	<div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-center c-font-bold">Services We Do</h3>
			<div class="c-line-center"></div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="c-content-tile-1 c-bg-green">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> Nothing is impossible for JANGO. Highly Flexible, always growing </h3>
										<a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-arrow-right c-arrow-green c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/79.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/79.jpg)"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-tile-1 c-bg-brown-2">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> Web Design </h3>
										<p class="c-tile-body c-font-white">Lorem ipsum consectetuer elit sit amet, sit adipiscing amet, coectetuer adipiscing elit sit ame.</p>
										<a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-arrow-right c-arrow-brown-2 c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/90.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/90.jpg)"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-tile-1 c-bg-red-2">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-arrow-left c-arrow-red-2 c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/41.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/41.jpg)"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> LEARN MORE ABOUT JANGO </h3>
										<a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-tile-1 c-bg-blue-3">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-arrow-left c-arrow-blue-3 c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/78.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/78.jpg)"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> The Future </h3>
										<p class="c-tile-body c-font-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, nonummy nibh euismod tincidunt.</p>
										<a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php }else{;?>
<div class="c-content-box c-size-md c-bg-grey-1">
	<div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-center c-font-bold">Services We Do</h3>
			<div class="c-line-center"></div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="c-content-tile-1">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-arrow-left c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/56.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/56.jpg)"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold"> LEARN MORE ABOUT JANGO </h3>
										<a href="#" class="btn btn-sm c-btn-grey-2 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-tile-1">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-arrow-left c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/65.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/65.jpg)"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold"> The Future </h3>
										<p class="c-tile-body">Lorem ipsum consectetuer elit sit amet, sit adipiscing amet, coectetuer adipiscing elit sit ame.</p>
										<a href="#" class="btn btn-sm c-btn-grey-2 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-tile-1">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold"> Nothing is impossible for JANGO. Highly Flexible, always growing </h3>
										<a href="#" class="btn btn-sm c-btn-grey-2 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-arrow-right c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/47.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/47.jpg)"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-tile-1">
					<div class="row">
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-content-v-center" data-height="height">
								<div class="c-wrapper">
									<div class="c-body c-center">
										<h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold"> Web Design </h3>
										<p class="c-tile-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, nonummy nibh euismod tincidunt.</p>
										<a href="#" class="btn btn-sm c-btn-grey-2 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="c-tile-content c-bg-white c-arrow-right c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#">
											<i class="icon-link"></i>
										</a>
										<a href="<?php echo site_url();?>assets/img/content/stock/66.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
											<i class="icon-magnifier"></i>
										</a>
									</div>
								</div>
								<div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo site_url();?>assets/img/content/stock/66.jpg)"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php };?>
<?php };?>