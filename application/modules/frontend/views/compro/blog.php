<div class="container" style="padding-top: 60px;">
	<div class="row">
		<div class="col-md-9">
			<div class="c-content-blog-post-1-view" style="padding: 0;">
				<div class="c-content-blog-post-1">
					<?php if(!empty($gallery)){;?>
					<div class="c-media">
						<?php foreach($gallery as $g){;?>
						<div class="c-content-media-2-slider" data-slider="owl" data-single-item="true" data-auto-play="4000">
							<div class="owl-carousel owl-theme c-theme owl-single">
								<div class="item">
									<div class="c-content-media-2" style="background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $g->image_square;?>); min-height: 460px;"> </div>
								</div>
							</div>
						</div>
						<?php };?>
					</div>
					<?php };?>
					<div class="c-title c-font-bold c-font-uppercase" style="<?php if(empty($gallery)){ echo 'margin-top:0';};?>">
						<a href="#"><?php echo $datas->title;?></a>
					</div>
					<div class="c-panel c-margin-b-30">
						<div class="c-author">
							<a href="#">By
								<span class="c-font-uppercase">Admin</span>
							</a>
						</div>
						<div class="c-date">on
							<span class="c-font-uppercase"> <?php echo date('l, d F Y',strtotime($datas->datecreated));?></span>
						</div>
						<ul class="c-tags c-theme-ul-bg">
							<li><?php echo str_replace('-',' ',$bc['category']);?></li>
						</ul>
						<!--<div class="c-comments">
							<a href="#">
								<i class="icon-speech"></i> 30 comments</a>
						</div>-->
					</div>
					<?php if(!empty($datas->image_square)){
						$img = base_url('assets').'/'.$zone.'/'.$bc['module'].'/'.$datas->image_square;
						$class = 'col-md-3';
						$class2 = 'col-md-9';
					}else{
						$img = '';
						$class = 'hide';
						$class2 = 'col-md-12';
					};?>
					<div class="<?php echo $class;?>" style="padding: 0;">
						<img src="<?php echo $img;?>" style="width:100%">
					</div>
					<div class="c-desc <?php echo $class2;?>">
						<?php echo $datas->description;?>
					</div>
					<!--comments goes here ya baby-->
				</div>
			</div>
		</div>
		<?php $this->load->view('sidebar-blog')?><!--sidebar here ya baby-->
	</div>
</div>