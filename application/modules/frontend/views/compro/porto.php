 <!-- BEGIN: CONTENT/PORTFOLIO/LATEST-WORKS-1 -->
<div class="c-content-box c-size-md c-bg-grey-1">
	<div class="container">
		<div class="c-content-title-1">
			<h3 class="c-center c-font-uppercase c-font-bold">Latest Portfolio</h3>
			<div class="c-line-center c-theme-bg"></div>
			<p class="c-center c-font-uppercase">Showcasing your latest designs, sketches, photographs or videos.</p>
		</div>
		<div class="cbp-panel">
			<!-- SEE: components.js:ContentCubeLatestPortfolio -->
			<div class="c-content-latest-works cbp cbp-l-grid-masonry-projects">
				<div class="cbp-item graphic">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/08-long.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project1.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/08.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item web-design logos">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/07.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project2.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/07.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="World Clock Widget<br>by Paul Flavius Nechita">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item graphic logos">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/09.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project3.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="http://vimeo.com/14912890" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="To-Do Dashboard<br>by Tiberiu Neamu">view video</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item identity web-design">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/014.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project4.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/014.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="WhereTO App<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item web-design graphic">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/34.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project5.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/34.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Events and  More<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item identity web-design">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/53.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project6.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/53.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Ski * Buddy<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item graphic logos">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/39.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project7.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/39.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/PORTFOLIO/LATEST-WORKS-1 -->
<!-- BEGIN: CONTENT/PORTFOLIO/LATEST-WORKS-2 -->
<div class="c-content-box c-size-md c-bg-white">
	<div class="container">
		<div class="c-content-title-1">
			<h3 class="c-center c-font-uppercase c-font-bold">Latest Portfolio</h3>
			<div class="c-line-center c-theme-bg"></div>
			<p class="c-center c-font-uppercase">Showcasing your latest designs, sketches, photographs or videos.</p>
		</div>
		<div class="cbp-panel">
			<!-- SEE: components.js:ContentCubeLatestPortfolio -->
			<div class="c-content-latest-works cbp cbp-l-grid-masonry-projects">
				<div class="cbp-item graphic">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/08-long_grey.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project1.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/08_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item web-design logos">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/07_grey.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project2.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/07_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="World Clock Widget<br>by Paul Flavius Nechita">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item graphic logos">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/09_grey.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project3.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="http://vimeo.com/14912890" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="To-Do Dashboard<br>by Tiberiu Neamu">view video</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item identity web-design">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/014_grey.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project4.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/014_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="WhereTO App<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item web-design graphic">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/34_grey.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project5.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/34_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Events and  More<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item identity web-design">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/53_grey.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project6.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/53_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Ski * Buddy<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cbp-item graphic logos">
					<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo site_url();?>assets/img/content/stock/39_grey.jpg" alt=""> </div>
						<div class="cbp-caption-activeWrap">
							<div class="c-masonry-border"></div>
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<a href="ajax/projects/project7.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">explore</a>
									<a href="<?php echo site_url();?>assets/img/content/stock/39_grey.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">zoom</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/PORTFOLIO/LATEST-WORKS-2 -->
<!-- BEGIN: CONTENT/PORTFOLIO/LATEST-WORKS-3 -->
<div class="c-content-box c-size-lg c-bg-grey-1 c-overflow-hide c-no-bottom-padding">
	<div class="cbp c-content-latest-works-fullwidth">
		<div class="cbp-item identity logos">
			<a href="<?php echo site_url();?>assets/img/content/stock/34.jpg" class="cbp-caption cbp-lightbox" data-title="Dashboard<br>by Paul Flavius Nechita">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/34.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Dashboard</div>
							<div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item web-design">
			<a href="<?php echo site_url();?>assets/img/content/stock/33.jpg" class="cbp-caption cbp-lightbox" data-title="Client chat app WIP<br>by Paul Flavius Nechita">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/33.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Client chat app WIP</div>
							<div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item motion identity">
			<a href="<?php echo site_url();?>assets/img/content/stock/32.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by Paul Flavius Nechita">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/32.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">World Clock Widget</div>
							<div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item identity graphic">
			<a href="<?php echo site_url();?>assets/img/content/stock/31.jpg" class="cbp-caption cbp-lightbox" data-title="Website Lightbox<br>by Paul Flavius Nechita">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/31.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Website Lightbox</div>
							<div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item motion logos">
			<a href="<?php echo site_url();?>assets/img/content/stock/30.jpg" class="cbp-caption cbp-lightbox" data-title="Skateshop Website<br>by Paul Flavius Nechita">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/30.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Skateshop Website</div>
							<div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item web-design">
			<a href="<?php echo site_url();?>assets/img/content/stock/29.jpg" class="cbp-caption cbp-lightbox" data-title="10 Navigation Bars<br>by Paul Flavius Nechita">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/29.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">10 Navigation Bars</div>
							<div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item identity motion">
			<a href="<?php echo site_url();?>assets/img/content/stock/28.jpg" class="cbp-caption cbp-lightbox" data-title="To-Do Dashboard<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/28.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">To-Do Dashboard</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item web-design graphic">
			<a href="<?php echo site_url();?>assets/img/content/stock/27.jpg" class="cbp-caption cbp-lightbox" data-title="Events and  More<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/27.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Events and More</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item graphic logos">
			<a href="<?php echo site_url();?>assets/img/content/stock/26.jpg" class="cbp-caption cbp-lightbox" data-title="WhereTO App<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/26.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">WhereTO App</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item graphic motion">
			<a href="<?php echo site_url();?>assets/img/content/stock/25.jpg" class="cbp-caption cbp-lightbox" data-title="Bolt UI<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/25.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Bolt UI</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item graphic">
			<a href="<?php echo site_url();?>assets/img/content/stock/24.jpg" class="cbp-caption cbp-lightbox" data-title="iDevices<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/24.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">iDevices</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item graphic logos">
			<a href="<?php echo site_url();?>assets/img/content/stock/23.jpg" class="cbp-caption cbp-lightbox" data-title="Ski * Buddy<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/23.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Ski * Buddy</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item graphic logos">
			<a href="<?php echo site_url();?>assets/img/content/stock/22.jpg" class="cbp-caption cbp-lightbox" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/22.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Seemple* Music for iPad</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item graphic logos">
			<a href="<?php echo site_url();?>assets/img/content/stock/21.jpg" class="cbp-caption cbp-lightbox" data-title="Drag 2 Upload ~ Widget<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/21.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Drag 2 Upload ~ Widget</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="cbp-item graphic logos">
			<a href="<?php echo site_url();?>assets/img/content/stock/20.jpg" class="cbp-caption cbp-lightbox" data-title="Starindeed Website<br>by Tiberiu Neamu">
				<div class="cbp-caption-defaultWrap">
					<img src="<?php echo site_url();?>assets/img/content/stock/20.jpg" alt=""> </div>
				<div class="cbp-caption-activeWrap">
					<div class="cbp-l-caption-alignLeft">
						<div class="cbp-l-caption-body">
							<div class="cbp-l-caption-title">Starindeed Website</div>
							<div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<!-- END: CONTENT/PORTFOLIO/LATEST-WORKS-3 -->