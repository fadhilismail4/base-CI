<?php if(!isset($category)){;?>
<?php foreach($datas as $d){;?>
<?php 
	if($datas[0]['datas'][0]->image_square){
		$cek_file = 'assets/'.$zone.'/'.$bc['module'].'/'.$datas[0]['datas'][0]->image_square;
		if(file_exists($cek_file)){
			$first_image = base_url('assets').'/'.$zone.'/'.$bc['module'].'/'.$datas[0]['datas'][0]->image_square;
		}else{
			$first_image = base_url('assets').'/'.$zone.'/category/'.$datas[0]['datas'][0]->image_square;
		}
	}else{
		$first_image = '';
	}
	;?>
<div class="c-content-box c-overflow-hide c-bg-grey">
	<div class="c-content-product-3 c-bs-grid-reset-space">
		<div class="row">
			<div class="col-md-8 col-sm-6">
				<div class="c-content-media-2-slider" data-slider="owl" data-single-item="true" data-auto-play="4000" style="margin-bottom:0px">
					<div class="owl-carousel owl-theme c-theme owl-single">
						<div class="item">
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 550px; background-image: url(<?php echo $first_image;?>);"></div>
						</div>
						<?php if(!empty($gallery)){;
							$link_gal = base_url().'assets/'.$zone.'/gallery/';?>
							<?php foreach($gallery as $g){;?>
							<div class="item">
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 550px; background-image: url(<?php echo $link_gal;?><?php echo $g->image_square;?>);"></div>
							</div>
							<?php };?>
						<?php };?>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="c-wrapper c-bg-red" style="height: 550px;background-color: #C32126 !important;">
					<div class="c-content c-border c-border-padding c-border-padding-right">
						<h3 class="c-title c-font-25 c-font-white c-font-uppercase c-font-bold"><?php echo $d['datas'][0]->title;?></h3>
						<p class="c-description c-font-17 c-font-white"><?php echo $d['datas'][0]->description;?></p>
						<!--<p class="c-price c-font-55 c-font-white c-font-thin">$249</p>
						<a href="shop-product-details-2.html" class="btn btn-lg c-btn-white c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="c-content-box c-size-md c-overflow-hide c-bg-grey-1">
	<div class="c-content-title-4 c-theme">
		<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
			<span class="c-bg-grey-1"><?php echo str_replace('_',' ',$datas[0]['section']->title);?></span>
		</h3>
	</div>
	<div class="row">
	<!--<div data-slider="owl" data-items="4">
			<div class="owl-carousel c-theme owl-reset-space">-->
		<div>
			<div class="c-theme owl-reset-space">
				<?php foreach($d['datas'] as $ds){;?>
					<?php if($ds->object_id != $datas[0]['datas'][0]->object_id){;?>
					<div class="col-md-3 item" style="padding: 7px;">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo str_replace(' ','-',strtolower($bc['category']));?>/<?php echo str_replace(' ','-',$d['section']->title);?>/<?php echo str_replace(' ','-',strtolower($ds->title));?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square"><?php echo $ds->title;?></a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $ds->image_square;?>);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<?php };?>
				<?php };?>
			</div>
		</div>
	</div>
</div>
<?php ;};?>
<?php }elseif(isset($category)){
	if($category->image_square){
		$first_image = base_url('assets').'/'.$zone.'/'.$bc['module'].'/'.$category->image_square;
	}else{
		$first_image = '';
	}
	;?>
	
<?php if((strtolower($category->title) != 'mix flavor') && (strtolower($category->title) != 'single flavor')){;?>

<?php if((strtolower($category->title) == 'package')){;?>
<div class="c-content-box c-size-md c-overflow-hide">
	<div class="c-content-title-4 c-theme">
		<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
			<span class="c-bg-grey-1"><?php echo str_replace('_',' ',$datas[0]['section']->title);?></span>
		</h3>
	</div>
	<div class="container">
		<div class="cbp-panel">
			<div id="filters-container" class="cbp-l-filters-buttonCenter">
				<div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> All
					<div class="cbp-filter-counter"></div>
				</div>
			</div>
			<div id="grid-container" class="cbp cbp-l-grid-masonry-projects">
				<?php foreach($datas as $d){;?>
				<?php foreach($d['datas'] as $ds){;?>
					<?php if(!empty($ds)){;?>
					<div class="cbp-item graphic">
						<div class="cbp-caption">
							<div class="cbp-caption-defaultWrap">
								<img src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $ds->image_square;?>" alt=""> </div>
							<div class="cbp-caption-activeWrap">
								<div class="c-masonry-border"></div>
								<div class="cbp-l-caption-alignCenter">
									<div class="cbp-l-caption-body">
										<a href="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $ds->image_square;?>" class="cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" download>Download</a>
										<a href="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $ds->image_square;?>" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">Zoom</a>
									</div>
								</div>
							</div>
						</div>
						<a href="#" class="cbp-singlePage cbp-l-grid-masonry-projects-title"><?php echo $ds->title;?></a>
						<!--<div class="cbp-l-grid-masonry-projects-desc">web design / graphic</div>-->
					</div>
					<?php };?>
				<?php };?>
				<?php };?>
				
			</div>
			<div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
				<!--<a href="ajax/masonry-gallery/load-more.html" class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase">
					<span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
					<span class="cbp-l-loadMore-loadingText">LOADING...</span>
					<span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
				</a>-->
			</div>
		</div>
	</div>
</div>
<div class="c-content-box c-size-md" id="event-gall" data-offset="0" data-param="event">
	
</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
	$.getScript("<?php echo base_url();?>assets/frontend/js/scripts/pages/masonry-gallery.js");
});
</script>
<?php }else{;?>
<div class="c-content-box c-overflow-hide c-bg-grey">
	<div class="c-content-product-3 c-bs-grid-reset-space">
		<div class="row">
			<div class="col-md-8 col-sm-6">
				<div class="c-content-media-2-slider" data-slider="owl" data-single-item="true" data-auto-play="4000" style="margin-bottom:0px">
					<div class="owl-carousel owl-theme c-theme owl-single">
						<div class="item">
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 550px; background-image: url(<?php echo $first_image;?>);"></div>
						</div>
						<?php if(!empty($gallery)){;
							$link_gal = base_url().'assets/'.$zone.'/gallery/';?>
							<?php foreach($gallery as $g){;?>
							<div class="item">
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 550px; background-image: url(<?php echo $link_gal;?><?php echo $g->image_square;?>);"></div>
							</div>
							<?php };?>
						<?php };?>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="c-wrapper c-bg-red" style="height: 550px;background-color: #C32126 !important;">
					<div class="c-content c-border c-border-padding c-border-padding-right">
						<h3 class="c-title c-font-25 c-font-white c-font-uppercase c-font-bold"><?php echo str_replace('_',' ',$datas[0]['section']->title);?></h3>
						<p class="c-description c-font-17 c-font-white"><?php echo $category->description;?></p>
						<!--<p class="c-price c-font-55 c-font-white c-font-thin">$249</p>
						<a href="shop-product-details-2.html" class="btn btn-lg c-btn-white c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="c-content-box c-size-md c-overflow-hide c-bg-grey-1">
	<div class="c-content-title-4 c-theme">
		<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
			<span class="c-bg-grey-1"><?php echo str_replace('_',' ',$datas[0]['section']->title);?></span>
		</h3>
	</div>
	<div class="row">
		<div>
			<div class="c-theme owl-reset-space">
			<?php foreach($datas as $d){;?>
				<?php foreach($d['datas'] as $ds){;?>
					<div class="col-md-3 item" style="padding: 7px;">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo str_replace(' ','-',strtolower($bc['category']));?>/<?php echo str_replace(' ','-',$d['section']->title);?>/<?php echo str_replace(' ','-',strtolower($ds->title));?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square"><?php echo $ds->title;?></a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $ds->image_square;?>);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				<?php };?>
			<?php ;};?>	
			</div>
		</div>
	</div>
</div>
<?php };?>

<?php }else{;?>
<?php if(!empty($datas[0]['datas'])){
	if($datas[0]['datas'][0]->image_square){
		$first_image = base_url('assets').'/'.$zone.'/'.$bc['module'].'/'.$datas[0]['datas'][0]->image_square;
	}else{
		$first_image = '';
	}
	;?>
<div class="c-layout-breadcrumbs-1 c-bgimage-full c-fonts-uppercase c-fonts-bold c-bg-img-center" style="background-image: url(<?php echo $breadcrumb->image;?>)">
	<div class="c-breadcrumbs-wrapper">
		<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
			<div class="container">
				<div class="c-page-title c-pull-left">
					<h1 class="c-font-uppercase c-font-bold c-font-dark"><?php echo $datas[0]['datas'][0]->title;?></h1>
					<h4 class="c-font-dark"><?php echo $datas[0]['datas'][0]->description;?></h4>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="c-media">
	<?php if(empty($gallery)){;?>
	<div class="c-content-media-2-slider" data-slider="owl" data-slide-style="" data-single-item="true" data-auto-play="4000">
	<?php }else{;?>
	<div class="c-content-media-2-slider" data-slider="owl" data-slide-style="fade" data-single-item="true" data-auto-play="4000">
	<?php };?>
		<div class="owl-carousel owl-theme c-theme owl-single">
			<div class="item">
				<div class="c-content-media-2" style="background-image: url(<?php echo $first_image;?>); min-height: 460px;"> </div>
			</div>
			<?php if(!empty($gallery)){;
				$link_gal = base_url().'assets/'.$zone.'/gallery/';?>
				<?php foreach($gallery as $g){;?>
				<div class="item">
					<div class="c-content-media-2" style="background-image: url(<?php echo $link_gal;?><?php echo $g->image_square;?>); min-height: 460px;"> </div>
				</div>
				<?php };?>
			<?php };?>
		</div>
	</div>
	
</div>
<?php };?>

<div class="c-content-box c-size-sm c-bg-white">
	<div class="container">
		<div class="cbp-l-project-desc">
			<!--<div class="cbp-l-project-desc-title"><span><?php echo $datas[0]['datas'][0]->title;?></span></div>-->
			<div class="cbp-l-project-desc-text"><?php echo $datas[0]['datas'][0]->description;?></div>
		</div>
	</div>
</div>
<?php };?>


<?php };?>