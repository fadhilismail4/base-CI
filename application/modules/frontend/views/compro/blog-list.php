<!-- BEGIN: BLOG LISTING -->
<input type="hidden" id="data-newslist" value="list" data-base-url="<?php echo base_url();?>" data-zone="<?php echo $zone;?>" data-url-zone="<?php echo $menu['link'];?>" data-base-modul="<?php echo $bc['module'];?>" data-url-first="list" data-url-second="list" data-id="<?php if(isset($bc['category'])){ echo $bc['category'];}else{ echo 'all';};?>" data-param="<?php echo $bc['module'];?>"/>
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="row">
			<?php $this->load->view('sidebar-blog')?><!--sidebar here ya baby-->
			<div class="col-md-9">
				<div class="throbber-loader" id="loader" style="display:none"></div>
				<div class="c-content-blog-post-1-list" id="view-newslist"></div>
				<div class="c-pagination" style="text-align: center;"><ul class="c-content-pagination c-theme"></ul></div>
			</div>
		</div>
	</div>
</div>
<!-- END: BLOG LISTING  -->