<?php if(isset($style['banner'])){;?>
<section class="c-layout-revo-slider c-layout-revo-slider-7" id="banner">
	<div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
		<div class="tp-banner">
			<ul>
				<?php foreach($banner as $b){
					if($is_mobile){
						if(isset($b->image_square2)){
							$img = $b->image_square2;
						}else{
							$img = $b->image_square;
						}
					}else{
						$img = $b->image_square;
					}
					$mime = mime_content_type('./assets/'.$zone.'/homepage/'.$img);
					if(strstr($mime, "video/")){
						$cek_file = 'video';
					}else if(strstr($mime, "image/")){
						$cek_file = 'image';
					}
					;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="500">
						<?php if($cek_file == 'image'){;?>
							<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $img;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<?php }else{;?>
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img src="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png" alt="">
							<div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false"
							data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%" data-videopreload="meta" data-videomp4="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $img;?>" data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"
							data-aspectratio="16:9" data-volume="100" data-videoposter="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png"> </div>
							<div class="tp-caption arrowicon customin rs-parallaxlevel-0 visible-xs" data-x="center" data-y="bottom" data-hoffset="0" data-voffset="-60" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="500" data-start="2000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 13;">
								<div class="rs-slideloop" data-easing="Power3.easeInOut" data-speed="0.5" data-xs="-5" data-xe="5" data-ys="0" data-ye="0">
									<span class="c-video-hint c-font-15 c-font-sbold c-font-center c-font-dark"> Tap to play video
										<i class="icon-control-play"></i>
									</span>
								</div>
							</div>
							<!--END-->
						<?php };?>
						
						<!--BEGIN: MAIN TITLE -->
						<?php if($style['banner'] != 'no_detail'){;?>
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-280" data-voffset="-100" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							<?php echo $b->title;?>
							</h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-345" data-voffset="30" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 <?php echo word_limiter($b->description,6);?>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-445" data-voffset="140" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
						<?php };?>
					</li>
				<?php };?>
			</ul>
		</div>
	</div>
</section>
<?php };?>
<?php if($zone == 'shisha'){;?>
<section style="margin-top:-50px;">
	<div style="color:#fff;position: relative;z-index: 44;text-align:center;font-size:24px">
		<i class="fa fa-chevron-down" style="text-shadow: 1px -3px 5px rgb(0, 0, 0);animation: bounce 2s infinite;"></i>
	</div>	
</section>
<?php };?>