<?php if(isset($style['divider'])){;?>
<?php if($style['divider'] == 'parallax'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-5 -->
<?php if(!empty($divider)){
	$c_datas = count($divider);
	;?>
<?php if($c_datas == 1){;?>	
<div class="c-content-box c-size-md c-bg-parallax" style="max-height:20%;background-image: url(<?php echo site_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $divider[0]->image_square;?>)">
	<div class="container">
		<div class="c-content-bar-4">
			<h3 class="c-font-uppercase c-font-bold">
				<?php echo $divider[0]->title;?>
				<br/>
				<?php echo $divider[0]->tagline;?>
			</h3>
			<div class="c-actions">
				<a href="#" class="btn btn-md c-btn-border-2x c-btn-square c-btn-white c-btn-uppercase c-btn-bold c-margin-b-100">Learn More</a>
			</div>
		</div>
	</div>
</div>
<?php }else{;?>
<style id="sdivider">
/* .c-bg-parallax{
	-webkit-background-size: cover !important; 
	-moz-background-size: cover !important; 
	-o-background-size: cover !important; 
    background-attachment: fixed !important;
    background-size: cover !important;
    background-color: transparent !important;
}; */

@media only screen and (min-width: 768px) {
	.c-content-box.c-size-md {
		padding: 200px 0;
	}
}
@media only screen and (max-width: 768px) {
	.c-bg-parallax{
		-webkit-background-size: 768px auto; 
		-moz-background-size: 768px auto; 
		-o-background-size: 768px auto; 
		background-size: 768px auto;
	};
};
</style>
<?php if(isset($divider)){; 
foreach($divider as $b){
	if($is_mobile){
		if(isset($b->image_square2)){
			$img = $b->image_square2;
		}else{
			$img = $b->image_square;
		}
	}else{
		$img = $b->image_square;
	};?>
<div id="<?php echo strtolower(str_replace(' ','_',$b->title));?>" class="c-content-box c-size-md c-bg-parallax img-parallax-effect" style="background-image: url(<?php echo site_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $img;?>)">
	<div class="container">
		<div class="c-content-bar-4">
			<h3 class="c-font-uppercase c-font-bold">
				<?php echo $b->title;?>
				<br/>
				<?php echo $b->tagline;?>
			</h3>
			<div class="c-actions">
				<a href="<?php echo $b->url;?>" class="btn btn-md c-btn-border-2x c-btn-square c-btn-white c-btn-uppercase c-btn-bold c-margin-b-100">Learn More</a>
			</div>
		</div>
	</div>
</div>
<div style="height:60px">
</div>
<?php };};?>


<?php };?>
<?php };?>
<!-- END: CONTENT/BARS/BAR-5 -->
<?php }else{;?>

<?php if($style['divider'] == 'center-border'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-6 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-bar-1 c-opt-1 c-bordered c-theme-border c-shadow">
			<h3 class="c-font-uppercase c-font-bold">JANGO is optimized to every development</h3>
			<p class="c-font-uppercase"> JANGO is build to completely support your web projects by
				<br> providing
				<strong>Ultra Flexibility</strong>,
				<strong>Increased productivity</strong> and
				<strong>Top quality</strong>
			</p>
			<button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-btn-dark c-btn-uppercase c-btn-bold c-margin-r-40">Explore</button>
			<button type="button" class="btn btn-md c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Purchase</button>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-6 -->	
<?php }elseif($style['divider'] == 'center'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-1 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-bar-1 c-opt-1">
			<h3 class="c-font-uppercase c-font-bold">Get JANGO as your development companion</h3>
			<p class="c-font-uppercase"> JANGO is built to completely support your web projects by
				<br> providing
				<strong>Ultra Flexibility</strong>,
				<strong>Increased productivity</strong> and
				<strong>Top quality</strong>
			</p>
			<button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-btn-dark c-btn-uppercase c-btn-bold c-margin-r-40 hide">Learn More</button>
			<button type="button" class="btn btn-md c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Purchase</button>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-1 -->
<?php }elseif($style['divider'] == 'left'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-3 -->
<div class="c-content-box c-size-md c-bg-dark">
	<div class="container">
		<div class="c-content-bar-3">
			<div class="row">
				<div class="col-md-7">
					<div class="c-content-title-1">
						<h3 class="c-font-uppercase c-font-bold">DEDICATED SUPPORT</h3>
						<p class="c-font-uppercase">JANGO comes with top-of-the-line support teams to ensure that we provide the best experience for our customers</p>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-2">
					<div class="c-content-v-center" style="height: 90px;">
						<div class="c-wrapper">
							<div class="c-body">
								<button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-theme-btn c-btn-uppercase c-btn-bold">Get Support</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-3 -->
<?php }elseif($style['divider'] == 'right'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-2 -->
<?php
$c_bg = 'c-bg-grey-1'; // or c-bg-white
;?>
<div class="c-content-box c-size-md c-bg-grey-1">
	<div class="container">
		<div class="c-content-bar-2 c-opt-1">
			<div class="row" data-auto-height="true">
				<div class="col-md-6">
					<!-- Begin: Title 1 component -->
					<div class="c-content-title-1" data-height="height">
						<h3 class="c-font-uppercase c-font-bold">Meet JANGO. The Theme Of 2015</h3>
						<p class="c-font-uppercase c-font-sbold"> The Ever growing Multipurpose Theme. Ultra responsive, Clean Coding with top quality modern design trends. </p>
						<button type="button" class="btn btn-md c-btn-border-2x c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Purchase</button>
					</div>
					<!-- End-->
				</div>
				<div class="col-md-6">
					<div class="c-content-v-center c-bg-red" data-height="height">
						<div class="c-wrapper">
							<div class="c-body">
								<h3 class="c-font-white c-font-bold">Just another option from an endless list of choices within JANGO.</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-2 -->
<?php };?>

<?php };?>
<?php };?>