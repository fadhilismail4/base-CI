<?php if(isset($breadcrumb->image)){
	$c_font = 'c-font-white';
	;?>
<div class="c-layout-breadcrumbs-1 c-bgimage-full c-fonts-uppercase c-fonts-bold c-bg-img-center" style="background-image: url(<?php echo $breadcrumb->image;?>)">
<div class="c-breadcrumbs-wrapper">
<?php }else{
	$c_font = 'c-font-dark';
	;?>
<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
<?php };?>
	<?php if(isset($datas)){;?>
	<?php if(is_object($datas)){;?>
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h1 class="c-font-uppercase c-font-bold <?php echo $c_font;?>"><?php echo $datas->title;?></h1>
			<h4 class="c-font-dark"><?php if(isset($datas->tagline)){ echo $datas->tagline;};?></h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
			<li>
				<a href="<?php echo $menu['link'];?>" class="<?php echo $c_font;?>">Home</a>
			</li>
			<?php if(isset($bc['module'])){;?>
				<li class="<?php echo $c_font;?>">/</li>
				<li>
					<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>" class="<?php echo $c_font;?>"><?php echo ucfirst($bc['module']);?></a>
				</li>
			<?php };?>
			<?php if(isset($bc['category'])){;?>
				<?php if(!empty($bc['category'])){;?>
				<li class="<?php echo $c_font;?>">/</li>
				<li>
					<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo $bc['category'];?>" class=	"<?php echo $c_font;?>"><?php echo ucwords(str_replace('-',' ',$bc['category']));?></a>
				</li>
				<?php };?>
			<?php };?>
			<li class="<?php echo $c_font;?>">/</li>
			<li class="c-state_active <?php echo $c_font;?>"><?php echo $datas->title;?></li>
		</ul>
	</div>
	<?php }else{;?>
	<style>
		.c-layout-breadcrumbs-1{padding: 0;}
	</style>
	<?php };?>
	<?php }elseif(isset($bc)){;?>
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-bold <?php echo $c_font;?>"><?php if(isset($bc['title'])){ echo ucwords(str_replace('-',' ',$bc['title']));}elseif(isset($bc['category'])){ echo ucwords(str_replace('-',' ',$bc['category'])) ;}else{ echo $bc['module'] ;};?></h3>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li>
					<a href="<?php echo $menu['link'];?>" class="<?php echo $c_font;?>">Home</a>
				</li>
				<li class="<?php echo $c_font;?>">/</li>
			<?php if(isset($bc['module'])){;?>
				<li>
					<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>" class=	"<?php echo $c_font;?>"><?php echo ucfirst($bc['module']);?></a>
				</li>
			<?php };?>
			<?php if(isset($bc['category'])){;?>
				<li class="<?php echo $c_font;?>">/</li>
				<li>
					<a href="<?php echo $menu['link'];?>/<?php echo $bc['module'];?>/<?php echo $bc['category'];?>" class="<?php echo $c_font;?>"><?php echo ucwords(str_replace('-',' ',$bc['category']));?></a>
				</li>
			<?php };?>
			<?php if(isset($bc['title'])){;?>
				<li class="<?php echo $c_font;?>">/</li>
				<li class="c-state_active <?php echo $c_font;?>"><?php echo ucwords(str_replace('-',' ',$bc['title']));?></li>
			<?php };?>
		</ul>
	</div>
	<?php }else{;?>
	<style>
		.c-layout-breadcrumbs-1{padding: 0;}
	</style>
	<?php };?>
	<?php if(isset($breadcrumb->image)){;?>
	</div>
	<?php };?>
</div>