<!--<div class="cbp-l-project-title"><?php echo $datas->title;?></div>
<div class="cbp-l-project-subtitle"><?php echo $datas->tagline;?></div>-->


<?php if(!empty($gallery)){
	if(count($gallery) == 1){
		echo '<style>.cbp-nav-controls{display:none;}</style>';
	}
	;?>
<div class="cbp-slider">
    <ul class="cbp-slider-wrap">
	<?php foreach($gallery as $g){;?>
	<li class="cbp-slider-item">
		<img src="<?php echo base_url('assets');?>/<?php echo $zone;?>/gallery/<?php echo $g->image_square;?>" alt="">
	</li>
	<?php };?>
	</ul>
</div>
<?php };?>
<div class="cbp-l-project-container">
    <div class="cbp-l-project-desc">
        <div class="cbp-l-project-desc-title"><span><?php echo $datas->title;?></span> </div>
		<div class="col-md-7" style="padding: 0;">
			<img src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $datas->image_square;?>" style="width:100%">
		</div>
		<div class="col-md-5" style="">
			<div class="cbp-l-project-desc-text"><?php echo $datas->description;?></div>
		</div>
    </div>
    <div class="cbp-l-project-details">
        <div class="cbp-l-project-details-title"><span>Event Details</span></div>

        <ul class="cbp-l-project-details-list">
			
			<?php if($bc['category'] == 'weekly_event'){;?>
				<li><strong>Every</strong><?php echo date("l", strtotime($datas->datecreated));?></li>
			<?php }else{;?>
				<li><strong>Start</strong><?php echo date("l, d F Y", strtotime($datas->datecreated));?></li>
				<li><strong>End</strong><?php echo date("l, d F Y", strtotime($datas->dateupdated));?></li>
			<?php };?>
            <li><strong>Categories</strong><?php echo ucwords(str_replace('_',' ',$bc['category']));?></li>
            <li><?php echo $datas->tagline;?></li>
        </ul>
        <!--<a href="#" target="_blank" class="cbp-l-project-details-visit btn btn-sm c-btn-border-1x c-btn-square c-btn-dark c-btn-uppercase c-btn-bold">visit the site</a>
		-->
	</div>
</div>

<!--<div class="cbp-l-project-container">
    <div class="cbp-l-project-related">
        <div class="cbp-l-project-desc-title"><span>Related Event</span></div>

        <ul class="cbp-l-project-related-wrap">
            <li class="cbp-l-project-related-item">
                <a href="ajax/project7.html" class="cbp-singlePage cbp-l-project-related-link">
                    <img src="<?php echo base_url('assets');?>/img/content/stock/15.jpg" alt="">
                    <div class="cbp-l-project-related-title">Seemple* Music for iPad</div>
                </a>
            </li>
            <li class="cbp-l-project-related-item">
                <a href="ajax/project9.html" class="cbp-singlePage cbp-l-project-related-link">
                    <img src="<?php echo base_url('assets');?>/img/content/stock/16.jpg" alt="">
                    <div class="cbp-l-project-related-title">Workout Buddy</div>
                </a>
            </li>
            <li class="cbp-l-project-related-item">
                <a href="ajax/project10.html" class="cbp-singlePage cbp-l-project-related-link">
                    <img src="<?php echo base_url('assets');?>/img/content/stock/5.jpg" alt="">
                    <div class="cbp-l-project-related-title">Bills Bills Bills</div>
                </a>
            </li>
        </ul>

    </div>
</div>-->

<br>
<br>
<br>
<script>
setInterval( 
	function(){
		$('.cbp-nav-next').click();
	},3000 
);
</script>