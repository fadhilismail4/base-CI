<!-- BEGIN: PAGE CONTENT -->
<input type="hidden" id="data-eventgal" data-base-url="<?php echo base_url();?>" data-zone="<?php echo $zone;?>" data-url-zone="<?php echo $menu['link'];?>" data-base-modul="<?php echo $bc['module'];?>" data-url-first="list" data-url-second="list" data-id="<?php if(isset($bc['category'])){ echo $bc['category'];}else{ echo 'all';};?>" data-param="<?php echo $bc['module'];?>"/>

<div class="c-content-box c-size-md" id="event-gall" data-offset="0" data-param="event">
	<div class="container">
		<div class="cbp-panel">
			<div id="year" class="cbp-l-filters-buttonCenter">
				<?php if(isset($bc['category'])){;?>
				<?php if($bc['category'] == 'weekly_event'){;?>
					<?php foreach($month as $m => $value){
					 $months = date('F', mktime(0,0,0,$m, 1, date('Y')));?>
					<div class="cbp-filter-item" data-filter="<?php echo $months;?>" data-month="<?php echo $m;?>" data-year="<?php echo $m;?>" onClick="get_data_eventgal(<?php echo $m;?>)"> <?php echo $months;?>
					</div>
					<?php };?>
				<?php }else{;?>
					<?php foreach($years as $y){;?>
						<div class="cbp-filter-item" data-filter="<?php echo $y->year;?>" data-year="<?php echo $y->year;?>" onClick="get_data_eventgal(<?php echo $y->year;?>)"> <?php echo $y->year;?>
						</div>
					<?php };?>
				<?php };?>
				<?php }else{;?>
					<?php foreach($years as $y){;?>
						<div class="cbp-filter-item" data-filter="<?php echo $y->year;?>" data-year="<?php echo $y->year;?>" onClick="get_data_eventgal(<?php echo $y->year;?>)"> <?php echo $y->year;?>
						</div>
					<?php };?>
				<?php };?>
			</div>
			<div id="filters-container" class="month cbp-l-filters-buttonCenter">
				<div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> All
					<div class="cbp-filter-counter"></div>
				</div>
				<?php if(isset($bc['category'])){;?>
				<?php if($bc['category'] == 'weekly_event'){;?>
					<?php for ($m=1; $m<=7; $m++) {
					 $month = date('l', mktime(0,0,0,8,$m,2011));?>
					<div data-filter=".<?php echo $month;?>" class="cbp-filter-item"> <?php echo $month;?>
						<div class="cbp-filter-counter"></div>
					</div>
					<?php };?>
				<?php }else{;?>
					<?php for ($m=1; $m<=12; $m++) {
					 $month = date('F', mktime(0,0,0,$m, 1, date('Y')));?>
					<div data-filter=".<?php echo $month;?>" class="cbp-filter-item"> <?php echo $month;?>
						<div class="cbp-filter-counter"></div>
					</div>
					<?php };?>
				<?php };?>
				
				<?php }else{;?>
				<?php for ($m=1; $m<=12; $m++) {
				 $month = date('F', mktime(0,0,0,$m, 1, date('Y')));?>
				<div data-filter=".<?php echo $month;?>" class="cbp-filter-item"> <?php echo $month;?>
					<div class="cbp-filter-counter"></div>
				</div>
				<?php };?>
				<?php };?>
				
			</div>
			<div class="throbber-loader" id="loader" style="display:none"></div>
			<div id="grid-container" class="cbp cbp-l-grid-masonry-projects"></div>
			<div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
				<!--<a href="ajax/masonry-gallery/load-more.html" class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase">
					<span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
					<span class="cbp-l-loadMore-loadingText">LOADING...</span>
					<span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
				</a>-->
			</div>
		</div>
	</div>
</div>
<!-- END: PAGE CONTENT -->
<script>
</script>