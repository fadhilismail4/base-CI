<?php if(isset($style['testimonials'])){;?>
<?php 
$bg_image = base_url('assets/img/content/backgrounds/bg-38.jpg');
if(isset($style['testimonials-color'])){
	$bg = $style['testimonial-color'];//trigger dark or grey or white
	$c_bg = 'c-bg-'.$bg;
	$c_theme = 'c-theme';
	if($bg == 'dark'){
		$c_font = 'c-font-white';
	}else{
		$c_font = ' ';
	};
}else{
	$bg = '';
	$c_bg = 'c-theme-bg';
	$c_theme = '';
	$c_font = 'c-font-white';
}
/* 
$c_border = 'c-border-'.$style['parallax'].'-'.$bg;
$c_content = 'c-content-'.$style['parallax'];

$c = 'c-'.$style['parallax'];
$c_line = 'c-line-'.$style['parallax']; */

;?>
<?php if($style['testimonials'] == 'full-no-image'){;?>
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-1 -->
<div class="c-content-box c-size-lg <?php echo $c_bg;?>">
	<div class="container">
		<!-- Begin: testimonials 2 component -->
		<div class="c-content-testimonials-1" data-slider="owl" data-single-item="true" data-auto-play="5000">
			<!-- Begin: Title 1 component -->
			<div class="c-content-title-1">
				<h3 class="c-center <?php echo $c_font;?> c-font-uppercase c-font-bold">Let's See What Our Customers Said</h3>
				<div class="c-line-center c-theme-bg c-theme-darken"></div>
			</div>
			<!-- End-->
			<!-- Begin: Owlcarousel -->
			<div class="owl-carousel owl-theme">
				<div class="item">
					<div class="c-testimonial">
						<p>"JANGO is an international, privately held company that specializes in the start-up, promotion and operation of multiple online marketplaces"</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">John Snow</span>, CEO, Mockingbird </h3>
					</div>
				</div>
				<div class="item">
					<div class="c-testimonial">
						<p>"After co-founding the company in 2006 the group launched JANGO, the first digital marketplace which focused on rich multimedia web content"</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">Arya Stark</span>, CFO, Valar Dohaeris </h3>
					</div>
				</div>
				<div class="item">
					<div class="c-testimonial">
						<p>"It was the smoothest implementation process I have ever been through with JANGO�s process and schedule."</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">Arya Stark</span>, CFO, Valar Dohaeris </h3>
					</div>
				</div>
				<div class="item">
					<div class="c-testimonial">
						<p>"A system change is always stressful and JANGO did a great job of staying positive, helpful, and patient with us."</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">Arya Stark</span>, CFO, Valar Dohaeris </h3>
					</div>
				</div>
			</div>
			<!-- End-->
		</div>
		<!-- End-->
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-1 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-3 --
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/backgrounds/bg-3.jpg)"> IF HAVE IMAGE
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-3 -->
<?php }elseif($style['testimonials'] == 'full-with-image'){;?>
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-9 --
<div class="c-content-box c-size-lg">
	<div class="container">
		<div class="c-content-testimonials-4" data-navigation="true" data-slider="owl" data-single-item="true" data-auto-play="5000">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Let's See What Our Customers Say</h3>
				<div class="c-line-center c-theme-bg"></div>
			</div>
			<div class="owl-carousel c-theme">
				<div class="item">
					<div class="c-content-testimonial-4">
						<div class="c-content"> Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat diam dolor sit amet consectetuer adipiscing elit </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team4.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-4">
						<div class="c-content"> Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat diam dolor sit amet consectetuer adipiscing elit </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team3.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">John Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CTO, Google Inc</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-9 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-10 -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/backgrounds/bg-65.jpg)">
	<div class="container">
		<!-- Begin: testimonials 4 component -->
		<div class="c-content-testimonials-4" data-navigation="true" data-slider="owl" data-single-item="true" data-auto-play="5000">
			<!-- Begin: Title 1 component -->
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Let's See What Our Customers Say</h3>
				<div class="c-line-center c-theme-bg"></div>
			</div>
			<!-- End-->
			<!-- Begin: Owlcarousel -->
			<div class="owl-carousel c-theme">
				<div class="item">
					<div class="c-content-testimonial-4">
						<div class="c-content"> Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat diam dolor sit amet consectetuer adipiscing elit </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team2.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-4">
						<div class="c-content"> Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat diam dolor sit amet consectetuer adipiscing elit </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team1.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">John Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CTO, Google Inc</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End-->
		</div>
		<!-- End-->
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-10 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-11 --
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/backgrounds/bg-51.jpg)">
	<div class="container">
		<div class="c-content-testimonials-4 c-icon-white" data-navigation="true" data-slider="owl" data-single-item="true" data-auto-play="5000">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold c-font-white">Let's See What Our Customers Say</h3>
				<div class="c-line-center c-theme-bg"></div>
			</div>
			<div class="owl-carousel c-theme">
				<div class="item">
					<div class="c-content-testimonial-4">
						<div class="c-content c-font-white"> Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat diam dolor sit amet consectetuer adipiscing elit </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/team/team7.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name c-font-white">Mike Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-4">
						<div class="c-content c-font-white"> Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat diam dolor sit amet consectetuer adipiscing elit </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/team/team3.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name c-font-white">John Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CTO, Google Inc</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-11 -->
			
<?php }elseif($style['testimonials'] == 'full'){;?>			
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-6-4 -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/backgrounds/bg-46.jpg)">
	<div class="container">
		<div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Customer Reviews</h3>
				<div class="c-line-center c-theme-bg"></div>
				<p class="c-center">Lorem ipsum dolor sit amet et consectetuer adipiscing elit</p>
			</div>
			<div class="owl-carousel owl-theme c-theme">
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team1.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Walmart</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team6.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team4.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Nina Kunis</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team8.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team7.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Jeep</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-6-4 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-6-1 -->
<div class="c-content-box c-size-md c-bg-grey-1">
	<div class="container">
		<div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Customer Reviews</h3>
				<div class="c-line-center c-theme-bg"></div>
				<p class="c-center c-font-uppercase1">Lorem ipsum dolor sit amet et consectetuer adipiscing elit</p>
			</div>
			<div class="owl-carousel owl-theme c-theme">
				<div class="item">
					<div class="c-content-testimonial-3 c-option-default">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team1.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Walmart</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-default">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team6.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-default">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team4.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Nina Kunis</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-default">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team8.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-default">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team7.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Jeep</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-6-1 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-6-2 -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/backgrounds/bg-37.jpg)">
	<div class="container">
		<div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-white c-font-bold">Customer Reviews</h3>
				<div class="c-line-center c-theme-bg"></div>
				<p class="c-center c-font-white">Lorem ipsum dolor sit amet et consectetuer adipiscing elit</p>
			</div>
			<div class="owl-carousel owl-theme c-theme">
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team1.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Walmart</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team6.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team4.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Nina Kunis</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team8.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team7.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Jeep</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-6-2 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-6-3 -->
<div class="c-content-box c-size-md c-bg-white">
	<div class="container">
		<div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Customer Reviews</h3>
				<div class="c-line-center c-theme-bg"></div>
				<p class="c-center c-font-uppercase1">Lorem ipsum dolor sit amet et consectetuer adipiscing elit</p>
			</div>
			<div class="owl-carousel owl-theme c-theme">
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team1.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Walmart</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team6.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team4.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Nina Kunis</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team8.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-light">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team7.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Jeep</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-6-3 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-6-5 -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/backgrounds/bg-48.jpg)">
	<div class="container">
		<div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-white c-font-bold">Customer Reviews</h3>
				<div class="c-line-center c-theme-bg"></div>
				<p class="c-center c-font-white">Lorem ipsum dolor sit amet et consectetuer adipiscing elit</p>
			</div>
			<div class="owl-carousel owl-theme c-theme">
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team1.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Doe</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Walmart</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team6.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team4.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Nina Kunis</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team8.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Ashley Benson</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, Loop Inc</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="c-content-testimonial-3 c-option-dark-transparent">
						<div class="c-content"> Lorem ipsum dolor sit amet et consectetuer adipiscing elit, sed nonummy nib euismod tincid unt ut ed laoreet dolore sit amet consectetuer adipiscing. </div>
						<div class="c-person">
							<img src="<?php echo base_url('assets');?>/img/content/avatars/team7.jpg" class="img-responsive">
							<div class="c-person-detail c-font-uppercase">
								<h4 class="c-name">Mark Jeep</h4>
								<p class="c-position c-font-bold c-theme-font">CFO, ERA Tech</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-6-5 -->
            

<?php }else{;?>
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-4 -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/stock3/51.jpg)">
	<div class="container">
		<div class="c-content-title-1">
			<h3 class="c-center c-font-white c-font-uppercase c-font-bold">Our Satisfied Customers</h3>
			<div class="c-line-center c-theme-bg"></div>
		</div>
		<div class="row c-margin-t-60">
			<div class="col-md-6">
				<div class="c-content-testimonial-2-slider" data-slider="owl" data-single-item="true" data-auto-play="6000">
					<div class="c-title c-font-uppercase c-font-bold c-theme-bg">Clients</div>
					<div class="owl-carousel owl-theme c-theme owl-single">
						<div class="item">
							<div class="c-content-testimonial-2" style="min-height: 320px;">
								<div class="c-testimonial c-font-uppercase c-font-bold">JANGO the best HTML theme I've purchased in months!</div>
								<div class="c-author">
									<div class="c-portrait" style="background-image: url(<?php echo base_url('assets');?>/img/content/team/team8.jpg)"></div>
									<div class="c-name c-font-uppercase">Ashley Benson</div>
									<p class="c-position c-theme c-font-uppercase">Wallmart</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-testimonial-2" style="min-height: 320px;">
								<div class="c-testimonial c-font-uppercase c-font-bold">Quick and extremely easy to use with awesome customer support</div>
								<div class="c-author">
									<div class="c-portrait" style="background-image: url(<?php echo base_url('assets');?>/img/content/team/team6.jpg)"></div>
									<div class="c-name c-font-uppercase">John Smith</div>
									<p class="c-position c-theme c-font-uppercase">Loop Inc.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-testimonial-2-slider" data-slider="owl" data-single-item="true" data-auto-play="6000">
					<div class="c-title c-font-uppercase c-font-bold c-theme-bg">Partners</div>
					<div class="owl-carousel owl-theme c-theme owl-single">
						<div class="item">
							<div class="c-content-testimonial-2" style="min-height: 320px;">
								<div class="c-testimonial c-font-uppercase c-font-bold">JANGO customer service was the greatest I've encountered so far!</div>
								<div class="c-author">
									<div class="c-portrait" style="background-image: url(<?php echo base_url('assets');?>/img/content/team/team11.jpg)"></div>
									<div class="c-name c-font-uppercase">Mary Jane</div>
									<p class="c-position c-theme c-font-uppercase">Comic Store</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-testimonial-2" style="min-height: 320px;">
								<div class="c-testimonial c-font-uppercase c-font-bold">JANGO's quick deployment saved me days of work!</div>
								<div class="c-author">
									<div class="c-portrait" style="background-image: url(<?php echo base_url('assets');?>/img/content/team/team3.jpg)"></div>
									<div class="c-name c-font-uppercase">Paul Garner</div>
									<p class="c-position c-theme c-font-uppercase">Ounce Inc.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-4 -->
<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-5 -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url('assets');?>/img/content/backgrounds/bg-29.jpg)">
	<div class="container">
		<div class="c-content-title-1">
			<h3 class="c-center c-font-white c-font-uppercase c-font-bold">Endless Satisfaction</h3>
			<div class="c-line-center c-theme-bg"></div>
		</div>
		<div class="row c-margin-t-60">
			<div class="col-md-6">
				<div class="c-content-testimonial-2-slider" data-slider="owl" data-single-item="true" data-auto-play="6000">
					<div class="c-title c-font-uppercase c-font-bold c-theme-bg">Customers</div>
					<div class="owl-carousel owl-theme c-theme owl-single">
						<div class="item">
							<div class="c-content-testimonial-2" style="min-height: 320px;">
								<div class="c-testimonial c-font-uppercase c-font-bold">JANGO the best HTML theme I've purchased in months!</div>
								<div class="c-author">
									<div class="c-portrait" style="background-image: url(<?php echo base_url('assets');?>/img/content/team/team8.jpg)"></div>
									<div class="c-name c-font-uppercase">Ashley Benson</div>
									<p class="c-position c-theme c-font-uppercase">Wallmart</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="c-content-testimonial-2" style="min-height: 320px;">
								<div class="c-testimonial c-font-uppercase c-font-bold">Quick and extremely easy to use with awesome customer support</div>
								<div class="c-author">
									<div class="c-portrait" style="background-image: url(<?php echo base_url('assets');?>/img/content/team/team6.jpg)"></div>
									<div class="c-name c-font-uppercase">John Smith</div>
									<p class="c-position c-theme c-font-uppercase">Loop Inc.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="c-content-v-center c-bg-red" style="height: 320px;">
					<div class="c-wrapper">
						<div class="c-body c-padding-20 c-center">
							<h3 class="c-font-25 c-line-height-34 c-font-white c-font-uppercase c-font-bold"> Nothing is impossible for JANGO
								<br>Highly Flexible, always growing</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-5 -->
<?php };?>
<?php };?>