<style>
.close-second{
	cursor: pointer;
}
@media screen and (min-width:767px){
	.m_height{
		height: 100% !important;
	}
	.m_height2{
		height: 50% !important;
	}
}
</style>
<div id="first-page" class="c-content-box c-overflow-hide c-bs-grid-reset-space" style="display:none">
	<div class="row">
		<div class="col-md-6">
			<div class="c-content-product-5">
				<div class="c-bg-img-center m_height" style="height: 800px;background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][0]->image_square;?>)">
					<div class="c-detail c-bg-dark c-bg-opacity-2" style="width:100%">
						<a href="<?php echo $menu['link'];?>/product" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
						<h3 class="c-title c-font-uppercase c-font-bold c-font-white c-font-90"><?php echo $datas['product_list']['datas'][0]->title;?></h3>
						<p class="c-desc c-font-white c-font-17"><?php echo $datas['product_list']['datas'][0]->tagline;?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div id="except" class="row">
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20"><?php echo $datas['product_list']['datas'][1]->title;?> </h3>
								<a href="<?php echo $menu['link'];?>/about-moonshine" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Read Now</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object m_height" data-height="height" style="height:400px;background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][1]->image_square;?>);"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
									<span class=""><?php echo $datas['product_list']['datas'][2]->title;?></span>
								</h3>
								<a href="<?php echo $menu['link'];?>/help" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Click Now</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object m_height" data-height="height" style="height:400px;background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][2]->image_square;?>);"></div>
					</div>
				</div>
			</div>
			<div id="except2" class="row">
				<div class="col-md-12">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
									<span class=""><?php echo $datas['product_list']['datas'][3]->title;?></span>
								</h3>
								<a href="<?php echo $menu['link'];?>/<?php echo str_replace(' ','-',strtolower($datas['product_list']['datas'][3]->title));?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">View Now</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object m_height" data-height="height" style="height:400px;background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][3]->image_square;?>)"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="second-page" class="c-content-box c-overflow-hide c-bs-grid-reset-space" style="display:none">
	<div class="row">
		<div class="col-md-12">
			<div class="c-content-product-5">
				<div class="close-second c-bg-img-center m_height" style="height:800px;background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][4]->image_square;?>)">
					<!--<div class="col-md-12 c-detail c-bg-dark c-bg-opacity-2 c-center" style="padding: 30px 45px;">
						<a href="javascript:;" data-toggle="modal" data-target="#signup-form" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square c-center col-md-6 col-xs-12">Sign Up</a>
						<a href="javascript:;" data-toggle="modal" data-target="#login-form" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square c-center col-md-6 col-xs-12">Log In</a>
						<a href="#" class="close-second btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square c-center col-md-12 col-xs-12">Enter Site</a>
					</div>-->
				</div>
			</div>
		</div>
	</div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) { 
	var createCookie = function(name, value, days) {
		var expires;
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 3 * 60 * 60 * 1000));
			expires = "; expires=" + date.toGMTString();
		}
		else {
			expires = "";
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}
	function getCookie(c_name) {
		if (document.cookie.length > 0) {
			c_start = document.cookie.indexOf(c_name + "=");
			if (c_start != -1) {
				c_start = c_start + c_name.length + 1;
				c_end = document.cookie.indexOf(";", c_start);
				if (c_end == -1) {
					c_end = document.cookie.length;
				}
				return unescape(document.cookie.substring(c_start, c_end));
			}
		}
		return "";
	}
	//createCookie('first','false','1');
	/* if(getCookie('first') == 'true'){
		$('#first-page').show();
	}else{ */
		$('#second-page').show();
		$('.c-bg-img-center').parents("*").addClass('m_height');
		$('#except').addClass('m_height2');
		$('#except2').addClass('m_height2');
		$('.close-second').on("click", function(e) {
			e.preventDefault();
			//createCookie('first','true','1');
			$('#first-page').slideDown();
			$('#second-page').slideUp();
		});
		
		if($(window).width()<769){
			var h = $(window).height();
			$('.close-second').css('height',h);
			$('.c-bg-img-center').parents("*").removeClass('m_height');
			$('#except').removeClass('m_height2');
			$('#except2').removeClass('m_height2');
		}
		
	//}
	//console.log(getCookie('first'));
});
</script>