<?php if(isset($style['banner'])){;?>
<section class="c-layout-revo-slider c-layout-revo-slider-7" id="banner">
	<div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
		<div class="tp-banner">
			<ul>
				<?php foreach($banner as $b){
					$mime = mime_content_type('./assets/'.$zone.'/homepage/'.$b->image_square);
					if(strstr($mime, "video/")){
						$cek_file = 'video';
					}else if(strstr($mime, "image/")){
						$cek_file = 'image';
					}
					;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="500">
						<?php if($cek_file == 'image'){;?>
							<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $b->image_square;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<?php }else{;?>
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img src="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png" alt="">
							<div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false"
							data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%" data-videopreload="meta" data-videomp4="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $b->image_square;?>" data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"
							data-aspectratio="16:9" data-volume="mute" data-videoposter="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png"> </div>
							<div class="tp-caption arrowicon customin rs-parallaxlevel-0 visible-xs" data-x="center" data-y="bottom" data-hoffset="0" data-voffset="-60" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="500" data-start="2000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 13;">
								<div class="rs-slideloop" data-easing="Power3.easeInOut" data-speed="0.5" data-xs="-5" data-xe="5" data-ys="0" data-ye="0">
									<span class="c-video-hint c-font-15 c-font-sbold c-font-center c-font-dark"> Tap to play video
										<i class="icon-control-play"></i>
									</span>
								</div>
							</div>
							<!--END-->
						<?php };?>
						
						<!--BEGIN: MAIN TITLE -->
						<?php if($style['banner'] != 'no_detail'){;?>
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-280" data-voffset="-100" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							<?php echo $b->title;?>
							</h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-345" data-voffset="30" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 <?php echo word_limiter($b->description,6);?>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-445" data-voffset="140" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
						<?php };?>
					</li>
				<?php };?>
			</ul>
		</div>
	</div>
</section>
<?php };?>

<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
				<span class="c-bg-white">Most Popular</span>
			</h3>
		</div>
		<div class="row">
			<div data-slider="owl" data-items="4" data-auto-play="8000">
				<div class="owl-carousel owl-theme c-theme owl-small-space">
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/shop5/18.png);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim">Samsung Galaxy Note 4</p>
								<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-2-2 -->

<?php if(isset($style['divider'])){;?>
<?php if($style['divider'] == 'parallax'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-5 -->
<?php if(!empty($divider)){
	$c_datas = count($divider);
	;?>
<?php if($c_datas == 1){;?>	
<div class="c-content-box c-size-md c-bg-parallax" style="max-height:20%;background-image: url(<?php echo site_url('assets');?>/<?php echo $zone;?>/<?php echo $bc['module'];?>/<?php echo $divider[0]->image_square;?>)">
	<div class="container">
		<div class="c-content-bar-4">
			<h3 class="c-font-uppercase c-font-bold">
				<?php echo $divider[0]->title;?>
				<br/>
				<?php echo $divider[0]->tagline;?>
			</h3>
			<div class="c-actions">
				<a href="#" class="btn btn-md c-btn-border-2x c-btn-square c-btn-white c-btn-uppercase c-btn-bold c-margin-b-100">Learn More</a>
			</div>
		</div>
	</div>
</div>
<?php }else{;?>
<section class="c-layout-revo-slider c-layout-revo-slider-2" id="banner">
	<div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
		<div class="tp-banner">
			<ul>
				<?php foreach($divider as $b){;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
						<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $b->image_square;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<!--BEGIN: MAIN TITLE -->
						<?php if($style['banner'] != 'no_detail'){;?>
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-280" data-voffset="-100" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							<?php echo $b->title;?>
							</h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-345" data-voffset="30" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 <?php echo word_limiter($b->description,6);?>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-445" data-voffset="140" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
						<?php };?>
					</li>
				<?php };?>
			</ul>
		</div>
	</div>
</section>
<?php };?>
<?php };?>
<!-- END: CONTENT/BARS/BAR-5 -->
<?php }else{;?>

<?php if($style['divider'] == 'center-border'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-6 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-bar-1 c-opt-1 c-bordered c-theme-border c-shadow">
			<h3 class="c-font-uppercase c-font-bold">JANGO is optimized to every development</h3>
			<p class="c-font-uppercase"> JANGO is build to completely support your web projects by
				<br> providing
				<strong>Ultra Flexibility</strong>,
				<strong>Increased productivity</strong> and
				<strong>Top quality</strong>
			</p>
			<button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-btn-dark c-btn-uppercase c-btn-bold c-margin-r-40">Explore</button>
			<button type="button" class="btn btn-md c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Purchase</button>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-6 -->	
<?php }elseif($style['divider'] == 'center'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-1 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-bar-1 c-opt-1">
			<h3 class="c-font-uppercase c-font-bold">Get JANGO as your development companion</h3>
			<p class="c-font-uppercase"> JANGO is built to completely support your web projects by
				<br> providing
				<strong>Ultra Flexibility</strong>,
				<strong>Increased productivity</strong> and
				<strong>Top quality</strong>
			</p>
			<button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-btn-dark c-btn-uppercase c-btn-bold c-margin-r-40 hide">Learn More</button>
			<button type="button" class="btn btn-md c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Purchase</button>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-1 -->
<?php }elseif($style['divider'] == 'left'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-3 -->
<div class="c-content-box c-size-md c-bg-dark">
	<div class="container">
		<div class="c-content-bar-3">
			<div class="row">
				<div class="col-md-7">
					<div class="c-content-title-1">
						<h3 class="c-font-uppercase c-font-bold">DEDICATED SUPPORT</h3>
						<p class="c-font-uppercase">JANGO comes with top-of-the-line support teams to ensure that we provide the best experience for our customers</p>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-2">
					<div class="c-content-v-center" style="height: 90px;">
						<div class="c-wrapper">
							<div class="c-body">
								<button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-theme-btn c-btn-uppercase c-btn-bold">Get Support</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-3 -->
<?php }elseif($style['divider'] == 'right'){;?>
<!-- BEGIN: CONTENT/BARS/BAR-2 -->
<?php
$c_bg = 'c-bg-grey-1'; // or c-bg-white
;?>
<div class="c-content-box c-size-md c-bg-grey-1">
	<div class="container">
		<div class="c-content-bar-2 c-opt-1">
			<div class="row" data-auto-height="true">
				<div class="col-md-6">
					<!-- Begin: Title 1 component -->
					<div class="c-content-title-1" data-height="height">
						<h3 class="c-font-uppercase c-font-bold">Meet JANGO. The Theme Of 2015</h3>
						<p class="c-font-uppercase c-font-sbold"> The Ever growing Multipurpose Theme. Ultra responsive, Clean Coding with top quality modern design trends. </p>
						<button type="button" class="btn btn-md c-btn-border-2x c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Purchase</button>
					</div>
					<!-- End-->
				</div>
				<div class="col-md-6">
					<div class="c-content-v-center c-bg-red" data-height="height">
						<div class="c-wrapper">
							<div class="c-body">
								<h3 class="c-font-white c-font-bold">Just another option from an endless list of choices within JANGO.</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/BARS/BAR-2 -->
<?php };?>

<?php };?>
<?php };?>