<style>
.c-content-testimonials-4 .c-person > img{
	max-width: 200px;
	width: 200px;
}
.c-content-testimonials-4 >.owl-theme .owl-controls{
	margin-top: 20px;
}
.c-content-testimonials-4 >.owl-theme .owl-controls > .owl-buttons{
	display:none;
}
</style>
<?php if(isset($style['banner'])){;?>
<section class="c-layout-revo-slider c-layout-revo-slider-7" id="banner">
	<div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
		<div class="tp-banner">
			<ul>
				<?php foreach($banner as $b){
					if(!empty($b->image_square)){
					$mime = mime_content_type('./assets/'.$zone.'/homepage/'.$b->image_square);
					if(strstr($mime, "video/")){
						$cek_file = 'video';
					}else if(strstr($mime, "image/")){
						$cek_file = 'image';
					}
					;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="500">
						<?php if($cek_file == 'image'){;?>
							<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $b->image_square;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<?php }else{;?>
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img src="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png" alt="">
							<div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false"
							data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%" data-videopreload="meta" data-videomp4="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $b->image_square;?>" data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"
							data-aspectratio="16:9" data-volume="100" data-videoposter="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png"> </div>
							<div class="tp-caption arrowicon customin rs-parallaxlevel-0 visible-xs" data-x="center" data-y="bottom" data-hoffset="0" data-voffset="-60" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="500" data-start="2000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 13;">
								<div class="rs-slideloop" data-easing="Power3.easeInOut" data-speed="0.5" data-xs="-5" data-xe="5" data-ys="0" data-ye="0">
									<span class="c-video-hint c-font-15 c-font-sbold c-font-center c-font-dark"> Tap to play video
										<i class="icon-control-play"></i>
									</span>
								</div>
							</div>
							<!--END-->
						<?php };?>
						
						<!--BEGIN: MAIN TITLE -->
						<?php if($style['banner'] != 'no_detail'){;?>
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-280" data-voffset="-100" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							<?php echo $b->title;?>
							</h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-345" data-voffset="30" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 <?php echo word_limiter($b->description,6);?>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-445" data-voffset="140" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
						<?php };?>
					</li>
				<?php };?>
				<?php };?>
			</ul>
		</div>
	</div>
</section>

<?php };?>


<?php //if(isset($style['testimonials'])){;?>
<?php 
$bg_image = base_url('assets/img/content/backgrounds/bg-38.jpg');///img/content/backgrounds/bg-65.jpg
if(isset($style['testimonials-color'])){
	$bg = $style['testimonial-color'];//trigger dark or grey or white
	$c_bg = 'c-bg-'.$bg;
	$c_theme = 'c-theme';
	if($bg == 'dark'){
		$c_font = 'c-font-white';
	}else{
		$c_font = ' ';
	};
}else{
	$bg = '';
	$c_bg = 'c-theme-bg';
	$c_theme = '';
	$c_font = 'c-font-white';
}
/* 
$c_border = 'c-border-'.$style['parallax'].'-'.$bg;
$c_content = 'c-content-'.$style['parallax'];

$c = 'c-'.$style['parallax'];
$c_line = 'c-line-'.$style['parallax']; */

;?>
<div class="c-content-box c-size-lg c-bg-parallax" style="">
	<div class="container">
		<!-- Begin: testimonials 4 component -->
		<div class="c-content-testimonials-4 col-md-8" data-navigation="true" data-slider="owl" data-single-item="true" data-auto-play="5000">
			<!-- Begin: Title 1 component -->
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Testimonials</h3>
				<div class="c-line-center c-theme-bg"></div>
			</div>
			<!-- End-->
			<!-- Begin: Owlcarousel -->
			<div class="owl-carousel c-theme">
				<?php foreach($testimonial as $sk_data){
					$m_footer = 'testimonial';
						if(isset($sk_data->img)){
							$sk_data_img = $sk_data->img;
						}else{
							$sk_data_img = base_url('assets').'/'.$zone.'/'.$m_footer.'/'.$sk_data->image_square;
						}
						if(isset($sk_data->url)){
							$sk_data_url = $sk_data->url;
						}else{
							$sk_data_url = $menu['link'].'/'.$m_footer.'/'.strtolower(str_replace(' ','_',$sk_data->category)).'/'.strtolower(str_replace(' ','_',$sk_data->title));
						}
					;?>
					<div class="item">
						<div class="c-content-testimonial-4">
							<div class="c-content"> <?php echo $sk_data->description;?> </div>
							<div class="c-person">
								<img src="<?php echo $sk_data_img;?>" style="border-radius: 0;" class="img-responsive">
								<div class="c-person-detail c-font-uppercase">
									<a href="<?php echo $sk_data_url;?>"><h4 class="c-name"><?php echo $sk_data->title;?></h4></a>
									<p class="c-position c-font-bold c-theme-font"><?php echo $sk_data->category;?></p>
								</div>
							</div>
						</div>
					</div>
				<?php };?>
			</div>
			<!-- End-->
		</div>
		<div class="col-md-4 c-theme-bg">
			<div class="c-padding-20 c-margin-t-40 c-bg-img-bottom-right c-theme-bg" style="">
			
				<div class="c-content-title-1 c-margin-t-20">
					<h3 class="c-font-uppercase c-font-bold c-font-white">Become Agent and Reseller</h3>
					<h1 class="" style="text-align:center">
						<a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#signup-form-reseller" class="c-btn-border-opacity-04 c-btn btn-no-focus btn btn-sm c-btn-border-1x c-btn-white c-btn-uppercase c-btn-sbold clearfix col-md-12 c-margin-b-20" style="margin-right: 0;margin-left: 0;">
							<i class="icon-user"></i> Register
						</a>
					</h1>
					<p class="c-font-thin c-font-white">Please register, by clicking 'Register' button</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php// };?>