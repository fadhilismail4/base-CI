<?php if($zone == 'shisha'){;?>
<?php if(!empty($gallery)){;?>
<section class="container c-layout-revo-slider2 c-layout-revo-slider-7" id="banner">
	<div class="tp-banner-container">
		<div class="tp-banner">
			<ul>
				<?php foreach($gallery as $b){
					$mime = mime_content_type('./assets/'.$zone.'/gallery/'.$b->image_square);
					if(strstr($mime, "video/")){
						$cek_file = 'video';
					}else if(strstr($mime, "image/")){
						$cek_file = 'image';
					}
					;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="500">
						<?php if($cek_file == 'image'){;?>
							<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/gallery/<?php echo $b->image_square;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<?php }else{;?>
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img src="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png" alt="">
							<div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false"
							data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%" data-videopreload="meta" data-videomp4="<?php echo base_url('assets');?>/<?php echo $zone;?>/gallery/<?php echo $b->image_square;?>" data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"
							data-aspectratio="16:9" data-volume="100" data-videoposter="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png"> </div>
							<div class="tp-caption arrowicon customin rs-parallaxlevel-0 visible-xs" data-x="center" data-y="bottom" data-hoffset="0" data-voffset="-60" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="500" data-start="2000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 13;">
								<div class="rs-slideloop" data-easing="Power3.easeInOut" data-speed="0.5" data-xs="-5" data-xe="5" data-ys="0" data-ye="0">
									<span class="c-video-hint c-font-15 c-font-sbold c-font-center c-font-dark"> Tap to play video
										<i class="icon-control-play"></i>
									</span>
								</div>
							</div>
							<!--END-->
						<?php };?>
					</li>
				<?php };?>
			</ul>
		</div>
	</div>
</section>
<?php };?>
<?php };?>

<style>
.mobile_view{
	padding: 5px 14px !important;
}
@media(max-width: 767px){
	.mobile_view{
		margin-top: 30px;
		padding: 5px 14px !important;
	}
	.c-shop-result-filter-1 .c-filter{
		padding-right: 20px !important;
	}
}
@media(max-width: 500px){
	.mobile_view{
		margin-top: 20px;
		padding: 5px 14px !important;
	}
	.c-shop-result-filter-1 .c-filter{
		padding-right: 20px !important;
	}
}

</style>
<input type="hidden" id="data-product" value="grid" data-base-url="<?php echo base_url();?>" data-zone="<?php echo $zone;?>" data-url-zone="<?php echo $menu['link'];?>" data-base-modul="<?php echo $bc['module'];?>" data-url-first="list" data-url-second="list" data-id="<?php if(isset($bc['category'])){ echo $bc['category'];}else{ echo 'all';};?>" data-param="<?php echo $bc['module'];?>"/>
<div class="container">
	<?php if($zone == 'moonshine'){;?>
	<style>
	.slider-track > div {
		background:#000 !important;
	}
	.c-shop-filter-search-1 > li > label {
		border-bottom: 1px solid #000;
		width:100%;
		margin-bottom: 15px;	
	}
	.slider-horizontal{
		width: 100% !important;
	}
	</style>
	<div class="c-layout-sidebar-menu c-theme ">
		<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU-2 -->
		<div class="c-sidebar-menu-toggler">
			<h3 class="c-title c-font-uppercase c-font-bold">Navigation</h3>
			<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
				<span class="c-line"></span>
				<span class="c-line"></span>
				<span class="c-line"></span>
			</a>
		</div>
		<!-- BEGIN: CONTENT/SHOPS/SHOP-FILTER-SEARCH-1 -->
		<ul class="c-shop-filter-search-1 list-unstyled" id="sidebar-menu-1">
			<li>
				<label class="control-label c-font-uppercase c-font-bold">Price Range (<?php echo $sidebar['price_range']['currency'];?>)</label>
				<div onclick="get_data_product(0);" class="c-price-range-slider c-theme-1 input-group" style="padding-right:10px;padding-left:10px;width: 100%;">
					<input id="data-product-range" type="text" class="c-price-slider" value="" data-slider-min="<?php echo $sidebar['price_range']['min'];?>" data-slider-max="<?php echo $sidebar['price_range']['max'];?>" data-slider-step="1" data-slider-value="[<?php echo $sidebar['price_range']['min'];?>,<?php echo $sidebar['price_range']['max'];?>]"> 
				</div>
				<span><?php echo number_format($sidebar['price_range']['min'],2,',','.');?></span>
				<span style="float: right;"><?php echo number_format($sidebar['price_range']['max'],2,',','.');?></span>
			</li>
			<li>
				<label class="control-label c-font-uppercase c-font-bold"><?php echo $sidebar['tags'];?></label>
				<div class="c-radio-list" onchange="get_data_product(0);">
					<div class="c-radio">
						<input type="radio" id="semua" value="" class="c-radio" name="tags" checked="">
						<label for="semua" class="c-font-bold c-font-20">
							<span class="inc"></span>
							<span class="check"></span>
							<span class="box"></span></label>
						<span>ALL</span>	
					</div>
					<?php foreach($tags as $t){;?>
					<div class="c-radio">
						<input type="radio" id="<?php echo $t->tags;?>" value="<?php echo $t->tags;?>" class="c-radio" name="tags">
						<label for="<?php echo $t->tags;?>" class="c-font-bold c-font-20">
							<span class="inc"></span>
							<span class="check"></span>
							<span class="box"></span></label>
						<span><?php echo strtoupper($t->tags);?></span>		
					</div>
					<?php };?>
				</div>
			</li>
		</ul>
	</div>
	<?php }else{;?>
	<div class="c-layout-sidebar-menu c-theme ">
		<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU -->
		<div class="c-sidebar-menu-toggler">
			<h3 class="c-title c-font-uppercase c-font-bold">Navigation</h3>
			<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
				<span class="c-line"></span>
				<span class="c-line"></span>
				<span class="c-line"></span>
			</a>
		</div>
		<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
			<li class="c-active">
				<a href="<?php echo $menu['link'].'/shop/product/';?>">All Categories
					<span class="c-arrow"></span>
				</a>
			</li>
			<?php foreach($category as $c){
				$c_url = $menu['link'].'/shop/product/'.str_replace(' ','_',strtolower($c->title));
				?>
			<?php if(!empty($c->child)){;?>
			<?php if($c->parent_id == 0){;?>
				<li class="c-dropdown c-active c-open">
					<a href="javascript:;" class="c-toggler"><?php echo $c->title;?>
						<span class="c-arrow"></span>
					</a>
					<ul class="c-dropdown-menu ">
					<?php }else{;?>
						<li class="">
							<a href="<?php echo $c_url;?>" class=""><?php echo $c->title;?>
								<span class="c-arrow"></span>
							</a>
						</li>
					<?php };?>
					
					<?php foreach($c->child as $c_c){
						$c_c_url = $menu['link'].'/shop/product/'.str_replace(' ','_',strtolower($c_c->title));
						?>
						<li class="">
							<a href="<?php echo $c_c_url;?>" class=""><?php echo $c_c->title;?>
								<span class="c-arrow"></span>
							</a>
						</li>
					<?php };?>
				<?php if($c->parent_id == 0){;?>		
					</ul>
				</li>
				<?php };?>
			<?php }else{;?>
				<li class="c-dropdown c-active">
					<a href="<?php echo $c_url;?>" class=""><?php echo $c->title;?>
					</a>
				</li>
			<?php };?>
			<?php };?>
		</ul>
		<!--<div class="c-padding-20 c-margin-t-40 c-bg-grey-1 c-bg-img-bottom-right" style="background-image:url(assets/base/img/content/misc/feedback_box_2.png)">
			<div class="c-content-title-1 c-margin-t-20">
				<h3 class="c-font-uppercase c-font-bold">Have a question?</h3>
				<div class="c-line-left"></div>
				<form action="#">
					<div class="input-group input-group-lg c-square">
						<input type="text" class="form-control c-square" placeholder="Ask a question" />
						<span class="input-group-btn">
							<button class="btn c-theme-btn c-btn-square c-btn-uppercase c-font-bold" type="button">Go!</button>
						</span>
					</div>
				</form>
				<p class="c-font-thin">Ask your questions away and let our dedicated customer service help you look through our FAQs to get your questions answered!</p>
			</div>
		</div>-->
	</div>
	<?php };?>
	
	<div class="c-layout-sidebar-content">
		<input type="hidden" name="data-product-offset" id="data-product-offset" value="0" />
		<div class="c-shop-result-filter-1 clearfix form-inline">
			<div class="c-filter">
				<label class="control-label c-font-16">Show:</label>
				<select class="form-control c-square c-theme c-input select-grid" id="data-product-show-number" onchange="get_data_product(0);" style="display:none;">
					<option value="6">6</option>
					<option value="12" selected="selected">12</option>
					<option value="18">18</option>
					<option value="24">24</option>
					<option value="30">30</option>
				</select>
				<select class="form-control c-square c-theme c-input select-list" id="data-product-show-number" onchange="get_data_product(0);">
					<option value="5" selected="selected">5</option>
					<option value="10">10</option>
					<option value="15">15</option>
					<option value="20">20</option>
					<option value="25">25</option>
				</select>
			</div>
			<div class="c-filter">
				<label class="control-label c-font-16">Sort&nbsp;By:</label>
				<select class="form-control c-square c-theme c-input" id="data-product-show-sort" onchange="get_data_product(0);">
					<?php if($zone=='moonshine'){;?>
					<option value="date" data-sort="asc" selected="selected">What's New</option>
					<option value="publish" data-sort="asc">Price (Low - High)</option>
					<option value="publish" data-sort="desc">Price (High - Low)</option>
					<option value="discount" data-sort="desc">Special Price</option>
					<?php }else{;?>
					<option value="title" data-sort="asc" selected="selected">Name (A - Z)</option>
					<option value="title" data-sort="desc">Name (Z - A)</option>
					<option value="publish" data-sort="asc">Price (Low - High)</option>
					<option value="publish" data-sort="desc">Price (High - Low)</option>
					<option value="discount" data-sort="asc">Discount (Low - High)</option>
					<option value="discount" data-sort="desc">Discount (High - Low)</option>
					<?php };?>
					
				</select>
			</div>
			<div class="c-filter">
				<label class="control-label c-font-16">Go to Page:</label>
				<select class="form-control c-square c-theme c-input" id="data-product-go-to-page" onchange="get_data_product('page');">
				</select>
			</div>
			<div class="c-filter">
				<button id="view-list" class="mobile_view btn btn-xs c-btn-dark c-btn-border-2x"><i class="fa fa-th-list"></i></button>
				<button id="view-grid" class="mobile_view btn btn-xs c-btn-dark c-btn-border-2x"><i class="fa fa-th-large"></i></button>
			</div>
		</div>
		<div class="c-margin-t-20"></div>
		<div class="throbber-loader" id="loader" style="display:none"></div>
		<div id="view-product"></div>
		<div class="c-margin-t-20"></div>
	</div>
</div>
<?php if($zone == 'shisha'){;?>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
	if ($(window).width() < 960) {
		$('#banner').removeClass('container');
		var slider = $('.c-layout-revo-slider2 .tp-banner');
		var cont = $('.c-layout-revo-slider2 .tp-banner-container');
		var api = slider.show().revolution({
			delay: 3000,    
			startwidth:1170,
			startheight: 420,
			navigationType: "hide",
			navigationArrows: "solo",
			touchenabled: "on",
			onHoverStop: "on",
			keyboardNavigation: "off",
			navigationType:"bullet",
			navigationArrows:"",
			navigationStyle:"round c-tparrows-hide c-theme",
			navigationHAlign: "right",
			navigationVAlign: "bottom", 
			navigationHOffset:60,
			navigationVOffset:60,
			spinner: "spinner2",
			fullScreen: 'on',   
			fullScreenAlignForce: 'on', 
			fullScreenOffsetContainer: '.c-layout-header',
			shadow: 0,
			fullWidth: "off",
			forceFullWidth: "off",
			hideTimerBar:"on",
			hideThumbsOnMobile: "on",
			hideNavDelayOnMobile: 1500,
			hideBulletsOnMobile: "on",
			hideArrowsOnMobile: "on",
			hideThumbsUnderResolution: 0
		});
	}
	else 
	{
		App.init(); // init core
		var slider = $('.c-layout-revo-slider2 .tp-banner');
		var cont = $('.c-layout-revo-slider2 .tp-banner-container');
		var api = slider.show().revolution({
			delay: 3000,    
			startwidth:1170,
			startheight: 420,
			navigationType: "hide",
			navigationArrows: "solo",
			touchenabled: "on",
			onHoverStop: "on",
			keyboardNavigation: "off",
			navigationType:"bullet",
			navigationArrows:"",
			navigationStyle:"round c-tparrows-hide c-theme",
			navigationHAlign: "right",
			navigationVAlign: "bottom", 
			navigationHOffset:60,
			navigationVOffset:60,
			spinner: "spinner2",
			fullScreen: 'off',   
			fullScreenAlignForce: 'on', 
			fullScreenOffsetContainer: '.c-layout-header',
			shadow: 0,
			fullWidth: "off",
			forceFullWidth: "off",
			hideTimerBar:"on",
			hideThumbsOnMobile: "on",
			hideNavDelayOnMobile: 1500,
			hideBulletsOnMobile: "on",
			hideArrowsOnMobile: "on",
			hideThumbsUnderResolution: 0
		});
	}
});
</script>
<?php };?>