<?php if(!empty($gallery)){;?>
<section class="container c-layout-revo-slider2 c-layout-revo-slider-7" id="banner">
	<div class="tp-banner-container">
		<div class="tp-banner">
			<ul>
				<?php foreach($gallery as $b){
					$mime = mime_content_type('./assets/'.$zone.'/gallery/'.$b->image_square);
					if(strstr($mime, "video/")){
						$cek_file = 'video';
					}else if(strstr($mime, "image/")){
						$cek_file = 'image';
					}
					;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="500">
						<?php if($cek_file == 'image'){;?>
							<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/gallery/<?php echo $b->image_square;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<?php }else{;?>
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img src="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png" alt="">
							<div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false"
							data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%" data-videopreload="meta" data-videomp4="<?php echo base_url('assets');?>/<?php echo $zone;?>/gallery/<?php echo $b->image_square;?>" data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"
							data-aspectratio="16:9" data-volume="100" data-videoposter="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png"> </div>
							<div class="tp-caption arrowicon customin rs-parallaxlevel-0 visible-xs" data-x="center" data-y="bottom" data-hoffset="0" data-voffset="-60" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="500" data-start="2000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 13;">
								<div class="rs-slideloop" data-easing="Power3.easeInOut" data-speed="0.5" data-xs="-5" data-xe="5" data-ys="0" data-ye="0">
									<span class="c-video-hint c-font-15 c-font-sbold c-font-center c-font-dark"> Tap to play video
										<i class="icon-control-play"></i>
									</span>
								</div>
							</div>
							<!--END-->
						<?php };?>
					</li>
				<?php };?>
			</ul>
		</div>
	</div>
</section>

<?php };?>
<?php if(isset($category)){;?>
<div id="list_category" class="container c-content-box c-overflow-hide c-bs-grid-reset-space" style="">
	<div class="row">
		<?php foreach($category as $key => $cat){
			if(isset($cat->image_square)){
			$cat_img = base_url('assets').'/'.$zone.'/category/'.$cat->image_square;
			$cat_url = $menu['link'].'/product/'.strtolower(str_replace('_',' ',$cat->title));
			;?>
		<div class="col-md-4 col-sm-4 col-xs-12" style="padding:10px">
			<?php if($key < 10){;?>
			<div class="c-content-product-5 c-content-overlay">
				<div class="c-overlay-wrapper">
					<div class="c-overlay-content">
						<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20"><?php echo $cat->title;?> </h3>
						<a href="<?php echo $cat_url;?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">View Now</a>
					</div>
				</div>
				<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 500px; background-image: url(<?php echo $cat_img;?>);"></div>
			</div>
			<?php }else{;?>
			<div class="c-content-product-5">
				<div class="c-bg-img-center c-center" data-height="height" style="height: 500px; background-image: url(<?php echo $cat_img;?>);">
					<div class="c-wrapper c-center-vertical">
						<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20"><?php echo $cat->title;?> </h3>
						<a href="<?php echo $cat_url;?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">View Now</a>
					</div>
				</div>
			</div>
			<?php };?>
			
		</div>
			<?php };};?>
	</div>
</div>
<?php };?>
<script>
document.addEventListener("DOMContentLoaded", function(event) { 
	App.init(); // init core
<?php if($zone == 'shisha'){;?>
	$('#banner').removeClass('container');
	$('#list_category').removeClass('container');
	var slider = $('.c-layout-revo-slider2 .tp-banner');
	var cont = $('.c-layout-revo-slider2 .tp-banner-container');
	var api = slider.show().revolution({
		delay: 3000,    
		startwidth:1170,
		startheight: 420,
		navigationType: "hide",
		navigationArrows: "solo",
		touchenabled: "on",
		onHoverStop: "off",
		keyboardNavigation: "off",
		navigationType:"bullet",
		navigationArrows:"",
		navigationStyle:"round c-tparrows-hide c-theme",
		navigationHAlign: "right",
		navigationVAlign: "bottom", 
		navigationHOffset:60,
		navigationVOffset:60,
		spinner: "spinner2",
		fullScreen: 'on',   
		fullScreenAlignForce: 'on', 
		fullScreenOffsetContainer: '.c-layout-header',
		shadow: 0,
		fullWidth: "off",
		forceFullWidth: "off",
		hideTimerBar:"on",
		hideThumbsOnMobile: "on",
		hideNavDelayOnMobile: 1500,
		hideBulletsOnMobile: "on",
		hideArrowsOnMobile: "on",
		hideThumbsUnderResolution: 0
	});
<?php }else{;?>
	if ($(window).width() < 960) {
		var slider = $('.c-layout-revo-slider2 .tp-banner');
		var cont = $('.c-layout-revo-slider2 .tp-banner-container');
		var api = slider.show().revolution({
			delay: 3000,    
			startwidth:1170,
			startheight: 420,
			navigationType: "hide",
			navigationArrows: "solo",
			touchenabled: "on",
			onHoverStop: "on",
			keyboardNavigation: "off",
			navigationType:"bullet",
			navigationArrows:"",
			navigationStyle:"round c-tparrows-hide c-theme",
			navigationHAlign: "right",
			navigationVAlign: "bottom", 
			navigationHOffset:60,
			navigationVOffset:60,
			spinner: "spinner2",
			fullScreen: 'on',   
			fullScreenAlignForce: 'on', 
			fullScreenOffsetContainer: '.c-layout-header',
			shadow: 0,
			fullWidth: "off",
			forceFullWidth: "off",
			hideTimerBar:"on",
			hideThumbsOnMobile: "on",
			hideNavDelayOnMobile: 1500,
			hideBulletsOnMobile: "on",
			hideArrowsOnMobile: "on",
			hideThumbsUnderResolution: 0
		});
	}
	else 
	{
		var slider = $('.c-layout-revo-slider2 .tp-banner');
		var cont = $('.c-layout-revo-slider2 .tp-banner-container');
		var api = slider.show().revolution({
			delay: 3000,    
			startwidth:1170,
			startheight: 420,
			navigationType: "hide",
			navigationArrows: "solo",
			touchenabled: "on",
			onHoverStop: "on",
			keyboardNavigation: "off",
			navigationType:"bullet",
			navigationArrows:"",
			navigationStyle:"round c-tparrows-hide c-theme",
			navigationHAlign: "right",
			navigationVAlign: "bottom", 
			navigationHOffset:60,
			navigationVOffset:60,
			spinner: "spinner2",
			fullScreen: 'off',   
			fullScreenAlignForce: 'on', 
			fullScreenOffsetContainer: '.c-layout-header',
			shadow: 0,
			fullWidth: "off",
			forceFullWidth: "off",
			hideTimerBar:"on",
			hideThumbsOnMobile: "on",
			hideNavDelayOnMobile: 1500,
			hideBulletsOnMobile: "on",
			hideArrowsOnMobile: "on",
			hideThumbsUnderResolution: 0
		});
	}
<?php };?>
	
});
</script>