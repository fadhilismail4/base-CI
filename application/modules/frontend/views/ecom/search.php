<div class="c-content-box c-size-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="c-content-blog-post-1-list">
					<?php foreach($datas as $ds){;?>
						<div class="c-content-blog-post-1">
							<?php if(isset($ds->image_square)){;?>
							<div class="col-md-3" style="padding: 0;">
								<img src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo strtolower($ds->module);?>/<?php echo $ds->image_square;?>" style="width:100%">
							</div>
							<?php };?>
							<div class="col-md-9">
								<div class="c-title c-font-bold c-font-uppercase" style="margin-top:0">
									<?php if(!empty($ds->category)){;?>
									<a href="<?php echo $menu['link'];?>/<?php echo strtolower($ds->module);?>/<?php echo str_replace(' ','-',strtolower($ds->category));?>/<?php echo str_replace(' ','_',strtolower($ds->title));?>">
									<?php }else{;?>
									<a>
									<?php };?>
										<?php echo $ds->title;?>
									</a>
								</div>
								<div class="c-desc" style="margin-bottom: 15px;">
									<?php echo word_limiter(strip_tags($ds->description),20);?>
									<br>
									<?php if(!empty($ds->category)){;?>
									<a href="<?php echo $menu['link'];?>/<?php echo strtolower($ds->module);?>/<?php echo str_replace(' ','-',strtolower($ds->category));?>/<?php echo str_replace(' ','_',strtolower($ds->title));?>" class="btn c-theme-btn c-btn-square">
										Read More
									</a>
									<?php };?>
								</div>
								<div class="c-panel">
									<div class="c-author">
										<a href="#">By
											<span class="c-font-uppercase">Nick Strong</span>
										</a>
									</div>
									<div class="c-date">on
										<span class="c-font-uppercase">20 May 2015, 10:30AM</span>
									</div>
									<ul class="c-tags c-theme-ul-bg">
										<li><?php echo str_replace('-',' ',$ds->category);?></li>
									</ul>
									<!--
									<div class="c-comments">
										<a href="#">
											<i class="icon-speech"></i> 30 comments
										</a>
									</div>
									-->
								</div>
							</div>
						</div>
					<?php };?>
					
					<div class="c-pagination">
						<ul class="c-content-pagination c-theme">
							<li class="c-prev">
								<a href="#">
									<i class="fa fa-angle-left"></i>
								</a>
							</li>
							<li>
								<a href="#">1</a>
							</li>
							<li class="c-active">
								<a href="#">2</a>
							</li>
							<li>
								<a href="#">3</a>
							</li>
							<li>
								<a href="#">4</a>
							</li>
							<li class="c-next">
								<a href="#">
									<i class="fa fa-angle-right"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: BLOG LISTING  -->