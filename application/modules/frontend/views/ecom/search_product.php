<style>
@media(min-width: 991px){
	.c-shop-filter-search-1.collapse{
		display: block
	}
}
#sidebar-menu-1 > li > a.active{
	color: <?php echo $menu['primaryc'];?>;
}
#sidebar-menu-1 > li{
	border-bottom: 1px solid #D0D7DE;
	padding-bottom:20px;
	padding-top:20px;
	padding-left:20px;
	margin-top:0px;
}
</style>
<input type="hidden" id="data-product" value="grid" data-base-url="<?php echo base_url();?>" data-zone="<?php echo $zone;?>" data-url-zone="<?php echo $menu['link'];?>" data-base-modul="product" data-url-first="list" data-url-second="list" data-id="<?php if(isset($bc['category'])){ echo $bc['category'];}else{ echo 'all';};?>" data-param="search"/>
<div class="container">
	<div class="c-layout-sidebar-menu c-theme ">
		<div class="c-sidebar-menu-toggler">
			<h3 class="c-title c-font-uppercase c-font-bold">Type</h3>
			<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
				<span class="c-line"></span>
				<span class="c-line"></span>
				<span class="c-line"></span>
			</a>
		</div>
		<ul id="sidebar-menu-1" class="c-shop-filter-search-1 list-unstyled collapse">
			<?php $i=0;$aktif = false;$cek = '';foreach($sidebar as $key => $s){
				$tot = count($datas[$key]['data']);
				if($tot){
					if($cek == 'active'){
						$cek = '';
					}else{
						$cek = 'active';
					}
				}else{
					$cek = '';
				};?>
			<li>
				<?php if($key == 0){;?>
					<a id="c_<?php echo $s;?>" href="#" class="c-font-uppercase c-font-bold <?php echo $cek;?>" onClick="get_data_product(0);"><?php echo $s;?> (<?php echo $tot;?>)</a>
				<?php }else{;?>
					<a id="c_<?php echo $s;?>" href="#" class="c-font-uppercase c-font-bold <?php echo $cek;?>" onClick="search_result('<?php echo $s;?>')"><?php echo $s;?> (<?php echo $tot;?>)</a>
				<?php };?>
			</li>
			<?php };?>
		</ul>
	</div>
	<div class="c-layout-sidebar-content ">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-ADVANCED-SEARCH-1 -->
		<form class="c-shop-advanced-search-1" action="<?php echo $menu['link'];?>/search" method="POST">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" placeholder="" value="<?php echo trim($this->security->get_csrf_hash()); ?>" class="form-control" autocomplete="off">
			<div class="row">
				<div class="form-group col-md-12">
					<div class="row">
						<div class="col-md-6">
							<label class="control-label">Keywords</label>
							<input id="keywords" name="query" type="text" class="form-control c-square c-theme input-md" placeholder="Enter keywords" value="<?php echo $datas[0]['last_query']->q;?>"> 
						</div>
						<div class="col-md-6">
							<label class="control-label">Category</label>
							<select id="category" class="form-control c-square c-theme input-md">
								<option value="all">All Categories</option>
								<?php foreach($category as $cat){;?>
									<option value="<?php echo $cat->title;?>"><?php echo $cat->title;?></option>
									<?php foreach($cat->child as $cc){;?>
									<option value="<?php echo $cc->title;?>"><?php echo $cc->title;?></option>
									<?php };?>
								<?php };?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group row" role="group">
				<div id="price_range" class="col-md-6">
					<div class="col-md-3" style="padding:0">
						<label class="control-label c-font-uppercase c-font-bold">Price Range</label>
					</div>
					<div class="col-md-9" style="padding:0">
						<div class="c-price-range-box input-group">
							<div class="c-price input-group col-md-6 col-xs-6 pull-left">
								<span class="input-group-addon c-square c-theme">IDR</span>
								<input id="from" type="number" class="numeric form-control c-square c-theme" placeholder="from"> </div>
							<div class="c-price input-group col-md-6 col-xs-6 pull-left">
								<span class="input-group-addon c-square c-theme">IDR</span>
								<input id="to" type="number" class="numeric form-control c-square c-theme" placeholder="to"> </div>
						</div>
						<div class="c-margin-b-20"></div>
						<input id="data-product-range" onClick="get_data_product(0);" type="text" class="hide numeric form-control c-square c-theme" value="">
					</div>
				</div>
				<div class="col-md-6">
					<!--<button id="btn-search" onClick="get_data_product(0);" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">-->
					<button type="submit" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">
						<i class="fa fa-search"></i>Search</button>
					<button class="btn btn-sm btn-default c-btn-square c-btn-uppercase c-btn-bold">Clear</button>
				</div>
			</div>
		</form>
		<hr>
		<div class="c-margin-t-30"></div>
		
		<section id="result">
			<div id="others">
			</div>
			<div id="product">
				<div class="c-layout-sidebar-content" style="padding: 0;">
					<input type="hidden" name="data-product-offset" id="data-product-offset" value="0" />
					<div class="c-shop-result-filter-1 clearfix form-inline">
						<div class="c-filter">
							<label class="control-label c-font-16">Show:</label>
							<select class="form-control c-square c-theme c-input select-grid" id="data-product-show-number" onchange="get_data_product(0);" style="display:none;">
								<option value="6" selected="selected">6</option>
								<option value="12">12</option>
								<option value="18">18</option>
								<option value="24">24</option>
								<option value="30">30</option>
							</select>
							<select class="form-control c-square c-theme c-input select-list" id="data-product-show-number" onchange="get_data_product(0);">
								<option value="5" selected="selected">5</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
							</select>
						</div>
						<div class="c-filter">
							<label class="control-label c-font-16">Sort&nbsp;By:</label>
							<select class="form-control c-square c-theme c-input" id="data-product-show-sort" onchange="get_data_product(0);">
								<option value="title" data-sort="asc" selected="selected">Name (A - Z)</option>
								<option value="title" data-sort="desc">Name (Z - A)</option>
								<option value="publish" data-sort="asc">Price (Low - High)</option>
								<option value="publish" data-sort="desc">Price (High - Low)</option>
								<option value="discount" data-sort="asc">Discount (Low - High)</option>
								<option value="discount" data-sort="desc">Discount (High - Low)</option>
							</select>
						</div>
						<div class="c-filter">
							<label class="control-label c-font-16">Go to Page:</label>
							<select class="form-control c-square c-theme c-input" id="data-product-go-to-page" onchange="get_data_product('page');">
							</select>
						</div>
						<div class="c-filter">
							<button id="view-list" class="btn btn-xs c-btn-dark c-btn-border-2x"><i class="fa fa-th-list"></i></button>
							<button id="view-grid" class="btn btn-xs c-btn-dark c-btn-border-2x"><i class="fa fa-th-large"></i></button>
						</div>
					</div>
					<div class="c-margin-t-20"></div>
					<div class="throbber-loader" id="loader" style="display:none"></div>
					<div id="view-product"></div>
					<div class="c-margin-t-20"></div>
				</div>
			</div>
			
		</section>
	</div>
</div>
<script>
function search_result(type){
	var base_url = '<?php echo base_url();?>';
		base_modul = type;
		url_zone = '<?php echo $menu['link'];?>';
		zone = '<?php echo $zone;?>';
		q = $('#keywords').val();
		data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : 'all',
			param : 'search',
			type : type,
			sort : 'ASC',
			order : 'title',
			limit : '6',
			query: q,
			offset : 0
		};
		url = "<?php echo $menu['link'];?>/list/view_detail/list";
		$('#loader').fadeIn("slow");
		$('#product').slideUp();
		$('#price_range').slideUp();
		$('#others').empty();
		$('#sidebar-menu-1 > li > a').removeClass('active');
		$('#c_'+type).addClass('active');
		//$('#btn-search').attr('onClick',"search_result("+"'"+type+"'"+")");
		$('#btn-search').attr('onClick',"").attr('type','submit');
		
		success_fun = function(json){
			view_grid = '';
			$.each(json.data, function(key, val) {
				category = val.section.title;
				$.each(val.datas, function(key2, val2) {
					urls = url_zone+'/'+type+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'/'+val2.title.toLowerCase().replace(/\s+/g, '_');
					if(type == 'branch'){
						urls = url_zone+'/'+type+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_');
					}else if(type == 'event'){
						urls = url_zone+'/'+type+'/'+val.section.title.toLowerCase().replace(/\s+/g, '_')+'#cbp='+urls;
					}
					
					if(val2.image_square.length){
						view_grid += '<div class="c-content-blog-post-1" style="margin-bottom:0px">'+
							'<div class="col-md-3" style="padding: 0;">'+
								'<img src="'+val2.image_square+'" style="width:100%">'+
							'</div>'+
							'<div class="col-md-9">'+
								'<div class="c-title c-font-bold c-font-uppercase" style="margin-top:0;margin-bottom:0;">'+
									'<a href="'+urls+'">'+val2.title+'</a>'+
								'</div>'+
								'<div class="c-panel" style="margin-bottom:10px;">'+
									'<div class="c-author">'+
										'<a href="#">By'+
										'	<span class="">Admin</span>'+
										'</a>'+
									'</div>'+
									'<div class="c-date">on'+
										'<span class="c-font-uppercase"> '+convertdate(val2.datecreated)+'</span>'+
									'</div>'+
									'<ul class="c-tags c-theme-ul-bg">'+
										'<li>'+val.section.title.toLowerCase()+'</li>'+
									'</ul>'+
								'</div>'+
								'<div class="c-desc" style="margin-bottom: 45px;">'+
									''+limitword($.trim(val2.description.replace(/(<([^>]+)>)/ig,"")),40)+''+
									'<br>'+
									'<a href="'+urls+'" class="btn c-theme-btn c-btn-square" style="float: right;">'+
									'	Read More'+
									'</a>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<hr>';
					}else{
						view_grid += '<div class="c-content-blog-post-1" style="margin-bottom:0px">'+
							'<div class="col-md-12" style="padding-left:0px">'+
								'<div class="c-title c-font-bold c-font-uppercase" style="margin-top:0;margin-bottom:0;">'+
									'<a href="'+urls+'">'+val2.title+'</a>'+
								'</div>'+
								'<div class="c-panel" style="margin-bottom:10px;">'+
									'<div class="c-author">'+
										'<a href="#">By'+
										'	<span class="">Admin</span>'+
										'</a>'+
									'</div>'+
									'<div class="c-date">on'+
										'<span class="c-font-uppercase"> '+convertdate(val2.datecreated)+'</span>'+
									'</div>'+
									'<ul class="c-tags c-theme-ul-bg">'+
										'<li>'+val.section.title.toLowerCase()+'</li>'+
									'</ul>'+
								'</div>'+
								'<div class="c-desc" style="margin-bottom: 45px;">'+
									''+limitword($.trim(val2.description.replace(/(<([^>]+)>)/ig,"")),40)+''+
									'<br>'+
									'<a href="'+urls+'" class="btn c-theme-btn c-btn-square" style="float: right;">'+
									'	Read More'+
									'</a>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<hr>';
					}
				});
			});
			$('#others').append(view_grid);
		};
		ajax_post('json', url, data, success_fun);
}
document.addEventListener("DOMContentLoaded", function(event) { 
	$('#sidebar-menu-1 > li > a.active').click();
	$('.numeric').each(function(){
		$('#to').on("click", function() {
			if($('#from').val().length != 0){
				if(parseInt($('#from').val()) > parseInt($('#to').val())){
					$('#to').val($('#from').val());
				}
			}
		});
	});
	$('#category').on("click", function() {
		if ($(this).find(':selected').val() != '') {
			var val = $(this).find(':selected').val();
			$('#data-product').data('id',val);
		};
	});
	
	var updateOutput = function (e) {
		var list = e.length ? e : $(e.target),
				output = list.data('output');
		if (window.JSON) {
			output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
		} else {
			output.val('JSON browser support required for this demo.');
		}
	};
	$('.c-price-range-box').on("change", function() {
		if($('#from').val() && $('#to').val()){
			text = $('#from').val()+','+$('#to').val();
			$('#data-product-range').val(text);
		}else{
			$('#data-product-range').val('');
		}
	});	
	
	$('#c_product').on('click',function(e){
		$('#others').empty();
		$('#product').slideDown();		
		$('#price_range').slideDown();
		
		$('#sidebar-menu-1 > li > a').removeClass('active');
		$(this).addClass('active');
		$('#btn-search').attr('onClick',"get_data_product(0)");
	});
})
</script>