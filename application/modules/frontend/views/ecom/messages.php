<style>
.folder-list li a:hover, .file-control.active{
	color: #f39c12;
	font-weight: 700;
	text-decoration: initial;
}
.mail-box {
    background-color: #fff;
    border: 1px solid #e7eaec;
    border-top: 0;
    padding: 0px;
    margin-bottom: 20px;
}
.mail-body {
    border-top: 1px solid #e7eaec;
    padding: 20px;
}
.mail-box-header {
    background-color: #ffffff;
    border: 1px solid #e7eaec;
    border-bottom: 0;
    padding: 30px 20px 20px 20px;
}
.mail-box-header h2 {
    margin-top: 0px;
}
.mail-box-header {
    background-color: #ffffff;
    border: 1px solid #e7eaec;
    border-bottom: 0;
    padding: 30px 20px 20px 20px;
}
.mail-search {
    max-width: 300px;
}
.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
.btn-primary {
    background-color: #1ab394;
    border-color: #1ab394;
    color: #FFFFFF;
}
.btn-white:hover, .btn-white:focus, .btn-white:active, .btn-white.active, .open .dropdown-toggle.btn-white {
    color: inherit;
    border: 1px solid #d2d2d2;
}
.btn-white {
    color: inherit;
    background: white;
    border: 1px solid #e7eaec;
}
.btn {
    border-radius: 3px;
}
.btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
.btn + .btn {
    margin-left: 0px;
}
</style>
<a id="<?php echo $type_id;?>" data-url="dashboard/<?php echo $child;?>" data-url2="inbox"  data-param="all" class="hide"></a>
<div class="row">
	<div class="col-lg-12">
		<div class="col-lg-12 mail-box-header" style="padding-top: 20px;">
			<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="compose" class="detail3 menu_type btn btn-sm btn-primary">
				Compose Message
			</a>
			<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="inbox" class="detail menu_type btn btn-sm btn-primary">
				Inbox
			</a>
			<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="sendmail" class="detail menu_type btn btn-sm btn-primary">
				Sendmail
			</a>
			<!--<form method="get" action="#" class="pull-right mail-search">
				<div class="input-group">
					<input type="text" class="form-control input-sm" name="search" placeholder="Search messages">
					<div class="input-group-btn">
						<button type="submit" class="btn btn-sm btn-primary">
							Search
						</button>
					</div>
				</div>
			</form>-->
		</div>
		<div class="col-lg-12 animated fadeInRight detail_content3" style="padding: 0;">
			<div class="mail-box-header">
				<div class="mail-tools tooltip-demo">
					<div class="btn-group pull-right">
						<button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
						<button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>
					</div>
					<h2 style="text-transform: capitalize;float:left;margin-right: 20px;">
						<?php echo $title;?> (<?php echo $m_total ;?>)
					</h2>
					<button id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="<?php echo $title;?>" data-lang="" class="detail btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox">
						<i class="fa fa-refresh"></i> Refresh
					</button>
					<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="read" data-title="Selected message" class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as read">
						<i class="fa fa-eye"></i> 
					</button>
					<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="important" data-title="Selected message" class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as important">
						<i class="fa fa-exclamation"></i> 
					</button>
					<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="trash" data-title="Selected message"class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash">
						<i class="fa fa-trash-o"></i> 
					</button>
					<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="unread" data-title="Selected message" class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as unread">
						<i class="fa fa-eye-slash"></i> 
					</button>
				</div>
			</div>
			<div class="mail-box">
				<table class="table table-hover table-mail">
					<tbody>
						<?php foreach($messages as $m){?>
						<tr class="<?php if(($m->read_status == 3) && ($m->sender != $this->session->userdata('adm_admin_id'))){echo 'unread' ;}else{echo 'read' ;}?>">
							<td class="check-mail">
								<input id="<?php echo $m->message_id;?>" type="checkbox" class="i-checks">
							</td>
							<td class="mail-ontact"><a href="#" id="<?php echo $m->message_id;?>" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="detail" class="detail3"><?php echo $m->name ;?></a></td>
							<td class="mail-subject"><a href="#" id="<?php echo $m->message_id;?>" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="detail" class="detail3"><?php echo $m->subject ;?></a></td>
							<td class="text-right mail-date"><?php if( date("Y-m-d") != date("Y-m-d", strtotime($m->datecreated))){ echo date("M d", strtotime($m->datecreated)) ;}else{ echo date("H:i", strtotime($m->datecreated))  ;} ;?></td>
						</tr>		
						<?php } ;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets');?>/admin/js/plugins/summernote/summernote.min.js"></script>
<script>
$( document ).ready(function() {
	$("[data-status='read']").hide();
	$("[data-status='important']").hide();
	$("[data-status='trash']").hide();
	$("[data-status='unread']").hide();
	
	$('.check-mail input').on('ifToggled', function (event) {
		if($(this).closest('tr').hasClass('unread')){
			$("[data-status='read']").slideDown();
		}else{
			$("[data-status='unread']").slideDown();
		}
		brands();
	});
	
	function brands() {
		var brands = [];
		$('.check-mail input').each(function (index, value) {
			if ($(this).is(':checked')) {
				brands.push($(this).attr('id'));
			}
		});
		
		$("[data-status='important']").slideDown();
		$("[data-status='trash']").slideDown();
		
		
		$("[data-status='read']").attr("id",brands);
		$("[data-status='important']").attr("id",brands);
		$("[data-status='trash']").attr("id",brands);
		$("[data-status='unread']").attr("id",brands);
		if(!brands.length){
			$("[data-status='read']").slideUp();
			$("[data-status='important']").slideUp();
			$("[data-status='trash']").slideUp();
			$("[data-status='unread']").slideUp();
		}
	}

});
</script>