<link href="<?php echo base_url('assets');?>/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<style>
.btn-white{
	background-color: #fff;
    border: 1px solid #ccc;
}
</style>
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
	<div class="container">
		<div id="" class="row animated fadeInRight">
			<div id="" class="col-md-7 c-margin-b-40 c-order-history-2">
				<div class="c-content-title-1">
					<h3 class="c-font-uppercase c-font-bold">Kode Transaksi: INV<?php echo $datas[0]->trx_code;?></h3>
				</div>
				<div id="" class="row c-cart-table-title">
					<div class="col-md-1 c-cart-image">
						<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">No</h3>
					</div>
					<div class="col-md-3 c-cart-ref">
						<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Product</h3>
					</div>
					<div class="col-md-2 c-cart-desc">
						<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2" style="text-align:center">QTY</h3>
					</div>
					<div class="col-md-3 c-cart-price">
						<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Price</h3>
					</div>
					<div class="col-md-3 c-cart-qty">
						<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Sub Total</h3>
					</div>
				</div>
				<?php if(!empty($datas)){;?>
					<?php $i=1;$total=0;foreach($datas as $ds){
						if($ds->object_id != 0){;?>
					<div id="<?php echo $ds->trx_code;?>" class="list_order row c-cart-table-row">
						<div class="col-md-1 col-sm-1 col-xs-1 c-cart-image">
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">No</p>
							<p><?php echo $i;?></p> </div>
						<div class="col-md-3 col-sm-3 col-xs-12 c-cart-ref">
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Product</p>
							<p><?php echo $ds->title;?> <?php if($ds->varian){;?>(<?php echo $ds->varian;?>)<?php };?></p>
							<img class="col-md-12 col-md-12 col-xs-12" src="<?php echo $ds->image;?>" style="padding:0">
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 c-cart-desc">
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold" style="text-align:center">QTY</p>
							<p style="text-align:center">
								<a class="c-font-bold c-theme-link c-font-dark"><?php echo $ds->total;?></a>
							</p>
						</div>
						<div class="clearfix col-md-3 col-sm-3 col-xs-6 c-cart-price">
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Price</p>
							<p class="c-cart-price c-font-bold"><?php echo 'IDR '.number_format(($ds->price/$ds->total),2,',','.');?></p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6 c-cart-qty">
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Sub Total</p>
							<p class="c-cart-price c-font-bold"><?php echo 'IDR '.number_format($ds->price,2,',','.');?></p>
						</div>
					</div>
					<?php $i++;$total=$total+$ds->price;};?>
					<?php };?>
					<div class="list_order row c-cart-table-row">
						<div class="col-md-12">
							<p style="text-align:left"><b>Buy on </b><?php echo date('D, d M Y H:i:s',strtotime($datas[0]->datecreated));?></p>
							<p style="text-align:left"><b>Payment Method: </b><?php if($datas[0]->code == 'TRXON'){ echo ' Direct Bank Transfer';}else{ echo 'OFFLINE';};?></p>
							<p style="text-align:left"><b>Shipment: </b><?php echo $datas[0]->title.' (IDR '.number_format($datas[0]->price,2,',','.').')';?></p>
						</div>
					</div>
					<div class="list_order row c-cart-table-row">
						<div class="col-md-9 col-sm-6 col-xs-6 c-cart-qty">
							<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Total</h3>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 c-cart-qty">
							<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"><?php echo 'IDR '.number_format(($datas[0]->price+$total),2,',','.');?></h3>
						</div>
					</div>
				<?php }else{;?>
					<div id="" class="list_order row c-cart-table-row">
						<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first"></h2>
						<div class="col-md-1 col-sm-3 col-xs-5 c-cart-image">
							<p></p> </div>
						<div class="col-md-2 col-sm-3 col-xs-6 c-cart-ref">
							<p></p>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-6 c-cart-desc">
							<p style="text-align:center">
								
							</p>
						</div>
						<div class="clearfix col-md-2 col-sm-3 col-xs-6 c-cart-price">
							<p class="c-cart-price c-font-bold">
							</p>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-6 c-cart-total">
							<p class="c-cart-price c-font-bold">
							</p>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-6 c-cart-qty">
							<p>
							</p>
						</div>
						<div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
							<p>
							</p>
						</div>
					</div>
				<?php };?>
			</div>
			<div class="col-md-5 c-margin-b-40 c-order-history-2">
				<div class="c-content-title-1">
					<h3 class="c-font-uppercase c-font-bold" style="color: red;"><?php echo $datas[0]->m;?></h3>
				</div>
				<div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
					<h2 class="c-font-bold c-font-uppercase c-font-24">Shipping Address</h2>
					<ul class="c-order list-unstyled">
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="col-md-5 c-font-20">
								<p class="c-font-bold c-font-20">Name</p>
							</div>
							<div class="col-md-6 c-font-20">
								<p class="c-font-20"><?php echo $user->name;?>
								</p>
							</div>
						</li>
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="col-md-5 c-font-20">
								<p class="c-font-bold c-font-20">Address</p>
							</div>
							<div class="col-md-6 c-font-20">
								<p class="c-font-20"><?php echo $user->address_2;?>
								</p>
							</div>
						</li>
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="col-md-5 c-font-20">
								<p class="c-font-bold c-font-20">Notes</p>
							</div>
							<div class="col-md-6 c-font-20">
								<p class="c-font-20"><?php echo $datas[0]->description;?>
								</p>
							</div>
						</li>
						<li class="row">
							<div class="form-group col-md-12" role="group">
								<?php if(strlen($datas[0]->cnf_status) > 0){;?>
								<?php if($datas[0]->cnf_status == 0){;?>
								<a href="#" data-toggle="modal" data-target="#confirmation-form2" class="col-md-6 col-xs-12 btn_checkout btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold btn-warning">Change File</a>
								<?php }elseif($datas[0]->cnf_status == 1){;?>
								
								<?php }elseif($datas[0]->cnf_status == 2){;?>
								<a href="#" data-toggle="modal" data-target="#confirmation-form2" class="col-md-6 col-xs-12 btn_checkout btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold btn-warning">Change File</a>
								<?php };?>
								<?php }else{;?>
								<a href="#" data-toggle="modal" data-target="#confirmation-form" class="col-md-6 col-xs-12 btn_checkout btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Confirmation</a>
								<a href="#" data-toggle="modal" data-target="#cancel-order" class="col-md-6 col-xs-12 btn_checkout btn btn-lg c-btn-square c-btn-uppercase c-btn-bold btn-danger" style="margin:0">Cancel Order</a>
								<?php };?>
							</div>
						</li>
					</ul>
				</div>
				
			</div>
		</div>

		<div class="modal fade c-content-login-form" id="confirmation-form" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content c-square">
					<div class="modal-header c-no-border">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<h3 class="c-font-24 c-font-sbold">Confirmation</h3>
						<!--<p>
							Please upload your Image
						</p>-->
						<div class="alert alert-danger" id="fail-upload" style="display:none;"></div>
						<div class="alert alert-info" id="success-upload" style="display:none;"></div>
						<form role="form" id="confirmation" method="post">
							<div class="form-group">
								<label for="inv" class="">Invoice Number:</label>
								<select class="form-control input-lg c-square" id="inv">
									<option value="<?php echo $datas[0]->trx_code;?>"><?php echo $datas[0]->trx_code;?></option>
								</select>
							</div>
							<div class="form-group">
								<label for="file" class="">File:</label>
								<input type="file" class="file form-control input-lg c-square" name="file" id="file">
							</div>
							<div class="form-group">
								<button type="submit" id="dashboard/view_detail/view" class="conf btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade c-content-login-form" id="confirmation-form2" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content c-square">
					<div class="modal-header c-no-border">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<h3 class="c-font-24 c-font-sbold">Update Confirmation</h3>
						<!--<p>
							Please upload your Image
						</p>-->
						<div class="alert alert-danger" id="fail2-upload" style="display:none;"></div>
						<div class="alert alert-info" id="success2-upload" style="display:none;"></div>
						<form role="form" id="confirmation2" method="post">
							<div class="form-group">
								<label for="inv" class="">Invoice Number:</label>
								<select class="form-control input-lg c-square" id="inv2">
									<option value="<?php echo $datas[0]->trx_code;?>"><?php echo $datas[0]->trx_code;?></option>
								</select>
							</div>
							<div class="form-group">
								<label for="file" class="">File:</label>
								<input type="file" class="file2 form-control input-lg c-square" name="file" id="file2">
							</div>
							<div class="form-group">
								<button type="submit" id="dashboard/view_detail/view" class="conf2 btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade c-content-login-form" id="cancel-order" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content c-square">
					<div class="modal-header c-no-border">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<h3 class="c-font-24 c-font-sbold">Cancel Order</h3>
						<!--<p>
							Please upload your Image
						</p>-->
						<div class="alert alert-danger" id="cancel-fail-upload" style="display:none;"></div>
						<div class="alert alert-info" id="cancel-success-upload" style="display:none;"></div>
						<form role="form" id="cancel-confirmation" method="post">
							<div class="form-group">
								<label for="inv3" class="">Invoice Number:</label>
								<input id="inv3" class="form-control input-lg c-square" disabled value="<?php echo $datas[0]->trx_code;?>"></input>
							</div>
							<div class="form-group">
								<button type="submit" id="dashboard/view_detail/view" class="cancel-conf btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
document.addEventListener('DOMContentLoaded', function(){ 
   $.getScript("<?php echo base_url('assets');?>/admin/js/plugins/fileinput/fileinput.min.js").done(function( script, textStatus ) {
	   <?php if(!empty($datas[0]->cnf_img)){;?>
	   img = '<?php echo base_url();?>assets/<?php echo $zone;?>/trx/cnf/<?php echo $datas[0]->cnf_img;?>';
		$(".file2").fileinput("refresh",{
				showRemove: false,
				showUpload: false,
				initialPreview: [
					"<img src="+img+" class='file-preview-image'>"
				]
			});
	   <?php }else{;?>
		$(".file").fileinput("refresh",{
			showRemove: false,
			showUpload: false,
		});
	   <?php };?>
		
	});
   
	$(document).delegate(".conf",'click',function(e){
	//$('#conf').on('click',function(e){ 
		
		e.preventDefault();  
		$('#success-upload').empty();
		$('#fail-upload').empty();
		$('.conf').attr('disabled','disabled');
		var inputFilePhoto = document.getElementById("file");
		var imageFile = inputFilePhoto.files[0];

		/* var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : $('#inv').val(),
			file: imageFile
		}; */
		
		var formData = new FormData();             
		formData.append("<?php echo $this->security->get_csrf_token_name(); ?>", '<?php echo trim($this->security->get_csrf_hash()); ?>');
		formData.append("param", 'order_history');
		formData.append("param2", 'confirmation');
		formData.append("id", $('#inv').val());
		formData.append("file", imageFile);
		
		var base_url = '<?php echo base_url();?>';
		var fp_url = $(this).attr('id');
		$.ajax({ 
			type: "POST",
			url: fp_url,
			secureuri: false,
			contentType: false,
			processData: false,
			dataType: 'JSON',
			data: formData,
			success: function(data){
				if (data.status == "success"){
					$('<p>'+data.m+'</p>').appendTo('#success-upload');
					$('#success-upload').show();scrolltonote('#confirmation-form');
					$('#success-upload').fadeTo(3000, 500).slideUp(500);
					setTimeout(function(){
						$('#confirmation-form').modal('toggle');
						$('.modal-backdrop').remove();
						$('body').removeClass('modal-open');
					}, 600);
					setTimeout(function(){
						location.reload();
					}, 1000);
				}else if(data.status == "ambigue"){
					type = data.m;
					modal_ambigue(type);
				}else{
					$('<p>'+data.m+'</p>').appendTo('#fail-upload');
					$('.conf').removeAttr('disabled');
					$('#fail-upload').show();
					scrolltonote('#confirmation-form');
					$('#fail-upload').fadeTo(3000, 500).slideUp(500);
				}
				eval(document.getElementById("crud_script").innerHTML);
			}
		});
		function removeCrud( itemid ) {
			var element = document.getElementById(itemid); // will return element
			elemen.parentNode.removeChild(element); // will remove the element from DOM
		}
		removeCrud('crud_script');
		$('#fail-upload').empty(); $('#success-upload').empty();
	});

	$(document).delegate(".conf2",'click',function(e){
	//$('#conf').on('click',function(e){ 
		
		e.preventDefault();  
		
		$('#success2-upload').empty();
		$('#fail2-upload').empty();
		$('.conf2').attr('disabled','disabled');
		var inputFilePhoto = document.getElementById("file2");
		var imageFile = inputFilePhoto.files[0];

		/* var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : $('#inv').val(),
			file: imageFile
		}; */
		
		var formData = new FormData();             
		formData.append("<?php echo $this->security->get_csrf_token_name(); ?>", '<?php echo trim($this->security->get_csrf_hash()); ?>');
		formData.append("param", 'order_history');
		formData.append("param2", 'confirmation');
		formData.append("id", $('#inv2').val());
		formData.append("file", imageFile);
		
		var base_url = '<?php echo base_url();?>';
		var fp_url = $(this).attr('id');
		$.ajax({ 
			type: "POST",
			url: fp_url,
			secureuri: false,
			contentType: false,
			processData: false,
			dataType: 'JSON',
			data: formData,
			success: function(data){
				if (data.status == "success"){
					$('<p>'+data.m+'</p>').appendTo('#success2-upload');
					$('#success2-upload').show();scrolltonote('#confirmation-form2');
					$('#success2-upload').fadeTo(3000, 500).slideUp(500);
					setTimeout(function(){
						$('#confirmation-form2').modal('toggle');
						$('.modal-backdrop').remove();
						$('body').removeClass('modal-open');
					}, 600);
					setTimeout(function(){
						location.reload();
					}, 1000);
				}else if(data.status == "ambigue"){
					type = data.m;
					modal_ambigue(type);
				}else{
					$('<p>'+data.m+'</p>').appendTo('#fail2-upload');
					$('.conf2').removeAttr('disabled');
					$('#fail2-upload').show();
					scrolltonote('#confirmation-form2');
					$('#fail2-upload').fadeTo(3000, 500).slideUp(500);
				}
				eval(document.getElementById("crud_script").innerHTML);
			}
		});
		function removeCrud( itemid ) {
			var element = document.getElementById(itemid); // will return element
			elemen.parentNode.removeChild(element); // will remove the element from DOM
		}
		removeCrud('crud_script');
		$('#fail2-upload').empty(); $('#success2-upload').empty();
	});

	$(document).delegate(".cancel-conf",'click',function(e){
		e.preventDefault();  
		
		$('#cancel-success-upload').empty();
		$('#cancel-fail-upload').empty();
		$('.cancel-conf').attr('disabled','disabled');
		
		var formData = new FormData();             
		formData.append("<?php echo $this->security->get_csrf_token_name(); ?>", '<?php echo trim($this->security->get_csrf_hash()); ?>');
		formData.append("param", 'order_history');
		formData.append("param2", 'confirmation');
		formData.append("param3", 'cancel');
		formData.append("id", $('#inv3').val());
		
		var base_url = '<?php echo base_url();?>';
		var fp_url = $(this).attr('id');
		$.ajax({ 
			type: "POST",
			url: fp_url,
			secureuri: false,
			contentType: false,
			processData: false,
			dataType: 'JSON',
			data: formData,
			success: function(data){
				if (data.status == "success"){
					$('<p>'+data.m+'</p>').appendTo('#cancel-success-upload');
					$('#cancel-success-upload').show();scrolltonote('#cancel-confirmation');
					$('#cancel-success-upload').fadeTo(3000, 500).slideUp(500);
					setTimeout(function(){
						$('#cancel-confirmation').modal('toggle');
						$('.modal-backdrop').remove();
						$('body').removeClass('modal-open');
					}, 600);
					setTimeout(function(){
						location.reload();
					}, 1000);
				}else if(data.status == "ambigue"){
					type = data.m;
					modal_ambigue(type);
				}else{
					$('<p>'+data.m+'</p>').appendTo('#cancel-fail-upload');
					$('.cancel-conf').removeAttr('disabled');
					$('#cancel-fail-upload').show();
					scrolltonote('#cancel-confirmation');
					$('#cancel-fail-upload').fadeTo(3000, 500).slideUp(500);
				}
				eval(document.getElementById("crud_script").innerHTML);
			}
		});
		function removeCrud( itemid ) {
			var element = document.getElementById(itemid); // will return element
			elemen.parentNode.removeChild(element); // will remove the element from DOM
		}
		removeCrud('crud_script');
		$('#cancel-fail-upload').empty(); $('#success-upload').empty();
	});
	function modal_ambigue(type){
		$('[id*="modal_"]').each(function(){
			$(this).remove();
		});
		var id = type.old_inv,
			base_url = '<?php echo base_url();?>',
			part = 'edit';
		var url = "dashboard/view_detail/view";
		var lang = 'aw2';//$(this).data('lang');
		var param = 'aw3';//$(this).data('param');
		var status = 'Save';//$(this).data('status');
		view_data = '<div class="col-sm-12">'+
					'<h2>'+type.message+'</h2>'+
					'<div class="row">'+
					'<div class="col-sm-3"><img class="col-md-12" src="'+type.old_data+'"></div>'+
					'<div class="col-sm-3"><img class="col-md-12" src="'+type.old_data+'"></div>'+
					'</div>'+
					'</div>';
		view_modal = 
			'<div class="modal fade" id="modal_'+id+'" tabindex="-1" role="dialog">'+
				'<div class="modal-dialog modal-lg">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal">'+
								'<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>'+
							'</button>'+
							'<h5 class="modal-title" style="text-transform:uppercase;float: none;">'+id+'</h5>'+
						'</div>'+
						'<div class="modal-body">'+
							'<div class="alert alert-danger" id="fail" style="display:none;"></div>'+
							'<div class="alert alert-info" id="success" style="display:none;"></div>'+
							'<div id="data" class="row">'+
								view_data+
							'</div>'+
							'<button type="submit" id="dashboard/view_detail/view" class="conf btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
		$('#confirmation-form').parent().append(view_modal.replace(/,/g, ""));
		$('#modal_'+id+'').modal('toggle');
		//$('.modal-backdrop').css('display','none');
	};
});
</script>
<script id="">

</script>