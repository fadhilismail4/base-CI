<!-- BEGIN: CONTENT/SHOPS/SHOP-MY-ADDRESSES-1 -->
<div class="c-content-title-1">
	<h3 class="c-font-uppercase c-font-bold">My Addresses</h3>
	<div class="c-line-left"></div>
</div>
<div class="row c-margin-t-25 animated fadeInRight">
	<div class="col-md-4 col-sm-4 col-xs-12 c-margin-b-20 c-margin-t-10">The following addresses will be used on the checkout page by default.</div>
	<div class="col-md-4 col-sm-4 col-xs-12 c-margin-b-20">
		<h3 class="c-font-uppercase c-font-bold">Billing Address</h3>
		<ul class="list-unstyled">
			<li><?php echo $address_1->address;?></li>
			<li>Phone: <?php echo $address_1->phone;?></li>
			<li>Mobile: <?php echo $address_1->mobile;?></li>
			<li>Postcode / Zip: <?php echo $address_1->zip;?></li>
		</ul>
		<a id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="edit_profile" class="detail btn c-theme-btn btn-xs">
			<i class="fa fa-edit"></i> Edit</a>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 c-margin-b-20">
		<h3 class="c-font-uppercase c-font-bold">Shipping Address</h3>
		<ul class="list-unstyled">
			<li><?php echo $address_2->address;?></li>
			<li>Phone: <?php echo $address_2->phone;?></li>
			<li>Mobile: <?php echo $address_2->mobile;?></li>
			<li>Postcode / Zip: <?php echo $address_2->zip;?></li>
		</ul>
		<a id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="edit_profile" class="detail btn c-theme-btn btn-xs">
			<i class="fa fa-edit"></i> Edit</a>
	</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-MY-ADDRESSES-1 -->