<div class="container">
	<div class="c-layout-sidebar-menu c-theme row">
		<div class="item col-sm-6 col-lg-12" style="padding:0">
			<div class="c-content-person-1 c-bordered c-shadow">
				<div class="c-caption c-content-overlay">
					<div class="row">
					<div class="col-md-6 col-xs-6">
						<?php $img = base_url('assets/img/'.$this->session->userdata($zone.'_foto'));
							if(!file_exists('assets/img/'.$this->session->userdata($zone.'_foto'))){
								$img = base_url('assets/'.$zone.'/user/member/'.$this->session->userdata($zone.'_foto'));
							}
						;?>
						<img class="c-overlay-object img-responsive" style="width:100%" src="<?php echo $img;?>" alt="">
					</div>
					<div class="col-md-6 col-xs-6">
						<strong><?php echo $this->session->userdata($zone.'_nama');?></strong>
						<?php if(isset($type->name)){;?>
						<strong><?php echo $type->name;?></strong>
						<?php };?>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="c-sidebar-menu-toggler col-sm-6 col-lg-12">
			<h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
			<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
				<span class="c-line"></span>
				<span class="c-line"></span>
				<span class="c-line"></span>
			</a>
		</div>
		<ul class="c-sidebar-menu collapse col-sm-6 col-lg-12" id="sidebar-menu-1">
			<li class="c-dropdown c-open">
				<a href="javascript:;" class="c-toggler">My Profile
					<span class="c-arrow"></span>
				</a>
				<ul class="c-dropdown-menu">
					<li class="menu_type c-active">
						<?php if($zone == 'shisha'){;?>
							<a href="#"><i class="fa fa-dashboard"></i> My Account</a>
						<?php }else{;?>
							<a href="#"><i class="fa fa-dashboard"></i> My Dashbord</a>
						<?php };?>
						
					</li>
					<li class="menu_type">
						<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="inbox" class="detail"><i class="fa fa-envelope-o"></i> Messages</a>
					</li>
					<li class="menu_type">
						<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="edit_profile" class="detail"><i class="fa fa-edit"></i> Edit Profile</a>
					</li>
					<li class="menu_type">
						<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="order_history" class="detail"><i class="fa fa-file-text-o"></i> Order History</a>
					</li>
					<li class="menu_type">
						<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="address" class="detail"><i class="fa fa-home"></i> My Addresses</a>
					</li>
					<!--<li class="menu_type">
						<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="wishlist" class="detail"><i class="fa fa-shopping-cart"></i> My Wishlist</a>
					</li>-->
				</ul>
			</li>
		</ul>
	</div>
	<div class="c-layout-sidebar-content detail_content">
		<div class="c-content-title-1">
			<?php if($is_verified){;?>
				<?php if(!empty($user->reg_id)){;?>
					<h2 class="c-theme-font pull-right">Verified Member with Card Number: <?php echo $user->reg_id;?></h2>
				<?php }else{;?>
					<a href="#" data-toggle="modal" data-target="#modal-verified" class="btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase"><?php echo $card_name;?></a>
				<?php };?>
			<?php };?>
			
			<h3 class="c-font-uppercase c-font-bold">My Dashboard</h3>
			<div class="c-line-left"></div>
			<p class=""> Hello
				<a href="" id="1" data-url="dashboard" data-url2="view" data-lang="2" data-param="edit_profile" class="detail c-theme-link">
					<strong><?php echo $this->session->userdata($zone.'_nama');?></strong>
				</a>.
				<br /> 
			</p>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 c-margin-b-20">
				<h3 class="c-font-uppercase c-font-bold"><?php echo $this->session->userdata($zone.'_nama');?></h3>
				<ul class="list-unstyled">
					<?php if(!empty($user->address)){;?>
					<li><?php echo $user->address;?></li>
					<?php };?>
					<?php if(!empty($user->phone)){;?>
					<li>Phone: <?php echo $user->phone;?></li>
					<?php };?>
					<?php if(!empty($user->mobile)){;?>
					<li>Mobile: <?php echo $user->mobile;?></li>
					<?php };?>
					<li>Postcode / Zip: <?php echo $user->zip;?></li>
					<li>Email:
						<a href="mailto:<?php echo $user->email;?>" class="c-theme-link"><?php echo $user->email;?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>      
<?php if($is_verified){;?>
<div class="modal fade" id="modal-verified" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body ">
				<div class="row">
					<div id="intro-verified" class="col-md-12">
						<a href="<?php echo $menu['link'].'/shop/product/member_card';?>" class="col-md-12 btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase" style="margin-bottom: 20px;">Buy</a>
						<a href="#" data-target="have-verified" class="col-md-12 btn btn-sm c-theme-btn pull-right c-btn-uppercase">Already Have One? Confirm</a>
					</div>
					<div id="have-verified" class="col-md-12" style="display:none;">
						<h3 class="c-font-24 c-font-sbold">Confirmation <?php echo $card_name;?></h3>
						<!--<p>
							Please upload your Image
						</p>-->
						<div class="alert alert-danger" id="cancel-fail-upload" style="display:none;"></div>
						<div class="alert alert-info" id="cancel-success-upload" style="display:none;"></div>
						<form role="form" id="cancel-confirmation" method="post">
							<div class="form-group">
								<p class="">Already have one?</p>
								<label for="inv3" class="">Please insert Card Number:</label>
								<input id="inv3" class="form-control input-lg c-square"></input>
							</div>
							<div class="form-group">
								<button type="submit" id="#" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Confirm</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
	$('a[data-target="have-verified"]').on("click", function(e) {
		$('#intro-verified').slideUp(500);
		$('#have-verified').slideDown(500);
	});
});
</script>
<?php };?>

<?php if (!$user->email || !$user->password) {;?>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
	target = $("[data-param='edit_profile']");
	if (!target.parent('.menu_type').hasClass('c-active')) {
		$("[data-param='edit_profile']")[0].click();
	}
});
</script>
<?php };?>