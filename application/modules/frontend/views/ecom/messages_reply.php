<style>
.note-editor{
	background-color: #fff;
}
.icon-reply{
	border-top: .938em solid #A3A3A3;
	border-left: .938em solid #A3A3A3;
	border-right: .938em solid #A3A3A3;
	width: 10px;
	cursor: pointer;
}
</style>
<div class="mail-box">


	<div class="mail-body">

		<form id="messages_compose" class="form-horizontal" method="get">
			<input id="parent_id" name="inputan"type="hidden" class="" value="<?php echo $messages->message_id;?>">
			<input id="subject" name="inputan" type="hidden" class="form-control" value="<?php if($part == 'reply'){ echo 'Re:' ;}else{ echo 'Fwd:';};?> <?php echo $messages->subject;?>">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<div class="form-group"><label class="col-sm-2 control-label">To:</label>
				<input id="tr_id" name="inputan" type="text" class="hide form-control" value="">
				<div class="col-sm-10">
					<div class="input-group">
						<select id="receiver" name="inputan" data-placeholder="Select" class="chosen-select" style="width:350px;" tabindex="2">
						<option value="">Select</option>
						<?php foreach($receiver as $r){?>
							<option value="<?php echo $r->id;?>" data-type="<?php echo $r->type_id;?>"><?php echo $r->name;?> (<?php echo $r->role;?>)</option>
						<?php } ?>
						</select>
					</div>
				</div>
			</div>
		</form>

	</div>

	<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">

		<div id="message" name="inputan_summer" class="summernote">
			
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="mail-body text-right tooltip-demo" style="background-color: #F9F8F8;">
		<a id="messages" data-url="create" data-url2="checkout" data-lang="2" data-param="<?php echo $type_id;?>" data-param2="messages" class="create btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-reply"></i> Send</a>
		<a class="discard btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
		<a id="messages" data-url="create" data-url2="checkout" data-lang="2" data-param="<?php echo $type_id;?>" data-param2="messages" class="create btn btn-white btn-sm" data-param="draft" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>
	</div>
    
	<div class="clearfix"></div>
</div>
<!-- SUMMERNOTE -->
<script src="<?php echo base_url('assets');?>/admin/js/plugins/summernote/summernote.min.js"></script>
<script>
var type = $('#receiver').find(':selected').data('type');
$('#tr_id').val(type);
$('#receiver').change(function() {
	var type = $(this).find(':selected').data('type');
	$('#tr_id').val(type);
});
	$(document).ready(function(){
		$('.discard').on("click", function(e) {
			e.preventDefault();
				$('[class*="detail_content2"]').each(function(){
					$(this).empty();
				});
		});
		$('.reply').children().hide();
		$('.icon-reply').on("click", function(e) {
			e.preventDefault();
			$(this).removeClass('icon-reply');
			$('.reply').children().slideDown();
		});
	});
	var edit = function() {
		$('.click2edit').summernote({focus: true});
	};
	var save = function() {
		var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
		$('.click2edit').destroy();
	};
$(document).ready(function(){
	$('.create').on("click", function(e) {
		$('#messages_compose > #success').empty();
		$('#messages_compose > #fail').empty();
		var obj = $(this).attr('id');
			param = $(this).data('param');
			param2 = $(this).data('param2');
		function get_val(){
			var item_obj = {};
				$('[name="inputan"]').each(function(){
					item_obj[this.id] = this.value;
				});
			var item_summer = {};
				$('[name="inputan_summer"]').each(function(){
					item_summer[this.id] = $(this).code();
				});
			$.extend($.extend($.extend($.extend(item_obj, item_summer),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param}),{param2 : param2});
			return item_obj;
		}
		var data = get_val();
		$.ajax({
			url : "<?php echo $menu['link'];?>/dashboard/view_detail/creates",
			secureuri: false,
			type: "POST",
			dataType: 'json',
			data: data,
			success: function(data){
				if (data.status == "success"){
					$('<p>'+data.m+'</p>').appendTo('#messages_compose > #success');
					scrolltonote('.c-layout-page');
					$('#messages_compose > #success').show();
					$('#messages_compose > #success').fadeTo(2000, 500).slideUp(500);
					$('.btn').each(function(){$(this).removeAttr('disabled');});
				}else{
					$('<p>'+data.m+'</p>').appendTo('#messages_compose > #fail');
					scrolltonote('.c-layout-page');
					$('#messages_compose > #fail').show();
					$('#messages_compose > #fail').fadeTo(4000, 500).slideUp(500); 
					$('.btn').each(function(){$(this).removeAttr('disabled');});
				}
			}
		});
	});
});
</script>