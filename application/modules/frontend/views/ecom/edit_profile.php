<!-- BEGIN: PAGE CONTENT -->
<div class="c-content-title-1">
	<h3 class="c-font-uppercase c-font-bold">Edit Profile</h3>
	<div class="c-line-left"></div>
</div>
<div class="c-shop-form-1 animated fadeInRight">
	<!-- BEGIN: ADDRESS FORM -->
	<div id="profile">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-12">
						<label class="control-label">Full Name</label>
						<input id="name" name="inputan" type="text" class="form-control c-square c-theme" placeholder="First Name" value="<?php echo $user->name;?>"> </div>
				</div>
			</div>
		</div>
		<!--<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Town / City</label>
				<input type="text" class="form-control c-square c-theme" placeholder="Town / City"> </div>
		</div>-->
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<!--<div class="form-group col-md-6">
						<label class="control-label">State / County</label>
						<select class="form-control c-square c-theme">
							<option value="0">Select an option...</option>
							<option value="1">Malaysia</option>
							<option value="2">Singapore</option>
							<option value="3">Indonesia</option>
							<option value="4">Thailand</option>
							<option value="5">China</option>
						</select>
					</div>-->
					<div class="col-md-6">
						<label class="control-label">Address</label>
						<input id="address" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Street Address"  value="<?php echo $user->address;?>"> 
					</div>	
					<div class="col-md-6">
						<label class="control-label">Postcode / Zip</label>
						<input id="zip" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Postcode / Zip" value="<?php echo $user->zip;?>"> 
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-6">
						<label class="control-label">Email Address</label>
						<input type="email" id="email" name="inputan" class="form-control c-square c-theme" placeholder="Email Address" value="<?php echo $user->email;?>"> </div>
					<div class="col-md-6">
						<label class="control-label">Phone</label>
						<input id="phone" name="inputan" type="tel" class="form-control c-square c-theme" placeholder="Phone" value="<?php echo $user->phone;?>"> </div>
				</div>
			</div>
		</div>
		<!-- END: BILLING ADDRESS -->
		<!-- BEGIN: PASSWORD -->
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Old Password</label>
				<?php if($user->email){ ;?>
					<input type="password" id="old" name="inputan" class="form-control c-square c-theme" placeholder="Old Password"> </div>
				<?php }else{ ;?>
					<input type="password" id="old" name="inputan" class="form-control c-square c-theme" disabled placeholder="Old Password" value="<?php echo $user->reg_id ;?>"> </div>
				<?php } ;?>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Change Password</label>
				<input type="password" id="p" name="inputan" class="form-control c-square c-theme" placeholder="Password"> </div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Repeat Password</label>
				<input type="password" id="p2" name="inputan" class="form-control c-square c-theme" placeholder="Repeat Password">
				<!--<p class="help-block">Hint: The password should be at least six characters long.
					<br /> To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).</p>-->
			</div>
		</div>
		<!-- END: PASSWORD -->
		<div class="row c-margin-t-30">
			<div class="form-group col-md-12" role="group">
				<button id="1" data-url="create" data-url2="checkout" data-lang="2" data-param="message" data-param2="edit_profile" class="create btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
				<a href="" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</a>
			</div>
		</div>
	</div>
	<!-- END: ADDRESS FORM -->
</div>
<!-- END: PAGE CONTENT -->
<script>
$(document).ready(function(){
	$('.create').on("click", function(e) {
		$('#profile > #success').empty();
		$('#profile > #fail').empty();
		var obj = $(this).attr('id');
			param = $(this).data('param');
			param2 = $(this).data('param2');
		function get_val(){
			var item_obj = {};
				$('[name="inputan"]').each(function(){
					item_obj[this.id] = this.value;
				});
			var item_array = {};
				$('[name="inputan_array"]').each(function(){
					item_array[this.id] = $(this).chosen().val();
				});
			$.extend($.extend($.extend($.extend(item_obj, item_array),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param}),{param2 : param2});
			return item_obj;
		}
		var data = get_val();
		$.ajax({
			url : "<?php echo $menu['link'];?>/dashboard/view_detail/creates",
			secureuri: false,
			type: "POST",
			dataType: 'json',
			data: data,
			success: function(data){
				if (data.status == "success"){
					$('<p>'+data.m+'</p>').appendTo('#profile > #success');
					scrolltonote('.c-layout-page');
					$('#profile > #success').show();
					$('#profile > #success').fadeTo(2000, 500).slideUp(500);
					$('.btn').each(function(){$(this).removeAttr('disabled');});
				}else{
					$('<p>'+data.m+'</p>').appendTo('#profile > #fail');
					scrolltonote('.c-layout-page');
					$('#profile > #fail').show();
					$('#profile > #fail').fadeTo(4000, 500).slideUp(500); 
					$('.btn').each(function(){$(this).removeAttr('disabled');});
				}
			}
		});
	});
});
</script>