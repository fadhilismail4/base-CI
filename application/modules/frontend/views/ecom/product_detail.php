<!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
<?php 
if(isset($datas->publish)){
	$non_curr = intval($datas->publish);
	$rp = 'IDR '.number_format($non_curr,2,',','.');
	$p_link = 'assets/'.$zone.'/product/'.$datas->image_square;
	
	if($datas->discount != 0){
		$non_curr = $datas->publish*(1-($datas->discount / 100));
		$rp = 'IDR '.number_format($non_curr,2,',','.');
		$dc = 'IDR '.number_format($datas->publish,2,',','.');
	}
	if($datas->vstatus == 1){
		if($datas->variant){
			$non_curr = $datas->variant[0]->publish;
			$stock = $datas->variant[0]->stock;
		}else{
			$non_curr = '';
			$stock = 0;
		}
	}else{
		$stock = $datas->stock;
	}
}
if(!empty($gallery)){
	$link_gal = base_url().'assets/'.$zone.'/gallery/';
	$link_gal_thumb = base_url().'assets/'.$zone.'/gallery/thumbnail/';
};?>
<?php if($zone == 'ilovekb'){
	$img_style = 'width: initial;';
}else{
	$img_style = 'height: inherit; width: initial;background-repeat:no-repeat;background-position: center center;';
};?>
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
	<div class="container">
		<div class="c-shop-product-details-2">
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<div class="c-product-gallery">
						<div class="c-product-gallery-content">
							<div class="c-zoom">
								<img src="<?php echo base_url('assets'.'/'.$zone);?>/product/<?php echo $datas->image_square;?>" style="<?php echo $img_style;?>"> </div>
							<?php if(!empty($gallery)){;?>
								<?php foreach($gallery as $g){;?>
									<div class="c-zoom">
										<img src="<?php echo $link_gal;?><?php echo $g->image_square;?>" style="<?php echo $img_style;?>">
									</div>
								<?php };?>
							<?php };?>
						</div>
						<?php if(!empty($gallery)){;?>
						<div class="row c-product-gallery-thumbnail">
							<div class="col-xs-3 c-product-thumb">
								<img src="<?php echo base_url('assets'.'/'.$zone);?>/product/<?php echo $datas->image_square;?>"> </div>
							<?php foreach($gallery as $g){;?>	
							<div class="col-xs-3 c-product-thumb">
								<?php if(file_exists('./assets/'.$zone.'/gallery/thumbnail/'.trim($g->image_square))){;?>
								<img src="<?php echo $link_gal_thumb;?><?php echo $g->image_square;?>"> </div>
								<?php }else{;?>
								<img src="<?php echo $link_gal_thumb;?><?php echo str_replace('.','_thumb.',$g->image_square);?>"> </div>
								<?php };?>
							<?php };?>	
						</div>
						<?php };?>
					</div>
				</div>
				<div class="col-md-8 col-sm-12">
					<div class="c-product-meta">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold"><?php echo $datas->title;?></h3>
							<div class="c-line-left"></div>
						</div>
						<div class="c-product-badge">
							<?php if($datas->discount != 0){;?>
							<div class="c-product-sale">Sale</div>
							<?php };?>
							<!--<div class="c-product-new">New</div>-->
						</div>
						<div class="c-product-review" style="margin: 0;">
							<!--<div class="c-product-rating">
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star-half-o c-font-red"></i>
							</div>
							<div class="c-product-write-review">
								<a class="c-font-red" href="#">Write a review</a>
							</div>-->
						</div>
						<!--<div class="c-product-price pull-right">
							<?php// if($datas->discount != 0){;?>
							<span class="c-font-line-through c-font-red"><?php// echo $dc;?></span>
							<?php// };?>
							<?php// echo $rp; ?>
						
						</div>-->
						 <div class="row c-product-variant">
							<?php if(!empty($datas->variant)){;?>
							<div class="col-md-3 col-sm-3 col-xs-6">
								<p class="c-product-meta-label c-product-margin-1 c-font-uppercase c-font-bold"><?php echo $datas->variant[0]->category;?>:</p>
								<div class="c-product-size">
									<select id="variant" onClick="get_variant_id_detail();">
										<?php foreach($datas->variant as $dv){;
										if(isset($dv->discount)){
											$dsc = $dv->discount;
										}else{
											$dsc = 0;
										}
										?>
										<option value="<?php echo $dv->settings_id;?>" data-publish="<?php echo $dv->publish;?>" data-stock="<?php echo $dv->stock;?>" data-id="<?php echo $datas->object_id;?>" data-discount="<?php echo $dsc;?>">
											<?php echo $dv->title;?>
										</option>
										<?php };?>
									</select>
								</div>
							</div>
							<?php };?>
							
							<!--<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="c-product-color">
									<p class="c-product-meta-label c-font-uppercase c-font-bold">Color:</p>
									<select>
										<option value="Red">Red</option>
										<option value="Black">Black</option>
										<option value="Beige">Beige</option>
										<option value="White">White</option>
									</select>
								</div>
							</div>-->
							<div class="col-md-3 col-sm-3 col-xs-6">
								<div class="c-input-group c-spinner">
									<p class="c-product-meta-label c-product-margin-2 c-font-uppercase c-font-bold">QTY:</p>
									<input name="qty" type="text" class="form-control c-item-1" value="1">
									<div class="c-input-group-btn-vertical">
										<button class="btn btn-default" type="button" data_input="c-item-1">
											<i class="fa fa-caret-up"></i>
										</button>
										<button class="btn btn-default" type="button" data_input="c-item-1">
											<i class="fa fa-caret-down"></i>
										</button>
									</div>
								</div>
							</div>
							<div id="view_price" class="col-md-3 col-xs-12">
							<?php if($datas->vstatus == 1){
								if($datas->variant){
									$rp = $datas->variant[0]->publish;
									if(isset($datas->variant[0]->discount)){
										$dsc = $datas->variant[0]->discount;
										$dc = number_format($rp,2,',','.');
										$rp = (((100-$dsc)*$rp)/100);
										$non_curr = $rp;
										$rp = number_format($rp,2,',','.');
									}else{
										$rp = number_format($rp,2,',','.');
										$dsc = 0;
									}
								}else{//out of stock
									$rp = 'Sold Out';
									$dsc = 0;
									$non_curr = '';
									$datas->variant = array(0=>(object)array('settings_id'=> ''));
								}
								
								;?>	
								<p class="var_<?php echo $datas->object_id;?> c-price c-font-18 c-font-thin">IDR <?php echo $rp;?> &nbsp;
								<?php if($dsc != 0){;?>
									<br><span class="c-font-18 c-font-line-through c-font-red" id="cross-price"> IDR <?php echo $dc;?></span>
								<?php };?>
								</p>
							<?php }else{
								if($datas->stock == 0){
									$rp = 'Sold Out';
									$dsc = 0;
									$non_curr = '';
								}	
									
								;?>
								<p class="var_<?php echo $datas->object_id;?> c-price c-font-18 c-font-thin"> <?php echo $rp;?> &nbsp;
								<?php if($datas->discount != 0){;?>
									<br><span class="c-font-18 c-font-line-through c-font-red" id="cross-price"> <?php echo $dc;?></span>
								<?php };?>
								</p>
							<?php };?>
							</div>
							<div class="col-md-3 col-sm-12 col-xs-12">
							<?php if($datas->vstatus == 1){;?>
								<button data-url="cart" data-url2="cart" data-param="add" data-id="<?php echo $datas->object_id;?>" data-qty="1" data-price="<?php echo $non_curr;?>" data-name="<?php echo $datas->title;?>" class="add_to_cart btn c-btn btn-sm c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase pull-right" data-variant="<?php echo $datas->variant[0]->settings_id;?>"><i class="fa fa-shopping-cart"></i> <?php echo $text['add_cart'];?></button>
							<?php }else{;?>
								<button data-url="cart" data-url2="cart" data-param="add" data-id="<?php echo $datas->object_id;?>" data-qty="1" data-price="<?php echo $non_curr;?>" data-name="<?php echo $datas->title;?>" class="add_to_cart btn c-btn btn-sm c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase pull-right"><i class="fa fa-shopping-cart"></i> <?php echo $text['add_cart'];?></button>
							<?php };?>
							</div>
						</div>
						<div class="c-product-short-desc c-margin-t-20"> <?php echo $datas->description;?> </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
<?php if(isset($rltd_o_w_oct) && !empty($rltd_o_w_oct)){;?>
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
				<span class="c-bg-white">Related Product</span>
			</h3>
		</div>
		<div class="row">
			<?php foreach($rltd_o_w_oct as $d){
			$non_curr = $d->publish;
			$rp = 'IDR '.number_format($non_curr,2,',','.');
			$p_link = 'assets/'.$zone.'/product/'.$d->image_square;
			if($d->discount != 0){
				$non_curr = $d->publish*(1-($d->discount / 100));
				$rp = 'IDR '.number_format($non_curr,2,',','.');
				$dc = 'IDR '.number_format($d->publish,2,',','.');
			}
			;?>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white c-border">
					<div class="c-content-overlay">
						<?php if($d->discount != 0){;?>
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<?php };?>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(<?php echo site_url();?><?php echo $p_link;?>);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-16 c-font-slim"><?php echo $d->title;?></p>
						<p class="c-price c-font-14 c-font-slim"><?php echo $rp;?> &nbsp;
							<?php if($d->discount != 0){;?>
							<span class="c-font-14 c-font-line-through c-font-red"><?php echo $dc;?></span>
							<?php };?>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a data-url="cart" data-url2="cart" data-param="add" data-id="<?php echo $d->object_id;?>" data-qty="1" data-price="<?php echo $non_curr;?>" data-name="<?php echo $d->title;?>" class="add_to_cart btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		<?php };?>
		</div>
	</div>
</div>
<?php };?>
<!-- END: CONTENT/SHOPS/SHOP-2-2 -->	
<script>	
document.addEventListener("DOMContentLoaded", function(event) { 
	w1 = $('.c-product-gallery-content').width();
	w2 = $('.c-zoom > img').width();
	h2 = $('.c-zoom > img').height();
	w = (w1-w2)/2;
	$('.c-zoom > img').css('margin-left',w);
	
	<?php if(($zone == 'shisha') && isset($bc)){;?>
		<?php if(isset($bc['category'])){;?>
			<?php if($bc['category'] == 'member_card'){;?>
				$('.c-zoom > img').attr('style', ''); 
				setTimeout(function(){
					h3 = $('.c-zoom > img').height();
					$('.c-product-gallery-content').css('height',h3);
					$('.c-zoom').css('height',h3);
				}, 500);
				$('[name="qty"]').attr('disabled','disabled');
				$('[name="qty"]').next().hide();
			<?php }else{;?>
				$('.c-product-gallery-content').css('height',h2);
			<?php };?>
		<?php }else{;?>
			$('.c-product-gallery-content').css('height',h2);
		<?php };?>
	<?php }else{;?>
		$('.c-product-gallery-content').css('height',h2);
	<?php };?>
	$('[name="qty"]').attr({
       "max" : <?php echo $stock;?>,        
       "min" : 1          
    });
	$('.c-input-group-btn-vertical > button').click(function() {
		var target = $('[name="qty"]');
		var max = parseInt(target.attr('max'));
		var min = parseInt(target.attr('min'));
		
		setTimeout(function(){
			if (target.val() > max)
			{
				target.val(max);
			}
			else if (target.val() < min)
			{
				target.val(min);
			}
		}, 200);
		 
	});
	$('[name="qty"]').change(function() {
      var max = parseInt($(this).attr('max'));
      var min = parseInt($(this).attr('min'));
      if ($(this).val() > max)
      {
          $(this).val(max);
      }
      else if ($(this).val() < min)
      {
          $(this).val(min);
      }       
    }); 
})
function get_variant_id_detail()
{
	$('#variant').change(function() {
		if ($(this).find(':selected').val() != '') {
			var val = $(this).find(':selected').val(),
				item_id = $(this).find(':selected').data('id');
				stock = $(this).find(':selected').data('stock');
				prc = $(this).find(':selected').data('publish');
				discount = $(this).find(':selected').data('discount');
			$('.var_'+item_id+'').remove();
			if(discount != 0){
				real_price = prc;
				view_disc = '<br><span class="c-font-18 c-font-line-through c-font-red" id="cross-price"> IDR '+currencyformat(real_price)+'</span>';
				prc = (((100-discount)*prc)/100)+'.00';
			}else{
				view_disc = '';
			}
			$('[name="qty"]').attr({
			   "max" : stock  
			});
			if($('[name="qty"]').val() > stock){
				$('[name="qty"]').val(stock);
			};
			$('.add_to_cart[data-id="'+item_id+'"]').attr('data-variant',val);
			$('.add_to_cart[data-id="'+item_id+'"]').attr('data-price',prc);
			$('#view_price').append('<p class="var_'+item_id+' c-price c-font-18 c-font-thin">IDR '+currencyformat(prc)+' &nbsp;										'+view_disc+'									</p>');
			$('.add_to_cart[data-page="'+item_id+'"]').attr('data-variant',val);
			$('.add_to_cart[data-page="'+item_id+'"]').attr('data-price',prc);
		}
	});
}
</script>	