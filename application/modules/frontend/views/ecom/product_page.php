<?php if(isset($style['banner'])){;?>
<section class="c-layout-revo-slider c-layout-revo-slider-7" id="banner">
	<div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
		<div class="tp-banner">
			<ul>
				<?php foreach($datas[0]['datas'] as $b){
					$mime = mime_content_type('./assets/'.$zone.'/product/'.$b->image_square);
					if(strstr($mime, "video/")){
						$cek_file = 'video';
					}else if(strstr($mime, "image/")){
						$cek_file = 'image';
					}
					;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="500">
						<?php if($cek_file == 'image'){;?>
							<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/product/<?php echo $b->image_square;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<?php }else{;?>
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img src="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png" alt="">
							<div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false"
							data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%" data-videopreload="meta" data-videomp4="<?php echo base_url('assets');?>/<?php echo $zone;?>/product/<?php echo $b->image_square;?>" data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"
							data-aspectratio="16:9" data-volume="100" data-videoposter="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png"> </div>
							<div class="tp-caption arrowicon customin rs-parallaxlevel-0 visible-xs" data-x="center" data-y="bottom" data-hoffset="0" data-voffset="-60" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="500" data-start="2000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 13;">
								<div class="rs-slideloop" data-easing="Power3.easeInOut" data-speed="0.5" data-xs="-5" data-xe="5" data-ys="0" data-ye="0">
									<span class="c-video-hint c-font-15 c-font-sbold c-font-center c-font-dark"> Tap to play video
										<i class="icon-control-play"></i>
									</span>
								</div>
							</div>
							<!--END-->
						<?php };?>
						
						<!--BEGIN: MAIN TITLE -->
						<?php if($style['banner'] != 'no_detail'){;?>
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-280" data-voffset="-100" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							<?php echo $b->title;?>
							</h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-345" data-voffset="30" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 <?php echo word_limiter($b->description,6);?>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-445" data-voffset="140" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
						<?php };?>
					</li>
				<?php };?>
			</ul>
		</div>
	</div>
</section>

<?php };?>
<div class="c-content-box c-no-padding c-overflow-hide c-bs-grid-reset-space">
	<div class="row"><?php//print_r($datas);?>
		<?php if(isset($datas[1])){;$i = 1;foreach($datas[1]['datas'] as $b){
			$mime = mime_content_type('./assets/'.$zone.'/product/'.$b->image_square);
			if(strstr($mime, "video/")){
				$cek_file = 'video';
			}else if(strstr($mime, "image/")){
				$cek_file = 'image';
			}
			;?>
		<?php if($i == 1){;?>	
		<div class="col-md-6 col-sm-6">
			<div class="c-content-product-4 c-content-bg-1">
				<div class="col-md-6 col-sm-6">
					<div class="c-wrapper">
						<?php if($cek_file == 'image'){;?>
						<div class="c-side-image" style="background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/product/<?php echo $b->image_square;?>);"></div>
						<?php }else{;?>
						<div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false" data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%" data-videopreload="meta" data-videomp4="<?php echo base_url('assets');?>/<?php echo $zone;?>/product/<?php echo $b->image_square;?>" data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"	data-aspectratio="16:9" data-volume="100" data-videoposter="<?php echo base_url('assets');?>/img/layout/sliders/revo-slider/base/blank.png"> 
						</div>
						<?php };?>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-content c-align-right">
						<h3 class="c-title c-font-bold c-font-35 c-font-dark"><?php echo $b->title;?></h3>
						<p class="c-description c-font-20 c-font-regular"><?php echo $b->description;?></p>
						<p class="c-price c-font-60 c-font-thin c-font-dark">IDR -</p>
						<a href="#" class="btn btn-lg c-btn-grey-3 c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>
					</div>
				</div>
			</div>
		</div>
		<?php }else{;?>
		<div class="col-md-6 col-sm-6">
			<div class="c-content-product-4 c-content-bg-2">
				<div class="col-md-6 col-sm-6">
					<div class="c-content c-align-left">
						<h3 class="c-title c-font-bold c-font-35 c-font-dark"><?php echo $b->title;?></h3>
						<p class="c-description c-font-20 c-font-regular"><?php echo $b->description;?></p>
						<p class="c-price c-font-60 c-font-thin c-font-dark">IDR -</p>
						<a href="#" class="btn btn-lg c-btn-grey-3 c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-wrapper">
						<div class="c-side-image" style="background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/product/<?php echo $b->image_square;?>);"></div>
					</div>
				</div>
			</div>
		</div>
		<?php };?>
		<?php $i++;};};?>
	</div>
</div>
<?php if(isset($datas['product_list']['datas'][0])){;?>
<div class="c-content-box c-overflow-hide c-bs-grid-reset-space">
	<div class="row">
		<div class="col-md-6">
			<div class="c-content-product-5">
				<div class="c-bg-img-center" style="height:800px;background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][0]->image_square;?>)">
					<div class="c-detail c-bg-dark c-bg-opacity-2">
						<a href="<?php echo base_url($zone);?>/product" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
						<h3 class="c-title c-font-uppercase c-font-bold c-font-white c-font-90"><?php echo $datas['product_list']['datas'][0]->title;?></h3>
						<p class="c-desc c-font-white c-font-17"><?php echo $datas['product_list']['datas'][0]->tagline;?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20"><?php echo $datas['product_list']['datas'][1]->title;?> </h3>
								<a href="<?php echo base_url($zone);?>/about-moonshine" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">View More</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 400px; background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][1]->image_square;?>);"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
									<span class="c-font-thin"><?php echo $datas['product_list']['datas'][2]->title;?></span>
								</h3>
								<a href="<?php echo base_url($zone);?>/miscellaneous." class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">EXPLORE</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 400px; background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][2]->image_square;?>);"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="c-content-product-5">
						<div class="c-bg-img-center c-center" style="height:400px;background-image: url(<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $datas['product_list']['datas'][3]->image_square;?>)">
							<div class="c-wrapper c-center-vertical">
								<h3 class="c-title c-margin-tb-30 c-font-30 c-font-uppercase c-font-bold c-font-white">
									<span class="c-line">
										<span class="c-font-thin">New</span> <?php echo $datas['product_list']['datas'][3]->title;?></span>
								</h3>
								<a href="<?php echo base_url($zone);?>/<?php echo str_replace(' ','-',strtolower($datas['product_list']['datas'][3]->title));?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square c-margin-t-20">EXPLORE</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php };?>