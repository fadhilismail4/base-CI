<link href="<?php echo base_url('assets');?>/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<style>
.btn-white{
	background-color: #fff;
    border: 1px solid #ccc;
}
</style>
<div class="c-content-title-1">
	<a href="#" data-toggle="modal" data-target="#confirmation-form" class="btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase">Confirmation</a>
	<h3 class="c-font-uppercase c-font-bold">Order History</h3>
</div>

<div id="" class="row animated fadeInRight">
	<div id="" class="col-md-12 c-margin-b-40 c-order-history-2">
		<div id="" class="row c-cart-table-title">
			<div class="col-md-1 c-cart-image">
				<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">No</h3>
			</div>
			<div class="col-md-2 c-cart-ref">
				<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Kode Transaksi</h3>
			</div>
			<div class="col-md-2 c-cart-desc">
				<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2" style="text-align:center">Jumlah</h3>
			</div>
			<div class="col-md-2 c-cart-price">
				<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Price</h3>
			</div>
			<div class="col-md-2 c-cart-total">
				<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Payment Method</h3>
			</div>
			<div class="col-md-2 c-cart-qty">
				<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Date</h3>
			</div>
			<div class="col-md-1 c-cart-qty">
				<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">#</h3>
			</div>
		</div>
		<?php if(!empty($datas)){;?>
			<?php $i=$start+1;foreach($datas as $ds){;?>
			<div id="<?php echo $ds->trx_code;?>" class="list_order row c-cart-table-row">
				<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first"><?php echo $ds->trx_code;?></h2>
				<div class="col-md-1 col-sm-3 col-xs-5 c-cart-image">
					<p><?php echo $i;?></p> </div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-ref">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Kode Transaksi</p>
					<a href="#" id="<?php echo $ds->trx_code;?>" data-url="dashboard/view_detail/view" data-url2="view" data-lang="2" data-param="order_history" data-param2="list" class="get_variant_list_trx"><?php echo 'INV'.$ds->trx_code;?></a>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-6 c-cart-desc">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Jumlah</p>
					<p style="text-align:center">
						<a class="c-font-bold c-theme-link c-font-dark"><?php echo $ds->total;?></a>
					</p>
				</div>
				<div class="clearfix col-md-2 col-sm-3 col-xs-6 c-cart-price">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Price</p>
					<p class="c-cart-price c-font-bold"><?php echo 'IDR '.number_format($ds->count_prc,2,',','.');?></p>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-total">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Payment Method</p>
					<p class="c-cart-price c-font-bold"><?php if($ds->code == 'TRXON'){ echo ' Direct Bank Transfer';}else{ echo 'OFFLINE';};?></p>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-qty">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Date</p>
					<p><?php echo date('D, d M Y H:i:s',strtotime($ds->datecreated));?></p>
				</div>
				<div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Status</p>
					<p>
					<?php if($ds->status == 0){;?>
						<i class="fa fa-clock-o"></i>
					<?php }elseif($ds->status == 1){;?>
						<i class="fa fa-check"></i>
					<?php };?>
					</p>
				</div>
			</div>
			<?php $i++;};?>
		<?php }else{;?>
			<div id="" class="list_order row c-cart-table-row">
				<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first"></h2>
				<div class="col-md-1 col-sm-3 col-xs-5 c-cart-image">
					<p></p> </div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-ref">
					<p></p>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-6 c-cart-desc">
					<p style="text-align:center">
						
					</p>
				</div>
				<div class="clearfix col-md-2 col-sm-3 col-xs-6 c-cart-price">
					<p class="c-cart-price c-font-bold">
					</p>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-total">
					<p class="c-cart-price c-font-bold">
					</p>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-qty">
					<p>
					</p>
				</div>
				<div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
					<p>
					</p>
				</div>
			</div>
		<?php };?>
		<!--<a href="#" data-toggle="modal" data-target="#confirmation-form2" class="col-md-12 btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase"><i class="fa fa-chevron-down"></i>View Complete History</a>
		<a href="#" data-toggle="modal" data-target="#confirmation-form2" class="col-md-12 btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase"><i class="fa fa-chevron-up"></i>View Latest History</a>-->
	</div>
</div>
<?php if($is_pagination){;?>
Total: <?php print_r($total);?> Orders
<div class="btn-group pull-right">
	<?php if($offset != 0){;?>
		<button id="1" data-param2="<?php echo $offset-1;?>" data-url="dashboard" data-url2="view" data-lang="2" data-param="order_history" class="btn btn-white btn-sm detail"><i class="fa fa-arrow-left"></i></button>
	<?php };?>
	<?php if($total > $limit*($offset+1)){;?>
		<button id="1" data-param2="<?php echo $offset+1;?>" data-url="dashboard" data-url2="view" data-lang="2" data-param="order_history" class="btn btn-white btn-sm detail"><i class="fa fa-arrow-right"></i></button>
	<?php };?>
</div>
<?php };?>

<div class="modal fade c-content-login-form" id="confirmation-form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Confirmation</h3>
				<!--<p>
					Please upload your Image
				</p>-->
				<div class="alert alert-danger" id="fail-upload" style="display:none;"></div>
				<div class="alert alert-info" id="success-upload" style="display:none;"></div>
				<form role="form" id="confirmation" method="post">
					<div class="form-group">
						<label for="inv" class="">Invoice Number:</label>
						<select class="form-control input-lg c-square" id="inv">
							<option value="0">Invoice Number</option>
							<?php if(!empty($datas2)){;?>
								<?php foreach($datas2 as $ds2){;?>
								<option value="<?php echo $ds2->trx_code;?>"><?php echo $ds2->trx_code;?></option>
								<?php };?>
							<?php };?>
						</select>
					</div>
					<div class="form-group">
						<label for="file" class="">File:</label>
						<input type="file" class="file form-control input-lg c-square" name="file" id="file">
					</div>
					<div class="form-group">
						<button type="submit" id="dashboard/view_detail/view" class="conf btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade c-content-login-form" id="confirmation-form2" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Update Confirmation</h3>
				<!--<p>
					Please upload your Image
				</p>-->
				<div class="alert alert-danger" id="fail2-upload" style="display:none;"></div>
				<div class="alert alert-info" id="success2-upload" style="display:none;"></div>
				<form role="form" id="confirmation2" method="post">
					<div class="form-group">
						<label for="inv" class="">Invoice Number:</label>
						<select class="form-control input-lg c-square" id="inv2">
						</select>
					</div>
					<div class="form-group">
						<label for="file" class="">File:</label>
						<input type="file" class="file2 form-control input-lg c-square" name="file" id="file2">
					</div>
					<div class="form-group">
						<button type="submit" id="dashboard/view_detail/view" class="conf2 btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade c-content-login-form" id="cancel-order" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content c-square">
			<div class="modal-header c-no-border">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h3 class="c-font-24 c-font-sbold">Cancel Order</h3>
				<!--<p>
					Please upload your Image
				</p>-->
				<div class="alert alert-danger" id="cancel-fail-upload" style="display:none;"></div>
				<div class="alert alert-info" id="cancel-success-upload" style="display:none;"></div>
				<form role="form" id="cancel-confirmation" method="post">
					<div class="form-group">
						<label for="inv3" class="">Invoice Number:</label>
						<input id="inv3" class="form-control input-lg c-square" disabled></input>
					</div>
					<div class="form-group">
						<button type="submit" id="dashboard/view_detail/view" class="cancel-conf btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets');?>/admin/js/plugins/fileinput/fileinput.min.js"></script>
<script id="">
$('.detail_content').delegate(".get_variant_list_trx",'click',function(e){
	//$('.get_variant_list_trx').on("click", function(e) {
	e.preventDefault();
	$('[id*="list_"]').each(function(){
		$(this).slideUp();
	});
	//$(this).removeClass('get_variant_list_trx');
	//$(this).addClass('remove_list');
	var id = $(this).attr('id');
	var param = $(this).data('param');
	var param2 = $(this).data('param2');
	var url = $(this).data('url');
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		id : id,
		param : param,
		param2 : param2
	};
	$.ajax({
		url : url,
		type: "POST",
		dataType: 'json',
		data: data,
		success: function(data){
			$('[id*="list_"]').each(function(){
				$(this).remove();
			});
			view_list = [];
			shipping = '';
			cnf = data[0].cnf_status;
			rcp = data[0].rcp_status;
			$.each(data, function(key, val) {
				if(val.code != 'TRXOF'){
					/* if(val.status == 0){
						status = 'waiting for payment';
					}else if(val.status == 1){
						if(val.c_status == 0){
							status = 'Proccess';
						}else if(val.c_status == 1){
							status = 'Delivery';
						}else if(val.c_status == 2){
							status = 'Returned';
						}
					}else if(val.status == 2){
						status = 'Cancel by System';
					}else if(val.status == 3){
						status = 'Cancel by User';
					} */
					status = val.m;
				}else{
					status = '-';
				}
				if(val.image.length != 0){
					img = '<img src="'+val.image+'" />';
				}else{
					img = '';
				}
				if(val.title.indexOf('JNE') > -1){
					shipping = '<p style="text-align:left"><b>Shipment: </b>'+val.title+'('+currencyformat(val.price)+')</p>';
				}else{
					view_list[key] = '<div class="row c-cart-table-row">\
						<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">Item 1</h2>\
						<div class="col-md-3 col-sm-3 col-xs-5 c-cart-image">\
							'+img+'</div>\
						<div class="col-md-2 col-sm-3 col-xs-6 c-cart-ref">\
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Jumlah</p>\
							<p style="text-align:center">'+val.total+'</p>\
						</div>\
						<div class="clearfix col-md-2 col-sm-3 col-xs-6 c-cart-price">\
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Price</p>\
							<p class="c-cart-price c-font-bold">IDR '+currencyformat(val.price)+'</p>\
						</div>\
						<div class="col-md-2 col-sm-6 col-xs-6 c-cart-desc">\
							<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Description</p>\
								<p class="c-font-bold c-theme-link c-font-dark">'+val.title+'</p>\
								<p>'+val.varian+'</p>\
						</div>\
						<div class="col-md-2 col-sm-6 col-xs-12 c-cart-desc">\
						</div>\
					</div>';
				}
				
			});
			if(cnf.length != ''){
				if(cnf == 0){
					action1 = '';
					action2 = '<a href="#" data-toggle="modal" data-target="#confirmation-form2" class="btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase">Change File</a>';
					update_file(data[0].trx_code,data[0].cnf_img);
				}else if(cnf == 1){
					if(rcp.length != ''){
						if(rcp == 0){
							action1 = '';
							action2 = '';
						}else if(rcp == 1){
							action1 = '';
							action2 = '';
						}else if(rcp == 2){
							action1 = '';
							action2 = '';
						}else if(rcp == 3){
							action1 = '';
							action2 = '';
						}
					}else{
						action1 = '';
						action2 = '';
					}
				}else if(cnf == 2){
					action1 = '';
					action2 = '<a href="#" data-toggle="modal" data-target="#confirmation-form2" class="btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase">Change File</a>';
					update_file(data[0].trx_code,data[0].cnf_img);
				}
			}else{
				action1 = '<a href="#" data-toggle="modal" data-target="#cancel-order" class="btn btn-sm c-btn-border-1x pull-right c-btn-uppercase btn-danger">Cancel Order</a>';
				action2 = '<a href="#" data-toggle="modal" data-target="#confirmation-form" class="btn btn-sm c-btn-border-1x c-theme-btn pull-right c-btn-uppercase">Confirmation</a>';
				$('#inv').val(data[0].trx_code);
				$('#inv3').val(data[0].trx_code);
			}
			view_action = '<div class="row c-cart-table-row" style="background-color: #fff;">\
					<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">Item 1</h2>\
					<div class="col-md-3 col-sm-3 col-xs-5 c-cart-image">\
						<p style="text-align:left"><b>Status: </b>'+data[0].m+'</p>\
						'+shipping+'\
					</div>\
					<div class="col-md-2 col-sm-3 col-xs-6 c-cart-ref">\
					</div>\
					<div class="clearfix col-md-2 col-sm-3 col-xs-6 c-cart-price">\
					</div>\
					<div class="col-md-2 col-sm-6 col-xs-6 c-cart-desc">\
						'+action1+'\
					</div>\
					<div class="col-md-2 col-sm-6 col-xs-12 c-cart-desc">\
						'+action2+'\
					</div>\
				</div>';
			view_trx = '<div id="list_'+data[0].trx_code+'" class="col-md-12" style="background-color:#f9fbfc; display:none">\
					<div class="c-margin-b-40 c-order-history-2"><div id="" class="row c-cart-table-title">\
						<div class="col-md-3 c-cart-image">\
							<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Image</h3>\
						</div>\
						<div class="col-md-2 c-cart-ref">\
							<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2" style="text-align:center">Jumlah</h3>\
						</div>\
						<div class="col-md-2 c-cart-price">\
							<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Price</h3>\
						</div>\
						<div class="col-md-4 c-cart-desc">\
							<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Description</h3>\
						</div>\
						<div class="col-md-1">\
							<h3 class="remove_list c-font-uppercase c-font-bold c-font-16 c-font-grey-2 pull-right"><a href="#" style="padding-right:10px"><i class="fa fa-remove"></i></a></h3>\
						</div>\
					</div>\
					'+view_list+'\
					'+view_action+'\
				</div></div>';
				$('#'+data[0].trx_code+'.list_order').append(view_trx.replace(/,/g, ""));
				$('#list_'+data[0].trx_code+'').slideDown(500);
			$('.remove_list').on("click", function(e) {
				$('[id*="list_"]').each(function(){
					$(this).slideUp();
				});
				setTimeout(function(){
					$('[id*="list_"]').each(function(){
						$(this).remove();
					});
				}, 500);
			});	
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
	function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script');
});


$(".file").fileinput("refresh",{
	showRemove: false,
	showUpload: false,
});

function update_file(trx_code,img){
	if(trx_code){
		view_option = '<option value="'+trx_code+'">'+trx_code+'</option>';
		$('#inv2').empty();
		$('#inv2').append(view_option);
		$('#inv2').val(trx_code);
		img = '<?php echo base_url();?>assets/<?php echo $zone;?>/trx/cnf/'+img;
		$(".file2").fileinput("refresh",{
			showRemove: false,
			showUpload: false,
			initialPreview: [
				"<img src="+img+" class='file-preview-image'>"
			]
		});
	}
}

$(document).delegate(".conf",'click',function(e){
//$('#conf').on('click',function(e){ 
	
	e.preventDefault();  
	$('#success-upload').empty();
	$('#fail-upload').empty();
	$('.conf').attr('disabled','disabled');
	var inputFilePhoto = document.getElementById("file");
	var imageFile = inputFilePhoto.files[0];

	/* var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		id : $('#inv').val(),
		file: imageFile
	}; */
	
	var formData = new FormData();             
	formData.append("<?php echo $this->security->get_csrf_token_name(); ?>", '<?php echo trim($this->security->get_csrf_hash()); ?>');
	formData.append("param", 'order_history');
	formData.append("param2", 'confirmation');
	formData.append("id", $('#inv').val());
	formData.append("file", imageFile);
	
	var base_url = '<?php echo base_url();?>';
	var fp_url = $(this).attr('id');
	$.ajax({ 
		type: "POST",
		url: fp_url,
		secureuri: false,
		contentType: false,
		processData: false,
		dataType: 'JSON',
		data: formData,
		success: function(data){
			if (data.status == "success"){
				$('<p>'+data.m+'</p>').appendTo('#success-upload');
				$('#success-upload').show();scrolltonote('#confirmation-form');
				$('#success-upload').fadeTo(3000, 500).slideUp(500);
				setTimeout(function(){
					$('#confirmation-form').modal('toggle');
					$('.modal-backdrop').remove();
					$('body').removeClass('modal-open');
				}, 600);
				setTimeout(function(){
					$('.c-dropdown-menu > .c-active > a')[0].click();
				}, 1000);
			}else if(data.status == "ambigue"){
				type = data.m;
				modal_ambigue(type);
			}else{
				$('<p>'+data.m+'</p>').appendTo('#fail-upload');
				$('.conf').removeAttr('disabled');
				$('#fail-upload').show();
				scrolltonote('#confirmation-form');
				$('#fail-upload').fadeTo(3000, 500).slideUp(500);
			}
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
	function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script');
	$('#fail-upload').empty(); $('#success-upload').empty();
});

$(document).delegate(".conf2",'click',function(e){
//$('#conf').on('click',function(e){ 
	
	e.preventDefault();  
	
	$('#success2-upload').empty();
	$('#fail2-upload').empty();
	$('.conf2').attr('disabled','disabled');
	var inputFilePhoto = document.getElementById("file2");
	var imageFile = inputFilePhoto.files[0];

	/* var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		id : $('#inv').val(),
		file: imageFile
	}; */
	
	var formData = new FormData();             
	formData.append("<?php echo $this->security->get_csrf_token_name(); ?>", '<?php echo trim($this->security->get_csrf_hash()); ?>');
	formData.append("param", 'order_history');
	formData.append("param2", 'confirmation');
	formData.append("id", $('#inv2').val());
	formData.append("file", imageFile);
	
	var base_url = '<?php echo base_url();?>';
	var fp_url = $(this).attr('id');
	$.ajax({ 
		type: "POST",
		url: fp_url,
		secureuri: false,
		contentType: false,
		processData: false,
		dataType: 'JSON',
		data: formData,
		success: function(data){
			if (data.status == "success"){
				$('<p>'+data.m+'</p>').appendTo('#success2-upload');
				$('#success2-upload').show();scrolltonote('#confirmation-form2');
				$('#success2-upload').fadeTo(3000, 500).slideUp(500);
				setTimeout(function(){
					$('#confirmation-form2').modal('toggle');
					$('.modal-backdrop').remove();
					$('body').removeClass('modal-open');
				}, 600);
				setTimeout(function(){
					$('.c-dropdown-menu > .c-active > a')[0].click();
				}, 1000);
			}else if(data.status == "ambigue"){
				type = data.m;
				modal_ambigue(type);
			}else{
				$('<p>'+data.m+'</p>').appendTo('#fail2-upload');
				$('.conf2').removeAttr('disabled');
				$('#fail2-upload').show();
				scrolltonote('#confirmation-form2');
				$('#fail2-upload').fadeTo(3000, 500).slideUp(500);
			}
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
	function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script');
	$('#fail2-upload').empty(); $('#success2-upload').empty();
});

$(document).delegate(".cancel-conf",'click',function(e){
	e.preventDefault();  
	
	$('#cancel-success-upload').empty();
	$('#cancel-fail-upload').empty();
	$('.cancel-conf').attr('disabled','disabled');
	
	var formData = new FormData();             
	formData.append("<?php echo $this->security->get_csrf_token_name(); ?>", '<?php echo trim($this->security->get_csrf_hash()); ?>');
	formData.append("param", 'order_history');
	formData.append("param2", 'confirmation');
	formData.append("param3", 'cancel');
	formData.append("id", $('#inv3').val());
	
	var base_url = '<?php echo base_url();?>';
	var fp_url = $(this).attr('id');
	$.ajax({ 
		type: "POST",
		url: fp_url,
		secureuri: false,
		contentType: false,
		processData: false,
		dataType: 'JSON',
		data: formData,
		success: function(data){
			if (data.status == "success"){
				$('<p>'+data.m+'</p>').appendTo('#cancel-success-upload');
				$('#cancel-success-upload').show();scrolltonote('#cancel-confirmation');
				$('#cancel-success-upload').fadeTo(3000, 500).slideUp(500);
				setTimeout(function(){
					$('#cancel-confirmation').modal('toggle');
					$('.modal-backdrop').remove();
					$('body').removeClass('modal-open');
				}, 600);
				setTimeout(function(){
					$('.c-dropdown-menu > .c-active > a')[0].click();
				}, 1000);
			}else if(data.status == "ambigue"){
				type = data.m;
				modal_ambigue(type);
			}else{
				$('<p>'+data.m+'</p>').appendTo('#cancel-fail-upload');
				$('.cancel-conf').removeAttr('disabled');
				$('#cancel-fail-upload').show();
				scrolltonote('#cancel-confirmation');
				$('#cancel-fail-upload').fadeTo(3000, 500).slideUp(500);
			}
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
	function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script');
	$('#cancel-fail-upload').empty(); $('#success-upload').empty();
});
function modal_ambigue(type){
	$('[id*="modal_"]').each(function(){
		$(this).remove();
	});
	var id = type.old_inv,
		base_url = '<?php echo base_url();?>',
		part = 'edit';
	var url = "dashboard/view_detail/view";
	var lang = 'aw2';//$(this).data('lang');
	var param = 'aw3';//$(this).data('param');
	var status = 'Save';//$(this).data('status');
	view_data = '<div class="col-sm-12">'+
				'<h2>'+type.message+'</h2>'+
				'<div class="row">'+
				'<div class="col-sm-3"><img class="col-md-12" src="'+type.old_data+'"></div>'+
				'<div class="col-sm-3"><img class="col-md-12" src="'+type.old_data+'"></div>'+
				'</div>'+
				'</div>';
	view_modal = 
		'<div class="modal fade" id="modal_'+id+'" tabindex="-1" role="dialog">'+
			'<div class="modal-dialog modal-lg">'+
				'<div class="modal-content">'+
					'<div class="modal-header">'+
						'<button type="button" class="close" data-dismiss="modal">'+
							'<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>'+
						'</button>'+
						'<h5 class="modal-title" style="text-transform:uppercase;float: none;">'+id+'</h5>'+
					'</div>'+
					'<div class="modal-body">'+
						'<div class="alert alert-danger" id="fail" style="display:none;"></div>'+
						'<div class="alert alert-info" id="success" style="display:none;"></div>'+
						'<div id="data" class="row">'+
							view_data+
						'</div>'+
						'<button type="submit" id="dashboard/view_detail/view" class="conf btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	$('#confirmation-form').parent().append(view_modal.replace(/,/g, ""));
	$('#modal_'+id+'').modal('toggle');
	//$('.modal-backdrop').css('display','none');
};
</script>