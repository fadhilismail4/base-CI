<style>
.ui-menu .ui-menu-item {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    box-shadow: none;
    outline: none;
    font-weight: 300;
    /* background: white; */
    border-color: #d0d7de;
	padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
}
<?php if($zone == 'moonshine'){;?>
.c-theme-color{
	color: #000 !important;
}
p.c-font-white{
	color: #000 !important;
}
.c-theme-border {
    border-color: #000 !important;
}
<?php };?>
</style>
<input type="hidden" id="data-checkout" data-base-url="<?php echo base_url();?>" data-zone="<?php echo $zone;?>" data-url-zone="<?php echo $menu['link'];?>" data-base-modul="<?php echo $bc['module'];?>" data-url-first="list" data-url-second="list" data-id="<?php if(isset($bc['category'])){ echo $bc['category'];}else{ echo 'all';};?>" data-param="<?php echo $bc['module'];?>"/>
<?php if(!$this->cart->contents()) : ?>
<div class="c-content-box c-size-lg">
	<div class="container">
		<div class="c-shop-cart-page-1 c-center">
			<i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i>
			<h2 class="c-font-thin c-center">Your Shopping Cart is Empty</h2>
			<a href="<?php echo base_url($zone);?>/product" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a>
		</div>
	</div>
</div>
<?php else : ?>
<div class="c-content-box c-size-lg">
	<div class="container">
		<form id="f-checkout" class="c-shop-form-1">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<div class="row">
				<!-- BEGIN: ADDRESS FORM -->
				<div class="col-md-7 c-padding-20">
					<?php if(isset($user)){;?>
					<?php if(!empty($user)){;?>
					<!-- BEGIN: BILLING ADDRESS -->
					<h3 class="c-font-bold c-font-uppercase c-font-24">Billing Address</h3>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-12">
									<label class="control-label">Full Name</label>
									<input id="name" name="inputan" type="text" class="required form-control c-square c-theme" placeholder="Full Name" value="<?php echo $user->name;?>">
								</div>
							</div>
						</div>
					</div>
					<!--<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Company Name</label>
							<input id="" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Company Name">
						</div>
					</div>-->
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6">
									<label class="control-label">State / County</label>
									<select id="country" name="inputan" class="form-control c-square c-theme" onchange="get_data_ongkir('loc_id', 'af1', 'af11')">
										<option value="0">Select an option...</option>
										<!--<option value="1">Malaysia</option>
										<option value="2">Singapore</option>-->
										<option value="3">Indonesia</option>
										<!--<option value="4">Thailand</option>
										<option value="5">China</option>-->
									</select>
								</div>
								<div class="col-md-6">
									<label class="control-label">Town / City</label>
									<!--<input id="city" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Town / City"> -->
									<select id="loc_id" name="inputan" class="form-control" disabled="disabled" onchange="get_price_ongkir('loc_id')">
										<option value="">Select Town/City</option>
									</select> 
								</div>
							</div>
						</div>
					</div>
					<div id="af1" class="row" style="">
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6">
									<label class="control-label">Address</label>
									<input id="address" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Street Address" value="<?php echo $user->address;?>" required>
								</div>
								<div class="col-md-6">
									<label class="control-label">Postcode / Zip</label>
									<input id="zip" name="inputan" type="text" class="form-control c-square c-theme" value="" placeholder="Postcode / Zip"> 
								</div>
							</div>
						</div>
					</div>
					<div id="af11" class="row" style="">
						<div class="form-group col-md-12">
							<div class="row">
								<div class="form-group col-md-6">
									<label class="control-label">Email Address</label>
									<input id="email" name="inputan" type="email" class="form-control c-square c-theme" placeholder="Email Address" value=""> </div>
								<div class="col-md-6">
									<label class="control-label">Phone</label>
									<input id="phone" name="inputan" type="tel" class="form-control c-square c-theme" placeholder="Phone" value=""> </div>
							</div>
						</div>
					</div>
					<!--<div class="row">
						<div class="form-group col-md-12">
							<input id="" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Apartment, suite, unit etc. (optional)"> 
						</div>
					</div>-->
					<!-- BILLING ADDRESS -->
					<!-- SHIPPING ADDRESS -->
					<h3 class="c-font-bold c-font-uppercase c-font-24">Shipping Address</h3>
					<div class="row">
						<div class="form-group col-md-12">
							<div class="c-checkbox-inline">
								<div class="c-checkbox c-toggle-hide" data-object-selector="c-shipping-address" data-animation-speed="600">
									<input type="checkbox" id="different_address" class="c-check">
									<label for="different_address">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span> Ship to different address? 
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="c-shipping-address">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">State / County</label>
										<select class="form-control c-square c-theme" onchange="get_data_ongkir('loc_id2', 'af2', 'af22')">
											<option value="0">Select an option...</option>
											<!--<option value="1">Malaysia</option>
											<option value="2">Singapore</option>-->
											<option value="3">Indonesia</option>
											<!--<option value="4">Thailand</option>
											<option value="5">China</option>-->
										</select>
									</div>
									<div class="col-md-6">
										<label class="control-label">Town / City</label>
										<select id="loc_id2" name="inputan" class="form-control" disabled="disabled" onchange="get_price_ongkir('loc_id2')">
											<option value="">Select Town/City</option>
										</select>
										<!--<input type="text" class="form-control c-square c-theme" placeholder="Town / City">--> 
									</div>
								</div>
							</div>
						</div>
						<div id="af2" class="row" style="">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">Address</label>
										<input id="address_2" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Street Address" value="<?php echo $user->address;?>" required>
									</div>
									<div class="col-md-6">
										<label class="control-label">Postcode / Zip</label>
										<input id="zip_2" name="inputan" type="text" class="form-control c-square c-theme" value="" placeholder="Postcode / Zip"> 
									</div>
								</div>
							</div>
						</div>
						<!--<div id="af22" class="row" style="">
							<div class="form-group col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">Email Address</label>
										<input id="email" name="inputan" type="email" class="form-control c-square c-theme" placeholder="Email Address" value=""> </div>
									<div class="col-md-6">
										<label class="control-label">Phone</label>
										<input id="phone" name="inputan" type="tel" class="form-control c-square c-theme" placeholder="Phone" value=""> </div>
								</div>
							</div>
						</div>-->
					</div>
					<!-- SHIPPING ADDRESS -->
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Order Notes</label>
							<textarea id="notes" name="inputan" class="form-control c-square c-theme" rows="3" placeholder="Note about your order, e.g. special notes for delivery."></textarea>
						</div>
					</div>
					<?php }else{;?>
					<h3 class="c-font-bold c-font-uppercase c-font-24">Billing Address</h3>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-12">
									<label class="control-label">Full Name</label>
									<input id="name" name="inputan" type="text" class="required form-control c-square c-theme" placeholder="Full Name">
								</div>
							</div>
						</div>
					</div>
					<!--<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Company Name</label>
							<input id="" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Company Name">
						</div>
					</div>-->
					<!--<div class="row">
						<div class="form-group col-md-12">
							<input id="" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Apartment, suite, unit etc. (optional)"> 
						</div>
					</div>-->
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6">
									<label class="control-label">State / County</label>
									<select id="country" name="inputan" class="form-control c-square c-theme" onchange="get_data_ongkir('loc_id', 'af3', 'af33')">
										<option value="0">Select an option...</option>
										<!--<option value="1">Malaysia</option>
										<option value="2">Singapore</option>-->
										<option value="3">Indonesia</option>
										<!--<option value="4">Thailand</option>
										<option value="5">China</option>-->
									</select>
								</div>
								<div class="col-md-6">
									<label class="control-label">Town / City</label>
									<select id="loc_id" name="inputan" class="form-control" disabled="disabled" onchange="get_price_ongkir('loc_id')">
										<option value="">Select Town/City</option>
									</select>
									<!--<input id="city" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Town / City" disabled='disabled'>-->
								</div>
							</div>
						</div>
					</div>
					<div id="af3" class="row" style="display:none">
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6">
									<label class="control-label">Address</label>
									<input id="address" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Street Address" required>
								</div>
								<div class="col-md-6">
									<label class="control-label">Postcode / Zip</label>
									<input id="zip" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Postcode / Zip"> 
								</div>
							</div>
						</div>
					</div>
					<div id="af33" class="row" style="display:none">
						<div class="form-group col-md-12">
							<div class="row">
								<div class="form-group col-md-6">
									<label class="control-label">Email Address</label>
									<input id="email" name="inputan" type="email" class="form-control c-square c-theme" placeholder="Email Address"> </div>
								<div class="col-md-6">
									<label class="control-label">Phone</label>
									<input id="phone" name="inputan" type="tel" class="form-control c-square c-theme" placeholder="Phone"> </div>
							</div>
						</div>
					</div>
					<div class="row c-margin-t-15">
						<div class="form-group col-md-12">
							<div class="c-checkbox c-toggle-hide" data-object-selector="c-account" data-animation-speed="600">
								<input type="checkbox" id="checkbox1-77" class="c-check">
								<label for="checkbox1-77">
									<span class="inc"></span>
									<span class="check"></span>
									<span class="box"></span> Create an account? </label>
							</div>
							<p class="help-block">Create an account by entering the information below. If you are a returning customer please login.</p>
						</div>
					</div>
					<div class="row c-account">
						<div class="form-group col-md-6">
							<label class="control-label">Account Username</label>
							<input id="username" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Username"> 
						</div>
						<div class="form-group col-md-6">
							<label class="control-label">Account Password</label>
							<input id="password" name="inputan" type="password" class="form-control c-square c-theme" placeholder="Password"> 
						</div>
					</div>
					<!-- BILLING ADDRESS -->
					<!-- SHIPPING ADDRESS -->
					<h3 class="c-font-bold c-font-uppercase c-font-24">Shipping Address</h3>
					<div class="row">
						<div class="form-group col-md-12">
							<div class="c-checkbox-inline">
								<div class="c-checkbox c-toggle-hide" data-object-selector="c-shipping-address" data-animation-speed="600">
									<input type="checkbox" id="different_address" class="c-check">
									<label for="different_address">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span> Ship to different address? 
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="c-shipping-address">
						<!--<div class="row">
							<div class="form-group col-md-12">
								<label class="control-label">Country</label>
								<select id="" name="inputan" class="form-control c-square c-theme">
									<option value="1">Malaysia</option>
									<option value="2">Singapore</option>
									<option value="3">Indonesia</option>
									<option value="4">Thailand</option>
									<option value="5">China</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group col-md-12">
										<label class="control-label">Full Name</label>
										<input type="text" class="form-control c-square c-theme" placeholder="First Name"> </div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<label class="control-label">Company Name</label>
								<input type="text" class="form-control c-square c-theme" placeholder="Company Name"> </div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<input type="text" class="form-control c-square c-theme" placeholder="Apartment, suite, unit etc. (optional)"> </div>
						</div>-->
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">State / County</label>
										<select class="form-control c-square c-theme" onchange="get_data_ongkir('loc_id2', 'af4', 'af44')">
											<option value="0">Select an option...</option>
											<!--<option value="1">Malaysia</option>
											<option value="2">Singapore</option>-->
											<option value="3">Indonesia</option>
											<!--<option value="4">Thailand</option>
											<option value="5">China</option>-->
										</select>
									</div>
									<div class="col-md-6">
										<label class="control-label">Town / City</label>
										<!--<input type="text" class="form-control c-square c-theme" placeholder="Town / City"> -->
										<select class="form-control" id="loc_id2" name="inputan" disabled="disabled" onchange="get_price_ongkir('loc_id2')">
											<option value="">Select Town/City</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row" id="af4">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">Address</label>
										<input id="address_2" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Street Address">
									</div>
									<div class="col-md-6">
										<label class="control-label">Postcode / Zip</label>
										<input id="zip_2" name="inputan" type="text" class="form-control c-square c-theme" placeholder="Postcode / Zip"> </div>
								</div>
							</div>
						</div>
						<!--<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">Email Address</label>
										<input type="email" class="form-control c-square c-theme" placeholder="Email Address"> </div>
									<div class="col-md-6">
										<label class="control-label">Phone</label>
										<input type="tel" class="form-control c-square c-theme" placeholder="Phone"> </div>
								</div>
							</div>
						</div>-->
					</div>
					<!-- SHIPPING ADDRESS -->
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Order Notes</label>
							<textarea id="notes" name="inputan" class="form-control c-square c-theme" rows="3" placeholder="Note about your order, e.g. special notes for delivery."></textarea>
						</div>
					</div>
					<?php };?>
					<?php };?>
				</div>
				<!-- END: ADDRESS FORM -->
				<!-- BEGIN: ORDER FORM -->
				<div class="col-md-5">
					<div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
						<h1 class="c-font-bold c-font-uppercase c-font-24">Your Order</h1>
						<ul class="c-order list-unstyled">
							<li class="row c-margin-b-15">
								<div class="col-md-5 c-font-20">
									<h2>Product</h2>
								</div>
								<div class="col-md-4 c-font-20">
									<h2>Total</h2>
								</div>
								<div class="col-md-3 c-font-20">
									<h2>Weight</h2>
								</div>
							</li>
							<li class="row c-border-bottom"></li>
							<?php $total = 0;$total_w = 0; foreach($this->cart->contents() as $items): ?>
							<li class="row c-margin-b-15 c-margin-t-15">
								<div class="col-md-5 c-font-16">
									<a href="#" class="c-theme-link"><p><?php echo $items['name']; ?> x <?php echo $items['qty']; ?></p></a>
								</div>
								<div class="col-md-4 c-font-16">
									<p class=""><?php echo 'IDR '.number_format($items['subtotal'],2,',','.'); ?></p>
								</div>
								<div class="col-md-3 c-font-16">
									<p>@<?php echo floatval($items['options']['weight']).' Kg'; ?></p>
									<p><?php echo floatval($items['options']['weight']*$items['qty']).' Kg'; ?></p>
								</div>
							</li>
							<?php $total = $total + $items['subtotal']; $total_w = $total_w + ($items['options']['weight']*$items['qty']);endforeach; ?>
							<li class="row c-margin-b-15 c-margin-t-15">
								<div class="col-md-5 c-font-bold c-font-16">Subtotal</div>
								<div class="col-md-4 c-font-bold c-font-16">
									<p class="">IDR
										<span class="c-subtotal"><?php echo ''.number_format($total,2,',','.'); ?></span>
									</p>
								</div>
								<div class="col-md-3 c-font-bold c-font-16">
									<p class="">
										<span class="c-subtotal"><?php echo $total_w; ?> Kg</span>
									</p>
								</div>
							</li>
							<li class="row c-border-top c-margin-b-15"></li>
							<li class="row">
								<div class="col-md-5 c-font-bold c-font-16">Shipping<p id="shipment_code"></p></div>
								<div class="col-md-6">
									<div class="c-radio-list c-shipping-calculator" data-name="shipping_price" data-subtotal-selector="c-subtotal" data-total-selector="c-shipping-total">
									</div>
								</div>
							</li>
							<li class="row c-border-top c-margin-b-15 c-margin-t-15"></li>
							<li class="row c-margin-b-15 c-margin-t-15">
								<div class="col-md-5 c-font-20">
									<p class="c-font-bold c-font-20">Total</p>
								</div>
								<div class="col-md-6 c-font-20">
									<p class="c-font-bold c-font-20">IDR
										<span class="c-shipping-total"><?php echo ''.number_format($total,2,',','.'); ?></span>
									</p>
								</div>
							</li>
							<li class="row">
								<div class="col-md-12">
									<div class="c-radio-list">
										<div class="c-radio">
											<input type="radio" id="radio1" class="c-radio" name="payment" checked="">
											<label for="radio1" class="c-font-bold c-font-18">
												<span class="inc"></span>
												<span class="check"></span>
												<span class="box"></span> Direct Bank Transfer </label>
											<p class="help-block">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won't be shipped until the funds have cleared in our account.</p>
										</div>
										<!--<div class="c-radio">
											<input type="radio" id="radio2" class="c-radio" name="payment">
											<label for="radio2" class="c-font-bold c-font-20">
												<span class="inc"></span>
												<span class="check"></span>
												<span class="box"></span> Cheque Payment </label>
										</div>
										<div class="c-radio">
											<input type="radio" id="radio3" class="c-radio" name="payment">
											<label for="radio3" class="c-font-bold c-font-20">
												<span class="inc"></span>
												<span class="check"></span>
												<span class="box"></span> Paypal </label>
											<img class="img-responsive" width="250" src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png" /> </div>
										-->	
									</div>
								</div>
							</li>
							<!--<li class="row c-margin-b-15 c-margin-t-15">
								<div class="form-group col-md-12">
									<div class="c-checkbox">
										<input type="checkbox" id="checkbox1-11" class="c-check">
										<label for="checkbox1-11">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span> I've read and accept the Terms & Conditions </label>
									</div>
								</div>
							</li>-->
							<li class="row">
								<div class="form-group col-md-12" role="group">
									<?php if($zone == 'shisha'){ ;?>
									<?php if($is_login && !$reg_id){;?>
										<a href="<?php echo base_url();?>product/member_card" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login c-cart-float-l" style ="margin-right: 10px">Buy Member Card</a>
									<?php }else if(!$is_login){;?>
										<a href="#" data-toggle="modal" data-target="#member-area-form" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login c-cart-float-l" style="margin-right: 10px">Member Area</a>
									<?php };?>
									<?php } ;?>
									<button id="checkout" class="btn_checkout btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
									<button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
</div>
<?php endif; ?>	

<link href="<?php echo base_url();?>assets/frontend/css/select2.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/frontend/css/select2-bootstrap.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript">
function scrolltonote(selector){
	 var sel = selector
	 $('html, body').animate({
		scrollTop: $(sel).offset().top
	}, 1000);
}

function get_data_ongkir(pilih, af, af2)
{
	$('#'+pilih).empty();
	var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					param : 'city',
					param2 : $('#country').find(':selected').text().toLowerCase()
				}
	$.ajax({
			url : "<?php echo $menu['link'];?>/cek/api/rajaongkir",
			secureuri: false,
			type: "POST",
			dataType: 'json',
			data: data,
			success: function(data){
				$.each(data, function (i, value) {
					var arr = data;
					$('#'+pilih).append($('<option>', { 
			        	text: arr[i].city_name+' ('+arr[i].type+') '+arr[i].province,
			        	value :  arr[i].city_id
			    	}));
				});
				$('#'+pilih).removeAttr('disabled','disabled');
				$('#'+af).slideDown();
				$('#'+af2).slideDown();
			}
		});
}

function get_price_ongkir(pilih)
{
	var id = $('#'+pilih).val();
	var data2 = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		param : 'cost',
		param2 : id,
		param3 : '<?php echo $total_w;?>'
	}
	$('#shipment_code').empty();
	$.ajax({
		url : "<?php echo $menu['link'];?>/cek/api/rajaongkir",
		secureuri: false,
		type: "POST",
		dataType: 'json',
		data: data2,
		success: function(data){
			$.each(data, function(key, val) {
				jenis = val.code;
				view_list = [];
				$('#shipment_code').append(jenis.toUpperCase());
				if(val.costs.length != 0){
					$.each(val.costs, function(key2, val2) {
						service = val2.service;
						if(val2.cost[0].etd){
							etd = '('+val2.cost[0].etd+' days)';
						}else{
							etd = '';
						}
						real_price = val2.cost[0].value;
						price = currencyformat(val2.cost[0].value);
						view_list[key2] = '<div class="c-radio">\
								<input type="radio" data-id="'+jenis+'-'+service+'-'+real_price+'" value="'+real_price+'" id="radio1'+key2+'" class="c-radio" name="shipping_price" checked="">\
								<label for="radio1'+key2+'" class="c-font-16">\
									<span class="inc"></span>\
									<span class="check"></span>\
									<span class="box"></span> '+service+' '+etd+'</label>\
								<p class="c-shipping-price c-font-bold c-font-16">IDR '+price+',00</p>\
							</div>';
					});
				}else{
					view_list = '<div class="c-radio">\
								<p class="c-font-16">No Shipping to the Town / City</p>\
							</div>';
				}
				
				$('.c-shipping-calculator').empty();
				$('.c-shipping-calculator').append(view_list);
				var $shipping_calculator = $('.c-shipping-calculator'),
					$radio_name = $($shipping_calculator).data('name'),
					$total_placeholder = $($shipping_calculator).data('total-selector'),
					$subtotal_placeholder = $($shipping_calculator).data('subtotal-selector'),
					$subtotal = parseFloat($('.' + $subtotal_placeholder).text().replace(/\./g, "")),
					expedisi_val = $('input[name=' + $radio_name + ']:checked', $shipping_calculator).attr('data-id');
					$shipping_calculator.append('<input type="hidden" name="inputan" id="expedisi" value="'+expedisi_val+'">');
				$('input[name=' + $radio_name + ']', $shipping_calculator).on('change', function () {
					var $price = parseFloat($('input[name=' + $radio_name + ']:checked', $shipping_calculator).val()),
						$overall_total = $subtotal + $price;
					$('.' + $total_placeholder).text(currencyformat($overall_total)+',00');
					var expedisi_val = $('input[name=' + $radio_name + ']:checked', $shipping_calculator).attr('data-id');
					$('#expedisi').remove();
					$shipping_calculator.append('<input type="hidden" name="inputan" id="expedisi" value="'+expedisi_val+'">');
				});
			});
		}
	});
}


$(document).ready(function(){
	$.getScript( "<?php echo base_url();?>assets/frontend/js/select2.min.js" ).done(function( script, textStatus ) {
		$('select#loc_id.form-control').select2();
		$('select#loc_id2.form-control').select2();
	});
	<?php if(isset($user)){;?>	
		<?php if(!empty($user)){;?>
			$('#country').val(3).change();
			<?php if($user->email){;?>
				$('#email').each(function(){
					$(this).val('<?php echo $user->email;?>');
				});
			<?php };?>
			$('#zip').each(function(){
				$(this).val('<?php echo $user->zip;?>');
			});
			$('#phone').each(function(){
				$(this).val('<?php echo $user->phone;?>');
			});
		<?php };?>
	<?php };?>		
});

$('.btn_checkout').on("click", function(e) {
	var cek = true;
	e.preventDefault();
	$('#f-checkout > #success').empty();
	$('#f-checkout > #fail').empty();
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	var obj = $(this).attr('id');
		param = $(this).data('param');
	function get_val(){
		var item_obj = {city: $("#loc_id option:selected").text(), city_2 : $("#loc_id2 option:selected").text()};
			$('[name="inputan"]').each(function(){
				item_obj[this.id] = this.value;
				if(this.classList.contains('required')){
					if(this.value){
					}else{
						cek = false;
						$(this).css('border','2px solid #ff9900');
					}
				}
			});
		var item_array = {};
			$('[name="inputan_array"]').each(function(){
				item_array[this.id] = $(this).chosen().val();
			});
		$.extend($.extend($.extend($.extend(item_obj, item_array),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param}),{loc_id : $('#city').data('id')});
		return item_obj;
	}
	var data = get_val();
	if(cek == true){
		$.ajax({
			url : "<?php echo $menu['link'];?>/cart/view_detail/checkout",
			secureuri: false,
			type: "POST",
			dataType: 'json',
			data: data,
			success: function(data){
				if (data.status == "success"){
					$('<p>'+data.m+'</p>').appendTo('#f-checkout > #success');
					scrolltonote('.c-layout-page');
					$('#f-checkout > #success').show();
					$('#f-checkout > #success').fadeTo(2000, 500).slideUp(500);
					$('.btn').each(function(){$(this).removeAttr('disabled');});
					view_list = [];
					view_bank = [];
					view_bank_account = [];
					view_bank_number = [];
					$.each(data.data, function(key, val) {
						if(val.varian.length != 0){
							varian = '';
						}else{
							varian = '';
						}
						view_list[key] = '<div class="c-border-bottom c-row-item">\
					<div class="row">\
						<div class="col-md-3 col-sm-12 c-image">\
							<div class="c-content-overlay">\
								<div class="c-overlay-wrapper">\
									<div class="c-overlay-content">\
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>\
									</div>\
								</div>\
								<div class="c-bg-img-top-center c-overlay-object" data-height="height">\
									<img width="100%" class="img-responsive" src="'+val.image+'"> </div>\
							</div>\
						</div>\
						<div class="col-md-5 col-sm-8">\
							<ul class="c-list list-unstyled">\
								<li class="c-margin-b-25">\
									<a href="shop-product-details-2.html" class="c-font-bold c-font-22 c-theme-link">'+val.title+'</a>\
								</li>\
								<li>Size: '+val.varian+'</li>\
								<li>Quantity: x'+val.total+'</li>\
							</ul>\
						</div>\
						<div class="col-md-2 col-sm-2">\
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Unit Price</p>\
							<p class="c-font-sbold c-font-uppercase c-font-18">IDR '+currencyformat(val.price/val.total)+',00</p>\
						</div>\
						<div class="col-md-2 col-sm-2">\
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Total</p>\
							<p class="c-font-sbold c-font-18">IDR '+currencyformat(val.price)+'</p>\
						</div>\
					</div>\
				</div>';
					});
					view_info = '<div class="c-border-bottom c-row-item">\
									<h3 class="c-font-regular c-font-22">Shipping: '+data.expedisi.type+' '+data.expedisi.package+'</h3>\
								</div>';
					$.each(data.bank, function(key2, val2) {
						view_bank[key2] = '<p>'+val2.char_1+'</p>';
						view_bank_account[key2] = '<p>'+val2.char_2+'<p>';
						view_bank_number[key2] = '<p>'+val2.char_3+'</p>';
					});
					view_success = '<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">\
	<div class="container">\
		<div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">\
			<div class="c-content-title-1">\
				<h3 class="c-center c-font-uppercase c-font-bold">Checkout Completed</h3>\
				<div class="c-line-center c-theme-bg"></div>\
			</div>\
			<div class="c-theme-bg">\
				<p class="c-message c-center c-font-white c-font-20 c-font-sbold">\
					<i class="fa fa-check"></i> Thank you. Your order has been received. <br>(You only have '+data.customer.valid+' to make payment to our official bank account)</p>\
				<?php if($zone == 'ilovekb'){;?>\
				<p class="c-message c-center c-font-white c-font-20 c-font-sbold">\
					Bagi yang sudah mendaftar menjadi member, mohon lakukan "konfirmasi pembayaran" pada tombol yang tersedia.<br>\
					Bagi non-member, bisa lakukan konfirmasi pembayaran dengan SMS/WA ke 0811831919 dengan format : PAID # BANK YANG DITUJU # NO. INVOICE</p>\
				<?php };?>	\
			</div>\
			<div class="row c-order-summary c-center">\
				<ul class="c-list-inline list-inline">\
					<li>\
						<h3>Invoice Number</h3>\
						<p>#INV'+data.data[0].trx_code+'</p>\
					</li>\
					<li>\
						<h3>Date Purchased</h3>\
						<p>'+data.total_trx.date+'</p>\
					</li>\
					<li>\
						<h3>Total Payable</h3>\
						<p>IDR '+currencyformat(parseFloat(data.total_trx.count_prc) + parseFloat(data.expedisi.price))+'</p>\
					</li>\
					<li>\
						<h3>Payment Method</h3>\
						<p>Direct Bank Transfer</p>\
					</li>\
				</ul>\
			</div>\
			<div class="c-bank-details c-margin-t-30 c-margin-b-30">\
				<p class="c-marglease use your Order ID as the payment reference. Your order wont be shipped until the funds have cleared in our account.</p>\
				<h3 class="c-margin-t-40 c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">OUR BANK DETAILS</h3>\
				<h3 class="c-border-bottom">\
				</h3>\
				<ul class="c-list-inline list-inline">\
					<li>\
						<h3>Account Name</h3>\
						'+view_bank_account.toString().replace(/,/g, "")+'\
					</li>\
					<li>\
						<h3>Account Number</h3>\
						'+view_bank_number.toString().replace(/,/g, "")+'\
					</li>\
					<li>\
						<h3>Bank</h3>\
						'+view_bank.toString().replace(/,/g, "")+'\
					</li>\
				</ul>\
			</div>\
			<div class="c-order-details">\
				<div class="c-border-bottom hidden-sm hidden-xs">\
					<div class="row">\
						<div class="col-md-3">\
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Product</h3>\
						</div>\
						<div class="col-md-5">\
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Description</h3>\
						</div>\
						<div class="col-md-2">\
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Unit Price</h3>\
						</div>\
						<div class="col-md-2">\
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Total</h3>\
						</div>\
					</div>\
				</div>\
				'+view_list+'\
				'+view_info+'\
				<div class="c-row-item c-row-total c-right">\
					<ul class="c-list list-unstyled">\
						<li>\
							<h3 class="c-font-regular c-font-22">Subtotal : &nbsp;\
								<span class="c-font-dark c-font-bold c-font-22">IDR '+currencyformat(data.total_trx.count_prc)+',00</span>\
							</h3>\
						</li>\
						<li>\
							<h3 class="c-font-regular c-font-22">Shipping Fee : &nbsp;\
								<span class="c-font-dark c-font-bold c-font-22">IDR '+currencyformat(data.expedisi.price)+',00</span>\
							</h3>\
						</li>\
						<li>\
							<h3 class="c-font-regular c-font-22">Grand Total : &nbsp;\
								<span class="c-font-dark c-font-bold c-font-22">IDR '+currencyformat(parseFloat(data.total_trx.count_prc) + parseFloat(data.expedisi.price))+',00</span>\
							</h3>\
						</li>\
					</ul>\
				</div>\
			</div>\
			<div class="c-customer-details row" data-auto-height="true">\
				<div class="col-md-6 col-sm-6 c-margin-t-20">\
					<div data-height="height">\
						<h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Shipping Address</h3>\
						<ul class="list-unstyled">\
							<li>Name: '+data.customer.name+'</li>\
							<li>Phone: '+data.customer.phone+'</li>\
							<li>Postcode / Zip: '+data.customer.zip_2+'</li>\
							<li>Email:\
								<a href="mailto:'+data.customer.email+'" class="c-theme-color">'+data.customer.email+'</a>\
							</li>\
							<li>Address:\
								<span class="c-theme-color">'+data.customer.address_2+'</span>\
							</li>\
						</ul>\
					</div>\
				</div>\
				<div class="col-md-6 col-sm-6 c-margin-t-20">\
					<div data-height="height">\
						<h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Billing Address</h3>\
						<ul class="list-unstyled">\
							<li>Name: '+data.customer.name+'</li>\
							<li>Phone: '+data.customer.phone+'</li>\
							<li>Postcode / Zip: '+data.customer.zip+'</li>\
							<li>Email:\
								<a href="mailto:'+data.customer.email+'" class="c-theme-color">'+data.customer.email+'</a>\
							</li>\
							<li>Address:\
								<span class="c-theme-color">'+data.customer.address+'</span>\
							</li>\
						</ul>\
					</div>\
				</div>\
			</div>\
		</div>\
	</div>\
</div>';
$('.c-layout-page').empty();
$('.c-layout-page').append(view_success);
				}else{
					$('<p>'+data.m+'</p>').appendTo('#f-checkout > #fail');
					scrolltonote('.c-layout-page');
					$('#f-checkout > #fail').show();
					$('#f-checkout > #fail').fadeTo(4000, 500).slideUp(500); 
					$('.btn').each(function(){$(this).removeAttr('disabled');});
				}
			}
		});
	}else{
		scrolltonote('#f-checkout');
		$('.btn').each(function(){$(this).removeAttr('disabled');});
	}
});
</script>