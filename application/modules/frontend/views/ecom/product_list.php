<?php if(isset($style['product_list'])){;?>

<?php if($style['product_list'] == 'category_block'){;?>
<div class="c-content-box c-overflow-hide c-bs-grid-reset-space">
	<div class="row">
		<div class="col-md-6">
			<div class="c-content-product-5">
				<div class="c-bg-img-center" style="height:800px;background-image: url(<?php echo site_url();?>assets/img/content/shop3/21.jpg)">
					<div class="c-detail c-bg-dark c-bg-opacity-2">
						<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
						<h3 class="c-title c-font-uppercase c-font-bold c-font-white c-font-90">Promo</h3>
						<p class="c-desc c-font-white c-font-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam dolore</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
									<span class="c-font-thin">Watch</span>
									<br/>Collection </h3>
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 400px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/59.jpg);"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
									<span class="c-font-thin">Sunglasses</span>
									<br/>Collection </h3>
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 400px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/26.jpg);"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="c-content-product-5">
						<div class="c-bg-img-center c-center" style="height:400px;background-image: url(<?php echo site_url();?>assets/img/content/shop3/05.jpg)">
							<div class="c-wrapper c-center-vertical">
								<h3 class="c-title c-margin-tb-30 c-font-30 c-font-uppercase c-font-bold c-font-white">
									<span class="c-line">
										<span class="c-font-thin">New</span> Collection</span>
								</h3>
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square c-margin-t-20">Shop Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php };?>
<?php if($style['product_list'] == 'grid_12'){;?>
<!-- END: CONTENT/SHOPS/SHOP-5-1 -->
<?php if(isset($datas['product'])){
	;?>
<style>
.owl-pagination{
	display:none;
}
.owl-theme .owl-controls{
	position: initial;
    top: 45px;
    width: 100%;
	margin: 0px !important;
}
.owl-theme .owl-controls .owl-buttons div{
	color: #FFF;
	display: block;
	zoom: 1;
	*display: inline;/*IE7 life-saver */
	margin: 5px;
	padding: 10px 20px;
    font-size: 22px;
	border-radius: 0;
	background: #869791;
	filter: Alpha(Opacity=50);/*IE7 fix*/
	opacity: 0.5;
	margin-top: -167px;
	z-index: 9999;
	-webkit-transform: translate3d(0,0,0);
	-webkit-user-select: text;
    -khtml-user-select: text;
    -moz-user-select: text;
    -ms-user-select: text;
    user-select: text;
}
.owl-prev{
	float:left;
}
.owl-next{
	float:right;
}
</style>
<div class="c-content-box c-size-md c-bg-white">
		<div class="c-content-title-4 c-theme">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
				<span class="" style="background-color:white">FAVORITE MENU</span>
			</h3>
		</div>
		<!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
		<div class="c-bs-grid-small-space">
			<div data-slider="owl" data-items="4">
				<div class="owl-carousel c-theme owl-reset-space">
				<?php foreach($datas['product'] as $d){
				$p_link = 'assets/'.$zone.'/homepage/'.$d->image_square;
				;?>
					<div class="item" style="padding: 7px;">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="<?php echo $d->url;?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square"><?php echo $d->tagline;?></a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?><?php echo $p_link;?>);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				<?php };?>	
				</div>
			</div>
		</div>
		<!-- END: CONTENT/SHOPS/SHOP-2-7 -->
</div>

<script>
document.addEventListener("DOMContentLoaded", function(event) {
	var ContentOwlcarousel = function () {

		var _initInstances = function () {
			$("[data-slider='owl'] .owl-carousel").each(function () {
				var parent = $(this).parent();

				var items;
				var itemsDesktop;
				var itemsDesktopSmall;
				var itemsTablet;
				var itemsTabletSmall;
				var itemsMobile;

				if (parent.data("single-item") == "true") {
					items = 1;
					itemsDesktop = 1;
					itemsDesktopSmall = 1;
					itemsTablet = 1;
					itemsTabletSmall = 1;
					itemsMobile = 1;
				} else {
					items = parent.data('items');
					itemsDesktop = [1199, parent.data('desktop-items') ? parent.data('desktop-items') : items];
					itemsDesktopSmall = [979, parent.data('desktop-small-items') ? parent.data('desktop-small-items') : 3];
					itemsTablet = [768, parent.data('tablet-items') ? parent.data('tablet-items') : 2];
					itemsMobile = [479, parent.data('mobile-items') ? parent.data('mobile-items') : 1];
				}

				$(this).owlCarousel({

					items: items,
					itemsDesktop: itemsDesktop,
					itemsDesktopSmall: itemsDesktopSmall,
					itemsTablet: itemsTablet,
					itemsTabletSmall: itemsTablet,
					itemsMobile: itemsMobile,

					navigation:true,
					navigationText: [
					  "<i class='icon-chevron-left icon-white'><</i>",
					  "<i class='icon-chevron-right icon-white'>></i>"
					  ],
					slideSpeed: parent.data('slide-speed'),
					transitionStyle: parent.data('slide-style'),
					paginationSpeed: parent.data('pagination-speed'),
					singleItem: parent.data("single-item") ? true : false,
					autoPlay: parent.data("auto-play")
				});
			});
		};

		return {

			//main function to initiate the module
			init: function () {

				_initInstances();
			}

		};
	}();
	// END: OwlCarousel
	ContentOwlcarousel.init();
});
</script>
<?php };?>
<?php };?>
<?php if($style['product_list'] == 'category_list'){;?>
<?php foreach($datas['product'] as $d){ //cadangan aja, buat list product yg ada harganya
	$non_curr = $d->publish;
	$rp = 'IDR '.number_format($non_curr,2,',','.');
	$p_link = 'assets/'.$zone.'/product/'.$d->image_square;
	if($d->discount != 0){
		$non_curr = $d->publish*(1-($d->discount / 100));
		$rp = 'IDR '.number_format($non_curr,2,',','.');
		$dc = 'IDR '.number_format($d->publish,2,',','.');
	}
	;?>
	<div class="col-md-3 col-sm-6 c-margin-b-20">
		<div class="c-content-product-2 c-bg-white c-border">
			<div class="c-content-overlay">
				<?php if($d->discount != 0){;?>
				<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
				<?php };?>
				<div class="c-overlay-wrapper">
					<div class="c-overlay-content">
						<a href="<?php echo $menu['link'].'/product/'.str_replace(' ','-',strtolower($d->category)).'/'.str_replace(' ','_',strtolower($d->title));?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
					</div>
				</div>
				<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(<?php echo site_url();?><?php echo $p_link;?>);"></div>
			</div>
			<div class="c-info">
				<p class="c-title c-font-16 c-font-slim"><?php echo $d->title;?></p>
				<p class="c-price c-font-14 c-font-slim"><?php echo $rp;?> &nbsp;
					<?php if($d->discount != 0){;?>
					<span class="c-font-14 c-font-line-through c-font-red"><?php echo $dc;?></span>
					<?php };?>
				</p>
			</div>
			<div class="btn-group btn-group-justified" role="group">
				<div class="btn-group c-border-top" role="group">
					<a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
				</div>
				<div class="btn-group c-border-left c-border-top" role="group">
					<a data-url="cart" data-url2="cart" data-param="add" data-id="<?php echo $d->object_id;?>" data-qty="1" data-price="<?php echo $non_curr;?>" data-name="<?php echo $d->title;?>" class="add_to_cart btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
				</div>
			</div>
		</div>
	</div>
<?php };?>	
<!-- BEGIN: CONTENT/SHOPS/SHOP-1-5 -->
<div class="c-content-box c-size-md">
	<div class="c-content-tab-5 c-bs-grid-reset-space c-theme">
		<!-- Nav tabs -->
		<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
			<li role="presentation" class="active">
				<a class="c-font-uppercase" href="#watches5" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
			</li>
			<li role="presentation">
				<a class="c-font-uppercase" href="#phone5" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
			</li>
			<li role="presentation">
				<a class="c-font-uppercase" href="#imac5" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
			</li>
			<li role="presentation">
				<a class="c-font-uppercase" href="#accessories5" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
			</li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="watches5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/69.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/70.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/79.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/60.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="phone5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/63.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/59.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/71.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/91.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="imac5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/73.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/74.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/91.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop2/71.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="accessories5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/63.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/67.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/77.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?>assets/img/content/shop3/74.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-1-5 -->
<?php };?>
<?php if($style['product_list'] == 'category_clean'){;?>
<!-- BEGIN: CONTENT/SHOPS/SHOP-6-2 -->
<div class="c-content-box c-size-md c-overflow-hide c-bg-white c-bs-grid-reset-space ">
	<div class="c-content-title-4">
		<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
			<span class="c-bg-white">Customers Choose</span>
		</h3>
	</div>
	<div class="row">
		<div class="col-md-5 col-sm-6">
			<div class="row">
				<div class="col-md-12">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 350px; background-image: url(<?php echo site_url();?>assets/img/content/shop4/79.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 350px; background-image: url(<?php echo site_url();?>assets/img/content/shop4/98.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7 col-sm-6">
			<div class="c-content c-content-overlay">
				<div class="c-overlay-wrapper c-overlay-padding">
					<div class="c-overlay-content">
						<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
					</div>
				</div>
				<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 700px; background-image: url(<?php echo site_url();?>assets/img/content/shop4/14.jpg);"></div>
				<div class="c-overlay-border"></div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-6-2 -->
<?php };?>
<?php if($style['product_list'] == 'popular'){;?>
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-3 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space" style="background-image: url(<?php echo site_url();?>assets/img/content/shop-backgrounds/135.jpg)">
	<div class="container">
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-center c-font-bold">Most Popular</h3>
			<div class="c-line-center c-theme-bg"></div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/21.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-top-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/16.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">New Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/65.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Trends</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/70.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Modern</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-2-3 -->
<?php };?>
<?php if($style['product_list'] == 'popular_bg'){;?>
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-4 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space" style="background-image: url(<?php echo site_url();?>assets/img/content/shop-backgrounds/145.jpg)">
	<div class="container">
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-center c-font-bold c-font-white">Most Popular</h3>
			<div class="c-line-center c-theme-bg"></div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-grey-1">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/40.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">New Trends</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-grey-1">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-top-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/32.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-grey-1">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/52.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Modern</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-grey-1">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(<?php echo site_url();?>assets/img/content/shop6/60.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-grey-1 c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php };?>
<!-- END: CONTENT/SHOPS/SHOP-2-4 -->
<?php };?>