<?php if(!$this->cart->contents()) : ?>
<div class="c-content-box c-size-lg">
	<div class="container">
		<div class="c-shop-cart-page-1 c-center">
			<i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i>
			<h2 class="c-font-thin c-center">You don't have any items yet.</h2>
			<a href="<?php echo $menu['link'];?>/product" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a>
		</div>
	</div>
</div>
<?php else : ?>
<div class="c-content-box c-size-lg">
	<div class="container" id="content-box-cart">
		<div class="c-shop-cart-page-1">
			<div class="row c-cart-table-title">
				<!--<div class="col-md-2 c-cart-image">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Image</h3>
				</div>-->
				<div class="col-md-6 c-cart-desc">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Description</h3>
				</div>
				<div class="col-md-1 c-cart-qty">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Qty</h3>
				</div>
				<div class="col-md-2 c-cart-price">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Unit Price</h3>
				</div>
				<div class="col-md-1 c-cart-total">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Subtotal</h3>
				</div>
				<div class="col-md-1 c-cart-remove"></div>
			</div>
			<?php $total = 0; foreach($this->cart->contents() as $items): ?>
			<div class="row c-cart-table-row" id="row-cart-<?php echo $items['rowid'];?>">
				<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first"><?php echo $items['name']; ?></h2>
				<div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">
					<img src="<?php echo $items['options']['img'];?>" /> </div>
				<div class="col-md-4 col-sm-9 col-xs-7 c-cart-desc">
					<h3>
						<a href="<?php echo $items['options']['url'];?>" class="c-font-bold c-theme-link c-font-22 c-font-dark"><?php echo $items['name']; ?></a>
					</h3>
					<?php if(!empty($items['options']['variant'])){;?>
						<p style=""><?php echo $items['options']['variant'];?> </p>
					<?php };?>
				</div>
				<div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">QTY</p>
					<input type="number" min="1" max="<?php echo $items['options']['max_qty'];?>" maxlength="2" id="qty-<?php echo $items['rowid'];?>" class="cart_qty_max form-control c-item-1" value="<?php echo $items['qty']; ?>" onchange="count_bill('<?php echo $items['rowid'];?>')"/>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-price">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
					<p id="<?php echo $items['rowid'];?>" class="c-cart-price c-font-bold price" data-value="<?php echo $items['price'];?>"><?php echo 'IDR '.number_format($items['price'],2,',','.'); ?></p>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-total">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Subtotal</p>
					<p class="c-cart-price c-font-bold subtotal" data-subtotal="<?php echo $items['subtotal'];?>" id="subtotal-<?php echo $items['rowid'];?>"><?php echo 'IDR '.number_format($items['subtotal'],2,',','.'); ?></p>
				</div>
				<div class="col-md-1 col-sm-12 c-cart-remove">
					<a href="#" data-url="cart" data-url2="cart" data-param="remove" data-id="<?php echo $items['rowid'];?>" class="add_to_cart c-theme-link">&#x2716;</a>
				</div>
			</div>
			<?php $total = $total + $items['subtotal']; endforeach; ?>
			
			<div class="row">
				<div class="c-cart-subtotal-row c-margin-t-20">
					<div class="col-md-4 col-sm-12 col-xs-12">	
						<div class="form-group">
							<button class="col-md-12 col-sm-12 col-xs-12 coupon c-btn-border-opacity-04 c-btn btn-no-focus btn btn-sm c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-sbold c-cart-float-l">
								<i class="fa fa-ticket"></i> Apply Promo Code
							</button>
							<input type="text" class="col-md-6 col-sm-12 col-xs-12 input-md c-square" id="coupon" placeholder="Insert Promo Code" style="display:none">
							<div id="btn-coupon" class="col-md-6 col-sm-12 col-xs-12" style="display:none;padding:0">
								<button id="cancel-coupon" class="col-md-6 col-sm-6 col-xs-6 btn c-btn btn-xs c-btn-red c-btn-square c-font-white c-font-uppercase">
									<i class="fa fa-ban"></i> CANCEL
								</button>
								<button id="cek-coupon" class="col-md-6 col-sm-6 col-xs-6 btn c-btn btn-xs c-theme-btn c-btn-square c-font-white c-font-uppercase" style="margin:0">
									<i class="fa fa-check-circle-o"></i> CEK
								</button>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="alert alert-danger" id="fail" style="display:none;padding: 2px 10px;"></div>
						<div class="alert alert-info" id="success" style="display:none;padding: 2px 10px;"></div>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Total</h3>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-bold c-font-16" id="total-bill"><?php echo 'IDR '.number_format($total,2,',','.'); ?></h3>
					</div>
				</div>
			</div>
			<div class="c-cart-buttons">
				<?php if($zone == 'shisha'){ ;?>
				
				<?php if($is_login && !$reg_id){;?>
					<a href="<?php echo base_url();?>product/member_card" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login c-cart-float-l" style ="margin-right: 10px">Buy Member Card</a>
				<?php }else if(!$is_login){;?>
					<button href="#" data-toggle="modal" data-target="#member-area-form" data-dismiss="modal" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login c-cart-float-l" style ="margin-right: 10px">Member Area</button>
				<?php };?>
				
				<?php } ;?>
				<a href="<?php echo $menu['link'];?>/product" class="btn c-btn btn-lg c-btn-red c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-l">Continue Shopping</a>
				<button data-url="<?php echo $menu['link'];?>/cart/view_detail/cart" data-redirect="<?php echo $menu['link'];?>/checkout" class="checkout btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Checkout</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>	
<?php if(isset($datas)) : ?>
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
				<span class="c-bg-white">Most Popular</span>
			</h3>
		</div>
		<div class="row">
			<div data-slider="owl" data-items="4" data-auto-play="8000">
				<div class="owl-carousel owl-theme c-theme owl-small-space">
					<?php foreach($datas as $d){
						$non_curr = $d->publish;
						$rp = 'IDR '.number_format($non_curr,2,',','.');
						$p_link = 'assets/'.$zone.'/product/'.$d->image_square;
						if($d->discount != 0){
							$non_curr = $d->publish*(1-($d->discount / 100));
							$rp = 'IDR '.number_format($non_curr,2,',','.');
							$dc = 'IDR '.number_format($d->publish,2,',','.');
						}
					;?>
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<?php if($d->discount != 0){;?>
								<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
								<?php };?>
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo site_url();?><?php echo $p_link;?>);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim"><?php echo $d->title;?></p>
								<p class="c-price c-font-16 c-font-slim"><?php echo $rp;?> &nbsp;
									<?php if($d->discount != 0){;?>
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
									<?php };?>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a data-url="cart" data-url2="cart" data-param="add" data-id="<?php echo $d->object_id;?>" data-qty="1" data-price="<?php echo $non_curr;?>" data-name="<?php echo $d->title;?>" class="add_to_cart btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
					<?php };?>
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/img/content/shop5/18.png);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim">Samsung Galaxy Note 4</p>
								<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/img/content/shop5/27.png);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim">Samsung Galaxy S4</p>
								<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/img/content/shop5/21.png);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim">Apple iPhone 5</p>
								<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
								<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/img/content/shop5/22.png);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim">HTC</p>
								<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/img/content/shop5/20.png);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim">Apple iPhone 6</p>
								<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="c-content-product-2 c-bg-white c-border">
							<div class="c-content-overlay">
								<div class="c-label c-bg-red-2 c-font-uppercase c-font-white c-padding-10 c-font-13 c-font-bold">Hot</div>
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?php echo base_url('assets');?>/img/content/shop5/24.png);"></div>
							</div>
							<div class="c-info">
								<p class="c-title c-font-18 c-font-slim">Apple iPhone 6+</p>
								<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
									<span class="c-font-16 c-font-line-through c-font-red">$600</span>
								</p>
							</div>
							<div class="btn-group btn-group-justified" role="group">
								<div class="btn-group c-border-top" role="group">
									<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
								</div>
								<div class="btn-group c-border-left c-border-top" role="group">
									<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>	
<!-- END: CONTENT/SHOPS/SHOP-2-2 -->
<!-- BEGIN: CONTENT/STEPS/STEPS-3 --
<div class="c-content-box c-size-md c-theme-bg">
	<div class="container">
		<div class="c-content-step-3 c-font-white">
			<div class="row">
				<div class="col-md-4 c-steps-3-block">
					<i class="fa fa-truck"></i>
					<div class="c-steps-3-title">
						<h2 class="c-font-white c-font-uppercase c-font-30 c-font-thin">Free shipping</h2>
						<em>Express delivery withing 3 days</em>
					</div>
					<span>&nbsp;</span>
				</div>
				<div class="col-md-4 c-steps-3-block">
					<i class="fa fa-gift"></i>
					<div class="c-steps-3-title">
						<h2 class="c-font-white c-font-uppercase c-font-30 c-font-thin">Daily Gifts</h2>
						<em>3 Gifts daily for lucky customers</em>
					</div>
					<span>&nbsp;</span>
				</div>
				<div class="col-md-4 c-steps-3-block">
					<i class="fa fa-phone"></i>
					<div class="c-steps-3-title">
						<h2 class="c-font-white c-font-uppercase c-font-30 c-font-thin">477 505 8877</h2>
						<em>24/7 customer care available</em>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/STEPS/STEPS-3 -->
