<div class="mail-box-header">
	<div class="pull-right tooltip-demo">
		<a id="<?php echo $messages->message_id;?>" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="reply" class="detail2 btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Reply</a>
		<!--<a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>-->
		<a id="<?php echo $messages->message_id;?>" data-url="<?php echo $child;?>" data-lang="2" data-status="trash" data-title="Selected message" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </a>
	</div>
	<div class="mail-tools tooltip-demo m-t-md">


		<h3>
			<span class="font-noraml">Subject: </span><?php echo $messages->subject;?>
		</h3>
		<h5>
			<span class="pull-right font-noraml"><?php if( date("Y-m-d") != date("Y-m-d", strtotime($messages->datecreated))){ echo date("M d", strtotime($messages->datecreated)).' (6 days ago)' ;}else{ echo date("H:i", strtotime($messages->datecreated))  ;} ;?></span>
			<span class="font-noraml">From: </span><?php echo $sender;?> (<?php echo $s_type;?>)<br>
			<span class="font-noraml">To: </span><?php echo $receiver;?> (<?php echo $r_type;?>)
		</h5>
	</div>
</div>
<div class="mail-box">

	<div class="mail-body">
		<?php echo $messages->message;?>
	</div>
	<div class="detail_content2">
	</div>
	<div class="mail-body text-right tooltip-demo">
		<a id="<?php echo $messages->message_id;?>" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="reply" class="detail2 btn btn-sm btn-white"><i class="fa fa-reply"></i> Reply</a>
		<a id="<?php echo $messages->message_id;?>" data-url="dashboard" data-url2="view" data-lang="2" data-param="message" data-param2="forward" class="detail2 btn btn-sm btn-white"><i class="fa fa-arrow-right"></i> Forward</a>
		<!--<button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="Print" class="btn btn-sm btn-white"><i class="fa fa-print"></i> Print</button>-->
		<button title="" data-placement="top" data-toggle="tooltip" data-original-title="Trash" class="btn btn-sm btn-white"><i class="fa fa-trash-o"></i> Remove</button>
	</div>
	<div class="clearfix"></div>
</div>