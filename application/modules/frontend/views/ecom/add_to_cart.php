<?php if(!$this->cart->contents()) : ?>
	<ul class="c-cart-menu-items">
		<li>
			<div style="text-align:center">
				You don't have any items yet.
			</div>
		</li>
	</ul>	
<?php else :?>
	<div class="c-cart-menu-title">
		<p class="c-cart-menu-float-l c-font-sbold" id="total_item"><?php echo $this->cart->total_items();?> item(s)</p>
		<p class="c-cart-menu-float-r c-theme-font c-font-sbold" id="total_price"><?php echo 'IDR '.number_format($this->cart->total(),2,',','.'); ?></p>
	</div>
	<ul class="c-cart-menu-items" id="list_item">
		<?php foreach($this->cart->contents() as $items): ?>
		<li>
			<div class="c-cart-menu-close">
				<button data-url="cart" data-url2="cart" data-param="remove" data-id="<?php echo $items['rowid'];?>" class="add_to_cart c-theme-link">�</button>
			</div>
			<img src="<?php echo site_url('');?>assets/shisha/product/<?php echo $items['options']['img'];?>" />
			<div class="c-cart-menu-content">
				<p><?php echo $items['qty']; ?> x
					<span class="c-item-price c-theme-font"><?php echo 'IDR '.number_format($items['price'],2,',','.'); ?></span>
				</p>
				<a href="shop-product-details-2.html" class="c-item-name c-font-sbold"><?php echo $items['name']; ?></a>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
	<div class="c-cart-menu-footer">
		<a href="<?php echo base_url($this->uri->segment(1)).'/cart';?>" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase"><?php echo $text['view_cart'];?></a>
		<a href="<?php echo base_url($this->uri->segment(1)).'/checkout';?>" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
	</div>
<?php endif;?>	