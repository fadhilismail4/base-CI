<style>
.do-hover .darkgreen {
	background-color: rgba(39,89,97, 0.75);
}
.desc {
	background-color: rgba(39,89,97, 1);
}
.desc{
	position: absolute;
	right: 0;
	height: 100%;
	padding: 50px;
}
.desc > h1{
	text-transform: uppercase; color: white;margin-bottom: 20px;
}
.desc > h3{
	text-transform: uppercase; color: white;
}
.closedesc{
	position: absolute;
	right: 50px;
	top: 50px;
	border: 5px solid #fff;
	border-radius: 50%;
	color:#fff;
	width: 35px;
	padding-left: 6px;
}
.desc > a{
	color:#fff;
	cursor: pointer;
	-webkit-transition-duration: 0.8s;
	-moz-transition-duration: 0.8s;
	-o-transition-duration: 0.8s;
	transition-duration: 0.8s;

	-webkit-transition-property: -webkit-transform;
	-moz-transition-property: -moz-transform;
	-o-transition-property: -o-transform;
	transition-property: transform;

	overflow:hidden;
}
.desc > a:hover{
	-webkit-transform:rotate(360deg);
	-moz-transform:rotate(360deg);
	-o-transform:rotate(360deg);
}
.desc > button{
	background: none;
	border: 2px solid #fff;
	padding: 5px 15px;
	color: #fff;
	text-transform: uppercase;
	font-weight: 600;
	margin-top: 20px;
	text-decoration: none;
}
.desc > button:hover > i{
	-webkit-transform: translate(0.6em,0);
	-moz-transform: translate(0.6em,0);
	-o-transform: translate(0.6em,0);
}
.desc > button > i{
	border: 3px solid #fff;
	border-radius: 50%;
	padding: 5px 7px;
	font-size: 14px;
	
	-webkit-transition-duration: 0.8s;
	-moz-transition-duration: 0.8s;
	-o-transition-duration: 0.8s;
	transition-duration: 0.8s;

	-webkit-transition-property: -webkit-transform;
	-moz-transition-property: -moz-transform;
	-o-transition-property: -o-transform;
	transition-property: transform;
}
.baris{
	position: relative;
}
.org{
	background-color:#fff;
	position: absolute;
	right: 0px;
	bottom: 50px;
	padding: 0 50px;
}
</style>
<section class="c-layout-revo-slider c-layout-revo-slider-2" id="banner">
	<div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
		<div class="tp-banner">
			<ul>
				<?php foreach($banner as $b){;?>
					<li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
						<img alt="" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo $b->image_square;?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						<!--BEGIN: MAIN TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-280" data-voffset="-100" data-speed="500" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<h3 class="c-font-44 c-font-bold c-font-uppercase c-font-white">
							<?php echo $b->title;?>
							</h3>
						</div>
						<!--END -->
						<!--BEGIN: SUB TITLE -->
						<div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="-345" data-voffset="30" data-speed="500" data-start="1500" data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600">
							<p class="c-font-24 c-font-white">
								 <?php echo word_limiter($b->description,6);?>
							</p>
						</div>
						<!--END -->
						<!--BEGIN: ACTION BUTTON -->
						<div class="caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="-445" data-voffset="140" data-speed="500" data-start="2000" data-easing="Back.easeOut">
							<a href="#" class="btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase">Explore</a>
						</div>
						<!--END -->
					</li>
				<?php };?>
			</ul>
		</div>
	</div>
</section>
<section id="for-youth" class="c-content-box" style="">
	<div class="c-content-title-1 c-margin-t-20">
		<h3 class="c-center c-font-uppercase c-font-bold"><?php echo $datas['divider']['section']->title;?></h3>
		<div class="c-line-center c-theme-bg"></div>
	</div>
	<?php $s=1; foreach($datas['divider']['datas'] as $key => $value){?>	
	<?php if($s == 1){?>
		<div class="c-content-box c-bg-grey-1 c-overflow-hide c-no-bottom-padding">
            <div class="cbp c-content-latest-works-fullwidth baris">
	<?php }?>
				<div id="section<?php echo $s;?>" class="cbp-item">
					<a id="<?php echo $value->object_id;?>" onClick="qview(this)" name="section<?php echo $s;?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $value->title;?>" style="cursor: pointer;">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo trim($value->image_square);?>" alt=""> 
						</div>
						<div class="cbp-caption-activeWrap">
							<div class="cbp-l-caption-alignLeft">
								<div class="cbp-l-caption-body">
									<div class="cbp-l-caption-title"><?php echo $value->title;?></div>
									<br>
									<div class="cbp-l-caption-desc"><?php echo $value->tagline;?></div>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div id="desc_section<?php echo $s;?>" class="col-md-9 desc" style="display:none; top:0">
					<h1 style="">
						<?php echo $value->title;?> 
					</h1>
					<a id="<?php echo $value->object_id;?>" onClick="closings(this)" name="section<?php echo $s;?>"><h1 class="closedesc">X</h1></a>
					<h3 style="">
						<?php echo strip_tags($value->description);?> 
					</h3>
					<div class="org col-md-12 col-xs-12">
					</div>
					<button onClick="window.open('<?php echo base_url('')?>microsite/details/<?php echo $value->object_id;?>')"><?php echo 'EXPLORE';?> <i class="fa fa-arrow-right"></i></button>
				</div>
	<?php if($s == count($datas['divider']['datas'])){?>
			</div>
		</div>
	<?php }?>
	<?php $s++;}?>
</section>	

<section id="for-organization" class="c-content-box" style="">
	<div class="c-content-title-1 c-margin-t-20">
		<h3 class="c-center c-font-uppercase c-font-bold"><?php echo $datas['parallax']['section']->title;?></h3>
		<div class="c-line-center c-theme-bg"></div>
	</div>
	<?php $s=5; foreach($datas['parallax']['datas'] as $key => $value){?>	
	<?php if($s == 5){?>
		<div class="c-content-box c-bg-grey-1 c-overflow-hide c-no-bottom-padding">
            <div class="cbp c-content-latest-works-fullwidth baris">
	<?php }?>
				<div id="section<?php echo $s;?>" class="cbp-item">
					<a id="<?php echo $value->object_id;?>" onClick="qview(this)" name="section<?php echo $s;?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $value->title;?>" style="cursor: pointer;">
						<div class="cbp-caption-defaultWrap">
							<img src="<?php echo base_url('assets');?>/<?php echo $zone;?>/homepage/<?php echo trim($value->image_square);?>" alt=""> 
						</div>
						<div class="cbp-caption-activeWrap">
							<div class="cbp-l-caption-alignLeft">
								<div class="cbp-l-caption-body">
									<div class="cbp-l-caption-title"><?php echo $value->title;?></div>
									<br>
									<div class="cbp-l-caption-desc"><?php echo $value->tagline;?></div>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div id="desc_section<?php echo $s;?>" class="col-md-9 desc" style="display:none; top:0">
					<h1 style="">
						<?php echo $value->title;?> 
					</h1>
					<a id="<?php echo $value->object_id;?>" onClick="closings(this)" name="section<?php echo $s;?>"><h1 class="closedesc">X</h1></a>
					<h3 style="">
						<?php echo strip_tags($value->description);?> 
					</h3>
					<div class="org col-md-12 col-xs-12">
					</div>
					<button onClick="window.open('<?php echo base_url('')?>microsite/details/<?php echo $value->object_id;?>')"><?php echo 'EXPLORE';?> <i class="fa fa-arrow-right"></i></button>
				</div>
	<?php if($s == count($datas['parallax']['datas'])){?>
			</div>
		</div>
	<?php }?>
	<?php $s++;}?>
</section>	

<?php 
$bg_image = base_url('assets/img/content/backgrounds/bg-38.jpg');
if(isset($style['testimonials-color'])){
	$bg = $style['testimonial-color'];//trigger dark or grey or white
	$c_bg = 'c-bg-'.$bg;
	$c_theme = 'c-theme';
	if($bg == 'dark'){
		$c_font = 'c-font-white';
	}else{
		$c_font = ' ';
	};
}else{
	$bg = '';
	$c_bg = 'c-theme-bg';
	$c_theme = '';
	$c_font = 'c-font-white';
}

;?>
<div class="c-content-box c-size-lg <?php echo $c_bg;?>">
	<div class="container">
		<!-- Begin: testimonials 2 component -->
		<div class="c-content-testimonials-1" data-slider="owl" data-single-item="true" data-auto-play="5000">
			<!-- Begin: Title 1 component -->
			<div class="c-content-title-1">
				<h3 class="c-center <?php echo $c_font;?> c-font-uppercase c-font-bold">Let's See What Youths Say</h3>
				<div class="c-line-center c-theme-bg c-theme-darken"></div>
			</div>
			<!-- End-->
			<!-- Begin: Owlcarousel -->
			<div class="owl-carousel owl-theme">
				<div class="item">
					<div class="c-testimonial">
						<p>"JANGO is an international, privately held company that specializes in the start-up, promotion and operation of multiple online marketplaces"</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">John Snow</span>, CEO, Mockingbird </h3>
					</div>
				</div>
				<div class="item">
					<div class="c-testimonial">
						<p>"After co-founding the company in 2006 the group launched JANGO, the first digital marketplace which focused on rich multimedia web content"</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">Arya Stark</span>, CFO, Valar Dohaeris </h3>
					</div>
				</div>
				<div class="item">
					<div class="c-testimonial">
						<p>"It was the smoothest implementation process I have ever been through with JANGO's process and schedule."</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">Arya Stark</span>, CFO, Valar Dohaeris </h3>
					</div>
				</div>
				<div class="item">
					<div class="c-testimonial">
						<p>"A system change is always stressful and JANGO did a great job of staying positive, helpful, and patient with us."</p>
						<h3>
							<span class="c-name <?php echo $c_theme;?>">Arya Stark</span>, CFO, Valar Dohaeris </h3>
					</div>
				</div>
			</div>
			<!-- End-->
		</div>
		<!-- End-->
	</div>
</div>
<script>
function qview(obj){
	var idsection = obj.name;
	var element = $("*[id^='section']");
	element.css('position','initial');
	element.not('#'+idsection).fadeOut();
	$('#desc_'+idsection).fadeIn();
	$(obj).parent().parent().parent().parent('.darkgreen').attr({style: "opacity: 1"});
};
function closings(obj){
	var idsect = obj.name;
	var ids = obj.id;
	var elements = $("*[id^='section']");
	$('#desc_'+idsect).fadeOut();
	elements.css('position','');
	elements.fadeIn();
	$(obj).parent().parent().children().children().children().children('.darkgreen').attr({style: " "});
};
</script>