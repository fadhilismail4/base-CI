<style>
#page-wrapper{
	background-color: rgb(255, 255, 255);
}
.frontend{
	margin: 0px -25px;
	margin-top: -20px;
	padding-left: 20px;
	padding-right: 20px;
	padding-top: 20px;
	width: 100%;
}
.frontend_active{
	position: fixed !important;
	top: 20px;
}
.frontend > i.fa{
	color: #bbb;
}
.frontend > i.fa:hover{
	color: #676a6c;
}
.frontend > i.fa.active{
	color: #676a6c;
}
ul.edit{
	list-style-type: none;
    padding: 0;
}
ul.edit > li{
	margin: 7.6px 0;
}
.dd-handle{
	width: 90%;
    float: left;
    background-color: transparent;
    border: 1px solid #1C84C6;
    border-radius: 0;
}
.dd-handle:hover{
	background-color: #fff;
	cursor: all-scroll;
}
.dd-item > button{
	background-color: transparent;
    border: 1px solid #1C84C6;
	border-right: none;
    border-radius: 0;
    padding: 15px 0;
}
.footer_list .container {
	width: 100%;
	padding-top:15px;
	padding-bottom:15px;
}
.c-socials{
    padding: inherit;
}
.c-socials > li {
    display: inline-block;
	float: left;
    padding-bottom: 10px;
}
.c-works {
	padding: 0;
    overflow: auto;
    margin: 0;
}
.c-works > li {
	list-style: none;
    display: inline-block;
    padding: 0;
    margin: 0 0 5px 0;
}
.c-works > li img{
	margin: 0;
    width: 74px;
}
.img-responsive{
	display: block;
    max-width: 100%;
    height: auto;
}
.c-blog > .c-post:first-child{
	padding-top: 0;
}
.c-blog > .c-post{
	border-bottom: 1px solid #fff;
	padding: 10px 0;
}
.c-blog > .c-post > .c-post-img {
    width: 40px;
    height: 40px;
    position: relative;
    float: left;
    margin-top: 10px;
}
.c-blog > .c-post > .c-post-img img {
    width: 100%;
    border-radius: 40px;
}
.c-blog > .c-post > .c-post-content {
    padding-left: 50px;
}
#data > .footer_list td:hover{
	background-color: #f8fafb !important;
}
.btn.btn-md{
	padding: 6px 12px;
}
.btn > i{
	margin-right: 0px;
}
.close_btn{
	padding: 2px 8px;
    margin: 0;
    border: 2px solid #DDD;
    border-radius: 50%;
    color: #9D0B0B;
    font-weight: bold;
}
.close_btn:hover{
	color:#fff;
    background-color: #9D0B0B;
	border: none;
	padding: 4px 10px;
	cursor:pointer;
}
.file-preview-frame{
	height:auto;
}
.file-preview-image{
	width:100%;
	height:auto !important;
}
.text_info{
	margin-top: 7px;
}
</style>
<div class="frontend row wrapper border-bottom white-bg page-heading" style="">
<b>beta version !</b> - This is what your visitor see
	<i class="fa fa-mobile" title="Mobile" style="font-size: 24px;position:absolute;right: 40px;"></i>
	<i class="fa fa-tablet" title="Tablet" style="font-size: 24px;position:absolute;right: 60px;"></i>
	<i class="fa fa-desktop active" title="Desktop" style="font-size: 24px;position:absolute;right: 85px;"></i>
</div>
<iframe id="iframe" src="<?php echo str_replace('/admin','',$menu[0]['link']);?>" width="100%" height="800" frameborder="0" style="border: 1px solid #e7eaec">

</iframe>

<script>
document.addEventListener("DOMContentLoaded", function(event) {
	$('#bread').parent().hide();
	$(window).scroll(function(){
		var target = $('.frontend');
		var target_2 = $('.frontend > i.fa');
		if($(window).scrollTop()>$('iframe').offset().top){
			target.hide();
			target.addClass('frontend_active');
			target_2.addClass('frontend_active');
			target.slideDown();
		}else{
			target.hide();
			target.removeClass('frontend_active');
			target_2.removeClass('frontend_active');
			target.slideDown();
		}
	});
				
	function ajax_post($type, $url, $data, functtoexec)
	{	
		$('.btn').each(function(){
			$(this).attr('disabled', 'disabled');
		});
		$.ajax({
				url : $url,
				type: "POST",
				dataType: $type,
				data: $data,
				success: function(data){
					$('.btn').each(function(){$(this).removeAttr('disabled');});
					functtoexec(data);
				}
		});
	}
	
	$('.frontend > i.fa').on("click", function(e) {
		e.preventDefault();
		$('#info_frontend').remove();
		var target = $(this);
		if(!target.hasClass('active')){
			$('.frontend > i.fa').removeClass('active');
			$(this).addClass('active');
			var view_tablet = '';
			if($('.frontend > i.fa-tablet').hasClass('active')){
				var view_tablet = '<div id="info_frontend" style="color: red;"><small>Note: iPad with Retina display still maintains a device width in the browser of 1024px in landscape, and 768px in portrait</small></div>';
				$('iframe').animate({width: 768}, 500 );
				setTimeout(function(){
					view_edit();
				}, 500);
			}else if($('.frontend > i.fa-mobile').hasClass('active')){
				var view_tablet = '<div id="info_frontend" style="color: red;"><small>Note: 767-480px (phone in landscape mode), 480-320px (phone in portrait mode)</small></div>';
				$('iframe').animate({width: 480}, 500 );
				setTimeout(function(){
					view_edit();
				}, 500);
			}else{
				$('iframe').animate({width: '100%'}, 500 );
				setTimeout(function(){
					view_edit();
				}, 500);
			}
			$('.frontend').append(view_tablet);
		}
	});
	
	$('iframe').on("load", function() {
		view_edit();
		//slow_connection();
	});
	
	jQuery.onDemandScript = function( url, options ) {

	  // Allow user to set any option except for dataType, cache, and url
	  options = $.extend( options || {}, {
		dataType: "script",
		cache: true,
		url: url
	  });


	  // Use $.ajax() since it is more flexible than $.getScript
	  // Return the jqXHR object so we can chain callbacks
	  return jQuery.ajax( options );
	};

	function view_edit(){
		var target = $('body iframe').contents();
		var header = target.find('header');
		var footer = target.find('footer');
		var body = target.find('.c-layout-page');
		if(target.find('#edit').length != 0){
			target.find('div[id*=edit]').remove();
		}
		var view_edit_header = '<div id="edit" data-type="header" style="position: absolute;background: rgba(255,255,255,0.8);padding: 10px;font-size: 14px;top:0px;"><a href="javascript:;"><i class="fa fa-edit"></i> Edit Header</a></div>';
		
		if(header.length != 0){
			header.append(view_edit_header);
		}
		var view_edit_body = '<div id="edit" data-type="body" style="position: absolute;background: rgba(255,255,255,0.8);padding: 10px;font-size: 14px;top:200px;z-index:9999"><a href="javascript:;"><i class="fa fa-edit"></i> Edit Body</a></div>';
		if(body.length != 0){
			body.append(view_edit_body);
		}
		if(footer.length != 0){
			var view_edit_footer = '<div id="edit" data-type="footer" style="position: absolute;background: rgba(255,255,255,0.8);padding: 10px;font-size: 14px;top:'+footer.offset().top+'px;"><a href="javascript:;"><i class="fa fa-edit"></i> Edit Footer</a></div>';
			footer.append(view_edit_footer);
		}
		$('body iframe').contents().find('div[id*=edit]').on('click', function(){
			var type = $(this).data(type);
			modal_edit(type);
		});
	}
	
	function refresh_iframe(){
		document.getElementById('iframe').contentDocument.location.reload(true);
	}
	
	function modal_edit(type){
		$('[id*="modal_"]').each(function(){
			$(this).remove();
		});
		var id = type.type,
			base_url = '<?php echo base_url();?>',
			part = 'edit';
		var url = "<?php echo $menu[0]['link'];?>/frontend/view_detail/"+part;
		var lang = 'aw2';//$(this).data('lang');
		var param = 'aw3';//$(this).data('param');
		var status = 'Save';//$(this).data('status');
		if(id == 'header'){
			view_data = 
				'<button id="header_menu" class="col-md-2 btn dim btn-sm btn-outline btn-success"><i class="fa fa-th-large"></i> <br>Edit Menu</button>'+
				'<button id="header_logo" class="col-md-2 btn dim btn-sm btn-outline btn-warning"><i class="fa fa-empire"></i> <br>Edit Logo</button>'+
				'<button id="header_background" class="col-md-2 btn dim btn-sm btn-outline btn-danger"><i class="fa fa-square"></i> <br>Edit Background</button>'+
				'<button id="header_font" class="col-md-2 btn dim btn-sm btn-outline btn-info"><i class="fa fa-font"></i> <br>Edit Font</button>';
		}else if(id == 'footer'){
			view_data = '';
		}else if(id == 'body'){
			view_data = 
				'<button id="body_list" class="col-md-2 btn dim btn-sm btn-outline btn-success"><i class="fa fa-list"></i> <br>Edit Content</button>'+
				'<button id="body_shell" class="col-md-2 btn dim btn-sm btn-outline btn-warning"><i class="fa fa-columns"></i> <br>Edit Frame</button>';
		}
		view_modal = 
			'<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">'+
				'<div class="modal-dialog modal-lg">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal">'+
								'<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>'+
							'</button>'+
							'<h5 class="modal-title" style="text-transform:uppercase;float: none;">'+id+'</h5>'+
						'</div>'+
						'<div class="modal-body">'+
							'<div class="alert alert-danger" id="fail" style="display:none;"></div>'+
							'<div class="alert alert-info" id="success" style="display:none;"></div>'+
							'<div id="data" class="row">'+
								view_data+
							'</div>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>'+
							'<button id="frontend" data-id="'+id+'" data-lang="'+lang+'" data-param="'+param+'" data-status="'+status.toLowerCase()+'" type="button" class="btn btn-info create">'+status+'</button>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
		$('#iframe').parent().append(view_modal.replace(/,/g, ""));
		$('#modal_'+id+'').modal('toggle');
		$('.modal-backdrop').css('display','none');
		if(id == 'header'){
			header_menu();
			header_logo();
			header_background();
			header_font();
		}else if(id == 'footer'){
			footer_list();
		}else if(id == 'body'){
			body_list();
			body_shell();
		}
		post();
	}
	
	function header_menu(){
		$('#header_menu').on('click', function (e) {
			e.preventDefault;
			$('#data').empty();
			$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			var id = 'header',
				base_url = '<?php echo base_url();?>',
				part = 'edit',
				url = "<?php echo $menu[0]['link'];?>/frontend/view_detail/"+part,
				lang = 'aw2',//$(this).data('lang');
				param = 'aw3',//$(this).data('param');
				param2 = 'aw3',//$(this).data('param');
				status = 'Save',//$(this).data('status');
				text = 'aw3';//$(this).data('title');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : id
			};
			success_fun = function(json)
			{
				/* if (json.data.length === 0) {
					console.log('empty');
				}else{ */
					$.onDemandScript(base_url+"assets/admin/js/plugins/nestable/real.jquery.nestable.js").done(function( script, textStatus ) {
						var data1 = [];
						var edit = [];
						v_open1 = '<div class="dd" id="nestable"><ol class="dd-list">';
						if(json.data){
							$.each(json.data, function(key, val) {
								var module = '';
								if(val.module){
									if(val.module != 'undefined'){
										var module = 'data-module="'+val.module+'"';
									}
								}
								var data_attr = 'data-type="'+val.type+'" data-link="'+val.link+'" data-title="'+val.title+'" '+module+'';
								if(val.children){
									var data2 = [];
									$.each(val.children, function(key2, val2) {
										var module2 = '';
										if(val2.module){
											if(val2.module != 'undefined'){
												var module2 = 'data-module="'+val2.module+'"';
											}
										}
										var data_attr2 = 'data-type="'+val2.type+'" data-link="'+val2.link+'" data-title="'+val2.title+'" '+module2+'';
										if(val2.children){
											var data3 = [];
											$.each(val2.children, function(key3, val3) {
												var module3 = '';
												if(val3.module){
													if(val3.module != 'undefined'){
														var module3 = 'data-module="'+val3.module+'"';
													}
												}
												var data_attr3 = 'data-type="'+val3.type+'" data-link="'+val3.link+'" data-title="'+val3.title+'" '+module3+'';
												if(val3.children){
													var data4 = [];
													$.each(val3.children, function(key4, val4) {
														var module4 = '';
														if(val4.module){
															if(val4.module != 'undefined'){
																var module4 = 'data-module="'+val4.module+'"';
															}
														}
														var data_attr4 = 'data-type="'+val4.type+'" data-link="'+val4.link+'" data-title="'+val4.title+'" '+module4+'';
														data4[key4] = '<li class="dd-item" '+data_attr4+'>'+
															'<div class="dd-handle" style="width: 77.9%;">'+val4.title+'</div>'+
															'<span id="" '+data_attr4+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
														'</li>';
													});
													data3[key3] = '<li class="dd-item" '+data_attr3+'>'+
														'<div class="dd-handle" style="width: 73.2%;">'+val3.title+'</div>'+
														'<span id="" '+data_attr3+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
														'<ol class="dd-list">'+
															data4+
														'</ol>'+
													'</li>';
												}else{
													data3[key3] = '<li class="dd-item" '+data_attr3+'>'+
														'<div class="dd-handle" style="width: 77.9%;">'+val3.title+'</div>'+
														'<span id="" '+data_attr3+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
													'</li>';
												}
											});
											data2[key2] = '<li class="dd-item" '+data_attr2+'>'+
												'<div class="dd-handle" style="width: 73.2%;">'+val2.title+'</div>'+
												'<span id="" '+data_attr2+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
												'<ol class="dd-list">'+
													data3+
												'</ol>'+
											'</li>';
										}else{
											data2[key2] = '<li class="dd-item" '+data_attr2+'>'+
												'<div class="dd-handle" style="width: 79.3%;">'+val2.title+'</div>'+
												'<span id="" '+data_attr2+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
											'</li>';
										}
									});
									data1[key] = '<li class="dd-item" '+data_attr+'>'+
										'<div class="dd-handle" style="width: 74.5%;">'+val.title+'</div>'+
											'<span id="" '+data_attr+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
										'<ol class="dd-list">'+
											data2+
										'</ol>'+
									'</li>';
								}else{
									data1[key] = '<li class="dd-item" '+data_attr+'>'+
										'<div class="dd-handle" style="width: 79.3%;">'+val.title+'</div>'+
											'<span id="" '+data_attr+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
									'</li>';
								}
							});				
						}else{
							data1 = '<h3>Empty Here, Please Create a New One</h3>';
						}
						
						v_close1 = '</ol></div>';	
						
						view_data1 = v_open1+data1+v_close1;
						
						view_data2 = '<p>From:</p>'+
									'<select id="" data-param="menu" class="select_list form-control m-b">'+
                                        '<option value="">choose </option>'+
										'<option value="page">Page</option>'+
                                        '<option value="module">Module</option>'+
                                        '<option value="parallax">Parallax</option>'+
                                        '<option value="custom">Custom</option>'+
                                   ' </select>';
						view = 
								'<div class="col-md-7">'+
										'<div id="nestable-menu">'+
											'<button type="button" data-action="expand-all" class="btn btn-white btn-sm">Expand All</button>'+
											'<button type="button" data-action="collapse-all" class="btn btn-white btn-sm active">Collapse All</button>'+
										'</div>'+
									view_data1+
								'</div>'+
								'<div id="view_data2" class="col-md-5" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;">'+
									'<p>Create New Menu:</p>'+
									'<div id="list_menu">'+view_data2+'</div>'+
									'<button id="" class="list_menu_create btn btn-sm btn-outline btn-success pull-right" style="">add to menu</button>'+
								'</div>'+
								'<textarea id="nestable-output" name="inputan" class="hide form-control"></textarea>';
								
						$('#data').empty();		
						$('#data').append(view.replace(/,/g, ""));
						$('button.create').attr('data-param','menu');
						script_nestable();
						edit_data_view2('header');
					});
				//}
			}
			ajax_post('json', url, data, success_fun);
		});
	}
	
	function header_logo(){
		$('#header_logo').on('click', function (e) {
			e.preventDefault;
			$('#data').empty();
			$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			base_url = '<?php echo base_url();?>';
			$('head').append('<link rel="stylesheet" href="'+base_url+'assets/admin/js/plugins/fileinput/fileinput.min.css" type="text/css" />');
			$.when(
				$.onDemandScript(base_url+"assets/admin/js/plugins/fileinput/fileinput.min.js"),
				$.onDemandScript(base_url+"assets/admin/js/ajaxfileupload.js"),
				$.Deferred(function( deferred ){
					$( deferred.resolve );
				})
			).done(function( script, textStatus ) {
				img = $('body iframe').contents().find('.c-desktop-logo').attr("src");//class logo in FE
				view_data1 = '<img src="'+img+'" width="100%">';
				view_data2 = '<input id="logo" name="logo" class="file" type="file" value="" style="">';
				edit = '';
				view_info = '<div class="col-md-12"><small style="color:red">recommended file type: .png</small></div>';
				view = '<div class="col-md-3">'+
							'<p>Before:</p>'+view_data1+
						'</div>'+
						'<div class="col-md-6">'+
							'<p>Change with:</p>'+view_data2+
						'</div>'+
						'<div class="col-md-1">'+
							'<ul class="edit">'+edit+'</ul>'+
						'</div>'+
						view_info;
						
				$('#data').empty();		
				$('#data').append(view.replace(/,/g, ""));
				$(".file").fileinput("refresh",{
					showRemove: false,
					showUpload: false,
					initialPreview: [
						"<img src="+img+" class='file-preview-image'>"
					]
				});
				$('button.create').attr('data-param','logo');
			});
		});
	}
	
	function header_background(){
		$('#header_background').on('click', function (e) {
			e.preventDefault;
			$('#data').empty();
			$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			base_url = '<?php echo base_url();?>';
			$('head').append('<link rel="stylesheet" href="'+base_url+'assets/admin/js/plugins/fileinput/fileinput.min.css" type="text/css" />');
			$('head').append('<link rel="stylesheet" href="'+base_url+'assets/admin/css/plugins/colorpicker/bootstrap-colorpicker.min.css" type="text/css" />');
			$('head').append('<link rel="stylesheet" href="'+base_url+'assets/admin/css/plugins/switchery/switchery.css" type="text/css" />');
			$.when(
				$.onDemandScript(base_url+"assets/admin/js/plugins/fileinput/fileinput.min.js"),
				$.onDemandScript(base_url+"assets/admin/js/ajaxfileupload.js"),
				$.onDemandScript(base_url+"assets/admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js"),
				$.onDemandScript(base_url+"assets/admin/js/plugins/switchery/switchery.js"),
				$.Deferred(function( deferred ){
					$( deferred.resolve );
				})
			).done(function( script, textStatus ) {
				img = $('body iframe').contents().find('header').css('background-image');//class header image in FE
				color_bg_before = $('body iframe').contents().find('header').css('background');//class header image in FE
				img = img.replace('url(','').replace(')','');
				img = img.replace(' ','/');
				if(img == 'none'){
					view_data1 = '<i>none</i>';
					cek_bg = 'display:none';
					cek_bg2 = '';
					cek_bg3 = 'No';
				}else{
					view_data1 = '<img src='+img+' width="100%">';
					cek_bg = 'display:block';
					cek_bg2 = 'checked';
					cek_bg3 = 'Yes';
				}
				view_data2 = '<input id="logo" name="logo" class="file" type="file" value="" style="">';
				view_data3 = '<input type="checkbox" class="js-switch_3" '+cek_bg2+'/>';
				edit = '';
				color_bg = color_bg_before.split(")")[0]+")";
				//view_info = '<div class="col-md-12"><small style="color:red">recommended file type: .png</small></div>';
				view = '<div class="col-md-12">'+
							'<div class="row">'+
								'<div class="col-md-3">'+
									'<p class="text_info">Background Color:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									'<input type="text" data-format="rgba" data-color="'+color_bg+'" value="'+color_bg+'" id="demo_apidemo" style="border-radius: 0;background-color:'+color_bg+';border: 1px solid #1C84C6;" class="btn small colorpicker-element">'+
								'</div>'+
								'<div class="col-md-2">'+
									'<p class="text_info">Use Image:</p>'+
								'</div>'+
								'<div class="col-md-2">'+
									view_data3+'<div class="text_info check_js" style="float:right">'+cek_bg3+'</div>'+
								'</div>'+
							'</div>'+
							'<br>'+
							'<div class="row bg_image" style="'+cek_bg+'">'+
								'<hr>'+
								'<div class="col-md-3">'+
									'<p class="text_info">Background Image:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									view_data1+
								'</div>'+
								'<div class="col-md-2">'+
									'<p class="text_info">Change with:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									view_data2+
								'</div>'+
							'</div>'+
						'</div>'+
						
						'<div class="col-md-1">'+
							'<ul class="edit">'+edit+'</ul>'+
						'</div>';
						
				$('#data').empty();		
				$('#data').append(view);
				$(".file").fileinput("refresh",{
					showRemove: false,
					showUpload: false,
				});
				$('button.create').attr('data-param','background');
				
				//aawww
				var divStyle = $('#demo_apidemo')[0].style;
				$('#demo_apidemo').colorpicker({
					color: divStyle.backgroundColor
				}).on('changeColor', function(ev) {
					divStyle.backgroundColor = $(this).val();
				});
				
				function hexToRgb(hex) {
					var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
					hex = hex.replace(shorthandRegex, function(m, r, g, b) {
						return r + r + g + g + b + b;
					});

					var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
					return result ? {
						r: parseInt(result[1], 16),
						g: parseInt(result[2], 16),
						b: parseInt(result[3], 16)
					} : null;
				}
				
				function rgbToHex(r, g, b) {
					return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
				}
				//end aawww
				
				var elem_3 = document.querySelector('.js-switch_3');
				var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });
				
				elem_3.onchange = function() {
					switchery_3.innerHTML = elem_3.checked;
					if ($('.js-switch_3').is(":checked")) {
					   $('.bg_image').slideDown();
					   $('.check_js').html('Yes');
					}
					else {
					   $('.bg_image').slideUp();
					   $('.check_js').html('No');
					}
				};
			});
		});
	}
	
	function header_font(){
		$('#header_font').on('click', function (e) {
			e.preventDefault;
			$('#data').empty();
			$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			base_url = '<?php echo base_url();?>';
			$('head').append('<link rel="stylesheet" href="'+base_url+'assets/admin/js/plugins/font/css/bootstrap-formhelpers.min.css" type="text/css" />');
			$('head').append('<link rel="stylesheet" href="'+base_url+'assets/admin/css/plugins/colorpicker/bootstrap-colorpicker.min.css" type="text/css" />');
			$('head').append('<link rel="stylesheet" href="'+base_url+'assets/admin/css/plugins/switchery/switchery.css" type="text/css" />');
			$.when(
				$.onDemandScript(base_url+"assets/admin/js/plugins/font/js/bootstrap-formhelpers.js"),
				$.onDemandScript(base_url+"assets/admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js"),
				$.onDemandScript(base_url+"assets/admin/js/plugins/switchery/switchery.js"),
				$.Deferred(function( deferred ){
					$( deferred.resolve );
				})
			).done(function( script, textStatus ) {
				img = $('body iframe').contents().find('header').css('background-image');//class header image in FE
				color_bg_before = $('body iframe').contents().find('header nav > ul > li:not(.c-active) > a').css('color');//class header image in FE
				color_bg = color_bg_before.split(")")[0]+")";
				
				color_bg_before2 = $('body iframe').contents().find('header nav > ul > li.c-active > a:not(.btn)').css('color');//class header image in FE
				color_bg2 = color_bg_before2;
				
				color_bg_before3 = $('body iframe').contents().find('header nav > ul > li.c-active > a:not(.btn)').css('background');//class header image in FE
				color_bg3 = color_bg_before3.split(")")[0]+")";
				
				img = img.replace('url(','').replace(')','');
				img = img.replace(' ','/');
				if(color_bg == color_bg2){
					view_data1 = '<input type="text" data-format="rgba" data-color="'+color_bg2+'" value="'+color_bg2+'" id="f_color_hover" style="border-radius: 0;background-color:'+color_bg2+';border: 1px solid #1C84C6;" class="btn small colorpicker-element">';
					view_data5 = '<input type="text" data-format="rgba" data-color="'+color_bg3+'" value="'+color_bg3+'" id="b_color" style="border-radius: 0;background-color:'+color_bg3+';border: 1px solid #1C84C6;" class="btn small colorpicker-element">';
					cek_bg = 'display:none';
					cek_bg2 = '';
					cek_bg3 = 'No';
				}else{
					view_data1 = '<input name="inputan" type="text" data-format="rgba" data-color="'+color_bg2+'" value="'+color_bg2+'" id="f_color_hover" style="border-radius: 0;background-color:'+color_bg2+';border: 1px solid #1C84C6;" class="btn small colorpicker-element">';
					view_data5 = '<input name="inputan" type="text" data-format="rgba" data-color="'+color_bg3+'" value="'+color_bg3+'" id="b_color" style="border-radius: 0;background-color:'+color_bg3+';border: 1px solid #1C84C6;" class="btn small colorpicker-element">';
					cek_bg = 'display:block';
					cek_bg2 = 'checked';
					cek_bg3 = 'Yes';
				}
				view_data2 = '<div id="f_family" name="inputan" class="bfh-selectbox bfh-fonts" data-font="Arial"></div>';
				view_data3 = '<input type="checkbox" class="js-switch_3" '+cek_bg2+'/>';
				view_data4 = '<select id="f_size" name="inputan" class="form-control bfh-fontsizes" data-fontsize="12"></select>';
				edit = '';
				view = '<div class="col-md-12">'+
							'<div class="row">'+
								'<div class="col-md-3">'+
									'<p class="text_info">Font Family:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									view_data2+
								'</div>'+
								'<div class="col-md-3">'+
									'<p class="text_info">Font Size:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									view_data4+
								'</div>'+
							'</div>'+
							'<br>'+
							'<div class="row">'+
								'<div class="col-md-3">'+
									'<p class="text_info">Font Color:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									'<input name="inputan" type="text" data-format="rgba" data-color="'+color_bg+'" value="'+color_bg+'" id="f_color" style="border-radius: 0;background-color:'+color_bg+';border: 1px solid #1C84C6;" class="btn small colorpicker-element">'+
								'</div>'+
								'<div class="col-md-3">'+
									'<p class="text_info">Change when Active:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									view_data3+'<div class="text_info check_js" style="float:right">'+cek_bg3+'</div>'+
								'</div>'+
							'</div>'+
							'<br>'+
							'<div class="row bg_image" style="'+cek_bg+'">'+
								'<hr>'+
								'<div class="col-md-3">'+
									'<p class="text_info">Font Color:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									view_data1+
								'</div>'+
								'<div class="col-md-3">'+
									'<p class="text_info">Background Color:</p>'+
								'</div>'+
								'<div class="col-md-3">'+
									view_data5+
								'</div>'+
							'</div>'+
						'</div>'+
						
						'<div class="col-md-1">'+
							'<ul class="edit">'+edit+'</ul>'+
						'</div>';
						
				$('#data').empty();		
				$('#data').append(view);
				$('button.create').attr('data-param','font');
				
				var divStyle = $('#f_color')[0].style;
				$('#f_color').colorpicker({
					color: divStyle.backgroundColor
				}).on('changeColor', function(ev) {
					divStyle.backgroundColor = $(this).val();
				});
				$('#f_color').val(color_bg);
				
				var divStyle2 = $('#f_color_hover')[0].style;
				$('#f_color_hover').colorpicker({
					color: divStyle2.backgroundColor
				}).on('changeColor', function(ev) {
					divStyle2.backgroundColor = $(this).val();
				});
				$('#f_color_hover').val(color_bg2);
				
				var divStyle3 = $('#b_color')[0].style;
				$('#b_color').colorpicker({
					color: divStyle3.backgroundColor
				}).on('changeColor', function(ev) {
					divStyle3.backgroundColor = $(this).val();
				});
				$('#b_color').val(color_bg3);
				
				var elem_3 = document.querySelector('.js-switch_3');
				var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });
				
				elem_3.onchange = function() {
					switchery_3.innerHTML = elem_3.checked;
					if ($('.js-switch_3').is(":checked")) {
					   $('.bg_image').slideDown();
					   $('.check_js').html('Yes');
					}
					else {
					   $('.bg_image').slideUp();
					   $('.check_js').html('No');
					}
				};
				
				$('.bfh-fonts').bfhselectbox('toggle');
				$('.bfh-fonts').bfhfonts({font: 'Arial'});
				$('.bfh-fontsizes').bfhfontsizes({fontsize: '12'});
				
			});
		});
	}
	
	function body_list(){
		$('#body_list').on('click', function (e) {
			e.preventDefault;
			$('#data').empty();
			$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			var id = 'body',
				base_url = '<?php echo base_url();?>',
				part = 'edit',
				url = "<?php echo $menu[0]['link'];?>/frontend/view_detail/"+part,
				lang = 'aw2',//$(this).data('lang');
				param2 = 'aw3',//$(this).data('param');
				status = 'Save',//$(this).data('status');
				text = 'aw3';//$(this).data('title');
				param = document.getElementById("iframe").contentWindow.location.href;
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : id,
				param : param
			};
			success_fun = function(json)
			{
				if (json.data.length === 0) {
					console.log(json);
				}else{
					$.onDemandScript(base_url+"assets/admin/js/plugins/nestable/real.jquery.nestable.js").done(function( script, textStatus ) {						
						view_data1 = '<div id="body_list_detail" class="row"></div>';
						var btn_body_list = '';
						$.each(json.data,function(k,v){
							if($.inArray(k, ['bc','category','content','type']) == -1){
								//if($.isArray(v)){
									btn_body_list += '<button id="'+k+'" data-target="json.data.'+k+'" class="body_list_detail col-md-2 btn dim btn-sm btn-outline btn-warning"><i class="fa fa-empire"></i> <br>'+k+'</button>';
								//}				
							}
						});						
						view_data2 = btn_body_list;
						view = 	view_data2+
								view_data1;
								
						$('#data').empty();		
						$('#data').append(view.replace(/,/g, ""));
						$('button.create').attr('data-param','body');
						body_list_detail();
						edit_data_view2('header');
					});
				}
			}
			ajax_post('json', url, data, success_fun);
		});
	}
	
	function body_list_detail(){
		$('.body_list_detail').on('click', function (e) {
			e.preventDefault;
			$('#body_list_detail').empty();
			$('#body_list_detail').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			var id = 'body',
				base_url = '<?php echo base_url();?>',
				part = 'edit',
				url = "<?php echo $menu[0]['link'];?>/frontend/view_detail/"+part,
				lang = 'aw2',//$(this).data('lang');
				param2 = 'aw3',//$(this).data('param');
				status = 'Save',//$(this).data('status');
				text = 'aw3';//$(this).data('title');
				param = document.getElementById("iframe").contentWindow.location.href;
				target = $(this).data('target');
				data_type = $(this).attr('id');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : id,
				param : param
			};
			success_fun = function(json)
			{
				if (json.data.length === 0) {
					console.log(json);
				}else{
					$.onDemandScript(base_url+"assets/admin/js/plugins/nestable/real.jquery.nestable.js").done(function( script, textStatus ) {
						var data1 = [];
						var list = false;
						
						if(json.data){
							var isi = json.data.datas;
							
							if(isi){
								var i = 0;
								v_open1 = '<div class="dd" id="nestable"><ol class="dd-list">';
								if('datas' in isi[0]){
									data1 = view_list_body(eval(target));
									//console.log(data1);
									if(data1 === false){
										data1 = '';
									}
									v_close1 = '</ol></div>';	
									view_data1 = '<div class="col-md-7">'+
											'<div id="nestable-menu">'+
												'<button type="button" data-action="expand-all" class="btn btn-white btn-sm">Expand All</button>'+
												'<button type="button" data-action="collapse-all" class="btn btn-white btn-sm active">Collapse All</button>'+
											'</div>'+v_open1+data1+v_close1+'</div>';
									list = true;
								}else{
									console.log(json);
								}
							}else{
								view_data1 = '';
							}									
						}else{
							view_data1 = '<h3>Empty Here, Please Create a New One</h3>';
						}
						additional = '<input id="url" name="inputan" type="hidden" value="'+param+'"><input id="data_type" name="inputan" type="hidden" value="'+data_type+'">';
						view =	view_data1+
								'<textarea id="nestable-output" name="inputan" class="hide form-control"></textarea>'+additional;
								
						$('#body_list_detail').empty();		
						$('#body_list_detail').append(view.replace(/,/g, ""));
						$('button.create').attr('data-param','body');
						if(list === true){
							script_nestable();
						}
						edit_data_view2('header');
					});
				}
			}
			ajax_post('json', url, data, success_fun);
		});
	}
	
	function body_shell(){
		$('#body_shell').on('click', function (e) {
			e.preventDefault;
			$('#data').empty();
			$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			base_url = '<?php echo base_url();?>';
			$('head').prepend('<link rel="stylesheet" href="'+base_url+'assets/frontend/plugins/socicon/socicon.css" type="text/css" />');
			$('head').prepend('<link rel="stylesheet" href="'+base_url+'assets/frontend/css/plugins.css" type="text/css" />');
			url = document.getElementById("iframe").contentWindow.location.href;
			$.when(
				$.onDemandScript(base_url+"assets/admin/js/plugins/fileinput/fileinput.min.js"),
				$.onDemandScript(base_url+"assets/admin/js/jquery-ui-1.10.4.min.js"),
				$.Deferred(function( deferred ){
					$( deferred.resolve );
				})
			).done(function( script, textStatus ) {
				var view_data1 = [];
				$('body iframe').contents().find(".c-layout-page").children().not("#edit").each(function(keys,vals){
					if($(this).length != 0){
						var c1 = $(this);
						var c1_class = 'col-md-12';//$(this).attr('class');
						var data2 = [];
						if(c1[0].nodeName != 'STYLE'){
							$.each(c1,function(key,val){
								var c2 = $(this).children();
								var c2_class = 'col-md-12';
								var c3_class = $(this).attr('class');
								var content = $(this).html();
								data2[key] = '<table class="'+c2_class+'" style="background: #fff;"><tbody><tr>'+
									'<td valign="top" onClick="" class="edit_hover '+c3_class+'" style="background: #fff;border-right: 1px solid #eee;border-left: 1px solid #eee;">'+
									'<p>'+content+'</p>'+
									'</td>'+
								'</tr></tbody></table>';	
							});
							view_data1[keys] = 
							'<div class="'+c1_class+'" style="background: #fff;border: 2px solid #eee;margin-top:15px">'+
								data2+
							'</div>';	
						}
					}
				});
				view_data2 = '<div id="view_data2" class=""><div class="col-md-12">'+
								'<button class="list_footer_create btn btn-primary btn-sm pull-right" type="button"><i class="fa fa-plus"></i> Add New</button>'+
							'</div></div>';
				view = '<div class="footer_list">'+view_data2+view_data1+'</div>';
				$('#data').empty();			
				$('#data').append(view.replace(/,/g, ""));
				$('#data > .footer_list').delegate('.edit_hover','mouseenter mouseleave',function(e) {
					if (e.type == 'mouseenter') {
						target = $(this);
						view = '<div id="btn_edit" class="btn-group">'+
								'<button class="btn_edit_footer btn btn-primary btn-outline btn-sm" type="button"><i class="fa fa-pencil"></i> Edit</button>'+
								'<button class="btn_delete_footer btn btn-danger btn-outline btn-sm" type="button"><i class="fa fa-remove"></i> Delete</button>'+
							'</div>';
						target.append(view);
						target.sortable({
							connectWith: ".edit_hover",
							cursor: "move"
						}).disableSelection();
						
					} else {
						$('#btn_edit').each(function(){
							$(this).remove();
						});
					}
				});
				edit_data_view2('body');
			});
		});
	}
	
	function footer_list(){
		$('#data').empty();
		$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
		base_url = '<?php echo base_url();?>';
		$('head').prepend('<link rel="stylesheet" href="'+base_url+'assets/frontend/plugins/socicon/socicon.css" type="text/css" />');
		$('head').prepend('<link rel="stylesheet" href="'+base_url+'assets/frontend/css/plugins.css" type="text/css" />');
		
		$.when(
			$.onDemandScript(base_url+"assets/admin/js/plugins/fileinput/fileinput.min.js"),
			$.onDemandScript(base_url+"assets/admin/js/jquery-ui-1.10.4.min.js"),
			$.Deferred(function( deferred ){
				$( deferred.resolve );
			})
		).done(function( script, textStatus ) {
			var view_data1 = [];
			$('body iframe').contents().find("footer").children().not("#edit").each(function(){
				if($(this).children().length != 0){
					var c1 = $(this).children();
					$.each(c1,function(key,val){
						var c1_class = 'col-md-12';//$(this).attr('class');
						var data2 = [];
						if($(this).children().length != 0){
							var c2 = $(this).children();
							$.each(c2,function(key2,val2){
								var c2_class = '';//$(this).attr('class');
								var data3 = [];
								if($(this).children().length != 0){
									var c3 = $(this).children();
									var i = 1;
									$.each(c3,function(key3,val3){
										var c3_class = $(this).attr('class');
										var attr_id = $(this).attr('id');
										var content = $(this).html();
										data3[key3] = '<td id="'+attr_id+'" data-class="'+c3_class+'" valign="top" onClick="" class="edit_hover '+c3_class+'" style="background: #fff;border-right: 1px solid #eee;border-left: 1px solid #eee;">'+
											'<p>'+content+'</p>'+
										'</td>';
										i++;
									});
								}
								data2[key2] = '<table class="'+c2_class+'" style="background: #fff;"><tbody><tr>'+
									data3+
								'</tr></tbody></table>';
							});
						}						
						view_data1[key] = 
							'<div class="'+c1_class+'" style="background: #fff;border: 2px solid #eee;margin-top:15px">'+
								data2+
							'</div>';
					});
				}
			});
			view_data2 = '<div id="view_data2" class=""><div class="col-md-12">'+
                            '<button class=" btn btn-primary btn-sm pull-right" type="button"><i class="fa fa-plus"></i> Add New</button>'+
                        '</div></div>';
						//.list_footer_create
			view = '<div class="footer_list">'+view_data2+view_data1+'</div>';
			$('#data').empty();			
			$('#data').append(view.replace(/,/g, ""));
			$('#data > .footer_list').delegate('.edit_hover','mouseenter mouseleave',function(e) {
				if (e.type == 'mouseenter') {
					target = $(this);
					view = '<div id="btn_edit" class="btn-group">'+
                            '<button class="btn_edit_footer btn btn-primary btn-outline btn-sm" type="button"><i class="fa fa-pencil"></i> Edit</button>'+
                            '<button class="btn_delete_footer btn btn-danger btn-outline btn-sm" type="button"><i class="fa fa-remove"></i> Delete</button>'+
                        '</div>';
					target.append(view);
					/* target.sortable({
						connectWith: ".edit_hover",
						cursor: "move"
					}).disableSelection(); */
					
				} else {
					$('#btn_edit').each(function(){
						$(this).remove();
					});
				}
			});
			$('.footer_list').append('<input id="datafooter" name="inputan" class="hide form-control"></input>');
			$('button.create').attr('data-param','footer');
			edit_data_view2('footer');
		});
		
	}
	
	function edit_data_view2(type){
		id = type;
		if(id == 'header'){
			canvas = $('#data');
			canvas.delegate(".edit_menu",'click',function(e){
				var data_type = $(this).attr('data-type');
				var data_link = $(this).attr('data-link');
				var data_title = $(this).attr('data-title');
				
				close = '<p class="close_btn pull-right">x</p>';
				view_data2 = '<p>Name: <input id="list_menu_title" type="text" class="form-control" value="'+data_title+'"></input></p>'+
							'<p>Link: <small id="list_menu_link">'+data_link+'</small></p>';
				view = '<div id="view_data3" class="col-md-5" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;">'+close+
					'<p>Edit Menu <b>'+data_title+'</b>:</p><br>'+
					view_data2+
					'<button id="" class="list_menu_update btn btn-sm btn-outline btn-primary pull-right" style="">update</button>'+
					'<button id="" class="list_menu_remove btn btn-sm btn-outline btn-danger pull-right" style="margin-right:10px">remove</button>'+
				'</div>';
				$('#view_data2').parent().append(view);
				$('.close_btn').on("click", function(e) {
					$(this).parent().remove();
				});
			});
			
			canvas.delegate(".select_list",'change',function(e){
				e.preventDefault;
				if ($(this).find(':selected').val() != '') {
					if($(this).hasClass('select2')){
						if($(this).data('param') == 'module'){
							$('[data-param*="category"]').each(function(){
								$(this).remove();
							});
						}
					}else{
						$('.select2').each(function(){
							$(this).remove();
						});
					}
					
					var obj = $(this).attr('data-param'),
						url = "<?php echo $menu[0]['link'];?>/frontend/view_detail/edit";
					var data = {
						<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
						id : 'header',
						param : $(this).data('param'),
						param2 : $(this).val(),
					};
					success_fun = function(json)
					{
						if (json.length === 0) {
							console.log('empty');
						}else{
							if(json.data.length != 0){
								view_data = [];
								$.each(json.data, function(key, val) {
									view_data[key] = '<option value="'+val.link+'">'+val.name+'</option>';
								});
								view = '<select id="" data-param="'+json.type+'" class="select2 select_list form-control m-b">'+
									'<option value="">choose '+json.type+'</option>'+
									view_data+
								'</select>';
							   $('.select_list').parent().append(view.replace(/,/g, ""));
							}
						}
					}
					ajax_post('json', url, data, success_fun);
				}else{
					if($(this).hasClass('select2')){
						
					}else{
						$('.select2').each(function(){
							$(this).remove();
						});
					}
				}
			});
			canvas.delegate(".list_menu_create",'click',function(e){
				e.preventDefault;
				$('#fail').empty();
				target = $('#list_menu').children().last().find(':selected');
				target_other = $('#list_menu').children();
				to = $('#nestable');
				type = 'multisite';
				link = target.val();
				if(link){
					if(link == 'all'){
						view = [];
						target_other.last().children().each(function(key,aw){
							if(($(this).val() != '') && ($(this).val() != 'all') && ($(this).val() != 'no')){
								val = $(this).text();
								link = $(this).val();
								view[key] = '<li class="dd-item" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'">'+
								'<div class="dd-handle" style="width: 79.3%;">'+
									val+
								'</div>'+
								'<span id="" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'" class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;">'+
									'<i class="fa fa-pencil"></i>'+
								'</span></li>';
							}
						});
					}else{
						if(link == 'no'){
							target = target_other.eq(-2).find(':selected');
							module = 'data-module="'+target.val()+'"';
						}else{
							cek_type = target_other.eq(-3).find(':selected').val();
							if(cek_type == 'module'){
								module = target_other.eq(-2).find(':selected').val();
							}else{
								module = target_other.eq(-2).find(':selected').val();
							}
							module = 'data-module="'+module+'"';
						}
						val = target.text();
						link = target.val();
						
						view = '<li class="dd-item" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'" '+module+'>'+
							'<div class="dd-handle" style="width: 79.3%;">'+
								val+
							'</div>'+
							'<span id="" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'" '+module+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;">'+
								'<i class="fa fa-pencil"></i>'+
							'</span></li>';
					}
						
					to.children().append(view);	
					script_nestable_updateOutput();
				}else{
					$('<p>please choose '+target_other.last().attr('data-param')+' first</p>').appendTo('#fail');
					$('#fail').show();
					$('#fail').fadeTo(4000, 500).slideUp(500); 
				}
			});
			
			canvas.delegate(".list_menu_remove",'click',function(e){
				e.preventDefault;
				target = $(this).parent();
				title = target.find('#list_menu_title').val();
				link = target.find('#list_menu_link').text();
				to = $('#nestable');
				to.find("li[data-link='" + link + "'] > .dd-handle").text(title);
				to = to.find("[data-link='" + link + "']");
				to.each(function(){
					$(this).remove();
				});
				$(this).parent().remove();
				script_nestable_updateOutput();
			});
			
			canvas.delegate(".list_menu_update",'click',function(e){
				e.preventDefault;
				target = $(this).parent();
				title = target.find('#list_menu_title').val();
				link = target.find('#list_menu_link').text();
				to = $('#nestable');
				to.find("li[data-link='" + link + "'] > .dd-handle").text(title);
				to = to.find("[data-link='" + link + "']");
				to.each(function(){
					$(this).data('title',title);
				});
				script_nestable_updateOutput();
			});
			
		}else if(id == 'footer'){
			canvas = $('#data > .footer_list');
			canvas.delegate(".list_footer_create ",'click',function(e){
				close = '<p class="close_btn pull-right">x</p>';
				view_data2 = '<p>Name: <input id="title" type="text" class="form-control" value=""></input></p>'+
				'<p>Text: <input id="text" type="text" class="form-control" value=""></input></p>';
				
				view = '<div id="view_data3" class="col-md-12" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;margin-top:15px">'+close+
					'<p>Create New Footer:</p><br>'+
					view_data2+
					'<button id="" class="list_menu_update btn btn-sm btn-outline btn-primary pull-right" style="">update</button>'+
				'</div>';
				$('#view_data2').append(view);
				$('.close_btn').on("click", function(e) {
					$(this).parent().remove();
				});
			});
			
			canvas.delegate(".btn_edit_footer",'click',function(e){
				target = $(this).closest('.edit_hover');
				attr_id = target.attr('id');
				class_text = target.attr('class').replace('edit_hover','');
				class_text = class_text.replace('ui-sortable','').trim();
				column = class_text;
				data_column =	'<option value="col-md-3">1/4 row</option>'+
								'<option value="col-md-4">1/3 row</option>'+
								'<option value="col-md-6">1/2 row</option>'+
								'<option value="col-md-9">3/4 row</option>'+
								'<option value="col-md-12">1 row</option>';		
				title = target.find('[id*="footer_title"]').text().trim();
				text = target.find('[id*="footer_text"]').text().trim();
				data = '';
				type = target.find('.c-content-title-1').last().attr('class');
				if(type == 'c-blog'){
					data_type = 'blog';
				}else if(type == 'c-works'){
					data_type = 'image';
				}else if(type == 'c-links'){
					data_type = 'link';
				}else if(type == 'c-socials'){
					data_type = 'socmed';
				}
				close = '<p class="close_btn pull-right">x</p>';
				view_data2 = 
				'<div class="row"><p class="col-md-6">Column: <select id="column" class="form-control m-b">'+data_column+'</select></p>'+
				'<p class="col-md-6">Name: <input id="title" type="text" class="form-control" value="'+title+'"></input></p></div>'+
				'<p>Text: <input id="text" type="text" class="form-control" value="'+text+'"></input></p>';
				view = '<div id="view_data3" class="col-md-12" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;margin-top:15px">'+close+
					'<p>Edit Footer:</p><br>'+
					view_data2+
					'<button id="" data-target="'+attr_id+'" class="list_menu_update btn btn-sm btn-outline btn-primary pull-right" style="">update</button>'+
				'</div>';
				$('#view_data2').append(view);
				$('.close_btn').on("click", function(e) {
					$(this).parent().remove();
				});
				$('#view_data3').find('#column').val(column);
			});
			
			canvas.delegate(".list_menu_update",'click',function(e){
				e.preventDefault;
				target = $(this).parent();
				column = target.find('#column').val();
				title = target.find('#title').val().trim();
				text = target.find('#text').val().trim();
				attr_id = $(this).data('target');
				
				if(attr_id != 'undefined'){
					to = $('.footer_list').find('#'+attr_id);
					to.find('[id*="footer_title"]').text(title);
					to.find('[id*="footer_text"]').text(text);
					to.removeClass('col-md-3').removeClass('col-md-4').removeClass('col-md-6').removeClass('col-md-9').removeClass('col-md-12');
					to.addClass(column);
					to.attr('data-class',column);
				}
				
				jsonArray = {};
				jsonSettings = [];
				jsonDatas = [];
				$('[id*="footer_column"]').each(function(){
					column = $(this).attr('data-class');
					title = $(this).find('[id*="footer_title"]').text().trim();
					text = $(this).find('[id*="footer_text"]').text().trim();
					data_type = $(this).find('[id*="footer_datatype"]').attr('data-type');
					
					item = {}
					item ["column"] = column;
					item ["title"] = title;
					item ["text"] = text;
					item ["data_type"] = data_type;

					jsonDatas.push(item);
				});
				jsonArray["datas"] = jsonDatas;
				jsonArray["settings"] = jsonSettings;
				jsonString = JSON.stringify(jsonArray);
				$('#datafooter').val(jsonString);
			});
		}else if(id == 'body'){
			
		}
	}
		
	function script_nestable(){
		setTimeout(function(){
			$('.dd').nestable('collapseAll');
		}, 200);
		
		var updateOutput = function (e) {
			var list = e.length ? e : $(e.target),
					output = list.data('output');
			if (window.JSON) {
				output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
			} else {
				output.val('JSON browser support required for this demo.');
			}
		};
		 // activate Nestable for list 1
		$('#nestable').nestable({
			group: 1
		}).on('change', updateOutput);

		 // output initial serialised data
		updateOutput($('#nestable').data('output', $('#nestable-output')));

		$('#nestable-menu > button').on('click', function (e) {
			var target = $(e.target),
					action = target.data('action');
			$('#nestable-menu > button').each(function(){
				$(this).removeClass('active');
			});
			target.addClass('active');
			if (action === 'expand-all') {
				 $('.dd').nestable('expandAll');
			}
			if (action === 'collapse-all') {
				$('.dd').nestable('collapseAll');
			}
		});
	}
	
	function script_nestable_updateOutput(){
		var updateOutput = function (e) {
			var list = e.length ? e : $(e.target),
					output = list.data('output');
			if (window.JSON) {
				output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
			} else {
				output.val('JSON browser support required for this demo.');
			}
		};
		 // activate Nestable for list 1
		$('#nestable').nestable({
			group: 1
		})
		setTimeout(function(){
			updateOutput;
		}, 200);

		 // output initial serialised data
		updateOutput($('#nestable').data('output', $('#nestable-output')));
	}
	
	function create_variable_to_string(json){
		var datas = '';
		$.each(json,function(k,v){
			datas += 'data-'+k+'="'+v.replace(/"/g, "'")+'" ';
		});
		return datas;
	}
	
	function view_list_body(json){
		var data1 = [];
		var i = 0;
		//console.log(json);
		//if($.isArray(json)){
			
			$.each(json, function(key, val) {
				var module = '';
				var cek = true;
				if(val.object_id){
					id = val.object_id;
					type = 'o';
					title = val.title;
				}else if(val.category_id){
					id = val.category_id;
					type = 'oct';
					title = val.title;
				}else if(val.gallery_id){
					id = val.gallery_id;
					type = 'ogl';
					title = val.image_square;
				}else{
					//view_list_body(val.datas);
					//cek = false;
				}
				
				if(cek == true){
					var data_id = 'data-'+i+'="'+id+'"';
					var data_attr = data_id;
					if(val.datas){
						var data2 = [];
						data1[key] = view_list_body(val.datas);
						
						/* btn_edit = '<span id="" '+data_attr+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>';
						data1[key] = '<li class="dd-item" '+data_attr+'>'+
							'<div class="dd-handle" style="width: 74.5%;">'+title+'</div>'+
								btn_edit+
							'<ol class="dd-list">'+
								data2+
							'</ol>'+
						'</li>'; */
					}else{
						
						btn_edit = '<span id="" '+data_attr+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>';
						data1[key] = '<li class="dd-item" '+data_attr+'>'+
							'<div class="dd-handle" style="width: 79.3%;">'+title+'</div>'+
								btn_edit+
						'</li>';
					}
					i++;
				}
			});	
			
			if(data1.length == 0){
				data1 = '';
			}
		//}else{
		//	data1 = false;
		//}
		return data1;
	}
	
	function post(){
		function scrolltonote_2(){
			 $('.modal').animate({
				scrollTop: $('.modal-content').scrollTop(0)
			}, 1000);
		}
		function ajax_post_frontend(obj,data,success_fun_2){
			if($(".file").val()){
				$.ajaxFileUpload({ 
					url : "<?php echo $menu[0]['link'];?>/"+obj+"/create",
					secureuri: false,
					fileElementId: file,
					type: "POST",
					dataType: 'JSON',
					data: data,
					success: function(data){
						success_fun_2(data);
					}
				});
			}else{
				$.ajax({
					url : "<?php echo $menu[0]['link'];?>/"+obj+"/create",
					secureuri: false,
					fileElementId: file,
					type: "POST",
					dataType: 'json',
					data: data,
					success: function(data){
						success_fun_2(data);
					}
				});
			}
		}
		
		$('.create').on("click", function(e) {
			e.preventDefault();
			$('#success').empty();
			$('#fail').empty();
			$('.btn').each(function(){
				$(this).attr('disabled', 'disabled');
			});
			var obj = $(this).attr('id');
				file = $(".file").attr('id');
				param = $(this).data('param');
			function get_val(){
				var item_obj = {};
					$('[name="inputan"]').each(function(){
						item_obj[this.id] = this.value;
					});
				var item_array = {};
					$('[name="inputan_array"]').each(function(){
						item_array[this.id] = $(this).chosen().val();
					});
				if($(".file").val()){
					var item_summer = {};
					$('[name="inputan_summer"]').each(function(){
						item_summer[this.id] = $(this).code().replace(/"/g, "&quot;");
					});
				}else{
					var item_summer = {};
					$('[name="inputan_summer"]').each(function(){
						item_summer[this.id] = $(this).code();
					});
				}
				$.extend($.extend($.extend($.extend(item_obj, item_array), item_summer),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{param : param});
				return item_obj;
			}
			var data = get_val();
			
			if($(".file").val()){
				success_fun_2 = function(data){
					JSON.parse(JSON.stringify(data), function(k, v) {
						var cek = JSON.parse(v.replace(/(<([^>]+)>)/ig,""));
						if (cek.status == "success"){
							$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#success');
							scrolltonote_2('.modal-body');
							$('#success').show();
							$('#success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							setTimeout(refresh_iframe(), 2500);
						}else{
							$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#fail');
							scrolltonote_2('.modal-body');
							$('#fail').show();
							$('#fail').fadeTo(4000, 500).slideUp(500); 
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						} 
					});    
				}
			}else{
				success_fun_2 = function(data){
					if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#success');
							scrolltonote_2('.modal-body');
							$('#success').show();
							$('#success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							setTimeout(refresh_iframe(), 2500);
						}else{
							$('<p>'+data.m+'</p>').appendTo('#fail');
							scrolltonote_2('.modal-body');
							$('#fail').show();
							$('#fail').fadeTo(4000, 500).slideUp(500); 
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					}
			}
			ajax_post_frontend(obj,data,success_fun_2);
		});
	}
	
	function slow_connection(){
		var target = $('body iframe').contents();
			target.find('.tp-bgimg').each(function(){
				$(this).attr('src', 'http://localhost/project/assets/img/layout/sliders/revo-slider/base/blank.png');
				$(this).attr('data-src', 'http://localhost/project/assets/img/layout/sliders/revo-slider/base/blank.png');
				$(this).removeAttr( 'style' );
			});
	}
});
</script>