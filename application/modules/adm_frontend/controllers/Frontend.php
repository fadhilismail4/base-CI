<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Frontend extends Admin {
		
	public function index($zone)
	{	
		if ($this->s_login)
    	{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['is_mobile']	= $this->is_mobile();
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['notif']		= $this->notif;
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			$data['child'] = '';
			
			//End set parameter view
			$data['custom_module'] = $custom_module;
			//$data['content'] = 'dashboard.php';
			
			$data['menu_active'] = 'frontend';	
			$data['css'] = array();
			$data['js'] = array();
			$data['parent'] = 'frontend';
			$data['ch'] = array(
				'title'=>'Frontend', 
				'small'=>"Here's a brief information about your website."
			);
			$data['breadcrumb'] = array(
				array(
					'url' => $this->zone.'/admin/frontend', 
					'bcrumb' => 'Frontend', 
					'active' => ''
				)
			);
			//$header = json_decode($this->dbfun_model->get_data_bys('shell','ccs_id = '.$this->zone_id.' and type like "header"','frt')->shell,true);
			$data['content'] = 'frontend.php';
			
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$part){
		if ($this->s_login && !empty($part))
		{
			$data = array();
			$id = $this->input->post('id');
			$param = $this->input->post('param');
			$param2 = $this->input->post('param2');
			$view = '';
			if($part == 'edit'){
				if($id == 'header'){
					$menu = array();
					$datas = array();
					if(!empty($param)){
						if(in_array($param2,array('custom','parallax','module','page'),true)){
							$param = $param2;
							$param2 = '';
						}elseif(in_array($param,array('custom','parallax','module','page'),true)){
							$param = $param;
							$param2 = $param2;
						}
						if($param == 'custom'){
							$part = $param;
						}elseif($param == 'parallax'){
							$part = $param;
						}elseif(in_array($param,array('module','page'),true)){
							if($param == 'module'){
								if(empty($param2)){
									$menu = $this->dbfun_model->get_all_data_bys('name,link','ccs_id = '.$this->zone_id.' and status = 1 and ftype = 1','mdl');
								}else{
									$ccs_key = $this->module_to_ccs_key($param2);
									$m = $this->dbfun_model->get_all_data_bys('title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and status = 1','oct');
									$menu[] = array(
												'name'=>'use module name',
												'link'=>'no'
											);
									$menu[] = array(
												'name'=>'all category',
												'link'=>'all'
											);
									foreach($m as $m){
										$link = strtolower(str_replace(' ','_',trim($m->title)));
										$menu[] = (object) array('name'=>$m->title,'link'=>$link);
									}
									$param = 'category';
								}
							}elseif($param == 'page'){
								if(empty($param2)){
									$ccs_key = $this->module_to_ccs_key('sitemap');
									$category_id = $this->category_to_category_id('page');
									$m = $this->dbfun_model->get_all_data_bys('title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$category_id.' and status = 1','odc');
									foreach($m as $m){
										$link = strtolower(str_replace(' ','_',trim($m->title)));
										$menu[] = (object) array('name'=>$m->title,'link'=>$link);
									}
								}
							}
							$part = $param;
							$datas = $menu;
						}else{
							
						}
					}else{
						$menu = $this->dbfun_model->get_data_bys('shell','ccs_id = '.$this->zone_id.' and type like "header"','frt');
						if(!empty($menu)){
							$datas = json_decode($menu->shell,true);
						}else{
							$module = 'sitemap';
							$sitemap_module_id = $this->dbfun_model->get_data_bys('module_id','ccs_mdl.status = 1 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.link like "'.$module.'"','mdl')->module_id;
							if(!empty($this->module_to_category_id('sitemap','menu'))){
								$menu_parent = $this->dbfun_model->get_all_data_bys('distinct(category_id), title, metakeyword, metadescription','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$sitemap_module_id.' and ccs_oct.parent_id = '.$this->module_to_category_id('sitemap','menu').' and LOWER(ccs_oct.parent_title) like "menu" order by ccs_oct.category_id ASC','oct');
								foreach($menu_parent as $mp){
									$menu_child = array();
									$child = array();
									$cek_module = $this->module_to_ccs_key(strtolower($mp->title));
									$menu_child = $this->dbfun_model->get_all_data_bys('url_id, orders,menu_type as type, type_id, category_id,object_id, title, link, status','ccs_id = '.$this->zone_id.' and ccs_key = '.$sitemap_module_id.' and category_id = '.$mp->category_id.' and status = 1 order by orders ASC,title','mnu');
									foreach($menu_child as $mc){
										if(!empty($mp->title) && !empty($mc->title)){
											if(strpos(strtolower($mp->title),strtolower($mc->title)) === FALSE){
												if(!empty($cek_module)){
													if($mc->type_id == 'category'){
														$ceks = $this->dbfun_model->get_data_bys('ccs_oct.parent_id, ccs_oct.category_id','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$mc->url_id.' and status = 1 and ccs_oct.category_id = '.$mc->object_id.' ','oct');
														if(isset($ceks->parent_id)){
															if($ceks->parent_id == 0){
																$grand_child = $this->dbfun_model->get_all_data_bys('ccs_mnu.orders,ccs_mnu.url_id, ccs_mnu.menu_type as type, ccs_mnu.type_id, ccs_mnu.category_id,ccs_mnu.object_id, ccs_mnu.title, ccs_mnu.link, ccs_mnu.status, ccs_oct.parent_id','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$mc->url_id.' and ccs_oct.status = 1 and ccs_mnu.url_id = ccs_oct.ccs_key and ccs_mnu.object_id = ccs_oct.category_id and ccs_oct.parent_id != 0 and ccs_oct.parent_id = '.$mc->object_id.' ','mnu, oct');
																$cld = (object) array_merge((array) $mc, array('module' => strtolower($mp->title)));
																if(!empty($grand_child)){
																	$child[] = (object) array_merge((array) $cld, array('children' => $grand_child));
																}else{
																	$child[] = (object) array_merge((array) $mc, array('module' => strtolower($mp->title)));
																}
															}				
														}
													}else{
														$child[] = (object) array_merge((array) $mc, array('module' => strtolower($mp->title)));
													}
												}else{
													$child[] = $mc;
												}
											}else{
												$mp = $mc;
											}
										}			
									}
									if(!empty($child)){
										$mp = (object) array_merge((array)$mp, array('link'=>str_replace(' ','_',strtolower($mp->title)),'type'=>'multisite'));
										$menu[] = (object) array_merge((array)$mp,array('children'=> $child));
									}else{
										$menu[] = (object) array_merge((array)$mp, array('link'=>str_replace(' ','_',strtolower($mp->title)),'type'=>'multisite'));
									}
								}
							}
							$datas = $menu;
						}
					}
					
					
				}elseif($id == 'footer'){
					$datas = 'test';
				}elseif($id == 'body'){
					$this->current_language = $this->session->userdata($this->zone.'_language');
					$param = str_replace($this->menu['link'].'/','', $param);
					$cek_base = str_replace($this->menu['link'],'', $param);
					$cek = explode('/', $param);
					$type = count($cek);
					$data = array();
					if(empty($cek_base)){
						$module = 'homepage';
						$data['bc'] = array(
											'module' => $module
										);
						$ccs_key = $this->module_to_ccs_key($module);
						$homepage_category_id = $this->dbfun_model->get_all_data_bys('category_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
						//$var2 = array_keys($this->style['homepage']);
						//$i = 0; 
						$datas2 = array();
						foreach($homepage_category_id as $key => $hp){
							$datas = array();
							$datas = $this->get_all_object($module,$hp->category_id,$this->current_language); 
							$data_frt = $this->glue($datas,$cek_base);
							if($data_frt){
								$datas = $data_frt;
							}
							/* if(!empty($var2[$i])){
								$datas2[$var2[$i]] = array('section'=>$hp,'datas'=>$datas);
							}else{
								$datas2['undefined'] = array('section'=>$hp,'datas'=>$datas);
							}
							$i++; */
							$datas2[] = array('section'=>$hp,'datas'=>$datas);
						}
					
						if(in_array($this->zone,array('shisha'),true)){
							$cek_category = $this->category_to_category_id(str_replace('-',' ','shoes'));
							$product = $this->get_all_object_with_order_by_limit('product', $cek_category,$this->current_language);
							$datas4 = array();
							if(!empty($product)){
								foreach($product as $ds){
									$ds_prc = (object) array_merge((array) $ds, (array) $this->get_prc('product',$cek_category,$ds->object_id));
									$m = $this->dbfun_model->get_data_bys('name','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.module_id = '.$ds->ccs_key.'','mdl');
									$datas4[] = (object) array_merge((array) $ds_prc, array('module'=> $m->name,'category' => $this->category_id_to_category($ds->ccs_key,$ds->category_id)));
								}
							}
							
							$datas2['product'] = $datas4;
						}
						$data['datas'] = $datas2;
						$category_banner = $this->module_to_category_id($module,'banner');
						$data['banner'] = $this->get_all_object_order_by($module,$category_banner,$this->current_language,'order by ccs_odc.title ASC'); 
						$data_frt_banner = $this->glue_homepage($data['banner'],'banner',$cek_base);
						if($data_frt_banner){
							$data['banner'] = $data_frt_banner;
						}
						$category_divider = $this->module_to_category_id($module,'divider');
						$data['divider'] = $this->get_all_object($module,$category_divider,$this->current_language); 
						$data_frt_divider = $this->glue_homepage($data['divider'],'divider',$cek_base);
						if($data_frt_divider){
							$data['divider'] = $data_frt_divider;
						}
						if($this->zone == 'moonshine'){
							$data['content'][] = 'moonshine';
						}elseif($this->zone == 'contribyouth'){
							$data['content'][] = 'contribyouth';
							//print_r($data);
						}elseif($this->zone == 'ilovekb'){
							$data['content'][] = 'ilovekb';
							//print_r($data);
						}else{
							if(!empty($data['banner'])){
								$data['content'][] = 'compro/big-slider';
							}	
							$data['content'][] = 'ecom/product_list';
							$data['content'][] = 'compro/divider';
							//$data['content'][] = 'compro/parallax-arrow';
							$data['content'][] = 'compro/tiles';
							$data['content'][] = 'compro/testimonials';
							//$data['content'][] = 'compro/porto';
						}
					}elseif($type == 1){
						$module = $cek[0];
						$ccs_key = $this->module_to_ccs_key($module);
						if($ccs_key){
							$data['type'] = 'module';
							$data['bc'] = array(
											'module' => $module
										);
							$category_category_id = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' order by category_id ASC','oct');
							$datas2 = array();
							foreach($category_category_id as $key => $hp){
								$datas = array();
								$datas = $this->get_all_object($module,$hp->category_id,$this->current_language); 
								$datas2[] = array('section'=>$hp,'datas'=>$datas);
							}
							$data['datas'] = $datas2;
							if($this->zone == 'moonshine'){
								
							}else{
								$data['content'][] = 'compro/breadcrumb';
							}
							if(!empty($datas2)){
								switch($module){
									case 'news':
										$data['category'] = $this->module_to_all_category($module);
										$data['content'][] = 'compro/blog-list';
										break;
									case 'event':
										$data['years'] = $this->dbfun_model->get_all_data_bys('DISTINCT(YEAR(dateupdated)) as year','ccs_key = '.$ccs_key.' and status != 0 order by dateupdated ASC','o');
										//$data['custom_js'] = '<script src="'.base_url().'assets/frontend/js/scripts/pages/masonry-gallery.js" type="text/javascript"></script>';
										$data['content'][] = 'compro/gallery';
										break;	
									case 'product':	
										if($this->zone == 'moonshine'){
											$c_parent = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,image_square','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = 0 order by category_id ASC','oct');
											foreach($c_parent as $c_p){
												$cc = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title,image_square','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$c_p->category_id.' order by category_id ASC','oct');
												$c_child[] = (object) array_merge((array)$c_p, array('child'=>$cc));
											}
											$data['category'] = $c_child;
											$data['gallery'] = $this->dbfun_model->get_all_data_bys('category_id,image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = 0 and status = 1','ogl');
											if(!empty($data['datas'])){
												$data['content'][] = 'ecom/product_list_page_category_image';
											}else{
												$data['content'][] = '404';
											}
										}else{
											$c_parent = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = 0 order by category_id ASC','oct');
											foreach($c_parent as $c_p){
												$cc = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$c_p->category_id.' order by category_id ASC','oct');
												$c_child[] = (object) array_merge((array)$c_p, array('child'=>$cc));
											}
											$data['category'] = $c_child;
											
											if(!empty($data['datas'])){
												$data['content'][] = 'ecom/product_list_page';
											}else{
												$data['content'][] = '404';
											}
										}
										
										break;
									case 'branch':
										$data['content'][] = 'compro/blog-grid';
										break;
									case 'testimonial':
										if((!empty($datas2))){
											$data['category'] = $this->module_to_all_category($module);
											$data['content'][] = 'compro/blog-list';
										}else{
											$data['content'][] = '404';
										}
										break;	
									default:
										$data['content'][] = 'compro/blog-grid';
										break;
								}
							}else{
								$data['content'][] = '404';
							}	
						}else{
							$data['type'] = 'page';
							$cek_category = $this->module_to_category_id('sitemap','page');
							$data['datas'] = $this->get_object_by_title($module,$cek_category,$this->current_language);
							if(empty($data['datas'])){
								$data['datas'] = $this->get_object_by_title(str_replace('-',' ',$module),$cek_category,$this->current_language);
							}
							if(!empty($data['datas'])){
								$data['custom_js'] = '<script src="'.base_url().'assets/frontend/js/scripts/pages/faq.js" type="text/javascript"></script>';
								if($this->zone == 'moonshine'){
									$data['is_banner'] = FALSE;
								}else{
									$data['content'][] = 'compro/breadcrumb';
								}
								$data['content'][] = 'compro/about';
							}else{
								$data['content'][] = '404';
							}
						}
					}elseif($type == 2){
						$module = $cek[0];
						$category = $cek[1];
						$ccs_key = $this->module_to_ccs_key($module);
						if($ccs_key){
							$data['bc'] = array(
											'module' => $module,
											'category' => $category
										);
							$cek_category = $this->module_to_category_id(str_replace('_',' ',$module),str_replace('_',' ',$category));
							if(empty($cek_category)){
								$cek_category = $this->module_to_category_id(str_replace('_',' ',$module),str_replace('-',' ',$category));
							}
							if($cek_category){
								$section[] = (object) array('title'=>$category);
								foreach($section as $key => $hp){
									$datas = array();
									$datas = $this->get_all_object($module,$cek_category,$this->current_language); 
									$datas2[] = array('section'=>$hp,'datas'=>$datas);
								}
								$data['datas'] = $datas2;
								$data['content'][] = 'compro/breadcrumb';
								switch($module){
									case 'news':
										$data['category'] = $this->module_to_all_category($module);
										$data['content'][] = 'compro/blog-list';
										break;
									case 'event':
										$data['content'][] = 'compro/gallery';
										break;
									case 'product':
										$c_parent = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = 0 order by category_id ASC','oct');
										foreach($c_parent as $c_p){
											$cc = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$c_p->category_id.' order by category_id ASC','oct');
											$c_child[] = (object) array_merge((array)$c_p, array('child'=>$cc));
										}
										$data['category'] = $c_child;
										if(!empty($datas2[0]['datas']) && (!in_array(strtolower(str_replace('_',' ',$category)), array('shisha merchandise','shisha shop')))){
											if($zone == 'moonshine'){
												$data['sidebar'] = array('price_range'=>array('min'=>'100000','max'=>'1000000','currency'=>'IDR'),'tags'=>'color');
												if(isset($data['sidebar']['tags'])){
													$data['tags'] = $this->dbfun_model->get_all_data_bys('tags','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and tags != ""','odc');
												}
											}elseif($zone == 'shisha'){
												$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id as object_id, category_id,image_square,title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1','ogl');
												$data_frt = $this->glue($data['gallery'],$param);
												if($data_frt){
													$data['gallery'] = $data_frt;
												}
											}
											$data['content'][] = 'ecom/product_list_page';
										}elseif(!empty($datas2[0]['datas'])){
											$cek_section = $this->dbfun_model->get_all_data_bys('ccs_odc.tagline','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$cek_category.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$this->current_language.' order by ccs_odc.tagline ASC','o, odc');
											$datas2 = array();
											foreach($cek_section as $key2 => $hp2){
												$datas = array();
												$section = explode(" ",$hp2->tagline);
												if($section){
													$find = '';
													$end = '';
													if(count($section) > 0){
														$end = array_pop($section);
														if(count($section) > 0){
															$find = implode(' ', $section); 
														}
													}
												}
												$datas = $this->dbfun_model->get_all_data_bys('ccs_odc.datecreated,ccs_odc.datepublish,ccs_o.object_id,ccs_o.image_square,ccs_odc.tagline,ccs_odc.title,ccs_odc.description,ccs_odc.metakeyword,ccs_odc.metadescription,ccs_odc.love,ccs_odc.viewer,ccs_o.istatus','ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_odc.status = 1 and ccs_o.ccs_id = '.$this->zone_id.' and ccs_odc.category_id = '.$cek_category.' and ccs_o.ccs_key = '.$ccs_key.' and ccs_odc.language_id = '.$this->current_language.' and ccs_odc.tagline like "'.$find.'%" order by ccs_odc.tagline ASC','o, odc');
												$datas2[] = array('section'=>$hp2,'datas'=>$datas);
											}
											$data['datas'] = $datas2;
											//print_r($datas2);
											if($zone == 'shisha'){
												$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id as object_id, category_id,image_square,title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cek_category.' and object_id = 0 and status = 1','ogl');
												$data_frt = $this->glue($data['gallery'],$param);
												if($data_frt){
													$data['gallery'] = $data_frt;
												}
											}
											$data['content'][] = 'ecom/product_page';
										}else{
											$data['content'][] = 'ecom/product_list_page';
										}
										break;
									case 'branch':
										//$cek_category = $this->module_to_category_id('sitemap','page');
										//$data['datas'] = $this->get_object_by_title($category,$cek_category,$this->current_language);
										/* $data['bc'] = array(
												'module' => $module,
											);
										$data['breadcrumb'] = (object) array(
												'image' => base_url('assets').'/'.$this->zone.'/sitemap/'.$data['datas']->image_square,
											); */
										//$data['content'][] = 'compro/breadcrumb';
										//print_r($datas2);
										if(!empty($data['datas'])){
											if(isset($datas2[0]['datas'][0]->object_id)){
												$data['loc'] = $this->dbfun_model->get_data_bys('*','object_id = '.$datas2[0]['datas'][0]->object_id.'','olc');
												$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$datas2[0]['datas'][0]->object_id);	
												$data['content'][] = 'compro/branch-list';
											}else{
												$data['content'][] = '404';
											}
										}else{
											$data['content'][] = '404';
										}
										break;	
									case 'menu':
										$cek_category = $this->category_to_category_id(str_replace('_',' ',$category));
										if($this->user_id){
											if(isset($data['datas']->object_id)){
												$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
											}
										}
										if($cek_category){
											//$section[] = (object) array('title'=>$title);
											$categories = $this->dbfun_model->get_all_data_bys('category_id,parent_id,title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0 and ccs_oct.language_id = '.$this->current_language.' and parent_id = '.$cek_category.' order by category_id ASC','oct');
											foreach($categories as $key => $hp){
												$datas = array();
												$datas = $this->get_all_object($module,$hp->category_id,$this->current_language); 
												$datas2[] = array('section'=>$hp,'datas'=>$datas);
											}
											$data['datas'] = $datas2;
										}
										if((!empty($datas2[0]['datas']))||(!empty($datas2[1]['datas']))){
											//$data['content'][] = 'compro/breadcrumb';
											//$data['content'][] = 'compro/blog-grid';
											$data['content'][] = 'compro/menu-list';
										}else{
											$data['content'][] = '404';
										}
										break;
									case 'testimonial':
										if((!empty($datas2))){
											$data['category'] = $this->module_to_all_category($module);
											$data['content'][] = 'compro/blog-list';
										}else{
											$data['content'][] = '404';
										}
										break;	
									default:
										$data['content'][] = 'compro/blog-grid';
										break;
								}
							}else{
								switch($module){
									default:
										$cek_category = $this->module_to_category_id('sitemap','page');
										$data['datas'] = $this->get_object_by_title($category,$cek_category,$this->current_language);
										$data['content'][] = 'compro/breadcrumb';
										$data['content'][] = 'compro/about';
										break;
								}
							}
						}
					}elseif($type == 3){
						$module = $cek[0];
						$category = $cek[1];
						$title = $cek[2];
						$additional = '';
						$this->user_id = '';
						$ccs_key = $this->module_to_ccs_key($module);
						$data['bc'] = array(
										'module' => $module,
										'category' => $category,
										'title' => $title,
									);		
						switch($module){
							case 'branch':
								$cek_category = $this->module_to_category_id('sitemap','page');
								$data['datas'] = $this->get_object_by_title($title,$cek_category,$this->current_language);
								/* $data['bc'] = array(
										'module' => $module,
									);
								$data['breadcrumb'] = (object) array(
										'image' => base_url('assets').'/'.$this->zone.'/branch/'.$data['datas']->image_square,
									); */
								//$data['content'][] = 'compro/breadcrumb';
								//$data['content'][] = 'compro/about';
								if(!empty($data['datas'])){
									$data['content'][] = 'compro/branch';
								}else{
									$data['content'][] = '404';
								}
								break;
							case 'menu':
								if($additional == ''){
									$cek_category = $this->category_to_category_id(str_replace('-',' ',$title));
									if($this->user_id){
										if(isset($data['datas']->object_id)){
											$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
										}
									}
									if($cek_category){
										$section[] = (object) array('title'=>$title);
										foreach($section as $key => $hp){
											$datas = array();
											$datas = $this->get_all_object($module,$cek_category,$this->current_language); 
											$data_frt = $this->glue($datas,$param);
											if($data_frt){
												$datas = $data_frt;
											}
											$datas2[] = array('section'=>$hp,'datas'=>$datas);
										}
										$data['datas'] = $datas2;
									}
									if(!empty($datas2[0]['datas'])){
										$data['category'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$this->module_to_ccs_key('menu').' and category_id = '.$cek_category.'','ccs_oct');
										$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$datas2[0]['datas'][0]->object_id);	
										$data['content'][] = 'compro/menu-detail';
									}else{
										
										$data['content'][] = '404';
									}
								}elseif($additional != ''){
									$cek_category = $this->category_to_category_id(str_replace('-',' ',$title));
									if($this->user_id){
										if(isset($data['datas']->object_id)){
											$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
										}
									}
									if($cek_category){
										$section[] = (object) array('title'=>$title);
										foreach($section as $key => $hp){
											$datas = array();
											$datas4 = $this->get_object_by_title(str_replace('-',' ',$additional),$cek_category,$this->current_language); 
											$datas3 = $this->get_all_object($module,$cek_category,$this->current_language); 
											if(!empty($datas4)){
												$datas = array_merge(array('0'=>$datas4),(array)$datas3); 
											}else{
												$datas = $datas3;
											}
											$datas2[] = array('section'=>$hp,'datas'=>$datas);
										}
										$data['datas'] = $datas2;
									}
									if(!empty($datas2[0]['datas'])){
										$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$datas2[0]['datas'][0]->object_id);
										$data['content'][] = 'compro/menu-detail';
									}else{
										
										$data['content'][] = '404';
									}
								}else{
									$cek_category = $this->category_to_category_id(str_replace('_',' ',$title));
									$data['datas'] = $this->get_object_by_title($additional,$cek_category,$this->current_language);
									if($this->user_id){
										if(isset($data['datas']->object_id)){
											$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
										}
									}
									$data['content'][] = 'compro/breadcrumb';
									$data['content'][] = 'compro/blog';
								}
								break;
							default:
								/* if($this->category_to_category_id(str_replace('_',' ',$category))){
									$cek_category = $this->category_to_category_id(str_replace('_',' ',$category));
								}else{
									$cek_category = $this->category_to_category_id(str_replace('-',' ',$category));
								} */
								$cek_category = $this->module_to_category_id(str_replace('_',' ',$module),str_replace('_',' ',$category));
								$data['datas'] = $this->get_object_by_title(str_replace('_',' ',$title),$cek_category,$this->current_language);
								$data['category'] = $this->module_to_all_category($module);
								if($this->user_id){
									if(isset($data['datas']->object_id)){
										$data['love'] = $this->dbfun_model->get_data_bys('distinct(type)','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$this->user_id.' and url_id = '.$data['datas']->object_id.' and language_id = '.$this->current_language.' ','ulg');
									}
								}
								if(isset($data['datas']->object_id)){
									$data['gallery'] = $this->get_all_object_gallery($module,$cek_category,$data['datas']->object_id);	
								}
								if($module == 'product'){
									$data['datas'] = (object) array_merge((array) $data['datas'], (array) $this->get_prc($module,$cek_category,$data['datas']->object_id));
									if($data['datas']->vstatus == 1){
										$ds_variant = $this->get_variant($module,$cek_category,$data['datas']->object_id);
										$price_max = 0;
										if(!empty($ds_variant)){
											$price_min = $ds_variant[0]->publish;
										}
										foreach($ds_variant as $dsv){
											$variant1 = (object) array_merge((array) $dsv, (array) $this->get_ocs($dsv->settings_id));
											if($this->s_login){
												$variant[] = (object) array_merge((array) $variant1, (array) $this->get_tkt($data['datas']->object_id,$variant1->varian_id));
											}else{
												$variant[] = $variant1;
											}
											
											if($dsv->publish > $price_max){//get max
												$price_max = $dsv->publish;
											}
											if($dsv->publish < $price_min){//get min
												$price_min = $dsv->publish;
											}
										}
										if(!empty($ds_variant)){
											if($price_min != $price_max){
												$price = str_replace('.',',',$price_min).' - '.str_replace('.',',',$price_max);
											}else{
												$price = str_replace('.',',',$price_min);
											}
										}
										$data['datas'] = (object) array_merge((array) $data['datas'], array('publish'=> $price,'discount'=> 0,'variant'=> $variant));
									}else{
										if($this->s_login){
											$dsc = $this->dbfun_model->get_data_bys('discount','ccs_id = '.$this->zone_id.' and oid = '.$data['datas']->object_id.' and stock != 0 and status = 1','ccs_tkt');
										}else{
											$dsc = 0;
										}
										
										if(!empty($dsc)){
											$dsc = $dsc->discount;
										}else{
											$dsc = 0;
										}
										$data['datas'] = (object) array_merge((array) $data['datas'], array('discount'=> $dsc));
									}
									$data['rltd_o_w_oct'] = '';
									//print_r($data['datas']);
									$data['content'][] = 'compro/breadcrumb';
									$data['content'][] = 'ecom/product_detail';
									$data['custom_js'] = "
									<script>
											$('.c-spinner').click(function() {
												var qty = $('input[name=".'"'."qty".'"'."]').val();
												$(".'"'."[data-id='".$data['datas']->object_id."']".'"'.").data('qty',qty);
											});
											
										</script>
										<script src='".base_url()."assets/frontend/plugins/zoom-master/jquery.zoom.min.js' type='text/javascript'></script>
									";
								}elseif($module == 'event'){
									//$data['content'][] = 'compro/breadcrumb';
									$view = $this->load->view('compro/ajax-detail', $data, TRUE);
									echo $view;
								}else{
									$data['content'][] = 'compro/breadcrumb';
									$data['content'][] = 'compro/blog';
								}
								break;
						}	
					}
					$datas = $data;
				}
			}elseif($part == 'gallery'){
				$id = $this->input->post('id');
				if($id){
					$module = $this->input->post('module');
					$category = $this->input->post('category');
					$ccs_key = $this->module_to_ccs_key($module);
					$category_id = $this->module_to_category_id($module, $category);
					if(!empty($module) && !empty($category)){
						$type = 'category';
						$q = 'and category_id = '.$category_id.' and ccs_key = '.$ccs_key.'';
					}elseif(!empty($module)){
						$type = 'module';
						$q = 'and ccs_key = '.$ccs_key.'';
					}else{
						$type = 'all';
						$q = '';
					}
				}
				if($id){
					$o = $this->dbfun_model->get_all_data_bys('ccs_key,dateupdated,object_id,image_square,istatus','ccs_id = '.$this->zone_id.' '.$q.' and image_square != ""','o');
					$ogl = $this->dbfun_model->get_all_data_bys('datecreated,object_id,image_square,istatus,gallery_id','ccs_id = '.$this->zone_id.' '.$q.' ','ogl');
					
					foreach($o as $o){
						$module = $this->dbfun_model->get_data_bys('name','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.module_id like "'.$o->ccs_key.'"','mdl');
						$url = strtolower(str_replace(' ','_',$module->name)).'/'.$o->image_square;
						if(file_exists('./assets/'.$zone.'/'.$url)){
							$data[] = (object) array('gallery_id'=>0,'datecreated'=> $o->dateupdated,'image_square'=>$url,'object_id'=>$o->object_id,'istatus'=>$o->istatus);
						}
					}
					
					$datas2 = array_merge((array) $data, (array) $ogl);
					
					$limit = $this->input->post('limit');
					if(empty($limit)){
						$limit = 8;
					}
					$offset = $this->input->post('offset');
					if(empty($offset)){
						$offset = 0;
					}
					$start = $offset*$limit;
					$sorting = SORT_ASC;
					$numpage = 0;
					$active_page = $offset;
					
					$order = 'date';//$this->input->post('order');
					
					$datas3 = array();
					foreach ($datas2 as $key => $row)
					{
						switch ($order) {
							case 'title':
								$datas3[$key] = $row->title;
								break;
							case 'date':
								$datas3[$key] = $row->datecreated;
								break;
							default:
								break;
						}
					};
					array_multisort($datas3, $sorting, $datas2);
					$total = count($datas2);
					$numpage = ceil($total/$limit);
					$datas = array_slice($datas2, $start, $limit, false);
					echo json_encode(array('type' => $type,'data' => $datas,'numpage' => $numpage, 'ap' => $active_page));
				}else{
					$datas = '';
				}
				
			}else{
				$datas = 'error';
			}
			if($part != 'gallery'){
				echo json_encode(array('type' => $part,'data' => $datas));
			}
		}else{
			redirect($this->login);
		}
	}
	
	public function create($zone){	
		if ($this->s_login)
		{
			$param = $this->input->post('param');
			$status = 'error';
			$message = '';
			if($param == 'menu'){
				$where = 'ccs_id = '.$this->zone_id.' and type like "header"';
				$datas = array('shell' => $this->input->post('nestable-output'));
				$table = 'ccs_frt';
				$cek = $this->dbfun_model->get_data_bys('shell',$where,$table);
				if(!empty($datas)){
					if(!empty($cek)){
						$this->dbfun_model->update_table($where, $datas, $table);
					}else{
						$datas['type'] = 'header';
						$datas['ccs_id'] = $this->zone_id;
						$this->dbfun_model->ins_return_id($table,$datas);
					}
					$status = 'success';
					$message = 'Data Created Successfully';
				}
			}elseif($param == 'logo'){
				$where = array('ccs_id'=> $this->zone_id);
				$table = 'ccs_zone';
				$file = 'logo';
				if(isset($_FILES[$file])){
					$updates = $this->update_data_bys('*',$where,$table,$file,$this->zone,$file,'jpeg|jpg|png');
					$this->dbfun_model->update_table($where, $updates, $table);
					$status = 'success';
					$message = 'Logo Updated Successfully';
				}
			}elseif($param == 'body'){
				$data_type = $this->input->post('data_type');
				$url = $this->input->post('url');
				$data = $this->input->post('nestable-output');
				
				$param = str_replace($this->menu['link'].'/','', $url);
				$cek_base = str_replace($this->menu['link'],'', $url);
				if(empty($cek_base)){
					$param = $cek_base;
				}
				$urls = explode('/', $param);
				$url = end($urls);
				$type = 'data';
				if(!empty($type)){
					$where = 'ccs_id = '.$this->zone_id.' and type like "'.$type.'" and url like "'.$param.'"';
					$table = 'ccs_frt';
					$cek = $this->dbfun_model->get_data_bys('front_id,shell',$where,$table);
					
					if(empty($param)){
						if(!empty($cek)){
							$ceks = json_decode($cek->shell,true);	
							$flag = true;
							foreach($ceks as $key => $val){
								if(strtolower($key) == strtolower($data_type)){
									$remix[$data_type] = json_decode($data,false);	
									$flag = false;
								}else{
									$remix[$key] = $val;
								}
							}
							if($flag === true){
								$remix[$data_type] = json_decode($data,false);	
							}
							$data = json_encode($remix,false);
						}else{
							$remix[$data_type] = json_decode($data,false);	
							$data = json_encode($remix,false);
						}
					}
					
					$datas = array(
								'shell' => $data,
								'url' => $param
							);
					
					if(!empty($datas)){
						if(!empty($cek)){
							$this->dbfun_model->update_table($where, $datas, $table);
						}else{
							$datas['type'] = $type;
							$datas['url'] = $param;
							$datas['ccs_id'] = $this->zone_id;
							$this->dbfun_model->ins_return_id($table,$datas);
						}
						$status = 'success';
						$message = 'Data Created Successfully';
					}
				}else{
					$status = 'error';
					$message = 'yeaah';
				}
			}elseif($param == 'footer'){
				$datafooter = $this->input->post('datafooter');
				$where = 'ccs_id = '.$this->zone_id.' and type like "footer"';
				$datas = array('shell' => $datafooter);
				$table = 'ccs_frt';
				$cek = $this->dbfun_model->get_data_bys('shell',$where,$table);
				if(!empty($datafooter)){
					if(!empty($cek)){
						$this->dbfun_model->update_table($where, $datas, $table);
						$message = 'Data Created Successfully';
					}else{
						$datas['type'] = 'footer';
						$datas['ccs_id'] = $this->zone_id;
						$this->dbfun_model->ins_return_id($table,$datas);
						$message = 'Data Updated Successfully';
					}
					$status = 'success';
				}else{
					$status = 'error';
					$message = 'No Data Updated';
				}
			}else{
				$status = 'error';
				$message = 'aaw';
			}
			echo json_encode(array('status' => $status,'m' => $message));
		}
	}
}