<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Report extends Admin {
		
	public function view ($zone)
	{	
		if ($this->s_login)
    	{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['is_mobile']	= $this->is_mobile();
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['notif']		= $this->notif;
			$data['child'] 		= '';
			
			//End set parameter view
			$data['content'] = 'report.php';
			
			$data['menu_active'] = 'report';
			$data['css'] = '';
			$data['js'] = '';
			$data['parent'] = 'report';
			$data['ch'] = array(
				'title'=> 'Report', 
				'small'=> "Here's a about Report."
			);

			if($this->is_active_domain === TRUE){
				$url = 'admin/report';
			}else{
				$url = $this->zone.'/admin/report/1';
			}

			$data['breadcrumb'] = array(
				array(
					'url' => $url, 
					'bcrumb' => 'Report', 
					'active' => true
				)
			);

			$data['custom_js'] = '<script>
				$(document).ready(function(){
					$("[data-param='. "'" .'category'. "'" .']")[0].click();
					$("[data-param='. "'" .'list'. "'" .']")[0].click();
				});
			</script>';	
			
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$part){
		(!$this->s_login) ? redirect($this->login) : '';

		$data = array();
		$id = $this->input->post('id');
		$view = '';

		if ($part == 'category') {
			$view = $this->load->view('report_category.php', $data, TRUE);
		}

		if ($part == 'list') {
			
			$view = $this->load->view('report_list.php', $data, TRUE);
		}

		echo $view;
	}
}