<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Modules_model extends CI_Model { 
	
		function get_promo($ccs_id, $ccs_key, $id, $url_id){
			$q="
				select  ccs_odc.title, ccs_odc.object_id, ccs_odc.category_id, ccs_tkt.discount, ccs_tkt.stock, ccs_tkt.sold, ccs_tkt.status, ccs_tkt.type_id, ccs_tkt.number, ccs_tkt.number_cat, ccs_tkt.ticket_id, ccs_tkt.datecreated, ccs_tkt.datestart, ccs_tkt.dateend, ccs_adm.name
				from ccs_odc, ccs_tkt, ccs_adm
				where ccs_tkt.ccs_key = ".$ccs_key."
				and ccs_odc.ccs_id = ".$ccs_id."
				and ccs_tkt.category_id = ".$id." 
				and ccs_tkt.url_id = ".$url_id." 
				and ccs_odc.ccs_key = ccs_tkt.url_id
				and ccs_odc.ccs_id = ccs_tkt.ccs_id
				and ccs_odc.object_id = ccs_tkt.object_id 
				and ccs_tkt.type_id = 0
				and ccs_tkt.ccs_id = ccs_adm.ccs_id
				and ccs_tkt.createdby = ccs_adm.admin_id
				UNION
				select ccs_oct.title, ccs_oct.parent_id as object_id , ccs_oct.category_id , ccs_tkt.discount, ccs_tkt.stock, ccs_tkt.sold, ccs_tkt.status, ccs_tkt.type_id, ccs_tkt.number, ccs_tkt.number_cat, ccs_tkt.ticket_id, ccs_tkt.datecreated, ccs_tkt.datestart, ccs_tkt.dateend, ccs_adm.name
				from ccs_oct, ccs_tkt, ccs_adm
				where ccs_oct.ccs_id = ".$ccs_id."
				and ccs_oct.ccs_key = ".$url_id." 
				and ccs_tkt.category_id = ".$id."
				and ccs_tkt.ccs_key = ".$ccs_key."
				and ccs_oct.category_id = ccs_tkt.object_id
				and ccs_oct.ccs_id = ccs_tkt.ccs_id
				and ccs_oct.ccs_key = ccs_tkt.url_id
				and ccs_tkt.type_id =1
				and ccs_tkt.ccs_id = ccs_adm.ccs_id
				and ccs_tkt.createdby = ccs_adm.admin_id
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_all_promo($ccs_id, $ccs_key, $url_id){
			$q="
				select  ccs_odc.title, ccs_odc.object_id, ccs_odc.category_id, ccs_tkt.discount, ccs_tkt.stock, ccs_tkt.sold, ccs_tkt.status, ccs_tkt.type_id, ccs_tkt.number, ccs_tkt.number_cat, ccs_tkt.ticket_id, ccs_tkt.datecreated, ccs_tkt.datestart, ccs_tkt.dateend, ccs_adm.name
				from ccs_odc, ccs_tkt, ccs_adm
				where ccs_tkt.ccs_key = ".$ccs_key."
				and ccs_odc.ccs_id = ".$ccs_id."
				and ccs_tkt.url_id = ".$url_id." 
				and ccs_odc.ccs_key = ccs_tkt.url_id
				and ccs_odc.ccs_id = ccs_tkt.ccs_id
				and ccs_odc.object_id = ccs_tkt.object_id 
				and ccs_tkt.type_id = 0
				and ccs_tkt.ccs_id = ccs_adm.ccs_id
				and ccs_tkt.createdby = ccs_adm.admin_id
				UNION
				select ccs_oct.title, ccs_oct.parent_id as object_id , ccs_oct.category_id , ccs_tkt.discount, ccs_tkt.stock, ccs_tkt.sold, ccs_tkt.status, ccs_tkt.type_id, ccs_tkt.number, ccs_tkt.number_cat, ccs_tkt.ticket_id, ccs_tkt.datecreated, ccs_tkt.datestart, ccs_tkt.dateend, ccs_adm.name
				from ccs_oct, ccs_tkt, ccs_adm
				where ccs_oct.ccs_id = ".$ccs_id."
				and ccs_oct.ccs_key = ".$url_id." 
				and ccs_tkt.ccs_key = ".$ccs_key."
				and ccs_oct.category_id = ccs_tkt.object_id
				and ccs_oct.ccs_id = ccs_tkt.ccs_id
				and ccs_oct.ccs_key = ccs_tkt.url_id
				and ccs_tkt.type_id =1
				and ccs_tkt.ccs_id = ccs_adm.ccs_id
				and ccs_tkt.createdby = ccs_adm.admin_id
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_category($ccs_key,$ccs_id, $id){
			$q="
				SELECT distinct(c.object_id), c.category_id, c.ccs_key, t.title, t.parent_id
				FROM ccs_odc c, ccs_oct t
				WHERE  t.category_id = ".$id."
				and c.category_id=t.category_id
				and t.ccs_key= c.ccs_key
				and t.ccs_key= ".$ccs_key."
				and t.ccs_id= c.ccs_id
				and t.ccs_id= ".$ccs_id."
				and t.language_id=2
				UNION 
				SELECT distinct(c.object_id), c.category_id, c.ccs_key, t.title, t.parent_id
				FROM ccs_odc c, ccs_oct t
				WHERE  t.parent_id = ".$id."
				and c.category_id=t.category_id
				and t.ccs_key= c.ccs_key
				and t.ccs_key= ".$ccs_key."
				and t.ccs_id= c.ccs_id
				and t.ccs_id= ".$ccs_id."
				and t.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_list($ccs_id,$type){
			$q="			
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.category_id=d.category_id								
				and c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and a.admin_id = d.createdby
				and c.ccs_id = m.ccs_id
				and c.ccs_id = d.ccs_id
				and c.ccs_id = a.ccs_id
				and c.ccs_id = ".$ccs_id."
				and c.ccs_key = ".$type."
				and c.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_listcat($ccs_key, $id){
			$q="
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id=2
				and c.category_id = ".$id."
				UNION 				
				SELECT distinct(m.object_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.object_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name
				FROM ccs_oct c, ccs_o m, ccs_odc d, ccs_adm a
				WHERE  c.category_id=d.category_id								
				and c.ccs_key = m.ccs_key 
				and m.object_id = d.object_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.ccs_key = ".$ccs_key."
				and c.language_id=2
				and c.parent_id = ".$id."
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
}
	