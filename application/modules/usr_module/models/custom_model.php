<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Custom_model extends CI_Model { 
	
		function get_location($ccs_id, $ccs_key, $id, $url_id){
			$q="
				select ccs_odc.title, ccs_odc.object_id, ccs_obm.category_id, ccs_obm.type, ccs_odc.ccs_key, ccs_obm.datecreated, ccs_odc.status, ccs_adm.name
				from ccs_odc, ccs_obm, ccs_adm
				where ccs_odc.ccs_id = ccs_obm.ccs_id
				and ccs_odc.ccs_id = ccs_adm.ccs_id
				and ccs_odc.ccs_id = ".$ccs_id."
				and ccs_odc.ccs_key = ccs_obm.url_id
				and ccs_obm.url_id = ".$url_id."
				and ccs_odc.object_id = ccs_obm.object_id 
				and ccs_odc.language_id = 2
				and ccs_obm.type = 'sub-area'
				and ccs_obm.category_id = ".$id."
				and ccs_obm.ccs_key = ".$ccs_key."
				and ccs_adm.admin_id = ccs_odc.createdby
				UNION
				select ccs_oct.title, ccs_oct.category_id as object_id , ccs_obm.category_id, ccs_obm.type,  ccs_oct.ccs_key, ccs_obm.datecreated, ccs_oct.status , ccs_adm.name
				from ccs_oct, ccs_obm, ccs_adm
				where ccs_oct.ccs_id = ccs_obm.ccs_id
				and ccs_oct.ccs_id = ccs_adm.ccs_id
				and ccs_oct.ccs_id = ".$ccs_id."
				and ccs_oct.ccs_key = ccs_obm.url_id
				and ccs_obm.url_id = ".$url_id."
				and ccs_oct.category_id = ccs_obm.object_id 
				and ccs_oct.language_id = 2
				and ccs_obm.type = 'area'
				and ccs_obm.category_id = ".$id."
				and ccs_obm.ccs_key = ".$ccs_key."
				and ccs_adm.admin_id = ccs_oct.createdby
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		
		function get_all_loc($ccs_id, $ccs_key, $url_id){
			$q="
				select ccs_odc.title, ccs_odc.object_id,   ccs_obm.category_id,ccs_obm.type, ccs_odc.ccs_key, ccs_obm.datecreated, ccs_odc.status, ccs_adm.name
				from ccs_odc, ccs_obm, ccs_adm
				where ccs_odc.ccs_id = ccs_obm.ccs_id
				and ccs_odc.ccs_id = ccs_adm.ccs_id
				and ccs_odc.ccs_id = ".$ccs_id."
				and ccs_odc.ccs_key = ccs_obm.url_id
				and ccs_obm.url_id = ".$url_id."
				and ccs_odc.object_id = ccs_obm.object_id 
				and ccs_odc.language_id = 2
				and ccs_obm.type = 'sub-area'
				and ccs_obm.ccs_key = ".$ccs_key."
				and ccs_adm.admin_id = ccs_odc.createdby
				UNION
				select ccs_oct.title, ccs_oct.category_id as object_id , ccs_obm.category_id, ccs_obm.type, ccs_oct.ccs_key, ccs_obm.datecreated, ccs_oct.status , ccs_adm.name
				from ccs_oct, ccs_obm, ccs_adm
				where ccs_oct.ccs_id = ccs_obm.ccs_id
				and ccs_oct.ccs_id = ccs_adm.ccs_id
				and ccs_oct.ccs_id = ".$ccs_id."
				and ccs_oct.ccs_key = ccs_obm.url_id
				and ccs_obm.type = 'area'
				and ccs_obm.url_id = ".$url_id."
				and ccs_oct.category_id = ccs_obm.object_id 
				and ccs_oct.language_id = 2
				and ccs_obm.ccs_key = ".$ccs_key."
				and ccs_adm.admin_id = ccs_oct.createdby
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
}
	