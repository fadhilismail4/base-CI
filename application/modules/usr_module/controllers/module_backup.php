<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module_backup extends Admin {
	
	public function view($zone,$type){
		if ($this->s_login && !empty($type))
		{
			$data = array();
			
			$data['title'] 		= $this->title;//Set parameter view
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['lang']		= 2;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				'css/plugins/dataTables/dataTables.bootstrap.css',
				'css/plugins/dataTables/dataTables.responsive.css',
				'css/plugins/dataTables/dataTables.tableTools.min.css'
			);
			$data['js'] = array(
				'js/plugins/dataTables/jquery.dataTables.js',
				'js/plugins/dataTables/dataTables.bootstrap.js',
				'js/plugins/dataTables/dataTables.responsive.js',
				'js/plugins/dataTables/dataTables.tableTools.min.js',
				'js/modul/datatables.js',
				'js/plugins/nestable/jquery.nestable.js'
			);
			if($this->module_pkey){
				$data['parent'] = $this->dbfun_model->get_data_bys('link','module_id = '.$this->module_pkey.' and ccs_id = '.$this->zone_id.'','mdl')->link;
				$data['child'] = $this->module_link;
			}else{
				$data['parent'] = $this->module_link;
				$data['child'] = $this->module_link;
			}
			
			$data['ch'] = array(
				'title'=> $this->module_name, 
				'small'=> 'Manage your '.$this->module_name.' '
			);
			$data['breadcrumb'] = array(
				array(
					'url' => $this->zone.'/admin/dashboard', 
					'bcrumb' => 'Dashboard', 
					'active' => ''
				),
				array(
					'url' => $this->zone.'/admin/'.$this->module_link.'/'.$this->zone_id, 
					'bcrumb' => $this->module_name, 
					'active' => true
				)
			);//End set parameter view
			
			$type_id = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.' and module_id = '.$this->module_key.'','ccs_mdl')->type_id;
			$type_view = $this->dbfun_model->get_data_bys('type_view','ccs_id = '.$this->zone_id.' and module_id = '.$this->module_key.'','ccs_mdl')->type_view;
			
			if($type_id == 0){//for timeline
				
			}elseif($type_id == 1){//for generic
			
			}elseif($type_id == 2){//for product
			
			}elseif($type_id == 3){//for gallery
				
			}
			
			$data['custom_js'] = '<script>
				$(document).ready(function(){
					$("[data-param='. "'" .'category'. "'" .']")[0].click();
					$("[data-param='. "'" .'list'. "'" .']")[0].click();
				});
			</script>';	
			$m_status = $this->dbfun_model->get_data_bys('status','ccs_id = '.$this->zone_id.' and module_id = '.$this->module_key.'','ccs_mdl')->status;
			if($m_status == 1){
				$data['content'] = 'objects.php';
			}else{
				$data['status'] = $m_status;
				$data['content'] = 'warning.php';
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$type){	
		if ($this->s_login && !empty($type)){
			$data = array();
			
			$cek = $this->dbfun_model->get_data_bys('type_id,module_id,status, type_view, name','ccs_id = '.$this->zone_id.' and link like "'.$type.'"','ccs_mdl');
			$type_view = $cek->type_view;
			$type_id = $cek->type_id;
			$ccs_key = $cek->module_id;
			$m_status = $cek->status;
			$key_name = $cek->name;
			
			$this->load->model('Modules_model');
			
			$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
			$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
			$id = $this->input->post('id');
			if($this->input->post('language_id')){
				$lang = $this->input->post('language_id');
			}else{
				$lang = 2;
			}
			
			$part = $this->input->post('param');
			$data['child'] = $part;
			$data['part'] = $part;
			$data['language_id'] = $lang;			
			$data['key_name'] = $key_name;
			$data['object_type'] = $type;
			if($this->form_validation->run() == TRUE ){
				if($part == 'category'){
					$data['ccs_key'] = $ccs_key;
					$category = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = 0 order by ccs_oct.title ASC','oct');
					$datas = array();
					foreach($category as $c){
						
						$data['count'] = $this->Modules_model->get_category($ccs_key,$this->zone_id, $c->category_id);
						$count = count($data['count']);
						
						$child = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = '.$c->category_id.' order by ccs_oct.title ASC','oct');
						$childs = array();
						foreach($child as $cd){
							$c_count = $this->dbfun_model->count_result(array('category_id'=> $cd->category_id,'ccs_id' => $this->zone_id),'ccs_odc'); 
							$childs[] =  array('child'=> $cd,'count'=> $c_count);
						}
						$datas[] = array('parent' => $c,'count' => $count,'child' => $childs);
					}
					$data['category'] = $datas;
					echo "<script>
							 $(document).ready(function(){
								 $('#nestable2').nestable();
							 });
						</script>";
					$view = $this->load->view('objects_category', $data, TRUE);
				}elseif($part == 'list'){
					if($id == 'all'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$data['object_contents'] = $this->Modules_model->get_list($this->zone_id,$ccs_key);
					}else{
						$parent = $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' ','oct')->parent_id;
						$cid = $this->dbfun_model->get_data_bys('category_id','category_id = '.$id.' ','oct')->category_id;
						$lang = 2;
						if($parent != 0 ){
							$data['object_contents'] = $this->dbfun_model->get_all_data_bys('ccs_o.category_id, ccs_odc.language_id, ccs_odc.metakeyword, ccs_odc.metadescription, ccs_odc.description, ccs_oct.title as category, ccs_o.media_id, ccs_oct.parent_id, ccs_oct.parent_title as pcat, ccs_o.image_square as image, ccs_odc.title, ccs_odc.viewer, ccs_odc.status, ccs_odc.createdby, ccs_odc.datecreated, ccs_adm.name','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_key = ccs_o.ccs_key and ccs_o.media_id = ccs_odc.media_id and ccs_odc.language_id = '.$lang.' and ccs_odc.language_id = ccs_oct.language_id and ccs_oct.category_id = ccs_odc.category_id and ccs_oct.category_id = '.$id.' and ccs_adm.admin_id = ccs_odc.createdby ','oct, o, odc, adm');
						}else{
							$data['object_contents'] = $this->Modules_model->get_listcat($ccs_key, $cid);
						}
						$data['categories'] = $this->dbfun_model->get_data_bys('category_id, parent_id, parent_title, title','category_id = '.$id.' ','oct');
					}
						if($type_id == 0){//for timeline
							$view = $this->load->view('objects_list', $data, TRUE);
						}elseif($type_id == 1){//for generic
							$view = $this->load->view('objects_list', $data, TRUE);
						}elseif($type_id == 2){//for product
							$view = $this->load->view('objects_list', $data, TRUE);
						}elseif($type_id == 3){//for gallery
							$view = $this->load->view('objects_list_sawa', $data, TRUE);
						}
				}elseif($part == 'detail')
				{	
					$data['detail'] = $this->dbfun_model->get_data_bys('ccs_odc.category_id, ccs_odc.language_id, ccs_odc.metakeyword, ccs_odc.metadescription, ccs_odc.description, ccs_oct.title as category, ccs_o.object_id, ccs_o.image_square as image, ccs_odc.title, ccs_odc.viewer, ccs_odc.status, ccs_odc.createdby, ccs_odc.datecreated','ccs_o.object_id = '.$id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_o.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = ccs_o.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_odc.language_id = '.$lang.' and ccs_odc.language_id = ccs_oct.language_id and ccs_oct.category_id = ccs_odc.category_id','oct, o, odc');
					$data['link_img'] = $this->zone.'/'.$type;
					$data['breadcrumb'] = array(
						array(
							'ajax' => false,
							'url' => '', 
							'bcrumb' => $type, 
							'active' => ''
						),
						array(
							'ajax' => true,
							'id' => $id,
							'param' => 'detail', 
							'bcrumb' => $data['detail']->title, 
							'active' => true
						)
					);
					$data['createdby'] = $this->dbfun_model->get_data_bys('name','admin_id = '.$data['detail']->createdby.'','adm');
					$data['language'] = $this->dbfun_model->get_data_bys('language_id, code, name','language_id = '.$data['detail']->language_id.'','lng');
					$view = $this->load->view('objects_detail', $data, TRUE);
				}elseif($part == 'content'){
					$data['news'] = $this->dbfun_model->get_data_bys('ccs_o.category_id, ccs_odc.language_id, ccs_odc.metakeyword, ccs_odc.metadescription, ccs_odc.description, ccs_oct.title as category, ccs_o.media_id, ccs_o.image_square as image, ccs_odc.title, ccs_odc.viewer, ccs_odc.status, ccs_odc.createdby, ccs_odc.datecreated','ccs_o.media_id = '.$id.' and ccs_oct.type_id = 1 and ccs_oct.type_id = ccs_o.type_id and ccs_o.media_id = ccs_odc.media_id and ccs_odc.language_id = 2 and ccs_odc.language_id = ccs_oct.language_id and ccs_oct.category_id = ccs_o.category_id','oct, o, odc');
					$data['createdby'] = $this->dbfun_model->get_data_bys('name','admin_id = '.$data['news']->createdby.'','adm');
					$view = $this->load->view('object_content', $data, TRUE);
				}elseif($part == 'seo'){
					$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and c.type_id = 1 and c.type_id = m.type_id and m.media_id = d.media_id and d.language_id = 2 and d.language_id = c.language_id and c.category_id = m.category_id','base_mediacat c, base_media m, base_mediadesc d');
					$view = $this->load->view('object_seo', $data, TRUE);
				}elseif($part == 'new'){
					echo "
					<link href='".base_url()."assets/admin/css/plugins/datapicker/datepicker3.css' rel='stylesheet'>
					<link href='".base_url()."assets/admin/css/plugins/iCheck/custom.css' rel='stylesheet'>
					<link href='".base_url()."assets/admin/css/plugins/iCheck/custom.css' rel='stylesheet'>
					<link href='".base_url()."assets/admin/css/plugins/summernote/summernote-bs3.css' rel='stylesheet'>
					<link href='".base_url()."assets/admin/css/plugins/summernote/summernote.css' rel='stylesheet'>
					<link href='".base_url()."assets/admin/js/plugins/fileinput/fileinput.min.css' rel='stylesheet'>";
					echo "				
					<script src='".base_url()."assets/admin/js/plugins/datapicker/bootstrap-datepicker.js' type='text/javascript'></script>
					<script src='".base_url()."assets/admin/js/plugins/summernote/summernote.min.js' type='text/javascript'></script>
					<script src='".base_url()."assets/admin/js/plugins/fileinput/fileinput.min.js' type='text/javascript'></script>
					<script src='".base_url()."assets/admin/js/ajaxfileupload.js' type='text/javascript'></script>";
					echo "<script>
						$(document).ready(function(){
							$('#data_2').hide();
							$('#status').change(function() {
								if ($(this).find(':selected').val() === '2') {
									$('#data_2').slideDown('slow');
								} else {
									$('#data_2').slideUp('slow');
								}
							});
						});</script>";
					echo "
					<script>
					$(document).ready(function(){
						$('.summernote').summernote();
						$('#datepicker').datepicker({
							format: 'yyyy-mm-dd',
							keyboardNavigation: false,
							forceParse: false
						});

						$('.date').datepicker({
							keyboardNavigation: false,
							todayBtn: 'linked',
							format: 'yyyy-mm-dd',
							forceParse: false
						});
					});
					var edit = function() {
						$('.click2edit').summernote({focus: true});
					};
					var save = function() {
						var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
						$('.click2edit').destroy();
					};</script>" ;
					$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','ccs_key = '.$ccs_key.' and ccs_id = '.$this->zone_id.' and language_id = '.$lang.'','oct');
					if($type_id == 0 || $type_id == 1){//for timeline or generic
						$data['type_id'] = $type_id;
						$view = $this->load->view('objects_new', $data, TRUE);
					}elseif($type_id == 2){//for product
						$view = $this->load->view('objects_new', $data, TRUE);
					}elseif($type_id == 3){//for gallery
						$view = $this->load->view('objects_new', $data, TRUE);
					}
				}elseif($part == 'update'){
					$view = $this->load->view('objects_update', $data, TRUE);
				}elseif($part == 'new_category'){
					$data['language_id'] = 2;
					$data['category'] = $this->dbfun_model->get_all_data_bys('title, language_id, parent_id, category_id, ccs_key','ccs_key = '.$id.' and language_id = '.$lang.' and parent_id = 0 and ccs_id = '.$this->zone_id.' order by title ASC','oct');
					$data['ccs_key'] = $ccs_key;
					$data['link_create'] = 'category';
					$view = $this->load->view('objects_category_new', $data, TRUE);
				}elseif($part == 'view_category'){
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
					$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_oct.title, ccs_oct.parent_title, ccs_oct.datecreated, ccs_oct.createdby, ccs_oct.status, ccs_adm.name','ccs_oct.ccs_key = '.$id.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_oct.language_id = '.$lang.' and ccs_adm.admin_id=ccs_oct.createdby order by ccs_oct.title ASC','oct, adm');
					$data['cat'] = $type;
					$data['id'] = $id; 
					$data['count_c'] = count($data['category']);
					$view = $this->load->view('objects_category_list', $data, TRUE);
				}
				echo $view;
			}
		}else{
			redirect($this->login);
		}
	}
	
	public function view_update($zone,$type,$id){
		if ($this->s_login && !empty($type))
		{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				'css/plugins/datapicker/datepicker3.css',
				'css/plugins/iCheck/custom.css',
				'js/plugins/fileinput/fileinput.min.css',
				'css/plugins/chosen/chosen.css',
				'css/plugins/summernote/summernote-bs3.css',
				'css/plugins/summernote/summernote.css'
			);
			$data['js'] = array(
				'js/plugins/iCheck/icheck.min.js',
				'js/plugins/datapicker/bootstrap-datepicker.js',
				'js/plugins/summernote/summernote.min.js',
				'js/plugins/chosen/chosen.jquery.js',
				'js/plugins/fileinput/fileinput.min.js',
				'js/ajaxfileupload.js',
				'js/modul/new.js'
			);
			$data['parent'] = 'media';
			//End set parameter view
			
			if($type == 'news')
			{
				$data['child'] = 'news';
				$data['ch'] = array(
					'title'=>' Add News', 
					'small'=>'Create your news '
				);
				
				$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, m.media_id, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and m.media_id = d.media_id and d.language_id = 2','base_mediacat c, base_media m, base_mediadesc d');
				
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/news', 
						'bcrumb' => 'News', 
						'active' => ''
					),
					array(
						'url' => 'admin/news/update/'.$id.'', 
						'bcrumb' => 'Update '.$data['news']->title.'', 
						'active' => true
					)
				);
				
				$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','type_id = 1 and language_id = 2','base_mediacat');
				
				$data['content'] = 'news_update.php';
				if(!empty($data['news']->image)){
					$data['custom_js'] = '<script>
						$(document).ready(function(){
							$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
							$("#status").val('.trim($data['news']->status).');
							$("#category_id").val('.trim($data['news']->category_id).');
							$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
							$(".file").fileinput("refresh",{
								initialPreview: [
									"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/news/'.$data['news']->image.'" class="file-preview-image">').'"
								]
							});
						});</script>';
				}else{
					$data['custom_js'] = '<script>
					$(document).ready(function(){
						$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
						$("#status").val('.trim($data['news']->status).');
						$("#category_id").val('.trim($data['news']->category_id).');
						$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
						$(".file").fileinput("refresh",{
							initialPreview: [
								"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/news/news_empty.png'.'" class="file-preview-image">').'"
							]
						});
					});</script>';
				}
			}
			elseif($type == 'event')
			{
				$data['child'] = 'Event';
				$data['ch'] = array(
					'title'=>' Add Event', 
					'small'=>'Create your event '
				);
				
				$data['news'] = $this->dbfun_model->get_data_bys('m.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, m.media_id, m.datestart, m.dateend, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated','m.media_id = '.$id.' and m.media_id = d.media_id and d.language_id = 2','base_mediacat c, base_media m, base_mediadesc d');
				
				$data['breadcrumb'] = array(
					array(
						'url' => 'admin/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => 'admin/event', 
						'bcrumb' => 'Events', 
						'active' => ''
					),
					array(
						'url' => 'admin/event/update/'.$id.'', 
						'bcrumb' => 'Update '.$data['news']->title.'', 
						'active' => true
					)
				);
				
				$data['category']= $this->dbfun_model->get_all_data_bys('category_id, title','type_id = 2 and language_id = 2','base_mediacat');
				
				$data['content'] = 'events_update.php';
				if(!empty($data['news']->image)){
					$data['custom_js'] = '<script>
						$(document).ready(function(){
							$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
							$("#status").val('.trim($data['news']->status).');
							$("#category_id").val('.trim($data['news']->category_id).');
							$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
							$("#datestart").val("'.trim(date('Y-m-d', strtotime($data['news']->datestart))).'");
							$("#dateend").val("'.trim(date('Y-m-d', strtotime($data['news']->dateend))).'");
							$(".file").fileinput("refresh",{
								initialPreview: [
									"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/event/'.$data['news']->image.'" class="file-preview-image">').'"
								]
							});
						});</script>';
				}else{
					$data['custom_js'] = '<script>
					$(document).ready(function(){
						$(".summernote").code("'.str_replace('"',"'",trim($data['news']->description)).'");
						$("#status").val('.trim($data['news']->status).');
						$("#category_id").val('.trim($data['news']->category_id).');
						$("#datecreated").val("'.trim(date('Y-m-d', strtotime($data['news']->datecreated))).'");
						$("#datestart").val("'.trim(date('Y-m-d', strtotime($data['news']->datestart))).'");
						$("#dateend").val("'.trim(date('Y-m-d', strtotime($data['news']->dateend))).'");
						$(".file").fileinput("refresh",{
							initialPreview: [
								"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/media/event/events_empty.png'.'" class="file-preview-image">').'"
							]
						});
					});</script>';
				}
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}
	
	public function create($zone,$type){
    	if ($this->s_login && !empty($type))
    	{
			$data = array();
			$status = '';
			$message = '';
			$cek = $this->dbfun_model->get_data_bys('type_id,module_id','ccs_id = '.$this->zone_id.' and link like "'.$type.'"','ccs_mdl');
			$type_id = $cek->type_id;
			$ccs_key = $cek->module_id;
			$param = $this->input->post('param');
			if($type)
			{
				if($param == 'category'){ //category
					$id = $this->input->post('category_id');
					$lang = $this->input->post('language_id');
					$admin_id = $this->admin_id;
				
					if($id) //update
					{
						if ($this->form_validation->run('ccs_oct') === TRUE)
						{
							$update = $this->update_data_bys('*', array('category_id' => $id), 'base_mediacat','','','','');
							
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{
								if($type == 'category_news'){
									$type_id = 1;
								}elseif($type == 'category_event'){
									$type_id = 2;
								}elseif($type == 'category_gallery'){
									$type_id = 3;
								}
								$parent = $this->input->post('parent_id');
								if($parent != 0){								
									$update['parent_title'] = $this->dbfun_model->get_data_bys('title','category_id = '.$parent.' and type_id = '.$type_id.' ','base_mediacat')->title;
								}else{								
									$update['parent_title'] = '';
								}
								$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang), $update, 'base_mediacat');
								$status = 'success';
								$message = 'Data updated successfully';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else  // create
					{
						if ($this->form_validation->run('ccs_oct') === TRUE)
						{								
							$parent = $this->input->post('parent_id');
							if($parent != 0){								
								$title = $this->dbfun_model->get_data_bys('title','category_id = '.$parent.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' ','ccs_oct')->title;
							}else{								
								$title = '';
							}
							
							$category_id = $this->dbfun_model->get_data_bys('MAX(category_id) as category_id','ccs_key = '.$ccs_key.' ','ccs_oct')->category_id + 1;
							$custom_primary = array(
										'ccs_id' => $this->zone_id,
										'ccs_key' => $ccs_key,
										'status' => 1,
										'createdby' => $admin_id,
										'parent_title' => $title,
										'category_id' => $category_id
									);	
							$this->new_data('ccs_oct',$custom_primary, '','', '','');
							$status = 'success';
							$message = 'Data created successfully';
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					echo json_encode(array('status' => $status, 'm' => $message));
				}else{ //object
					$id = $this->input->post('media_id');
					$lang = $this->input->post('language_id');
					if($id) //update
					{
						if ($this->form_validation->run('ccs_odc') === TRUE)
						{
							if(isset($_FILES['image_square'])){
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','image_square','img/media/news','image_square','jpeg|jpg|png');
							}else{
								$update = $this->update_data_bys('*', array('media_id' => $id), 'base_media','','','','');
							}
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{
								$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_media');
								$status = 'success';
								$message = 'Data updated successfully';
							}
							if ($this->form_validation->run('ccs_odc') === TRUE)
							{
								$update2 = $this->update_data_bys('*', array('media_id' => $id), 'ccs_odc','','','','');
								if($update2)
								{
									$this->dbfun_model->update_table(array('media_id' => $id, 'language_id' => $lang), $update2, 'ccs_odc');
									$status = 'success';
									$message = 'Data updated successfully';
								}
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else  // create
					{
						if ($this->form_validation->run('ccs_odc') === TRUE)
						{
							$image = $this->input->post('image_square');
							$custom_primary = array(
										'type_id' => $type_id,
										'ccs_id' => $this->zone_id,
										'ccs_key' => $ccs_key
									);
							if (!is_dir('assets/'.$this->zone.'/'.$type))
							{
								mkdir('./assets/'.$this->zone.'/'.$type, 0777, true);
							}
							$value = $this->new_data('ccs_o',$custom_primary, 'image_square','image_square', $this->zone.'/'.$type,'jpeg|jpg|png');
														
							if ($this->form_validation->run('ccs_odc') === TRUE)
							{
								$status = $this->input->post('status');
								$admin_id = $this->admin_id;
								if($status == 0){
									$custom = array(
												'object_id' => $value,
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'createdby' => $admin_id
											);
								}elseif($status == 1){
									$custom = array(
												'object_id' => $value,
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'createdby' => $admin_id,
												'datecreated' => date('Y-m-d')
											);
								}elseif($status == 2){
									$custom = array(
												'object_id' => $value,
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'createdby' => $admin_id,
												'datecreated' => $this->input->post('datecreated'),
												'datepublish' => $this->input->post('datecreated')
											);
								}
								$create_new_secondary = $this->new_data_secondary('ccs_odc',$custom,'','','','');	
								$this->dbfun_model->ins_to('ccs_odc', $create_new_secondary);
								
								$status = 'success';
								$message = 'Data created successfully';
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					echo json_encode(array('status' => $status, 'm' => $message));
				}
			}
			/* elseif($type == 'category_ideas')
			{
				$id = $this->input->post('category_id');
				$lang = $this->input->post('language_id');
				$admin_id = $this->session->userdata('adm_admin_id');
				
					if($id) //update
					{
						if ($this->form_validation->run('com_ideacat') === TRUE)
						{
							$update = $this->update_data_bys('*', array('category_id' => $id), 'com_ideacat','','','','');
							
							$status = 'success';
							$message = 'No data updated';
							
							if($update)
							{	
								$parent = $this->input->post('parent_id');
								if($parent != 0){								
									$update['parent_title'] = $this->dbfun_model->get_data_bys('title','category_id = '.$parent.'','com_ideacat')->title;
								}else{										
									$update['parent_title'] = '';
								}
								$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang), $update, 'com_ideacat');
								$status = 'success';
								$message = 'Data updated successfully';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					
				echo json_encode(array('status' => $status, 'm' => $message));
			} */
		}else{
			redirect($this->login);
		}
    }
	
	public function status($type){
		if ($this->s_login && !empty($type))
    	{
			$data = array();
    		$status = '';
    		$message = '';
			
			$this->form_validation->set_rules('id', 'page', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('lang', 'language', 'trim|numeric|xss_clean');
			$id = $this->input->post('id');
			$action = $this->input->post('status');
			$lang = $this->input->post('lang');
			if ($this->form_validation->run() === TRUE)
			{
				if($type == 'news')
				{
					if($action == 'draft')
					{
						$update = array
						(
							'status' => 0
						);
					}
					elseif($action == 'publish')
					{
						$update = array
						(
							'status' => 1
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('media_id' => $id,'language_id' => $lang), $update, 'base_mediadesc');
					}
					else
					{
						$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_mediadesc');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}
				elseif($type == 'event')
				{
					if($action == 'draft')
					{
						$update = array
						(
							'status' => 0
						);
					}
					elseif($action == 'publish')
					{
						$update = array
						(
							'status' => 1
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('media_id' => $id,'language_id' => $lang), $update, 'base_mediadesc');
					}
					else
					{
						$this->dbfun_model->update_table(array('media_id' => $id), $update, 'base_mediadesc');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}elseif($type == 'category_media')
				{
					$part = $this->input->post('param');
					if($part == 'news'){
						$type_id = 1;
					}elseif($part == 'event'){
						$type_id = 2;
					}elseif($part == 'gallery'){
						$type_id = 3;
					}
					if($action == 'inactivate')
					{
						$update = array
						(
							'status' => 0,
							'type_id' => $type_id
						);
					}
					elseif($action == 'activate')
					{
						$update = array
						(
							'status' => 1,
							'type_id' => $type_id
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang, 'type_id' => $type_id), $update, 'base_mediacat');
					}
					else
					{
						$this->dbfun_model->update_table(array('category_id' => $id, 'type_id' => $type_id), $update, 'base_mediacat');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}elseif($type == 'category_ideas')
				{
					if($action == 'inactivate')
					{
						$update = array
						(
							'status' => 0
						);
					}
					elseif($action == 'activate')
					{
						$update = array
						(
							'status' => 1
						);
					}
					if($lang)
					{
						$this->dbfun_model->update_table(array('category_id' => $id,'language_id' => $lang), $update, 'com_ideacat');
					}
					else
					{
						$this->dbfun_model->update_table(array('category_id' => $id), $update, 'com_ideacat');
					}
					
					$status = 'success';
					$message = 'Data updated successfully';
				}
			}else{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}
	}
	
	public function delete($type){
    	if ($this->s_login && !empty($type))
    	{
			$data = array();
    		$status = '';
    		$message = '';
			
			if($type == 'news'){

				$this->form_validation->set_rules('id', 'news id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{
					$id = $this->input->post('id');
					$lang = $this->input->post('lang');
					
					$this->cek_n_delete_data('media_id,image_square', array('media_id' => $id), 'base_media', 'image_square','img/media/news',array('media_id' => $id),'base_mediadesc',array('media_id' => $id, 'language_id' => $lang));
			
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
			}elseif($type == 'event'){

				$this->form_validation->set_rules('id', 'event id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{
					$id = $this->input->post('id');
					$lang = $this->input->post('lang');
					
					$this->cek_n_delete_data('media_id,image_square', array('media_id' => $id), 'base_media', 'image_square','img/media/event',array('media_id' => $id),'base_mediadesc',array('media_id' => $id, 'language_id' => $lang));
			
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
			}elseif(($type == 'category_event') || ($type == 'category_event')){

				$this->form_validation->set_rules('id', 'event id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{
					$id = $this->input->post('id');
					$lang = $this->input->post('lang');
					
					$this->dbfun_model->del_tables('category_id = '.$id.' and language_id = '.$lang.' ','base_mediacat');
			
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
			}
		}else{
			redirect($this->login);
		}
    }

}