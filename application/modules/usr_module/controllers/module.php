<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module extends User {
	
	public function view($zone,$sigilo){
		if ($this->s_login && !empty($sigilo))
		{
			$data = array();
			
			$data['title'] 		= $this->title;//Set parameter view
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['lang']		= 2;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['theme'] 		= $this->theme_id;
			$data['user'] 		= $this->user;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				'css/plugins/dataTables/dataTables.bootstrap.css',
				'css/plugins/dataTables/dataTables.responsive.css',
				'css/plugins/dataTables/dataTables.tableTools.min.css'
			);
			$data['js'] = array(
				'js/plugins/dataTables/jquery.dataTables.js',
				'js/plugins/dataTables/dataTables.bootstrap.js',
				'js/plugins/dataTables/dataTables.responsive.js',
				'js/plugins/dataTables/dataTables.tableTools.min.js',
				'js/modul/datatables.js',
				'js/plugins/nestable/jquery.nestable.js'
			);
			if($this->module_pkey){
				$data['parent'] = $this->dbfun_model->get_data_bys('link','module_id = '.$this->module_pkey.' and ccs_id = '.$this->zone_id.'','umd')->link;
				$data['child'] = $this->module_link;
			}else{
				$data['parent'] = $this->module_link;
				$data['child'] = $this->module_link;
			}
			$cek = $this->dbfun_model->get_data_bys('ccs_umd.type_id, ccs_umd.type_view, ccs_umd.status as stato, ccs_umd.tu_id as parole, ccs_zone.type_id as chiave','ccs_umd.ccs_id = '.$this->zone_id.' and ccs_umd.module_id = '.$this->module_key.' and ccs_umd.ccs_id = ccs_zone.ccs_id','umd, zone');
			$chiave = $cek->chiave;
			$parole = $cek->parole;
			$stato = $cek->stato;
			$duum = $cek->type_id;
			$virato = $cek->type_view;
			$data['ch'] = array(
				'title'=> $this->module_name, 
				'small'=> 'Manage your '.$this->module_name.' '
			);
			$data['breadcrumb'] = array(
				array(
					'url' => $this->zone.'/admin/dashboard', 
					'bcrumb' => 'Dashboard', 
					'active' => ''
				),
				array(
					'url' => $this->zone.'/user/'.$this->module_link.'/'.$this->zone_id, 
					'bcrumb' => $this->module_name, 
					'active' => true
				)
			);
			
			if($chiave == 5){
			
				$data['ch'] = array(
					'title'=> $this->module_name, 
					'small'=> 'Kelola data '.strtolower($this->module_name).' dalam website ...'
				);
				$data['breadcrumb'] = array(
					array(
						'url' => $this->zone.'/admin/dashboard', 
						'bcrumb' => 'Dasbor', 
						'active' => ''
					),
					array(
						'url' => $this->zone.'/user/'.$this->module_link.'/'.$this->zone_id, 
						'bcrumb' => $this->module_name, 
						'active' => true
					)
				);
			
				$data['custom_js'] = '<script>
					$(document).ready(function(){
						$("[data-param='. "'" .'category'. "'" .']")[0].click();
						$("[data-param='. "'" .'list'. "'" .']")[0].click();
					});
				</script>';		
			}
			else
			{
				if($chiave == 2){
					if($duum == 0){
						switch ($virato) {
							case '0' :
								$v = array(
									1 => 'col-lg-9',
									'child' => array(
										1 => 'col-lg-12 detail_content2 {data:list_o}',
									),
									2 => 'col-lg-3 detail_content {data:sidebar_oct}'
								);break;
						}
						$data['css'] = '';
						$data['js'] = '';
						$data['custom'] = true;
						$data['custom_js'] = '<script>
							$(document).ready(function(){
								$("[data-param='. "'" .'list'. "'" .']")[0].click();
								$("[data-param='. "'" .'sidebar'. "'" .']")[0].click();
							});
						</script>';	
					}elseif($duum == 1){
						switch ($virato) {
							case '0' :
								$v = array(
									1 => 'col-lg-8 detail_content {data:list_o}',
									2 => 'col-lg-4 detail_content2 {data:sidebar_o}'
								);break;
						}
						$data['custom'] = true;
						$data['custom_js'] = '<script>
							$(document).ready(function(){
								$("[data-param='. "'" .'list'. "'" .']")[0].click();
								$("[data-param='. "'" .'sidebar'. "'" .']")[0].click();
							});
						</script>';	
					}elseif($duum == 3){
						switch ($virato) {
							case '3' :
								$v = array(
									1 => 'col-lg-3 detail_content {data:category}',
									2 => 'col-lg-9 detail_content2 {data:list}'
								);break;
						}
						$data['custom'] = true;
						$data['custom_js'] = '<script>
							$(document).ready(function(){
								$("[data-param='. "'" .'list'. "'" .']")[0].click();
								$("[data-param='. "'" .'category'. "'" .']")[0].click();
							});
						</script>';	
					}else{
						$brush = (array) $virato;
					}
					if($duum == 0){
						if($virato == 3){
							$v = array(
									1 => 'col-lg-3 detail_content {data:category}',
									2 => 'col-lg-9 detail_content2 {data:list}'
								);
							$data['custom'] = true;
							$data['custom_js'] = '<script>
								$(document).ready(function(){
									$("[data-param='. "'" .'list'. "'" .']")[0].click();
									$("[data-param='. "'" .'category'. "'" .']")[0].click();
								});
							</script>';	
						};
					}
				}else{
					$brush = (array) $virato;
				}
				if(!isset($v)){
					foreach($brush as $key => $value){
						switch ($value) {
							case '0':
								$v[] = 'start'; $v[] = 'col-lg-0'; $v[] = 'col-lg-12'; $v[] = 'end';
								break;
							case '1':
								$v[] = 'start'; $v[] = 'col-lg-3'; $v[] = 'col-lg-9'; $v[] = 'end';
								break;
							case '2':
								$v[] = 'start'; $v[] = 'col-lg-6'; $v[] = 'col-lg-6'; $v[] = 'end';
								break;
							case '3':
								$v[] = 'start'; $v[] = 'col-lg-9'; $v[] = 'col-lg-3'; $v[] = 'end';
								break;
							case '4':
								$v[] = 'start'; $v[] = 'col-lg-0'; $v[] = 'col-lg-12'; $v[] = 'end';
								break;
							case '5':
								$v[] = 'start'; $v[] = 'col-lg-4'; $v[] = 'col-lg-4'; $v[] = 'col-lg-4'; $v[] = 'end';
								break;
							default:
								break;
						}
					}
					$data['custom_js'] = '<script>
						$(document).ready(function(){
							$("[data-param='. "'" .'category'. "'" .']")[0].click();
							$("[data-param='. "'" .'list'. "'" .']")[0].click();
						});
					</script>';	
				}
				
				$data['canvas'] = $v;
			}
			if($stato == 1){
				if($chiave == 5){
					$data['content'] = 'module.php';
				}else{
					$data['content'] = 'canvas.php';
				}
			}else{
				$data['status'] = $stato;
				$data['content'] = 'warning.php';
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$sigilo){
		if ($this->s_login && !empty($sigilo)){
			$data = array();
			$this->load->model('Modules_model');
			$cek = $this->dbfun_model->get_data_bys('ccs_umd.type_id, ccs_umd.type_view, ccs_umd.status as stato, ccs_umd.tu_id as parole, ccs_zone.type_id as chiave, ccs_umd.name as essentiel, ccs_umd.module_id as essentiel_id','ccs_umd.ccs_id = '.$this->zone_id.' and ccs_umd.link like "'.$sigilo.'" and ccs_umd.ccs_id = ccs_zone.ccs_id','umd, zone');
			$chiave = $cek->chiave;
			$parole = $cek->parole;
			$data['simo'] = $this->simo ;
			$stato = $cek->stato;	
			$duum = $cek->type_id;
			$virato = $cek->type_view;
			$fascia = $this->zone_id; 
			$essentiel = $cek->essentiel; 
			$essentiel_id = $cek->essentiel_id; 
			
			$part = $this->input->post('param');
			$part2 = $this->input->post('param2');
			$data['child'] = $part;
			$data['part'] = $part;
			$data['zone'] = $zone;
						
			$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
			$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
			$id = $this->input->post('id');
			if($this->input->post('language_id')){
				$lingua = $this->input->post('language_id');
			}else{
				$lingua = 2;
			}
			
			
			if($this->form_validation->run() == TRUE ){
				if($chiave == 5){
					$data['lingua'] = $lingua;
					$data['essentiel_id'] = $essentiel_id;
					$data['essentiel'] = $essentiel;
					$data['sigilo'] = $sigilo;
					$data['fascia'] = $fascia;
					$data['fattore'] = $this->user_id;
					$segretis = $this->segretis;
					$fattore = $this->user_id;
					$simo = $this->simo;
					$balai = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Balai"','mdl')->module_id;
					$lokasi = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Lokasi"','mdl')->module_id;
					$jadwal = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Penjadwalan"','mdl')->module_id;
					$inspeksi = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Inspeksi"','mdl')->module_id;
					$jembatan = $this->dbfun_model->get_data_bys('module_id, name','ccs_id = '.$fascia.' and name = "Jembatan" ','mdl')->module_id;
					
					$anchor = $this->dbfun_model->get_data_bys('o_email','ccs_id = '.$fascia.' and user_id = '.$fattore.'','usr')->o_email;
					
					if($part == 'category'){
						
						echo "<script>
								 $(document).ready(function(){
									 $('#nestable2').nestable();
								 });
						</script>";
						if($virato == 0){							
							if($duum == 0){											
								$category = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.category_id as kategori','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' order by ccs_oct.category_id ASC','oct, obm');
								$datas = array();
								foreach($category as $c){
									$data['count'] = $this->Modules_model->get_category($lokasi,$fascia, $c->category_id);
									$count = count($data['count']);
									$datas[] = array('parent' => $c,'count' => $count,);
								}
								$data['category'] = $datas;
								$view = $this->load->view('custom_hanacat', $data, TRUE);
							}elseif($duum == 3){
								$category = $this->dbfun_model->get_all_data_bys('title, category_id, language_id, parent_id as object_id','ccs_id = '.$fascia.' and ccs_key = '.$jadwal.' and language_id = '.$lingua.'','oct');
								$datas = array();
								foreach($category as $c){
									
									$child = $this->dbfun_model->get_all_data_bys('title, category_id, language_id, object_id','ccs_id = '.$fascia.' and ccs_key = '.$jadwal.' and language_id = '.$lingua.' and category_id = '.$c->category_id.'  ','odc');
									
									$childs = array();
									foreach($child as $cd){
										$c_count = count($this->dbfun_model->get_all_data_bys('title, category_id, language_id, object_id, love','ccs_id = '.$fascia.' and category_id = '.$segretis.' and ccs_key = '.$inspeksi.' and love = '.$cd->object_id.' ','odc'));
										$data['c_count'] = $c_count;
										$childs[] =  array('child'=> $cd,'count'=> $c_count);
									}
									$datas[] = array('parent' => $c,'child' => $childs);
									//$data['category'] = $child;
								}
								$data['category'] = $datas;
								//print_r($data['category']);
								$view = $this->load->view('custom_kadacat', $data, TRUE);
							}elseif(($duum == 6) && ($simo == 0)){//role
								$category = $this->dbfun_model->get_all_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and rt_id = 0 order by name ASC','umr');
								foreach($category as $c){
									$count = $this->dbfun_model->count_result('ccs_id = '.$this->zone_id.' and type_id = '.$c->type_id.' and rt_id = '.$segretis.'','usr');
									$datas[] = (object) array_merge((array) $c,array('total' => $count));
								}
								$data['category'] = $datas;
								$data['cat_count'] = count($data['category']);
								$view = $this->load->view('custom_padacat', $data, TRUE);	
							}		
						}elseif($virato == 1){							
							if($duum == 1){										
								$category = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.category_id as kategori','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' order by ccs_oct.category_id ASC','oct, obm');
								$datas = array();
								foreach($category as $c){
									$data['count'] = $this->Modules_model->get_category($lokasi,$fascia, $c->category_id);
									$count = count($data['count']);
									$datas[] = array('parent' => $c,'count' => $count,);
								}
								$data['category'] = $datas;
								$view = $this->load->view('custom_caracat', $data, TRUE);
							}elseif($duum == 3){
								$category = $this->dbfun_model->get_all_data_bys('title, category_id, language_id, parent_id as object_id','ccs_id = '.$fascia.' and ccs_key = '.$jadwal.' and language_id = '.$lingua.'','oct');
								$datas = array();
								foreach($category as $c){
									
									$child = $this->dbfun_model->get_all_data_bys('title, category_id, language_id, object_id','ccs_id = '.$fascia.' and ccs_key = '.$jadwal.' and language_id = '.$lingua.' and category_id = '.$c->category_id.'  ','odc');
									
									$childs = array();
									foreach($child as $cd){
										$c_count = count($this->dbfun_model->get_all_data_bys('title, category_id, language_id, object_id, love','ccs_id = '.$fascia.' and category_id = '.$segretis.' and ccs_key = '.$inspeksi.' and love = '.$cd->object_id.' ','odc'));
										$data['c_count'] = $c_count;
										$childs[] =  array('child'=> $cd,'count'=> $c_count);
									}
									$datas[] = array('parent' => $c,'child' => $childs);
									//$data['category'] = $child;
								}
								$data['category'] = $datas;
								//print_r($data['category']);
								$view = $this->load->view('custom_tasacat', $data, TRUE);
							}	
						}elseif($virato == 3){							
							if($duum == 0){			
								$category = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$this->user_id.'','usr');
								$data['profile'] = $category;
								$data['zone'] = $this->zone;
								/* print_r($category); */
								$view = $this->load->view('custom_wala', $data, TRUE);
							}elseif($duum == 6){//belum
								$view = $this->load->view('custom_tasacat', $data, TRUE);
							}	
						}						
					}
					elseif($part == 'list' ){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$data['id'] = $id;
						if($virato == 0){							
							if($duum == 0){							
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
								if($id == 'all'){
									
									$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_odc.category_id, ccs_odc.language_id,ccs_oct.title as category, ccs_o.object_id, ccs_oct.parent_id, ccs_oct.parent_title as pcat, ccs_o.image_square as image, ccs_odc.title, ccs_odc.viewer, ccs_odc.status, ccs_odc.createdby, ccs_odc.datecreated, ccs_adm.name','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$balai.' and ccs_oct.ccs_key = ccs_o.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_odc.language_id = '.$lingua.' and ccs_odc.language_id = ccs_oct.language_id and ccs_oct.category_id = ccs_odc.category_id and ccs_adm.admin_id = ccs_odc.createdby ','oct, o, odc, adm');
								}elseif($id != 0){
									
									$data['objects'] = $this->dbfun_model->get_all_data_bys('object_id, title, category_id','ccs_id = '.$fascia.' and ccs_key = '.$lokasi.' and category_id = '.$id.' and language_id = '.$lingua.' ','odc');
									$data['categories'] = $this->dbfun_model->get_data_bys('title, ccs_key, category_id','ccs_id = '.$fascia.' and ccs_key = '.$lokasi.' and category_id ='.$id.'','oct');
								}								
								$data['c_objects'] = count($data['objects']);
								$view = $this->load->view('custom_hanalist', $data, TRUE);
							}elseif($duum == 3){
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								$datas = array();
								if($id == 'all'){	
									$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend','ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.ccs_key = '.$inspeksi.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.metadescription = "'.$anchor.'" and ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key','odc, adm, o');
									
								}elseif($id != 0){
									$data['categories'] = $this->dbfun_model->get_data_bys('ccs_oct.category_id, ccs_oct.title','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.category_id = '.$id.' ','oct'); 
									
									$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend','ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.ccs_key = '.$inspeksi.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.metadescription = "'.$anchor.'" and ccs_odc.category_id = '.$id.' and ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key','odc, adm, o');
								}
								
								$data['c_objects'] = count($data['objects']);
								$view = $this->load->view('custom_kadalist', $data, TRUE);
							}elseif(($duum == 6) && ($simo == 0)){ //role
								if($id == 'all'){
									$obj = $this->dbfun_model->get_all_data_bys('ccs_usr.user_id,ccs_umr.type_id,ccs_usr.name,ccs_umr.name as type, ccs_usr.status,ccs_usr.createdby','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.role_id = ccs_umr.role_id and ccs_usr.rt_id = '.$segretis.'','usr,umr');
								}else{
									$obj = $this->dbfun_model->get_all_data_bys('ccs_usr.user_id,ccs_umr.type_id,ccs_usr.name,ccs_umr.name as type, ccs_usr.status,ccs_usr.createdby','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.role_id = ccs_umr.role_id and ccs_usr.rt_id = '.$segretis.' and ccs_umr.type_id = '.$id.'','usr,umr');
									$data['role'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$id.'','umr');
								}
								$data['objects'] = $obj;
								$data['c_objects'] = count($obj);
								$data['user_id'] = $this->user_id;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 9 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
								$view = $this->load->view('custom_padalist', $data, TRUE);	
							}
						}elseif($virato == 1){							
							if($duum == 1){							
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
								$data['istatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
								if($id == 'all'){															
									$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_odc.*, ccs_oct.title as area, ccs_usr.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_usr.ccs_id and ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.createdby = ccs_usr.user_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_odc.ccs_key = '.$jembatan.' and ccs_odc.love = ccs_oct.category_id and ccs_oct.ccs_key = '.$lokasi.' and ccs_odc.metakeyword = "'.$anchor.'" and ccs_o.object_id = ccs_odc.object_id and ccs_oct.language_id = ccs_odc.language_id and ccs_oct.language_id = '.$lingua.' ','oct, o, odc, usr');
								}elseif($id != 0){				
									
									$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_odc.*, ccs_oct.title as area, ccs_usr.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_usr.ccs_id and ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.createdby = ccs_usr.user_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_odc.ccs_key = '.$jembatan.' and ccs_odc.love = ccs_oct.category_id and ccs_oct.ccs_key = '.$lokasi.' and ccs_odc.metakeyword = "'.$anchor.'" and ccs_o.object_id = ccs_odc.object_id and ccs_oct.language_id = ccs_odc.language_id and ccs_oct.language_id = '.$lingua.' and ccs_odc.love = '.$id.' ','oct, o, odc, usr');
									$data['categories'] = $this->dbfun_model->get_data_bys('metakeyword as title, ccs_key, category_id','ccs_id = '.$fascia.' and ccs_key = '.$lokasi.' and category_id ='.$id.'','oct');
								}								
								$data['c_objects'] = count($data['objects']);
								$view = $this->load->view('custom_caralist', $data, TRUE);
							}elseif($duum == 3){
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								$datas = array();
								if($id == 'all'){	
									$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_obr.*, ccs_usr.name, ccs_oct.title as tahun, ccs_odc.category_id as c_id','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.metakeyword = "'.$anchor.'" and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.object_id = ccs_obr.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_key = ccs_odc.ccs_key','obr, usr, odc, oct');
									
								}elseif($id != 0){
									$data['categories'] = $this->dbfun_model->get_data_bys('ccs_oct.category_id, ccs_oct.title','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.category_id = '.$id.' ','oct'); 
									
									$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_obr.*, ccs_usr.name, ccs_oct.title as tahun, ccs_odc.category_id as c_id','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.metakeyword = "'.$anchor.'" and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.category_id ='.$id.' and ccs_odc.object_id = ccs_obr.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_key = ccs_odc.ccs_key ','obr, usr, odc, oct');
								}
								
								$data['c_objects'] = count($data['objects']);
								$view = $this->load->view('custom_tasalist', $data, TRUE);
							}		
						}elseif($virato == 3){							
							if($duum == 0){							
								$obj = $this->dbfun_model->get_all_data_bys('ccs_key,url_id,type,datecreated','ccs_id = '.$this->zone_id.' and user_id = '.$this->user_id.' group by ccs_key,url_id,type order by datecreated DESC LIMIT 5','ccs_ulg');
								foreach($obj as $o){
									$ccs_key = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and module_id = '.$o->ccs_key.'','umd');
									if(!empty($ccs_key)){
										$module = (object) array_merge((array) $o,array('module' => $ccs_key->name));
										$oo = $this->dbfun_model->get_data_bys('title,category_id','ccs_id = '.$this->zone_id.' and object_id = '.$o->url_id.' and ccs_key = '.$o->ccs_key.'','odc');
										if(!empty($oo)){
											$cat = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and category_id = '.$oo->category_id.' and ccs_key = '.$o->ccs_key.'','oct');
											$oo2 = (object) array_merge((array) $module,array('title' => $oo->title));
											$objects[] = (object) array_merge((array) $oo2,array('category' => $cat->title));
										}else{
											$objects[] = $module;
										}
									}else{
										if(!empty($o->url_id)){
											$objects[] = (object) array_merge((array) $o,array('module' => 'login'));
										}else{
											$objects[] = (object) array_merge((array) $o,array('module' => 'logout'));
										}
									}
								}
								$data['objects'] = $objects;
								$view = $this->load->view('custom_walalist', $data, TRUE);
							}elseif($duum == 6){
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								$datas = array();
								if($id == 'all'){	
									$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_obr.*, ccs_usr.name, ccs_oct.title as tahun, ccs_odc.category_id as c_id','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.metakeyword = "'.$anchor.'" and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.object_id = ccs_obr.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_key = ccs_odc.ccs_key','obr, usr, odc, oct');
									
								}elseif($id != 0){
									$data['categories'] = $this->dbfun_model->get_data_bys('ccs_oct.category_id, ccs_oct.title','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.category_id = '.$id.' ','oct'); 
									
									$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_obr.*, ccs_usr.name, ccs_oct.title as tahun, ccs_odc.category_id as c_id','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.metakeyword = "'.$anchor.'" and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.category_id ='.$id.' and ccs_odc.object_id = ccs_obr.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_key = ccs_odc.ccs_key ','obr, usr, odc, oct');
								}
								
								$data['c_objects'] = count($data['objects']);
								$view = $this->load->view('custom_tasalist', $data, TRUE);
							}		
						}			
					}
					elseif($part == 'view_category'){
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');														
						$segretis = $this->segretis;
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
						if($virato == 0){							
							if($duum == 0){
								
								$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.status, ccs_oct.datecreated, ccs_oct.description, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.object_id, ccs_adm.name','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' and ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_adm.admin_id = ccs_oct.createdby order by ccs_oct.category_id ASC','oct, obm, adm');
								$data['cat_count'] = count($data['category']);
								$view = $this->load->view('custom_hanaview', $data, TRUE);
							}elseif($duum == 3){
								$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_adm.admin_id = ccs_oct.createdby','oct, adm');
								$data['cat_count'] = count($data['category']);
								$view = $this->load->view('custom_kadaview', $data, TRUE);
							}
						}elseif($virato == 1 ){
							if($duum == 1){
								$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.status, ccs_oct.datecreated, ccs_oct.description, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.object_id, ccs_adm.name','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' and ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_adm.admin_id = ccs_oct.createdby order by ccs_oct.category_id ASC','oct, obm, adm');
								$data['cat_count'] = count($data['category']);
								$view = $this->load->view('custom_caraview', $data, TRUE);
							}elseif($duum == 3){
								$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_adm.admin_id = ccs_oct.createdby','oct, adm');
								$data['cat_count'] = count($data['category']);
								$view = $this->load->view('custom_tasaview', $data, TRUE);
							}
						}
					}
					elseif($part == 'detail_category'){
						$id = $this->input->post('id');
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
						if($virato == 0){							
							if($duum == 0){
								$data['category'] = $this->dbfun_model->get_data_bys('ccs_oct.metakeyword, ccs_oct.title, ccs_oct.status, ccs_oct.datecreated, ccs_oct.description, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.object_id, ccs_adm.name','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' and ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_adm.admin_id = ccs_oct.createdby and ccs_obm.object_id = '.$id.'','oct, obm, adm');
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
								$data['child'] = $this->dbfun_model->get_all_data_bys('object_id, title, category_id','ccs_id = '.$fascia.' and ccs_key = '.$lokasi.' and category_id = '.$id.' and language_id = '.$lingua.' ','odc');
								$view = $this->load->view('custom_hanadetail', $data, TRUE);
							}elseif($duum == 3){
								$data['id'] = $id;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								
								$data['categories'] = $this->dbfun_model->get_data_bys('*','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.category_id = '.$id.' ','oct'); 
								$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_odc.*, ccs_adm.name, ccs_oct.title as tahun','ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.ccs_key = '.$inspeksi.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.category_id = '.$id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_key = '.$jadwal.' and ccs_odc.metadescription = "'.$anchor.'"','odc, adm, oct');
								$data['c_objects'] = count($data['objects']);
								$view = $this->load->view('custom_kadadetail', $data, TRUE);
							}
						}elseif($virato == 1){				
							if($duum == 1){
								$data['category'] = $this->dbfun_model->get_data_bys('ccs_oct.metakeyword, ccs_oct.title, ccs_oct.status, ccs_oct.datecreated, ccs_oct.description, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.object_id, ccs_adm.name','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' and ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_adm.admin_id = ccs_oct.createdby and ccs_obm.object_id = '.$id.'','oct, obm, adm');
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
								$data['child'] = $this->dbfun_model->get_all_data_bys('object_id, title, category_id','ccs_id = '.$fascia.' and ccs_key = '.$lokasi.' and category_id = '.$id.' and language_id = '.$lingua.' ','odc');
								$view = $this->load->view('custom_caradetail', $data, TRUE);
							}elseif($duum == 3){
								$data['id'] = $id;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								
								$data['categories'] = $this->dbfun_model->get_data_bys('*','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.category_id = '.$id.' ','oct'); 
								$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_odc.*, ccs_adm.name, ccs_oct.title as tahun','ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.ccs_key = '.$inspeksi.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.category_id = '.$id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_key = '.$jadwal.' and ccs_odc.metadescription = "'.$anchor.'"','odc, adm, oct');
								$data['c_objects'] = count($data['objects']);
								$view = $this->load->view('custom_tasadetail', $data, TRUE);
							}
						}
					}
					elseif($part == 'new'){
						
						$data['id'] = $id;
						$data['obcultus'] = $inspeksi;
						$data['anchor'] = $anchor;
						if(($simo == 0)&&($virato == 0)){
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
						
							if($duum == 3){
								$data['kota'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.category_id, ccs_oct.ccs_key, ccs_oct.metakeyword','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.parent_id = 0 and ccs_obm.object_id = ccs_oct.category_id and ccs_obm.category_id = '.$segretis.' and ccs_obm.url_id =  ccs_oct.ccs_key','oct, obm');
								
								if($id == 'all'){	
									
									$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_adm.admin_id = ccs_oct.createdby','oct, adm');
									$view = $this->load->view('custom_kadanew', $data, TRUE);
								}elseif($id != 'all'){	
									$data['category'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_oct.title as category, ccs_oct.metakeyword as code, ccs_oct.datecreated as datestart, ccs_oct.dateupdated as dateend','ccs_odc.ccs_id = '.$fascia.' and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.object_id = '.$id.' and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_odc.language_id = ccs_oct.language_id and ccs_oct.createdby = ccs_adm.admin_id and ccs_oct.ccs_id = ccs_adm.ccs_id','odc, oct, adm'); 
									$view = $this->load->view('custom_kadanewo', $data, TRUE);
								}
								
							}
						}elseif(($simo == 0)&&($virato == 1)){
							if($duum == 3){
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								
								$data['kota'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.category_id, ccs_oct.ccs_key, ccs_oct.metakeyword','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.parent_id = 0 and ccs_obm.object_id = ccs_oct.category_id and ccs_obm.category_id = '.$segretis.' and ccs_obm.url_id =  ccs_oct.ccs_key','oct, obm');
								
								$data['tahun'] = $this->dbfun_model->get_all_data_bys('title, category_id, datecreated','ccs_id = '.$fascia.' and ccs_key = '.$jadwal.' and language_id = '.$lingua.' order by datecreated desc','oct');
								if($id == 'all'){		
									
									$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_adm.admin_id = ccs_oct.createdby','oct, adm');
									$view = $this->load->view('custom_tasanew', $data, TRUE);
								}elseif($id != 'all'){	
									$data['kota'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.category_id, ccs_oct.ccs_key, ccs_oct.metakeyword','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.parent_id = 0 and ccs_obm.object_id = ccs_oct.category_id and ccs_obm.category_id = '.$segretis.' and ccs_obm.url_id =  ccs_oct.ccs_key','oct, obm');
								
									$data['category'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_oct.title as category, ccs_oct.metakeyword as code, ccs_oct.datecreated as datestart, ccs_oct.dateupdated as dateend','ccs_odc.ccs_id = '.$fascia.' and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.object_id = '.$id.' and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_odc.language_id = ccs_oct.language_id and ccs_oct.createdby = ccs_adm.admin_id and ccs_oct.ccs_id = ccs_adm.ccs_id','odc, oct, adm'); 
									$view = $this->load->view('custom_tasanew', $data, TRUE);
								}
								
							}elseif($duum == 1){
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
						
								$data['segretis'] = $segretis;
								if($id == 'all'){
									$data['id'] = $id;
									$data['categories'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.category_id as kategori','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' order by ccs_oct.category_id ASC','oct, obm');								
									$view = $this->load->view('custom_caranew', $data, TRUE);
									
								}else{
									
									$data['id'] = $id;
									$data['categories'] = $this->dbfun_model->get_data_bys('ccs_oct.title, ccs_oct.metakeyword, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key, ccs_obm.category_id as kategori','ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.'  and ccs_oct.category_id = '.$id.'','oct, obm');		
									$view = $this->load->view('custom_caranewo', $data, TRUE);
								}
								
							}
						}
					}
					elseif($part == 'detail_object'){
						$id = $this->input->post('id');	
						if($virato == 0){							
							if($duum == 3){
								$data['id'] = $id;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								
								$data['categories'] = $this->dbfun_model->get_data_bys('ccs_odc.* , ccs_adm.name, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_oct.title as tahun','ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.createdby = ccs_adm.admin_id and ccs_odc.object_id = '.$id.' and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.object_id = ccs_o.object_id and ccs_odc.ccs_id = ccs_o.ccs_id and ccs_o.ccs_key = ccs_o.ccs_key and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.category_id = ccs_odc.category_id','o, odc, adm, oct');
								$view = $this->load->view('custom_kadadetailo', $data, TRUE);
							}elseif($duum == 6){
								$obj = $this->dbfun_model->get_all_data_bys('ccs_key,url_id,type,datecreated','ccs_id = '.$this->zone_id.' and user_id = '.$this->input->post('id').' group by ccs_key,url_id,type order by datecreated DESC LIMIT 5','ccs_ulg');
								foreach($obj as $o){
									$ccs_key = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and module_id = '.$o->ccs_key.'','umd');
									if(!empty($ccs_key)){
										$module = (object) array_merge((array) $o,array('module' => $ccs_key->name));
										$oo = $this->dbfun_model->get_data_bys('title,category_id','ccs_id = '.$this->zone_id.' and object_id = '.$o->url_id.' and ccs_key = '.$o->ccs_key.'','odc');
										if(!empty($oo)){
											$cat = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and category_id = '.$oo->category_id.' and ccs_key = '.$o->ccs_key.'','oct');
											$oo2 = (object) array_merge((array) $module,array('title' => $oo->title));
											$objects[] = (object) array_merge((array) $oo2,array('category' => $cat->title));
										}else{
											$objects[] = $module;
										}
									}else{
										if(!empty($o->url_id)){
											$objects[] = (object) array_merge((array) $o,array('module' => 'login'));
										}else{
											$objects[] = (object) array_merge((array) $o,array('module' => 'logout'));
										}
									}
								}
								$data['objects'] = $objects;
								$data['profile'] = $this->dbfun_model->get_data_bys('ccs_usr.*, ccs_umr.name as type','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.user_id = '.$this->input->post('id').' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.type_id = ccs_umr.type_id','usr,umr');
								$view = $this->load->view('custom_padadetail', $data, TRUE);
							}
						}elseif($virato == 1){							
							if($duum == 3){
								$data['id'] = $id;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
								echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
								$data['objects']= $this->dbfun_model->get_data_bys('ccs_obr.obr_id, ccs_obr.ccs_id, ccs_obr.ccs_key, ccs_obr.type_id, ccs_obr.category_id, ccs_obr.object_id, ccs_obr.area_id, ccs_obr.utl_id, ccs_obr.title, ccs_obr.parole, ccs_obr.description, ccs_obr.chiave, ccs_obr.metakeyword, ccs_obr.metadescription, ccs_obr.datestart, ccs_obr.dateend, ccs_obr.datecreated, ccs_obr.dateupdated, ccs_obr.status, ccs_usr.name, ccs_oct.title as tahun,  ccs_odc.dateupdated as lastdate, ccs_odc.status as last_status, ccs_odc.tagline','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.metakeyword = "'.$anchor.'" and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jembatan.' and ccs_obr.obr_id ='.$id.' and ccs_odc.object_id = ccs_obr.utl_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = '.$jadwal.'  ','obr, usr, odc, oct');
								$objects = $data['objects'];
								$utl = $objects->utl_id;
								$lst = $this->dbfun_model->get_data_bys('object_id','object_id = '.$utl.' and ccs_key = '.$inspeksi.' and ccs_id = '.$fascia.' ',' olg');
								
								$data['cl'] = count($lst);
								if($data['cl'] != 0){
									$list = $lst->object_id;
									$data['list'] = $this->dbfun_model->get_all_data_bys('ccs_olg.image_square, ccs_olg.log_id, ccs_obr.obr_id, ccs_obr.type_id, ccs_obr.title, ccs_obr.chiave, ccs_obr.parole, ccs_obr.datestart, ccs_obr.dateend, ccs_obr.category_id, ccs_obr.object_id, ccs_oct.title as tahun, ccs_usr.name, ccs_obr.area_id, ccs_obr.utl_id, ccs_obr.metakeyword, ccs_obr.metadescription, ccs_olg.status, ccs_olg.i_status','ccs_obr.obr_id = ccs_olg.url_id and ccs_obr.ccs_id = ccs_olg.ccs_id and ccs_obr.ccs_key = ccs_olg.ccs_key and ccs_obr.ccs_id = '.$fascia.' and ccs_obr.ccs_key ='.$inspeksi.' and ccs_obr.utl_id = ccs_olg.object_id and ccs_olg.object_id = '.$list.' and ccs_usr.ccs_id = ccs_olg.ccs_id and ccs_usr.user_id = ccs_olg.createdby and ccs_oct.ccs_id = ccs_obr.ccs_id and ccs_oct.ccs_key = '.$jadwal.' and ccs_obr.type_id = ccs_oct.category_id and ccs_olg.type = "inspeksi" order by ccs_obr.datestart ASC','olg, obr, usr, oct');
								}
								$view = $this->load->view('custom_tasadetailo', $data, TRUE);
							}elseif($duum == 1){
								$data['id'] = $id;	
								$lang = $this->input->post('language_id');
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
								$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.status, ccs_odc.tagline, ccs_odc.object_id, ccs_odc.love, ccs_odc.title, ccs_odc.description, ccs_odc.metakeyword, ccs_odc.metadescription, ccs_o.image_square, ccs_odc.dateupdated as lastdate, ccs_odc.datecreated, ccs_odc.datecreated, ccs_oct.title as area, ccs_usr.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_usr.ccs_id and ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.createdby = ccs_usr.user_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_odc.ccs_key = '.$jembatan.' and ccs_odc.love = ccs_oct.category_id and ccs_oct.ccs_key = '.$lokasi.' and ccs_odc.metakeyword = "'.$anchor.'" and ccs_o.object_id = ccs_odc.object_id and ccs_oct.language_id = ccs_odc.language_id and ccs_oct.language_id = 2 and ccs_odc.object_id = '.$id.' ','oct, o, odc, usr');
								if($lang == 2){
									$data['list'] = $this->dbfun_model->get_data_bys('chiave, obr_id, title, datestart, dateend','ccs_id = '.$fascia.' and ccs_key = '.$inspeksi.' and utl_id = '.$id.' order by datecreated desc','obr');
									$view = $this->load->view('custom_caradetailo', $data, TRUE);
								}elseif($lang == 1){
									echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
									$data['istatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
									$list = $this->dbfun_model->get_all_data_bys('ccs_oct.title as tahun, ccs_obr.type_id, ccs_obr.obr_id, ccs_obr.chiave, ccs_obr.title, ccs_obr.datestart, ccs_obr.dateend, ccs_olg.datecreated, ccs_usr.name, ccs_olg.log_id, ccs_olg.object_id, ccs_olg.title, ccs_olg.status, ccs_olg.i_status, ccs_obr.status as b_status','ccs_obr.ccs_id = ccs_olg.ccs_id and ccs_olg.ccs_id = ccs_usr.ccs_id and ccs_usr.user_id = ccs_obr.createdby and ccs_usr.ccs_id = '.$fascia.' and ccs_olg.ccs_key = ccs_obr.ccs_key and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.utl_id = ccs_olg.object_id and ccs_obr.utl_id = '.$id.' and ccs_obr.obr_id = ccs_olg.url_id and ccs_olg.parent_id = 0 and ccs_oct.ccs_id = ccs_obr.ccs_id and ccs_oct.category_id = ccs_obr.type_id and ccs_oct.ccs_key = '.$jadwal.' order by ccs_obr.datecreated asc ','obr, olg, usr, oct');
									$cl = count($list);
									if($cl != 0){
										$data['list'] = $list;
									}
									$data['cl'] = $cl;
									
									
									$view = $this->load->view('custom_caradetailol', $data, TRUE);
								}elseif($lang == 2){
								}							
							}
						}
					}
					elseif($part == 'edit_object')
					{
						if($virato == 0){
							if($duum == 6){
								$data['role'] = $this->dbfun_model->get_all_data_bys('role_id, name,status','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and role_id != '.$this->user_role.'','umr');
								$data['profile'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','usr');
								//print_r($data['profile']);
								$view = $this->load->view('custom_padaedito', $data, TRUE);	
							}
						}
					}
					elseif($part == 'detail_custom'){
						$id = $this->input->post('id');
						//echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
						if($virato == 0){							
							if($duum == 3){
								$data['id'] = $id;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
								
								$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend , ccs_adm.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_odc.ccs_key = '.$inspeksi.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.createdby = ccs_adm.admin_id and ccs_odc.object_id = ccs_o.object_id and ccs_odc.object_id = '.$id.'','o, odc, adm'); 
								$categories = $data['objects']->category_id;	
								$love = $data['objects']->love;
								$data['category'] = $this->dbfun_model->get_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$jadwal.' and ccs_oct.language_id = '.$lingua.' and ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_adm.admin_id = ccs_oct.createdby and ccs_odc.ccs_id = ccs_oct.ccs_key and ccs_oct.category_id = ccs_odc.category_id and ccs_oct.category_id = '.$categories.' and ccs_oct.language_id = ccs_odc.language_id and ccs_odc.object_id = '.$love.'','oct, adm, odc');
								$view = $this->load->view('custom_kadadetailor', $data, TRUE);
							}
						}elseif($virato == 1){							
							if($duum == 3){
								$data['id'] = $id;
								$data['zone'] =  $this->zone;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
								echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
								$data['objects'] = $this->dbfun_model->get_data_bys('ccs_olg.*, ccs_o.dateupdated as lastdate, ccs_odc.status as last_status, ccs_odc.title as jembatan, ccs_odc.metadescription, ccs_olg.log_id, ccs_olg.url_id, ccs_usr.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_olg.ccs_id and ccs_odc.ccs_id = ccs_usr.ccs_id and ccs_olg.createdby = ccs_usr.user_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = '.$jembatan.' and ccs_odc.ccs_id = '.$fascia.' and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = ccs_olg.object_id and ccs_olg.log_id= '.$id.'','odc, o, olg, usr');
								$objects = $data['objects'];
								$url = $objects->url_id;
								$o = $objects->object_id;
								$data['inspeksi'] = $this->dbfun_model->get_data_bys('ccs_obr.*, ccs_oct.title as tahun','ccs_obr.ccs_id = '.$fascia.' and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.obr_id = '.$url.' and ccs_oct.ccs_id = ccs_obr.ccs_id and ccs_oct.ccs_key = '.$jadwal.' and ccs_obr.type_id = ccs_oct.category_id','obr, oct');
								$data['list']= $this->dbfun_model->get_data_bys('*','ccs_ori.ccs_id = ccs_orc.ccs_id and ccs_ori.ccs_id = ccs_ord.ccs_id and ccs_ori.ccs_id = '.$fascia.' and ccs_ori.ccs_key = ccs_orc.ccs_key and ccs_ori.ccs_key = ccs_ord.ccs_key and ccs_ori.ccs_key = '.$inspeksi.' and ccs_ori.url_id = ccs_orc.url_id and ccs_ori.url_id = ccs_ord.url_id and ccs_ori.url_id = '.$id.' and ccs_ori.object_id =  ccs_ord.object_id and ccs_ori.object_id = ccs_orc.object_id and ccs_ori.object_id = '.$o.'','ori, orc, ord');
								$view = $this->load->view('custom_tasadetailoc', $data, TRUE);
							}
						}
					}
					elseif($part == 'gallery_custom'){
						$id = $this->input->post('id');
						if($virato == 1){							
							if($duum == 3){
								$data['id'] = $id;
								$data['zone'] =  $this->zone;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
								$data['gallery'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$inspeksi.' and object_id = '.$id.'','ogl');
								
								$data['objects'] = $this->dbfun_model->get_data_bys('ccs_olg.*, ccs_o.dateupdated as lastdate, ccs_odc.status as last_status, ccs_odc.title as jembatan, ccs_odc.metadescription, ccs_olg.log_id, ccs_olg.url_id, ccs_usr.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_olg.ccs_id and ccs_odc.ccs_id = ccs_usr.ccs_id and ccs_olg.createdby = ccs_usr.user_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = '.$jembatan.' and ccs_odc.ccs_id = '.$fascia.' and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = ccs_olg.object_id and ccs_olg.log_id= '.$id.'','odc, o, olg, usr');
								$objects = $data['objects'];
								$url = $objects->url_id;
								$o = $objects->object_id;
								$data['inspeksi'] = $this->dbfun_model->get_data_bys('ccs_obr.*, ccs_oct.title as tahun','ccs_obr.ccs_id = '.$fascia.' and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.obr_id = '.$url.' and ccs_oct.ccs_id = ccs_obr.ccs_id and ccs_oct.ccs_key = '.$jadwal.' and ccs_obr.type_id = ccs_oct.category_id','obr, oct');
								$data['list']= $this->dbfun_model->get_data_bys('*','ccs_ori.ccs_id = ccs_orc.ccs_id and ccs_ori.ccs_id = ccs_ord.ccs_id and ccs_ori.ccs_id = '.$fascia.' and ccs_ori.ccs_key = ccs_orc.ccs_key and ccs_ori.ccs_key = ccs_ord.ccs_key and ccs_ori.ccs_key = '.$inspeksi.' and ccs_ori.url_id = ccs_orc.url_id and ccs_ori.url_id = ccs_ord.url_id and ccs_ori.url_id = '.$id.' and ccs_ori.object_id =  ccs_ord.object_id and ccs_ori.object_id = ccs_orc.object_id and ccs_ori.object_id = '.$o.'','ori, orc, ord');
								$view = $this->load->view('custom_tasagal', $data, TRUE);
							}
						}
					}
					elseif($part == 'edit_custom'){
						if($virato == 1){
							if($duum == 3){								
								$data['id'] = $id;
								$data['zone'] =  $this->zone;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
								$data['objects'] = $this->dbfun_model->get_data_bys('ccs_olg.*, ccs_odc.title as jembatan, ccs_o.dateupdated as lastdate, ccs_odc.status as last_status, ccs_odc.title as jembatan, ccs_odc.metadescription, ccs_olg.log_id, ccs_olg.url_id, ccs_usr.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_olg.ccs_id and ccs_odc.ccs_id = ccs_usr.ccs_id and ccs_olg.createdby = ccs_usr.user_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = '.$jembatan.' and ccs_odc.ccs_id = '.$fascia.' and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = ccs_olg.object_id and ccs_olg.log_id= '.$id.'','odc, o, olg, usr');
								$objects = $data['objects'];
								$url = $objects->url_id;
								$o = $objects->object_id;
								$data['inspeksi'] = $this->dbfun_model->get_data_bys('ccs_obr.*, ccs_oct.title as tahun','ccs_obr.ccs_id = '.$fascia.' and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.obr_id = '.$url.' and ccs_oct.ccs_id = ccs_obr.ccs_id and ccs_oct.ccs_key = '.$jadwal.' and ccs_obr.type_id = ccs_oct.category_id','obr, oct');
								$data['list']= $this->dbfun_model->get_data_bys('*','ccs_ori.ccs_id = ccs_orc.ccs_id and ccs_ori.ccs_id = ccs_ord.ccs_id and ccs_ori.ccs_id = '.$fascia.' and ccs_ori.ccs_key = ccs_orc.ccs_key and ccs_ori.ccs_key = ccs_ord.ccs_key and ccs_ori.ccs_key = '.$inspeksi.' and ccs_ori.url_id = ccs_orc.url_id and ccs_ori.url_id = ccs_ord.url_id and ccs_ori.url_id = '.$id.' and ccs_ori.object_id =  ccs_ord.object_id and ccs_ori.object_id = ccs_orc.object_id and ccs_ori.object_id = '.$o.'','ori, orc, ord');
							
								$view = $this->load->view('custom_tasaedit', $data, TRUE);							
							}
						}
					}
					elseif($part == 'view_custom'){						
						if($virato == 1){
							if($duum == 3){								
								$data['id'] = $id;
								$data['zone'] =  $this->zone;
								$data['obcultus'] = $inspeksi;
								$data['lingua'] = $lingua;
								$data['anchor'] = $anchor;
								$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
								$data['objects'] = $this->dbfun_model->get_data_bys('ccs_olg.*, ccs_odc.title as jembatan, ccs_o.dateupdated as lastdate, ccs_odc.status as last_status, ccs_odc.title as jembatan, ccs_odc.metadescription, ccs_olg.log_id, ccs_olg.url_id, ccs_usr.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = ccs_olg.ccs_id and ccs_odc.ccs_id = ccs_usr.ccs_id and ccs_olg.createdby = ccs_usr.user_id and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = '.$jembatan.' and ccs_odc.ccs_id = '.$fascia.' and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = ccs_olg.object_id and ccs_olg.log_id= '.$id.'','odc, o, olg, usr');
								$objects = $data['objects'];
								$url = $objects->url_id;
								$o = $objects->object_id;
								$data['inspeksi'] = $this->dbfun_model->get_data_bys('ccs_obr.*, ccs_oct.title as tahun','ccs_obr.ccs_id = '.$fascia.' and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.obr_id = '.$url.' and ccs_oct.ccs_id = ccs_obr.ccs_id and ccs_oct.ccs_key = '.$jadwal.' and ccs_obr.type_id = ccs_oct.category_id','obr, oct');
								if($lingua == 1){
									$view = $this->load->view('custom_tasanewd', $data, TRUE);			
								}elseif($lingua == 2){		
									$data['reason'] = $this->dbfun_model->get_data_bys('ccs_olg.*, ccs_usr.name','ccs_olg.ccs_id = ccs_usr.ccs_id and ccs_olg.createdby = ccs_usr.user_id and ccs_olg.ccs_key = '.$inspeksi.' and ccs_olg.ccs_id = '.$fascia.' and ccs_olg.parent_id= '.$id.'','olg, usr');
									$view = $this->load->view('custom_tasanewr', $data, TRUE);			
								}			
							}
						}
					}
					elseif($part == 'new_custom'){						
						if($virato == 1){
							if($duum == 3){
								if($id == 'all'){
									$data['id'] = $id;
									$data['essentiels'] = $inspeksi;
									$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
									$data['kota'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.category_id, ccs_oct.ccs_key, ccs_oct.metakeyword','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_key = '.$lokasi.' and ccs_oct.parent_id = 0 and ccs_obm.object_id = ccs_oct.category_id and ccs_obm.category_id = '.$segretis.' and ccs_obm.url_id =  ccs_oct.ccs_key','oct, obm');
								
									$data['tahun'] = $this->dbfun_model->get_all_data_bys('title, category_id, datecreated','ccs_id = '.$fascia.' and ccs_key = '.$jadwal.' and language_id = '.$lingua.' order by datecreated desc','oct');
							
									
									$view = $this->load->view('custom_tasanewo', $data, TRUE);
								}else{
									$data['id'] = $id;
									$data['essentiels'] = $inspeksi;
									$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$fascia.' and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
									$data['objects']= $this->dbfun_model->get_data_bys('ccs_obr.obr_id, ccs_obr.ccs_id, ccs_obr.ccs_key, ccs_obr.type_id, ccs_obr.category_id, ccs_obr.object_id, ccs_obr.area_id, ccs_obr.utl_id, ccs_obr.title, ccs_obr.parole, ccs_obr.chiave, ccs_obr.metakeyword, ccs_obr.metadescription, ccs_obr.datestart, ccs_obr.dateend, ccs_obr.datecreated, ccs_obr.dateupdated, ccs_obr.status, ccs_usr.name, ccs_oct.title as tahun,  ccs_odc.dateupdated as lastdate, ccs_odc.status as last_status, ccs_odc.tagline','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.metakeyword = "'.$anchor.'" and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jembatan.' and ccs_obr.obr_id ='.$id.' and ccs_odc.object_id = ccs_obr.utl_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = '.$jadwal.' ','obr, usr, odc, oct');
								
									$view = $this->load->view('custom_tasanewoc', $data, TRUE);
								}
								
							}
						}
					}
					elseif($part == 'list_url'){
						$id = $this->input->post('id');
						//echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
						if($virato == 1){							
							if($duum == 3){
								
								$key = $this->input->post('url');								
								if($key == 'jembatan'){
									$url = $this->dbfun_model->get_all_data_bys('title, object_id as url_id ','ccs_key = '.$jembatan.' and ccs_id = '.$fascia.' and love = '.$id.' and metakeyword = "'.$anchor.'"','odc');
										echo '<option value="">Pilih Jembatan</option>
											<option value="0">Semua Jembatan</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'">'.$url->title.'</option>';
									}
								}elseif($key == 'tahun'){
									$url = $this->dbfun_model->get_all_data_bys('ccs_odc.love, ccs_odc.title, ccs_odc.tagline, ccs_odc.object_id as url_id','ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.ccs_key = '.$inspeksi.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.metadescription = "'.$anchor.'" and ccs_odc.category_id = '.$id.'','odc, adm');
									//$url = $this->dbfun_model->get_all_data_bys('title, object_id as url_id ','ccs_key = '.$jembatan.' and ccs_id = '.$fascia.' and love = '.$id.' and metakeyword = "'.$anchor.'"','odc');
										echo '<option value="">Pilih Kuartal</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'" data-param="'.$url->tagline.'" data-object="'.$url->love.'">'.$url->title.'</option>';
									}
								}elseif($key == 'inspeksi'){
									$url = $this->dbfun_model->get_all_data_bys('title, object_id as url_id ','ccs_key = '.$jembatan.' and ccs_id = '.$fascia.' and love = '.$id.' and metakeyword = "'.$anchor.'"','odc');
										echo '<option value="">Pilih Jembatan</option>
											';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'">'.$url->title.'</option>';
									}
								}
								$view = '';
							}
							
						}
					}
					elseif($part == 'user'){
						$lingua = $this->input->post('language_id');
						if($virato == 3){
							if($duum == 0){//profile
								if($lingua == 0){//edit profile
									$obj = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','ccs_usr');
									$role = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$obj->type_id.'','umr');
									$objects = (object) array_merge((array) $obj,array('role' => $role->name));
									
									$data['objects'] = $objects;
									$data['zone'] = $this->zone;
									$view = $this->load->view('custom_walaedit', $data, TRUE);	
								}elseif($lingua == 1){//create
									$data['role'] = $this->dbfun_model->get_all_data_bys('role_id, name,status','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and role_id != '.$this->user_role.'','umr');
									$view = $this->load->view('custom_walacreate', $data, TRUE);	
								}
							}elseif($duum == 6){//user
							
							}
						}elseif(($simo == 0) && ($virato == 0)){
							if($duum == 6){//role	
								if($lingua == 0){//list
									echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
									$category = $this->dbfun_model->get_all_data_bys('role_id, name,status','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and role_id != '.$this->user_role.' and rt_id = '.$segretis.'','umr');
									$datas2 = array();
									foreach($category as $c){
										$dt = $this->dbfun_model->get_all_data_bys('ccs_umd.module_id, ccs_umd.name','ccs_umt.ccs_id = '.$this->zone_id.' and ccs_umt.role_id = '.$c->role_id.' and ccs_umt.ccs_id = ccs_umd.ccs_id and ccs_umt.module_id = ccs_umd.module_id and ccs_umd.parent_id = 0','umt,umd');
										$datas2[] = array('role' => $c, 'module' => $dt);
									}
									$data['category'] = $datas2;
									$data['cat_count'] = count($category);
									$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 9 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
									$view = $this->load->view('custom_padaview', $data, TRUE);		
								}elseif(in_array($lingua,array('1','2'),true)){//create & edit role
									echo '<link href="http://buzcon.com/project//assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet" type="text/css">';
									echo '<script src="http://buzcon.com/project//assets/admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>';
									echo '<script>
									$(".i-checks").iCheck({
										checkboxClass: "icheckbox_square-green",
										radioClass: "iradio_square-green",
									});</script>';
									$module = $this->dbfun_model->get_all_data_bys('name, icon, module_id, parent_id, description','parent_id = 0 and ccs_id = '.$this->zone_id.' and tu_id = '.$this->user_type.' and status = 1 order by name ASC','umd');
									$module2 = $this->dbfun_model->get_all_data_bys('name, icon, module_id, parent_id, description','parent_id = 0 and ccs_id = '.$this->zone_id.' and tu_id = 2 and status = 1 order by name ASC','umd');
									if($lingua == 1){//create role
										$data['category'] = $module;
										$data['category2'] = $module2;
										$data['umr'] = $this->dbfun_model->get_all_data_bys('role_id,name','ccs_id = '.$this->zone_id.' and rt_id = 0 and status = 1','umr');
										$view = $this->load->view('custom_padacreate', $data, TRUE);
									}elseif($lingua == 2){//edit role
										$data['module'] = $module;
										$category = $this->dbfun_model->get_all_data_bys('role_id,type_id, name,description, status','ccs_id = '.$this->zone_id.' and role_id = '.$id.' and status = 1','umr');
										foreach($category as $c){
											$dt = $this->dbfun_model->get_all_data_bys('ccs_umd.module_id, ccs_umd.name,ccs_umd.icon','ccs_umt.ccs_id = '.$this->zone_id.' and ccs_umt.role_id = '.$id.' and ccs_umt.ccs_id = ccs_umd.ccs_id and ccs_umt.module_id = ccs_umd.module_id and ccs_umd.parent_id = 0 and ccs_umd.status = 1','umt,umd');
											$datas2[] = array('role' => $c, 'module' => $dt);
										}
										$data['category'] = $datas2;
										$view = $this->load->view('custom_padaedit', $data, TRUE);	
									}
								}
							}
						}
					}
				}
				else
				{
					$data['lingua'] = $lingua;
					$data['essentiel_id'] = $essentiel_id;
					$data['essentiel'] = $essentiel;
					$data['sigilo'] = $sigilo;
					$data['fascia'] = $fascia;
					$data['fattore'] = $this->user_id;
					$segretis = $this->segretis;
					$fattore = $this->user_id;
					$simo = $this->simo;
					
					//custom for $chiave 2
					if($chiave == 2){
						switch($sigilo){
							case 'org':
							case 'youth':
								switch($part){
									case 'list':
										$cek_log = $this->dbfun_model->get_all_data_bys('distinct(url_id), ccs_key, user_id, language_id,datecreated as created, type','ccs_id = '.$this->zone_id.' and ccs_key = 1 and user_id = '.$this->user_id.' order by datecreated DESC','ulg');
										if(!empty($cek_log)){
											foreach($cek_log as $c){
												$ds = $this->dbfun_model->get_data_bys('ccs_usr.user_id,ccs_usr.name,ccs_usr.avatar,ccs_usr.address,ccs_usr.mobile,ccs_usr.phone,ccs_usr.email,ccs_usr.description,ccs_umr.name as type','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.type_id = ccs_umr.type_id and ccs_usr.user_id = '.$c->url_id.'','usr,umr');
												if(!empty($ds)){
													$datas[] = $ds;
												}
											}
											if(isset($datas)){
												$c_q = $datas;
											}else{
												$c_q = '';
											}
										}else{
											$c_q = '';
										}
										$c_v = 'community/connection.php';
										break;
									case 'sidebar':
										$cek_log = $this->dbfun_model->get_all_data_bys('distinct(url_id), ccs_key, user_id, language_id,datecreated as created, type','ccs_id = '.$this->zone_id.' and ccs_key = 1 and user_id = '.$this->user_id.' order by datecreated DESC','ulg');
										foreach($cek_log as $c){
											$u_exist[] = $c->url_id;
										};
										$u_exist[] = $this->user_id;
										$c_q = $this->dbfun_model->get_all_data_bys('ccs_usr.user_id,ccs_usr.type_id,ccs_usr.name,ccs_usr.avatar,ccs_umr.name as type','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.type_id = ccs_umr.type_id and ccs_usr.user_id not in ('.implode(', ', $u_exist).')','usr,umr');
										$c_v = 'community/user_all.php';
										break;
								}
								break;
							case 'development':	
							case 'ideas':
								switch($part){
									case 'sidebar':
										$c_v = 'community/sidebar.php';
										break;
									case 'list':
										echo '<script src="'.base_url('assets/admin/js/plugins/summernote/summernote.min.js').'" type="text/javascript"></script>';
										echo '<script src="'.base_url('assets/admin/js/plugins/fileinput/fileinput.min.js').'" type="text/javascript"></script>';
										echo '<script src="'.base_url('assets/admin/js/ajaxfileupload.js').'" type="text/javascript"></script>';
										$c_add = array(',ccs_odc.category_id,ccs_odc.dateupdated,ccs_odc.createdby,ccs_usr.name as creator,ccs_usr.avatar,ccs_umr.name','and ccs_usr.user_id = ccs_odc.createdby and ccs_usr.type_id = ccs_umr.type_id and ccs_odc.ccs_id = ccs_umr.ccs_id order by ccs_odc.dateupdated DESC',',usr,umr');
										$c_v = 'community/list-block.php';
										break;
									case 'detail':
										$c_v = 'community/detail.php';
										break;
									case 'new':
										echo '<link href="http://buzcon.com/project/assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">';
										echo '<link href="http://buzcon.com/project/assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">';
										echo '<link href="http://buzcon.com/project/assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">';
										$c_v = 'community/new.php';
										break;
								}
								break;
						}
					}
					//end custom for $chiave 2
					
					
					if(in_array($sigilo,array('profile','profile','role','roles'))){//primary
						switch($part){
							case 'category':
								switch($virato){
									case '0':
										if(($duum == 6) && ($simo == 0)){//role
											$category = $this->dbfun_model->get_all_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and rt_id = 0 order by name ASC','umr');
											foreach($category as $c){
												$count = $this->dbfun_model->count_result('ccs_id = '.$this->zone_id.' and type_id = '.$c->type_id.' and rt_id = '.$segretis.'','usr');
												$datas[] = (object) array_merge((array) $c,array('total' => $count));
											}
											$data['category'] = $datas;
											$data['cat_count'] = count($data['category']);
											$view = $this->load->view('custom_padacat', $data, TRUE);	
										}
										break;
									case '3':
										if($duum == 0){//profile
											$category = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$this->user_id.'','usr');
											$data['profile'] = $category;
											$data['zone'] = $this->zone;
											/* print_r($category); */
											$view = $this->load->view('custom_wala', $data, TRUE);
										}	
										break;
								}break;
							case 'list':
								switch($virato){
									case '0':
										$data['id'] = $this->input->post('id');
										if(($duum == 6) && ($simo == 0)){ //role
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
											if($id == 'all'){
												$obj = $this->dbfun_model->get_all_data_bys('ccs_usr.user_id,ccs_umr.type_id,ccs_usr.name,ccs_umr.name as type, ccs_usr.status,ccs_usr.createdby','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.role_id = ccs_umr.role_id and ccs_usr.rt_id = '.$segretis.'','usr,umr');
											}else{
												$obj = $this->dbfun_model->get_all_data_bys('ccs_usr.user_id,ccs_umr.type_id,ccs_usr.name,ccs_umr.name as type, ccs_usr.status,ccs_usr.createdby','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.role_id = ccs_umr.role_id and ccs_usr.rt_id = '.$segretis.' and ccs_umr.type_id = '.$id.'','usr,umr');
												$data['role'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$id.'','umr');
											}
											$data['objects'] = $obj;
											$data['c_objects'] = count($obj);
											$data['user_id'] = $this->user_id;
											$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 9 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
											$view = $this->load->view('custom_padalist', $data, TRUE);	
										}
										break;
									case '3':
										if($duum == 0){//log					
											$obj = $this->dbfun_model->get_all_data_bys('ccs_key,url_id,type,datecreated','ccs_id = '.$this->zone_id.' and user_id = '.$this->user_id.' group by ccs_key,url_id,type order by datecreated DESC LIMIT 5','ccs_ulg');
											foreach($obj as $o){
												$ccs_key = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and module_id = '.$o->ccs_key.'','umd');
												if(!empty($ccs_key)){
													$module = (object) array_merge((array) $o,array('module' => $ccs_key->name));
													$oo = $this->dbfun_model->get_data_bys('title,category_id','ccs_id = '.$this->zone_id.' and object_id = '.$o->url_id.' and ccs_key = '.$o->ccs_key.'','odc');
													if(!empty($oo)){
														$cat = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and category_id = '.$oo->category_id.' and ccs_key = '.$o->ccs_key.'','oct');
														$oo2 = (object) array_merge((array) $module,array('title' => $oo->title));
														$objects[] = (object) array_merge((array) $oo2,array('category' => $cat->title));
													}else{
														$objects[] = $module;
													}
												}else{
													if(!empty($o->url_id)){
														$objects[] = (object) array_merge((array) $o,array('module' => 'login'));
													}else{
														$objects[] = (object) array_merge((array) $o,array('module' => 'logout'));
													}
												}
											}
											$data['objects'] = $objects;
											$view = $this->load->view('custom_walalist', $data, TRUE);
										}
										break;
								}break;	
							case 'user':
								$lingua = $this->input->post('language_id');
								switch($virato){
									case '0':
										if($duum == 6){//role
											if($lingua == 0){//list
												echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
												$category = $this->dbfun_model->get_all_data_bys('role_id, name,status','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and role_id != '.$this->user_role.'','umr');
												$datas2 = array();
												foreach($category as $c){
													$dt = $this->dbfun_model->get_all_data_bys('ccs_umd.module_id, ccs_umd.name','ccs_umt.ccs_id = '.$this->zone_id.' and ccs_umt.role_id = '.$c->role_id.' and ccs_umt.ccs_id = ccs_umd.ccs_id and ccs_umt.module_id = ccs_umd.module_id and ccs_umd.parent_id = 0','umt,umd');
													$datas2[] = array('role' => $c, 'module' => $dt);
												}
												$data['category'] = $datas2;
												$data['cat_count'] = count($category);
												$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 9 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
												$view = $this->load->view('custom_padaview', $data, TRUE);		
											}elseif(in_array($lingua,array('1','2'),true)){//create & edit role
												echo '<link href="http://buzcon.com/project//assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet" type="text/css">';
												echo '<script src="http://buzcon.com/project//assets/admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>';
												echo '<script>
												$(".i-checks").iCheck({
													checkboxClass: "icheckbox_square-green",
													radioClass: "iradio_square-green",
												});</script>';
												$module = $this->dbfun_model->get_all_data_bys('name, icon, module_id, parent_id, description','parent_id = 0 and ccs_id = '.$this->zone_id.' and tu_id = '.$this->user_type.' and status = 1 order by name ASC','umd');
												if($lingua == 1){//create role
													$data['category'] = $module;
													$data['umr'] = $this->dbfun_model->get_all_data_bys('role_id,name','ccs_id = '.$this->zone_id.' and rt_id = 0 and status = 1','umr');
													$view = $this->load->view('custom_padacreate', $data, TRUE);
												}elseif($lingua == 2){//edit role
													$data['module'] = $module;
													$category = $this->dbfun_model->get_all_data_bys('role_id,type_id, name,description, status','ccs_id = '.$this->zone_id.' and role_id = '.$id.' and status = 1','umr');
													foreach($category as $c){
														$dt = $this->dbfun_model->get_all_data_bys('ccs_umd.module_id, ccs_umd.name,ccs_umd.icon','ccs_umt.ccs_id = '.$this->zone_id.' and ccs_umt.role_id = '.$id.' and ccs_umt.ccs_id = ccs_umd.ccs_id and ccs_umt.module_id = ccs_umd.module_id and ccs_umd.parent_id = 0 and ccs_umd.status = 1','umt,umd');
														$datas2[] = array('role' => $c, 'module' => $dt);
													}
													$data['category'] = $datas2;
													$view = $this->load->view('custom_padaedit', $data, TRUE);	
												}
											}
										}
										break;
									case '3':
										if($duum == 0){//profile
											if($lingua == 0){//edit profile
												$obj = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','ccs_usr');
												$role = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$obj->type_id.'','umr');
												$objects = (object) array_merge((array) $obj,array('role' => $role->name));
												
												$data['objects'] = $objects;
												$data['zone'] = $this->zone;
												$view = $this->load->view('custom_walaedit', $data, TRUE);	
											}elseif($lingua == 1){//create
												$data['role'] = $this->dbfun_model->get_all_data_bys('role_id, name,status','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and role_id != '.$this->user_role.'','umr');
												$view = $this->load->view('custom_walacreate', $data, TRUE);	
											}
										}elseif($duum == 6){//user
										
										}
										break;
								}break;
							case 'role':
							case 'roles':
								switch($virato){
									case '0':
										$data['id'] = $this->input->post('id');
										if(($duum == 6) && ($simo == 0)){ //role
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
											if($id == 'all'){
												$obj = $this->dbfun_model->get_all_data_bys('ccs_usr.user_id,ccs_umr.type_id,ccs_usr.name,ccs_umr.name as type, ccs_usr.status,ccs_usr.createdby','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.role_id = ccs_umr.role_id and ccs_usr.rt_id = '.$segretis.'','usr,umr');
											}else{
												$obj = $this->dbfun_model->get_all_data_bys('ccs_usr.user_id,ccs_umr.type_id,ccs_usr.name,ccs_umr.name as type, ccs_usr.status,ccs_usr.createdby','ccs_usr.ccs_id = '.$this->zone_id.' and ccs_usr.ccs_id = ccs_umr.ccs_id and ccs_usr.role_id = ccs_umr.role_id and ccs_usr.rt_id = '.$segretis.' and ccs_umr.type_id = '.$id.'','usr,umr');
												$data['role'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$id.'','umr');
											}
											$data['objects'] = $obj;
											$data['c_objects'] = count($obj);
											$data['user_id'] = $this->user_id;
											$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 9 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
											$view = $this->load->view('custom_padalist', $data, TRUE);	
										}
										break;
								}break;
						}
					}
					else
					{
						switch($part){
							case 'sidebar':
								switch($part2){
									case 'o':
										if(isset($c_add)){
											$data['datas'] = $this->get_all_object_from_module_custom($c_add[0],$c_add[1],$c_add[2],$sigilo,$lingua);
										}elseif(isset($c_q)){
											$data['datas'] = $c_q;
										}else{
											$data['datas'] = $this->get_all_object_from_module($sigilo,$lingua);
										}
										break;
									case 'oct':
										if(isset($c_add)){
											$data['datas'] = $this->get_all_category_custom($c_add[0],$c_add[1],$c_add[2],$sigilo,$lingua);
										}elseif(isset($c_q)){
											$data['datas'] = $c_q;
										}else{
											$data['datas'] = $this->get_all_category($sigilo,$lingua);
										}
										break;
								}
								if(isset($c_v)){
									$view = $this->load->view($c_v, $data, TRUE);
								}else{
									$view = $this->load->view('basic/list.php', $data, TRUE);
								}
								break;
							case 'list':
								switch($part2){
									case 'o':
										if(isset($c_add)){
											$data['datas'] = $this->get_all_object_from_module_custom($c_add[0],$c_add[1],$c_add[2],$sigilo,$lingua);
										}elseif(isset($c_q)){
											$data['datas'] = $c_q;
										}else{
											$data['datas'] = $this->get_all_object_from_module($sigilo,$lingua);
										}
										break;
									case 'oct':
										if(isset($c_add)){
											$data['datas'] = $this->get_all_category_custom($c_add[0],$c_add[1],$c_add[2],$sigilo,$lingua);
										}elseif(isset($c_q)){
											$data['datas'] = $c_q;
										}else{
											$data['datas'] = $this->get_all_category($sigilo,$lingua);
										}
										break;
								}
								
								if(isset($c_v)){
									$view = $this->load->view($c_v, $data, TRUE);
								}else{
									$view = $this->load->view('basic/list.php', $data, TRUE);
								}
								break;
							case 'detail':
								switch($part2){
									case 'o':
										if(isset($c_add)){
											$data['datas'] = $this->get_object_by_id_custom($c_add[0],$c_add[1],$c_add[2],$id,$lingua);
										}elseif(isset($c_q)){
											$data['datas'] = $c_q;
										}else{
											$data['datas'] = $this->get_object_by_id($id,$lingua);
										}
										break;
									case 'oct':
										if(isset($c_add)){
											$data['datas'] = $this->get_category_custom($c_add[0],$c_add[1],$c_add[2],$id,$lingua);
										}elseif(isset($c_q)){
											$data['datas'] = $c_q;
										}else{
											$data['datas'] = $this->get_category($sigilo,$id,$lingua);
										}
										break;
								}		
								
								if(isset($c_v)){
									$view = $this->load->view($c_v, $data, TRUE);
								}else{
									print_r($data['datas']);
									$view = $this->load->view('basic/detail.php', $data, TRUE);
								}
								break;
							case 'new':		
								if(isset($c_add)){
									$data['datas'] = $this->get_all_category_custom($c_add[0],$c_add[1],$c_add[2],$sigilo,$lingua);
								}elseif(isset($c_q)){
									$data['datas'] = $c_q;
								}else{
									$data['datas'] = $this->get_all_category($sigilo,$lingua);
								}
								if(isset($c_v)){
									$view = $this->load->view($c_v, $data, TRUE);
								}else{
									$view = $this->load->view('basic/detail.php', $data, TRUE);
								}
								break;
							case 'update':
								
								break;
						}
					}
					
					
				}
				echo $view;
			}
		}else{
			redirect($this->login);
		}
	}
	
	public function status($zone, $sigilo){
		if ($this->s_login && !empty($sigilo)){
			$data = array();
    		$status = '';
    		$message = '';
			
			$this->form_validation->set_rules('id', 'Zone Status', 'trim|required|numeric|xss_clean');
			$id = $this->input->post('id');
			$param = $this->input->post('param');
			$key_link = $this->input->post('url');			
			$fascia = $this->zone_id; 
			$inspeksi = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Inspeksi"','mdl')->module_id;
			
			if ($this->form_validation->run() === TRUE){
				if($sigilo == 'objects'){
					
					$ccs_key = $this->dbfun_model->get_data_bys('ccs_umd.module_id','ccs_umd.link like  "'.$key_link.'" and ccs_umd.ccs_id = '.$this->zone_id.' ','umd')->module_id;
					if($ccs_key){
						$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
						$update = array(
						'status' => $s
						);
						$this->dbfun_model->update_table(array('object_id' => $id), $update, 'odc');
										
						$status = 'success';
						$message = 'Data updated successfully';
					}
				}elseif($sigilo == 'category'){
					$ccs_key = $this->dbfun_model->get_data_bys('ccs_umd.module_id','ccs_umd.link like  "'.$key_link.'" and ccs_umd.ccs_id = '.$this->zone_id.' ','umd')->module_id;
					if($ccs_key){
						$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
						$update = array(
						'status' => $s
						);
						$this->dbfun_model->update_table(array('category_id' => $id), $update, 'oct');
										
						$status = 'success';
						$message = 'Data updated successfully';
					}
				}elseif($sigilo == 'inspeksi'){
					$ccs_key = $this->dbfun_model->get_data_bys('ccs_umd.module_id','ccs_umd.link like  "'.$key_link.'" and ccs_umd.ccs_id = '.$this->zone_id.' ','umd')->module_id;
					if($ccs_key){
						$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
						$update = array(
						'status' => $s
						);
						$this->dbfun_model->update_table(array('obr_id' => $id), $update, 'obr');
										
						$status = 'success';
						$message = 'Data updated successfully';
					}
				}elseif($sigilo == 'validasi'){
					if($param == 1){
						$update = array(
							'i_status' => $param
						);
						$this->dbfun_model->update_table(array('log_id' => $id), $update, 'olg');
						$objects = $this->dbfun_model->get_data_bys('object_id, status','ccs_id = '.$fascia.' and ccs_key = '.$inspeksi.' and log_id = '.$id.'','olg');
						
						$updates = array(
							'status' => $objects->status,
							'dateupdated' => date('Y-m-d H:i:s')
						);
						$this->dbfun_model->update_table(array('object_id' => $objects->object_id), $updates, 'odc');
						
						$status = 'success';
						$message = 'Data updated successfully';
					}
				}elseif($sigilo == 'roles'){
					if($param == 1){//saringan edit
						$update = array(
							'status' => $param
						);
						$this->dbfun_model->update_table(array('role_id' => $id), $update, 'umr');
						$status = 'success';
						$message = 'Data updated successfully';
					}
				}elseif($sigilo == 'profil'){
					if($param == 1){//saringan edit
						$update = array(
							'status' => $param
						);
						$this->dbfun_model->update_table(array('user_id' => $id), $update, 'usr');
						$status = 'success';
						$message = 'Data updated successfully';
					}
				}
			}else{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}
	}
	
	public function create($zone,$sigilo){
    	if ($this->s_login && !empty($sigilo)){
			$data = array();
			$status = '';
			$message = '';
			if(!empty($this->input->post('ccs_id'))){
				$fascia = $this->input->post('ccs_id');
			}else{
				$fascia = $this->zone_id;
			}
			$essentiel = $this->input->post('ccs_key');
			$stato = $this->input->post('status');
			$param = $this->input->post('param');	
			$lingua = $this->input->post('language_id');
			$genus = $this->input->post('category_id');	
			$lemma = $this->input->post('title');		
			$fattore = $this->user_id;
			$segretis = $this->segretis;
			
			$mdl = $this->dbfun_model->get_data_bys('ccs_umd.type_id, ccs_umd.type_view, ccs_umd.status as stato, ccs_umd.tu_id as parole, ccs_zone.type_id as chiave, ccs_umd.name as essentiel, ccs_umd.module_id as essentiel_id','ccs_umd.ccs_id = '.$fascia.' and ccs_umd.link like "'.$sigilo.'" and ccs_umd.ccs_id = ccs_zone.ccs_id','umd, zone');		
			if(!empty($mdl)){
				$chiave = $mdl->chiave;
				$parole = $mdl->parole;
				$duum = $mdl->type_id;
				$virato = $mdl->type_view;
				$essentiel_id = $mdl->essentiel_id;
			}else{
				$chiave = false;
			}
			
			if($chiave == 5){
				if(($sigilo) &&($param == 'custom')){ 
					
					if(($virato == 1)&& ($duum == 3)){
						$id = $this->input->post('custom_id');
						
						if($id){//update
							
						}else{ // create
							if ($this->form_validation->run('ccs_obr') === TRUE){
								$area_id = $this->input->post('area_id');
								$anchor = trim($this->input->post('simo'));
								$utl = $this->input->post('utl_id');
								$area = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Lokasi"','mdl')->module_id;						
								$utility = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Jembatan"','mdl')->module_id;
								$ceks = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Inspeksi"','mdl')->module_id;
								$balai = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and name = "Balai"','mdl')->module_id;
								$object = $this->input->post('object_id');
								$key = trim($this->input->post('tagline'));	
								if(($area_id == 0) && ($utl == 0)){
									$category = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.category_id','ccs_oct.ccs_key = '.$area.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0 and ccs_oct.ccs_key = ccs_obm.url_id and ccs_obm.ccs_key = '.$balai.' and ccs_obm.ccs_id = ccs_oct.ccs_id  and ccs_oct.category_id = ccs_obm.object_id and ccs_obm.category_id = '.$segretis.' order by ccs_oct.category_id ASC','oct, obm');
									foreach($category as $cat){
										$jembatan = $this->dbfun_model->get_all_data_bys('title, object_id, category_id, love, metakeyword, metadescription  ','ccs_key = '.$utility.' and ccs_id = '.$fascia.' and love = '.$cat->category_id.' and metakeyword = "'.$anchor.'"','odc');
										$cj = count($jembatan);
										if($cj != 0){
											foreach($jembatan as $j){			
												$cek = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and area_id = '.$j->love.' and utl_id = '.$j->object_id.' and  chiave = "'.$key.'"','obr');
												if(!$cek){													
													$custom = array(
														'createdby' => $fattore,
														'area_id' => $j->love,
														'utl_id' => $j->object_id,
														'chiave' => $key,
														'parole' => $j->title,
														'metakeyword' => $j->metakeyword,
														'metadescription' => $j->metadescription																
													);
													$create_new_secondary = $this->new_data_secondary('ccs_obr',$custom,'','','','');	
													$this->dbfun_model->ins_to('ccs_obr', $create_new_secondary);
													
													$status = 'success';
													$message = 'Data berhasil ditambahkan ke dalam sistem. Terimakasih...';
												}else{
													$status = 'error';
													$message = 'Data dengan kode inspeksi yang anda masukkan sudah ada di dalam sistem, silahkan masukkan data lainnya. Terimakasih...';
												}
											} 
										}																							
									}											
								}elseif(($area_id != 0) &&($utl == 0)){
									$cek = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and area_id = '.$area_id.' and utl_id = '.$utl.' and  chiave = "'.$key.'" and category_id = '.$genus.'  ','obr');
									if(!$cek){		
										$j_count = count($this->dbfun_model->get_all_data_bys('title','ccs_key = '.$utility.' and ccs_id = '.$fascia.' and love = '.$area_id.' and metakeyword = "'.$anchor.'"','odc'));
										if($j_count != 0){
											$jembatan = $this->dbfun_model->get_all_data_bys('title, object_id, category_id, love, metakeyword, metadescription  ','ccs_key = '.$utility.' and ccs_id = '.$fascia.' and love = '.$area_id.' and metakeyword = "'.$anchor.'" ','odc');
											foreach($jembatan as $j){
												$cek2 = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and area_id = '.$j->love.' and utl_id = '.$j->object_id.' and object_id = '.$object.' and  chiave = "'.$key.'"','obr');
												if(!$cek2){				
													$custom = array(
														'createdby' => $fattore,
														'area_id' => $j->love,
														'utl_id' => $j->object_id,
														'object_id'=>$object,
														'chiave' => $key,
														'parole' => $j->title,
														'metakeyword' => $j->metakeyword,
														'metadescription' => $j->metadescription																
													);
													$create_new_secondary = $this->new_data_secondary('ccs_obr',$custom,'','','','');	
													$this->dbfun_model->ins_to('ccs_obr', $create_new_secondary);
													$status = 'success';
													$message = 'Data berhasil ditambahkan ke dalam sistem. Terimakasih...';
												}else{
												$status = 'error';
												$message = 'Data dengan kode inspeksi yang anda masukkan sudah ada di dalam sistem, silahkan masukkan data lainnya. Terimakasih...';
												}
											}												
										}else{
											$status = 'error';
											$message = 'Data jembatan tidak ditemukan, silahkan kembali ke modul jembatan dan tambahkan data jembatan terlebih dahulu. Terimakasih...';
										}																								
									}else{
										$status = 'error';
										$message = 'Data dengan kode inspeksi yang anda masukkan sudah ada di dalam sistem, silahkan masukkan data lainnya. Terimakasih...';
									}
								}elseif(($area_id != 0) &&($utl != 0)){
									$cek = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and category_id = '.$genus.' and area_id = '.$area_id.' and object_id = '.$object.' and utl_id = '.$utl.' and  chiave = "'.$key.'"','obr');
									if(!$cek){				
										$jembatan = $this->dbfun_model->get_data_bys('title, object_id, category_id, love, metakeyword, metadescription  ','ccs_key = '.$utility.' and ccs_id = '.$fascia.' and love = '.$area_id.' and object_id = '.$utl.' and metakeyword = "'.$anchor.'"','odc');
										$ceks= $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and category_id = '.$genus.' and area_id = '.$jembatan->love.' and object_id = '.$object.' and utl_id = '.$jembatan->object_id.' and  chiave = "'.$key.'"','obr');
										if(!$ceks){			
											$custom = array(
												'createdby' => $fattore,
												'chiave' => $key,
												'parole' => $jembatan->title,
												'metakeyword' => $jembatan->metakeyword,
												'metadescription' => $jembatan->metadescription																
											);
											$create_new_secondary = $this->new_data_secondary('ccs_obr',$custom,'','','','');	
											$this->dbfun_model->ins_to('ccs_obr', $create_new_secondary);
											$status = 'success';
											$message = 'Data berhasil ditambahkan ke dalam sistem. Terimakasih...';
										}									
									}else{
										$status = 'error';
										$message = 'Data dengan kode inspeksi yang anda masukkan sudah ada di dalam sistem, silahkan masukkan data lainnya. Terimakasih...';
									}
								}
								
							}else{
								$status = 'error';
								$message = validation_errors();
							}
						}				
						echo json_encode(array('status' => $status, 'm' => $message));						
					}
				}elseif(($sigilo) &&($param == 'new_custom')){ 
					
					if(($virato == 1)&& ($duum == 3)){
						$id = $this->input->post('custom_id');
						
						if($id){//update
							if ($this->form_validation->run('ccs_olg') === TRUE){
								$keys = $this->dbfun_model->get_data_bys('ri_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and url_id = '.$id.'','ori')->ri_id;
								
								if(isset($_FILES['image_square'])){
									$updates = $this->update_data_bys('*', array('log_id' => $id), 'ccs_olg','image_square', $this->zone.'/'.$sigilo,'image_square','jpeg|jpg|png');
								}else{
									$updates = $this->update_data_bys('*', array('log_id' => $id), 'ccs_olg','','','','');
								}
									$updates['title'] = $lemma;
									$updates['description'] = trim($this->input->post('description'));
									$updates['dateupdated'] = date('Y-m-d H:i:s');
									$updates['updatedby'] = $fattore;
									$updates['status'] = $stato;
									$updates['i_status'] = 3;
								/* $updates = array(
									'title' => $lemma,
									'description' => trim($this->input->post('description')),
									'dateupdated' => date('Y-m-d H:i:s'),
									'updatedby' => $fattore,
									'status' => $stato,
									'i_status' => 3
								); */
								//$this->update_data_bys('*',array('log_id' => $id),'ccs_olg','image_square', $this->zone.'/'.$sigilo,'image_square','png|jpg|jpeg');
								$this->dbfun_model->update_table(array('log_id' => $id), $updates, 'olg');
								
								$updates = array(
									'url_1' => $this->input->post('url_1'),
									'url_2' => $this->input->post('url_2'),
									'url_3' => $this->input->post('url_3'),
									'url_4' => $this->input->post('url_4'),
									'url_5' => $this->input->post('url_5')
								);
								$this->dbfun_model->update_table(array('ri_id' => $keys), $updates, 'ori');
								
								$updates = array(
									'number_1' => $this->input->post('number_1'),
									'number_2' => $this->input->post('number_2'),
									'number_3' => $this->input->post('number_3'),
								);
								$this->dbfun_model->update_table(array('rd_id' => $keys), $updates, 'ord');
								
								$status = 'success';
								$message = 'Data berhasil diubah, silahkan lihat kembali detail data yang anda ubah. Terimakasih...' ;	
							}else{
								$status = 'error';
								$message = validation_errors();
							}		
							echo json_encode(array('status' => $status, 'm' => $message));									
						}else{ // create
							if ($this->form_validation->run('ccs_olg') === TRUE){
								$keys = $this->input->post('url_id');
								$object = $this->input->post('object_id');
								$parent = $this->input->post('parent_id');
								if($parent == 0){
									if($keys == 0){
										$obr = $this->dbfun_model->get_all_data_bys('obr_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.'  and utl_id = '.$object.' and object_id = '.$genus.'','obr');
										foreach($obr as $o){
											$key = $o->obr_id;
											$cek = $this->dbfun_model->get_all_data_bys('log_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and url_id = '.$key.' and object_id = '.$object.' and category_id = '.$genus.'','olg');
											if(!$cek){
												$image = $this->input->post('image_square');
												if (!is_dir('assets/'.$this->zone.'/'.$sigilo))
												{
													mkdir('./assets/'.$this->zone.'/'.$sigilo, 0777, true);
												}
												$custom = array(
													'createdby' => $fattore,
													'url_id' => $key,
													'i_status' => 0

												);
												$value = $this->new_data('ccs_olg',$custom,'image_square','image_square', $this->zone.'/'.$sigilo,'jpeg|jpg|png');	
												//$this->dbfun_model->ins_to('ccs_olg', $create_new_secondary);
												
												$konten = $this->dbfun_model->get_data_bys('*','obr_id = '.$key.'','obr');
												$content = array(
													'url_id' => $value,
													'char_1' => $konten->title,
													'char_2' => $konten->parole,
													'char_3' => $konten->metakeyword,
													'char_4' => $konten->metadescription,
													'status' => 1

												);
												$create_new_content= $this->new_data_secondary('ccs_orc',$content,'','','','');	
												$this->dbfun_model->ins_to('ccs_orc', $create_new_content);
												
												$ord = array(
													'url_id' => $value,
													'status' => 1

												);
												$create_new_ord= $this->new_data_secondary('ccs_ord',$ord,'','','','');	
												$this->dbfun_model->ins_to('ccs_ord', $create_new_ord);
												
												$ori = array(
													'url_id' => $value,
													'status' => 1

												);
												$create_new_ori= $this->new_data_secondary('ccs_ori',$ori,'','','','');	
												$this->dbfun_model->ins_to('ccs_ori', $create_new_ori);
												
												$status = 'success';
												$message = 'Data berhasil ditambahkan ke dalam sistem. Terimakasih...';
											}else{
												$status = 'error';
												$message = 'Data inspeksi untuk jembatan yang anda masukkan sudah ada di dalam sistem. Mohon liat rincian inspeksi tentang jembatan itu. Terima kasih... ';
											}
											
										
										}
									}else{									
										$key = $keys;
										$cek = $this->dbfun_model->get_data_bys('log_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and url_id = '.$key.' and object_id = '.$object.' and category_id = '.$genus.'','olg');
										
										if(!$cek){
											$image = $this->input->post('image_square');
											if (!is_dir('assets/'.$this->zone.'/'.$sigilo))
											{
												mkdir('./assets/'.$this->zone.'/'.$sigilo, 0777, true);
											}
											$custom = array(
												'createdby' => $fattore,
												'i_status' => 0

											);
											$value = $this->new_data('ccs_olg',$custom,'image_square','image_square', $this->zone.'/'.$sigilo,'jpeg|jpg|png');	
											//$this->dbfun_model->ins_to('ccs_olg', $create_new_secondary);
											
											$konten = $this->dbfun_model->get_data_bys('*','obr_id = '.$key.'','obr');
											$content = array(
												'url_id' => $value,
												'char_1' => $konten->title,
												'char_2' => $konten->parole,
												'char_3' => $konten->metakeyword,
												'char_4' => $konten->metadescription,
												'status' => 1

											);
											$create_new_content= $this->new_data_secondary('ccs_orc',$content,'','','','');	
											$this->dbfun_model->ins_to('ccs_orc', $create_new_content);
											
											$ord = array(
												'url_id' => $value,
												'status' => 1

											);
											$create_new_ord= $this->new_data_secondary('ccs_ord',$ord,'','','','');	
											$this->dbfun_model->ins_to('ccs_ord', $create_new_ord);
											
											$ori = array(
												'url_id' => $value,
												'status' => 1

											);
											$create_new_ori= $this->new_data_secondary('ccs_ori',$ori,'','','','');	
											$this->dbfun_model->ins_to('ccs_ori', $create_new_ori);
											
											$status = 'success';
											$message = 'Data berhasil ditambahkan ke dalam sistem. Terimakasih...';										
										}else{
											$status = 'error';
											$message = 'Data inspeksi untuk jembatan yang anda masukkan sudah ada di dalam sistem. Mohon liat rincian inspeksi tentang jembatan itu. Terima kasih... ';
										}								
									}								
								}else{
									$log_id = $this->input->post('log');
									$cek = $this->dbfun_model->get_data_bys('log_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and url_id = '.$keys.' and parent_id = '.$log_id.'','olg');
									$log = $this->dbfun_model->get_data_bys('category_id, object_id, title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel.' and url_id = '.$keys.' and log_id = '.$log_id.'','olg');
									if(!$cek){										
										$custom = array(
											'createdby' => $fattore,
											'parent_title' => $log->title,
											'category_id' => $log->category_id,
											'object_id' => $log->object_id,
											'parent_id' => $log_id,
											'status' => 0,
											'i_status' => $stato

										);
										$value = $this->new_data('ccs_olg',$custom,'','','','g');	
										$status = 'success';
										$message = 'Alasan penolakan untuk inspeksi '.strtolower($log->title).' sudah ditambahkan. Terimakasih...' ;	
									}else{
										$status = 'error';
										$message = 'Alasan penolakan untuk inspeksi '.strtolower($log->title).' sudah ada dalam sistem. Terimakasih...' ;	
									}
								}
							}else{
								$status = 'error';
								$message = validation_errors();
							}		
							echo json_encode(array('status' => $status, 'm' => $message));	
						}
					}
				}elseif(($sigilo) && ($param == 'roles')){
					$id = $this->input->post('role_id');
					if($id){
						unset($cek);
						if ($this->form_validation->run('ccs_umr') === TRUE){
							$ceks = $this->dbfun_model->get_data_bys('name,role_id','ccs_id = '.$this->zone_id.' and LOWER(name) like "'.strtolower($this->input->post('name')).'" and type_id = '.$this->user_type.' and rt_id = '.$segretis.'','umr');
							if(empty($ceks) || (($ceks->role_id == $id) && (strtolower($ceks->name) == strtolower($this->input->post('name'))))){
								$module = $this->input->post('module_id');
								$cek_mods = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.' and type_id = '.$this->user_type.'','umt'); //cek lang
								if(!empty($cek_mods)){
									foreach($cek_mods as $cek_mod){
										$cek[] = $cek_mod->module_id;
									}
								}else{
									$cek = '';
								}
								if(strpos($module,',') !== false){//cek if the value from post is array
									$ids = explode(',',$module);
									
									foreach($ids as $module_id){
										if(!in_array($module_id, $cek)){
											$custom_primarys = array(
												'role_id' => $id,
												'status' => 1,
												'ccs_id' => $this->zone_id,
												'type_id' => $this->user_type,
												'createdby' => $this->user_id,
												'module_id' => $module_id,
											);
											$this->new_data('ccs_umt',$custom_primarys, '','', '','');
										}else{
											//no new language added to this zone coz already have it
										}
									}
									
								}else{
									if(!in_array($module, $cek)){
										$custom_primarys = array(
											'role_id' => $id,
											'status' => 1,
											'ccs_id' => $this->zone_id,
											'type_id' => $this->user_type,
											'createdby' => $this->user_id,
											'module_id' => $module,
										);
										$this->new_data('ccs_umt',$custom_primarys, '','', '','');
									}else{
										//no new language added to this zone coz already have it
									}
								}
								//delete data if not in the value from post
								$del_langs = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.' and type_id = '.$this->user_type.' and module_id NOT IN ('.$module.')','ccs_umt'); //cek lang
								if(!empty($del_langs)){
									foreach($del_langs as $dl){
										$this->dbfun_model->del_tables('ccs_id = '.$this->zone_id.' and role_id = '.$id.' and type_id = '.$this->user_type.' and module_id = '.$dl->module_id.'','ccs_umt');
									};
								};
								$datas = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'role_id'=>$id), 'ccs_umr','','','','');
								$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'role_id' => $id),$datas,'umr');
								$status = 'success';
								$message = 'Data berhasil dirubah ke dalam sistem';
							}else{
								$status = 'error';
								$message = 'Role dengan nama '.$ceks->name.' sudah ada, silahkan dicoba dengan nama yang lain';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}else{
						if ($this->form_validation->run('ccs_umr') === TRUE){
							$ceks = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and LOWER(name) like "'.strtolower($this->input->post('name')).'" and type_id = '.$this->user_type.'','umr');
							if(empty($ceks)){
								$ceks2 = $this->dbfun_model->get_data_bys('role_id,type_id,rt_id','ccs_id = '.$this->zone_id.' and role_id = '.$this->user_role.'','umr');
								if((!empty($this->segretis)) && (empty($this->simo))){
									$custom_primary = array(
										'status' => 1,
										'createdby' => $this->user_id,
										'type_id' => $this->user_type,
										'rt_id' => $this->segretis,
										'rw_id' => $this->simo,
										'ccs_id' => $this->zone_id
									);
								}elseif((!empty($this->user_type)) && (empty($this->segretis))){
									$custom_primary = array(
										'status' => 1,
										'createdby' => $this->user_id,
										'type_id' => $this->user_type,
										'rt_id' => $this->segretis,
										'rw_id' => $this->simo,
										'ccs_id' => $this->zone_id
									);
								}
								
								$role_id = $this->new_data('ccs_umr',$custom_primary, '','', '','');
								
								$module = $this->input->post('module_id');
								if(strpos($module,',') !== false){//cek if the value from post is array
									$ids = explode(',',$module);
									
									foreach($ids as $module_id){
										$custom_primarys = array(
											'role_id' => $role_id,
											'status' => 1,
											'ccs_id' => $this->zone_id,
											'type_id' => $this->user_type,
											'createdby' => $this->user_id,
											'module_id' => $module_id,
										);
										$this->new_data('ccs_umt',$custom_primarys, '','', '','');
									}
									
								}else{
									$custom_primarys = array(
										'role_id' => $role_id,
										'status' => 1,
										'ccs_id' => $this->zone_id,
										'type_id' => $this->user_type,
										'createdby' => $this->user_id,
										'module_id' => $module,
									);
									$this->new_data('ccs_umt',$custom_primarys, '','', '','');
								}
								$status = 'success';
								$message = 'Data berhasil ditambahkan ke dalam sistem. Terimakasih...';
							}else{
								$status = 'error';
								$message = 'Role dengan nama '.$ceks->name.' sudah ada, silahkan dicoba dengan nama yang lain';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					echo json_encode(array('status' => $status, 'm' => $message));
				}elseif(($sigilo) && ($param == 'profil')){
					$user_id = $this->user_id;
					$id = $this->input->post('user_id');
					$username = $this->input->post('username');
					if($id){
						unset($cek);
						$this->form_validation->set_rules('user_id', 'User ID', 'trim|required|numeric|xss_clean');
						$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean');
						if ($this->form_validation->run() === TRUE){
							$cek = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','usr');
							if($cek){
								$cek_type_user = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$cek->type_id.' and rt_id = 0','umr');
								foreach($_POST as $key => $val)  
								{
									$data1[$key] = $this->input->post($key); 
								}
								if(isset($_FILES['avatar'])){
									$data1['avatar'] = true; 
								}
								$data2 = get_object_vars($cek);
								$data_profile = array_intersect_key(array_diff_assoc($data1, $data2),$data2);
								if($data_profile){
									if(isset($_FILES['avatar'])){
										if (!is_dir('assets/'.$this->zone.'/user'))
											{
												mkdir('./assets/'.$this->zone.'/user', 0777, true);
											}
										if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name)))
											{
												mkdir('./assets/'.$this->zone.'/user'.strtolower($cek_type_user->name), 0777, true);
											}
										if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$id))
											{
												mkdir('./assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$id, 0777, true);
											}
										$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'user_id'=>$id), 'ccs_usr','avatar',$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$id,'avatar','jpeg|jpg|png');
									}else{
										$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'user_id'=>$id), 'ccs_usr','','','','');
									}
									$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'user_id' => $id),$update,'usr');
									$new_data = $this->dbfun_model->get_data_bys('name,avatar','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','usr');
									if(isset($_FILES['avatar'])){
										$foto = 'assets/'.$zone.'/user/'.strtolower($cek_type_user->name).'/'.$id.'/'.$new_data->avatar;
									}else{
										$foto = $this->session->userdata($this->zone.'_foto');
									}
									$session = array(
										strtolower($zone).'_nama' => $new_data->name,
										strtolower($zone).'_foto' => $foto
									);
									$this->session->set_userdata($session);
									$status = 'success';
									$message = 'Data updated successfully';
								}else{
									$status = 'error';
									$message = 'No Data updated';
								}
							}else{
								$status = 'error';
								$message = 'No Data Updated';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}else{
						$this->form_validation->set_rules('username', 'Full Name', 'trim|required|xss_clean');
						$this->form_validation->set_rules('password', 'Full Name', 'trim|required|xss_clean');
						$this->form_validation->set_rules('confirm', 'Full Name', 'trim|required|xss_clean');
						$this->form_validation->set_rules('role_id', 'Role', 'trim|numeric|required|xss_clean');
						if ($this->form_validation->run() === TRUE){
							$cek_username= $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and LOWER(username) like "'.strtolower($username).'"','usr');
							if(empty($cek_username)){
								if($this->input->post('password') == $this->input->post('confirm')){
									$cek_role = $this->dbfun_model->get_data_bys('type_id,rt_id,rw_id','ccs_id = '.$this->zone_id.' and role_id = '.$this->input->post('role_id').'','umr');
									if($cek_role->type_id == $this->user_type){//child user from this role ex:admin jembatan
										if((!empty($this->segretis)) && (!empty($this->simo))){
											$custom_primary = array(
												'status' => 1,
												'createdby' => $this->user_id,
												'type_id' => $this->user_type,
												'rt_id' => $this->segretis,
												'rw_id' => $this->simo,
												'ccs_id' => $this->zone_id,
												'password' => MD5($this->input->post('password'))
											);
										}elseif((!empty($this->user_type)) && (!empty($this->segretis))){
											$custom_primary = array(
												'status' => 1,
												'createdby' => $this->user_id,
												'type_id' => $this->user_type,
												'rt_id' => $this->segretis,
												'ccs_id' => $this->zone_id,
												'password' => MD5($this->input->post('password'))
											);
										}
									}else{//category user balai or pengguna or etc
										if((!empty($this->segretis)) && (!empty($this->simo))){
											$custom_primary = array(
												'status' => 1,
												'createdby' => $this->user_id,
												'type_id' => $cek_role->type_id,
												'rt_id' => $this->segretis,
												'rw_id' => $this->simo,
												'ccs_id' => $this->zone_id,
												'password' => MD5($this->input->post('password'))
											);
										}elseif((!empty($this->user_type)) && (!empty($this->segretis))){
											$custom_primary = array(
												'status' => 1,
												'createdby' => $this->user_id,
												'type_id' => $cek_role->type_id,
												'rt_id' => $this->segretis,
												'rw_id' => $this->user_id,
												'ccs_id' => $this->zone_id,
												'password' => MD5($this->input->post('password'))
											);
										}
									}
									
									//custom for BINAMARGA
									$custom_primary['o_email'] = $this->dbfun_model->get_data_bys('o_email','ccs_id = '.$fascia.' and user_id = '.$fattore.'','usr')->o_email;
									//end custom for BINAMARGA
									
									if(isset($_FILES['avatar'])){
										$user_id = $this->dbfun_model->get_max_data('max(user_id) as user_id','ccs_usr')->user_id +1;
										$cek_type_user = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$cek_role->type_id.' and rt_id = 0','umr');
										if (!is_dir('assets/'.$this->zone.'/user'))
											{
												mkdir('./assets/'.$this->zone.'/user', 0777, true);
											}
										if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name)))
											{
												mkdir('./assets/'.$this->zone.'/user'.strtolower($cek_type_user->name), 0777, true);
											}
										if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$user_id))
											{
												mkdir('./assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$user_id, 0777, true);
											}
										$this->new_data('ccs_usr',$custom_primary, 'avatar','avatar', $this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$user_id,'jpeg|jpg|png');	
									}else{
										$this->new_data('ccs_usr',$custom_primary, '','', '','');
									}
									
									$status = 'success';
									$message = 'Data updated successfully';
								}else{
									$status = 'error';
									$message = 'Password tidak sama';
								}
							}else{
								$status = 'error';
								$message = 'Username telah ada silahkan coba lagi dengan username yang lain';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					echo json_encode(array('status' => $status, 'm' => $message));
				}
			}else{
				if(in_array($param,array('role','roles','profil','profile'),true)){
					switch($param){
						case 'role':
						case 'roles':
							$id = $this->input->post('role_id');
							if($id){
								if ($this->form_validation->run('ccs_umr') === TRUE){
									$ceks = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and LOWER(name) like "'.strtolower($this->input->post('name')).'" and type_id = '.$this->user_type.'','umr');
									if(empty($ceks)){
										$module = $this->input->post('module_id');
										$cek_mods = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.' and type_id = '.$this->user_type.'','umt'); 
										if(!empty($cek_mods)){
											foreach($cek_mods as $cek_mod){
												$cek[] = $cek_mod->module_id;
											}
										}else{
											$cek = '';
										}
										if(strpos($module,',') !== false){
											$ids = explode(',',$module);
											
											foreach($ids as $module_id){
												if(!in_array($module_id, $cek)){
													$custom_primarys = array(
														'role_id' => $id,
														'status' => 1,
														'ccs_id' => $this->zone_id,
														'type_id' => $this->user_type,
														'createdby' => $this->user_id,
														'module_id' => $module_id,
													);
													$this->new_data('ccs_umt',$custom_primarys, '','', '','');
												}else{
													
												}
											}
											
										}else{
											if(!in_array($module, $cek)){
												$custom_primarys = array(
													'role_id' => $id,
													'status' => 1,
													'ccs_id' => $this->zone_id,
													'type_id' => $this->user_type,
													'createdby' => $this->user_id,
													'module_id' => $module,
												);
												$this->new_data('ccs_umt',$custom_primarys, '','', '','');
											}else{
												
											}
										}
										
										$del_langs = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.' and type_id = '.$this->user_type.' and module_id NOT IN ('.$module.')','ccs_umt'); 
										if(!empty($del_langs)){
											foreach($del_langs as $dl){
												$this->dbfun_model->del_tables('ccs_id = '.$this->zone_id.' and role_id = '.$id.' and type_id = '.$this->user_type.' and module_id = '.$dl->module_id.'','ccs_umt');
											};
										};
										$datas = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'role_id'=>$id), 'ccs_umr','','','','');
										$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'role_id' => $id),$datas,'umr');
										$status = 'success';
										$message = 'Data berhasil dirubah ke dalam sistem';
									}else{
										$status = 'error';
										$message = 'Role dengan nama '.$ceks->name.' sudah ada, silahkan dicoba dengan nama yang lain';
									}
								}else{
									$status = 'error';
									$message = validation_errors();
								}
							}else{
								if ($this->form_validation->run('ccs_umr') === TRUE){
									$ceks = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and LOWER(name) like "'.strtolower($this->input->post('name')).'" and type_id = '.$this->user_type.'','umr');
									if(empty($ceks)){
										$ceks2 = $this->dbfun_model->get_data_bys('role_id,type_id,rt_id','ccs_id = '.$this->zone_id.' and role_id = '.$this->user_role.'','umr');
										if((!empty($this->segretis)) && (empty($this->simo))){
											$custom_primary = array(
												'status' => 1,
												'createdby' => $this->user_id,
												'type_id' => $this->user_type,
												'rt_id' => $ceks2->role_id,
												'rw_id' => $ceks2->rt_id,
												'ccs_id' => $this->zone_id
											);
										}elseif((!empty($this->user_type)) && (empty($this->segretis))){
											$custom_primary = array(
												'status' => 1,
												'createdby' => $this->user_id,
												'type_id' => $this->user_type,
												'rt_id' => $ceks2->role_id,
												'ccs_id' => $this->zone_id
											);
										}
										
										$role_id = $this->new_data('ccs_umr',$custom_primary, '','', '','');
										
										$module = $this->input->post('module_id');
										if(strpos($module,',') !== false){
											$ids = explode(',',$module);
											
											foreach($ids as $module_id){
												$custom_primarys = array(
													'role_id' => $role_id,
													'status' => 1,
													'ccs_id' => $this->zone_id,
													'type_id' => $this->user_type,
													'createdby' => $this->user_id,
													'module_id' => $module_id,
												);
												$this->new_data('ccs_umt',$custom_primarys, '','', '','');
											}
											
										}else{
											$custom_primarys = array(
												'role_id' => $role_id,
												'status' => 1,
												'ccs_id' => $this->zone_id,
												'type_id' => $this->user_type,
												'createdby' => $this->user_id,
												'module_id' => $module,
											);
											$this->new_data('ccs_umt',$custom_primarys, '','', '','');
										}
										$status = 'success';
										$message = 'Data berhasil ditambahkan ke dalam sistem. Terimakasih...';
									}else{
										$status = 'error';
										$message = 'Role dengan nama '.$ceks->name.' sudah ada, silahkan dicoba dengan nama yang lain';
									}
								}else{
									$status = 'error';
									$message = validation_errors();
								}
							}
							echo json_encode(array('status' => $status, 'm' => $message));
							break;
						case 'profil':	
						case 'profile':
							$user_id = $this->user_id;
							$id = $this->input->post('user_id');
							$username = $this->input->post('username');
							if($id){
								unset($cek);
								$this->form_validation->set_rules('user_id', 'User ID', 'trim|required|numeric|xss_clean');
								$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean');
								if ($this->form_validation->run() === TRUE){
									$cek = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','usr');
									if($cek){
										$cek_type_user = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$cek->type_id.' and rt_id = 0','umr');
										foreach($_POST as $key => $val)  
										{
											$data1[$key] = $this->input->post($key); 
										}
										if(isset($_FILES['avatar'])){
											$data1['avatar'] = true; 
										}
										$data2 = get_object_vars($cek);
										$data_profile = array_intersect_key(array_diff_assoc($data1, $data2),$data2);
										if($data_profile){
											if(isset($_FILES['avatar'])){
												if (!is_dir('assets/'.$this->zone.'/user'))
													{
														mkdir('./assets/'.$this->zone.'/user', 0777, true);
													}
												if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name)))
													{
														mkdir('./assets/'.$this->zone.'/user'.strtolower($cek_type_user->name), 0777, true);
													}
												if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$id))
													{
														mkdir('./assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$id, 0777, true);
													}
												$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'user_id'=>$id), 'ccs_usr','avatar',$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$id,'avatar','jpeg|jpg|png');
											}else{
												$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'user_id'=>$id), 'ccs_usr','','','','');
											}
											$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'user_id' => $id),$update,'usr');
											$new_data = $this->dbfun_model->get_data_bys('name,avatar','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','usr');
											if(isset($_FILES['avatar'])){
												$foto = 'assets/'.$zone.'/user/'.strtolower($cek_type_user->name).'/'.$id.'/'.$new_data->avatar;
											}else{
												$foto = $this->session->userdata($this->zone.'_foto');
											}
											$session = array(
												strtolower($zone).'_nama' => $new_data->name,
												strtolower($zone).'_foto' => $foto
											);
											$this->session->set_userdata($session);
											$status = 'success';
											$message = 'Data updated successfully';
										}else{
											$status = 'error';
											$message = 'No Data updated';
										}
									}else{
										$status = 'error';
										$message = 'No Data Updated';
									}
								}else{
									$status = 'error';
									$message = validation_errors();
								}
							}else{
								$this->form_validation->set_rules('username', 'Full Name', 'trim|required|xss_clean');
								$this->form_validation->set_rules('password', 'Full Name', 'trim|required|xss_clean');
								$this->form_validation->set_rules('confirm', 'Full Name', 'trim|required|xss_clean');
								$this->form_validation->set_rules('role_id', 'Role', 'trim|numeric|required|xss_clean');
								if ($this->form_validation->run() === TRUE){
									$cek_username= $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and LOWER(username) like "'.strtolower($username).'"','usr');
									if(empty($cek_username)){
										if($this->input->post('password') == $this->input->post('confirm')){
											$cek_role = $this->dbfun_model->get_data_bys('type_id,rt_id,rw_id','ccs_id = '.$this->zone_id.' and role_id = '.$this->input->post('role_id').'','umr');
											if($cek_role->type_id == $this->user_type){//child user from this role ex:admin jembatan
												if((!empty($this->segretis)) && (!empty($this->simo))){
													$custom_primary = array(
														'status' => 1,
														'createdby' => $this->user_id,
														'type_id' => $this->user_type,
														'rt_id' => $this->segretis,
														'rw_id' => $this->simo,
														'ccs_id' => $this->zone_id,
														'password' => MD5($this->input->post('password'))
													);
												}elseif((!empty($this->user_type)) && (!empty($this->segretis))){
													$custom_primary = array(
														'status' => 1,
														'createdby' => $this->user_id,
														'type_id' => $this->user_type,
														'rt_id' => $this->segretis,
														'ccs_id' => $this->zone_id,
														'password' => MD5($this->input->post('password'))
													);
												}
											}else{//category user balai or pengguna or etc
												if((!empty($this->segretis)) && (!empty($this->simo))){
													$custom_primary = array(
														'status' => 1,
														'createdby' => $this->user_id,
														'type_id' => $cek_role->type_id,
														'rt_id' => $this->segretis,
														'rw_id' => $this->simo,
														'ccs_id' => $this->zone_id,
														'password' => MD5($this->input->post('password'))
													);
												}elseif((!empty($this->user_type)) && (!empty($this->segretis))){
													$custom_primary = array(
														'status' => 1,
														'createdby' => $this->user_id,
														'type_id' => $cek_role->type_id,
														'rt_id' => $this->segretis,
														'rw_id' => $this->user_id,
														'ccs_id' => $this->zone_id,
														'password' => MD5($this->input->post('password'))
													);
												}
											}
											
											//custom for BINAMARGA
											/* $custom_primary['o_email'] = $this->dbfun_model->get_data_bys('o_email','ccs_id = '.$fascia.' and user_id = '.$fattore.'','usr')->o_email; */
											//end custom for BINAMARGA
											
											if(isset($_FILES['avatar'])){
												$user_id = $this->dbfun_model->get_max_data('max(user_id) as user_id','ccs_usr')->user_id +1;
												$cek_type_user = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$cek_role->type_id.' and rt_id = 0','umr');
												if (!is_dir('assets/'.$this->zone.'/user'))
													{
														mkdir('./assets/'.$this->zone.'/user', 0777, true);
													}
												if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name)))
													{
														mkdir('./assets/'.$this->zone.'/user'.strtolower($cek_type_user->name), 0777, true);
													}
												if (!is_dir('assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$user_id))
													{
														mkdir('./assets/'.$this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$user_id, 0777, true);
													}
												$this->new_data('ccs_usr',$custom_primary, 'avatar','avatar', $this->zone.'/user/'.strtolower($cek_type_user->name).'/'.$user_id,'jpeg|jpg|png');	
											}else{
												$this->new_data('ccs_usr',$custom_primary, '','', '','');
											}
											
											$status = 'success';
											$message = 'Data updated successfully';
										}else{
											$status = 'error';
											$message = 'Password tidak sama';
										}
									}else{
										$status = 'error';
										$message = 'Username telah ada silahkan coba lagi dengan username yang lain';
									}
								}else{
									$status = 'error';
									$message = validation_errors();
								}
							}
							echo json_encode(array('status' => $status, 'm' => $message));
							break;
					}
				}else{
					switch($sigilo){
						case 'development':
						case 'ideas':
							switch($param){
								case 'create':
									$dt = array('o','odc');
									//$c_valid = array('title' => 'trim|required|xss_clean','description'=> 'trim|required|xss_clean');
									break;
							}
							break;
						case 'action':
							switch($param){
								case 'follow':
								case 'participate':
								case 'join':
									$action = 5;
									break;
								case 'like':	
								case 'support':
									$action = 6;
									break;
								case 'unfollow':
								case 'unparticipate':
								case 'unjoin':
									$action = 7;
									break;
								case 'unlike':
								case 'unsupport':
									$action = 8;
									break;
							}
							$url_id = $this->input->post('param2');
							if(!empty($url_id)){
								$this->log_user($this->user_id,1,$url_id,$action);
								$cek_o = $this->get_object_by_id($url_id,2);
								$cek_usr = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.'','usr');
								if(!empty($cek_o)){
									$m = $cek_o->title;
								}elseif(!empty($cek_usr)){
									$m = $cek_usr->name;
								}
								$status = 'success';
								$message = 'You '.$param.' '.$m.'';
							}else{
								$status = 'error';
							}
							break;
					}
					$b_dt = array(
							'ccs_id' => $this->zone_id,
							'ccs_key' => $this->module_to_ccs_key($sigilo),
							'tu' => $this->user_type
						);	
					if(isset($dt)){
						foreach($dt as $d){
							switch($d){
								case 'o':
									$id = $this->input->post('object_id');
									if ($this->form_validation->run('ccs_odc') === TRUE){
										if(isset($_FILES['image_square'])){
											if (!is_dir('assets/'.$this->zone.'/'.$sigilo))
											{
												mkdir('./assets/'.$this->zone.'/'.$sigilo, 0777, true);
											}
											if($id){
												$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'object_id'=>$id), 'ccs_o','image_square',$this->zone.'/'.$sigilo,'image_square','jpeg|jpg|png');
											}
										}else{
											if($id){
												$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'object_id'=>$id), 'ccs_o','','','','');
											}
										}
										
										if($id){//update
											$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'object_id' => $id),$update,'o');
											$status = 'success';
											$message = 'Data Updated Successfully';
										}else{
											if(!isset($s_valid)){
												$s_valid = true;
											}
											if(isset($c_o)){
												$custom_primary = array_merge((array) $b_dt, (array) $c_o);
											}else{
												$custom_primary = $b_dt;
											}
											if(isset($_FILES['image_square'])){
												if(isset($c_valid)){
													foreach($c_valid as $key => $v){
														$this->form_validation->set_rules($key, $key, $v);
													}
													if ($this->form_validation->run() === TRUE){
														$o = $this->new_data('ccs_o',$custom_primary, 'image_square','image_square', $this->zone.'/'.$sigilo,'jpeg|jpg|png');
													}else{
														$s_valid = false;
														$status = 'error';
														$message = validation_errors();
													}
												}else{
													$o = $this->new_data('ccs_o',$custom_primary, 'image_square','image_square', $this->zone.'/'.$sigilo,'jpeg|jpg|png');	
												}
											}else{
												if(isset($c_valid)){
													foreach($c_valid as $key => $v){
														$this->form_validation->set_rules($key, $key, $v);
													}
													if ($this->form_validation->run() === TRUE){
														$o = $this->new_data('ccs_o',$custom_primary, '','', '','');
													}else{
														$s_valid = false;
														$status = 'error';
														$message = validation_errors();
													}
												}else{
													$o = $this->new_data('ccs_o',$custom_primary, '','', '','');
												}
											}
											if($s_valid === true){
												$status = 'success';
												$message = 'Data Created Successfully';
											}
										}
									}else{
										$status = 'error';
										$message = validation_errors();
										break;
									}
									break;
								case 'odc':
									$id = $this->input->post('object_id');
									if ($this->form_validation->run('ccs_odc') === TRUE){
										if($id){//update
											$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'object_id'=>$id), 'ccs_odc','','','','');
											$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'object_id' => $id),$update,'odc');
											$status = 'success';
											$message = 'Data Updated Successfully';
										}else{
											if(!isset($s_valid)){
												$s_valid = true;
											}
											if(isset($c_odc)){
												$first = array_merge((array) $b_dt, array('object_id'=> $o));
												$custom_primary = array_merge((array) $first, (array) $c_odc);
											}else{
												$custom_primary = array_merge((array) $b_dt, array('object_id'=> $o));
											}
											if(isset($c_valid)){
												foreach($c_valid as $key => $v){
													$this->form_validation->set_rules($key, $key, $v);
												}
												if ($this->form_validation->run() === TRUE){
													$this->new_data('ccs_odc',$custom_primary, '','', '','');
												}else{
													$s_valid = false;
													$status = 'error';
													$message = validation_errors();
												}
											}else{
												$this->new_data('ccs_odc',$custom_primary, '','', '','');
											}
											if($s_valid === true){
												$status = 'success';
												$message = 'Data Created Successfully';
											}
										}
									}else{
										$status = 'error';
										$message = validation_errors();
										break;
									}
									break;
							}
						};
					};
					echo json_encode(array('status' => $status, 'm' => $message));
				}
			}
		}else{
			redirect($this->login);
		}
	}
}