<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Profil</h5>
	</div>
	<div class="ibox-content">
		<form id="create_user" class="form-horizontal">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<input id="user_id" name="inputan" type="hidden" class="form-control" value="">
			<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $fascia;?>">
			<div class="form-group">
				<label class="col-sm-2 control-label">Nama Lengkap</label>
				<div class="col-sm-10"><input id="name" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group"><label class="col-sm-2 control-label">Jenis Kelamin</label>
				<div class="col-sm-10">
					<select id="gender" name="inputan" class="form-control m-b">
						<option value="">Jenis Kelamin</option>
						<option value="1">Pria</option>
						<option value="2">Wanita</option>
					</select>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nomor Telepon</label>
				<div class="col-sm-10"><input id="phone" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nomor HP</label>
				<div class="col-sm-10"><input id="mobile" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">E-mail</label>
				<div class="col-sm-10"><input id="email" name="inputan" type="text" class="form-control required" value=""></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group"><label class="col-sm-2 control-label">Role</label>
				<div class="col-sm-10">
					<select id="role_id" name="inputan" class="form-control m-b">
						<option value="">Jenis Role</option>
						<?php foreach($role as $o){;?>
						<option value="<?php echo $o->role_id;?>"><?php echo $o->name;?></option>
						<?php };?>
					</select>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Username</label>
				<div class="col-sm-10"><input id="username" name="inputan" type="text" class="form-control required" value="" ></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password</label>
				<div class="col-sm-10"><input id="password" name="inputan" type="password" class="form-control required" value="" ></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Ulangi Password</label>
				<div class="col-sm-10"><input id="confirm" name="inputan" type="password" class="form-control required" value="" ></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<button id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" class="detail2 btn btn-white" type="">Batalkan</button>
					<button id="<?php echo $sigilo;?>" data-param="<?php echo $sigilo;?>" class="create_mdl btn btn-primary" type="submit">Buat</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script src="<?php echo base_url('assets');?>/admin/js/plugins/validate/jquery.validate.min.js"></script>
<script>
$(document).ready(function(){
	$('.create_mdl').validate({
		rules: {
			confirm: {
				equalTo: "#password"
			}
		},
		messages: {
			confirm: {
				required: "<li>Please enter a name.</li>"
			}
		}
	});
});
</script>