<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $inspeksi->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2">
					Jemb. <?php echo $inspeksi->parole ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_custom" data-lang="2" class="detail2">
					<?php echo $inspeksi->chiave ;?>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="<?php echo $inspeksi->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_object" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Kembali</button>
			<button id="<?php echo $objects->log_id ;?>" data-url="<?php echo $objects->object_id ;?>" data-url2="<?php echo $objects->ccs_key ;?>"  data-lang="2" class="modal_upload_data btn-sm btn btn-primary pull-right" style="margin: 0 5px">Tambah Foto</button>
		</div>
	</div>
	
	<div class="ibox-content row">
		<div class="row" style="margin-bottom: 20px">		
			<div class="col-sm-6 pull-left">
				<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_custom" data-lang="2" class="detail2 btn-xs btn btn-white pull-left ">Hasil Inspeksi</button>
				<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-lang="2" data-url3="detail" data-param="gallery_custom" class="detail2 btn-xs btn btn-white pull-left " style="margin : 0 5px">Foto Inspeksi</button>
			</div>
		</div>
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<div class="row">
			<div class="col-sm-4" style="padding-right: 0px !important">
				<img alt="image" class="col-md-12 img-responsive" src="
					<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $sigilo ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-right: auto; margin-left: auto">
			</div>
			<div class="col-sm-8 row" style="padding: 5px 0px !important">
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Nama</div>
					<div class="col-sm-9 col-xs-8 "><p>: <strong><?php echo $objects->title?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">No. Inspeksi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <strong><?php echo $inspeksi->chiave?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Status Jembatan</div>
					<div class="col-sm-9 col-xs-8 ">
						<p>: <strong>
							<?php foreach($status as $s){ ;?>
								<?php if($s->value == $objects->status){ echo $s->name ;} ;?>
							<?php } unset($s);?>
						</strong></p>
					</div>
				</div>	
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Tanggal Inspeksi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <strong><?php echo date("l, d M Y", strtotime($objects->datecreated)) ?></strong></p></div>
				</div>	
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Lokasi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <?php echo $inspeksi->metadescription?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Tahun</div>
					<div class="col-sm-9 col-xs-8 "><p>: <?php echo $inspeksi->tahun?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Tim Inspeksi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <?php echo $objects->name?></p></div>
				</div>			
			</div>
			<div class="col-sm-12 col-xs-12" style="margin-top: 20px">
				<div class="col-sm-2 col-xs-12 ">Catatan</div>
				<div class="col-sm-10 col-xs-12 "><p> <?php echo $objects->description?></p></div>					
			</div>
		</div>
		<div class="" style="margin-top: 20px">
			<h3>Hasil inspeksi untuk <?php echo $objects->title ;?><span> pada inspeksi tanggal <?php echo date("d M Y", strtotime($objects->datecreated)) ?></span></h3>
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
				<thead >
						<tr>
							<th rowspan="2" style="text-align: center">No</th>
							<th rowspan="2" style="text-align: center">Tanggal Inspeksi</th>
							<th colspan="5" style="text-align: center">Kondisi Umum</th>
							<th colspan="3" style="text-align: center">Nilai IRMS</th>
							<th colspan="2" style="text-align: center">Status</th>
						</tr>
						<tr >
							<th >BA</th>
							<th>LNT</th>
							<th>BB</th>
							<th>DAS</th>
							<th>JBT</th>
							<th>TRAFC</th>
							<th>AADT</th>
							<th>JLN</th>
							<th>Status</th>
							<th>Aktivasi</th>
						</tr>
				</thead>
				<tbody>
					<tr >
						<td style="text-align: center">1</td>
						<td><?php echo date("l, d M Y", strtotime($objects->datecreated)) ?> </td>
						<td><?php echo $list->url_1?></td>
						<td><?php echo $list->url_2?></td>
						<td><?php echo $list->url_3?></td>
						<td><?php echo $list->url_4?></td>
						<td><?php echo $list->url_5?></td>
						<td><?php echo $list->number_1?></td>
						<td><?php echo $list->number_2?></td>
						<td><?php echo $list->number_3?></td>						
						<td>
							<?php if($objects->i_status == 0){?>
								<?php foreach($status as $s){ ;?>
									<?php if($s->value == $objects->status){ echo $s->name ;} ;?>
								<?php } ;?>
							<?php }elseif($objects->i_status == 1){ ;?>
								Inspeksi Diterima
							<?php }elseif($objects->i_status == 2){ ;?>
								Ulang Inspeksi
							<?php }elseif($objects->i_status == 3){ ;?>
								Menunggu Konfirmasi
							<?php };?>
						</td>
						<td>
							
							<?php if($simo == 0){ ;?>
								<?php if(($objects->i_status == 0) || ($objects->i_status == 3)){ ;?>
									<button id="<?php echo $objects->log_id ?>" data-url="validasi" data-param="1" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="Terima" data-title="<?php echo $inspeksi->chiave ;?>" class="modal_stat btn btn-primary btn-xs" type="button" style="margin: 2px">Terima</button>
									<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="view_custom" data-lang="1" class="detail2 btn-xs btn btn-danger">Tolak</button>
								<?php }elseif($objects->i_status == 2){ ;?>
									<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="view_custom" data-lang="2" class="detail2 btn-xs btn btn-warning" style="margin: 2px 0">Lihat Alasan</button>
								<?php } ;?>
							<?php }else{ ;?>
								<?php if($objects->i_status == 2){ ;?>
									
								<?php }elseif($objects->i_status == 3){ ;?>								
									<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="view_custom" data-lang="2" class="detail2 btn-xs btn btn-warning" style="margin: 2px 0">Lihat Alasan</button>
								<?php }elseif($objects->i_status == 0){ ;?>								
									<button class="btn btn-warning btn-xs" type="button" style="margin: 2px">Menunggu Konfirmasi</button>
								<?php } ;?>
							<?php } ;?>
							<button id="<?php echo $objects->log_id?>" data-url="module" data-url2="<?php echo $sigilo ;?>" data-lang="2" data-param="edit_custom" class="detail2 btn-xs btn btn-info ">Ubah</button>	
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div>
</div>