<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo ucfirst($essentiel);?>				
				</a>
			</li>
			<?php if($id != 'all'){ ;?>
			<li class="">
				<a id="" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_category" data-lang="2" class="detail2">
				</a>
			</li>
			<li class="">
				<a id="" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2">
					
				</a>
			</li>
			<li class="active">
				<a id="" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="new" data-lang="2" class="detail2">
					Tambah
				</a>
			</li>
			<?php }else{ ;?>
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="new" data-lang="2" class="detail2">
					Tambah
				</a>
			</li>
			<?php } ;?>
		</ol>
		<div class="ibox-tools">
			<?php if ($id == 'all'){ ;?>
				<button id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
			<?php }else{ ;?>
				<button id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">	
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="category_id" name="inputan" type="hidden" class="form-control" value="tagline">	
		<input id="tagline" name="inputan" type="hidden" class="form-control" value="category_id">	
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $obcultus?>">	
		<input id="simo" name="inputan" type="hidden" class="form-control" value="<?php echo $anchor?>">	
		<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $fascia?>">
		
		<div class="form-horizontal">			
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Tahun</label>
				<div class="col-sm-4">
					<div class="input-group col-sm-12" >
						<select class="select_to_select2 form-control m-b" name="inputan"  data-url="module" data-url2="<?php echo $sigilo?>" data-param="list_url"  id="type_id">
							<option value="">Pilih Tahun</option>
							<?php foreach($tahun as $t){ ;?>
								<option value="<?php echo $t->category_id?>" data-url="tahun"><?php echo $t->title?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="" id="data_object">
					<button id="" data-url="module" data-url2="jadwal" data-lang="2" data-param="detail_custom" class="mdl_object btn-sm btn btn-success pull-right "><i class="fa fa-search-plus"></i>  Detail Jadwal</button>
				</div>
			</div>
			<div class="form-group">
				<div class="clearfix" id="data_3">
					<label class="col-sm-2 control-label" style="text-align: left !important">Kuartal</label>
					<div class="col-sm-4">
						<div class="input-group col-sm-12" >
							<select class="select_content2 form-control m-b" name="inputan" id="object_id">
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Pilih Area / Lokasi</label>
				<div class="col-sm-4">
					<div class="input-group col-sm-12" >
						<select class="select_to_select form-control m-b" name="inputan" data-url="module" data-url2="<?php echo $sigilo?>" data-param="list_url" id="area_id">
							<option value="">Pilih Area / Lokasi</option>
							<option value="0" data-url="jembatan">Semua Area</option>
							<?php foreach($kota as $l){ ;?>
								<option value="<?php echo $l->category_id?>" data-url="jembatan" ><?php echo $l->title?></option>
							<?php }?>
						</select>
					</div>
				</div>	
				<div class="clearfix" id="data_2">
					<label class="col-sm-2 control-label" style="text-align: left !important; ">Jembatan</label>
					<div class="col-sm-4" >
						<div class="input-group col-sm-12" >
							<select class="select_content form-control m-b" name="inputan" id="utl_id">
								
							</select>
						</div>
					</div>	
				</div>	
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Nama</label>
				<div class="col-sm-10">
					<input id="title" name="inputan" type="text" class="form-control" value="">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-2" style="text-align: left !important; ">Mulai Tanggal</label>
				<div class="col-sm-4">
					<div class="input-group date input-group col-sm-12">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="datestart" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
					</div>
				</div>
				<label class="control-label col-sm-2" style="text-align: left !important; ">Sampai Tanggal</label>
				<div class="col-sm-4">
					<div class="input-group date input-group col-sm-12">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="dateend" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Aktivasi</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="status">
							<option value="">Pilih Status</option>
							<?php foreach($status as $s){ ;?>
							<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			
			<form class="form-horizontal" method="get">	
				<div class="form-group">
					<label class="col-sm-2 control-label" style="text-align: left !important">Catatan</label>
				</div>
				<div class="mail-box">
					<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
						<div id="description" name="inputan_summer" class="summernote">
							
						</div>
					</div>			
					<div class="mail-body text-right tooltip-demo">
					</div>
				</div>
			
			</form>
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $sigilo;?>" data-param="custom" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Tambah</button>
				<div class="space-25"></div>
				<button id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script><script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>

<script>
$(document).ready(function(){
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>
<script>
$(document).ready(function(){
	$('#data_2').hide();
	$('#data_3').hide();
	$('#data_object').hide();
	$('#type_id').change(function() {
		if ($(this).find(':selected').val() != '') {
			$('#data_3').slideDown('slow');
		} else {
			$('#data_3').slideUp('slow');
		}
	});
	$('#area_id').change(function() {
		if ($(this).find(':selected').val() != '') {
			$('#data_2').slideDown('slow');
		} else {
			$('#data_2').slideUp('slow');
		}
	});
	$('#object_id').change(function() {
			if ($(this).find(':selected').val() != '') {
				var url = $(this).find(':selected').data('param');
				var obj = $(this).find(':selected').data('object');
				$('#tagline').val('');
				$('#category_id').val('');
				$('#data_object').children('.mdl_object').attr('id',$(this).val());	
				$('#tagline').val(''+url+'');		
				$('#category_id').val(''+obj+'');		
				$('#data_object').slideDown('slow');
			} else {
				$('#tagline').val('');	
				$('#category_id').val('');				
				$('#data_object').slideUp('slow');
			}
		});
	$('#datepicker').datepicker({
		format: 'yyyy-mm-dd',
		keyboardNavigation: false,
		forceParse: false
	});

	$('.date').datepicker({
		keyboardNavigation: false,
		todayBtn: 'linked',
		format: 'yyyy-mm-dd',
		forceParse: false
	});
});
</script>

<script id="object_script">
$(document).ready(function(){
	$('.mdl_object').on("click", function(e) {
			e.preventDefault();
			
			$('.pace-done').css('padding-right','');
			$('.btn').each(function(){
				$(this).attr('disabled', 'disabled');
			});
			var id = $(this).attr('id');
			var obj = $(this).data('url');
			var part = $(this).data('url2');
			var lang = $(this).data('lang');
			var param = $(this).data('param');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : id,
				param : param,
				language_id : lang
			};
			$('[id*="modal_"]').each(function(){
				$(this).remove();
			});
			var url = "<?php echo base_url('binamarga');?>/user/"+obj+"/view_detail/"+part;
			$.ajax({
				url : url,
				type: "POST",
				dataType: 'html',
				data: data,
				success: function(html){
					$('#data_object').parent().append('<div class="modal inmodal  animated fadeInRight" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-lg" style="width: 80%">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">JADWAL INSPEKSI</h5>                                        </div>                                        <div class="modal-body">    '+html.replace(/detail2/g, 'mdl_object')+'                                      </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                       </div>                                    </div>                               </div>                            </div>');
					$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
					$('.modal-backdrop').css('display','none');
					$('.btn').each(function(){$(this).removeAttr('disabled');});
					eval(document.getElementById("object_script").innerHTML);
					removeCrud('object_script');
				}
			});
			function removeCrud( itemid ) {
				var element = document.getElementById(itemid); // will return element
				elemen.parentNode.removeChild(element); // will remove the element from DOM
			}
			removeCrud('object_script');
		});
	});
	
</script>

