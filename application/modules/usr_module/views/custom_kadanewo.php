<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo ucfirst($essentiel);?>				
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_category" data-lang="2" class="detail2">
					<?php echo $category->category ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $category->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2">
					<?php echo $category->title ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $category->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="new" data-lang="2" class="detail2">
					Tambah
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<?php if ($id == 'all'){ ;?>
				<button id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
			<?php }else{ ;?>
				<button id="<?php echo $category->category_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">	
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $obcultus?>">		
		<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $fascia?>">
		
		<div class="form-horizontal">
			
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">No. Inspeksi</label>
				<div class="col-sm-4">
					<input id="tagline" name="inputan" type="text" class="form-control" value="">
				</div>
				<label class="col-sm-2 control-label" style="text-align: left !important">Nama</label>
				<div class="col-sm-4">
					<input id="title" name="inputan" type="text" class="form-control" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Tahun</label>
				<div class="col-sm-4">
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="category_id">
							<option value="<?php echo $category->category_id?>"><?php echo $category->category?></option>	
						</select>
					</div>
				</div>
				<label class="col-sm-2 control-label" style="text-align: left !important">Kuartal</label>
				<div class="col-sm-4">
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="object_id">
							<option value="<?php echo $category->object_id?>"><?php echo $category->title?></option>	
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Pilih Area / Lokasi</label>
				<div class="col-sm-4">
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="url_3">
							<option value="">Pilih Area / Lokasi</option>
							<?php if($id == 0){?>
							<?php }else{?>
								<?php foreach($kota as $l){ ;?>
								<option value="<?php echo $l->category_id?>" data-url="<?php echo $l->ccs_key?>" data-area="<?php echo trim($l->metakeyword)?>" data-love="<?php echo  $l->category_id?>"><?php echo $l->title?></option>
								<?php }?>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Jembatan</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="ios_id">
							<option value="">Pilih Jembatan</option>
							<option value="0">Semua Jembatan</option>
							<option value="1">Salah Satu Jembatan</option>
						</select>
					</div>
				</div>	
				<div class="clearfix" id="data_2">
					<label class="col-sm-2 control-label" style="text-align: left !important; ">Jembatan</label>
					<div class="col-sm-4" >
						<div class="input-group col-sm-12" >
							<select class="form-control m-b" name="inputan" id="type_id">
								<option value="">Pilih Jembatan</option>
								<?php foreach($jembatan as $j){ ;?>
									<option value="<?php echo $j->object_id?>"><?php echo $j->title?></option>
								<?php } ;?>
							</select>
						</div>
					</div>	
				</div>	
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" style="text-align: left !important; ">Mulai Tanggal</label>
				<div class="col-sm-4">
					<div class="input-group date input-group col-sm-12">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="datestart" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
					</div>
				</div>
				<label class="control-label col-sm-2" style="text-align: left !important; ">Sampai Tanggal</label>
				<div class="col-sm-4">
					<div class="input-group date input-group col-sm-12">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="dateend" name="inputan" data-date-format="dd/mm/yyyy" type="text" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Aktivasi</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="status">
							<option value="">Pilih Status</option>
							<?php foreach($status as $s){ ;?>
							<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			
			<form class="form-horizontal" method="get">	
				<div class="form-group">
					<label class="col-sm-2 control-label" style="text-align: left !important">Catatan</label>
				</div>
				<div class="mail-box">
					<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
						<div id="description" name="inputan_summer" class="summernote">
							
						</div>
					</div>			
					<div class="mail-body text-right tooltip-demo">
					</div>
				</div>
			
			</form>
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $sigilo;?>" data-param="custom" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Tambah</button>
				<div class="space-25"></div>
				<button id="<?php echo $category->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_object" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script><script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>

<script>
$(document).ready(function(){
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>
<script>
$(document).ready(function(){
	$('#data_2').hide();
		$('#ios_id').change(function() {
			if ($(this).find(':selected').val() === '1') {
				$('#data_2').slideDown('slow');
			} else {
				$('#data_2').slideUp('slow');
			}
		});
	$('#datepicker').datepicker({
		format: 'yyyy-mm-dd',
		keyboardNavigation: false,
		forceParse: false
	});

	$('.date').datepicker({
		keyboardNavigation: false,
		todayBtn: 'linked',
		format: 'yyyy-mm-dd',
		forceParse: false
	});
});
</script>
