<div class="ibox float-e-margins">
	<div class="ibox-title">
		<ol class="breadcrumb col-md-7">
			<li class="active">
				<a id="<?php echo $sigilo?>" data-url="module" data-url2="<?php echo $sigilo;?>"  data-param="view_category" data-lang="2" class="detail2">
					<h5>Jenis Role</h5>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<?php if($cat_count != 6){?>
			<a id="0" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="user" data-lang="1" class="detail2 btn btn-sm btn-primary ">Tambah Jenis Role</a>
			<?php } ;?>
			<button id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
		</div>
	</div>
	<div class="ibox-content">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>		
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Role </th>
					<th>Modul</th>
					<th>Status</th>
					<th>Aktivasi</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($category)){ ?>
				<?php $i = 1; foreach($category as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<?php echo $n['role']->name ?>
					</td>
					<td>
						<?php foreach($n['module'] as $m){ ;?>
							<?php echo $m->name;?>,
						<?php };?>
					</td>
					
					<td><?php if($n['role']->status == 1){ echo "Active" ;}elseif($n['role']->status == 2){echo "On Hold" ;}else{ echo "Inactive" ;};?></td>
					<td>
						<?php if($n['role']->role_id != 0){;?>
						<?php foreach($status as $s){ ;?>
							<?php if($n['role']->status != $s->value){ ;?>
								<button id="<?php echo $n['role']->role_id ?>" data-url="category" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n['role']->name ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php ;} ;?>
						<button id="<?php echo $n['role']->role_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-param="user" data-lang="2" class="dtl btn btn-info btn-xs" type="button">Ubah</button>
						<?php }else{;?>
						-
						<?php };?>
					</td>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
	</div>
</div>