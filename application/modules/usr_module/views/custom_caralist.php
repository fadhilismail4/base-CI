<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					Semua  <?php echo $essentiel?>
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
		</ol>
		<?php } ;?>
		<div class="ibox-tools">
			<?php if(($simo == 0) && ($id == 'all')){ ;?>
				<a id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Tambah Jembatan</a>
			<?php }elseif(($simo == 0) && ($id != 'all')){ ;?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Tambah Jembatan</a>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>Data <?php echo strtolower($essentiel) ?> masih kosong, silahkan isi area untuk masing-masing <?php echo strtolower($essentiel) ?> terlebih dahulu. </h3>
		<?php }else{ ;?>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Nama </th>
					<th>Area </th>
					<th>Admin</th>
					<th>Inspeksi terakhir </th>
					<th>Status</th>
					<th>Aktivasi</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach($objects as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-lang="2" data-param="detail_object" class="detail2">
							Jemb. <?php echo $n->title ?> 
						</a> 
					</td>
					
					<td>
						<a id="<?php echo $n->love?>" data-url="module" data-url2="<?php echo $sigilo?>" data-lang="2" data-param="list" class="detail2">
							<?php echo $n->metadescription ?>
						</a>
					</td>
					<td><?php echo $n->name ?></td>
					<td><?php echo date("l, d M Y", strtotime($n->dateupdated)) ?></td>
					<td>
						<?php foreach($istatus as $j){  ;?>
							<?php if($j->value == $n->status){ echo $j->name ;} ?>
						<?php } ;?>
					</td>
					<td>
						<?php if($simo == 0){?>
							<!--
							<?php foreach($status as $s){ ;?>
								<?php if($n->status != $s->value){ ;?>
									<button id="<?php echo $n->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
								<?php ;} ;?>
							<?php ;} ;?>
							-->
							<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-param="edit_object" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Ubah</button>
						<?php } ;?>
						<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-param="detail_object" data-lang="2" class="dtl btn btn-success ;?> btn-xs" type="button">Lihat</button>
					</td>
				</tr>	
				<?php $i++;} ?>
			</tbody>
		</table>
		<?php };?>
	</div>
</div>