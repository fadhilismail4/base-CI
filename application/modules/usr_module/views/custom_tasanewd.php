<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $inspeksi->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2">
					Jemb. <?php echo $inspeksi->parole ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_custom" data-lang="2" class="detail2">
					<?php echo $inspeksi->chiave ;?>
				</a>
			</li>
			<?php if($lingua == 2){ ;?>
			<li class="active">
				<a id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="view_custom" data-lang="2" class="detail2">
					Lihat Alasan
				</a>
			</li>
			<?php }else{ ;?>
			<li class="active">
				<a id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="view_custom" data-lang="1" class="detail2">
					Buat Alasan
				</a>
			</li>
			<?php } ;?>
		</ol>
		<div class="ibox-tools">
			<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_custom" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Kembali</button>
		</div>
	</div>
	<div class="ibox-content row">	
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="parent_id" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->log_id?>">	
		<input id="log" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->log_id?>">
		<input id="url_id" name="inputan" type="hidden" class="form-control" value="<?php echo $inspeksi->obr_id?>">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $obcultus?>">	
		<input id="simo" name="inputan" type="hidden" class="form-control" value="<?php echo $anchor?>">	
		<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $fascia?>">
		<input id="type" name="inputan" type="text" class="form-control hide" value="reason"></input>		
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<div class="row" id="data_object">
			<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo?>" data-lang="2" data-param="detail_custom" class="mdl_object btn-xs btn btn-success pull-right "><i class="fa fa-search-plus"></i>  Detail Inspeksi</button>
		</div>
		<div class="row">
			
			<div class="col-sm-4" style="padding-right: 0px !important">
				<img alt="image" class="col-md-12 img-responsive" src="
					<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $sigilo ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-right: auto; margin-left: auto">
			</div>
			<div class="col-sm-8 row" style="padding: 5px 0px !important">
				
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Nama</div>
					<div class="col-sm-7 col-xs-6 "><p>: <strong><?php echo $objects->title?></strong></p></div>
					
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">No. Inspeksi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <strong><?php echo $inspeksi->chiave?></strong></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Status Jembatan</div>
					<div class="col-sm-9 col-xs-8 ">
						<p>: <strong>
							<?php foreach($status as $s){ ;?>
								<?php if($s->value == $objects->status){ echo $s->name ;} ;?>
							<?php } unset($s);?>
						</strong></p>
					</div>
				</div>	
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Tanggal Inspeksi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <strong><?php echo date("l, d M Y", strtotime($objects->datecreated)) ?></strong></p></div>
				</div>	
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Lokasi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <?php echo $inspeksi->metadescription?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Tahun</div>
					<div class="col-sm-9 col-xs-8 "><p>: <?php echo $inspeksi->tahun?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-4 ">Tim Inspeksi</div>
					<div class="col-sm-9 col-xs-8 "><p>: <?php echo $objects->name?></p></div>
				</div>			
			</div>
		</div>
	</div>
</div>

<div class="ibox float-e-margins" id="data_content">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				Buat Alasan
			</li>
		</ol>
	</div>
	<div class="ibox-content row">		
		<form class="form-horizontal" method="get">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Judul</label>
				<div class="col-sm-5">
					<input id="title" name="inputan" type="text" class="form-control" value="">
				</div>
				<label class="col-sm-2 control-label" style="text-align: left !important">Status</label>
				<div class="col-sm-3">
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="status">
							<option value="">Pilih Alasan</option>
							<option value="4">Data Kurang Lengkap</option>
							<option value="5">Inspeksi Tidak Valid</option>							
						</select>
					</div>
				</div>
			</div>			
			<div class="form-group">
				<label class="col-sm-12 control-label" style="text-align: left !important">Alasan Inspeksi Ditolak</label>
			</div>
		</form>
		<div class="mail-box">
			<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
				<div id="description" name="inputan_summer" class="summernote">
					
				</div>
			</div>
		
			<div class="mail-body text-right tooltip-demo">
			</div>
		</div>
		<div class="space-25"></div>
			<button id="<?php echo $sigilo;?>" data-param="new_custom" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Tambah</button>
			<div class="space-25"></div>			
			<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_custom" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
			
		</div>
	</div>
</div>
<script id="object_script">
	$('.mdl_object').on("click", function(e) {
		e.preventDefault();
		
		$('.pace-done').css('padding-right','');
		$('.btn').each(function(){
			$(this).attr('disabled', 'disabled');
		});
		var id = $(this).attr('id');
		var obj = $(this).data('url');
		var part = $(this).data('url2');
		var lang = $(this).data('lang');
		var param = $(this).data('param');
		var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : id,
			param : param,
			language_id : lang
		};
		$('[id*="modal_"]').each(function(){
			$(this).remove();
		});
		var url = "<?php echo base_url('binamarga');?>/user/"+obj+"/view_detail/"+part;
		$.ajax({
			url : url,
			type: "POST",
			dataType: 'html',
			data: data,
			success: function(html){
				$('#data_object').parent().append('<div class="modal inmodal  animated fadeInRight" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-lg" style="width: 80%">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">JADWAL INSPEKSI</h5>                                        </div>                                        <div class="modal-body">    '+html.replace(/detail2/g, 'mdl_object')+'                                      </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                       </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				$('.btn').each(function(){$(this).removeAttr('disabled');});
				eval(document.getElementById("object_script").innerHTML);
				removeCrud('object_script');
			}
		});
		function removeCrud( itemid ) {
			var element = document.getElementById(itemid); // will return element
			elemen.parentNode.removeChild(element); // will remove the element from DOM
		}
		removeCrud('object_script');
	});
</script>

<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>

<script>
$(document).ready(function(){
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>

