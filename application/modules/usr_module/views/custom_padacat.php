<div class="ibox float-e-margins">
	<div class="ibox-title">
		<ol class="breadcrumb " style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a href="">
					<?php echo ucfirst($sigilo) ?>
				</a>
			</li>
		</ol>
	</div>
	<div class="ibox-content">
		<div class="row" style="margin: 0px;">
			<?php if($cat_count >= 2 ){ ?>
				<a id="0" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="user" data-lang="0" class="detail2 btn btn-sm btn-success col-md-12">Lihat Role</a>
				<div class="space-15"></div>
			<?php }else{ ;?>
				<a id="0" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="user" data-lang="1" class="detail2 btn btn-sm btn-primary col-md-6">Tambah Role</a>
				<a id="0" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="user" data-lang="0" class="detail2 btn btn-sm btn-success col-md-6">Lihat Role</a>
			<?php } ;?>
		</div>	
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
							<span class="label label-info"></span> <strong>Lihat Semua Akun</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<?php if(!empty($category)){;?>
					<?php foreach($category as $c){;?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<a id="<?php echo $c->type_id;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-lang="" data-param="list" class="detail2">
							<span class="label label-info"></span> <?php echo $c->name;?>
							</a>
							<p class="pull-right"><?php echo '('.$c->total.')' ;?></p> 
						</div>			
					</li>		
					<?php ;};?>
					<?php ;}else{;?>
					<br>No Category Added
					<?php };?>
				</ol>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>	