<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo ucfirst($sigilo);?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $category[0]['role']->role_id?>" data-url="module" data-url2="<?php echo $sigilo;?>"  data-param="edit_category" data-lang="2" class="detail2">
					Update
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="view_category" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Back</button>
		</div>		
	</div>
	<div class="ibox-content row">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Role Name</label>
				<div class="col-sm-10"><input id="name" name="inputan" type="text" class="form-control" value="<?php echo $category[0]['role']->name;?>"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control"><?php echo $category[0]['role']->description?></textarea></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>		
			<input id="ccs_id" name="inputan" type="text" class="form-control hide" value="<?php echo $fascia;?>"></input>
			<input id="module_id" name="inputan" type="text" class="form-control hide" value=""></input>
			<input id="role_id" name="inputan" type="text" class="form-control hide" value="<?php echo $category[0]['role']->role_id?>"></input>
			<div id="language" class="form-group col-sm-12">	
				<?php 
				foreach($module as $c){;?>
					<div class="col-sm-3" style="padding: 10px 0">
						<div class="col-sm-2">
							<input id="<?php echo $c->module_id ;?>" type="checkbox" class="i-checks">
						</div>
						<div class="col-sm-9" style="">
							<i class="fa fa-<?php echo $c->icon;?>"></i> <?php echo $c->name?>
						</div>
					</div>
				<?php };?>
			</div>
		</form>
		<div class="form-group col-sm-12">
			<div class="hr-line-dashed"></div>			
			<div class="col-md-12">
				<button id="<?php echo $sigilo;?>" data-param="<?php echo $sigilo;?>" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Update</button>
				<button id="all" data-url="module" data-url2="role" data-param="list" data-lang="2" class="detail2 btn btn-md btn-white pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('#language input').each(function(){
		if(jQuery.inArray($(this).attr('id'), [<?php foreach($category as $c){ foreach($c['module'] as $cc){ echo '"'.$cc->module_id.'",';};};?>]) !== -1){
			$(this).parent().addClass('checked');
			$(this).prop('checked', true);
			brands();
		}
	});
	
	
	$("#language input").on("ifToggled", function (event) {
		$(".create_mdl").slideDown();
		brands();
	});
	
	function brands() {
		var brands = [];
		$("#language input").each(function (index, value) {
			if ($(this).is(":checked")) {
				brands.push($(this).attr("id"));
			}
		});
		console.log(brands);
		$("#module_id").val(brands);
		if(!brands.length){
			$(".create_mdl").slideUp();
		}
	}
});
</script>