<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					Semua  <?php echo $essentiel?>
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
		</ol>
		<?php } ;?>
		<div class="ibox-tools">
			<?php if($id == 'all'){ ;?>
				<a id="0" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Tambah Lokasi</a>
			<?php }else{ ;?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Tambah Lokasi</a>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>Area <?php echo strtolower($essentiel) ?> masih kosong, silahkan isi area untuk masing-masing <?php echo strtolower($essentiel) ?> terlebih dahulu. </h3>
		<?php }else{ ;?>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Nama </th>
					<th>Kategori </th>
					<th>Admin</th>
					<th>Tanggal Dibuat </th>
					<th>Status</th>
					<th>Aktivasi</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach($objects as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->object_id?>" data-url="module" data-url2="lokasi" data-lang="2" data-param="<?php if($n->type == 'area'){echo "detail_category";}else{echo "detail_object" ;}?>" class="detail2">
							<?php echo $n->title ?> 
						</a> 
						<p style="color: #f39c12"> ( <?php echo ucfirst($n->type)?> )</p>
					</td>
					
					<td>
						<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $key_link ;?>" data-lang="2" data-param="list" class="detail2">
						<?php if($id == 'all'){ ;?>
							<?php foreach($categories as $c){ ?>
								<?php if($n->category_id == $c->category_id){ echo $c->title ;} ;?>
							<?php } ;?>
						<?php }elseif($id != 0){ ;?>
							<?php if($n->category_id == $categories->category_id){ echo $categories->title ;} ;?>
						<?php } ;?>
						</a>
					</td>
					<td><?php echo $n->name ?></td>
					<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
					<td><?php if($n->status == 1){ echo "Aktif" ;}else{echo "Tidak Aktif" ;};?></td>
					<td>
						<?php foreach($status as $s){ ;?>
							<?php if($n->status != $s->value){ ;?>
								<button id="<?php echo $n->object_id ?>" data-url="<?php if($n->type == 'area'){echo "category" ;}else{echo "objects" ;}?>" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $key_link?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php ;} ;?>
						<!--
						<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Ubah</button>
						-->
					</td>
				</tr>	
				<?php $i++;} ?>
			</tbody>
		</table>
		<?php };?>
	</div>
</div>