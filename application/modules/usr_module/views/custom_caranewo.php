<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->metakeyword ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="new" data-lang="2" class="detail2">
					Tambah
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
		</div>
	</div>
	
	<div class="ibox-content row">	
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $essentiel?>">	
		<input id="category_id" name="inputan" type="hidden" class="form-control" value="<?php echo $segretis?>">	
		<div class="row">
			<div class="col-sm-4">
				<input id="image_square" name="image_square" class="file" type="file" value="">
				<div class="col-sm-12" style="margin-top: 20px !important">
					<a id="1" data-lang="2" data-param="latitude" data-param2="longitude" data-title="" class="modal_maps btn btn-success btn-block"><i class="fa fa-globe"></i>  Add Location</a>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="form-horizontal">
					<div class="form-group">
						<div class="clearfix" id="data_3">
							<label class="col-sm-3 control-label" style="text-align: left !important">Pilih Lokasi / Area</label>
							<div class="col-sm-9">
								<div class="input-group col-sm-12" >
									<select class="form-control m-b" name="inputan" id="love">
										<option value="<?php echo $categories->category_id?>"><?php echo $categories->title?></option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" style="text-align: left !important">Nama</label>
						<div class="col-sm-9">
							<input id="title" name="inputan" type="text" class="form-control" value="">
						</div>
					</div>
					<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align: left !important">No. Jembatan</label>
						<div class="col-sm-5">
							<input id="tagline" name="inputan" type="text" class="form-control" value="">
						</div>
						<label class="col-sm-2 control-label" style="text-align: left !important">Jml Bent</label>
						<div class="col-sm-2">
							<input id="number_5" name="inputan" type="text" class="form-control" value="" placeholder="">
						</div>
					</div>
					<div class="form-group" >				
						<label class="col-sm-3 control-label" style="text-align: left !important">Tahun Dibangun</label>
						<div class="col-sm-4">
							<div class="input-daterange input-group col-sm-12" id="datepicker">
								<input id="datepublish" name="inputan"type="text" class="input-sm form-control"/>
							</div>
						</div>	
						<label class="col-sm-2 control-label" style="text-align: left !important; ">Tipe Jalan</label>
						<div class="col-sm-3" >
							<div class="input-group col-sm-12" >
								<select class="form-control m-b" name="inputan" id="url_2">
									<option value="">Pilih Tipe Jalan</option>
									<option value="0">Jalan Nasional</option>
									<option value="1">Jalan Provinsi</option>
									<option value="2">Jalan Kota / Kabupaten</option>
									<option value="3">Jalan Kecamatan</option>
								</select>
							</div>
						</div>	
					</div>
					<div class="form-group" >	
						<label class="col-sm-3 control-label" style="text-align: left !important">Lokasi Dari</label>
						<div class="col-sm-4">
							<input id="char_2" name="inputan" type="text" class="form-control" value="" placeholder="">
						</div>
						<label class="col-sm-2 control-label" style="text-align: left !important">KM</label>
						<div class="col-sm-2">
							<input id="number_4" name="inputan" type="text" class="form-control" value="" placeholder="KM ">
						</div>	
					</div>
					<div class="form-group" >						
						<label class="col-sm-3 control-label" style="text-align: left !important">Panjang</label>
						<div class="col-sm-2">
							<input id="number_1" name="inputan" type="text" class="form-control" value="" placeholder="meter">
						</div>
						<label class="col-sm-2 control-label" style="text-align: left !important">Lebar</label>
						<div class="col-sm-2">
							<input id="number_2" name="inputan" type="text" class="form-control" value="" placeholder="meter">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label" style="text-align: left !important; ">Status Jembatan</label>
						<div class="col-sm-4" >
							<div class="input-group col-sm-12" >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Pilih Status</option>
									<?php foreach($status as $s){ ;?>
									<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
									<?php }?>
								</select>
							</div>
						</div>	
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ibox float-e-margins">
	<div class="ibox-content row">
		<form class="form-horizontal" method="get">	
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Ruas Jalan</label>
				<div class="col-sm-10">
					<textarea id="char_3" name="inputan" type="text" class="form-control" value="" placeholder="Nama ruas jalan... "></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Catatan</label>
			</div>
			<div class="mail-box">
				<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
					<div id="description" name="inputan_summer" class="summernote">
						
					</div>
				</div>			
				<div class="mail-body text-right tooltip-demo">
				</div>
			</div>
		
		</form>
		
		<div class="col-sm-12">
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $sigilo;?>" data-param="jembatan" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Tambah</button>
				<div class="space-25"></div>	
				<button id="<?php echo $id?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>

<script>
$(document).ready(function(){
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
	
	
	$('#datepicker').datepicker({
		format: " yyyy", // Notice the Extra space at the beginning
		viewMode: "years", 
		minViewMode: "years",
		keyboardNavigation: true,
		forceParse: false
	});
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php echo base_url('assets').'/img/image_empty.png' ;?>' class='file-preview-image'>"
		],			
		showRemove: false,
		showUpload: false
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>