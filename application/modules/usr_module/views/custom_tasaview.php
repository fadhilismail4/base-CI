<div class="ibox float-e-margins">
	<div class="row ibox-title">
		<ol class="breadcrumb col-md-7">
			<li class="active">
				<a id="<?php echo $essentiel_id?>" data-url="module" data-url2="<?php echo $sigilo;?>"  data-param="view_category" data-lang="2" class="detail2">
					 <h5>Semua <?php echo $essentiel;?></h5>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
		</div>
	</div>
	<div class="row ibox-content">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>		
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Nama </th>
					<th>Deskripsi </th>
					<th>Admin</th>
					<th>Tanggal Dibuat </th>
					<th>Status</th>
					<?php if($simo == 0){?>
					<th>Aktivasi</th>
					<?php }?>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($category)){ ?>
				<?php $i = 1; foreach($category as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-lang="2" data-param="detail_category" class="detail2">
							<?php echo $n->title ?>
						</a>
					</td>
					<td>
						<?php echo word_limiter($n->description, 3) ;?>
					</td>
					<td><?php echo $n->name ?></td>
					<td><?php echo date("l, d M Y", strtotime($n->datecreated)) ?></td>
					<td><?php if($n->status == 1){ echo "Aktif" ;}else{ echo "Tidak Aktif" ;};?></td>
					
					<?php if($simo == 0){?>
					<td>
						<?php foreach($status as $s){ ;?>
							<?php if($n->status != $s->value){ ;?>
								<button id="<?php echo $n->category_id ?>" data-url="category" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php ;} ;?>
						<button id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $essentiel?>" data-param="edit_category" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Ubah</button>
					</td>
					<?php }?>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
	</div>
</div>