<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5><?php echo $essentiel;?> Connection</h5>
	</div>
	<div class="ibox-content">
		<div class="row">
			<?php if(!empty($datas)){ foreach($datas as $ds){ 
				if($ds->avatar){
					$creator_img = base_url('assets/'.$zone.'/user/'.strtolower($ds->type).'/'.$ds->createdby.'/'.$ds->avatar.'');
				}else{
					$creator_img = base_url('assets/img/admin_empty.jpg');
				};
			;?>
			<div class="col-lg-6">
				<div class="contact-box"style="min-height: 225px">
					<a href="<?php echo base_url('')?>admin/youth/detail/<?php echo $ds->user_id?>">
						<div class="col-sm-12">
							<div class="text-center">
								<img alt="image" class="img-circle m-t-xs img-responsive" src="<?php echo $creator_img;?>" style="margin: auto;">
							</div>
						</div>
						<div class="col-sm-12" style="min-height: 25px">
							<h3 class="text-center">
								<strong><?php echo $ds->name?></strong>
							</h3>
							<p></p>
							<address class="text-center">
								<?php if($ds->address){?>
								<i class="fa fa-map-marker"></i> <?php echo $ds->address ;?>
								<?php ;};?>
								<br>
								<i class="fa fa-envelope-o"></i> <?php echo $ds->email?> 
								<?php if($ds->phone){?> <br>
								<i class="fa fa-phone"></i> <?php echo $ds->phone?> 
								<?php ;};?>
								<?php if($ds->mobile){?> <br>
								<i class="fa fa-mobile-phone"></i> <?php echo $ds->mobile?> 
								<?php ;};?>						
							</address>		
						</div>
						<br>
						<p class="text-center">						
						<?php echo word_limiter($ds->description, 25) ;?>
						</p>
						<div class="clearfix"></div>
					</a>
				</div>
			</div>
			<?php };}else{ ;?>
			<div class="col-md-12">Let's start Make a Connection</div>
			<?php };?>
		</div>
	</div>
</div>
