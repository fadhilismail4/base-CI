<div class="chat-form">
	<form role="form">
		<div class="form-group">
			<textarea class="form-control" placeholder="Message"></textarea>
		</div>
		<div class="text-right">
			<button type="submit" class="btn btn-sm btn-primary m-t-n-xs"><strong>Send message</strong></button>
		</div>
	</form>
</div>