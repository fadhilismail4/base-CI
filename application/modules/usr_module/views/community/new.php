<div id="new_form" class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content" style="border:0px">
				<div class="alert alert-danger" id="fail" style="display:none;"></div>
				<div class="alert alert-info" id="success" style="display:none;"></div>
				<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $fascia;?>">
				<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $essentiel_id;?>">
				<input id="status" name="inputan" type="hidden" class="form-control" value="1">
				<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
				<div class="form-horizontal">
					<div class="form-group ">
						<div class="row">
							<div class="col-md-3">
								<input id="image_square" name="image_square" class="file" type="file" value="">
							</div>	
							<div class="col-md-9">
								<div class="col-md-8">
									<div class="form-group ">
										<label class="col-sm-2 control-label" style="text-align: left !important">Title</label>
										<div class="col-sm-10">
											<input id="title" name="inputan" type="text" class="form-control" value="">
										</div>
									</div>
									<?php if(!empty($datas)){;?>
									<div class="form-group ">
										<label class="col-sm-2 control-label" style="text-align: left !important">Category</label>
										<div class="col-sm-10">
											<div class="input-group col-md-12">
												<select class="form-control m-b" name="inputan" id="category_id">
													<?php foreach($datas as $d){;?>
														<option value="<?php echo $d->category_id;?>"><?php echo $d->title;?></option>
													<?php };?>
												</select>
											</div>
										</div>
									</div>
									<?php };?>
								</div>
								<div class="col-lg-4">
									<button id="<id>" data-param="<param>" data-param2="<param2>" class="create_mdl btn btn-block btn-primary">Create</button>
									<div class="space-15"></div>
									<button class="del_this btn btn-block btn-warning">Cancel</button>
									<div class="clearfix"></div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="col-sm-2 control-label" style="text-align: left !important">Content</label>
										<div class="h-200" style="padding: 15px;display: inline-block; width: 100%;">
											<div id="description" name="inputan_summer" class="summernote">
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>