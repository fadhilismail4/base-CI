<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Recent Activites</h5>
		<button type="button" id="<?php echo $youth->user_id;?>" data-lang="" data-url="youth" data-url2="activities" class="detail btn btn-default btn-sm  pull-right"><i class="fa fa-puzzle-piece"></i> Youth Dashboard</button>
	</div>
	<div class="ibox-content">
		<div>
			<div class="feed-activity-list">
				<?php foreach($activities as $a){?>
					<div class="feed-element">
						<a href="" class="pull-left">
							<img alt="image" class="img-circle" src="<?php echo base_url()?>assets/img/avatar/member/<?php if($youth->avatar){ echo $youth->avatar;}else{ echo "image_empty.png"  ;} ;?>">
						</a>
						<div class="media-body ">
							<small class="pull-right text-navy">
							<?php 
								$created = strtotime($a->datecreated); 
								$nows=strtotime(date('Y-m-d H:i:s')); 
								$range=$nows-$created; 
								$jam = round(($range )/60)
							;?>
							
							<?php 
								if( $today == date("Y-m-d", strtotime($a->datecreated))){ 
									if($jam < 60){
										echo $jam." minutes ago"
									;}else{
										echo round($jam/60)." hour ago"
									;}
								;}else{ 
									if($jam < 2880){
										echo " yesterday"
									;}else{
										echo (date("D M Y", strtotime($a->datecreated)))
									;}
								;}
							;?>
							</small>
							
							<?php if( $a->category_id == 0){ ;?>
								<?php if($a->url_id == 0){ echo "have been logout." ;}else{ echo "have been login." ;}?>
							<?php ;}elseif(($a->category_id == 1) && ($a->url_id == $youth->user_id)){ ;?>	
								<strong>
									<a href="<?php echo base_url()?>admin/youth/detail/<?php echo $a->con_id ;?>">
										<?php echo $a->name?>
									</a> 
								</strong> started following <strong>you</strong>. 
							<?php ;}elseif(($a->category_id == 1) && ($a->user_id == $youth->user_id)){ ;?>											
								<strong>
									<?php echo $youth->name?>
								</strong> 
								started following
								<strong>
									<a href="<?php echo base_url()?>admin/youth/detail/<?php echo $a->con_id ;?>">
										<?php echo $a->name?>
									</a> 
								</strong>. 
							<?php ;}elseif(($a->status == 1) && ($a->category_id > 1 )){ ;?>										
								<strong>
									<?php echo $youth->name?>
								</strong> 
								started following
								<strong>
									<a href="
										<?php if($a->category_id == 2){
													echo site_url()."admin/organization/detail/".$a->url_id;
												}elseif($a->category_id == 3){
													echo site_url()."admin/ideas/detail/".$a->url_id;
												}else{
													echo site_url()."admin/development/detail/".$a->url_id;
												}
										?>">
										<?php echo $a->name?>
									</a> 
								</strong>. 
							<?php ;}else{ ;?>
								<strong>
									<?php echo $youth->name?>
								</strong> 
								started to support
								<strong>
									<a href="
										<?php if($a->category_id == 2){
													echo site_url()."admin/organization/detail/".$a->url_id;
												}elseif($a->category_id == 3){
													echo site_url()."admin/ideas/detail/".$a->url_id;
												}else{
													echo site_url()."admin/development/detail/".$a->url_id;
												}
										?>">
										<?php echo $a->name?>
									</a> 
								</strong>. 
							<?php ;} ;?>
							<br>
							<small class="text-muted">
								<?php if( $today == date("Y-m-d", strtotime($a->datecreated))){ echo "Today ".date("H:i", strtotime($a->datecreated)) ;}else{ echo date("H:i:s a, d M Y", strtotime($a->datecreated))  ;} ;?>
							</small>												
							<?php foreach($actinfo as $i){ ?>
								<?php if($i->category_id < 3){?>
									<?php if(($a->category_id ==1)&& ($a->url_id == $youth->user_id) && ($a->user_id == $i->user_id)){ ;?>
										<div class="photos">								
											<div class="pull-left" style="padding-right: 10px;">
												<img alt="image" class="img-circle" src="<?php echo base_url()?>assets/img/avatar/member/<?php if($i->image_square){ echo $i->image_square ;}else{ echo "image_empty.png" ;} ;?>">
											</div>
											<div class="media-body ">
												<a href="<?php echo base_url('') ?>admin/youth/detail/<?php echo $i->con_id?>">
													<h5><?php echo $a->name ;?></h5>
												</a>
												<p>
													<?php echo word_limiter($i->description,51)?>
												</p>
											</div>															
										</div>
									<?php }elseif(($a->category_id ==1)&& ($a->user_id == $youth->user_id) && ($a->url_id == $i->user_id)){ ;?>
										<div class="photos">								
											<div class="pull-left" style="padding-right: 10px;">
												<img alt="image" class="img-circle" src="<?php echo base_url()?>assets/img/avatar/member/<?php if($i->image_square){ echo $i->image_square ;}else{ echo "image_empty.png" ;} ;?>">
											</div>
											<div class="media-body ">
												<a href="<?php echo base_url('') ?>admin/youth/detail/<?php echo $i->url_id?>">
													<h5><?php echo $a->name ;?></h5>
												</a>
												<p>
													<?php echo word_limiter($i->description,51)?>
												</p>
											</div>															
										</div>
									<?php ;}elseif(($a->category_id ==2)&& ($a->url_id == $i->url_id)){ ;?>
										<div class="photos">								
											<div class="pull-left" style="padding-right: 10px;">
												<img alt="image" class="img-circle" src="<?php echo base_url()?>assets/img/avatar/org/<?php if($i->image_square){ echo $i->image_square ;}else{ echo "image_empty.png" ;} ;?>">
											</div>
											<div class="media-body ">
												<a href="<?php echo base_url('') ?>admin/organization/detail/<?php echo $i->url_id?>">
													<h5><?php echo $a->name ;?></h5>
												</a>
												<p>
													<?php echo word_limiter($i->description,51)?>
												</p>
											</div>															
										</div>
									<?php ;} ;?>
								<?php }elseif(($a->category_id > 2) && ($a->category_id < 4) && ($a->category_id === $i->category_id )){ ;?>
									<div class="photos">								
										<div  class="pull-left" style="padding-right: 10px;">
											<img alt="image" class="img-circle" src="<?php echo base_url()?>assets/img/avatar/idea/<?php if($i->image_square){ echo $i->image_square ;}else{ echo "image_empty.png" ;} ;?>">
										</div>
										<div class="media-body ">
											<a href="<?php echo base_url('') ?>admin/ideas/detail/<?php echo $i->url_id?>">
												<h5><?php echo $a->name ;?></h5>
											</a>
											<p>
												<?php echo word_limiter($i->description,51)?>
											</p>
										</div>															
									</div>
								<?php }elseif(($a->category_id > 3) && ($a->category_id < 5) && ($a->category_id === $i->category_id )){ ;?>
									<div class="photos">								
										<div class="pull-left" style="padding-right: 10px;">
										<img alt="image" class="img-circle" src="<?php echo base_url()?>assets/img/avatar/dev/<?php if($i->image_square){ echo $i->image_square ;}else{ echo "image_empty.png" ;} ;?>">
										</div>
										<div class="media-body ">
											<a href="<?php echo base_url('') ?>admin/development/detail/<?php echo $i->url_id?>">
												<h5><?php echo $a->name ;?></h5>
											</a>
											<p>
												<?php echo word_limiter($i->description,51) ?>
											</p>
										</div>															
									</div>
								<?php ;} ;?>
							<?php } ;?>
						</div>
					</div>										
				<?php }?>
			</div>
			<button class="btn btn-primary btn-block m"><i class="fa fa-arrow-down"></i> Show More</button>
		</div>
	</div>
</div>
