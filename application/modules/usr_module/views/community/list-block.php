<div class="col-lg-3 col-xs-12" style="background: #fff;">
	<div class="widget white-bg">
		<div class="row vertical-align">
			<div class="col-xs-3">
				<h3><?php echo $essentiel;?></h3>
			</div>
			<div class="col-xs-9 text-right">
				<h3 class="font-bold"></h3>
			</div>
		</div>
	</div>
</div>
<a class="col-lg-3 col-sm-4 col-xs-12" style="background: #fff;">
	<div class="widget lazur-bg">
		<div class="row vertical-align">
			<div class="col-xs-3">
				<h3>All</h3>
			</div>
			<div class="col-xs-9 text-right">
				<h3 class="font-bold"></h3>
			</div>
		</div>
	</div>
</a>
<div class="col-lg-3 col-sm-4 col-xs-6" style="background: #fff;">
	<div class="widget lazur-bg">
		<div class="row vertical-align">
			<div class="col-xs-3">
				<h3>Own</h3>
			</div>
			<div class="col-xs-9 text-right">
				<h3 class="font-bold">2</h3>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-3 col-sm-4 col-xs-6" style="background: #fff;">				
	<div class="widget lazur-bg">
		<div class="row vertical-align">
			<div class="col-xs-3">
				<h3>Following</h3>
			</div>
			<div class="col-xs-9 text-right">
				<h3 class="font-bold">120</h3>
			</div>
		</div>
	</div>
</div>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<button type="button" id="1" data-lang="" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="new" data-param2="o" class="modal_create btn btn-warning btn-sm  pull-right"><i class="fa fa-lightbulb-o"></i> Add New <?php echo $essentiel;?></button>
		<div id="policy" class="row" style="display:none;">
			<div class="col-lg-12">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum semper lacus, non molestie sem ultricies non. Phasellus molestie vitae mauris vel viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce non ante velit. Fusce scelerisque, dolor at feugiat sollicitudin, nisi augue varius urna, ut aliquam libero metus nec quam. In suscipit hendrerit velit. Nunc elementum neque et felis ultrices, et convallis massa cursus. Donec dolor nulla, mattis non gravida id, scelerisque sed orci. Nulla dapibus magna in placerat tincidunt. Sed a tempus mauris, et consequat lectus. Pellentesque gravida ullamcorper est eget laoreet. Sed lobortis metus ut libero efficitur varius. Curabitur nisl tortor, ullamcorper a efficitur vel, efficitur et est. Nam ultricies suscipit metus, quis pulvinar lacus feugiat vel. Nunc ac blandit urna, non lobortis erat.</p>
				<br>
				<p>Maecenas ornare eget lorem iaculis scelerisque. Nunc massa nulla, maximus non nibh vel, aliquet ultricies tellus. Nam id mi tempor, maximus risus eget, convallis turpis. Vestibulum elementum ac nisl ut scelerisque. Donec cursus mi ac vehicula semper. Sed dignissim scelerisque eleifend. Morbi quis nunc ac justo efficitur dictum scelerisque sed odio. Ut vel nisi tristique, fringilla est sit amet, lacinia mauris. Nam viverra massa nec arcu suscipit suscipit. Sed pretium facilisis euismod. Sed maximus arcu id ante lobortis, sit amet rhoncus sem lacinia. Vestibulum placerat justo velit, eget tempus libero pretium ut. Integer turpis leo, vestibulum nec libero quis, consectetur sollicitudin lacus.</p>
				<br>
				<p class="pull-right"><input type="checkbox" class="display_new_form"> I Agree nih</input></p>
			</div>
		</div>
	</div>
	<div class="ibox-content">
		<div class="row">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<?php foreach($datas as $d){?>
			<div id="detail_on_page_<?php echo $d->object_id;?>" class="col-md-4 col-sm-6">
				<div style="border-top: 4px solid #f7a54a">
					<div id="ava2" class="ibox-content no-padding border-left-right">
						<img id="ava" alt="image" class="img-responsive" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo $sigilo;?>/<?php echo $d->image_square;?>" style="width: 100%;">
					</div>
					<div style="border: 2px solid #F3F3F4;">
						<div class="ibox-content profile-content">
							<a href="#" id="<?php echo $d->object_id;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail" data-param2="o" data-lang="2" class="detail_next"><h4><strong><?php echo $d->title;?></strong></h4></a>
							<small><?php echo date('d F Y',strtotime($d->dateupdated));?></small>
							<h5>
								<strong>by: </strong>
									<img alt="image" class="img-circle m-t-xs" src="
									<?php if($d->avatar){ ;?>
										<?php echo base_url('assets/'.$zone.'/user/'.strtolower($d->name).'/'.$d->createdby.'')?>/<?php echo $d->avatar ;?>
									<?php ;}else{ ;?>
										<?php echo base_url('assets/img/admin_empty.jpg');?>
									<?php ;}?>
									" style="width:50px">
									<?php echo $d->creator;?>
							</h5>
							<p class="text-justify">
								<?php if(!empty(trim($d->description))){echo word_limiter(str_replace('"',' ',strip_tags(trim($d->description))),20);}else{echo '-';};?>
							</p>
							<hr>
							<p><i class="fa fa-tags"></i></p>
						</div>
						<div class="row">
							<?php if($d->createdby != $fattore){;?>
							<a id="action" data-param="join" data-param2="<?php echo $d->object_id;?>" class="create_mdl">
							<?php }else{;?>
							<a id="#">
							<?php };?>
								<div class="col-sm-6 col-xs-6">
									<div class="ibox-content" style="background-color:transparent;">
										<div class="row" style="text-align: center;">
											<p style="color:#f7a54a">0</p>
											<i class="fa fa-circle-o-notch"></i> JOIN
										</div>
									</div>	
								</div>
							</a>
							<a href="#">
								<div class="col-sm-6 col-xs-6">
									<div class="ibox-content" style="background-color:transparent;">
										<div class="row" style="text-align: center;">
											<p style="color:#f7a54a">0</p>
											<i class="fa fa-thumbs-o-up"></i> LIKE
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php };?>
		</div>
	</div>
</div>
<script>
// end ajax to view maps

$('.modal_create').on("click", function(e) {
	e.preventDefault();
	var id = $(this).attr('id');
	var param = $(this).data('param');
	var param2 = $(this).data('param2');
	var url = $(this).data('url');
	var url2 = $(this).data('url2');
	var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : id,
			param : param,
			param2 : param2
		};
	$('.display_new_form').prop('checked', false);	
	$('#policy').slideDown();
	$.ajax({
		url : "<?php echo base_url($zone);?>/user/module/view_detail/"+url2,
		type: "POST",
		dataType: 'html',
		data: data,
		success: function(html){
			var view_file = html.replace('<id>','<?php echo $sigilo;?>').replace('<param>','create');
			$('.modal_create').parent().append(view_file); 	
			$('#new_form').hide(); 	
			$('.modal_create').slideUp();
			$(".file").fileinput("refresh",{
				initialPreview: [
					"<img src='<?php echo base_url('assets').'/img/image_empty.png' ;?>' class='file-preview-image'>"
				],			
				showRemove: false,
				showUpload: false
			});
			$('.summernote').summernote({
				onImageUpload: function(files) {
					url = $(this).data('upload'); //path is defined as data attribute for  textarea
					sendFile(files[0], url, $(this));
				}
			});
			var edit = function() {
				$('.click2edit').summernote({focus: true});
			};
			var save = function() {
				var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
				$('.click2edit').destroy();
			};
			$('.del_this').on("click", function(e) {
				$('#new_form').slideUp().remove();
				$('.modal_create').slideDown();
			});
			eval(document.getElementById("crud_script").innerHTML);
		}
	});
		
	 function removeCrud( itemid ) {
		var element = document.getElementById(itemid); // will return element
		elemen.parentNode.removeChild(element); // will remove the element from DOM
	}
	removeCrud('crud_script'); 
});

$('.detail_next').on("click", function(e) {
	e.preventDefault();
	var id = $(this).attr('id');
	var param = $(this).data('param');
	var param2 = $(this).data('param2');
	var url = $(this).data('url');
	var url2 = $(this).data('url2');
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		id : id,
		param : param,
		param2 : param2
	};
	$('[id*="detail_on_page_"]').each(function(){
		$(this).not('#detail_on_page_'+id).slideUp();
	});
	$('.detail_on_page_content').each(function(){
		$(this).slideUp();
		$(this).remove();
	});
	$('#detail_on_page_'+id).after('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
	$.ajax({
		url : "<?php echo base_url($zone);?>/user/module/view_detail/"+url2,
		type: "POST",
		dataType: 'html',
		data: data,
		success: function(html){
			$('.sk-spinner').remove();
			var id = $($.parseHTML(html)).filter('.detail_on_page_content').attr('id');
			$('#detail_on_page_'+id).after(html); 	
			$('.remove_detail').on("click", function(e) {
				$('.detail_on_page_content').each(function(){
					$(this).slideUp();
					$(this).remove();
				});
				$('[id*="detail_on_page_"]').each(function(){
					$(this).slideDown();
				});
			});
		}
	});
});

$('.display_new_form').on("click", function(e) {
	$('#new_form').slideDown();
	$('#policy').slideUp();
	$('.display_new_form').prop('checked', true);
});
</script>