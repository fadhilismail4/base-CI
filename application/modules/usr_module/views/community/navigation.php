<div class="col-lg-3">
	<div class="widget style1 yellow-bg">
		<div class="row vertical-align">
			<div class="col-xs-3">
				<i class="fa fa-shield fa-3x"></i>
			</div>
			<div class="col-xs-9 text-right">
				<h2 class="font-bold">610</h2>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-3">				
	<div class="widget style1 lazur-bg">
		<div class="row vertical-align">
			<div class="col-xs-3">
				<i class="fa fa-phone fa-3x"></i>
			</div>
			<div class="col-xs-9 text-right">
				<h2 class="font-bold">120</h2>
			</div>
		</div>
	</div>
</div>