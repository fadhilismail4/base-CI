<?php// print_r($datas);?>
<div id="<?php echo $datas->object_id;?>" class="detail_on_page_content col-lg-8" style="padding: 0;">
	<div class="ibox float-e-margins">
		<div class="ibox-content" style="border:0px">
			<div class="pull-right">
				<a class="remove_detail btn btn-white btn-bitbucket">
					<i class="fa fa-remove"></i>
				</a>
			</div>
			<h1><?php echo $datas->title;?></h1>
			<h3><?php if(isset($datas->category)){ echo $datas->category;}?></h3>
			<i class="fa fa-eye"></i> <?php echo $datas->viewer;?>
			<i class="fa fa-heart-o"></i> <?php echo $datas->love;?>
			<small><?php echo $datas->datecreated;?></small>
			<?php echo $datas->description;?>
		</div>
	</div>
</div>