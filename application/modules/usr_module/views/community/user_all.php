<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Other Member</h5>
	</div>
	<div class="ibox-content">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<table class="table" style="margin-bottom: 0px">
			<tbody>
			<?php foreach($datas as $d){
				if($d->avatar){
					$creator_img = base_url('assets/'.$zone.'/user/'.strtolower($d->type).'/'.$d->user_id.'/'.$d->avatar.'');
				}else{
					$creator_img = base_url('assets/img/admin_empty.jpg');
				};
				?>
			<tr>
				<td>
					<img alt="image" class="img-circle" style="width:50px" src="<?php echo $creator_img;?>">
				</td>
				<td><?php echo $d->name;?></td>
				<td><?php echo $d->type;?></td>
				<td><button id="action" data-param="follow" data-param2="<?php echo $d->user_id;?>" class="create_mdl btn btn-outline btn-default btn-sm">follow</button></td>
			</tr>
			<?php };?>
			</tbody>
		</table>

	</div>
</div>