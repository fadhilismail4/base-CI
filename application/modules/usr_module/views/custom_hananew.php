<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_link);?>				
				</a>
			</li>
			<?php if($id != 0){ ;?>
			<li class="">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $cat->title ;?>
				</a>
			</li>
			<?php } ;?>
			<?php if($id != 0){ ;?>
			<li class="active">
				<a id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
					Tambah
				</a>
			</li>
			<?php }else{ ;?>
			<li class="active">
				<a id="0" data-url="module" data-url2="<?php echo $key_link;?>" data-param="new" data-lang="2" class="detail2">
					Tambah
				</a>
			</li>
			<?php } ;?>
		</ol>
		<div class="ibox-tools">
			<?php if ($id == 0){ ;?>
				<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
			<?php }else{ ;?>
				<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">	
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
		<div class="form-horizontal">	
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Nama</label>
				<div class="col-sm-4">
					<input id="title" name="inputan" type="text" class="form-control" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Deskripsi</label>
				<div class="col-sm-10">
					<div class="col-sm-12" style="padding: 0"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
				</div>
			</div>		
			
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important";>Kategori</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="category_id">						
							<?php if($id == 0){ ;?>
								<option value="">Pilih Kategori</option>								
								<?php foreach($category as $cat){;?>
								<option value="<?php echo $cat->category_id ;?>"><?php echo $cat->title ;?></option>
								<?php } ;?>
							<?php }else{ ;?>
								<option value="<?php echo $id?>">	<?php echo $cat->title ;?></option>			
							<?php };?>
						</select>
					</div>
				</div>
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Aktivasi</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="status">
							<option value="">Pilih Status</option>
							<?php foreach($status as $s){ ;?>
							<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="object" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Tambah</button>
				<div class="space-25"></div>					
				<?php if ($id == 0){ ;?>
					<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
				<?php }else{ ;?>
					<button id="<?php echo $cat->category_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="list" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
				<?php } ;?>
			</div>
		</div>
	</div>
</div>