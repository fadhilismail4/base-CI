<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					Semua Role dan User
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					Role
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $role->name ;?>
				</a>
			</li>
		</ol>
		<?php } ;?>
		<div class="ibox-tools">
			<?php if($c_objects != 4){?>
			<a id="<?php echo $id ?>" data-url="module" data-url2="profil" data-param="user" data-lang="1" class="detail2 btn-sm btn btn-primary ">Tambah Akun</a>
			<?php } ;?>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>Kosong... </h3>
		<?php }else{ ;?>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Name </th>
					<th>Category </th>
					<th>Status</th>
					<th>Aktivasi</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach($objects as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-lang="2" data-param="detail_object" class="detail2">
							<?php echo $n->title ?>
						</a>
					</td>
					
					<td>
						<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-lang="2" data-param="detail_category" class="detail2">
							<?php echo $n->category_id ;?>
						</a>
					</td>
					<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{echo "On Hold" ;};?></td>
					<td>
						<?php if($n->user_id != $user_id){;?>
						<?php foreach($status as $s){ ;?>
							<?php if($n->status != $s->value){ ;?>
								<button id="<?php echo $n->user_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->name ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php ;} ;?>
						<button id="<?php echo $n->user_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-param="edit_object" data-lang="2" class="dtl btn btn-info btn-xs" type="button">Ubah</button>
						<?php }else{;?>
						-
						<?php };?>
					</td>
				</tr>	
				<?php $i++;} ?>
			</tbody>
		</table>
		<?php };?>
	</div>
</div>