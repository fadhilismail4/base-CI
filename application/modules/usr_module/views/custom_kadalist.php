<div class="ibox">
	<div class="ibox-title row">
		<?php if ($id == 'all'){ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="active">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					Semua  <?php echo $essentiel?>
				</a>
			</li>
		</ol>
		<?php }else{ ;?>
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $categories->category_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $categories->title ;?>
				</a>
			</li>
		</ol>
		<?php } ;?>
		<div class="ibox-tools">
			<!--
			<?php if(($id == 'all') && ( $simo == 0)){ ;?>
				<a id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Buat Jadwal</a>
			<?php }elseif(($id != 'all') && ( $simo == 0)){ ;?>
				<a id="<?php echo $id ?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary ">Buat Jadwal</a>
			<?php } ;?>
			-->
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<?php if($c_objects == 0){ ;?>	
			<h3>Data <?php echo strtolower($essentiel) ?> masih kosong, silahkan isi area untuk masing-masing <?php echo strtolower($essentiel) ?> terlebih dahulu. </h3>
		<?php }else{ ;?>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>No. Inspeksi </th>
					<th>Tahun </th>
					<th>Admin</th>
					<th>Tanggal Inspeksi </th>
					<th>Status</th>
					<?php if($simo == 0){?>
					<th>Aktivasi</th>
					<?php }?>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach($objects as $n){ ;?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $sigilo ;?>" data-lang="2" data-param="detail_custom" class="detail2">
							<?php echo $n->title ?> 
						</a> 
					</td>										
					<td>
						<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $sigilo ;?>" data-lang="2" data-param="list" class="detail2">
							Tahun <?php echo date("Y", strtotime($n->datestart)) ?>
						</a>
					</td>
					<td><?php echo $n->name ?></td>
					<td>
						<?php echo date("l, d M Y", strtotime($n->datestart)) ?> - <br>
						<?php echo date("l, d M Y", strtotime($n->dateend)) ?>
					</td>
					<td>
						<?php foreach($status as $s){ ;?>
							<?php if($n->status == $s->value){ echo $s->invert;}?>							
						<?php unset($s);} ;?>
					</td>
					<?php if($simo == 0){?>
					<td>
						<?php foreach($status as $s){ ;?>
							<?php if($n->status != $s->value){ ;?>
								<button id="<?php echo $n->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button" style="margin: 2px"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php unset($s);} ;?>
						<!--
						<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Ubah</button>
						-->
					</td>
					<?php } ;?>
				</tr>	
				<?php $i++;} ?>
			</tbody>
		</table>
		<?php };?>
	</div>
</div>