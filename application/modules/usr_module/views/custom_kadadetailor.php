<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-sm-9 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->category_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					Tahun <?php echo date("Y", strtotime($objects->datestart)) ?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_custom" data-lang="2" class="detail2">
					<?php echo $objects->title ;?>
				</a>
			</li>
		</ol>
		<div class="ibox-tools ">
			<button id="<?php echo $objects->category_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_category" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Kembali</button>
			<?php if($simo == 0){ ;?>
				<a id="all" data-url="module" data-url2="inspeksi" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-primary " style="margin: 0 5px">Buat Inspeksi</a>
			<?php };?>			
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<div class="row">
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Kode</p></div>
				<div class="col-sm-10 col-xs-9"><h4><strong><?php echo $objects->tagline?></strong></h4></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Nama</p></div>
				<div class="col-sm-10 col-xs-9"><h4><strong><?php echo $objects->title?></strong></h4></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Tahun</p></div>
				<div class="col-sm-10 col-xs-9"><p>Tahun <?php echo date("Y", strtotime($objects->datestart)) ?> </p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Kuartal</p></div>
				<div class="col-sm-10 col-xs-9"><p><?php echo $objects->metakeyword?></p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Waktu Inspeksi</p></div>
				<div class="col-sm-10 col-xs-9">
					<?php echo date("l, d M Y", strtotime($objects->datestart)) ?> - <?php echo date("l, d M Y", strtotime($objects->dateend)) ?>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p><strong>Dinas Binamarga</strong></p></div>
				<div class="col-sm-10 col-xs-9">
					<p><strong><?php echo $objects->name?></strong></p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p><strong>Catatan dari Dinas</strong></p></div>
				<div class="col-sm-10 col-xs-9"><p><?php echo $objects->description?></p></div>
			</div>
		</div>
		<!--
		<?php if($c_objects == 0){ ;?>	
			<h3>Data <?php echo strtolower($essentiel) ?> masih kosong, silahkan isi area untuk masing-masing <?php echo strtolower($essentiel) ?> terlebih dahulu. </h3>
		<?php }else{ ;?>
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Kuartal </th>
					<th>Tahun </th>
					<th>Admin</th>
					<th>Tanggal Inspeksi </th>
					<th>Status</th>
					<?php if($simo == 0){?>
					<th>Aktivasi</th>
					<?php }?>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach($objects as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $sigilo ;?>" data-lang="2" data-param="detail_object" class="detail2">
							<?php echo $n->title ?> 
						</a> 
					</td>					
					<td>
						<a id="<?php echo $n->category_id?>" data-url="module" data-url2="<?php echo $sigilo ;?>" data-lang="2" data-param="detail_category" class="detail2">
							<?php echo $n->category ?> 
						</a>
					</td>
					<td><?php echo $n->name ?></td>
					<td>
						<?php echo date("l, d M Y", strtotime($n->datecreated)) ?> - <br>
						<?php echo date("l, d M Y", strtotime($n->dateupdated)) ?>
					</td>
					<td>
						<?php foreach($status as $s){ ;?>
							<?php if($n->status == $s->value){ echo $s->invert;}?>							
						<?php unset($s);} ;?>
					</td>
					<?php if($simo == 0){?>
					<td>
						<?php foreach($status as $s){ ;?>
							<?php if($n->status != $s->value){ ;?>
								<button id="<?php echo $n->object_id ?>" data-url="objects" data-param="<?php echo $s->status_id ?>" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->title ;?>" class="modal_stat btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php unset($s);} ;?>
						
						<button id="<?php echo $n->object_id?>" data-url="module" data-url2="<?php echo $key_link?>" data-param="edit_object" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Ubah</button>
					</td>
					<?php } ;?>
				</tr>	
				<?php $i++;} ?>
			</tbody>
		</table>
		<?php };?>
		-->
	</div>
</div>