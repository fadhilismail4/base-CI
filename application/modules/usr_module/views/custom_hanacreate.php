<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7"  style="font-size: 14px; padding-top: 6px; padding-left: 0px; margin-bottom: 0px">
			<li class="">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="view_category" data-lang="2" class="detail2">
					<?php echo $key_name;?> 
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $ccs_key?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="new_category" data-lang="2" class="detail2">
					Tambah
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $key_link ?>" data-param="view_category" data-lang="2" class="detail2 btn btn-sm  btn-warning ">Kembali</button>
		</div>		
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="parent_id" name="inputan" type="text" class="form-control hide" value="0"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Nama</label>
				<div class="col-sm-10"><input id="title" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Kode</label>
				<div class="col-sm-10"><input id="metakeyword" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Deskripsi</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-2 pull-right">		
					<button id="<?php echo $key_link;?>" data-param="category" class="create_mdl btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square"></i>  Tambah</button>		
					<button id="all" data-url="<?php echo $key_link;?>" data-url2="list" data-lang="<?php if(!empty($category)){  echo $category[0]->language_id;}else{ $language_id;};?>" class="detail2 btn btn-md btn-white pull-right" type="submit"><i class="fa fa-times-circle"></i> Batal</button>
				</div>
			</div>
		</form>
	</div>
</div>