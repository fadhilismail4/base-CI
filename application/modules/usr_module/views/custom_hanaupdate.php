
<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px; padding-left: 0px">
			<li class="">
				<a href="">
					<?php echo ucfirst($key_link);?>				
				</a>
			</li>
			<li class="">
				<a id="<?php echo $cat->category_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_category" data-lang="2" class="detail2">
					<?php echo $cat->title;?> 
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="detail_object" data-lang="2" class="detail2">
					<?php echo $objects->title;?> 
				</a>
			</li>					
			<li class="active">
				<a id="<?php echo $objects->object_id?>" data-url="module" data-url2="<?php echo $key_link;?>"  data-param="edit_object" data-lang="2" class="detail2">
					Ubah
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="hidden" class="form-control" value="2">
		<input id="ccs_key" name="inputan" type="hidden" class="form-control" value="<?php echo $ccs_key?>">
		<input id="object_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->object_id?>"></input>
		
		<div class="form-horizontal">	
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important" >Nama</label>
				<div class="col-sm-4">
					<input id="title" name="inputan" type="text" class="form-control" value="<?php echo $objects->title ;?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Deskripsi</label>
				<div class="col-sm-10">
					<div class="col-sm-12" style="padding: 0"><textarea id="tagline" name="inputan" type="text" class="form-control"><?php echo $objects->description ;?></textarea></div>
				</div>
			</div>
			<div class="form-group" >
				<label class="col-sm-2 control-label" style="text-align: left !important";>Kategori</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="category_id">							
							<option value="<?php echo $cat->category_id ;?>"><?php echo $cat->title ;?></option>
							<option value="">Pilih Kategori</option>								
							<?php foreach($category as $c){ if($c->category_id != $cat->category_id) {?>
							<option value="<?php echo $c->category_id ;?>"><?php echo $c->title ;?></option>
							<?php ;} ;} ;?>
						</select>
					</div>
				</div>
				<label class="col-sm-2 control-label" style="text-align: left !important; ">Aktivasi</label>
				<div class="col-sm-4" >
					<div class="input-group col-sm-12" >
						<select class="form-control m-b" name="inputan" id="status">
							<option value="<?php echo $objects->value?>"><?php echo $objects->name?></option>
							<option value="">Pilih Status</option>
							<?php foreach($status as $s){ ;?>
							<option value="<?php echo $s->value?>"><?php echo $s->name?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $key_link;?>" data-param="object" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Ubah</button>
				<div class="space-25"></div>		
					<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $key_link ?>" data-param="detail_object" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
			</div>
		</div>
	</div>
</div>