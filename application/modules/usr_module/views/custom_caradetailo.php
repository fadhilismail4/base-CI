<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->love ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->metadescription ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="0" class="detail2">
					Jemb. <?php echo $objects->title ;?>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
				<button id="<?php echo $objects->love ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<div class="row" style="margin-bottom: 20px">		
			<div class="col-sm-6 pull-left">
				<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2 btn-xs btn btn-white pull-left ">Konten</button>
				<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object"  data-lang="1" class="detail2 btn-xs btn btn-white pull-left " style="margin : 0 5px">Riwayat Inspeksi</button>
			</div>
		</div>
		<div class="row">
			
			<div class="col-sm-4" style="margin-bottom: 20px">
				<img alt="image" class="col-md-12 img-responsive" src="<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $sigilo ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-right: auto; margin-left: auto">
			</div>
			<div class="col-sm-8 row" style="padding: 5px 0px !important">
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>No. Jembatan</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo $objects->tagline?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Nama</p></div>
					<div class="col-sm-9 col-xs-9"><h4>: <strong>Jemb. <?php echo $objects->title?></strong></h4></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Lokasi</p></div>
					<div class="col-sm-9 col-xs-9">
						<p>: 	
							<a id="<?php echo $objects->love ;?>" data-url="module" data-url2="<?php echo $sigilo?>" data-param="list" data-lang="2" class="detail2">
							<?php echo $objects->metadescription?>
							</a>
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Area</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo $objects->metakeyword?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Tanggal Dibuat</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo date("l, d M Y", strtotime($objects->datecreated)) ?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Dibuat Oleh</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo $objects->name?></p></div>
				</div>
				
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-3 col-xs-3 pull-left"><p>Inpeksi Terakhir</p></div>
				<div class="col-sm-3 col-xs-5">
					<p>: 
						<strong>
						<?php echo date("l, d M Y", strtotime($objects->lastdate)) ?>
						</strong>
					</p>
				</div>
				<div class="col-sm-2 col-xs-3 pull-left"><p>Status Terakhir</p></div>
				<div class="col-sm-4 col-xs-3">
					<p>
						: <strong>
						<?php foreach($status as $s){ ;?>
							<?php if($s->value == $objects->status){echo $s->name ;}  ?>
						<?php } ;?>
						</strong>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-3 col-xs-3 pull-left"><p>Inspeksi Berikutnya</p></div>
				<div class="col-sm-3 col-xs-9">
					<p>
						: <strong>
							<a id="<?php echo $list->obr_id ;?>" data-url="module" data-url2="inspeksi" data-param="detail_object" data-lang="2" class="detail2">
							<?php echo $list->chiave?>
							</a>
						</strong>
					</p>
				</div>
				<div class="col-sm-2 col-xs-3 pull-left"><p>Tanggal</p></div>
				<div class="col-sm-4 col-xs-9">
					<p>: 
						<?php echo date("l, d M Y", strtotime($list->datestart)) ?> - <?php echo date("l, d M Y", strtotime($list->dateend)) ?>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Catatan</p></div>
				<div class="col-sm-10 col-xs-9">
					<?php echo $objects->description?>
				</div>
		</div>
	</div>
</div>