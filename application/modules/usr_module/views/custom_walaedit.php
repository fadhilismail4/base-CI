<link href="<?php echo base_url('assets');?>/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Profil</h5>
	</div>
	<div class="ibox-content">
		<form class="form-horizontal">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<input id="user_id" name="inputan" type="hidden" class="form-control" value="<?php echo $objects->user_id;?>">
			<input id="ccs_id" name="inputan" type="hidden" class="form-control" value="<?php echo $fascia;?>">
			<div class="form-group">
				<label class="col-sm-2 control-label">Nama Lengkap</label>
				<div class="col-sm-10"><input id="name" name="inputan" type="text" class="form-control" value="<?php echo $objects->name;?>"></div>
			</div>
			<div class="form-group"><label class="col-sm-2 control-label">Jenis Kelamin</label>
				<div class="col-sm-10">
					<select id="gender" name="inputan" class="form-control m-b">
						<option value="">Jenis Kelamin</option>
						<option value="1">Pria</option>
						<option value="2">Wanita</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Tanggal Lahir</label>
				<div class="col-sm-10">
					<div class="input-group date">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="birthday" name="inputan" data-date-format="yyyy-mm-dd" type="text" class="form-control">
					</div>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nomor Telepon</label>
				<div class="col-sm-10"><input id="phone" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nomor HP</label>
				<div class="col-sm-10"><input id="mobile" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">E-mail</label>
				<div class="col-sm-10"><input id="email" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Alamat</label>
				<div class="col-sm-10"><input id="address" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group"><label class="col-sm-2 control-label">Role</label>
				<div class="col-sm-10">
					<select id="role_id" name="" disabled="disabled" class="form-control m-b">
						<option value=""><?php echo $objects->role;?></option>
					</select>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Tentang Saya</label>
				<div class="col-sm-10">
					<textarea id="description" name="inputan" type="text" class="form-control" rows="5" maxlength="200"></textarea>
				</div>
			</div>
			<?php if(($simo == 0) && ($fattore != $objects->user_id)){;?>
			<div class="form-group"><label class="col-sm-2 control-label">Status</label>
				<div class="col-sm-10">
					<select id="status" name="inputan" class="form-control m-b">
						<option value="">Status</option>
						<option value="1">Active</option>
						<option value="2">Inactive</option>
					</select>
				</div>
			</div>
			<?php };?>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<button id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" class="detail2 btn btn-white" type="">Batalkan</button>
					<button id="<?php echo $sigilo;?>" data-param="<?php echo $sigilo;?>" class="create_mdl btn btn-primary" type="submit">Ubah</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Data picker -->
<script src="<?php echo base_url('assets');?>/admin/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url('assets');?>/admin/js/ajaxfileupload.js"></script>
<script>
$('#gender').val('<?php echo $objects->gender;?>');
$('#email').val('<?php echo $objects->email;?>');
<?php if(strtotime($objects->birthday) != '0000-00-00'){ 
	$birth = $objects->birthday;
;}else{
	$birth = '1945-01-01';
};?>
$('#birthday').val('<?php echo $birth;?>');
$('#description').val('<?php echo $objects->description;?>');
$('#address').val('<?php echo $objects->address;?>');
$('#role').val('<?php echo $objects->role;?>');
$('#mobile').val('<?php echo $objects->mobile;?>');
$('#phone').val('<?php echo $objects->phone;?>');
$('#status').val('<?php echo $objects->status;?>');

$("[data-param='user']").slideUp();
$('.file-input').slideDown();
$('#ava').slideUp();
$('#ava2').append('<input id="avatar" name="avatar" class="file" type="file" value="" style="">');
$(document).ready(function(){
	$('#birthday').datepicker({
		keyboardNavigation: false,
		todayBtn: "linked",
		format: 'yyyy-mm-dd',		
		forceParse: false
	});
	$(".file").fileinput("refresh",{
		 showRemove: false,
		showUpload: false,
		initialPreview: [
			"<img src='<?php echo base_url();?><?php echo $this->session->userdata($zone.'_foto');?>' class='file-preview-image'>"
		]
	});
});
</script>