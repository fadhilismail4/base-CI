<link href="<?php echo base_url();?>assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $inspeksi->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2">
					Jemb. <?php echo $inspeksi->parole ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_custom" data-lang="2" class="detail2">
					<?php echo $inspeksi->chiave ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="edit_custom" data-lang="2" class="detail2">
					Ubah
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_custom" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Kembali</button>
		</div>
	</div>
	
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_key" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->ccs_key?>"></input>
		<input id="type" name="inputan" type="text" class="form-control hide" value="<?php echo $sigilo?>"></input>
		<input id="ccs_id" name="inputan" type="text" class="form-control hide" value="<?php echo $fascia?>"></input>
		<input id="custom_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->log_id?>"></input>
		<input id="url_id" name="inputan" type="text" class="form-control hide" value="<?php echo $objects->url_id?>"></input>
		<div class="row">
			<div class="col-sm-4">
				<input id="image_square" name="image_square" class="file" type="file" value="">
			</div>
			<div class="col-sm-8">
				<div class="col-sm-12">
					<h3 class="row">Pilih Data</h3>
				</div>				
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Tahun</label>
						<div class="col-sm-8 col-xs-12">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="select_to_select2 form-control m-b" name="inputan"  data-url="module" data-url2="<?php echo $sigilo?>" data-param="list_url"  id="type_id">
									<option value="<?php echo $inspeksi->category_id?>" data-url="tahun"><?php echo $inspeksi->tahun?></option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="clearfix" id="data_3">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Kuartal</label>
							<div class="col-sm-8 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12" >
									<select class=" form-control m-b" name="inputan" id="category_id">
											<option value="<?php echo $inspeksi->category_id?>" ><?php echo $inspeksi->chiave?></option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Pilih Area / Lokasi</label>
						<div class="col-sm-8 col-xs-12">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class=" form-control m-b col-sm-12 col-xs-12" name="inputan" id="area_id">
									<option value="<?php echo $inspeksi->area_id?>" ><?php echo $inspeksi->metadescription?></option>
								</select>
							</div>
						</div>	
					</div>
					<div class="form-group">
						<div class="clearfix" id="data_2">
							<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important; ">Jembatan</label>
							<div class="col-sm-8 col-xs-12" >
								<div class="input-group col-sm-12 col-xs-12" >
									<select class=" form-control m-b" name="inputan" id="object_id">
										<option value="<?php echo $objects->object_id?>" >Jembatan <?php echo $objects->jembatan?></option>
									</select>
								</div>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-4 control-label" style="text-align: left !important">Status</label>
						<div class="col-sm-4 col-xs-12">
							<div class="input-group col-sm-12 col-xs-12" >
								<select class="form-control m-b" name="inputan" id="status">
									<option value="">Pilih Status</option>
									<?php foreach($status as $s){;?>
										<option value="<?php echo $s->value?>"><?php echo $s->name ?></option>
									<?php ;}?>
								</select>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="col-sm-12" style="margin-top: 20px">
				<div class="col-sm-12">
					<h3 class="row">Kondisi Umum</h3>
				</div>				
				<div class="form-horizontal">					
					<div class="form-group">
						<label class="col-sm-1 col-xs-3 control-label" style="text-align: left !important">BA</label>
						<div class="col-sm-1 col-xs-3">
							<input id="url_1" name="inputan" type="text" class="form-control" value="<?php echo $list->url_1?>">
						</div>	
						<label class="col-sm-1 col-xs-3 control-label" style="text-align: left !important">LNT</label>
						<div class="col-sm-1 col-xs-3">
							<input id="url_2" name="inputan" type="text" class="form-control" value="<?php echo $list->url_2?>">
						</div>
						<label class="col-sm-1 col-xs-3 control-label" style="text-align: left !important">BB</label>
						<div class="col-sm-1 col-xs-3">
							<input id="url_3" name="inputan" type="text" class="form-control" value="<?php echo $list->url_3?>">
						</div>						
						<label class="col-sm-1 col-xs-3 control-label" style="text-align: left !important">DAS</label>
						<div class="col-sm-1 col-xs-3">
							<input id="url_4" name="inputan" type="text" class="form-control" value="<?php echo $list->url_4?>">
						</div>			
						<label class="col-sm-1 col-xs-3 control-label" style="text-align: left !important">JBT</label>
						<div class="col-sm-1 col-xs-3">
							<input id="url_5" name="inputan" type="text" class="form-control" value="<?php echo $list->url_5?>">
						</div>										
					</div>
				</div>
				<div class="col-sm-12">
					<h3 class="row">Nilai IRMS</h3>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-1 col-xs-3 control-label" style="text-align: left !important">TRAFC</label>
						<div class="col-sm-3  col-xs-3">
							<input id="number_1" name="inputan" type="text" class="form-control" value="<?php echo $list->number_1?>" placeholder=" ">
						</div>	
						<label class="col-sm-1  col-xs-3 control-label" style="text-align: left !important">AADT</label>
						<div class="col-sm-3  col-xs-3">
							<input id="number_2" name="inputan" type="text" class="form-control" value="<?php echo $list->number_2?>" placeholder=" ">
						</div>
						<label class="col-sm-1   col-xs-3 control-label" style="text-align: left !important">JLN</label>
						<div class="col-sm-3  col-xs-3">
							<input id="number_3" name="inputan" type="text" class="form-control" value="<?php echo $list->number_3?>" placeholder=" ">
						</div>	
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ibox float-e-margins">
	<div class="ibox-content row">
		<form class="form-horizontal" method="get">	
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Judul</label>
				<div class="col-sm-10">
					<input id="title" name="inputan" type="text" class="form-control" value="<?php echo $objects->title?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align: left !important">Catatan</label>
				<div class="col-sm-10">
					<div class="mail-box ">
						<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">
							<div id="description" name="inputan_summer" class="summernote">
								<?php echo $objects->description?>
							</div>
						</div>			
						<div class="mail-body text-right tooltip-demo">
						</div>
					</div>
				</div>
			</div>		
		</form>
		
		<div class="col-sm-12">
			<div class="form-group">					
				<div class="space-25"></div>
				<button id="<?php echo $sigilo;?>" data-param="new_custom" class="create_mdl btn btn-primary btn-md col-sm-3 pull-right"><i class="fa fa-check-square"></i>  Ubah</button>
				<div class="space-25"></div>		
					<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_custom" data-lang="2" class="detail2 btn-md btn btn-white col-sm-3  pull-right" style="margin-right: 20px"><i class="fa fa-times-circle"></i>  Batal</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fileinput/fileinput.min.js" type='text/javascript'></script>
<script src="<?php echo base_url();?>assets/admin/js/ajaxfileupload.js" type='text/javascript'></script>

<script>
$(document).ready(function(){
		$('#status').val(<?php echo $objects->status;?>);
	$('.summernote').summernote({
		onImageUpload: function(files) {
			url = $(this).data('upload'); //path is defined as data attribute for  textarea
			sendFile(files[0], url, $(this));
		}
	});
	$(".file").fileinput("refresh",{
		initialPreview: [
			"<img src='<?php if($objects->image_square){ echo base_url('assets').'/binamarga/' ;?><?php echo $sigilo.'/'.$objects->image_square; }else{ echo base_url('assets').'/img/image_empty.png' ;}?>' class='file-preview-image'>"
		],			
		showRemove: false,
		showUpload: false
	});
});
var edit = function() {
	$('.click2edit').summernote({focus: true});
};
var save = function() {
	var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
	$('.click2edit').destroy();
};
</script>