<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-9 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2">
					Jemb. <?php echo $objects->parole ;?>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="all" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="list" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Kembali</button>	
			<button id="<?php echo $objects->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new_custom" data-lang="2" class="detail2 btn-sm btn btn-primary " style="margin: 0 5px">Tambah Data</button>			
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<div class="row" style="margin-bottom: 20px">		
			<div class="col-sm-6 pull-left">
				<button id="<?php echo $objects->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2 btn-xs btn btn-white pull-left ">Konten</button>
				<!--
				<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="jadwal" data-lang="2" data-param="gallery_custom" class="mdl_object btn-xs btn btn-white pull-left " style="margin : 0 5px">Foto Jembatan</button>
				-->
			</div>
			<div class="col-sm-6 pull-right">
				<button id="<?php echo $objects->utl_id ;?>" data-url="module" data-url2="jembatan"  data-param="detail_object" data-lang="2" class="mdl_object btn-xs btn btn-success pull-right ">Detail Jembatan</button>
				<!--
				<button id="<?php echo $objects->obr_id ;?>" data-url="module" data-url2="inspeksi" data-lang="2" data-param="detail_custom" class=" btn-xs btn btn-primary pull-right" style="margin : 0 5px">Tambah Foto</button>
				-->
			</div>
		</div>
		<div id="data_object"></div>
		<div class="row">
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>No. Jembatan</p></div>
				<div class="col-sm-10 col-xs-9"><p>: <?php echo $objects->tagline?></p></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Nama</p></div>
				<div class="col-sm-10 col-xs-9"><h4>: <strong>Jemb. <?php echo $objects->parole?></strong></h4></div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Lokasi</p></div>
				<div class="col-sm-10 col-xs-9">
					<p>: 	
						<a id="<?php echo $objects->area_id ;?>" data-url="module" data-url2="lokasi" data-param="detail_category" data-lang="2" class="detail2">
						<?php echo $objects->metadescription?>
						</a>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Inspeksi Terakhir</p></div>
				<div class="col-sm-4 col-xs-5">
					<p>: 
						<strong>
						<?php echo date("l, d M Y", strtotime($objects->lastdate)) ?>
						</strong>
					</p>
				</div>
				<div class="col-sm-2 col-xs-3 pull-left"><p>Status Terakhir</p></div>
				<div class="col-sm-4 col-xs-3">
					<p>
						: <strong>
						<?php foreach($status as $s){ ;?>
							<?php if($s->value == $objects->last_status){echo $s->name ;}  ?>
						<?php } ;?>
						</strong>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-2 col-xs-3 pull-left"><p>Inspeksi Berikutnya</p></div>
				<div class="col-sm-4 col-xs-9">
					<p>
						: <strong>
							<a id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="jadwal" data-param="detail_custom" data-lang="2" class="detail2">
							<?php echo $objects->chiave?>
							</a>
						</strong>
					</p>
				</div>
				<div class="col-sm-2 col-xs-3 pull-left"><p>Tanggal</p></div>
				<div class="col-sm-4 col-xs-9">
					<p>: 
						<?php echo date("l, d M Y", strtotime($objects->datestart)) ?> - <?php echo date("l, d M Y", strtotime($objects->dateend)) ?>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" style="padding: 0px">
				<div class="col-sm-6 col-xs-12 "><h4><strong>Catatan dari Balai</strong></h4></div>
				<div class="col-sm-12 col-xs-12 "><p> <?php echo $objects->description?></p></div>					
			</div>
		</div>
		<div class="row">
		</div>
		<?php if($cl == 0){ ;?>	
		<div class="row">
			<h4 class="col-sm-10">Data <?php echo strtolower($essentiel) ?> untuk jembatan <?php echo $objects->parole?> masih kosong, silahkan mulai menambahkan data inspeksi. </h4>
			
		<?php }else{ ;?>
			<div class="row" style="padding-bottom: 10px !important">
				<h4 class="col-sm-10">Data <?php echo strtolower($essentiel) ?> untuk jembatan <?php echo $objects->parole?>. </h4>
				
			</div>
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" style="padding-top: 20px" >
				<thead>
					<tr>
						<th>No</th>
						<th>No. Inspeksi </th>
						<th>Tahun </th>
						<th>Tim Inspeksi</th>
						<th>Tanggal Inspeksi </th>
						<th>Status</th>
						<th>Aktivasi</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($list as $n){ ;?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>				
						<td>
							<a id="<?php echo $n->log_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-lang="2" data-param="detail_custom" class="detail2">
								<?php echo $n->chiave ?> 
							</a>
						</td>									
						<td>
							<a id="<?php echo $n->type_id?>" data-url="module" data-url2="jadwal" data-lang="2" data-param="list" class="detail2">
								<?php echo $n->tahun ?> 
							</a>
						</td>
						<td><?php echo $n->name ?></td>
						<td>
							<?php echo date("l, d M Y", strtotime($n->datestart)) ?> <br> - 
							<?php echo date("l, d M Y", strtotime($n->dateend)) ?>
						</td>
						<td>
							<?php if($n->i_status == 0){?>
								<?php foreach($status as $s){ ;?>
									<?php if($s->value == $n->status){ echo $s->name ;} ;?>
								<?php } ;?>
							<?php }elseif($n->i_status == 1){ ;?>
								Inspeksi Diterima
							<?php }elseif($n->i_status == 2){ ;?>
								Ulang Inspeksi
							<?php }elseif($n->i_status == 3){ ;?>
								Menunggu Konfirmasi
							<?php };?>
						</td>
						
						<td>
							
							<?php if($simo == 0){ ;?>
								<?php if(($n->i_status == 0) || ($n->i_status == 3)){ ;?>
									<button id="<?php echo $n->log_id ?>" data-url="validasi" data-param="1" data-url3="<?php echo $sigilo?>"  data-lang="2" data-status="Terima" data-title="<?php echo $n->chiave ;?>" class="modal_stat btn btn-primary btn-xs" type="button" style="margin: 2px">Terima</button>
									<button id="<?php echo $n->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="view_custom" data-lang="1" class="detail2 btn-xs btn btn-danger">Tolak</button>
								<?php }elseif($n->i_status == 2){ ;?>
									<button id="<?php echo $n->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="view_custom" data-lang="2" class="detail2 btn-xs btn btn-warning"  style="margin: 2px 0">Lihat Alasan</button>
								<?php } ;?>
							<?php }else{ ;?>
								<?php if($n->i_status == 2){ ;?>
									
								<?php }elseif($n->i_status == 3){ ;?>
									<button id="<?php echo $n->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="view_custom" data-lang="2" class="detail2 btn-xs btn btn-warning" style="margin: 2px 0">Lihat Alasan</button>
								<?php }elseif($n->i_status == 0){ ;?>								
									<button class="btn btn-warning btn-xs" type="button" style="margin: 2px 0">Menunggu Konfirmasi</button>
								<?php } ;?>
							<?php } ;?>
							<button id="<?php echo $n->log_id?>" data-url="module" data-url2="<?php echo $sigilo ;?>" data-lang="2" data-param="edit_custom" class="detail2 btn-xs btn btn-info ">Ubah</button>		
							<button id="<?php echo $n->log_id?>" data-url="module" data-url2="<?php echo $sigilo?>" data-param="detail_custom" data-lang="2" class="detail2 btn btn-success ;?> btn-xs" type="button">Lihat</button>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		
		<?php };?>
		
		</div>
	</div>
</div>

<script id="object_script">
	
	$('#data_object').hide();
	$('.mdl_object').on("click", function(e) {
		e.preventDefault();
		
		$('.pace-done').css('padding-right','');
		$('.btn').each(function(){
			$(this).attr('disabled', 'disabled');
		});
		var id = $(this).attr('id');
		var obj = $(this).data('url');
		var part = $(this).data('url2');
		var lang = $(this).data('lang');
		var param = $(this).data('param');
		var data = {
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			id : id,
			param : param,
			language_id : lang
		};
		$('[id*="modal_"]').each(function(){
			$(this).remove();
		});
		var url = "<?php echo base_url('binamarga');?>/user/"+obj+"/view_detail/"+part;
		$.ajax({
			url : url,
			type: "POST",
			dataType: 'html',
			data: data,
			success: function(html){
				$('#data_object').parent().append('<div class="modal inmodal  animated fadeInRight" id="modal_'+id.replace(/,/g, '')+'" tabindex="-1" role="dialog">                              <div class="modal-dialog modal-lg">                                    <div class="modal-content">                                        <div class="modal-header">                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                            <h5 class="modal-title" style="text-transform:uppercase;float: none;">JADWAL INSPEKSI</h5>                                        </div>                                        <div class="modal-body">    '+html.replace(/detail2/g, 'mdl_object')+'                                      </div>                                        <div class="modal-footer">                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                                       </div>                                    </div>                               </div>                            </div>');
				$('#modal_'+id.replace(/,/g, '')+'').modal('toggle');
				$('.modal-backdrop').css('display','none');
				$('.btn').each(function(){$(this).removeAttr('disabled');});
				eval(document.getElementById("object_script").innerHTML);
				removeCrud('object_script');
			}
		});
		function removeCrud( itemid ) {
			var element = document.getElementById(itemid); // will return element
			elemen.parentNode.removeChild(element); // will remove the element from DOM
		}
		removeCrud('object_script');
	});
</script>

