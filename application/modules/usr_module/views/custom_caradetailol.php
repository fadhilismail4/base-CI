<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $objects->love ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $objects->metadescription ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="0" class="detail2">
					Jemb. <?php echo $objects->title ;?>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
				<button id="<?php echo $objects->love ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="new" data-lang="2" class="detail2 btn-sm btn btn-warning ">Kembali</button>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<div class="row" style="margin-bottom: 20px">		
			<div class="col-sm-6 pull-left">
				<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2 btn-xs btn btn-white pull-left ">Konten</button>
				<button id="<?php echo $objects->object_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object"  data-lang="1" class="detail2 btn-xs btn btn-white pull-left " style="margin : 0 5px">Riwayat Inspeksi</button>
			</div>
		</div>
		<div class="row">
			
			<div class="col-sm-4" style="margin-bottom: 10px">
				<img alt="image" class="col-md-12 img-responsive" src="<?php if($objects->image_square){ ;?><?php echo base_url('assets')?>/<?php echo $zone ?>/<?php echo $sigilo ?>/<?php echo trim($objects->image_square)?>
					<?php }else{ echo base_url('assets').'/img/logo_empty.png'; }?>" style="display: block; margin-right: auto; margin-left: auto">
			</div>
			<div class="col-sm-8 row" style="padding: 5px 0px !important">
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>No. Jembatan</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo $objects->tagline?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Nama</p></div>
					<div class="col-sm-9 col-xs-9"><h4>: <strong>Jemb. <?php echo $objects->title?></strong></h4></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Lokasi</p></div>
					<div class="col-sm-9 col-xs-9">
						<p>: 	
							<a id="<?php echo $objects->love ;?>" data-url="module" data-url2="<?php echo $sigilo?>" data-param="list" data-lang="2" class="detail2">
							<?php echo $objects->metadescription?>
							</a>
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Area</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo $objects->metakeyword?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Tanggal Dibuat</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo date("l, d M Y", strtotime($objects->datecreated)) ?></p></div>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding: 0px">
					<div class="col-sm-3 col-xs-3 pull-left"><p>Dibuat Oleh</p></div>
					<div class="col-sm-9 col-xs-9"><p>: <?php echo $objects->name?></p></div>
				</div>
				
			</div>
		</div>
		<?php if($cl == 0){ ;?>	
		<div class="row">
			<h4 class="col-sm-10">Data inspeksi untuk jembatan <?php echo $objects->title?> masih kosong, silahkan mulai menambahkan data inspeksi. </h4>
			
		<?php }else{ ;?>
			<div class="row" style="padding-bottom: 10px !important">
				<h4 class="col-sm-10">Data inspeksi untuk jembatan <?php echo $objects->title?>. </h4>
				
			</div>
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" style="padding-top: 20px" >
				<thead>
					<tr>
						<th>No</th>
						<th>No. Inspeksi </th>
						<th>Tahun </th>
						<th>Tim Inspeksi</th>
						<th>Tanggal Inspeksi </th>
						<th>Status Jemb.</th>
						<th>Aktivasi</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach($list as $n){ ;?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>				
						<td>
							<a id="<?php echo $n->log_id?>" data-url="module" data-url2="inspeksi" data-lang="2" data-param="detail_custom" class="detail2">
								<?php echo $n->chiave ?> 
							</a>
						</td>									
						<td>
							<a id="<?php echo $n->type_id?>" data-url="module" data-url2="jadwal" data-lang="2" data-param="list" class="detail2">
								<?php echo $n->tahun ?> 
							</a>
						</td>
						<td><?php echo $n->name ?></td>
						<td>
							<?php echo date("l, d M Y", strtotime($n->datecreated)) ?> 
						</td>
						<td>
							<?php foreach($status as $s){ ;?>
								<strong><?php if($s->value == $n->status){ echo $s->name ;} ;?></strong>
							<?php } ;?>
						</td>
						
						<td>		
							<button id="<?php echo $n->log_id?>" data-url="module" data-url2="inspeksi" data-param="detail_custom" data-lang="2" class="detail2 btn btn-success ;?> btn-xs" type="button">Lihat</button>
						</td>
					</tr>	
					<?php $i++;} ?>
				</tbody>
			</table>
		
		<?php };?>
	</div>
</div>