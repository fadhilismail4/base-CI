 <div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Aktifitas Terakhir</h5>
	</div>
	<?php 
	
	$foto = 'assets/'.strtolower($zone).'/user/'.strtolower($profile->type).'/'.$profile->user_id.'/'.$profile->avatar;
	if (!file_exists($foto))
	{
		$foto =  'assets/img/admin_empty.jpg';
	}
	;?>
	<div class="ibox-content">
		<div class="row">
			<div class="col-md-4">
				<div id="ava2" class="ibox-content no-padding border-left-right">
					<img id="ava" alt="image" class="img-responsive" src="
					<?php echo base_url();?><?php echo $foto;?>
					" style="width: 100%;">
				</div>
				<div class="ibox-content profile-content">
					<p class="pull-right"><strong><?php echo '';?></strong></p>
					<h4><strong id="name" name="inputan"><?php echo $profile->name;?></strong></h4>
					<p><i class="fa fa-map-marker"></i> <?php if(!empty(trim($profile->address))){echo trim($profile->address);}else{echo '-';};?></p>
					<h5>
						Tentang Saya
					</h5>
					<p>
						<?php if(!empty(trim($profile->description))){echo trim($profile->description);}else{echo '-';};?>
					</p>
				</div>
			</div>
			<div class="col-md-8">
				<ul class="list-group clear-list">
				<?php if(!empty($objects)){;?>
					<?php $i=1;foreach($objects as $lg){
						if(isset($lg->title)){
							if($lg->type == 1){
								$info = '<span class="pull-right"> <span class="label label-warning">CREATE</span> </span>';
							}elseif($lg->type == 2){
								$info = '<span class="pull-right"> <span class="label label-primary">READ</span> </span>';
							}elseif($lg->type == 3){
								$info = '<span class="pull-right"> <span class="label label-success">UPDATE</span> </span>';
							}elseif($lg->type == 4){
								$info = '<span class="pull-right"> <span class="label label-danger">DELETE</span> </span>';
							}else{
								$info = '';
							}
						}else{
							$info = '';
						}
						?>
						<li class="list-group-item <?php if($i == 1){echo 'fist-item';}?>">
							<?php echo $info;?>
							<strong><?php echo $lg->module;?></strong><?php if(isset($lg->title)){ echo ' / '.$lg->category;};?><?php if(isset($lg->title)){ echo ' / '.$lg->title;};?><br><small><?php echo date('d-M-Y H:i:s', strtotime($lg->datecreated));?></small>
						</li>
					<?php ;$i++;}?>
				<?php }else{ ?>
				<i>Belum ada aktifitas terbaru...</i>	
				</ul>
				
				<?php } ?>
				
			</div>
		</div>
	</div>
</div>