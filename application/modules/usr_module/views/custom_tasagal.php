<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-8 col-xs-12" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
					<?php echo $essentiel?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $inspeksi->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_object" data-lang="2" class="detail2">
					Jemb. <?php echo $inspeksi->parole ;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_custom" data-lang="2" class="detail2">
					<?php echo $inspeksi->chiave ;?>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<button id="<?php echo $inspeksi->obr_id ;?>" data-url="module" data-url2="<?php echo $sigilo ?>" data-param="detail_object" data-lang="2" class="detail2 btn-sm btn btn-warning pull-right">Kembali</button>
			<button id="<?php echo $objects->log_id ;?>" data-url="<?php echo $objects->object_id ;?>" data-url2="<?php echo $objects->ccs_key ;?>"  data-lang="2" class="modal_upload_data btn-sm btn btn-primary pull-right" style="margin: 0 5px">Tambah Foto</button>
			<div id="edit_gallery" val="<?php echo $objects->log_id?>" class="btn-sm btn btn-success pull-right" >Edit Foto</div>
		</div>
	</div>
	
	<div class="ibox-content row">
		<div class="row" style="margin-bottom: 20px">		
			<div class="col-sm-6 pull-left">
				<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="detail_custom" data-lang="2" class="detail2 btn-xs btn btn-white pull-left ">Hasil Inspeksi</button>
				<button id="<?php echo $objects->log_id ;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-lang="2" data-url3="detail" data-param="gallery_custom" class="detail2 btn-xs btn btn-white pull-left " style="margin : 0 5px">Foto Inspeksi</button>
			</div>
		</div>
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		
		<div class="col-sm-12">			
			<div class="row">
				<?php foreach($gallery as $gal){
					if(file_exists('assets/binamarga/gallery/'.$gal->image_square))	{									
				?>
					<a class="fancybox" title="<?php echo $gal->title?>"  rel="gallery" href="<?php echo base_url()?>assets/binamarga/gallery/<?php echo $gal->image_square?>">
						<img src="<?php echo base_url()?>assets/binamarga/gallery/<?php echo $gal->image_square?>" width="" height="" alt="chimp-1-th">
					</a>
					<div id="data_gallery">
						Test
					</div>
				<?php ;};}?>									
			</div>
        </div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox-thumbs.js"></script>
<script id="fancy_script" type="text/javascript">
if (!$("link[href='<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css']").length){
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">').appendTo("head");
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox-thumbs.css" rel="stylesheet">').appendTo("head");
	$(document).ready(function() {
		$(".fancybox")
			.fancybox({
				prevEffect	: 'none',
				nextEffect	: 'none',
				helpers : {
					title : {
						type: 'inside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					},
					buttons	: {}
				}  
			});
	});
}
</script>   
<script>
$(document).ready(function(){
	$('#data_gallery').hide();
	$('#edit_gallery').change(function() {
		if ($(this).val() != '') {
			$('#data_gallery').slideDown('slow');
		}else{
			$('#data_gallery').slideUp('slow');
		}
	});
});
</script> 