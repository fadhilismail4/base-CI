<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Kategori <?php echo $essentiel ;?></h5>
	</div>
	<div class="ibox-content">
		<div class="row" style="margin: 0px;">
			<!--
			<a id="<?php echo $essentiel_id;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="new_category" data-lang="<?php echo $lingua;?>" class="detail2 btn btn-sm btn-primary btn-block">Tambah Lokasi</a>
			-->
			<a id="<?php echo $essentiel_id;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-lang="<?php echo $lingua;?>" data-param="view_category" class="detail2 btn btn-sm btn-success btn-block">Lihat Lokasi</a>
		</div>
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="all" data-url="module" data-url2="<?php echo $sigilo;?>" data-param="list" data-lang="2" class="detail2">
							<span class="label label-info"></span> <strong>View All</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<?php if(!empty($category)){;?>
					<?php foreach($category as $c){;?>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag">		
							<a id="<?php echo $c['parent']->category_id;?>" data-url="module" data-url2="<?php echo $sigilo;?>" data-lang="<?php echo $category[0]['parent']->language_id;?>" data-param="list" class="detail2">
							<span class="label label-info"></span> <?php echo $c['parent']->title;?>
							</a>
							<p class="pull-right"><?php echo '('.$c['count'].')' ;?></p> 
						</div>				
					</li>
					<?php ;};?>
					<?php ;}?>
				</ol>
			</div>
		</div>
	</div>
</div>