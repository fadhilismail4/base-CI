<style>
.note-editor{
	background-color: #fff;
}
.icon-reply{
	border-top: .938em solid #A3A3A3;
	border-left: .938em solid #A3A3A3;
	border-right: .938em solid #A3A3A3;
	width: 10px;
	cursor: pointer;
}
</style>
<div class="mail-box">


	<div class="mail-body">

		<form class="form-horizontal" method="get">
			<input id="parent_id" name="inputan"type="hidden" class="" value="<?php echo $messages->message_id;?>">
			<input id="subject" name="inputan" type="hidden" class="form-control" value="<?php if($part == 'reply'){ echo 'Re:' ;}else{ echo 'Fwd:';};?> <?php echo $messages->subject;?>">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<div class="form-group"><label class="col-sm-2 control-label">To:</label>
			<input id="tr_id" name="inputan" type="text" class="hide form-control" value="">
				<div class="col-sm-10">
					<div class="input-group">
						<select id="receiver" name="inputan" data-placeholder="Select" class="chosen-select" style="width:350px;" tabindex="2">
						<option value="">Select</option>
						<?php foreach($receiver as $r){?>
							<option value="<?php echo $r->id;?>" data-type="<?php echo $r->type_id;?>"><?php echo $r->name;?> (<?php echo $r->role;?>)</option>
						<?php } ?>
						</select>
					</div>
				</div>
			</div>
		</form>

	</div>

	<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">

		<div id="message" name="inputan_summer" class="summernote">
			
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="mail-body text-right tooltip-demo" style="background-color: #F9F8F8;">
		<a id="<?php echo $child;?>" class="create btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-reply"></i> Send</a>
		<a class="discard btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
		<a id="<?php echo $child;?>" class="create btn btn-white btn-sm" data-param="draft" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>
	</div>
    
	<div class="clearfix"></div>
</div>
<!-- SUMMERNOTE -->
<script src="<?php echo base_url('assets');?>/admin/js/plugins/summernote/summernote.min.js"></script>
<script>
	$(document).ready(function(){
		var type = $('#receiver').find(':selected').data('type');
		$('#tr_id').val(type);
		$('#receiver').change(function() {
			var type = $(this).find(':selected').data('type');
			$('#tr_id').val(type);
		});
		$('.discard').on("click", function(e) {
			e.preventDefault();
				$('[class*="detail_content2"]').each(function(){
					$(this).empty();
				});
		});
		$('.reply').children().hide();
		$('.icon-reply').on("click", function(e) {
			e.preventDefault();
			$(this).removeClass('icon-reply');
			$('.reply').children().slideDown();
		});
	});
	var edit = function() {
		$('.click2edit').summernote({focus: true});
	};
	var save = function() {
		var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
		$('.click2edit').destroy();
	};
	
</script>