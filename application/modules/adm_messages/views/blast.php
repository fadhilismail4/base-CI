<style>
.note-editor{
	background-color: #fff;
}
</style>
<link href="<?php echo base_url('assets');?>/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet">
<div class="mail-box-header">
	<div class="pull-right tooltip-demo">
		<!--<a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->
		<a href="" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
	</div>
	<h2>
		Email Blast
	</h2>
</div>
<div class="mail-box">
	<?php if(!empty($by[0])){;?>

	<div class="mail-body">

		<form class="form-horizontal" method="get">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">From:</label>
				<div class="col-sm-10">
					<div class="input-group">
						<select id="by" name="inputan" style="width:350px;" class="form-control m-b">
							<option value="">Select</option>
							<?php foreach($by as $by){;?>
							<option value="<?php echo $by->char_7;?>"><?php echo $by->char_5;?></option>
							<?php };?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Choose Type:</label>
				<div class="col-sm-10">
					<div class="input-group">
						<select id="type" name=""  style="width:350px;" class="form-control m-b">
							<option value="">Choose Type to Receive</option>
							<option value="automatic">by Role</option>
							<option value="manual">Manual</option>
						</select>
					</div>
				</div>
			</div>
			<div id="manual" class="form-group" style="display:none">
				<label class="col-sm-2 control-label">To:</label>
				<div class="col-sm-10">
					<div class="input-group">
						<select id="receiver" name="inputan_array" multiple data-placeholder="Select" class="chosen-select" style="width:350px;" tabindex="2">
						<option value="">Select</option>
						<?php foreach($receiver as $r){?>
							<option value="<?php echo $r->id;?>,<?php echo $r->type_id;?>"><?php echo $r->name;?> (<?php echo $r->role;?>)</option>
						<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div id="automatic" class="form-group" style="display:none">
				<label class="col-sm-2 control-label">To:</label>
				<div class="col-sm-10">
					<div class="input-group">
						<select id="receiver" name="inputan_array" multiple data-placeholder="Select" class="chosen-select" style="width:350px;" tabindex="2">
						<option value="">Select</option>
						<?php foreach($type as $t){?>
							<?php if(!empty($t)){;?>
							<option value="<?php echo $t->type_id;?>"><?php echo $t->name;?></option>
							<?php };?>
						<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Subject:</label>

				<div class="col-sm-10"><input id="subject" name="inputan" type="text" class="form-control" value=""></div>
			</div>
		</form>

	</div>

	<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">

		<div id="message" name="inputan_summer" class="summernote">
			
		</div>
		<div class="clearfix"></div>
		<div class="col-md-5">
			<p>Attachment ( jpg | jpeg | png | pdf ):</p>
		</div>
		<div class="col-md-3" style="margin-bottom: 15px;float: right;">
			<input type="file" class="file form-control input-lg c-square" name="image_square" id="image_square">
		</div>
	</div>
	<div class="mail-body text-right tooltip-demo">
		<a id="messages" data-param="blast" class="create btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-reply"></i> Send</a>
		<a href="" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
		<!--<a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->
	</div>
    
	<div class="clearfix"></div>
	<?php }else{;?>
	<div class="mail-body">
		<h2>Please Create <b>Email Configuration</b> in Website Settings First</h2>
		<a href="<?php echo base_url($zone);?>/admin/web_settings/1"><h3>Click Here</h3></a>
	</div>
	<?php };?>
</div>
<!-- SUMMERNOTE -->
<script src="<?php echo base_url('assets');?>/admin/js/plugins/summernote/summernote.min.js"></script>
<script src="<?php echo base_url('assets');?>/admin/js/plugins/fileinput/fileinput.min.js"></script>
<script>
$(document).ready(function(){
	$('#type').change(function() {
		if ($(this).find(':selected').val() != '') {
			if($(this).find(':selected').val() == 'automatic'){
				$('#automatic').slideDown();
				$('#automatic .chosen-container').css('width','350px');
				$('#automatic .chosen-select').attr('name','inputan_array');
				$('#manual').slideUp();
				$('#manual .chosen-select').attr('name','');
			}else if($(this).find(':selected').val() == 'manual'){
				$('#manual').slideDown();
				$('#manual .chosen-container').css('width','350px');
				$('#manual .chosen-select').attr('name','inputan_array');
				$('#automatic').slideUp();
				$('#automatic .chosen-select').attr('name','');
			}
		}else{
			$('#manual').slideUp();
			$('#automatic').slideUp();
			$('#manual .chosen-select').attr('name','');
			$('#automatic .chosen-select').attr('name','');
		}
	});
	$(".file").fileinput("refresh",{
		showRemove: false,
		showUpload: false,
	});
});
	setTimeout(function(){$("#gal").remove();}, 500);
	$(document).ready(function(){
		$('.summernote').summernote();
	});
	var edit = function() {
		$('.click2edit').summernote({focus: true});
	};
	var save = function() {
		var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
		$('.click2edit').destroy();
	};

</script>