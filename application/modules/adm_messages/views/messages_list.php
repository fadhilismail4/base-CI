<div class="mail-box-header">
	<div class="mail-tools tooltip-demo">
		<div class="btn-group pull-right">
			<button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
			<button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>
		</div>
		<h2 style="text-transform: capitalize;float:left;margin-right: 20px;">
			<?php echo $title;?> (<?php echo $m_total ;?>)
		</h2>
		<button id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="<?php echo $title;?>" data-param="<?php echo $param;?>" class="detail btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox">
			<i class="fa fa-refresh"></i> Refresh
		</button>
		<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="read" data-title="Selected message" class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as read">
			<i class="fa fa-eye"></i> 
		</button>
		<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="important" data-title="Selected message" class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as important">
			<i class="fa fa-exclamation"></i> 
		</button>
		<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="trash" data-title="Selected message"class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash">
			<i class="fa fa-trash-o"></i> 
		</button>
		<button id="" data-url="<?php echo $child;?>" data-lang="2" data-status="unread" data-title="Selected message" class="modal_status btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as unread">
			<i class="fa fa-eye-slash"></i> 
		</button>
	</div>
</div>
<div class="mail-box">
	<table class="table table-hover table-mail">
		<tbody>
			<?php foreach($messages as $m){?>
			<tr class="<?php if(($m->read_status == 3) && ($m->sender != $this->session->userdata('adm_admin_id'))){echo 'unread' ;}else{echo 'read' ;}?>">
				<td class="check-mail">
					<input id="<?php echo $m->message_id;?>" type="checkbox" class="i-checks">
				</td>
				<td class="mail-ontact"><a id="<?php echo $m->message_id;?>" data-url="<?php echo $child;?>" data-url2="detail" data-lang="" data-param="<?php echo $title;?>" class="detail"><?php echo $m->name ;?></a></td>
				<td class="mail-subject"><a id="<?php echo $m->message_id;?>" data-url="<?php echo $child;?>" data-url2="detail" data-lang="" data-param="<?php echo $title;?>" class="detail"><?php echo $m->subject ;?></a></td>
				<td class="text-right mail-date"><?php if( date("Y-m-d") != date("Y-m-d", strtotime($m->datecreated))){ echo date("M d", strtotime($m->datecreated)) ;}else{ echo date("H:i", strtotime($m->datecreated))  ;} ;?></td>
			</tr>		
			<?php } ;?>
		</tbody>
	</table>
</div>
<script>
$( document ).ready(function() {
	$("[data-status='read']").hide();
	$("[data-status='important']").hide();
	$("[data-status='trash']").hide();
	$("[data-status='unread']").hide();
	
	$('.check-mail input').on('ifToggled', function (event) {
		if($(this).closest('tr').hasClass('unread')){
			$("[data-status='read']").slideDown();
		}else{
			$("[data-status='unread']").slideDown();
		}
		brands();
	});
	
	function brands() {
		var brands = [];
		$('.check-mail input').each(function (index, value) {
			if ($(this).is(':checked')) {
				brands.push($(this).attr('id'));
			}
		});
		
		$("[data-status='important']").slideDown();
		$("[data-status='trash']").slideDown();
		
		
		$("[data-status='read']").attr("id",brands);
		$("[data-status='important']").attr("id",brands);
		$("[data-status='trash']").attr("id",brands);
		$("[data-status='unread']").attr("id",brands);
		if(!brands.length){
			$("[data-status='read']").slideUp();
			$("[data-status='important']").slideUp();
			$("[data-status='trash']").slideUp();
			$("[data-status='unread']").slideUp();
		}
	}

});
</script>