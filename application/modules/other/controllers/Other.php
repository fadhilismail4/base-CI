<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other extends Admin {
	
	public function export_user($zone,$type){
		if ($this->s_login && !empty($type))
		{
			$view = '';
			$status = 'error';
			$message = '';
			if($type == 'extract'){
				$data = array();
				$name = '';
				if(isset($_FILES['export_user'])){
					if (!is_dir('assets/'.$this->zone.'/other'))
					{
						mkdir('./assets/'.$this->zone.'/other', 0777, true);
					}
					$path = $this->zone.'/other';
					$file_name = 'export_user';
					$file = 'export_user';
					$config = unggah_berkas($path, $file_name, 'xls|xlsx');
					$this->upload->initialize($config);
													
					if (!$this->upload->do_upload($file))
					{	
						$status = 'error';
						$message = 'No data Extracted. Cheers...';
					}
					else
					{
						$upload_data = $this->upload->data();
						
						$file_dir = 'assets/'.$this->zone.'/other/'.$file_name.$upload_data['file_ext'];
						if(is_file($file_dir))
						{
							chmod($file_dir, 0777);
						}
						$status = "success";
						$message = "Data has been Extracted";	
						
						$data['excels'] = $this->export_excel('extract',$file_dir);
						
						$name = $file_name.$upload_data['file_ext'];
					}
				}else{
					$status = 'error';
					$message = "No Data Extracted";
				}
				echo json_encode(array('status' => $status, 'm' => $message, 'datas'=> $data, 'filename'=> $name));
				//echo $view;
			}else if($type == 'create'){
				$data = array();
				$name = $this->input->post('data');
				$datas = $this->export_excel('create','assets/'.$this->zone.'/other/'.$name);
				$i = 0;
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				foreach($datas['ready'] as $k => $d){
					if(!empty($d)){
						$flag = false;
						
						foreach($d as $kk => $dd){
							if(!empty($dd)){
								$flag = true;
								continue;
							}
							if($flag){
								if(isset($d['email'])){
									if(!empty($d['email'])){
										$cek_email = $this->dbfun_model->get_data_bys('email', 'LOWER(email) like "'.strtolower($d['email']).'" and ccs_id = '.$this->zone_id.' and status < 2','usr');
										if(empty($cek_email)){
											$data[$i] = $d;
											$data[$i]['activation_key'] = substr( str_shuffle( $chars ), 0, 10 );
											$data[$i]['approvedby'] = $this->admin_id;
											$data[$i]['ccs_id'] = $this->zone_id;
											$data[$i]['createdby'] = $this->admin_id;
											if(empty($data[$i]['description'])){
												$data[$i]['description'] = '';
											}else{
												if(is_array($data[$i]['description'])){
													$temp = '';
													foreach($data[$i]['description'] as $key => $ds){
														$temp .= $key.' = '.$ds.'<br>';
													}
													$data[$i]['description'] = $temp;
												}
											}
											$data[$i]['loc_id'] = 2;
											$data[$i]['love'] = 0;
											$data[$i]['username'] = strtolower(str_replace(' ','',$data[$i]['name']));
											$data[$i]['password'] = MD5(intval(preg_replace('/[^0-9]+/', '', $data[$i]['reg_id']), 10));
											$data[$i]['role_id'] = 0;
											$data[$i]['rt_id'] = 0;
											$data[$i]['rw_id'] = 0;
											$data[$i]['status'] = 1;
											$data[$i]['support'] = 0;
											$data[$i]['type_id'] = 2;
											$i++;
											break;
										}
									}
									else{
										$cek_user = $this->dbfun_model->get_data_bys('name', 'LOWER(name) like "'.strtolower($d['name']).'" or reg_id = "'.$d['reg_id'].'" and ccs_id = '.$this->zone_id.'  and status < 2','usr');
										if(empty($cek_user)){
											$data[$i] = $d;
											$data[$i]['activation_key'] = substr( str_shuffle( $chars ), 0, 10 );
											$data[$i]['approvedby'] = $this->admin_id;
											$data[$i]['ccs_id'] = $this->zone_id;
											$data[$i]['createdby'] = $this->admin_id;
											if(empty($data[$i]['description'])){
												$data[$i]['description'] = '';
											}else{
												if(is_array($data[$i]['description'])){
													$temp = '';
													foreach($data[$i]['description'] as $key => $ds){
														$temp .= $key.' = '.$ds.'<br>';
													}
													$data[$i]['description'] = $temp;
												}
											}
											$data[$i]['loc_id'] = 2;
											$data[$i]['love'] = 0;
											$data[$i]['username'] = strtolower(str_replace(' ','',$data[$i]['name']));
											$data[$i]['password'] = MD5(intval(preg_replace('/[^0-9]+/', '', $data[$i]['reg_id']), 10));
											$data[$i]['role_id'] = 0;
											$data[$i]['rt_id'] = 0;
											$data[$i]['rw_id'] = 0;
											$data[$i]['status'] = 1;
											$data[$i]['support'] = 0;
											$data[$i]['type_id'] = 2;
											$i++;
											break;
										}
									}
								}
							}
						}
					
					}
				}
				
				if(!empty($data)){
					foreach($data as $da){
						$this->new_data('ccs_usr',$da,'','','','');
					}
					
					$status = 'success';
					$message = 'Users Created';
				}else{
					$status = 'error';
					$message = 'No User Created'.$flag;
				}
				
				
				echo json_encode(array('status' => $status, 'm' => $message, 'datas'=> $data));
			}
		}else{
			redirect($this->login);
		}
	}	
	
}