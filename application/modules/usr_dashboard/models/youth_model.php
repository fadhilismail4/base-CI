<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Youth_model extends CI_Model { 
		
		function get_ideas( $id){
			$q="
				SELECT distinct(c.idea_id), c.category_id, t.title, t.parent_id
				FROM com_ideadesc c, com_ideacat t
				WHERE  t.category_id = ".$id."
				and c.category_id=t.category_id
				and t.language_id=2
				UNION 
				SELECT distinct(c.idea_id), c.category_id,  t.title, t.parent_id
				FROM com_ideadesc c, com_ideacat t
				WHERE  t.parent_id = ".$id."
				and c.category_id=t.category_id
				and t.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
				
		function get_connection($id){
			$q="
				select c.user_id as connection_id, u.avatar, c.category_id, c.status, d.name, d.description, c.user_id, c.url_id, d.address, d.address, d.mobile, d.phone , d.email
				from base_usermember d, com_connection c , base_user u
				where c.user_id=d.user_id and c.url_id=".$id." and c.category_id=1 and d.user_id = u.user_id
				union
				select c.url_id as connection_id, u.avatar, c.category_id, c.status, d.name, d.description, c.user_id, c.url_id , d.address, d.address, d.mobile, d.phone , d.email
				from base_usermember d, com_connection c , base_user u
				where c.url_id=d.user_id and c.user_id=".$id." and c.category_id=1 and d.user_id = u.user_id
			";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_following($id){
			$q="
				select c.category_id, c.status,  d.name, d.description, d.user_id 
				from base_userpartner d, com_connection c 
				where c.url_id=d.user_id and c.user_id = ".$id." and c.category_id = 2 
				UNION 
				select c.category_id, c.status,  d.title as name, d.description, d.idea_id as user_id 
				from com_ideadesc d, com_connection c 
				where c.url_id=d.idea_id and c.user_id = ".$id." and d.language_id = 2 and c.category_id = 3 
				UNION 
				select c.category_id, c.status,  d.title as name, d.description, d.development_id as user_id 
				from com_developmentdesc d, com_connection c 
				where c.url_id=d.development_id and c.user_id = ".$id." and d.language_id=2 and c.category_id = 4 
			";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_log($id){
			$q="
			select * from(
				select c.category_id, c.url_id, c.status, d.name, c.user_id,  d.user_id as con_id, c.datecreated, c.avatar 
				from base_usermember d, base_userlog c 
				where c.user_id=d.user_id and c.user_id=".$id." and c.category_id=0 
				UNION 
				select c.category_id, c.url_id, c.status, d.name, c.user_id,  d.user_id as con_id, c.datecreated , c.avatar 
				from base_usermember d, base_userlog c 
				where c.url_id=d.user_id and c.user_id=".$id." and c.category_id=1 and c.status = 1
				UNION 
				select c.category_id, c.url_id, c.status, d.name, c.user_id,  d.user_id as con_id, c.datecreated , c.avatar 
				from base_usermember d, base_userlog c 
				where c.user_id=d.user_id and c.url_id=".$id." and c.category_id=1 and c.status = 1
				UNION 
				select c.category_id, c.url_id, c.status,  d.name, c.user_id,  d.user_id as con_id, c.datecreated , c.avatar 
				from base_userpartner d, base_userlog c 
				where c.url_id=d.user_id and c.user_id=".$id." and c.category_id=2 
				UNION 
				select c.category_id, c.url_id, c.status,  d.title as name, c.user_id,  d.idea_id as con_id, c.datecreated , c.avatar 
				from com_ideadesc d, base_userlog c 
				where c.url_id=d.idea_id and c.user_id=".$id." and d.language_id=2 and c.category_id=3 
				UNION 
				select c.category_id, c.url_id, c.status,  d.title as name, c.user_id,  d.development_id as con_id, c.datecreated , c.avatar 
				from com_developmentdesc d, base_userlog c 
				where c.url_id=d.development_id and c.user_id=".$id." and d.language_id=2 and c.category_id=4 
			) as log order by log.datecreated desc
			";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_loginfo($id){
			$q="
			select * from(
				select c.avatar as image_square, c.category_id, c.url_id, c.status, d.name, d.description, d.user_id, c.user_id as con_id,  c.datecreated 
				from base_usermember d, base_userlog c, base_user u 
				where c.url_id=d.user_id and c.user_id=".$id." and c.category_id=1 and c.status = 1 and d.user_id=u.user_id
				UNION 
				select u.avatar, c.category_id, c.url_id, c.status, d.name, d.description, d.user_id, c.user_id as con_id,c.datecreated 
				from base_usermember d, base_userlog c, base_user u 
				where c.user_id=d.user_id and c.url_id=".$id." and c.category_id=1 and c.status = 1 and d.user_id=u.user_id
				UNION 
				select c.avatar as image_square ,c.category_id, c.url_id, c.status,  d.name, d.description, d.user_id, c.user_id as con_id,c.datecreated 
				from base_userpartner d, base_userlog c, base_user u 
				where c.url_id=d.user_id and c.user_id=".$id." and c.category_id=2 and d.user_id=u.user_id
				UNION 
				select c.avatar as image_square, c.category_id, c.url_id, c.status,  d.title as name, d.description, d.idea_id as user_id, c.user_id as con_id,c.datecreated 
				from com_ideadesc d, base_userlog c 
				where c.url_id=d.idea_id and c.user_id=".$id." and d.language_id=2 and c.category_id=3 
				UNION 
				select c.avatar as image_square, c.category_id, c.url_id, c.status,  d.title as name, d.description, d.development_id as user_id, c.user_id as con_id, c.datecreated 
				from com_developmentdesc d, base_userlog c 
				where c.url_id=d.development_id and c.user_id=".$id." and d.language_id=2 and c.category_id=4
			) as log order by log.datecreated desc
			";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_detail($id){
			$q="
				select * from(
					select i.love, i.support,c.url_id, c.datecreated, c.category_id, c.status,  d.name, d.description, d.user_id , i.avatar, l.loc_id, l.parent_id, l.title as loc, l.parent_title as ploc, l.type_id, i.user_id as creator_id, i.name as creator,d.datecreated as datestart, d.dateupdated as dateend 
					from base_userpartner d, com_connection c , base_user i, base_locdesc l
					where c.url_id = d.user_id and c.user_id=".$id." and c.category_id = 2 and i.user_id = d.user_id and l.loc_id = i.loc_id and l.language_id = 2
					UNION 
					select i.love, i.support,c.url_id, c.datecreated, c.category_id, c.status,  d.title as name, d.description, d.idea_id as user_id , i.image_square as avatar, l.loc_id, l.parent_id, l.title as loc, l.parent_title as ploc, l.type_id, u.user_id as creator_id, u.username as creator , i.datestart, i.dateend
					from com_ideadesc d, com_connection c , com_idea i, base_user u, base_locdesc l
					where c.url_id = d.idea_id and c.user_id=".$id." and d.language_id = 2 and c.category_id = 3 and i.idea_id = d.idea_id and d.createdby = u.user_id  and l.loc_id = d.loc_id and l.language_id = d.language_id
					UNION 
					select i.love, i.support, c.url_id, c.datecreated, c.category_id, c.status, d.title as name, d.description, d.development_id as user_id , i.image_square as avatar, l.loc_id, l.parent_id, l.title as loc, l.parent_title as ploc, l.type_id, u.user_id as creator_id, u.username as creator , i.datestart, i.dateend 
					from com_developmentdesc d, com_connection c , com_development i, base_user u, base_locdesc l
					where c.url_id = d.development_id and c.user_id=".$id." and d.language_id = 2 and c.category_id = 4 and d.development_id = i.development_id and d.createdby = u.user_id  and l.loc_id = d.loc_id and l.language_id = d.language_id
				) as detail order by detail.datecreated desc
			";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_category($id){
			$q="
				SELECT distinct(c.idea_id), c.category_id, t.title, t.parent_id
				FROM com_ideadesc c, com_ideacat t
				WHERE  t.category_id = ".$id."
				and c.category_id=t.category_id
				and t.language_id=2
				UNION 
				SELECT distinct(c.idea_id), c.category_id, t.title, t.parent_id
				FROM com_ideadesc c, com_ideacat t
				WHERE  t.parent_id = ".$id."
				and c.category_id=t.category_id
				and t.language_id=2
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
		
		function get_listcat($id){
			$q="
				SELECT distinct(m.idea_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.idea_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name, m.datestart, m.dateend
				FROM com_ideacat c, com_idea m, com_ideadesc d, base_admin a
				WHERE  m.idea_id = d.idea_id 
				and d.language_id = c.language_id 
				and c.category_id = d.category_id 
				and a.admin_id = d.createdby
				and c.language_id=2
				and c.category_id = ".$id."
				UNION 				
				SELECT distinct(m.idea_id), d.category_id, d.language_id, d.metakeyword, d.metadescription, d.description, c.title as category, m.idea_id, c.parent_id, c.parent_title as pcat, m.image_square as image, d.title, d.viewer, d.status, d.createdby, d.datecreated, a.name, m.datestart, m.dateend
				FROM com_ideacat c, com_idea m, com_ideadesc d, base_admin a
				WHERE  c.category_id=d.category_id	
				and m.idea_id = d.idea_id 
				and d.language_id = c.language_id 
				and a.admin_id = d.createdby
				and c.language_id=2
				and c.parent_id = ".$id."
				";
			$result = $this->db->query($q)->result();
			return $result; 
			mysql_close($q);
		}
				
	}
	