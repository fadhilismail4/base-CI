<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Dashboard extends User {
		
	public function index($zone)
	{	
		if (($this->s_login && ($this->zone_type_id != 3)) && ($this->user_type))
    	{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			$data['child'] = '';
			$data['parent'] = 'dashboard';
			$data['ch'] = array(
				'title'=>'Dashboard', 
				'small'=>"Sekilas tentang informasi data anda."
			);
			$data['breadcrumb'] = array(
				array(
					'url' => $this->zone.'/user/dashboard', 
					'bcrumb' => 'Dashboard', 
					'active' => 'active'
				)
			);
			//End set parameter view
			$data['css'] = '';
			$data['js'] = '';
			$data['custom_module'] = $custom_module;
			$data['content'] = 'dashboard.php';
			if($zone == 'contribyouth'){
				$id = $this->user_id;
				$type_id = $this->user_type;
				$lang = 2;
				$this->load->model('Youth_model');
				$data['user_id'] = $this->session->userdata($zone.'_user_id');
				$data['youth'] = $this->dbfun_model->get_data_bys('avatar, user_id, name, username, datecreated, status, email','user_id = '.$id.' and type_id = '.$type_id.' ','usr');
				$datas = $this->dbfun_model->get_all_data_bys('distinct(url_id), ccs_key, user_id, language_id,datecreated as created','ccs_id = '.$this->zone_id.' and ccs_key != 0 and user_id = '.$data['user_id'].' order by datecreated DESC','ulg');
				$datas2 = array();
				foreach($datas as $d){
					if($d->ccs_key == 1){//this user followed other user and what they post
						$cek_follow = $this->dbfun_model->get_data_bys('url_id,user_id,type,ccs_key','ccs_id = '.$this->zone_id.' and ccs_key = '.$d->ccs_key.' and user_id = '.$data['user_id'].' order by datecreated DESC','ulg');
						if(($cek_follow->type != 7) || ($cek_follow->type != 8)){
							$cek_user_post = $this->dbfun_model->get_data_bys('distinct(url_id), ccs_key, user_id, language_id,datecreated as created','ccs_id = '.$this->zone_id.' and ccs_key = 43 or ccs_key = 46 and datecreated >= "'.$d->created.'" order by datecreated DESC','ulg');
							if(!empty($cek_user_post)){
								$cek_usr = $this->dbfun_model->get_data_bys('name,avatar,type_id','ccs_id = '.$this->zone_id.' and user_id = '.$cek_user_post->user_id.'','usr');
								$cek_module = $this->ccs_key_to_module($cek_user_post->ccs_key);
								$cek_usr_type = $this->dbfun_model->get_data_bys('name,type_id','ccs_id = '.$this->zone_id.' and type_id = '.$cek_usr->type_id.'','umr');
								$cek_o_n_odc = $this->get_object_by_id_custom(',ccs_odc.dateupdated,ccs_odc.createdby,ccs_odc.tu','','',$cek_user_post->url_id,$lang);
								if(!empty($cek_o_n_odc)){//from user
									$cek_log = $this->dbfun_model->get_all_data_bys('user_id,type','ccs_id = '.$this->zone_id.' and url_id = '.$cek_o_n_odc->object_id.' and ccs_key = '.$cek_user_post->ccs_key.' and type > 4','ulg');//cek log from this object
									$cek_category = $this->get_category($cek_module,$cek_o_n_odc->category_id,$lang);
									$ds2 = (object) array_merge((array) $cek_o_n_odc,array('creator'=> $cek_usr->name,'avatar'=>$cek_usr->avatar,'type'=> $cek_usr_type->name));
									$ds3 = (object) array_merge((array) $d,(array) $ds2);
									$datas2[] = (object) array_merge((array) $ds3,array('module'=>$cek_module,'category'=>$cek_category,'log'=>$cek_log));
								}else{//direct to object
									$cek_o_n_odc = $this->get_object_by_id_custom(',ccs_odc.dateupdated,ccs_odc.createdby,ccs_odc.tu','','',$d->url_id,$lang);
									if(!empty($cek_o_n_odc)){
										$cek_log = $this->dbfun_model->get_all_data_bys('user_id,type','ccs_id = '.$this->zone_id.' and url_id = '.$cek_o_n_odc->object_id.' and ccs_key = '.$d->ccs_key.' and type > 4','ulg');//cek log from this object
										$cek_usr = $this->dbfun_model->get_data_bys('name,avatar,type_id','ccs_id = '.$this->zone_id.' and user_id = '.$cek_o_n_odc->createdby.'','usr');
										$cek_category = $this->get_category($cek_module,$cek_o_n_odc->category_id,$lang);
										$ds2 = (object) array_merge((array) $cek_o_n_odc,array('creator'=> $cek_usr->name,'avatar'=>$cek_usr->avatar,'type'=> $cek_usr_type->name));
										$ds3 = (object) array_merge((array) $d,(array) $ds2);
										$datas2[] = (object) array_merge((array) $ds3,array('module'=>$cek_module,'category'=>$cek_category,'log'=>$cek_log));
									}else{
										$cek_usr = $this->dbfun_model->get_data_bys('name,avatar,type_id','ccs_id = '.$this->zone_id.' and user_id = '.$d->url_id.'','usr');
										$datas2[] = (object) array_merge((array) $d,array('creator'=> $cek_usr->name,'avatar'=>$cek_usr->avatar,'type'=> $cek_usr_type->name,'createdby'=>$cek_usr_type->type_id));
									}
								}
							}
						}
					}elseif(($d->ccs_key == 43) || ($d->ccs_key == 46)){//user to object
						$cek_usr = $this->dbfun_model->get_data_bys('name,avatar,type_id','ccs_id = '.$this->zone_id.' and user_id = '.$d->user_id.'','usr');
						$cek_module = $this->ccs_key_to_module($d->ccs_key);
						$cek_usr_type = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$cek_usr->type_id.'','umr');
						$cek_o_n_odc = $this->get_object_by_id_custom(',ccs_odc.dateupdated,ccs_odc.createdby,ccs_odc.tu','','',$d->url_id,$lang);
						if(!empty($cek_o_n_odc)){
							$cek_log = $this->dbfun_model->get_all_data_bys('user_id,type','ccs_id = '.$this->zone_id.' and url_id = '.$cek_o_n_odc->object_id.' and ccs_key = '.$d->ccs_key.' and type > 4','ulg');//cek log from this object
							$cek_category = $this->get_category($cek_module,$cek_o_n_odc->category_id,$lang);
							$ds2 = (object) array_merge((array) $cek_o_n_odc,array('creator'=> $cek_usr->name,'avatar'=>$cek_usr->avatar,'type'=> $cek_usr_type->name));
							$ds3 = (object) array_merge((array) $d,(array) $ds2);
							$datas2[] = (object) array_merge((array) $ds3,array('module'=>$cek_module,'category'=>$cek_category,'log'=>$cek_log));
						}
					}
				}unset($datas);
				$data['datas'] = $datas2;
				//print_r($aw_yeah);
				//$data['activities'] =  $this->Youth_model->get_log($id);
				//$data['actinfo'] =  $this->Youth_model->get_loginfo($id);
				$data['today'] = date("Y-m-d");
				$data['sekarang'] = date("Y-m-d H:i:s");
				$data['content'] = 'contribyouth.php';
			}elseif($zone == 'binamarga'){
				$anchor = $this->dbfun_model->get_data_bys('o_email','ccs_id = '.$this->zone_id.' and user_id = '.$this->user_id.'','usr')->o_email;
				$data['css'] = '';
				$data['js'] = '';
				$data['custom_js'] = '
				<script>
					
					$(document).ready(function(){
						$("[data-url2='. "'" .'list'. "'" .']")[0].click();
					});
					$.ajax({
						type: "POST",
						url: "'.base_url($zone).'/user/dashboard/pointer",
						dataType: "html",
						data: {
							'.$this->security->get_csrf_token_name().' : "'.trim($this->security->get_csrf_hash()).'",
							cat: "'.$anchor.'"
						},
						success: function(html){
							$("script").remove( ":contains('."'initMap'".')" );
							$(html).insertAfter("div#maps");
							$("html, body").animate({scrollTop:$("#map-canvas").offset()}, "slow");
							$(".chkcat").each(function(){
								$(this).removeAttr("disabled", "disabled");
							});
							initMap();
						 }
					});
					</script>
				';
				$data['parent'] = 'dasbor';
				$data['ch'] = array(
					'title'=>'Dasbor', 
					'small'=>"Kesimpulan Seluruh Informasi"
				);
				$data['breadcrumb'] = array(
					array(
						'url' => $this->zone.'/user/dashboard', 
						'bcrumb' => 'Dasbor', 
						'active' => true
					)
				);
				$data['content'] = 'binamarga.php';
			}
			$this->load->view('admin/index',$data);
		}else{
			if($this->session->userdata($this->zone.'_admin_id')){
				redirect(base_url($this->zone).'/admin');
			}else{
				redirect(base_url($this->zone));
			}
		}
	}	
	
	public function detail_ajax($zone,$part){	
		if ($this->s_login && !empty($part))
		{
			$data = array();
			$id = $this->input->post('id');
			$view = '';
			if($part == 'map_detail'){
				$anchor = $this->input->post('anchor');
				$data['ds'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.image_square, ccs_olc.latitude, ccs_olc.longitude','ccs_o.object_id = '.$id.' and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = 9 and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = 103 and ccs_o.object_id = ccs_odc.object_id and ccs_odc.metakeyword = "'.$anchor.'" and ccs_olc.ccs_id = ccs_odc.ccs_id and ccs_olc.ccs_key = ccs_odc.ccs_key and ccs_olc.object_id = ccs_odc.object_id','ccs_odc, ccs_o, ccs_olc');
				$data['status'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 9 and ccs_key = 1','sts');
				$data['isp'] = $this->dbfun_model->get_data_bys('ccs_olg.datecreated, ccs_olg.log_id, ccs_obr.metakeyword, ccs_olg.description, ccs_usr.name','ccs_obr.utl_id = '.$id.' and ccs_obr.obr_id = ccs_olg.url_id and ccs_obr.ccs_id = ccs_olg.ccs_id and ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_id = 9 and ccs_obr.ccs_key = ccs_olg.ccs_key and ccs_obr.ccs_key = 105 and ccs_olg.createdby = ccs_usr.user_id and ccs_olg.parent_id = 0 order by ccs_olg.datecreated desc','obr, olg, usr');
				$isp = $data['isp'];
				$log = $isp->log_id;
				$data['gallery'] = $this->dbfun_model->get_all_data_bys('gallery_id, title, image_square','ccs_id = 9 and ccs_key = 105 and object_id = '.$log.' and category_id = '.$id.' limit 4','ogl');
				$view = $this->load->view('map_detail', $data, TRUE);
			}elseif($part == 'list'){				
				$anchor = $this->dbfun_model->get_data_bys('o_email','ccs_id = 9 and user_id = '.$this->user_id.'','usr')->o_email;
				$inspeksi = $this->dbfun_model->get_data_bys('module_id','ccs_id = 9 and name = "Inspeksi"','mdl')->module_id;
				$jadwal = $this->dbfun_model->get_data_bys('module_id','ccs_id = 9 and name = "Penjadwalan"','mdl')->module_id;
				$data['status'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 9 and ccs_key = 2','sts');			
				$data['objects']= $this->dbfun_model->get_all_data_bys('ccs_obr.*, ccs_usr.name, ccs_oct.title as tahun, ccs_odc.category_id as c_id','ccs_obr.ccs_id = ccs_usr.ccs_id and ccs_obr.ccs_key = '.$inspeksi.' and ccs_obr.metakeyword = "'.$anchor.'" and ccs_obr.createdby = ccs_usr.user_id and ccs_odc.ccs_id = ccs_obr.ccs_id and ccs_odc.ccs_key = '.$jadwal.' and ccs_odc.object_id = ccs_obr.category_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_obr.type_id = ccs_oct.category_id and ccs_oct.ccs_key = ccs_odc.ccs_key order by ccs_obr.datestart desc limit 7','obr, usr, odc, oct');
									
				$view = $this->load->view('map_list', $data, TRUE);
			}
			echo $view;
		}
	}
	
	private function get_map($poi, $zoom, $center){
		$data = array();
		$aww = null;
		if($poi && is_array($poi)){
			foreach($poi as $val){
				if((substr_count($val->latitude,'.') == 1) && (substr_count($val->longitude,'.') == 1)){
					$aww[] = ('[\'aa\','.$val->latitude.','.$val->longitude.','.$val->status.','.$val->object_id.',"'.$val->metakeyword.'"],');
				}else{
					$posisiX= trim($val->latitude);
					$posisiY= trim($val->longitude);
					if ($dot=strrpos($posisiY,".")) {
						$posisiY=trim(substr($posisiY,0,$dot)).substr(str_replace('.','',$posisiY),$dot);
					}
					if ($dot=strrpos($posisiX,".")) {
						$posisiX=trim(substr($posisiX,0,$dot)).substr(str_replace('.','',$posisiX),$dot);
					}
					$aww[] = ('[\'aa\','.$posisiY.','.$posisiX.','.$val->status.','.$val->object_id.',"'.$val->metakeyword.'"],');
				}
			}
			$data['peta_pointer'] = $aww;
		}else{
			$data['peta_pointer'] = '';
		}
		$data['zone'] = $this->zone;
		$data['zoom'] = $zoom;
		$data['center'] = $center;
		$view = $this->load->view('map_init', $data, TRUE);
		echo $view;
	}
	
	public function pointer($zone){
		$zoom = '13';
		$center = '-6.920867, 107.609558';
		$anchor = $this->input->post('cat');
		if (!empty($anchor)){
			$poi = $this->dbfun_model->get_all_data_bys('ccs_odc.object_id,ccs_odc.status,ccs_odc.metakeyword,ccs_odc.status,ccs_olc.latitude, ccs_olc.longitude','ccs_o.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = 9 and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_o.ccs_key = 103 and ccs_o.object_id = ccs_odc.object_id and ccs_odc.metakeyword = "'.$anchor.'" and ccs_olc.ccs_id = ccs_odc.ccs_id and ccs_olc.ccs_key = ccs_odc.ccs_key and ccs_olc.object_id = ccs_odc.object_id','ccs_odc, ccs_o, ccs_olc');
			$this->get_map($poi, $zoom, $center);
		}else{
			$poi = '';
			$this->get_map($poi, $zoom, $center);
		}
	}
}