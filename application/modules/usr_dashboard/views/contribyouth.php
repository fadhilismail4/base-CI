<div class="row">
	<div class="col-md-9">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Recent Activites</h5>
				<button type="button" id="" data-lang="" data-url="youth" data-url2="activities" class="btn btn-default btn-sm  pull-right"><i class="fa fa-puzzle-piece"></i> Youth Dashboard</button>
			</div>
			<div class="ibox-content">
				<div>
					<div class="feed-activity-list">
						<?php if(!empty($datas)){foreach($datas as $a){
							if(isset($a->tu)){
								if(($a->createdby == $user_id) && ($a->tu != 0)){
								$creator = 'You';
								}else{
									if($a->ccs_key == 1){
										$creator = 'You';
									}else{
										$creator = $a->creator;
									}
								}
							}else{
								if($a->ccs_key == 1){
									$creator = 'You';
								}else{
									$creator = $a->creator;
								}
							}
							
							if($a->avatar){
								$creator_img = base_url('assets/'.$zone.'/user/'.strtolower($a->type).'/'.$a->createdby.'/'.$a->avatar.'');
							}else{
								$creator_img = base_url($this->session->userdata($zone.'_foto'));
							}
							$user_img = base_url($this->session->userdata($zone.'_foto'));
							$created = strtotime($a->created); 
							$nows= strtotime(date('Y-m-d H:i:s')); 
							$range= $nows-$created; 
							$jam = round(($range )/60);
							if( $today == date("Y-m-d", strtotime($a->created))){ 
								if($jam < 60){
									$time = $jam." minutes ago";
								}else{
									$time = round($jam/60)." hour ago";
								}
							}else{ 
								if($jam < 2880){
									$time = " yesterday";
								}else{
									$time = (date("D M Y", strtotime($a->created)));
								}
							}
							?>
							<div class="feed-element">
								<a href="#" class="pull-left">
									<img alt="image" class="img-circle" src="<?php if($a->createdby == $user_id){echo $creator_img;}else{ echo $user_img;}?>">
								</a>
								<div class="media-body ">
									<small class="pull-right text-navy">							
									<?php echo $time;?>
									</small>
									
									<?php if($a->createdby == $user_id){;?>
										<strong>
											<?php echo $creator?>
										</strong> 
										created
										<strong>
											<a href="
												<?php if($a->category_id == 2){
															echo site_url()."admin/organization/detail/".$a->url_id;
														}elseif($a->category_id == 3){
															echo site_url()."admin/ideas/detail/".$a->url_id;
														}else{
															echo site_url()."admin/development/detail/".$a->url_id;
														}
												?>">
												<?php echo $a->title?>
											</a> 
										</strong>. 
									<?php }elseif(($a->ccs_key == 0) && ($a->url_id == $user_id)){ ;?>	
										<strong>
											<a href="<?php echo base_url()?>admin/youth/detail/<?php echo $a->con_id ;?>">
												<?php echo $a->name?>
											</a> 
										</strong> started following <strong>you</strong>. 
									<?php ;}elseif(($a->ccs_key == 1) && ($a->user_id == $user_id)){ ;?>
										<strong>
											<?php echo $creator?>
										</strong> 
										started following
										<strong>
											<?php if(isset($a->title)){;?>
											<a href="#">
												<?php echo $a->title?>
											</a> 
											<?php }else{;?>
											<a href="#">
												<?php echo $a->creator?>
											</a> 
											<?php };?>
										</strong>. 
									<?php ;}else{ ;?>
										<strong>
											<?php echo $youth->name?>
										</strong> 
										started to support
										<strong>
											<a href="
												<?php if($a->category_id == 2){
															echo site_url()."admin/organization/detail/".$a->url_id;
														}elseif($a->category_id == 3){
															echo site_url()."admin/ideas/detail/".$a->url_id;
														}else{
															echo site_url()."admin/development/detail/".$a->url_id;
														}
												?>">
												<?php echo $a->title?>
											</a> 
										</strong>. 
									<?php ;} ;?>
									<br>
									<small class="text-muted">
										<?php if( $today == date("Y-m-d", strtotime($a->created))){ echo "Today ".date("H:i", strtotime($a->created)) ;}else{ echo date("H:i:s a, d M Y", strtotime($a->created))  ;} ;?>
									</small>												
									<?php if(($a->ccs_key == 43) || ($a->ccs_key == 46)){ ?>
										<div class="photos">								
											<div class="pull-left" style="padding-right: 10px;">
												<a target="" href="#"> 
													<img alt="image" class="feed-photo" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo strtolower($a->module);?>/<?php echo $a->image_square;?>">
												</a>
											</div>
										</div>
										<div class="media-body ">
											<a href="<?php echo base_url('') ?>admin/development/detail/<?php echo $a->url_id?>">
												<h5><?php echo $a->title ;?></h5>
											</a>
											<p>
												<?php echo word_limiter(strip_tags($a->description),51) ?>
											</p>
										</div>	
									<?php }elseif(($a->ccs_key == 1) && (isset($a->title))){ ;?>
										<div class="photos">								
											<div class="pull-left" style="padding-right: 10px;">
												<a target="" href="#"> 
													<img alt="image" class="feed-photo" src="<?php echo base_url('assets');?>/<?php echo $zone;?>/<?php echo strtolower($a->module);?>/<?php echo $a->image_square;?>">
												</a>
											</div>
										</div>
										<div class="media-body ">
											<a href="<?php echo base_url('') ?>admin/development/detail/<?php echo $a->url_id?>">
												<h5><?php echo $a->title ;?></h5>
											</a>
											<p>
												<small>
													<strong>by:</strong> 
													<a href="#" class="">
														<img alt="image" class="img-circle" src="<?php echo $creator_img;?>">
														<?php echo $a->creator;?>
													</a>
												</small>
											</p>
											<p>
												<?php echo word_limiter(strip_tags($a->description),51) ?>
											</p>
										</div>	
									<?php };?>
								</div>
							</div>										
						<?php };};?>
					</div>
					<button class="btn btn-primary btn-block" style="margin-top:15px"><i class="fa fa-arrow-down"></i> Show More</button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
	ini sidebar
	</div>
</div>