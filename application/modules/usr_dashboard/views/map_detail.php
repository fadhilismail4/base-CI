<div class="ibox-title row" style="margin-left: 2px">
	<h5>Jembatan <?php echo $ds->title;?></h5>
	<div class="ibox-tools">
		<i id="all" data-url="dashboard" data-url2="list" data-param="" data-lang="2" class="detail2 fa fa-times pull-right btn"></i>
	</div>
</div>
<div class="ibox row " style="margin-left: 2px">
	<div class="ibox-content">
		<div class="row" >
			<div class="col-sm-12 col-xs-12"style="padding-left:  0px">
				<div class="col-sm-4 col-xs-4 " style="padding-left: 10px"><p>Nomor</p></div>
				<div class="col-sm-8 col-xs-8"><p>: <strong><?php echo $ds->tagline;?></strong></p></div>
			</div>
			<div class="col-sm-12 col-xs-12"style="padding-left:  0px">
				<div class="col-sm-4 col-xs-4 " style="padding-left: 10px"><p>Nama</p></div>
				<div class="col-sm-8 col-xs-8"><p>: <strong>Jembatan <?php echo $ds->title;?></strong></p></div>
			</div>
			<div class="col-sm-12 col-xs-12"style="padding-left:  0px">
				<div class="col-sm-4 col-xs-4 " style="padding-left: 10px"><p>Lokasi</p></div>
				<div class="col-sm-8 col-xs-8">
					<p>: 
						<a href="<?php echo base_url('binamarga')?>/user/jembatan/">
							<?php echo $ds->metadescription;?>
						</a>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12"style="padding-left:  0px">
				<div class="col-sm-4 col-xs-4 " style="padding-left: 10px"><p>Status Terakhir</p></div>
				<div class="col-sm-8 col-xs-8">
					<p>: <strong>
						<?php foreach($status as $s){ ;?>
							<?php if($s->value == $ds->status){ echo $s->name ;} ;?>
						<?php } ;?>
					</strong></p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12"style="padding-left:  0px">
				<div class="col-sm-4 col-xs-4 " style="padding-left: 10px"><p>Inspeksi Terakhir</p></div>
				<div class="col-sm-8 col-xs-8">
					<p>: 
						 <?php echo date("d M Y H:i:s", strtotime($ds->dateupdated)) ?>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12"style="padding-left:  0px">
				<div class="col-sm-4 col-xs-4 " style="padding-left: 10px"><p>Oleh</p></div>
				<div class="col-sm-8 col-xs-8">
					<p>: 
						 <?php echo $isp->name;?>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12"style="padding-left:  0px">
				<div class="col-sm-12 col-xs-12 " style="padding-left: 10px"><p><strong>Catatan </strong></p></div>
				<div class="col-sm-12 col-xs-12 " style="padding-left: 10px"><p><?php echo word_limiter(strip_tags($isp->description), 15);?></p></div>
			</div>
			<div class="col-sm-12 col-xs-12">			
				<div class="col-sm-12 col-xs-12" style="padding:  0px" >
					<?php foreach($gallery as $gal){
						if(file_exists('assets/binamarga/gallery/'.$gal->image_square))	{									
					?>
						<a class="fancybox " title="<?php echo $gal->title?>"  rel="gallery" href="<?php echo base_url()?>assets/binamarga/gallery/<?php echo $gal->image_square?>">
							<img src="<?php echo base_url()?>assets/binamarga/gallery/thumbnail/<?php echo $gal->image_square?>" width="60%" height="auto" alt="chimp-1-th">
						</a>
					<?php ;};}?>									
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/plugins/fancybox/jquery.fancybox-thumbs.js"></script>
<script id="fancy_script" type="text/javascript">
if (!$("link[href='<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css']").length){
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">').appendTo("head");
	$('<link href="<?php echo base_url();?>assets/admin/css/plugins/fancybox/jquery.fancybox-thumbs.css" rel="stylesheet">').appendTo("head");
	$(document).ready(function() {
		$(".fancybox")
			.fancybox({
				prevEffect	: 'none',
				nextEffect	: 'none',
				helpers : {
					title : {
						type: 'inside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					},
					buttons	: {}
				}  
			});
	});
}
</script>   
<script>
$(document).ready(function(){
	$('#data_gallery').hide();
	$('#edit_gallery').change(function() {
		if ($(this).val() != '') {
			$('#data_gallery').slideDown('slow');
		}else{
			$('#data_gallery').slideUp('slow');
		}
	});
});
</script> 