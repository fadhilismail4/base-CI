<div class="ibox-title row" style="margin-left: 2px">
	<h5>Daftar Inspeksi Berikutnya</h5>
</div>
<div class="ibox row" style="margin-left: 2px">
	<div class="ibox-content">
		<table class="table table-hover margin bottom">
			<thead>
				<tr>
					<th style="width: 1%" class="text-center">No.</th>
					<th>Jembatan</th>
					<th class="text-center; width: 25%">Tanggal</th>
					<th class="text-center">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach($objects as $n){ ?>
				<tr>
					<td class="text-center"><?php echo $i ;?></td>
					<td>
						<?php echo $n->parole ;?>
						<br>
						<?php echo $n->chiave ;?>
					</td>
					<td class="text-center small">
						<?php echo date("d M Y", strtotime($n->datestart)) ?>
						<br>
						- 
						<?php echo date("d M Y", strtotime($n->dateend)) ?>
					</td>
					<td class="text-center">
						<?php foreach($status as $s){ if($s->value == $n->status ){ ;?>
						<span class="label label-<?php echo $s->color?>">
							<?php echo $s->name?>
						</span>
						<?php ; } ;} ;?>
					</td>
				</tr>
				<?php $i++;} ?>
			</tbody>
		</table>
		<div class="row">
			<a href="<?php echo base_url('')?>binamarga/user/inspeksi/1" class="btn-sm btn btn-primary pull-right ">Lihat Lainnya</a>
		</div>
	</div>
</div>