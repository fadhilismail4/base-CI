<script>
function initMap(){
	var options = {
		zoom: <?php echo trim($zoom);?>,
		center: new google.maps.LatLng(<?php echo trim($center);?>),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById('map-canvas'), options);
	<?php
		if($peta_pointer){
			?>var locations = [
				<?php 
					foreach($peta_pointer as $val){
						print_r(trim($val));
					}
				?>
			];
		<?php }else{ ?>
			var locations = [];
		<?php }
	?>
	var infowindow = new google.maps.InfoWindow();

	var marker, i;
	 /* kode untuk menampilkan banyak marker */
	for (i = 0; i < locations.length; i++) {
		if(locations[i][3] == 0){
			var map_icon = '<?php echo base_url();?>assets/img/map_icon/bridge_green.png';
		}else if(locations[i][3] == 1){
			var map_icon = '<?php echo base_url();?>assets/img/map_icon/bridge_blue.png';
		}else if(locations[i][3] == 2){
			var map_icon = '<?php echo base_url();?>assets/img/map_icon/bridge_yellow.png';
		}else if(locations[i][3] == 3){
			var map_icon = '<?php echo base_url();?>assets/img/map_icon/bridge_red.png';
		}else{
			var map_icon = '<?php echo base_url();?>assets/img/map_icon/bridge_red.png';
		};
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map,
			icon: map_icon
		  });
		  marker.setValues({type: locations[i][5], id: locations[i][4]});
		
	  google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
		  var datas = {
			  <?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
			  id : locations[i][4],
			  anchor : locations[i][5]
		  }
		  var url = "<?php echo base_url($zone);?>/user/dashboard/view_detail/map_detail";
			$.ajax({
				url : url,
				type: "POST",
				dataType: 'html',
				data: datas,
				success: function(html){
					$('.detail_content2').empty();
					$('.detail_content2').append(html).hide();
					$('.detail_content2').slideDown();
					$('.btn').each(function(){$(this).removeAttr('disabled');});
					eval(document.getElementById("crud_script").innerHTML);
				}
			});
			function removeCrud( itemid ) {
				var element = document.getElementById(itemid); // will return element
				elemen.parentNode.removeChild(element); // will remove the element from DOM
			}
			removeCrud('crud_script');
		}
	  })(marker, i));
	}
	
}
</script>