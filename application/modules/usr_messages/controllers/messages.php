<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends User {
		
	public function view($zone,$type){	
		if ($this->s_login && !empty($type))
		{
			$data = array();
			$admin_id = $this->user_id;
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['user'] 		= $this->user;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['css'] = array(
				'css/plugins/iCheck/custom.css',
				'js/plugins/fileinput/fileinput.min.css',
				'css/plugins/chosen/chosen.css',
				'css/plugins/summernote/summernote-bs3.css',
				'css/plugins/summernote/summernote.css'
			);
			$data['js'] = array(
					'js/plugins/iCheck/icheck.min.js',
					'js/plugins/datapicker/bootstrap-datepicker.js',
					'js/plugins/summernote/summernote.min.js',
					'js/plugins/chosen/chosen.jquery.js',
					'js/plugins/fileinput/fileinput.min.js',
					'js/ajaxfileupload.js',
					'js/modul/new.js'
				);
			$data['custom_js'] = '<script>
				$(document).ready(function(){
					$("[data-url2='. "'" .'category'. "'" .']")[0].click();
					$("[data-url2='. "'" .'inbox'. "'" .']")[0].click();
				});
				$(".menu_type").on("click", function(e) {
					$(".menu_type").removeClass("active");
					$(this).addClass("active");
				});
				</script>
				';		
			$data['parent'] = 'messages';
			//End set parameter view
			
			if($type == 'user')
			{
				$data['child'] = 'messages';
				$data['ch'] = array(
					'title'=> $this->module_name, 
					'small'=>'Manage messages from your websites '
				);
				$data['breadcrumb'] = array(
					array(
						'url' => $this->zone.'/user/dashboard', 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => $this->zone.'/user/'.$this->module_link, 
						'bcrumb' => $this->module_name, 
						'active' => true
					)
				);
				$data['type_id'] = $this->user_type;
				$cek = $this->dbfun_model->get_data_bys('type_id,rt_id,rw_id','ccs_id = '.$this->zone_id.' and type_id = '.$this->user_type.' and user_id = '.$this->user_id.'','usr');
				if($cek->rw_id != 0){
					$ck = array();
					//$ck[] = (object) array('type_id' => '0', 'name' => 'Admin');
					$cek2 = $this->dbfun_model->get_all_data_bys('distinct(type_id)','ccs_id = '.$this->zone_id.' and rt_id = '.$cek->rt_id.'','usr');
					foreach($cek2 as $cc){
						$ck[] = $this->dbfun_model->get_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and type_id = '.$cc->type_id.'','umr');
					}
					$data['type'] = $ck;
				}elseif($cek->rt_id != 0){
					$ck = array();
					$ck[] = (object) array('type_id' => '0', 'name' => 'Admin');
					$cek2 = $this->dbfun_model->get_all_data_bys('distinct(type_id)','ccs_id = '.$this->zone_id.' and rt_id = '.$cek->rt_id.'','usr');
					foreach($cek2 as $cc){
						$ck[] = $this->dbfun_model->get_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and type_id = '.$cc->type_id.'','umr');
					}
					$data['type'] = $ck;
				}elseif($cek->type_id != 0){
					$ck = array();
					$ck[] = (object) array('type_id' => '0', 'name' => 'Admin');
					$cek2 = $this->dbfun_model->get_all_data_bys('distinct(type_id)','ccs_id = '.$this->zone_id.' and rt_id = '.$cek->rt_id.'','usr');
					foreach($cek2 as $cc){
						$ck[] = $this->dbfun_model->get_data_bys('type_id,name','ccs_id = '.$this->zone_id.' and type_id = '.$cc->type_id.'','umr');
					}
					$data['type'] = $ck;
				}else{
					$data['type'] = '';
				}
			}
			
			$data['content'] = 'messages.php';
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}
	
	public function detail_ajax($zone,$type,$part){	
		if ($this->s_login && !empty($type))
		{
			$data = array();
			$data['child'] = 'messages';
			if($type == 'messages'){
				$this->form_validation->set_rules('id','id page', 'trim|required|numeric|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');

				if($this->form_validation->run() == TRUE ){
					$admin_id = $this->user_id;
					$id = $this->input->post('id');
					$param = $this->input->post('param');
					$lang = $this->input->post('language_id');
					if($id == 0){
						$table = 'adm';
						$column = 'admin_id';
						$data['type_id'] = '0';
					}else{
						$table = 'usr';
						$column = 'user_id';
						$user = "ccs_usr.type_id = ".$this->user_type."";
						$data['type_id'] = $this->user_type;
					}
					if(in_array($part, array('inbox','sendmail','important','drafts','trash','junk'))){
						if($part == 'inbox'){
							$sender = 'tr_id = '.$id.' and receiver = '.$admin_id.'';
							$read_status = ''.$sender.' and ccs_msg.status = 1 and read_status >= 2 and read_status <= 3';
							$type_user = 'and ts_id = '.$param.'';
						}elseif($part == 'sendmail'){
							$sender = 'ts_id = '.$id.' and sender = '.$admin_id.'';
							$read_status = ''.$sender.' and ccs_msg.status = 1';
							$type_user = 'and tr_id = '.$param.'';
						}elseif($part == 'important'){
							$sender = 'tr_id = '.$id.' and receiver = '.$admin_id.'';
							$read_status = ''.$sender.' and ccs_msg.status = 1 and ccs_msg.read_status = 6';
							$type_user = 'and ts_id = '.$param.'';
						}elseif($part == 'drafts'){
							$sender ='ts_id = '.$id.' and sender = '.$admin_id.'';
							$read_status = ''.$sender.' and ccs_msg.status = 0 and ccs_msg.read_status = 0';
							$type_user = 'and tr_id = '.$param.'';
						}elseif($part == 'trash'){
							$sender = 'tr_id = '.$id.' and receiver = '.$admin_id.'';
							$read_status = ''.$sender.' and ccs_msg.status = 1 and ccs_msg.read_status = 4';
							$type_user = 'and ts_id = '.$param.'';
						}elseif($part == 'junk'){
							$sender = 'tr_id = '.$id.' and receiver = '.$admin_id.'';
							$read_status = ''.$sender.' and ccs_msg.status = 1 and ccs_msg.read_status = 5';
							$type_user = 'and ts_id = '.$param.'';
						}
						$ds2 = array();
						if($param == 'all'){
							$msg = $this->dbfun_model->get_all_data_bys('message_id, ts_id,tr_id, sender,receiver, subject, status,read_status,datecreated','ccs_id = '.$this->zone_id.' and '.$read_status.'','msg');
						}else{
							$msg = $this->dbfun_model->get_all_data_bys('message_id, ts_id,tr_id, sender,receiver, subject, status,read_status,datecreated','ccs_id = '.$this->zone_id.' and '.$read_status.' '.$type_user.'','msg');
						}
						$i= 0;
						foreach($msg as $m){
							if(in_array($part, array('sendmail','drafts'))){
								$object = $m->receiver;
								if($m->tr_id == 0){
									$db = 'adm';
									$db_column = 'admin_id';
								}else{
									$db = 'usr';
									$db_column = 'user_id';
								}
							}else{
								$object = $m->sender;
								if($m->ts_id == 0){
									$db = 'adm';
									$db_column = 'admin_id';
								}else{
									$db = 'usr';
									$db_column = 'user_id';
								}
							}
							$ds = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and '.$db_column.' = '.$object.'',$db);
							$ds2[] = (object) array_merge((array) $m, array('name' => $ds->name));
							if(($m->status == 1) && ($m->read_status == 3)){
								$i++;
							}
						};
						$data['messages'] = $ds2;
						$data['m_total'] = $i;
						$data['title'] = $part;
						
						echo '<script>
							$(".i-checks").iCheck({
								checkboxClass: "icheckbox_square-green",
								radioClass: "iradio_square-green",
							});</script>';
						$view = $this->load->view('messages_list', $data, TRUE);
					}elseif(in_array($part, array('detail','compose','reply','forward'))){
						$data['part'] = $part;
						$data['title'] = $param;
						if($part != 'detail'){
							echo '<link href="'.base_url().'assets/admin/css/plugins/chosen/chosen.css" rel="stylesheet">';
							echo '<script src="'.base_url().'assets/admin/js/plugins/chosen/chosen.jquery.js"></script>
							<script>
							var config = {
								".chosen-select"           : {},
								".chosen-select-deselect"  : {allow_single_deselect:true},
								".chosen-select-no-single" : {disable_search_threshold:10},
								".chosen-select-no-results": {no_results_text:"Oops, nothing found!"},
								".chosen-select-width"     : {width:"95%"}
							}
							for (var selector in config) {
								$(selector).chosen(config[selector]);
							}
							</script>
							';
							if($this->user_type != 0){//cek user to receive the message
								$this->load->model('Messages_model');
								if($this->simo != 0){
									$receivers = $this->Messages_model->get_all_user_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 2
								}else{
									$receivers = $this->Messages_model->get_all_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 1
								}
								
								$data['receiver'] = $receivers;
							}else{//berarti ini admin
								$data['receiver'] = $this->dbfun_model->get_all_data_bys(''.$column.' as id, name',''.$column.' != '.$admin_id.' and ccs_id = '.$this->zone_id.' ',$table);
							}
						}
						if($part == 'compose'){
							$view = $this->load->view('messages_compose', $data, TRUE);
						}else{
							$data['messages'] = $this->dbfun_model->get_data_bys('msg.message_id, msg.parent_id,msg.ts_id,msg.tr_id, msg.status, msg.subject, msg.message, msg.datecreated, msg.read_status, msg.sender as id, msg.receiver as idr','msg.message_id = '.$id.' and ccs_msg.ccs_id = '.$this->zone_id.' ','ccs_msg');
							if(($data['messages']->tr_id != 0) && ($data['messages']->ts_id != 0)){ //penerima bukan admin dan pengirim bukan admin
								$data['r_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->tr_id.'','umr')->name);
								$data['s_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->ts_id.'','umr')->name);
								$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->id.' and type_id = '.$data['messages']->ts_id.'','ccs_usr')->name;
							}elseif(($data['messages']->tr_id == 0) && ($data['messages']->ts_id != 0)){ //penerima admin dan pengirim bukan admin
								$data['r_type'] = 'Admin';
								$data['s_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->ts_id.'','umr')->name);
								$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->id.' and type_id = '.$data['messages']->ts_id.'','ccs_usr')->name;
							}elseif(($data['messages']->tr_id != 0) && ($data['messages']->ts_id == 0)){ //penerima bukan admin dan pengirim admin
								$data['r_type'] = ucwords($this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$data['messages']->tr_id.'','umr')->name);
								$data['s_type'] = 'Admin';
								$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->id.'','ccs_adm')->name;
							}else{ //penerima admin dan pengirim admin
								$data['r_type'] = 'Admin';
								$data['s_type'] = 'Admin';
								$data['sender'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->id.'','ccs_adm')->name;
							}
							if($part == 'detail'){
								if(($data['messages']->read_status == 3) && ($data['messages']->id != $admin_id)){
									$this->dbfun_model->update_table(array('message_id' => $id), array('read_status' => 2), 'ccs_msg');
								}
								if($param == 'drafts'){
									if(isset($this->simo)){//cek user to receive the message
										$this->load->model('Messages_model');
										if($this->simo != 0){
											$receivers = $this->Messages_model->get_all_user_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 2
										}else{
											$receivers = $this->Messages_model->get_all_receiver($this->zone_id,$this->user_id,$this->segretis); //layer 1
										}
										
										$data['receiver'] = $receivers;
									}else{//berarti ini admin
										$this->load->model('Messages_model');
										$data['receiver'] = $this->Messages_model->get_all_user_receiver_from_admin($this->zone_id,$this->admin_id);
									}
									echo '
									<script>
									$(document).ready(function(){
										$(".summernote").summernote();
										$(".summernote").code("'.str_replace('"',"'", trim($data['messages']->message)).'");
										$("#message_id").val("'.$data['messages']->message_id.'");
										$("#subject").val("'.$data['messages']->subject.'");
										$("#receiver").chosen().val('.$data['messages']->idr.');
										$("#receiver").trigger("chosen:updated");
									});
									</script>';
									$view = $this->load->view('messages_update', $data, TRUE);
									
								}else{
									if(($data['messages']->tr_id != 0) && ($data['messages']->ts_id != 0)){ //penerima bukan admin dan pengirim bukan admin
										$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->idr.' and type_id = '.$data['messages']->tr_id.'','ccs_usr')->name;
									}elseif(($data['messages']->tr_id == 0) && ($data['messages']->ts_id != 0)){ //penerima admin dan pengirim bukan admin
										$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->idr.'','ccs_adm')->name;
									}elseif(($data['messages']->tr_id != 0) && ($data['messages']->ts_id == 0)){ //penerima bukan admin dan pengirim admin
										$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and user_id = '.$data['messages']->idr.' and type_id = '.$data['messages']->tr_id.'','ccs_usr')->name;
									}else{ //penerima admin dan pengirim admin
										$data['receiver'] = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and admin_id = '.$data['messages']->idr.'','ccs_adm')->name;
									}
									$view = $this->load->view('messages_detail', $data, TRUE);
								}
							}elseif($part == 'reply'){
								echo '
								<script>
								$(document).ready(function(){
									$(".summernote").summernote();
									$(".summernote").code("'.str_replace('"',"'", '<br><div class="icon-reply reply"><hr><small>'.date('Y-m-d', strtotime($data['messages']->datecreated)).'</small><br><p>'.$data['sender'].':</p> <br><i style="color: #aaa;">'.trim($data['messages']->message)).'</i></div>'.'");
									$("#receiver").chosen().val('.$data['messages']->id.');
									$("#receiver").trigger("chosen:updated");
								});
								</script>';
								$view = $this->load->view('messages_reply', $data, TRUE);
							}elseif($part == 'forward'){
								echo '<script>
								$(document).ready(function(){
									$(".summernote").summernote();
									$(".summernote").code("'.str_replace('"',"'", '<br><div class="forward"><hr>---Forwarded Message---<br><small>'.date('Y-m-d', strtotime($data['messages']->datecreated)).'</small><br><p>'.$data['sender'].':</p> <br><i style="color: #aaa;">'.trim($data['messages']->message)).'</i></div>'.'");
									$("#receiver").val();
								});</script>';
								$view = $this->load->view('messages_reply', $data, TRUE);
							}
						}
					}elseif($part == 'category'){
						$data['zone'] = $this->zone;
						$data['param'] = $param;
						if(isset($param)){
							$lang = $this->input->post('language_id');
							if($param == 'all')
							{
								$data['m_inbox'] = $this->dbfun_model->count_result('receiver = '.$admin_id.' and ccs_msg.ccs_id = '.$this->zone_id.' and ccs_msg.status = 1 and ccs_msg.read_status >= 2 and ccs_msg.read_status <= 3 and ccs_msg.read_status = 3','msg');
								$data['m_drafts'] = $this->dbfun_model->count_result('sender = '.$admin_id.' and ccs_msg.ccs_id = '.$this->zone_id.'  and ccs_msg.status = 0','msg');
								$data['m_junk']= $this->dbfun_model->count_result('receiver = '.$admin_id.' and ccs_msg.ccs_id = '.$this->zone_id.' and ccs_msg.status = 1 and ccs_msg.read_status = 5 and ccs_msg.read_status = 3','msg');
							}
							else
							{
								$data['m_inbox'] = $this->dbfun_model->count_result('receiver = '.$admin_id.' and ts_id = '.$param.' and ccs_msg.ccs_id = '.$this->zone_id.' and ccs_msg.status = 1 and ccs_msg.read_status >= 2 and ccs_msg.read_status <= 3 and ccs_msg.read_status = 3','msg');
								$data['m_drafts'] = $this->dbfun_model->count_result('sender = '.$admin_id.' and tr_id = '.$param.' and ccs_msg.ccs_id = '.$this->zone_id.'  and ccs_msg.status = 0','msg');
								$data['m_junk']= $this->dbfun_model->count_result('receiver = '.$admin_id.' and ts_id = '.$param.' and ccs_msg.ccs_id = '.$this->zone_id.' and ccs_msg.status = 1 and ccs_msg.read_status = 5 and ccs_msg.read_status = 3','msg');
							}
							
							echo '
								<script>
									$("#category > li").on("click", function(e) {
										$("#category > li > a").removeClass("active");
										$(this).children("a").addClass("active");
									});
								</script>
							';
							$view = $this->load->view('messages_sidebar', $data, TRUE);
						}
					}
					$view = $view;
					echo $view;
				}
			}
		}else{
			redirect($this->login);
		}
	}
			
	public function create($zone,$type)
    {
    	if ($this->s_login && !empty($type))
    	{
			$data = array();
			$status = '';
			$message = '';
			if($type == 'messages'){
				$id = $this->input->post('message_id');
				$lang = 2;
				$admin_id = $this->user_id;
				$type_id = $this->user_type;
					if($id) //update
					{
						//if still draft or save as draft
						if ($this->form_validation->run('ccs_msg') === TRUE)
						{
							$status = 'success';
							$message = 'No data updated';
							$update = $this->update_data_bys('*', array('message_id' => $id), 'msg','','','','');
							$param = $this->input->post('param');
							if($param == 'draft'){
								$update['ts_id'] = $type_id;
								$update['ccs_id'] = $this->zone_id;
								$update['status'] = 0;
								$status = 'success';
								$message = 'Draft updated';
							}elseif($param == 'sent'){
								$update['ts_id'] = $type_id;
								$update['ccs_id'] = $this->zone_id;
								$update['status'] = 1;
								$update['read_status'] = 3;
								$update['datecreated'] = date('Y-m-d H:i:s');
								$status = 'success';
								$message = 'Message sent';
							}
							if($update) 
							{
								if($lang)
								{
									$this->dbfun_model->update_table(array('message_id' => $id,'language_id' => $lang), $update, 'msg');
								}
								else
								{
									$this->dbfun_model->update_table(array('message_id' => $id), $update, 'msg');
								}
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else  // create
					{
						if ($this->form_validation->run('ccs_msg') === TRUE)
						{
							if($this->input->post('param') == 'draft'){
								$custom_primary = array(
									'ts_id' => $type_id,
									'ccs_id' => $this->zone_id,
									'language_id' => 2,
									'sender' => $admin_id,
									'createdby' => $admin_id,
									'message' => str_replace('icon-reply reply',' ',$this->input->post('message')),
									'read_status' => 0,
									'status' => 0
								);	
							}else{
								$custom_primary = array(
									'ts_id' => $type_id,
									'ccs_id' => $this->zone_id,
									'language_id' => 2,
									'sender' => $admin_id,
									'createdby' => $admin_id,
									'message' => str_replace('icon-reply reply',' ',$this->input->post('message')),
									'read_status' => 3,
									'status' => 1
								);	
							}
							
							$this->new_data('ccs_msg',$custom_primary, '','', '','');
							$status = 'success';
							$message = 'Message sent';
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					
				echo json_encode(array('status' => $status, 'm' => $message));
			}
		}else{
			redirect($this->login);
		}
    }
	
	public function status($zone,$type)
	{
		if ($this->s_login && !empty($type))
    	{
			$data = array();
    		$status = '';
    		$message = '';
			
			$this->form_validation->set_rules('id', 'page', 'trim|required|xss_clean');
			$this->form_validation->set_rules('lang', 'language', 'trim|numeric|xss_clean');
			$id = $this->input->post('id');
			$action = $this->input->post('status');
			$lang = $this->input->post('lang');
			if ($this->form_validation->run() === TRUE)
			{
				if($type == 'messages')
				{
					if(in_array($action, array('read','unread','trash','junk','important')))
					{
						if($action == 'read'){
							$update = array
							(
								'read_status' => 2
							);
						}elseif($action == 'unread'){
							$update = array
							(
								'read_status' => 3
							);
						}elseif($action == 'trash'){
							$update = array
							(
								'read_status' => 4
							);
						}elseif($action == 'junk'){
							$update = array
							(
								'read_status' => 5
							);
						}elseif($action == 'important'){
							$update = array
							(
								'read_status' => 6
							);
						}
					}
					if(strpos($id,',') !== false){
						$ids = explode(',',$id);
						foreach($ids as $ids){
							if($lang)
							{
								$this->dbfun_model->update_table(array('message_id' => $ids,'language_id' => $lang, 'ccs_id' => $this->zone_id), $update, 'msg');
							}
							else
							{
								$this->dbfun_model->update_table(array('message_id' => $ids, 'ccs_id' => $this->zone_id), $update, 'msg');
							}
						}
					}else{
						if($lang)
						{
							$this->dbfun_model->update_table(array('message_id' => $id,'language_id' => $lang, 'ccs_id' => $this->zone_id), $update, 'msg');
						}
						else
						{
							$this->dbfun_model->update_table(array('message_id' => $id, 'ccs_id' => $this->zone_id), $update, 'msg');
						}
					}
					$status = 'success';
					$message = 'Data updated successfully';
				}
			}else{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}
	}

}