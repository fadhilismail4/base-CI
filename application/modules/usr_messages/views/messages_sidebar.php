<div class="ibox float-e-margins">
	<div class="ibox-content">
		<div class="file-manager">
			<a id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="compose" data-lang="" class="detail btn btn-block btn-primary compose-mail">Compose Messages</a>
			<div class="space-25"></div>
			<ul id="category" class="folder-list m-b-md" style="padding: 0">
				<li><a id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="inbox" data-param="<?php echo $param;?>" class="detail file-control active"> <i class="fa fa-inbox"></i> Inbox <?php if($m_inbox != 0){;?><span class="label label-primary pull-right"><?php echo $m_inbox ;?></span><?php ;}?> </a></li>
				<li><a id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="sendmail" data-param="<?php echo $param;?>" class="detail file-control"> <i class="fa fa-envelope-o"></i> Send Mail<span class="label label-success pull-right"></a></a></li>
				<li><a id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="important" data-param="<?php echo $param;?>" class="detail file-control"> <i class="fa fa-certificate"></i> Important</a></li>
				<li><a id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="drafts" data-param="<?php echo $param;?>" class="detail file-control"> <i class="fa fa-file-text-o"></i> Drafts <?php if($m_drafts != 0){;?><span class="label label-warning pull-right"><?php echo $m_drafts ;?></span><?php ;}?></a></li>
				<li><a id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="trash" data-param="<?php echo $param;?>" class="detail file-control"> <i class="fa fa-trash-o"></i> Trash </a></li>
				<li><a id="<?php echo $type_id;?>" data-url="<?php echo $child;?>" data-url2="junk" data-param="<?php echo $param;?>" class="detail file-control"> <i class="fa fa-exclamation-triangle"></i> Junk <?php if($m_junk != 0){;?><span class="label label-danger pull-right"><?php echo $m_junk ;?></span><?php ;}?></a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
	</div>
</div>