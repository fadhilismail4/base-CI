<style>
.note-editor{
	background-color: #fff;
}
</style>
<div class="mail-box-header">
	<div class="pull-right tooltip-demo">
		<!--<a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->
		<a href="" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
	</div>
	<h2>
		Compose messages
	</h2>
</div>
<div class="mail-box">


	<div class="mail-body">

		<form class="form-horizontal" method="get">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<div class="form-group"><label class="col-sm-2 control-label">To:</label>
				<input id="tr_id" name="inputan" type="text" class="hide form-control" value="">
				<div class="col-sm-10">
					<div class="input-group">
						<select id="receiver" name="inputan" data-placeholder="Select" class="chosen-select" style="width:350px;" tabindex="2">
						<option value="">Select</option>
						<?php foreach($receiver as $r){?>
							<option value="<?php echo $r->id;?>" data-type="<?php echo $r->type_id;?>"><?php echo $r->name;?> (<?php echo $r->role;?>)</option>
						<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group"><label class="col-sm-2 control-label">Subject:</label>

				<div class="col-sm-10"><input id="subject" name="inputan" type="text" class="form-control" value=""></div>
			</div>
		</form>

	</div>

	<div class="mail-text h-200" style="padding: 0;display: inline-block; width: 100%;">

		<div id="message" name="inputan_summer" class="summernote">
			
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="mail-body text-right tooltip-demo">
		<a id="messages" data-param="<?php echo $type_id;?>" class="create btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-reply"></i> Send</a>
		<a href="" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
		<!--<a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->
	</div>
    
	<div class="clearfix"></div>
</div>
<!-- SUMMERNOTE -->
<script src="<?php echo base_url('assets');?>/admin/js/plugins/summernote/summernote.min.js"></script>
<script>
$('#receiver').change(function() {
	var type = $(this).find(':selected').data('type');
	$('#tr_id').val(type);
});
	$(document).ready(function(){
		$('.summernote').summernote();
	});
	var edit = function() {
		$('.click2edit').summernote({focus: true});
	};
	var save = function() {
		var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
		$('.click2edit').destroy();
	};

</script>