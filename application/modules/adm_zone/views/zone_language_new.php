<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone->name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="language" data-lang="2" data-param="module" class="detail2">
					Language
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="new_language" data-lang="2" data-param="module" class="detail2">
					Add Language
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="language" data-lang="2" data-param="module" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="row"  style="margin-bottom: 20px !important">
			<div class="pull-right">
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
				<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
			</div>
		</div>
		<div class="row"  style="font-size: 14px">
			<?php if($zone->active_lang == 1){ ;?>
				<div class="col-md-3">
					Language Module
				</div>
				<div class="col-md-9">
					<strong style="padding-right: 20px">Active</strong>
				</div>
				<div class="col-md-3" style="margin: 10px 0">
					Remains Language
				</div>
				<div class="col-md-9" style="margin: 10px 0">
					<strong style="padding-right: 20px"><?php echo ($zone->total_lang - count($language)) ?> Language</strong>
				</div>
				
				<div class="col-md-3" style="margin-bottom: 10px">
					Language Active
				</div>
				<div class="col-md-9" style="margin-bottom: 10px">
					<?php foreach($language as $l){ ;?>
					<button  class="btn btn-white btn-xs" type="button"><?php echo $l->code?></button>
					<?php }; unset($l);?>
				</div>
			<?php }else{ ;?>
				<div class="col-md-3">
					Language Module
				</div>
				<div class="col-md-9">
					<strong style="padding-right: 20px">Inactive</strong>
				</div>
				<div class="col-md-3" style="margin: 10px 0">
					Total Language
				</div>
				<div class="col-md-9" style="margin: 10px 0">
					<strong style="padding-right: 20px">1 Language</strong>
				</div>
				<div class="col-md-3" style="margin-bottom: 10px">
					Language Active
				</div>
				<div class="col-md-9" style="margin-bottom: 10px">
					<button  class="btn btn-white btn-xs" type="button">ID</button>
				</div>
			<?php } ;?>
		</div>
		<form class="form-horizontal">
			<div class="hr-line-dashed"></div>
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>			
			<input id="ccs_id" name="inputan" type="text" class="form-control hide" value="<?php echo $zone->ccs_id ?>"></input>
			<input id="action" name="inputan" type="text" class="form-control hide" value="add"></input>
			<input id="lang_id" name="inputan" type="text" class="form-control hide" value=""></input>
			<div id="language" class="form-group col-sm-12">	
				<?php 
				foreach($language as $lg){ $cek[] = $lg->language_id;};
				foreach($lng as $lng){
					if(!in_array($lng->language_id,$cek)){;?>
					<div class="col-sm-3" style="padding: 10px 0">
						<div class="col-sm-4">
							<input id="<?php echo $lng->language_id ;?>" type="checkbox" class="i-checks">
						</div>
						<div class="col-sm-8" style="">
							<?php echo $lng->name?>
						</div>
					</div>
				<?php };} ;?>
			</div>
		</form>
		<div class="form-group col-sm-12">
			<div class="hr-line-dashed"></div>			
			<div class="col-md-12">
				<button id="create_lang" data-param="zone" class="create btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Add</button>
				<button id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="language" data-lang="2" data-param="module" class="detail2 btn btn-md btn-warning pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$("#create_lang").hide();
	
	$("#language input").on("ifToggled", function (event) {
		$("#create_lang").slideDown();
		brands();
	});
	
	function brands() {
		var brands = [];
		$("#language input").each(function (index, value) {
			if ($(this).is(":checked")) {
				brands.push($(this).attr("id"));
			}
		});
		console.log(brands);
		$("#lang_id").val(brands);
		if(!brands.length){
			$("#create_lang").slideUp();
		}
	}
});
</script>