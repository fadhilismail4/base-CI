<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Category</h5>
	</div>
	<div class="ibox-content">
		<div class="row" style="margin: 0px;">
		</div>	
		<div class="file-manager">	
			<div class="dd" id="nestable2">
				<ol class="dd-list">
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="all" data-url="<?php echo $part;?>" data-url2="list" data-lang="2" data-param="zone" class="detail2">
							<span class="label label-info"></span> <strong>View All</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="1" data-url="<?php echo $part;?>" data-url2="list" data-lang="2" data-param="zone" class="detail2">
							<span class="label label-info"></span> <strong>Company Profile</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="2" data-url="<?php echo $part;?>" data-url2="list" data-lang="2" data-param="zone" class="detail2">
							<span class="label label-info"></span> <strong>Community</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="3" data-url="<?php echo $part;?>" data-url2="list" data-lang="2" data-param="zone" class="detail2">
							<span class="label label-info"></span> <strong>E-commerce</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="4" data-url="<?php echo $part;?>" data-url2="list" data-lang="2" data-param="zone" class="detail2">
							<span class="label label-info"></span> <strong>Siakad</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
					<li class="dd-item">
						<div class="dd-handle dd-nodrag" style="padding-left:30px">		
							<a id="5" data-url="<?php echo $part;?>" data-url2="list" data-lang="2" data-param="zone" class="detail2">
							<span class="label label-info"></span> <strong>Custom</strong>
							</a>
							<p class="pull-right"></p> 
						</div>
					</li>
				</ol>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>	