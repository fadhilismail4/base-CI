<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7">
			<li class="active">
				<a id="all" data-url="zone" data-url2="list" data-lang="2" data-param="zone" class="detail2">
					<h5>All Zone (<?php echo count($zones)?>) </h5>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="1" data-url="zone" data-url2="detail" data-lang="2" data-param="basic_module" class="detail2">
				<button  class="btn btn-sm btn-info ">View Module</button>
			</a>
			<a id="1" data-url="zone" data-url2="new" data-lang="2" data-param="zone" class="detail2 btn btn-primary btn-sm ">Add Zone</a>
			<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Title </th>
					<th>Contract Type </th>
					<th>Createdby</th>
					<th>Date Created </th>
					<th>Category </th>
					<th>Status</th>
					<th>Activation</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($zones)){ ?>
				<?php $i = 1; foreach($zones as $n){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td><a id="<?php echo $n->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3"><?php echo $n->name ?></a></td>
					<td><?php if($n->zone_id == 0){ echo "Forever" ;}elseif($n->zone_id == 1){ echo "Monthly" ;}else{ echo "Yearly" ;} ;?></td>
					<td><?php echo $n->nama ?></td>
					<td><?php if(strtotime($n->datecreated) != strtotime('0000-00-00 00:00:00')){echo date("l, d M Y", strtotime($n->datecreated));}else{ echo '-';};?></td>
					<td><?php if($n->type_id == 1){ echo "Company Profile" ;}elseif($n->type_id == 2){ echo "Community" ;}elseif($n->type_id == 3){ echo "E-commerce" ;}elseif($n->type_id == 4){ echo "Siakad" ;}elseif($n->type_id == 5){ echo "Custom" ;} ;?></td>
					<td><?php if($n->status == 1){ echo "Active" ;}elseif($n->status == 0){ echo "Inactive" ;}else{ echo "Hold " ;} ;?></td>
					<td>
						<?php foreach($status as $s ){ ;?>
							<?php if($n->status != $s->value ){ ;?>
								<button id="<?php echo $n->ccs_id ?>" data-param="<?php echo $s->status_id ;?>" data-url="zone" data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $n->name ;?>" class="modal_sts btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php ;} ;?>
					</td>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
	</div>
</div>