<div class="ibox">
	<div class="ibox-title">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $detail->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone ;?>
				</a>
			</li>
			
			<li class="">
				<a id="<?php echo $detail->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="module" class="detail2">
					Module
				</a>
			</li>
			<li class="">
				<a id="<?php echo $detail->ccs_id ?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $detail->module_id ?>" class="detail2">
					<?php echo $detail->name ?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $detail->ccs_id ?>"  data-url="zone" data-url2="edit_module" data-lang="2" data-param="<?php echo $detail->module_id?>" class="detail2">
					Edit
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $detail->ccs_id ?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $detail->module_id ?>" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content">
		<div class=""  style="margin-bottom: 20px !important">
			<div class="pull-right">
				<button id="<?php echo $detail->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
				<button id="<?php echo $detail->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
				<button id="<?php echo $detail->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
				<button  id="<?php echo $detail->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
			</div>
		</div>
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-4"><input id="name" name="inputan" type="text" class="form-control" value="<?php echo $detail->name ?>"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Icon</label>
				<div class="col-sm-4"><input id="icon" name="inputan" type="text" class="form-control" value="<?php echo $detail->icon ?>"></input></div>
				<label class="col-sm-2 control-label">Link</label>
				<div class="col-sm-4"><input id="link" name="inputan" type="text" class="form-control" value="<?php echo $detail->link ?>"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control" value="<?php echo $detail->description?>"><?php echo $detail->description?></textarea></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Category</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="parent_id">
						<?php if($detail->parent_id != 0){?>							
						<option value="<?php echo $parent->parent_id ?>"><?php echo $parent->pname ?></option>
						<?php ;} ;?>
						<option value="0">Set as Parent Category</option>
						<?php foreach($module as $m){  if($detail->module_id != $m->module_id){ ;?>
							<option value="<?php echo $m->module_id ;?>"><?php echo $m->name ;?></option>						
						<?php ;} ;} ;?>
					</select>
				</div>
				
				<label class="col-sm-2 control-label">Module View</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="type_view">
						<?php if($detail->type_view == 0){ ;?>							
							<option value="0">Basic View</option>
						<?php }elseif($detail->type_view == 1){ ;?>								
							<option value="1">Category View</option>
						<?php }elseif($detail->type_view == 2){ ;?>
							<option value="2">Gallery View</option>
						<?php }elseif($detail->type_view == 3){ ;?>
							<option value="3">User View</option>
						<?php }elseif($detail->type_view == 4){ ;?>
							<option value="4">User View</option>
						<?php }elseif($detail->type_view == 5){ ;?>
							<option value="5">Commerce View</option>
						<?php }else{ ;?>							
							<option value="6">Custom View</option>
						<?php } ;?>
						<option value="">Select Module View</option>
						<option value="0">Basic View</option>
						<option value="1">Category View</option>
						<option value="2">Gallery View</option>
						<option value="3">User View</option>
						<option value="4">Message View</option>
						<option value="5">Commerce View</option>
						<option value="6">Custom View</option>
					</select>
				</div>
					
				<label class="col-sm-2 control-label">Module Type</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="type_id">
						<?php if($detail->type_id == 0){ ;?>							
							<option value="0">Basic Module</option>
						<?php }elseif($detail->type_id == 1){ ;?>								
							<option value="1">Category Module</option>
						<?php }elseif($detail->type_id == 2){ ;?>								
							<option value="2">Zone Module</option>
						<?php }elseif($detail->type_id == 3){ ;?>								
							<option value="3">Timely Module</option>
						<?php }elseif($detail->type_id == 4){ ;?>
							<option value="4">Gallery Module</option>
						<?php }elseif($detail->type_id == 5){ ;?>
							<option value="5">Commerce Module</option>
						<?php }elseif($detail->type_id == 6){ ;?>
							<option value="6">User Module</option>
						<?php }elseif($detail->type_id == 7){ ;?>
							<option value="7">Messages Module</option>
						<?php }elseif($detail->type_id == 8){ ;?>
							<option value="8">Menu Module</option>
						<?php }else{ ;?>							
							<option value="9">Custom Module</option>
						<?php } ;?>
						
						<option value="">Select Module Type</option>
						<option value="0">Basic Module</option>
						<option value="1">Category Module</option>
						<option value="2">Zone Module</option>
						<option value="3">Timely Module</option>
						<option value="4">Gallery Module</option>
						<option value="5">Commerce Module</option>
						<option value="6">User Module</option>
						<option value="7">Messages Module</option>
						<option value="8">Menu Module</option>
						<option value="9">Custom Module</option>
					</select>
				</div>
				
				<label class="col-sm-2 control-label">Frontend View</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="ftype">
					
						<?php if($detail->ftype == 0){ ;?>
							<option value="0">No</option>
						<?php }else{ ;?>							
							<option value="1">Yes</option>
						<?php } ;?>
						<option value="">Select to View on Frontend</option>
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>
				</div>
				<input id="url_id" name="inputan" type="text" class="hide" value="<?php echo $detail->module_id ?>"></input>
				<input id="ccs_key" name="inputan" type="text" class="hide" value="12"></input>
				<input id="ccs_id" name="inputan" type="text" class="hide" value="<?php echo $detail->ccs_id ?>"></input>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group row">			
				<div class="col-md-12">
					<button id="update_module" class="create btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Update</button>
					<button id="<?php echo $detail->ccs_id ?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $detail->module_id?>" class="detail2 btn btn-md btn-warning pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>