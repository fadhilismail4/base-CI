<style>
ul.edit{
	list-style-type: none;
    padding: 0;
}
ul.edit > li{
	margin: 7.6px 0;
}
.dd-handle{
	width: 90%;
    float: left;
    background-color: transparent;
    border: 1px solid #1C84C6;
    border-radius: 0;
}
.dd-handle:hover{
	background-color: #fff;
	cursor: all-scroll;
}
.dd-item > button{
	background-color: transparent;
    border: 1px solid #1C84C6;
	border-right: none;
    border-radius: 0;
    padding: 15px 0;
}
.footer_list .container {
	width: 100%;
	padding-top:15px;
	padding-bottom:15px;
}
.c-socials{
    padding: inherit;
}
.c-socials > li {
    display: inline-block;
	float: left;
    padding-bottom: 10px;
}
.c-works {
	padding: 0;
    overflow: auto;
    margin: 0;
}
.c-works > li {
	list-style: none;
    display: inline-block;
    padding: 0;
    margin: 0 0 5px 0;
}
.c-works > li img{
	margin: 0;
    width: 74px;
}
.img-responsive{
	display: block;
    max-width: 100%;
    height: auto;
}
.c-blog > .c-post:first-child{
	padding-top: 0;
}
.c-blog > .c-post{
	border-bottom: 1px solid #fff;
	padding: 10px 0;
}
.c-blog > .c-post > .c-post-img {
    width: 40px;
    height: 40px;
    position: relative;
    float: left;
    margin-top: 10px;
}
.c-blog > .c-post > .c-post-img img {
    width: 100%;
    border-radius: 40px;
}
.c-blog > .c-post > .c-post-content {
    padding-left: 50px;
}
#data > .footer_list td:hover{
	background-color: #f8fafb !important;
}
.btn.btn-md{
	padding: 6px 12px;
}
.btn > i{
	margin-right: 0px;
}
.close_btn{
	padding: 2px 8px;
    margin: 0;
    border: 2px solid #DDD;
    border-radius: 50%;
    color: #9D0B0B;
    font-weight: bold;
}
.close_btn:hover{
	color:#fff;
    background-color: #9D0B0B;
	border: none;
	padding: 4px 10px;
	cursor:pointer;
}
.file-preview-frame{
	height:auto;
}
.file-preview-image{
	width:100%;
	height:auto !important;
}
.text_info{
	margin-top: 7px;
}
</style>
<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone->name;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="module" class="detail2">
					Module
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $zone->ccs_id ?>" data-type="detail" class="auto_create">
				<button  class="btn btn-sm btn-primary ">Add Otomatic Module</button>
			</a>
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="new_module" data-lang="2" data-param="zone" class="detail2">
				<button  class="btn btn-sm btn-primary ">Add Module</button>
			</a>
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	
	<div class="ibox-content row">
		<div class="pull-right">
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
			<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
		</div>
		<div class="pull-left">
			<button class="disabled btn btn-white btn-xs" type="button"></button>
		</div>
		<div class=""  style="margin-top: 40px">
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example " >
			<thead>
				<tr>
					<th>No</th>
					<th>Module </th>
					<th>Category </th>
					<th>Createdby</th>
					<th>Date Created </th>
					<th>Status</th>
					<th>Activation</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($module)){ ?>
				<?php $i = 1; foreach($module as $m){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $m->module_id ?>" class="detail2"><?php echo $m->name ?></a>
					</td>
					<td>
						<a id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $m->module_id ?>" class="detail2"><?php echo $m->pname ?></a>
					</td>
					<?php foreach($creator as $c){ ;?>
						<td><?php if($m->createdby == $c->admin_id){echo $c->name ;} ;?></td>
					<?php ;};?>
					<td><?php if(strtotime($m->datecreated) != strtotime('0000-00-00 00:00:00')){echo date("l, d M Y", strtotime($m->datecreated));}else{ echo '-';};?> </td>
					<td><?php foreach($status as $s){ ;?>
						<?php if($m->status == $s->value){echo $s->invert ;} ;?>
					<?php ;} unset($s) ;?>
					</td>
					<td>
					<?php foreach($status as $s){ ;?>
						<?php if($m->status != $s->value){ ;?>
							<button id="<?php echo $m->module_id ?>" data-param="<?php echo $s->status_id ;?>" data-url="module" data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $m->name ;?>" class="modal_sts btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
						<?php ;} ;?>
					<?php ;} ;?>
						
							<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="edit_module" data-param="<?php echo $m->module_id ?>" data-lang="2" class="dtl btn btn-info ;?> btn-xs" type="button">Edit</button>
					</td>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<h5>ZONE :</h5>
					<a id="<?php echo $zone->type_id ?> " data-url="zone" data-url2="list" data-lang="2" data-param="zone" class="detail2">
						<p id="" name="" class=""><?php if($zone->type_id == 1){ echo "Company Profile" ;}elseif($zone->type_id == 2){ echo "Community" ;}elseif($zone->type_id == 3){ echo "E-commerce" ;}elseif($zone->type_id == 4){ echo "Siakad" ;}elseif($zone->type_id == 5){ echo "Custom" ;} ;?></p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery.onDemandScript = function( url, options ) {

	  // Allow user to set any option except for dataType, cache, and url
	  options = $.extend( options || {}, {
		dataType: "script",
		cache: true,
		url: url
	  });


	  // Use $.ajax() since it is more flexible than $.getScript
	  // Return the jqXHR object so we can chain callbacks
	  return jQuery.ajax( options );
	};
	
	$('.auto_create').on('click', function(){
		var type = $(this).data(type);
		modal_edit(type);
	});
		
	function modal_edit(type){
		$('[id*="modal_"]').each(function(){
			$(this).remove();
		});
		var id = type.type,
			base_url = '<?php echo base_url();?>',
			part = 'edit';
		var lang = 'aw2';
		var param = 'aw3';
		var status = 'Save';
		if(id == 'detail'){
			view_data = '<button id="detail_list" data-param="module" class="detail_list col-md-2 btn dim btn-sm btn-outline btn-success"><i class="fa fa-th-large"></i> <br>From Another Zone</button>'+
				'<button id="detail_list" data-param="admin" class="detail_list col-md-2 btn dim btn-sm btn-outline btn-warning"><i class="fa fa-empire"></i> <br>Create SuperAdmin</button>'+
				'<button id="detail_list" data-param="category" class="detail_list col-md-2 btn dim btn-sm btn-outline btn-danger"><i class="fa fa-cogs"></i> <br>Create Category</button>';
				/* '<button id="header_background" class="col-md-2 btn dim btn-sm btn-outline btn-danger"><i class="fa fa-square"></i> <br>Edit Background</button>'+
				'<button id="header_font" class="col-md-2 btn dim btn-sm btn-outline btn-info"><i class="fa fa-font"></i> <br>Edit Font</button>'; */
		}
		view_modal = 
			'<div class="modal inmodal fade" id="modal_'+id+'" tabindex="-1" role="dialog">'+
				'<div class="modal-dialog modal-lg">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal">'+
								'<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>'+
							'</button>'+
							'<h5 class="modal-title" style="text-transform:uppercase;float: none;">'+id+'</h5>'+
						'</div>'+
						'<div class="modal-body">'+
							'<div class="alert alert-danger" id="fail" style="display:none;"></div>'+
							'<div class="alert alert-info" id="success" style="display:none;"></div>'+
							'<div id="data" class="row">'+
								view_data+
							'</div>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>'+
							'<button id="create" data-id="'+id+'" data-lang="'+lang+'" data-param="'+param+'" data-status="'+status.toLowerCase()+'" data-type="" type="button" class="btn btn-info create_auto">'+status+'</button>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
		$('#page-wrapper').append(view_modal.replace(/,/g, ""));
		$('#modal_'+id+'').modal('toggle');
		$('.modal-backdrop').css('display','none');
		if(id == 'detail'){
			detail_list();
			/* header_logo();
			header_background();
			header_font(); */
		}
	}
	
	function detail_list(){
		$('.detail_list').on('click', function (e) {
			e.preventDefault;
			$('#data').empty();
			$('#data').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
			var id = 'detail',
				base_url = '<?php echo base_url();?>',
				part = 'detail',
				url = "<?php echo base_url($ccszone);?>/admin/zone/auto_create/"+part
				lang = 'aw2',//$(this).data('lang');
				param = $(this).data('param');
				param2 = 'aw3',//$(this).data('param');
				status = 'Save',//$(this).data('status');
				text = 'aw3';//$(this).data('title');
			var data = {
				<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
				id : id,
				layer : 1,
				ccs_id_origin : <?php echo $zone->ccs_id;?>,
				param : param,
				type_id : <?php echo $zone->type_id;?>
			};
			success_fun = function(json)
			{
				if(param == 'module'){
					if(json.type == 'list'){
						$.onDemandScript("<?php echo base_url();?>assets/admin/js/plugins/nestable/real.jquery.nestable.js").done(function( script, textStatus ) {
							
							view_data = [];
							$.each(json.data, function(key, val) {
								view_data[key] = '<option value="'+val.ccs_id+'">'+val.name+'</option>';
							});
							view = '<select id="" data-param="'+json.type+'" class="select_to_list form-control m-b">'+
								'<option value="">choose '+json.type+'</option>'+
								view_data+
							'</select>';
						   //$('.select_to_list').parent().append(view.replace(/,/g, ""));
							
							$('#data').empty();		
							$('#data').append(view.replace(/,/g, ""));
							
							$('button.create_auto').attr('data-type','');
							
							select_to_list();
						});
					}else if(json.type == 'delete'){
						button = '<button class="btn btn-danger create_auto">Delete All Module</button>';
						view = '<p>You already have a module created</p><p> to use this function Please Delete all module first</p><br>'+button;
						$('#data').empty();		
						$('#data').append(view.replace(/,/g, ""));
					}
				}else if(param == 'admin'){
					view = '';
					if(json.type == 'create'){
						view_data = [];
						view_data[0] = '<div class="form-group">'+
											'<label class="col-sm-5 control-label">username*</label>'+
											'<div class="col-sm-7"><input id="username" name="inputan" type="text" class="form-control"></div>'+
										'</div>';
						view_data[1] = '<div class="form-group">'+
											'<label class="col-sm-5 control-label">password*</label>'+
											'<div class="col-sm-7"><input id="password" name="inputan" type="password" class="form-control"></div>'+
										'</div>';
						view_data[2] = '<div class="form-group">'+
											'<label class="col-sm-5 control-label">confirm password*</label>'+
											'<div class="col-sm-7"><input id="password2" name="inputan" type="password" class="form-control"></div>'+
										'</div>';
						view = '<form class="col-md-6 form-horizontal">'+view_data+'<hr><span style="float:right">* required</span></form>';
					}else if(json.type == 'delete'){
						button = '<button class="btn btn-danger create_auto">Delete SuperAdmin</button>';
						view = '<p>You already created SuperAdmin</p><p> to use this function Please Delete SuperAdmin first</p><br>'+button;
					}else if(json.type == 'module'){
						button = '<button id="detail_list" data-param="module" class="detail_list btn btn-success create_auto">Create Module First</button>';
						view = "<p>You haven't created a module</p><p> to use this function Please Create module first</p><br>"+button;
					}
					
					$('#data').empty();		
					$('#data').append(view.replace(/,/g, ""));
					if(json.type == 'module'){
						detail_list();
					}else if(json.type == 'create'){
						$('button.create_auto').attr('data-type','create');
						$('button.create_auto').attr('data-param','admin');
						post();
					}
				}else if(param == 'category'){
					if(json.type == 'list'){
						$.onDemandScript("<?php echo base_url();?>assets/admin/js/plugins/nestable/real.jquery.nestable.js").done(function( script, textStatus ) {
							
							view_data = [];
							view = '<div class="col-md-7">'+
											'<div id="nestable-menu">'+
												'<button type="button" data-action="expand-all" class="btn btn-white btn-sm">Expand All</button>'+
												'<button type="button" data-action="collapse-all" class="btn btn-white btn-sm active">Collapse All</button>'+
											'</div>'+
										view_list_body(json)+
									'</div>'+
									'<textarea id="nestable-output" name="inputan" class="hide form-control"></textarea></div>';
							
							$('#data').empty();		
							$('#data').append(view.replace(/,/g, ""));
							
							$('button.create_auto').attr('data-type','');
							
							$('button.create_auto').attr('data-type','create');
							$('button.create_auto').attr('data-param','category');
							
							script_nestable();
							
							edit_data_view2('header');
							
							post();
						});
					}else if(json.type == 'delete'){
						button = '<button class="btn btn-danger create_auto">Delete All Module</button>';
						view = '<p>You already have a module created</p><p> to use this function Please Delete all module first</p><br>'+button;
						$('#data').empty();		
						$('#data').append(view.replace(/,/g, ""));
					}
				}
			}
			ajax_post('json', url, data, success_fun);
		});
	}
	
	function select_to_list(){
		$('.select_to_list').on('change', function (e) {
			e.preventDefault;
			if ($(this).find(':selected').val() != '') {
				
				var obj = $(this).attr('data-param'),
					url = "<?php echo base_url($ccszone);?>/admin/zone/auto_create/detail",
					ccsid = $(this).val();
					param = 'module';
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id : 'detail',
					param : param,
					layer : 2,
					ccs_id : ccsid,
					type_id : <?php echo $zone->type_id;?>
				};
				success_fun = function(json)
				{
					if (json.length === 0) {
						console.log('empty');
					}else{
						if(json.data.length != 0){
							view_data1 = view_list_body(json);
						
							view_data2 = '<p>From:</p>'+
										'<select id="" data-param="menu" class="select_list form-control m-b">'+
											'<option value="">choose </option>'+
											'<option value="page">Page</option>'+
											'<option value="module">Module</option>'+
											'<option value="parallax">Parallax</option>'+
											'<option value="custom">Custom</option>'+
									   ' </select>';
							view = 
									'<div class="select_to_list_result"><div class="col-md-7">'+
											'<div id="nestable-menu">'+
												'<button type="button" data-action="expand-all" class="btn btn-white btn-sm">Expand All</button>'+
												'<button type="button" data-action="collapse-all" class="btn btn-white btn-sm active">Collapse All</button>'+
											'</div>'+
										view_data1+
									'</div>'+
									'<div id="view_data2" class="col-md-5" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;">'+
										'<p>Create New Menu:</p>'+
										'<div id="list_menu">'+view_data2+'</div>'+
										'<button id="" class="list_menu_create btn btn-sm btn-outline btn-success pull-right" style="">add to menu</button>'+
									'</div>'+
									'<textarea id="nestable-output" name="inputan" class="hide form-control"></textarea></div>'+
									'<input id="ccs_id" type="hidden" name="inputan" value="'+ccsid+'">';
									
							$('.select_to_list_result').remove();		
							$('.select_to_list').parent().append(view.replace(/,/g, ""));
							$('button.create_auto').attr('data-type','create');
							$('button.create_auto').attr('data-param','module');
							
							script_nestable();
							
							edit_data_view2('header');
							post();
						}
					}
				}
				ajax_post('json', url, data, success_fun);
			}else{
				/* if($(this).hasClass('select2')){
					
				}else{
					$('.select2').each(function(){
						$(this).remove();
					});
				} */
			}
		});
	}
	
	function view_list_body(json){
	
			var data1 = [];
			var edit = [];
			v_open1 = '<div class="dd" id="nestable"><ol class="dd-list">';
			if(json.data){
				$.each(json.data, function(key, val) {
					var module = '';
					if(val.module_id){
						if(val.module_id != 'undefined'){
							var module = 'data-module="'+val.module_id+'"';
						}
					}
					var data_attr = 'data-type="'+val.icon+'" data-link="'+val.link+'" data-title="'+val.name+'" '+module+'';
					if(val.children){
						var data2 = [];
						$.each(val.children, function(key2, val2) {
							var module2 = '';
							if(val2.module_id){
								if(val2.module_id != 'undefined'){
									var module2 = 'data-module="'+val2.module_id+'"';
								}
							}
							var data_attr2 = 'data-type="'+val2.icon+'" data-link="'+val2.link+'" data-title="'+val2.name+'" '+module2+'';
							if(val2.children){
								var data3 = [];
								$.each(val2.children, function(key3, val3) {
									var module3 = '';
									if(val3.module_id){
										if(val3.module_id != 'undefined'){
											var module3 = 'data-module="'+val3.module_id+'"';
										}
									}
									var data_attr3 = 'data-type="'+val3.icon+'" data-link="'+val3.link+'" data-title="'+val3.name+'" '+module3+'';
									if(val3.children){
										var data4 = [];
										$.each(val3.children, function(key4, val4) {
											var module4 = '';
											if(val4.module_id){
												if(val4.module_id != 'undefined'){
													var module4 = 'data-module="'+val4.module_id+'"';
												}
											}
											var data_attr4 = 'data-type="'+val4.icon+'" data-link="'+val4.link+'" data-title="'+val4.name+'" '+module4+'';
											data4[key4] = '<li class="dd-item" '+data_attr4+'>'+
												'<div class="dd-handle" style="width: 77.9%;">'+val4.name+'</div>'+
												'<span id="" '+data_attr4+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
											'</li>';
										});
										data3[key3] = '<li class="dd-item" '+data_attr3+'>'+
											'<div class="dd-handle" style="width: 73.2%;">'+val3.name+'</div>'+
											'<span id="" '+data_attr3+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
											'<ol class="dd-list">'+
												data4+
											'</ol>'+
										'</li>';
									}else{
										data3[key3] = '<li class="dd-item" '+data_attr3+'>'+
											'<div class="dd-handle" style="width: 77.9%;">'+val3.name+'</div>'+
											'<span id="" '+data_attr3+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
										'</li>';
									}
								});
								data2[key2] = '<li class="dd-item" '+data_attr2+'>'+
									'<div class="dd-handle" style="width: 73.2%;">'+val2.name+'</div>'+
									'<span id="" '+data_attr2+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
									'<ol class="dd-list">'+
										data3+
									'</ol>'+
								'</li>';
							}else{
								data2[key2] = '<li class="dd-item" '+data_attr2+'>'+
									'<div class="dd-handle" style="width: 79.3%;">'+val2.name+'</div>'+
									'<span id="" '+data_attr2+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
								'</li>';
							}
						});
						data1[key] = '<li class="dd-item" '+data_attr+'>'+
							'<div class="dd-handle" style="width: 74.5%;">'+val.name+'</div>'+
								'<span id="" '+data_attr+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
							'<ol class="dd-list">'+
								data2+
							'</ol>'+
						'</li>';
					}else{
						data1[key] = '<li class="dd-item" '+data_attr+'>'+
							'<div class="dd-handle" style="width: 79.3%;">'+val.name+'</div>'+
								'<span id="" '+data_attr+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;"><i class="fa fa-pencil"></i></span>'+
						'</li>';
					}
				});				
			}else{
				data1 = '<h3>Empty Here, Please Create a New One</h3>';
			}
			
			v_close1 = '</ol></div>';	
			
			view_data1 = v_open1+data1+v_close1;
			
			return view_data1;
	}
	
	function edit_data_view2(type){
		id = type;
		if(id == 'header'){
			canvas = $('#data');
			canvas.delegate(".edit_menu",'click',function(e){
				var data_type = $(this).attr('data-type');
				var data_link = $(this).attr('data-link');
				var data_title = $(this).attr('data-title');
				
				close = '<p class="close_btn pull-right">x</p>';
				view_data2 = '<p>Name: <input id="list_menu_title" type="text" class="form-control" value="'+data_title+'"></input></p>'+
							'<p>Link: <small id="list_menu_link">'+data_link+'</small></p>';
				view = '<div id="view_data3" class="col-md-5" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;">'+close+
					'<p>Edit Menu <b>'+data_title+'</b>:</p><br>'+
					view_data2+
					'<button id="" class="list_menu_update btn btn-sm btn-outline btn-primary pull-right" style="">update</button>'+
					'<button id="" class="list_menu_remove btn btn-sm btn-outline btn-danger pull-right" style="margin-right:10px">remove</button>'+
				'</div>';
				$('#view_data2').parent().append(view);
				$('.close_btn').on("click", function(e) {
					$(this).parent().remove();
				});
			});
			
			canvas.delegate(".select_list",'change',function(e){
				e.preventDefault;
				if ($(this).find(':selected').val() != '') {
					if($(this).hasClass('select2')){
						if($(this).data('param') == 'module'){
							$('[data-param*="category"]').each(function(){
								$(this).remove();
							});
						}
					}else{
						$('.select2').each(function(){
							$(this).remove();
						});
					}
					
					var obj = $(this).attr('data-param'),
						url = "<?php echo base_url($ccszone);?>/admin/zone/auto_create/edit";
					var data = {
						<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
						id : 'header',
						param : $(this).data('param'),
						param2 : $(this).val(),
					};
					success_fun = function(json)
					{
						if (json.length === 0) {
							console.log('empty');
						}else{
							if(json.data.length != 0){
								view_data = [];
								$.each(json.data, function(key, val) {
									view_data[key] = '<option value="'+val.link+'">'+val.name+'</option>';
								});
								view = '<select id="" data-param="'+json.type+'" class="select2 select_list form-control m-b">'+
									'<option value="">choose '+json.type+'</option>'+
									view_data+
								'</select>';
							   $('.select_list').parent().append(view.replace(/,/g, ""));
							}
						}
					}
					ajax_post('json', url, data, success_fun);
				}else{
					if($(this).hasClass('select2')){
						
					}else{
						$('.select2').each(function(){
							$(this).remove();
						});
					}
				}
			});
			canvas.delegate(".list_menu_create",'click',function(e){
				e.preventDefault;
				$('#fail').empty();
				target = $('#list_menu').children().last().find(':selected');
				target_other = $('#list_menu').children();
				to = $('#nestable');
				type = 'multisite';
				link = target.val();
				if(link){
					if(link == 'all'){
						view = [];
						target_other.last().children().each(function(key,aw){
							if(($(this).val() != '') && ($(this).val() != 'all') && ($(this).val() != 'no')){
								val = $(this).text();
								link = $(this).val();
								view[key] = '<li class="dd-item" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'">'+
								'<div class="dd-handle" style="width: 79.3%;">'+
									val+
								'</div>'+
								'<span id="" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'" class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;">'+
									'<i class="fa fa-pencil"></i>'+
								'</span></li>';
							}
						});
					}else{
						if(link == 'no'){
							target = target_other.eq(-2).find(':selected');
							module = 'data-module="'+target.val()+'"';
						}else{
							cek_type = target_other.eq(-3).find(':selected').val();
							if(cek_type == 'module'){
								module = target_other.eq(-2).find(':selected').val();
							}else{
								module = target_other.eq(-2).find(':selected').val();
							}
							module = 'data-module="'+module+'"';
						}
						val = target.text();
						link = target.val();
						
						view = '<li class="dd-item" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'" '+module+'>'+
							'<div class="dd-handle" style="width: 79.3%;">'+
								val+
							'</div>'+
							'<span id="" data-type="'+type+'" data-link="'+link+'" data-title="'+val+'" '+module+' class="edit_menu btn btn-md btn-outline btn-success" style="margin: 4.47px;">'+
								'<i class="fa fa-pencil"></i>'+
							'</span></li>';
					}
						
					to.children().append(view);	
				}else{
					$('<p>please choose '+target_other.last().attr('data-param')+' first</p>').appendTo('#fail');
					$('#fail').show();
					$('#fail').fadeTo(4000, 500).slideUp(500); 
				}
			});
			
			canvas.delegate(".list_menu_remove",'click',function(e){
				e.preventDefault;
				target = $(this).parent();
				title = target.find('#list_menu_title').val();
				link = target.find('#list_menu_link').text();
				to = $('#nestable');
				to.find("li[data-link='" + link + "'] > .dd-handle").text(title);
				to = to.find("[data-link='" + link + "']");
				to.each(function(){
					$(this).remove();
				});
				$(this).parent().remove();
			});
			
			canvas.delegate(".list_menu_update",'click',function(e){
				e.preventDefault;
				target = $(this).parent();
				title = target.find('#list_menu_title').val();
				link = target.find('#list_menu_link').text();
				to = $('#nestable');
				to.find("li[data-link='" + link + "'] > .dd-handle").text(title);
				to = to.find("[data-link='" + link + "']");
				to.each(function(){
					$(this).attr('data-title',title);
				});
			});
			
		}else if(id == 'footer'){
			canvas = $('#data > .footer_list');
			canvas.delegate(".list_footer_create ",'click',function(e){
				close = '<p class="close_btn pull-right">x</p>';
				view_data2 = '<p>Name: <input id="title" type="text" class="form-control" value=""></input></p>'+
				'<p>Text: <input id="text" type="text" class="form-control" value=""></input></p>';
				
				view = '<div id="view_data3" class="col-md-12" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;margin-top:15px">'+close+
					'<p>Create New Footer:</p><br>'+
					view_data2+
					'<button id="" class="list_menu_update btn btn-sm btn-outline btn-primary pull-right" style="">update</button>'+
				'</div>';
				$('#view_data2').append(view);
				$('.close_btn').on("click", function(e) {
					$(this).parent().remove();
				});
			});
			canvas.delegate(".btn_edit_footer",'click',function(e){
				target = $(this).closest('.edit_hover');
				class_text = target.attr('class').replace('edit_hover','');
				class_text = class_text.replace('ui-sortable','').trim();
				column = class_text;
				/* data_column =	'<option value="col-md-1">1/12 row</option>'+
								'<option value="col-md-2">2/12 row</option>'+
								'<option value="col-md-3">3/12 row</option>'+
								'<option value="col-md-4">4/12 row</option>'+
								'<option value="col-md-5">5/12 row</option>'+
								'<option value="col-md-6">6/12 row</option>'+
								'<option value="col-md-7">7/12 row</option>'+
								'<option value="col-md-8">8/12 row</option>'+
								'<option value="col-md-9">9/12 row</option>'+
								'<option value="col-md-10">10/12 row</option>'+
								'<option value="col-md-11">11/12 row</option>'+
								'<option value="col-md-12">12/12 row</option>'; */
				data_column =	'<option value="col-md-3">1/4 row</option>'+
								'<option value="col-md-4">1/3 row</option>'+
								'<option value="col-md-6">1/2 row</option>'+
								'<option value="col-md-9">3/4 row</option>'+
								'<option value="col-md-12">1 row</option>';		
				title = target.find('h3').text().trim();
				text = target.find('.c-text').text().trim();
				data = '';
				type = target.find('.c-content-title-1').last().attr('class');
				if(type == 'c-blog'){
					data_type = 'blog';
				}else if(type == 'c-works'){
					data_type = 'image';
				}else if(type == 'c-links'){
					data_type = 'link';
				}else if(type == 'c-socials'){
					data_type = 'socmed';
				}
				close = '<p class="close_btn pull-right">x</p>';
				view_data2 = 
				'<div class="row"><p class="col-md-6">Column: <select id="column" class="form-control m-b">'+data_column+'</select></p>'+
				'<p class="col-md-6">Name: <input id="title" type="text" class="form-control" value="'+title+'"></input></p></div>'+
				'<p>Text: <input id="text" type="text" class="form-control" value="'+text+'"></input></p>';
				view = '<div id="view_data3" class="col-md-12" style="background: #fff;padding-top: 15px;padding-bottom: 15px;border: 2px solid #eee;margin-top:15px">'+close+
					'<p>Edit Footer:</p><br>'+
					view_data2+
					'<button id="" class="list_menu_update btn btn-sm btn-outline btn-primary pull-right" style="">update</button>'+
				'</div>';
				$('#view_data2').append(view);
				$('.close_btn').on("click", function(e) {
					$(this).parent().remove();
				});
				$('#view_data3').find('#column').val(column);
			});
			/* canvas = $('#data');
			canvas.delegate("btn_edit_footer",'click',function(e){
				target = $(this).closest('.edit_hover').html();
				$('#view_data2').append(target);
				console.log(target);
			}); */
		}else if(id == 'body'){
			
		}
	}
	
	function post(){
		function scrolltonote_2(){
			 $('.modal').animate({
				scrollTop: $('.modal-content').scrollTop(0)
			}, 1000);
		}
		function ajax_post_auto(obj,data,success_fun_2){
			if($(".file").val()){
				$.ajaxFileUpload({ 
					url : "<?php echo base_url($ccszone);?>/admin/zone/auto_create/"+obj,
					secureuri: false,
					fileElementId: file,
					type: "POST",
					dataType: 'JSON',
					data: data,
					success: function(data){
						success_fun_2(data);
					}
				});
			}else{
				$.ajax({
					url : "<?php echo base_url($ccszone);?>/admin/zone/auto_create/"+obj,
					secureuri: false,
					fileElementId: file,
					type: "POST",
					dataType: 'json',
					data: data,
					success: function(data){
						success_fun_2(data);
					}
				});
			}
		}
		
		$('.create_auto').on("click", function(e) {
			e.preventDefault();
			$('#success').empty();
			$('#fail').empty();
			$('.btn').each(function(){
				$(this).attr('disabled', 'disabled');
			});
			var obj = $(this).data('type');
				file = $(".file").attr('id');
				param = $(this).data('param');
				ccs_id_origin = <?php echo $zone->ccs_id;?>;
			function get_val(){
				var item_obj = {};
					$('[name="inputan"]').each(function(){
						item_obj[this.id] = this.value;
					});
				var item_array = {};
					$('[name="inputan_array"]').each(function(){
						item_array[this.id] = $(this).chosen().val();
					});
				if($(".file").val()){
					var item_summer = {};
					$('[name="inputan_summer"]').each(function(){
						item_summer[this.id] = $(this).code().replace(/"/g, "&quot;");
					});
				}else{
					var item_summer = {};
					$('[name="inputan_summer"]').each(function(){
						item_summer[this.id] = $(this).code();
					});
				}
				$.extend($.extend($.extend($.extend($.extend(item_obj, item_array), item_summer),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'}),{ccs_id_origin : ccs_id_origin}),{param : param});
				return item_obj;
			}
			var data = get_val();
			
			if($(".file").val()){
				success_fun_2 = function(data){
					JSON.parse(JSON.stringify(data), function(k, v) {
						var cek = JSON.parse(v.replace(/(<([^>]+)>)/ig,""));
						if (cek.status == "success"){
							$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#success');
							scrolltonote_2('.modal-body');
							$('#success').show();
							$('#success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							setTimeout(refresh_iframe(), 2500);
						}else{
							$('<p>'+cek.m.replace(/(&nbsp;|<([^>]+)>)/ig,"")+'</p>').appendTo('#fail');
							scrolltonote_2('.modal-body');
							$('#fail').show();
							$('#fail').fadeTo(4000, 500).slideUp(500); 
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						} 
					});    
				}
			}else{
				success_fun_2 = function(data){
					if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#success');
							scrolltonote_2('.modal-body');
							$('#success').show();
							$('#success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
							setTimeout(refresh_iframe(), 2500);
						}else{
							$('<p>'+data.m+'</p>').appendTo('#fail');
							scrolltonote_2('.modal-body');
							$('#fail').show();
							$('#fail').fadeTo(4000, 500).slideUp(500); 
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					}
			}
			ajax_post_auto(obj,data,success_fun_2);
		});
	}
	
	function script_nestable(){
		setTimeout(function(){
			$('.dd').nestable('collapseAll');
		}, 200);
		
		var updateOutput = function (e) {
			var list = e.length ? e : $(e.target),
					output = list.data('output');
			if (window.JSON) {
				output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
			} else {
				output.val('JSON browser support required for this demo.');
			}
		};
		 // activate Nestable for list 1
		$('#nestable').nestable({
			group: 1
		}).on('change', updateOutput);

		 // output initial serialised data
		updateOutput($('#nestable').data('output', $('#nestable-output')));

		$('#nestable-menu > button').on('click', function (e) {
			var target = $(e.target),
					action = target.data('action');
			$('#nestable-menu > button').each(function(){
				$(this).removeClass('active');
			});
			target.addClass('active');
			if (action === 'expand-all') {
				 $('.dd').nestable('expandAll');
			}
			if (action === 'collapse-all') {
				$('.dd').nestable('collapseAll');
			}
		});
	}
	function ajax_post($type, $url, $data, functtoexec)
	{	
		$('.btn').each(function(){
			$(this).attr('disabled', 'disabled');
		});
		$.ajax({
				url : $url,
				type: "POST",
				dataType: $type,
				data: $data,
				success: function(data){
					$('.btn').each(function(){$(this).removeAttr('disabled');});
					functtoexec(data);
				}
		});
	}
</script>