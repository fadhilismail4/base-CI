<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone->name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="2" data-param="type" class="detail2">
					User
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="2" data-param="type_new" class="detail2">
					New Type
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="module" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class=""  style="margin-bottom: 20px !important">
			<div class="pull-right">
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
				<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
			</div>
		</div>
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="ccs_id" name="inputan" type="text" class="form-control hide" value="<?php echo $zone->ccs_id;?>"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">User Type</label>
				<div class="col-sm-4"><input id="name" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group row">			
				<div class="col-md-12">
					<button id="create_user" data-param="type" class="create btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Create</button>
					<button id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="2" data-param="type" class="detail2 btn btn-md btn-warning pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>