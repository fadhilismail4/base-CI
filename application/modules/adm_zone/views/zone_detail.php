<div class="row animated fadeInRight">
	<div class="col-md-3">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Zone Detail</h5>
				<div class="ibox-tools">
				<?php foreach($status as $s ){ ;?>
					<?php if($zone->status != $s->value ){ ;?>
						<button id="<?php echo $zone->ccs_id ?>" data-param="<?php echo $s->status_id ;?>" data-url="zone" data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $zone->name ;?>" class="modal_status btn btn-<?php echo $s->color ;?> btn-sm" type="button"> <?php echo $s->name ;?></button>
					<?php ;} ;?>
				<?php ;} ;?>
				</div>
			</div>
			<div class="ibox-content " >
				<div class="row">
					<img alt="image" class="col-md-12 img-responsive" src="<?php if($zone->logo){ echo base_url('assets').'/'.strtolower($zone->name).'/'.$zone->logo;}else{ echo base_url('assets').'/img/logo_empty.png';}?>" style="display: block; margin-right: auto; margin-left: auto">
				</div>
				<div style="margin: 10px 0px !important; text-align: center">
					<div class="pull-right">
						<button  id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="update" data-param="zone" data-lang="" class="detail2 btn btn-white btn-xs" type="button"><i class="fa fa-pencil"></i> Edit</button>
					</div>
					<h3 style="text-transform: uppercase" >	
						<strong>
							<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
							<?php echo $zone->name;?>
							</a>
						</strong>
					</h3>				
				</div>
				<div class="row">	
					<p class="col-md-3">Owner</p><p class="col-md-9"><?php echo $zone->pic?></p>
					<p class="col-md-3">Phone</i></p><p class="col-md-9"><?php echo $zone->phone?></p>	
					<p class="col-md-3">Email</i></p><p class="col-md-9"><?php echo $zone->email?></p>	
					<p class="col-md-3">Start</i></p><p class="col-md-9">
						<?php if(strtotime($zone->datestart) != strtotime('0000-00-00 00:00:00')){echo date("l, d M Y", strtotime($zone->datestart));}else{ echo '-';};?> 
					</p>		
					<p class="col-md-3">End</i></p><p class="col-md-9">	
						<strong>
							<?php if(strtotime($zone->dateend) != strtotime('0000-00-00 00:00:00')){echo date("l,  d M Y", strtotime($zone->dateend));}else{ echo '-';};?> 
						</strong>
					</p>
					<p class="col-md-3">PIC</i></p><p class="col-md-9"><?php echo $zone->nama?></p>	
				</div>			
			</div>
		</div>
	</div>
	<div class="col-lg-9 detail_content2">
		<div class="ibox">
			<div class="ibox-title row">
				<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
					<li class="">
						<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
							<?php echo $module_name ;?>
						</a>
					</li>
					<li class="active">
						<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
							<?php echo $zone->name;?>
						</a>
					</li>
				</ol>
				<div class="ibox-tools">
					<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
						<button  class="btn btn-sm btn-warning ">Back</button>
					</a>
				</div>
			</div>
			<div class="ibox-content row">
				<div class="pull-right">
					<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
					<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
					<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admins" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
					<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
				</div>
				<div class="pull-left">
					<button class="disabled btn btn-white btn-xs" type="button"></button>
				</div>
				<div class="">
					<div class="text-center article-title">
						<div style="margin-bottom: -80px !important;">
							<h1 id="title" name="inputan" >
							<?php echo $zone->name;?>	
							</h1>
							
						</div>
					</div>
					<div id="description" name="inputan_summer">
						<?php echo trim($zone->description);?>
					</div>
					<!--
					<hr>
					<div class="row">
						<div class="col-md-6">
							<h4>Module: <span><button id="<?php echo $zone->ccs_id;?>" data-url="module" data-url2="new" data-lang="" data-param="zone" class="detail2 btn btn-white btn-xs" type="button">+ Module</button></span></h4>
							
							<?php foreach($module as $m){ 
									if(count($m['child']) == 0){
							;?>
								<p class="">
									<i class="fa fa-<?php if($m['menu']->icon){ echo $m['menu']->icon;};?>"></i><?php echo $m['menu']->name;?>
								</p>
							<?php ;} else { ?>
								<p class="">
									<i class="fa fa-<?php if($m['menu']->icon){ echo $m['menu']->icon;};?>"></i> <?php echo $m['menu']->name;?>
								</p>
								<?php if(!empty($m['child'])){ foreach($m['child'] as $c){?>
								<p style="padding-left:9px"> - <?php echo $c->name;?></p>
								<?php };}?>
							<?php ;} ;}?>
						</div>
					</div>
					-->
					<hr>
					<div class="row">
						<div class="col-md-6">
							<h5>ZONE :</h5>
							<a id="<?php echo $zone->type_id ?> " data-url="zone" data-url2="list" data-lang="2" data-param="zone" class="detail2">
								<p id="" name="" class=""><?php if($zone->type_id == 1){ echo "Company Profile" ;}elseif($zone->type_id == 2){ echo "Community" ;}elseif($zone->type_id == 3){ echo "E-commerce" ;}elseif($zone->type_id == 4){ echo "Siakad" ;}elseif($zone->type_id == 5){ echo "Custom" ;} ;?></p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>