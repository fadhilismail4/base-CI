<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone->name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="language" data-lang="2" data-param="module" class="detail2">
					Language
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="edit_language" data-param="<?php echo $zone->ccs_id;?>" data-lang="2" class="detail2">
					Edit 
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="language" data-lang="2" data-param="module" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="row"  style="margin-bottom: 20px !important">
			<div class="pull-right">
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
				<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
			</div>
		</div>
		<div class="row"  style="font-size: 14px">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<input id="ccs_id" name="inputan" type="text" class="form-control hide" value="<?php echo $zone->ccs_id ?>"></input>
			<input id="action" name="inputan" type="text" class="form-control hide" value="update"></input>
			<input id="lang_id" name="inputan" type="text" class="form-control hide" value=""></input>
			<form class="form-horizontal">
				<div class="form-group ">
					<label class="col-sm-3 control-label" style="text-align: left; margin-left: 15px">Language Module</label>
					<div class="col-sm-3">	
						<select class="form-control m-b" name="inputan" id="active_lang">
							<option value="">Select Activation</option>
							<option value="0">Inactive</option>
							<option value="1">Active</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align: left; margin-left: 15px">Total Allowed Language</label>
					<div class="col-sm-1"><input id="total_lang" name="inputan" type="text" class="form-control" value="<?php echo $zone->total_lang  ?>"></input></div>
					<label class="control-label">Language</label>
				</div>
				<div id="language" class="form-group col-sm-12 ">	
					<?php foreach($lng as $lng){ ;?>
						<div class="col-sm-3" style="padding: 10px 0">
							<div class="col-sm-4">
								<input id="<?php echo $lng->language_id ;?>" type="checkbox" class="i-checks">
							</div>
							<div class="col-sm-8" style="">
								<?php echo $lng->name?>
							</div>									
						</div>						
					<?php } ;?>	
				</div>
				<button id="" data-url="" data-lang="2" data-status="array" style="display: none;">
				</button>
			</form>	
			<div class="form-group col-sm-12">

				<div class="hr-line-dashed"></div>			
				<div class="col-md-12">
					<button id="create_lang" data-param="zone" class="create btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Update</button>
					<button id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="language" data-lang="2" data-param="module" class="detail2 btn btn-md btn-warning pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</div>
		
	</div>
</div>
<script>
$(document).ready(function(){
	$('#language input').each(function(){
		if(jQuery.inArray($(this).attr('id'), [<?php foreach($language as $l){ echo '"'.$l->language_id.'",';};?>]) !== -1){
			$(this).parent().addClass('checked');
			$(this).prop('checked', true);
			brands();
		}
	});
	$('#active_lang').val(<?php echo $zone->active_lang;?>);
	$("#language input").on("ifToggled", function (event) {
		brands();
	});
	
	function brands() {
		var brands = [];
		$("#language input").each(function (index, value) {
			if ($(this).is(":checked")) {
				brands.push($(this).attr("id"));
			}
		});
		//console.log(brands);
		$("#lang_id").val(brands);
	}
});
</script>