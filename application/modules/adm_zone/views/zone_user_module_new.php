<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>			
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone->name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="<?php echo $type->type_id?>" data-param="type" class="detail2">
					User
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="<?php echo $type->type_id?>" data-param="module_new" class="detail2">
					New Module
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="2" data-param="type" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class=""  style="margin-bottom: 20px !important">
			<div class="pull-right">
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
				<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
				<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
			</div>
		</div>
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<input id="ccs_id" name="inputan" type="text" class="form-control hide" value="<?php echo $zone->ccs_id?>"></input>
		<input id="tu_id" name="inputan" type="text" class="form-control hide" value="<?php echo $type->type_id?>"></input>
		<input id="type_id" name="inputan" type="text" class="form-control hide" value="<?php echo $type->type_id?>"></input>
		<input id="role_id" name="inputan" type="text" class="form-control hide" value="<?php echo $type->role_id?>"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-4"><input id="name" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Icon</label>
				<div class="col-sm-4"><input id="icon" name="inputan" type="text" class="form-control"></input></div>
				<label class="col-sm-2 control-label">Link</label>
				<div class="col-sm-4"><input id="link" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Category</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="parent_id">
						<option value="0">Set as Parent Category</option>
						<?php foreach($module as $m){ ;?>
						<option value="<?php echo $m->module_id ;?>"><?php echo $m->name ;?></option>
						<?php ;} ;?>
					</select>
				</div>
				
				<label class="col-sm-2 control-label">Module View</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="type_view">
						<option value="">Select Module View</option>
						<option value="0">Basic View</option>
						<option value="1">Category View</option>
						<option value="2">Gallery View</option>
						<option value="3">User View</option>
						<option value="4">Message View</option>
						<option value="5">Commerce View</option>
						<option value="6">Custom View</option>
					</select>
				</div>
				<label class="col-sm-2 control-label">Module Type</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="type_id">
						<option value="">Select Module Type</option>
						<option value="0">Basic Module</option>
						<option value="1">Category Module</option>
						<option value="2">Zone Module</option>
						<option value="3">Timely Module</option>
						<option value="4">Gallery Module</option>
						<option value="5">Commerce Module</option>
						<option value="6">User Module</option>
						<option value="7">Messages Module</option>
						<option value="8">Menu Module</option>
						<option value="9">Custom Module</option>
					</select>
				</div>
				
				<label class="col-sm-2 control-label">Frontend View</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="ftype">
						<option value="">Select to View on Frontend</option>
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>
				</div>
				<input id="param" name="inputan" type="text" class="hide" value="zone"></input>
				<input id="id" name="inputan" type="text" class="hide" value="<?php echo $zone->ccs_id ?>"></input>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group row">			
				<div class="col-md-12">
					<button id="create_user" data-param="module" class="create btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Create</button>
					<button id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="2" data-param="type" class="detail2 btn btn-md btn-warning pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>