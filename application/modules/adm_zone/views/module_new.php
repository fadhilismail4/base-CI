<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="1" data-url="zone" data-url2="detail" data-lang="2" data-param="basic_module" class="detail2">
					Module
				</a>
			</li>
			<li class="active">
				<a id="1" data-url="zone" data-url2="new_module" data-lang="2" data-param="1" class="detail2">
					New
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="1" data-url="zone" data-url2="detail" data-lang="2" data-param="basic_module" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-4"><input id="name" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Icon</label>
				<div class="col-sm-4"><input id="icon" name="inputan" type="text" class="form-control"></input></div>
				<label class="col-sm-2 control-label">Link</label>
				<div class="col-sm-4"><input id="link" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Category</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="parent_id">
						<option value="0">Set as Parent Category</option>
						<?php foreach($module as $m){ ;?>
						<option value="<?php echo $m->module_id ;?>"><?php echo $m->name ;?></option>
						<?php ;} ;?>
					</select>
				</div>
				<label class="col-sm-2 control-label">Module Type</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="type_id">
						<option value="">Select Module Type</option>
						<option value="0">Timely Module</option>
						<option value="1">Basic Module</option>
						<option value="2">Gallery Module</option>
						<option value="3">Commerce Module</option>
						<option value="4">Custom Module</option>
					</select>
				</div>
				<input id="param" name="inputan" type="text" class="hide" value="ccs"></input>
				<input id="id" name="inputan" type="text" class="hide" value="0"></input>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group row">			
				<div class="col-md-12">
					<button id="create_module" class="create btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Create</button>
					<button <a id="1" data-url="zone" data-url2="detail" data-lang="2" data-param="basic_module" class="detail2 btn btn-md btn-warning pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>