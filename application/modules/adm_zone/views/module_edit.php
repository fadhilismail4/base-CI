<div class="ibox">
	<div class="ibox-title">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="1" data-url="zone" data-url2="detail" data-lang="2" data-param="basic_module" class="detail2">
					Module
				</a>
			</li>
			<li class="">
				<a id="1" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $detail->module_id ?>" class="detail2">
					<?php echo $detail->name ?>
				</a>
			</li>
			<li class="active">
				<a id="1" data-url="zone" data-url2="edit_module" data-lang="2" data-param="<?php echo $detail->module_id?>" class="detail2">
					Edit
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="1" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $detail->module_id ?>" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-4"><input id="name" name="inputan" type="text" class="form-control" value="<?php echo $detail->name ?>"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Icon</label>
				<div class="col-sm-4"><input id="icon" name="inputan" type="text" class="form-control" value="<?php echo $detail->icon ?>"></input></div>
				<label class="col-sm-2 control-label">Link</label>
				<div class="col-sm-4"><input id="link" name="inputan" type="text" class="form-control" value="<?php echo $detail->link ?>"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control" value="<?php echo $detail->description?>"><?php echo $detail->description?></textarea></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Category</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="parent_id">
						<?php if($detail->parent_id != 0){?>							
						<option value="<?php echo $parent->parent_id ?>"><?php echo $parent->pname ?></option>
						<?php ;} ;?>
						<option value="0">Set as Parent Category</option>
						<?php foreach($module as $m){  if($detail->module_id != $m->module_id){ ;?>
							<option value="<?php echo $m->module_id ;?>"><?php echo $m->name ;?></option>						
						<?php ;} ;} ;?>
					</select>
				</div>
				<label class="col-sm-2 control-label">Module Type</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="type_id">
						<?php if($detail->type_id == 0){ ;?>							
							<option value="0">Timely Module</option>
						<?php }elseif($detail->type_id == 1){ ;?>								
							<option value="1">Basic Module</option>
						<?php }elseif($detail->type_id == 2){ ;?>
							<option value="2">Gallery Module</option>
						<?php }elseif($detail->type_id == 3){ ;?>
							<option value="3">Commerce Module</option>
						<?php }else{ ;?>							
							<option value="4">Custom Module</option>
						<?php } ;?>
						
						<option value="">Select Module Type</option>
						<option value="0">Timely Module</option>
						<option value="1">Basic Module</option>
						<option value="2">Gallery Module</option>
						<option value="3">Commerce Module</option>
						<option value="4">Custom Module</option>
					</select>
				</div>
				<input id="url_id" name="inputan" type="text" class="hide" value="<?php echo $detail->module_id ?>"></input>
				<input id="ccs_key" name="inputan" type="text" class="hide" value="12"></input>
				<input id="ccs_id" name="inputan" type="text" class="hide" value="0"></input>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group row">			
				<div class="col-md-12">
					<button id="update_module" class="create btn btn-md btn-primary pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Update</button>
					<button id="<?php echo $detail->ccs_id?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $detail->module_id?>" class="detail2 btn btn-md btn-warning pull-right"><i class="fa fa-times-circle"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>