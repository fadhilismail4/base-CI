<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone->name;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="2" data-param="type" class="detail2">
					User
				</a>
			</li>
			<li class="active">
				<a href="" id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="module" data-lang="<?php echo $type->type_id;?>" class="detail2">
					<?php echo $type->name;?> Module
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="<?php echo $type->type_id;?>" data-param="type_edit" class="detail2">
				<button  class="btn btn-sm btn-info ">Edit <?php echo $type->name;?> Type</button>
			</a>
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="<?php echo $type->type_id;?>" data-param="module_new" class="detail2">
				<button  class="btn btn-sm btn-primary ">Add <?php echo $type->name;?> Module</button>
			</a>
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="user" data-lang="2" data-param="type" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="pull-right">
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
			<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
		</div>
		<div class="pull-left">
			<button class="disabled btn btn-white btn-xs" type="button"></button>
		</div>
		<div class="" style="margin-top: 40px">
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example " >
				<thead>
					<tr>
						<th>No</th>
						<th>Module </th>
						<th>Category </th>
						<th>Createdby</th>
						<th>Date Created </th>
						<th>Status</th>
						<th>Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($module)){ ?>
					<?php $i = 1; foreach($module as $m){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<a id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-lang="<?php echo $m->module_id?>" data-param="detail_module" class="detail2"><?php echo $m->name ?></a>
						</td>
						<td>
							<a id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $m->module_id ?>" class="detail2"><?php echo $m->pname ?></a>
						</td>
						<?php foreach($creator as $c){ ;?>
							<td><?php if($m->createdby == $c->admin_id){echo $c->name ;} ;?></td>
						<?php ;};?>
						<td><?php if(strtotime($m->datecreated) != strtotime('0000-00-00 00:00:00')){echo date("l, d M Y", strtotime($m->datecreated));}else{ echo '-';};?> </td>
						<td><?php foreach($status as $s){ ;?>
							<?php if($m->status == $s->value){echo $s->invert ;} ;?>
						<?php ;} unset($s) ;?>
						</td>
						<td>
						<?php foreach($status as $s){ ;?>
							<?php if($m->status != $s->value){ ;?>
								<button id="<?php echo $m->module_id ?>" data-param="<?php echo $s->status_id ;?>" data-url="user_module" data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $m->name ;?>" class="modal_sts btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php ;} ;?>
							
								<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="edit_module" data-lang="<?php echo $m->module_id ?>" class="dtl btn btn-info ;?> btn-xs" type="button">Edit</button>
						</td>
					</tr>	
					<?php $i++;} ?>
				<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
				</tbody>
			</table>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<h5>ZONE :</h5>
					<a id="<?php echo $zone->type_id ?> " data-url="zone" data-url2="list" data-lang="2" data-param="zone" class="detail2">
						<p id="" name="" class=""><?php if($zone->type_id == 1){ echo "Company Profile" ;}elseif($zone->type_id == 2){ echo "Community" ;}elseif($zone->type_id == 3){ echo "E-commerce" ;}elseif($zone->type_id == 4){ echo "Siakad" ;}elseif($zone->type_id == 5){ echo "Custom" ;} ;?></p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>