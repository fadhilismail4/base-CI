<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone->name;?>
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="language" data-lang="2" data-param="module" class="detail2">
					Language
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<?php if( $zone->total_lang > count($language)){;?>
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="new_language" data-lang="2" data-param="module" class="detail2">
				<button  class="btn btn-sm btn-primary ">Add Language</button>
			</a>
			<?php };?>
			<a id="<?php echo $zone->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="pull-right">
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
			<button id="<?php echo $zone->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
			<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
		</div>
	
		<div class="pull-left">
			<button class="disabled btn btn-white btn-xs" type="button"></button>
		</div>
		<div class="row"  style="margin-top: 40px; font-size: 14px">
			
			<?php if($zone->active_lang == 1){ ;?>
				<div class="col-md-3">
					Language Module
				</div>
				<div class="col-md-9">
					<strong style="padding-right: 20px">Active</strong>
					<button  id="<?php echo $zone->ccs_id;?>"  data-url="zone" data-url2="edit_language" data-param="<?php echo $zone->ccs_id;?>" data-lang="2" class="detail2 btn btn-warning btn-xs" type="button">Edit</button>
				</div>
				<div class="col-md-3" style="margin: 10px 0">
					Total Allowed Language
				</div>
				<div class="col-md-9" style="margin: 10px 0">
					<strong style="padding-right: 20px"><?php echo $zone->total_lang ?> Language ( <?php echo count($language);?> Used )</strong>
				</div>
			<?php }else{ ;?>
				<div class="col-md-3">
					Language Module
				</div>
				<div class="col-md-9">
					<strong style="padding-right: 20px">Inactive</strong>
				</div>
				<div class="col-md-3" style="margin: 10px 0">
					Total Language
				</div>
				<div class="col-md-9" style="margin: 10px 0">
					<strong style="padding-right: 20px">1 Language</strong>
				</div>
			<?php } ;?>
		</div>
		<div class="">
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example " >
				<thead>
					<tr>
						<th>No</th>
						<th>Language </th>
						<th>Code </th>
						<th>Createdby</th>
						<th>Date Created </th>
						<th>Status</th>
						<th>Activation</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($status)){ ?>
					<?php $i = 1; foreach($language as $l){ ?>
					<tr class="gradeX">
						<td><?php echo $i ?></td>
						<td>
							<?php echo $l->name ?>
						</td>
						<td>
							<?php echo $l->code ?>
						</td>
						<?php foreach($creator as $c){ ;?>
							<td><?php if($l->createdby == $c->admin_id){echo $c->name ;} ;?></td>
						<?php ;};?>
						<td><?php if(strtotime($l->datecreated) != strtotime('0000-00-00 00:00:00')){echo date("l, d M Y", strtotime($l->datecreated));}else{ echo '-';};?> </td>
						<td><?php foreach($status as $s){ ;?>
							<?php if($l->status == $s->value){echo $s->invert ;} ;?>
						<?php ;} unset($s) ;?>
						</td>
						<td>
						<?php foreach($status as $s){ ;?>
							<?php if($l->status != $s->value){ ;?>
								<button id="<?php echo $l->zol_id ?>" data-param="<?php echo $s->status_id ;?>" data-url="language" data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $l->name ;?>" class="modal_sts btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<?php ;} ;?>
						<?php ;} ;?>
						</td>
					</tr>	
					<?php $i++;} ?>
				<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
				</tbody>
			</table>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<h5>ZONE :</h5>
					<a id="<?php echo $zone->type_id ?> " data-url="zone" data-url2="list" data-lang="2" data-param="zone" class="detail2">
						<p id="" name="" class=""><?php if($zone->type_id == 1){ echo "Company Profile" ;}elseif($zone->type_id == 2){ echo "Community" ;}elseif($zone->type_id == 3){ echo "E-commerce" ;}elseif($zone->type_id == 4){ echo "Siakad" ;}elseif($zone->type_id == 5){ echo "Custom" ;} ;?></p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>