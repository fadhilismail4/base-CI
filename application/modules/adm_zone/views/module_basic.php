<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="active">
				<a id="1" data-url="zone" data-url2="detail" data-lang="2" data-param="basic_module" class="detail2">
					Module
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="1" data-url="zone" data-url2="new_module" data-lang="2" data-param="1" class="detail2">
				<button  class="btn btn-sm btn-primary ">Add Module</button>
			</a>
			<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class=""  style="margin-top: 40px">
			<table class="table table-responsive table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>No</th>
					<th>Module </th>
					<th>Category </th>
					<th>Createdby</th>
					<th>Date Created </th>
					<th>Status</th>
					<th>Activation</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($module)){ ?>
				<?php $i = 1; foreach($module as $m){ ?>
				<tr class="gradeX">
					<td><?php echo $i ?></td>
					<td>
						<a id="1" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $m->module_id ?>" class="detail2"><?php echo $m->name ?></a>
					</td>
					<td>
						<a id="1" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $m->module_id ?>" class="detail2"><?php echo $m->pname ?></a>
					</td>
					<?php foreach($creator as $c){ ;?>
						<td><?php if($m->createdby == $c->admin_id){echo $c->name ;} ;?></td>
					<?php ;};?>
					<td><?php if(strtotime($m->datecreated) != strtotime('0000-00-00 00:00:00')){echo date("l, d M Y", strtotime($m->datecreated));}else{ echo '-';};?> </td>
					<td><?php foreach($status as $s){ ;?>
						<?php if(($m->status != $s->value) && ($s->value != 2)){echo $s->invert ;} ;?>
					<?php ;} unset($s) ;?>
					</td>
					<td>
					<?php foreach($status as $s){ ;?>
						<?php if(($m->status != $s->value) && ($s->value != 2)){ ;?>
							<button id="<?php echo $m->module_id ?>" data-param="<?php echo $s->status_id ;?>" data-url="module" data-lang="2" data-status="<?php echo $s->name ;?>" data-title="<?php echo $m->name ;?>" class="modal_sts btn btn-<?php echo $s->color ;?> btn-xs" type="button"> <?php echo $s->name ;?></button>
							<button id="1" data-url="zone" data-url2="edit_module" data-param="<?php echo $m->module_id ?>" data-lang="2" class="dtl btn btn-warning ;?> btn-xs" type="button">Edit</button>
						<?php ;} ;?>
					<?php ;} ;?>
					</td>
				</tr>	
				<?php $i++;} ?>
			<?php }else{ echo '<tr><td style="text-align: center" colspan="10">There is no data. Please create a new one... </td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td  style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>' ;} ;?>	
			</tbody>
		</table>
			<hr>
			<!--
			<div class="row">
				<div class="col-md-6">
					<h5>ZONE :</h5>
					<a id="<?php echo $zone->type_id ?> " data-url="zone" data-url2="list" data-lang="2" data-param="zone" class="detail2">
						<p id="" name="" class=""><?php if($zone->type_id == 1){ echo "Company Profile" ;}elseif($zone->type_id == 2){ echo "Community" ;}elseif($zone->type_id == 3){ echo "E-commerce" ;}elseif($zone->type_id == 4){ echo "Siakad" ;}elseif($zone->type_id == 5){ echo "Custom" ;} ;?></p>
					</a>
				</div>
			</div>
			-->
		</div>
	</div>
</div>