<div class="ibox">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					<?php echo $module_name ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $detail->ccs_id ?>" data-url="zone" data-url2="detail" data-lang="2" data-param="zone" class="detail3">
					<?php echo $zone ;?>
				</a>
			</li>
			<li class="">
				<a id="<?php echo $detail->ccs_id?>" data-url="zone" data-url2="detail" data-lang="2" data-param="module" class="detail2">
					Module
				</a>
			</li>
			<li class="active">
				<a id="<?php echo $detail->ccs_id;?>" data-url="zone" data-url2="detail_module" data-lang="2" data-param="<?php echo $detail->module_id ?>" class="detail2">
					<?php echo $detail->name ?>
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a id="<?php echo $detail->ccs_id?>" data-url="zone" data-url2="edit_module" data-lang="2" data-param="<?php echo $detail->module_id?>" class="detail2">
				<button  class="btn btn-sm btn-white "  style="color: #999999">Edit</button>
			</a>
			<a id="<?php echo $detail->ccs_id?>" data-url="zone" data-url2="detail" data-lang="2" data-param="module" class="detail2">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="pull-right">
			<button id="<?php echo $detail->ccs_id;?>" data-url="zone" data-url2="language" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Language</button>
			<button id="<?php echo $detail->ccs_id;?>" data-url="zone" data-url2="user" data-param="type" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">User</button>
			<button id="<?php echo $detail->ccs_id;?>" data-url="zone" data-url2="admin" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Admin</button>
			<button  id="<?php echo $detail->ccs_id;?>"  data-url="zone" data-url2="detail" data-param="module" data-lang="2" class="detail2 btn btn-white btn-xs" type="button">Module</button>
		</div>
		<div class="pull-left">
			<button class="disabled btn btn-white btn-xs" type="button"></button>
		</div>
		<div class=""  style="margin-top: 40px">
			<div class="row">
				<div class=" col-md-2">
					 <h4>Name</h4>
				</div>
				<div class="col-md-10">
					 <h4><?php echo $detail->name?></h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					Category 
				</div>
				<div class="col-md-10">
					 <?php if($detail->pname){echo $detail->pname ;} ;?>
				</div>
			</div>
			<div class="row" style="padding: 10px 0">
				<div class="col-md-2">
					Description 
				</div>
				<div class="col-md-10">
					 <?php if($detail->description){echo $detail->description ;} ;?>
				</div>
			</div>		
			<div class="row" style="padding-bottom: 10px">
				<div class="col-md-2">
					Module Type 
				</div>
				<div class="col-md-10">
					<?php if($detail->type_id == 0){
						echo "Timely Module" ;
					}elseif($detail->type_id == 1){
						echo "Basic Module" ;
					}elseif($detail->type_id == 2){
						echo "Gallery Module" ;
					}elseif($detail->type_id == 3){
						echo "Commmerce Module" ;
					}else{
						echo "Custom Module" ;					
					} ;?>
				</div>
			</div>
			<div class="row" style="padding-bottom: 10px">
				<div class="col-md-2">
					Status
				</div>
				<div class="col-md-10">
					<?php if($detail->status == 0){
						echo "Inactive" ;
					}else{
						echo "Active" ;					
					} ;?>
				</div>
			</div>
			<div class="row" style="padding-bottom: 10px">
				<div class="col-md-2">
					Detail
				</div>
				<div class="col-md-10">
					<?php if(!empty($datas)){;?>
						<p class="pull-left">Total Data: <?php echo count($datas);?> </p>
						<button id="<?php echo $detail->ccs_id;?>" data-param="<?php echo $detail->module_id;?>" data-param2="" data-type="all" class="reset btn btn-white btn-xs pull-left" type="button" style="margin-left:10px">Reset</button>
					<?php }else{
						echo "Not Available at this moment";					
					} ;?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('.reset').on("click", function(e) {
	e.preventDefault();
	$('.btn').each(function(){
		$(this).attr('disabled', 'disabled');
	});
	var type = $(this).data('type');
	var data = {
		<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
		param : $(this).data('param'),
		ccs_id : $(this).attr('id'),
	};
	var url = '<?php echo base_url($ccszone);?>/admin/zone/remove/'+type;
	$.ajax({
		url : url,
		type: "POST",
		dataType: 'html',
		data: data,
		success: function(html){
			$('.btn').each(function(){$(this).removeAttr('disabled');});
			$('ol.breadcrumb li.active > a').click();
		}
	});
});
// end ajax to call view
</script>