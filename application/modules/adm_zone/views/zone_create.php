<div class="ibox float-e-margins">
	<div class="ibox-title row">
		<ol class="breadcrumb col-md-7" style="font-size: 14px; padding-top: 6px">
			<li class="">
				<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
					Zone
				</a>
			</li>
			<li class="active">
				<a id="1" data-url="zone" data-url2="new" data-lang="2" data-param="zone" class="detail2">
					New
				</a>
			</li>
		</ol>
		<div class="ibox-tools">
			<a href="<?php echo base_url()?><?php echo $ccszone ;?>/admin/zone">
				<button  class="btn btn-sm btn-warning ">Back</button>
			</a>
		</div>
	</div>
	<div class="ibox-content row">
		<div class="alert alert-danger" id="fail" style="display:none;"></div>
		<div class="alert alert-info" id="success" style="display:none;"></div>
		<input id="language_id" name="inputan" type="text" class="form-control hide" value="2"></input>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-4"><input id="name" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group" id="data_5">
				<label class="col-sm-2 control-label" style="">Zone Start</label>
				<div class="col-sm-10">
					<div class="input-daterange input-group" id="datepicker">
						<input id="datestart" name="inputan"type="text" class="input-sm form-control"/>
						<span class="input-group-addon">Until</span>
						<input id="dateend" name="inputan" type="text" class="input-sm form-control" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Contact Person</label>
				<div class="col-sm-4"><input id="pic" name="inputan" type="text" class="form-control"></input></div>
				<label class="col-sm-2 control-label">Email</label>
				<div class="col-sm-4"><input id="email" name="inputan" type="text" class="form-control"></input></div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Phone</label>
				<div class="col-sm-4"><input id="phone" name="inputan" type="text" class="form-control"></input></div>
				<label class="col-sm-2 control-label">Price</label>
				<div class="col-sm-4"><input id="price" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10"><textarea id="description" name="inputan" type="text" class="form-control"></textarea></div>
			</div>
			
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Category</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="type_id">
						<option value="">Select Zone Category</option>
						<option value="1">Company Profile</option>
						<option value="2">Community</option>
						<option value="3">E-Commerce</option>
						<option value="4">Academic System</option>
						<option value="5">Custom</option>
					</select>
				</div>
				<label class="col-sm-2 control-label">Payment Method</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="zone_id">
						<option value="">Select Payment Method</option>
						<option value="0">Once Time</option>
						<option value="1">Monthly</option>
						<option value="2">Yearly</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Language Module</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="active_lang">
						<option value="">Select Activation</option>
						<option value="0">Inactive</option>
						<option value="1">Active</option>
					</select>
				</div>
				<label class="col-sm-2 control-label">User Module</label>
				<div class="col-sm-4">
					<select class="form-control m-b" name="inputan" id="active_user">
						<option value="">Select Activation</option>
						<option value="0">Inactive</option>
						<option value="1">Active</option>
					</select>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group row">
				<div class="col-md-12">				
					<button id="<?php echo $link_create;?>" class="create btn btn-primary btn-md pull-right" style="margin-left: 20px"><i class="fa fa-check-square-o"></i> Create</button>
					<button id="all" data-url="<?php echo $part;?>" data-url2="list" data-lang="<?php if(!empty($category)){  echo $category[0]->language_id;}else{ $language_id;};?>" data-param="zone" class="btn btn-warning btn-md pull-right" type="submit"><i class="fa fa-times-circle-o"></i> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>