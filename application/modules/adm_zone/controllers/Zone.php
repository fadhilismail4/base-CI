<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Zone extends Admin {
		
	public function index($zone){	
		if ($this->s_login)
    	{
			$data = array();
			//Set parameter view
			$data['title'] 		= $this->title;
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['zone'] 	= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu;
			$data['is_mobile']	= $this->is_mobile();
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['menu_active'] = 'dashboard';	
			$data['parent'] = 'zone';
			$data['ch'] = array(
				'title'=>'Zone', 
				'small'=>'Be silent and let the success bring the noise'
			);
			$data['breadcrumb'] = array(
				array(
					'url' => ''.$this->zone.'/admin/dashboard', 
					'bcrumb' => 'Dashboard', 
					'active' => ''
				),array(
					'url' => ''.$this->zone.'/admin/zone', 
					'bcrumb' => $this->module_name, 
					'active' => true
				)
			);
			$data['child'] = '';
			$data['css'] = array(
				'css/plugins/dataTables/dataTables.bootstrap.css',
				'css/plugins/dataTables/dataTables.responsive.css',
				'css/plugins/dataTables/dataTables.tableTools.min.css'
			);
			$data['js'] = array(
				'js/plugins/dataTables/jquery.dataTables.js',
				'js/plugins/dataTables/dataTables.bootstrap.js',
				'js/plugins/dataTables/dataTables.responsive.js',
				'js/plugins/dataTables/dataTables.tableTools.min.js',
				'js/modul/datatables.js',
				'js/plugins/nestable/jquery.nestable.js'
			);
			//End set parameter view
			$data['custom_js'] = '<script>
				$(document).ready(function(){
					$("[data-param='. "'" .'category'. "'" .']")[0].click();
					$("[data-param='. "'" .'zone'. "'" .']")[0].click();
				});
			</script>';
			$data['content'] = 'zone.php';
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$type){
		if ($this->s_login && !empty($type)){
			
			//$this->load->model('Media_model');
			$data = array();
			//$data['child'] = $part;
			if($type == 'new'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|xss_clean');

				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['ccszone'] = $this->zone;
					
					
					if($part == 'zone'){
						echo '<link href="'.base_url().'assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet"><script src="'.base_url().'assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type="text/javascript"></script><script>$("#datepicker").datepicker({
							format: "yyyy-mm-dd",
							keyboardNavigation: false,
							forceParse: false
						});</script>';
						$data['language_id'] = 2;
						$data['part'] = $part;
						$data['link_create'] = 'create_zone';
						$view = $this->load->view('zone_create', $data, TRUE);
					}
					echo $view;
				}
			}elseif($type == 'detail'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['module_name'] = $this->module_name;
					$data['ccszone'] = $this->zone;
					//$data['zonekey'] = $this->module_key;
					
					if($part == 'zone'){
						$data['part'] = 'zone';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['module'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$id.'','mdl');
						$menu_parent = $this->dbfun_model->get_all_data_bys('mdl.module_id, mdl.parent_id, mdl.icon, mdl.name,mdl.link','ccs_mdl.ccs_id = '.$id.' and ccs_mdl.parent_id = 0 order by ccs_mdl.module_id ASC','mdl');
						$module = array();
							foreach($menu_parent as $mp){
								$module_child = $this->dbfun_model->get_all_data_bys('mdl.module_id, mdl.parent_id, mdl.icon, mdl.name, mdl.link','ccs_mdl.parent_id = '.$mp->module_id.'','mdl');
								$module[] = array('menu' => $mp, 'child' => $module_child);
							}
						$data['module'] = $module;
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$this->zone_id.' and ccs_sts.ccs_key = '.$this->module_key.' ','sts');
						$view = $this->load->view('zone_detail', $data, TRUE);
					}elseif($part == 'module'){						
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');						
						$data['module'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = '.$id.'','mdl');
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$this->zone_id.' and ccs_sts.ccs_key = '.$this->module_key.' ','sts');
						$data['creator'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 1 ','adm');
						
						
						$view = $this->load->view('zone_module', $data, TRUE);
					}elseif($part == 'basic_module'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = 1 and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');						
						$data['module'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 0 order by module_id ASC','mdl');
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$this->zone_id.' and ccs_sts.ccs_key = '.$this->module_key.' ','sts');
						$data['creator'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 1 ','adm');
						$view = $this->load->view('module_basic', $data, TRUE);
					}
					echo $view;					
				}
			}elseif($type == 'list'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['ccszone'] = $this->zone;
					
					if($part == 'category'){
						$data['part'] = 'zone';
						$view = $this->load->view('category', $data, TRUE);
					}elseif($part == 'zone'){
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';
						$data['zone'] = $this->zone;
						if($id == 'all'){
							$data['zones'] = $this->dbfun_model->get_all_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id != 0 and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
							
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$this->zone_id.' and ccs_sts.ccs_key = '.$this->module_key.' ','sts');
						}else{
							$data['zones'] = $this->dbfun_model->get_all_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id != 0 and ccs_zone.type_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
							
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = '.$this->zone_id.' and ccs_sts.ccs_key = '.$this->module_key.' ','sts');
						}
						//print_r($category);
						$view = $this->load->view('list', $data, TRUE);
					}
					echo $view;
				}
			}elseif( $type== 'new_module'){	
				$this->form_validation->set_rules('id','Zone ID', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['ccszone'] = $this->zone;					
					$data['module_name'] = $this->module_name;
					if($id == 1){
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = 1 and ccs_zone.createdby = ccs_adm.admin_id ','zone,adm');						
						//$data['module'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = '.$id.'','mdl');
						
						$data['module'] = $this->dbfun_model->get_all_data_bys('module_id, parent_id, name, description','ccs_id = 0 and parent_id = 0', 'mdl');	
						$view = $this->load->view('module_new', $data, TRUE);
					}else{
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');						
						$data['module'] = $this->dbfun_model->get_all_data_bys('distinct(ccs_mdl.module_id),ccs_mdl.*','ccs_id = 0 or ccs_id = '.$id.' and ccs_mdl.parent_id = 0 ','mdl');
						//$data['module'] = $this->dbfun_model->get_all_data_bys('distinct(ccs_mdl.name),ccs_mdl.*','ccs_id = 0 or ccs_id = '.$id.' ','mdl');
						$view = $this->load->view('zone_module_new', $data, TRUE);
					}
				}
				echo $view;
			}elseif($type == 'update'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['ccszone'] = $this->zone;
					if($part == 'zone'){
						echo '<link href="'.base_url().'assets/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet"><script src="'.base_url().'assets/admin/js/plugins/datapicker/bootstrap-datepicker.js" type="text/javascript"></script><script>$("#datepicker").datepicker({
							format: "yyyy-mm-dd",
							keyboardNavigation: false,
							forceParse: false
						});</script>';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['language_id'] = 2;
						$data['part'] = $part;
						$data['link_create'] = 'create_zone';
						echo '<link href="'.base_url().'assets/admin/js/plugins/fileinput/fileinput.min.css" rel="stylesheet"><script src="'.base_url().'assets/admin/js/plugins/fileinput/fileinput.min.js" type="text/javascript"></script><script src="'.base_url().'assets/admin/js/ajaxfileupload.js" type="text/javascript"></script>';
						if(!empty($data['zone']->logo)){
							echo '<script>
								$(document).ready(function(){
									$("#datestart").val("'.trim(date('Y-m-d', strtotime($data['zone']->datestart))).'");
									$("#dateend").val("'.trim(date('Y-m-d', strtotime($data['zone']->dateend))).'");
									$(".file").fileinput("refresh",{
										initialPreview: [
											"'.str_replace('"',"'",'<img src="'.base_url('assets').'/'.strtolower($data['zone']->name).'/'.$data['zone']->logo.'" class="file-preview-image">').'"
										]
									});
								});</script>';
						}else{
							echo '<script>
							$(document).ready(function(){
								$("#datestart").val("'.trim(date('Y-m-d', strtotime($data['zone']->datestart))).'");
								$("#dateend").val("'.trim(date('Y-m-d', strtotime($data['zone']->dateend))).'");
								$(".file").fileinput("refresh",{
									initialPreview: [
										"'.str_replace('"',"'",'<img src="'.base_url('assets').'/img/logo_empty.png'.'" class="file-preview-image">').'"
									]
								});
							});</script>';
						}
						$view = $this->load->view('zone_update', $data, TRUE);
					}
					echo $view;
				}
			}elseif( $type== 'edit_module'){	
				$this->form_validation->set_rules('id','Zone ID', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['ccszone'] = $this->zone;					
					$data['module_name'] = $this->module_name;
					if($id == 1){
						$data['detail'] = $this->dbfun_model->get_data_bys('mdl.* ','ccs_mdl.module_id = '.$part.' ','mdl');
						if( $data['detail']->parent_id != 0 ){
							$data['parent'] = $this->dbfun_model->get_data_bys('mdl.parent_id, mdl.pname ','ccs_mdl.module_id = '.$part.' ','mdl');
						}
						$data['module'] = $this->dbfun_model->get_all_data_bys('module_id, parent_id, name, description','ccs_id = 0 and parent_id = 0', 'mdl');	
						$view = $this->load->view('module_edit', $data, TRUE);
					}else{
						$data['detail'] = $this->dbfun_model->get_data_bys('mdl.* ','ccs_mdl.module_id = '.$part.' ','mdl');
						if( $data['detail']->parent_id != 0 ){
							$data['parent'] = $this->dbfun_model->get_data_bys('mdl.parent_id, mdl.pname ','ccs_mdl.module_id = '.$part.' ','mdl');
						}
						$data['zone'] = $this->dbfun_model->get_data_bys('name','ccs_zone.ccs_id = '.$id.' ','zone')->name;			
						$data['module'] = $this->dbfun_model->get_all_data_bys('module_id, parent_id, name, description','ccs_id = '.$id.' and parent_id = 0', 'mdl');	
						$view = $this->load->view('zone_module_edit', $data, TRUE);
					}
				}
				echo $view;
			}elseif( $type== 'detail_module'){	
				$this->form_validation->set_rules('id','Zone ID', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');				
					$data['module_name'] = $this->module_name;					
					$data['ccszone'] = $this->zone;
					
					if($id == 1){
						$data['detail'] = $this->dbfun_model->get_data_bys('mdl.* ','ccs_mdl.module_id = '.$part.' ','mdl');
						if( $data['detail']->parent_id != 0 ){
							$data['parent'] = $this->dbfun_model->get_data_bys('mdl.parent_id, mdl.pname ','ccs_mdl.module_id = '.$part.' ','mdl');
						}
						$data['module'] = $this->dbfun_model->get_all_data_bys('module_id, parent_id, name, description','ccs_id = 0 and parent_id = 0', 'mdl');	
						$view = $this->load->view('module_view', $data, TRUE);
					}else{
						$data['detail'] = $this->dbfun_model->get_data_bys('mdl.* ','ccs_mdl.module_id = '.$part.' ','mdl');
						if( $data['detail']->parent_id != 0 ){
							$data['parent'] = $this->dbfun_model->get_data_bys('mdl.parent_id, mdl.pname ','ccs_mdl.module_id = '.$part.' ','mdl');
						}
						$data['zone'] = $this->dbfun_model->get_data_bys('name','ccs_zone.ccs_id = '.$id.' ','zone')->name;					
						$data['module'] = $this->dbfun_model->get_all_data_bys('module_id, parent_id, name, description','ccs_id = '.$id.' and parent_id = 0', 'mdl');	
						if(in_array($data['detail']->link,array('transaction','transactions'),true)){
							$data['datas'] = $this->dbfun_model->get_all_data_bys('trx_id','ccs_key = '.$part.' and ccs_id = '.$id.'','trx');
						}else{
							$data['datas'] = array();
						}
						$view = $this->load->view('zone_module_view', $data, TRUE);
					}
				}
				echo $view;
			}elseif($type == 'language'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['module_name'] = $this->module_name;
					$data['ccszone'] = $this->zone;
					//$data['zonekey'] = $this->module_key;
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
					if(($part == 'module') &&($id != 1)){
						$data['part'] = 'Language';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['module'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$id.'','mdl');
						$data['language'] = $this->dbfun_model->get_all_data_bys('ccs_lng.createdby, ccs_lng.code, ccs_lng.language_id, ccs_lng.name, ccs_zol.zol_id, ccs_zol.status, ccs_zol.datecreated','ccs_id = '.$id.' and ccs_zol.lang_id = ccs_lng.language_id ','zol, lng');
						$data['lang'] = $this->dbfun_model->get_all_data_bys('ccs_lng.createdby, ccs_lng.code, ccs_lng.language_id, ccs_lng.name, ccs_zol.zol_id, ccs_zol.status, ccs_zol.datecreated','ccs_id = '.$id.' and ccs_zol.lang_id = ccs_lng.language_id and ccs_zol.status = 1','zol, lng');
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 1 and ccs_sts.ccs_key = 3 order by ccs_sts.value asc ','sts');
						$data['creator'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 1 ','adm');
						
						$view = $this->load->view('zone_language', $data, TRUE);
					}
					echo $view;					
				}
			}elseif($type == 'new_language'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				
				echo '<link href="http://buzcon.com/project//assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet" type="text/css">';
				echo '<script src="http://buzcon.com/project//assets/admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>';
				echo '<script>
				$(".i-checks").iCheck({
					checkboxClass: "icheckbox_square-green",
					radioClass: "iradio_square-green",
				});</script>';
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['module_name'] = $this->module_name;
					$data['ccszone'] = $this->zone;
					//$data['zonekey'] = $this->module_key;
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
					if(($part == 'module') &&($id != 1)){
						$data['part'] = 'Language';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['module'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$id.'','mdl');
						$data['language'] = $this->dbfun_model->get_all_data_bys('ccs_lng.code, ccs_lng.language_id','ccs_id = '.$id.' and ccs_zol.lang_id = ccs_lng.language_id ','zol, lng');				
						$data['lng'] = $this->dbfun_model->get_all_data_bys('ccs_lng.language_id, ccs_lng.code, ccs_lng.name','ccs_lng.status != 0','lng');
					
						$view = $this->load->view('zone_language_new', $data, TRUE);
					}
					echo $view;					
				}
			}elseif( $type== 'edit_language'){	
				$this->form_validation->set_rules('id','Zone ID', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				
				echo '<link href="http://buzcon.com/project//assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet" type="text/css">';
				echo '<script src="http://buzcon.com/project//assets/admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>';
				echo '<script>
				$(".i-checks").iCheck({
					checkboxClass: "icheckbox_square-green",
					radioClass: "iradio_square-green",
				});</script>';
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$lang = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['ccszone'] = $this->zone;			
					$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
					$data['language'] = $this->dbfun_model->get_all_data_bys('ccs_lng.language_id','ccs_id = '.$id.' and ccs_zol.lang_id = ccs_lng.language_id ','zol, lng');
					echo '';				
					$data['lng'] = $this->dbfun_model->get_all_data_bys('ccs_lng.language_id, ccs_lng.code, ccs_lng.name','ccs_lng.status != 0','lng');
					$data['module_name'] = $this->module_name;
					$view = $this->load->view('zone_language_edit', $data, TRUE);
				}
				echo $view;
			}elseif($type == 'user'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$user_type_id = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['module_name'] = $this->module_name;
					$data['ccszone'] = $this->zone;
					//$data['zonekey'] = $this->module_key;
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
					if(($part == 'type') &&($id != 1)){
						$data['part'] = 'User Type';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$type = $this->dbfun_model->get_all_data_bys('role_id,type_id, name, status, createdby, datecreated','ccs_id = '.$id.' ','umr');
						$type_user = array();
						foreach($type as $t){
							$total = $this->dbfun_model->count_result('type_id = '.$t->type_id.' and ccs_id = '.$id.'','usr');
							$type_user[] = array('type' => $t, 'total' => $total);
						}
						$data['type'] = $type_user;
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 1 and ccs_sts.ccs_key = 3 order by ccs_sts.value asc ','sts');
						$data['creator'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 1 ','adm');
						
						$view = $this->load->view('zone_user_type', $data, TRUE);
					}elseif(($part == 'type_new') &&($id != 1)){
						$data['part'] = 'User Type';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$view = $this->load->view('zone_user_type_new', $data, TRUE);
					}elseif(($part == 'type_edit') &&($id != 1)){
						$data['part'] = 'User Type';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['type'] = $this->dbfun_model->get_data_bys('role_id,type_id, name, description,status, createdby, datecreated','ccs_id = '.$id.' and type_id = '.$user_type_id.'','umr');
						$view = $this->load->view('zone_user_type_edit', $data, TRUE);
					}elseif(($part == 'module') &&($id != 1)){
						$data['part'] = 'User Type';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['type'] = $this->dbfun_model->get_data_bys('role_id,type_id, name, status, createdby, datecreated','ccs_id = '.$id.' and type_id = '.$user_type_id.'','umr');
						$data['module'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = '.$id.' and tu_id = '.$data['type']->type_id.' ','umd');
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 1 and ccs_sts.ccs_key = 3 order by ccs_sts.value asc ','sts');
						$data['creator'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 1 ','adm');
						
						$view = $this->load->view('zone_user_module', $data, TRUE);
					}elseif(($part == 'module_new') &&($id != 1)){
						$data['part'] = 'User Type';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['type'] = $this->dbfun_model->get_data_bys('role_id,type_id, name, status, createdby, datecreated','ccs_id = '.$id.' and type_id = '.$user_type_id.'','umr');
						$data['module'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = '.$id.' and tu_id = '.$data['type']->type_id.' ','umd');
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 1 and ccs_sts.ccs_key = 3 order by ccs_sts.value asc ','sts');
						$data['creator'] = $this->dbfun_model->get_all_data_bys('*','ccs_id = 1 ','adm');
						
						$view = $this->load->view('zone_user_module_new', $data, TRUE);
					}elseif(($part == 'detail_module') &&($id != 1)){
						$data['detail'] = $this->dbfun_model->get_data_bys('umd.* ','ccs_umd.module_id = '.$user_type_id.' ','umd');
						if( $data['detail']->parent_id != 0 ){
							$data['parent'] = $this->dbfun_model->get_data_bys('umd.parent_id, umd.pname ','ccs_umd.module_id = '.$user_type_id.' ','umd');
						}
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['type'] = $this->dbfun_model->get_data_bys('role_id,type_id, name, status, createdby, datecreated','ccs_id = '.$id.' and type_id = '.$user_type_id.'','umr');			
						$data['module'] = $this->dbfun_model->get_all_data_bys('module_id, parent_id, name, description','ccs_id = '.$id.' and parent_id = 0', 'umd');	
						$view = $this->load->view('zone_user_module_view', $data, TRUE);
					}elseif(($part == 'edit_module') &&($id != 1)){
						$data['detail'] = $this->dbfun_model->get_data_bys('umd.* ','ccs_umd.module_id = '.$user_type_id.' ','umd');
						if( $data['detail']->parent_id != 0 ){
							$data['parent'] = $this->dbfun_model->get_data_bys('umd.parent_id, umd.pname ','ccs_umd.module_id = '.$user_type_id.' ','umd');
						}
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						$data['type'] = $this->dbfun_model->get_data_bys('role_id,type_id, name, status, createdby, datecreated','ccs_id = '.$id.' and type_id = '.$user_type_id.'','umr');			
						$data['module'] = $this->dbfun_model->get_all_data_bys('module_id, parent_id, name, description','ccs_id = '.$id.' and parent_id = 0', 'umd');	
						$view = $this->load->view('zone_user_module_edit', $data, TRUE);
					}
					echo $view;					
				}
			}elseif($type == 'admins'){
				$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
				$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
				$this->form_validation->set_rules('param','parameter', 'trim|required|xss_clean');
				if($this->form_validation->run() == TRUE ){
					$id = $this->input->post('id');
					$user_type_id = $this->input->post('language_id');
					$part = $this->input->post('param');
					$data['module_name'] = $this->module_name;
					$data['ccszone'] = $this->zone;
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
					if(($part == 'type') &&($id != 1)){
						$data['part'] = 'User Type';
						$data['zone'] = $this->dbfun_model->get_data_bys('zone.*, adm.name as nama','ccs_zone.zone_id != 0 and ccs_zone.ccs_id = '.$id.' and ccs_zone.createdby = ccs_adm.admin_id and ccs_adm.ccs_id = 1','zone,adm');
						
						$view = $this->load->view('zone_admin_type', $data, TRUE);
					}
					
				}
				echo $view;
			}
		}else{
			redirect($this->login);
		}
	}
	
	public function create($zone,$type){
    	if ($this->s_login && !empty($type)){
			$data = array();
			$status = '';
			$message = '';
			if($type == 'create_zone'){
				$id = $this->input->post('ccs_id');
				$ccskey = $this->module_key;
				$name = strtolower($this->input->post('name'));
				
				if($id) //update
				{
					if ($this->form_validation->run('ccs_zone') === TRUE){
						if(isset($_FILES['logo'])){
							$datas = $this->update_data_bys('*', array('ccs_id' => $id), 'ccs_zone','logo',''.$name.'','logo','jpeg|jpg|png');
						}else{
							$datas = $this->update_data_bys('*', array('ccs_id' => $id), 'ccs_zone','','','','');
						}
						
						$this->dbfun_model->update_table(array('ccs_id' => $id),$datas,'zone');
						$status = 'success';
						$message = 'Data updated successfully';
					}else{
							$status = 'error';
							$message = validation_errors();
					}
				}else{ //create
					if ($this->form_validation->run('ccs_zone') === TRUE)
					{
						$custom_primary = array(
									'status' => 1,
									'ccs_key' => 3
								);
						$create_new = $this->new_data('ccs_zone',$custom_primary, '','', '','');
						
						//$this->dbfun_model->ins_to('ccs_zone', $create_new);
						if (!is_dir('assets/'.$name))
						{
							mkdir('./assets/'.$name, 0777, true);
						}
						$status = 'success';
						$message = 'Data updated successfully';
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				
			echo json_encode(array('status' => $status, 'm' => $message));
				
			}elseif( $type == 'create_module'){
					
				$this->form_validation->set_rules('id', 'Zone Status', 'trim|numeric|xss_clean');
				$id = $this->input->post('id');
				$param = $this->input->post('param');
				
				if($param == 'ccs'){
					if($id == 0){
						if ($this->form_validation->run('ccs_mdl') === TRUE)
						{
							$parent = $this->input->post('parent_id') ;
							if( $parent != 0){
								$title = $this->dbfun_model->get_data_bys('ccs_mdl.name','ccs_mdl.module_id = '.$parent.' ','mdl')->name;
							}else{
								$title = '';
							}
							$custom_primary = array(
										'ccs_id' => 0,
										'pname' => $title,
										'ccs_key' => 12, 
										'createdby' => $this->admin_id, 
										'status' => 1
									);
							$create_new = $this->new_data('ccs_mdl',$custom_primary,'','','','');
							
							//$this->dbfun_model->ins_to('mdl', $create_new);
							
							$status = 'success';
							$message = 'Data created successfully';
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}elseif($param == 'zone'){
					if ($this->form_validation->run('ccs_mdl') === TRUE){
						$parent = $this->input->post('parent_id') ;
						$name = $this->dbfun_model->get_data_bys('ccs_mdl.name','ccs_mdl.name LIKE "'.$this->input->post('name').'" and ccs_mdl.ccs_id = '.$id.' ','mdl');
						if( !$name && ( $parent == 0)){
							if( $parent != 0){
								$title = $this->dbfun_model->get_data_bys('ccs_mdl.name','ccs_mdl.module_id = '.$parent.' ','mdl')->name;
							}else{
								$title = '';
							}
							$custom_primary = array(
										'ccs_id' => $id,
										'pname' => $title, 
										'ccs_key' => 12, 
										'createdby' => $this->admin_id, 
										'status' => 1
									);
							$value = $this->new_data('ccs_mdl',$custom_primary,'','','','');
							//$value = $this->dbfun_model->ins_return_id('mdl', $create_new);
							
							$custom_mtr = array(
										'ccs_id' => $id,
										'module_id' => $value, 
										'createdby' => $this->admin_id, 
										'status' => 1,
										'role_id' => 1
									);
							$this->new_data('ccs_mtr',$custom_mtr,'','','','');
							//$values = $this->dbfun_model->ins_return_id('mtr', $create_news);
							
							$status = 'success';
							$message = 'Data created successfully';
						}elseif( $parent == 0){
							if( $name){
								$status = 'error';
								$message = 'The menu name already exist, please create other name. ';
							}else{
								$custom_primary = array(
										'ccs_id' => $id,
										'createdby' => $this->admin_id, 
										'status' => 1
									);
								$value = $this->new_data('ccs_mdl',$custom_primary,'','','','');
								//$value = $this->dbfun_model->ins_return_id('mdl', $create_new);		
								
								$custom_mtr = array(
											'ccs_id' => $id,
											'module_id' => $value, 
											'createdby' => $this->admin_id, 
											'status' => 1,
											'role_id' => 1
										);
								$this->new_data('ccs_mtr',$custom_mtr,'','','','');
																
								$status = 'success';
								$message = 'Data created successfully';
							}								
						}elseif($parent != 0){
							if( $name && ( $id < 2)){
								$status = 'error';
								$message = 'The menu name already exist, please create other name. ';
							}else{
								$title = $this->dbfun_model->get_data_bys('ccs_mdl.name','ccs_mdl.module_id = '.$parent.' ','mdl')->name;
								$custom_primary = array(
										'ccs_id' => $id,
										'pname' => $title,
										'ccs_key' => 12, 
										'createdby' => $this->admin_id, 
										'status' => 1
									);
								$value = $this->new_data('ccs_mdl',$custom_primary,'','','','');
								//$value = $this->dbfun_model->ins_return_id('mdl', $create_new);		
								
								$custom_mtr = array(
											'ccs_id' => $id,
											'module_id' => $value, 
											'createdby' => $this->admin_id, 
											'status' => 1,
											'role_id' => 1
										);
								$this->new_data('ccs_mtr',$custom_mtr,'','','','');
																
								$status = 'success';
								$message = 'Data created successfully';
							}								
						}else{
							$status = 'error';
							$message = 'The menu name already exist, please create other name. ';
						}							
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}else{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));			
			}elseif($type == 'update_module'){
				$id = $this->input->post('url_id');
				$ccskey = $this->input->post('ccs_key');
				$parent = $this->input->post('parent_id');
				if ($this->form_validation->run('ccs_mdl') === TRUE){					
					$datas = $this->update_data_bys('*', array('module_id' => $id), 'ccs_mdl','','','','');
					if($parent == 0){
						$datas['pname'] = ' ' ;
					}else{
						$datas['pname'] = $this->dbfun_model->get_data_bys('name','module_id = '.$parent.'','mdl')->name;
					}
					$this->dbfun_model->update_table(array('module_id' => $id),$datas,'mdl');
					$status = 'success';
					$message = 'Data updated successfully';
				}else{
					$status = 'error';
					$message = validation_errors();
				}				
				echo json_encode(array('status' => $status, 'm' => $message));
				
			}elseif($type == 'edit_module'){
				$id = $this->input->post('url_id');
				$ccskey = $this->input->post('ccs_key');
				$parent = $this->input->post('parent_id');
				if ($this->form_validation->run('ccs_umd') === TRUE){					
					$datas = $this->update_data_bys('*', array('module_id' => $id), 'ccs_umd','','','','');
					if($parent == 0){
						$datas['pname'] = ' ' ;
					}else{
						$datas['pname'] = $this->dbfun_model->get_data_bys('name','module_id = '.$parent.'','umd')->name;
					}
					$this->dbfun_model->update_table(array('module_id' => $id),$datas,'umd');
					$status = 'success';
					$message = 'Data updated successfully';
				}else{
					$status = 'error';
					$message = validation_errors();
				}				
				echo json_encode(array('status' => $status, 'm' => $message));
				
			}elseif($type == 'create_user'){
				$id = $this->input->post('type_id');
				$ccs_id = $this->input->post('ccs_id');
				$name = strtolower(str_replace(' ','_',$this->input->post('name')));
				$param = $this->input->post('param');
				$ccs_name = strtolower($this->dbfun_model->get_data_bys('name','ccs_id = '.$ccs_id.'','ccs_zone')->name);
				if($param == 'type') //user_type
				{
					if($id) //update
					{
						if ($this->form_validation->run('ccs_umr') === TRUE){
							
							$datas = $this->update_data_bys('*', array('ccs_id' => $ccs_id,'type_id' => $id), 'ccs_umr','','','','');
							
							if(!empty($datas)){
								$name_before = strtolower(str_replace(' ','_',$this->dbfun_model->get_data_bys('name','ccs_id = '.$ccs_id.' and type_id = '.$id.'','ccs_umr')->name));
								if (!is_dir('assets/'.$ccs_name.'/user'))
								{
									mkdir('./assets/'.$ccs_name.'/user', 0777, true);
								}
								if(strpos($name_before,$name) === true){
									if (!is_dir('assets/'.$ccs_name.'/user/'.$name))
									{
										mkdir('./assets/'.$ccs_name.'/user/'.$name, 0777, true);
									}
								}else{
									if (is_dir('assets/'.$ccs_name.'/user/'.$name_before))
									{
										rename('./assets/'.$ccs_name.'/user/'.$name_before,'./assets/'.$ccs_name.'/user/'.$name);
									}else{
										mkdir('./assets/'.$ccs_name.'/user/'.$name, 0777, true);
									}
								}
								
								$this->dbfun_model->update_table(array('ccs_id' => $ccs_id,'type_id' => $id),$datas,'umr');
								$status = 'success';
								$message = 'Data updated successfully';
							}else{
								$status = 'success';
								$message = 'No Data updated';
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}else{ //create
						if ($this->form_validation->run('ccs_umr') === TRUE)
						{
							$custom_primary = array(
										'status' => 1,
										'type_id' => $this->dbfun_model->get_max_data_where('max(type_id) as type_id','ccs_id = '.$ccs_id.'','ccs_umr')->type_id + 1
									);
							$create_new = $this->new_data('ccs_umr',$custom_primary, '','', '','');
							
							if (!is_dir('assets/'.$ccs_name.'/user'))
							{
								mkdir('./assets/'.$ccs_name.'/user', 0777, true);
							}
							
							if (!is_dir('assets/'.$ccs_name.'/user/'.$name))
							{
								mkdir('./assets/'.$ccs_name.'/user/'.$name, 0777, true);
							}
							$status = 'success';
							$message = 'Data updated successfully';
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
				}elseif($param == 'module'){
					if ($this->form_validation->run('ccs_umd') === TRUE){
						$parent = $this->input->post('parent_id') ;
						$tu = $this->input->post('tu_id') ;
						$name = $this->dbfun_model->get_data_bys('ccs_umd.name','ccs_umd.name LIKE "'.$this->input->post('name').'" and ccs_umd.ccs_id = '.$ccs_id.' and ccs_umd.tu_id = '.$tu.'','umd');
						if( !$name && ( $parent == 0)){
							$custom_primary = array(
								'ccs_id' => $ccs_id,
								'createdby' => $this->admin_id, 
								'status' => 1
							);
							$value = $this->new_data('ccs_umd',$custom_primary,'','','','');
							
							$custom_mtr = array(
								'ccs_id' => $ccs_id,
								'module_id' => $value, 
								'createdby' => $this->admin_id, 
								'type_id' => $tu,
								'status' => 1,
								'role_id' => $this->input->post('role_id')
							);
							$this->new_data('ccs_umt',$custom_mtr,'','','','');
							
							$status = 'success';
							$message = 'Data created successfully';							
						}elseif(!$name && ( $parent != 0)){
							$title = $this->dbfun_model->get_data_bys('name','ccs_id = '.$ccs_id.' and tu_id = '.$tu.' and module_id = '.$parent.'','umd');
							$custom_primary = array(
								'ccs_id' => $ccs_id,
								'createdby' => $this->admin_id, 
								'parent_id' => $parent,
								'pname' => $title->name,
								'status' => 1
							);
							$value = $this->new_data('ccs_umd',$custom_primary,'','','','');
							
							$custom_mtr = array(
								'ccs_id' => $ccs_id,
								'module_id' => $value, 
								'createdby' => $this->admin_id, 
								'type_id' => $tu,
								'status' => 1,
								'role_id' => $this->input->post('role_id')
							);
							$this->new_data('ccs_umt',$custom_mtr,'','','','');
							
							$status = 'success';
							$message = 'Data created successfully';	
						}else{
							$status = 'error';
							$message = 'The menu name already exist, please create other name. ';
						}							
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				
			}elseif($type == 'create_lang'){
				$id = $this->input->post('zol_id');
				$ccs_id = $this->input->post('ccs_id');
				$param = $this->input->post('param');
				if($param == 'zone') //zone_language
				{
					$this->form_validation->set_rules('ccs_id', 'CCS ID', 'required');
					$this->form_validation->set_rules('lang_id', 'Language', 'required');
					$this->form_validation->set_rules('action', 'Action', 'required');
					
					if ($this->form_validation->run() === TRUE)//create and delete
					{
						$action = $this->input->post('action');
						$langs = $this->input->post('lang_id');
						$cek_langs = $this->dbfun_model->get_all_data_bys('lang_id','ccs_id = '.$ccs_id.'','zol'); //cek lang
						if(!empty($cek_langs)){
							foreach($cek_langs as $cek_lang){
								$cek[] = $cek_lang->lang_id;
							}
						}else{
							$cek = '';
						}
						if(strpos($langs,',') !== false){//cek if the value from post is array
							$ids = explode(',',$langs);
							
							foreach($ids as $lang){
								if(!in_array($lang, $cek)){
									$custom_primarys = array(
										'status' => 1,
										'ccs_id' => $ccs_id,
										'lang_id' => $lang,
									);
									$this->new_data('ccs_zol',$custom_primarys, '','', '','');
								}else{
									//no new language added to this zone coz already have it
								}
							}
							
						}else{
							if(!in_array($langs, $cek)){
								$custom_primary = array(
									'status' => 1,
									'ccs_id' => $ccs_id,
									'lang_id' => $langs,
								);
								$this->new_data('ccs_zol',$custom_primary, '','', '','');
							}else{
								//no new language added to this zone coz already have it
							}
						}
						
						if($action == 'add'){
							
						}elseif($action == 'update'){
							//delete data if not in the value from post
							$del_langs = $this->dbfun_model->get_all_data_bys('lang_id','ccs_id = '.$ccs_id.' and lang_id NOT IN ('.$langs.')','zol'); //cek lang
							if(!empty($del_langs)){
								foreach($del_langs as $dl){
									$this->dbfun_model->del_tables('ccs_id = '.$ccs_id.' and lang_id = '.$dl->lang_id.'','zol');
								}
							}			
							if($this->input->post('active_lang')){
								$datas = $this->update_data_bys('*', array('ccs_id' => $ccs_id), 'ccs_zone','','','','');
								$this->dbfun_model->update_table(array('ccs_id' => $ccs_id),$datas,'zone');
							}
						}
						
						$status = 'success';
						$message = 'Data Created successfully';
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				
			}else{
				redirect($this->login);
			}
		}
    }
	
	public function status($zone, $type){
		if ($this->s_login && !empty($type)){
			$data = array();
    		$status = '';
    		$message = '';
			
			$this->form_validation->set_rules('id', 'Zone Status', 'trim|required|numeric|xss_clean');
			$id = $this->input->post('id');
			$param = $this->input->post('param');
			$action = $this->input->post('status');
			if ($this->form_validation->run() === TRUE){
				if($type == 'zone'){
					$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
					$update = array(
						'status' => $s
					);
					$this->dbfun_model->update_table(array('ccs_id' => $id), $update, 'zone');
					
					
					$status = 'success';
					$message = 'Data updated successfully';
				}elseif($type == 'module'){
					$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
					$update = array(
						'status' => $s
					);
					$this->dbfun_model->update_table(array('module_id' => $id), $update, 'mdl');
					
					
					$status = 'success';
					$message = 'Data updated successfully';
				}elseif($type == 'user_module'){
					$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
					$update = array(
						'status' => $s
					);
					$this->dbfun_model->update_table(array('module_id' => $id), $update, 'umd');
					
					
					$status = 'success';
					$message = 'Data updated successfully';
				}elseif($type == 'language'){
					$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
					$update = array(
						'status' => $s
					);
					$this->dbfun_model->update_table(array('zol_id' => $id), $update, 'zol');
					
					
					$status = 'success';
					$message = 'Data updated successfully';
				}
			}else{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
		}
	}
	
	public function auto_create($zone,$type){
		if ($this->s_login && !empty($type)){
			$data = array();
			$param = $this->input->post('param');
			$param2 = $this->input->post('param2');
			$status = 'error';
			$message = 'error';
			if($type == 'detail'){
				$ccs_id = $this->input->post('ccs_id');
				$type_id = $this->input->post('type_id');
				$layer = $this->input->post('layer');
				$origin =  $this->input->post('ccs_id_origin');
				$v = '';
				if($param == 'module'){
					if($layer == 1){ //list all zone based on type_id
						$v = 'list';
						$status = 'success';
						$module_before = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$origin.'','mdl');
						if(empty($module_before)){
							$data = $this->dbfun_model->get_all_data_bys('ccs_id,name','type_id = '.$type_id.' and status = 1','zone');
						}else{
							$status = 'error';
							$v = 'delete';
						}
					}elseif($layer == 2){ //list all modul from ccs_id
						$menu = array();
						$v = 'list';
						$status = 'success';
						$menu_parent = $this->dbfun_model->get_all_data_bys('distinct(ccs_mdl.module_id), mdl.parent_id, mdl.icon, mdl.name,mdl.link,mdl.status','ccs_mdl.ccs_id = '.$ccs_id.' and ccs_mdl.parent_id = 0 order by ccs_mdl.module_id ASC','mdl');
						foreach($menu_parent as $mp){
							$menu_child = $this->dbfun_model->get_all_data_bys('distinct(ccs_mdl.module_id), mdl.parent_id, mdl.icon, mdl.name, mdl.link,mdl.status','ccs_mdl.status !=0 and ccs_mdl.parent_id = '.$mp->module_id.'','mdl');
							if(!empty($menu_child)){
								$menu[] = (object) array_merge((array) $mp, array('children' => $menu_child));
							}else{
								$menu[] = $mp;
							}
							
						}
						$data = $menu;
					}
				}elseif($param == 'category'){
					$menu = array();
					$v = 'list';
					$ccs_id = 3;
					$module = array('web_settings','sitemap');
					$status = 'success';
					foreach($module as $m){
						$ceks = $this->dbfun_model->get_data_bys('module_id','ccs_mdl.status != 0 and ccs_mdl.ccs_id = '.$ccs_id.' and ccs_mdl.link like "'.$m.'"','mdl');
						if($ceks){
							$ccs_key = $ceks->module_id;
						}else{
							$ccs_key = FALSE;
						}
						if($ccs_key){
							$cek = $this->dbfun_model->get_all_data_bys('category_id as module_id,parent_id,title as name,parent_title','ccs_oct.ccs_id = '.$ccs_id.' and ccs_oct.ccs_key = '.$ccs_key.' and parent_id = 0 and ccs_oct.status != 0','oct');
							foreach($cek as $c){
								$total = '';
								$cek2 = $this->dbfun_model->get_all_data_bys('category_id as module_id,parent_id,title as name,parent_title','ccs_oct.ccs_id = '.$ccs_id.' and parent_id = '.$c->module_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.status != 0','oct');
								$total_child = 0;
								$cc2 = array();
								foreach($cek2 as $c2){
									$total2 = '';
									$cc2[] = (object) array_merge((array) $c2, array('sum' => $total2));
									$total_child += $total2;
								}
								if(isset($total_child)){
									$total = $total + $total_child;
								}
								if(!empty($cc2)){
									$cc[] = (object) array_merge((array) $c, array('sum' => $total));
								}else{
									$cc[] = (object) array_merge((array) $c, array('sum' => $total));
								}
							}
						}
					}
					
					if(!empty($cc)){
						$menu = $cc;
					}else{
						$menu = array();
					}
					
					$data = $menu;
				}elseif($param == 'admin'){
					$cek = $this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$origin.'','adm');
					$module_before = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$origin.'','mdl');
					if(empty($module_before)){
						$status = 'error';
						$v = 'module';
					}else{
						if(count($cek) == 0){
							$status = 'success';
							$v = 'create';
						}else{
							$status = 'error';
							$v = 'delete';
						}
					}
					
				}
				
				echo json_encode(array('status' => $status, 'data' => $data, 'type'=> $v));
			}else{
				$ccs_id = $this->input->post('ccs_id');
				$origin =  $this->input->post('ccs_id_origin');
				$zone_name = trim(strtolower($this->dbfun_model->get_data_bys('name','ccs_id = '.$origin.'','zone')->name));
				$status = 'success';
				if($param == 'module'){
					if($type == 'create'){						
						$data = json_decode($this->input->post('nestable-output'),false);
						$message = 'Data created successfully';
						if(!empty($data)){
							$module_before = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$origin.'','mdl');
							if(empty($module_before)){
								foreach($data as $m){
									$cek = $this->dbfun_model->get_data_bys('icon,parent_id,type_id, type_view,ftype,','ccs_id = '.$ccs_id.' and module_id = '.$m->module.'','mdl');
									if($cek){
										$custom_primary = array(
											'ccs_id' => $origin,
											'name' => $m->title,
											'parent_id' => 0,
											'pname' => '',
											'link' => $m->link,
											'icon' => $cek->icon, 
											'type_id' => $cek->type_id, 
											'type_view' => $cek->type_view, 
											'ftype' => $cek->ftype, 
											'createdby' => $this->admin_id, 
											'status' => 1
										);
										$value = $this->new_data('ccs_mdl',$custom_primary,'','','','');
										if (!is_dir('assets/'.$zone_name.'/'.$m->link))
										{
											mkdir('./assets/'.$zone_name.'/'.$m->link, 0777, true);
										}
										if(isset($m->children)){
											foreach($m->children as $mc){
												$custom_primary = array(
													'ccs_id' => $origin,
													'name' => $mc->title,
													'parent_id' => $value,
													'pname' => $m->title,
													'link' => $mc->link,
													'icon' => $cek->icon, 
													'type_id' => $cek->type_id, 
													'type_view' => $cek->type_view, 
													'ftype' => $cek->ftype, 
													'createdby' => $this->admin_id, 
													'status' => 1
												);
												$this->new_data('ccs_mdl',$custom_primary,'','','','');
												if (!is_dir('assets/'.$zone_name.'/'.$mc->link))
												{
													mkdir('./assets/'.$zone_name.'/'.$mc->link, 0777, true);
												}
											}
										}
									}else{
										$status = "error";
										$message = 'Please choose a valid data';
									}
								}
							}else{
								$status = "error";
								$message = "Please Delete ALL MODULE first";
							}
						}else{
							$status = 'error';
							$message = "Please choose list first";
						}
						
					}elseif($type == 'delete'){
						$status = 'success';
						$message = 'Data deleted successfully';
					}elseif($type == 'update'){
						$status = 'success';
						$message = 'Data updated successfully';
					}else{
						$status = 'success';
						$message = 'Data updated successfully';
					}
				}elseif($param == 'admin'){
					$message = 'Data created successfully';
					if($type == 'create'){
						$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean');
						$this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
						$this->form_validation->set_rules('password2', 'Confirmation', 'required|trim|xss_clean');
						
						if ($this->form_validation->run() === TRUE)
						{
							$u = trim($this->input->post('username'));
							$p = trim($this->input->post('password'));
							$p2 = trim($this->input->post('password2'));
							$real_pass = MD5($p);
							if($p == $p2){
								$role_id = 0; //superadmin
								$cek_mdr = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$origin.'','mdr');
								if(empty($cek_mdr)){
									$custom_mdr = array(
										'ccs_id' => $origin,
										'type_id' => 0,
										'tu_id' => 0,
										'name' => 'superadmin',
										'description' => 'Role untuk super admin '.$zone_name.'',
										'createdby' => 0,
										'status' => 1
									);
									$this->new_data('ccs_mdr',$custom_mdr,'','','','');
								}else{
									$role_id = $cek_mdr->type_id;
								}
								
								$custom_adm = array(
									'ccs_id' => $origin,
									'role_id' => $role_id,
									'lang_id' => 2,
									'username' => $u,
									'password' => $real_pass,
									'email' => '',
									'gender' => 0,
									'name' => $u, 
									'phone' => '', 
									'mobile' => '', 
									'address' => '', 
									'description' => 'description detail about this admin account', 
									'createdby' => 0, 
									'status' => 1
								);
								$admin_id = $this->new_data('ccs_adm',$custom_adm,'','','','');
								
								if(empty($cek_mdr)){
									$module = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$origin.'','mdl');
									foreach($module as $m){
										$custom_mtr = array(
											'role_id' => $role_id,
											'ccs_id' => $origin,
											'module_id' => $m->module_id,
											'createdby' => $admin_id,
											'status' => 1
										);
										$this->new_data('ccs_mtr',$custom_mtr,'','','','');
									}
								};
							}else{
								$status = 'error';
								$message = "Password doesn't match";
							}
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
			}
			
		}else{
			redirect($this->login);
		}
	}
	
	public function remove($zone,$type){
		if ($this->s_login && !empty($type)){
			$data = array();
			$ccs_id = $this->input->post('ccs_id');
			$param = $this->input->post('param');
			$param2 = $this->input->post('param2');
			$status = 'error';
			$message = 'error';
			$datas = array();
			$delets = array();
			$module = $this->dbfun_model->get_data_bys('*','ccs_id = '.$ccs_id.' and module_id = '.$param.'','mdl');
			
			if(!empty($module)){
				$module_name = $module->link;
				switch($module_name)
				{
					case 'transaction':
					case 'transactions':
						$delets = array(
							0 => array(
									'where'=> 'ccs_id = '.$ccs_id.' and ccs_key = '.$param.'',
									'table'=> 'ccs_rcp',
								),
							1 => array(
									'where'=> 'ccs_id = '.$ccs_id.' and ccs_key = '.$param.'',
									'table'=> 'ccs_trx',
								),
							2 => array(
									'where'=> 'ccs_id = '.$ccs_id.' and ccs_key = '.$param.'',
									'table'=> 'ccs_inv',
								),
						);
					break;
					case 'news':
						$status = 'success';
					break;
					case 'event':
						$status = 'success';
					break;
					case 'product':
						$status = 'success';
					break;
					default: 
						$status = 'error';
						$message = 'No Function Added';
					break;
				};
				if(!empty($delets)){
					$status = 'success';
					$message = 'Isinya dah di hapus semua jd bersih';
					
					foreach($delets as $d){
						$this->dbfun_model->del_tables($d['where'],$d['table']);
					}
				}else{
					$status = 'error';
					$message = 'No Function Added';
				}
			}
			echo json_encode(array('status' => $status, 'm' => $message, 'data'=> $datas));
		}
	}
}