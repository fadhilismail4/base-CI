<div class="row">
	<div class="col-md-3">
		<button id="profile_view_add_new" type="button" class="menu btn btn-primary col-md-1 col-xs-2 hide">Add New</button>
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Profile Detail</h5>
			</div>
			<div>
				<div class="ibox-content no-padding border-left-right">
					<img alt="image" class="img-responsive" src="<?php echo base_url();?><?php if(!empty($profile->main_image)){ echo 'assets/img/avatar/admin/'.$profile->main_image;}else{ echo 'assets/img/admin_empty.jpg';}?>" style="width: 100%;">
				</div>
				<div class="ibox-content profile-content">
					<p class="pull-right"><strong>Superadmin</strong></p>
					<h4><strong id="name" name="inputan"><?php echo $profile->name;?></strong></h4>
					<p><i class="fa fa-map-marker"></i> <?php if(!empty(trim($profile->address))){echo trim($profile->address);}else{echo '-';};?></p>
					<h5>
						About me
					</h5>
					<p>
						<?php if(!empty(trim($profile->description))){echo trim($profile->description);}else{echo '-';};?>
					</p>
					<div class="user-button">
						<div class="row">
								<button id="<?php echo $profile->admin_id;?>" data-url="profile" data-url2="detail" type="button" class="detail btn btn-primary btn-sm pull-right" style="margin:0 5px"><i class="fa fa-pencil"></i> Edit</button>
								<button id="<?php echo $profile->admin_id;?>" data-url="profile" data-url2="log" type="button" class="detail btn btn-primary btn-sm pull-right" style="margin:0 5px"><i class="fa fa-clock-o"></i> Last Activity</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if(!empty($other_profile)){
			foreach($other_profile as $op){?>
		<!--<div class="contact-box">
			<div class="col-sm-4">
				<div class="text-center">
					<img alt="image" class="img-circle m-t-xs img-responsive" src="<?php echo base_url();?><?php if(!empty($op['profile']->main_image)){ echo 'assets/img/avatar/admin/'.$op['profile']->main_image;}else{ echo 'assets/img/admin_empty.jpg';}?>" style="width: 100%;">
					<div class="m-t-xs font-bold"><?php if($op['role']){echo $op['role'];}else{echo '-';}?></div>
				</div>
			</div>
			<div class="col-sm-8">
				<?php if($op['profile']->status == 1){echo '<span class="label label-info pull-right">Active</span>';}else{echo '<span class="label label-danger pull-right">Inactive</span>';}?>
				<h3><strong><?php echo $op['profile']->name;?></strong></h3>
				<p><i class="fa fa-map-marker"></i> <?php if($op['profile']->address){echo $op['profile']->address;}else{echo '-';}?></p>
				<address>
					<abbr title="Email"><i class="fa fa-envelope-o"></i></abbr> <?php if($op['profile']->email){echo $op['profile']->email;}else{echo '-';}?>
					<br>
					<abbr title="Phone"><i class="fa fa-phone"></i></abbr>  <?php if($op['profile']->phone){echo $op['profile']->phone;}else{echo '-';}?>
					<br>
					<abbr title="Mobile"><i class="fa fa-mobile-phone" style="font-size: 20px;"></i></abbr> <?php if($op['profile']->mobile){echo $op['profile']->mobile;}else{echo '-';}?>
				</address>
			</div>
			<div class="user-button">
				<div class="row">
					<button id="<?php echo strtolower($child);?>" data-detail="<?php echo $op['profile']->admin_id;?>" type="button" class="delete btn btn-danger btn-sm pull-right" style="margin:0 5px"><i class="fa fa-times"></i> Inactive</button>
					<button id="<?php echo strtolower($child);?>" data-detail="<?php echo $op['profile']->admin_id;?>" type="button" class="detail btn btn-primary btn-sm pull-right" style="margin:0 5px"><i class="fa fa-pencil"></i> Edit</button>
					<button id="profile_timeline" data-detail="<?php echo $op['profile']->admin_id;?>" type="button" class="detail btn btn-primary btn-sm pull-right" style="margin:0 5px"><i class="fa fa-clock-o"></i> Last Activity</button>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>-->
		<?php ;} 
		}?>
	</div>
	<div class="col-md-9 detail_content">
		
	</div>
</div>
