 <div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Last activity</h5>
	</div>
	<div class="ibox-content">
		
		<ul class="list-group clear-list">
		<?php if(!empty($log)){;?>
			<?php $i=1;foreach($log as $lg){
				if(($lg['type'] == 1) && ($lg['module_id'] != 0)){
					$info = '<span class="pull-right"> <span class="label label-warning">CREATE</span> </span>';
				}elseif($lg['type'] == 2){
					$info = '<span class="pull-right"> <span class="label label-primary">READ</span> </span>';
				}elseif($lg['type'] == 3){
					$info = '<span class="pull-right"> <span class="label label-success">UPDATE</span> </span>';
				}elseif($lg['type'] == 4){
					$info = '<span class="pull-right"> <span class="label label-danger">DELETE</span> </span>';
				}else{
					$info = '';
				}
				?>
				<li class="list-group-item <?php if($i == 1){echo 'fist-item';}?>">
					<?php echo $info;?>
					<strong><?php echo $lg['module'];?></strong> <br><small><?php echo date('d-M-Y H:i:s', strtotime($lg['datecreated']));?></small>
				</li>
			<?php ;$i++;}?>
		<?php }else{ ?>
		<i>Doesn't have any recent activity</i>	
		</ul>
		
		<?php } ?>
		<strong>Notes</strong>
		<p id="<?php echo $this->session->userdata('adm_admin_id');?>" data-url="profile" data-url2="detail" class="detail">
			Please full fill your personal account <i class="fa fa-arrow-circle-o-right"></i>
		</p>
	</div>
</div>