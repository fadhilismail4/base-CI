 <div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Role</h5>
	</div>
	<div class="ibox-content">
		<ul class="sortable-list connectList agile-list ui-sortable">
			<?php foreach($role as $r){;?>
			<li class="info-element">
				<h5><?php echo $r['name'];?> <small><?php echo $r['desc'];?></small></h5>
				<div class="agile-detail">
					<?php foreach($r['module'] as $m){;?>
						<small class="label label-plain"><?php echo $m->name;?></small>
					<?php } ?>
					<button id="role_view_add_new" data-id="<?php echo $r['role_id'];?>" data-lang="2" class="btn btn-xs btn-white pull-right view_update"><i class="fa fa-pencil"> EDIT</i></button>
				</div>
			</li>
			<?php } ?>
		</ul>
	</div>	
</div>	
<script>
$(document).ready(function () {
	$('#button-action').empty();
	$('#button-action').append('<button id="role_view_add_new" type="button" class="menu btn btn-primary col-md-1 col-xs-2">Add New</button>');
});
</script>