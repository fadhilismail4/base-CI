<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Profile</h5>
	</div>
	<p id="admin_id" name="inputan" class="hide"><?php echo trim($profile->admin_id);?></p>
	<div class="ibox-content">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Full Name</label>
				<div class="col-sm-10"><p id="name" name="inputan" class="form-control-static"><?php echo trim($profile->name);?></p></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">E-mail</label>
				<div class="col-sm-10"><p id="email" name="inputan" class="form-control-static"><?php echo trim($profile->email);?></p></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Phone Number</label>
				<div class="col-sm-10"><p id="phone" name="inputan" class="form-control-static"><?php echo trim($profile->phone);?></p></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Mobile Number</label>
				<div class="col-sm-10"><p id="mobile" name="inputan" class="form-control-static"><?php echo trim($profile->mobile);?></p></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Address</label>
				<div class="col-sm-10"><p id="address" name="inputan" class="form-control-static"><?php if(!empty($profile->address)){echo trim($profile->address);};?></p></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Date of Birth</label>
				<div class="col-sm-10"><p id="" name="" class="form-control-static"><?php echo date('d-M-Y', strtotime($profile->birthday));?></p>
				<p id="birthday" name="inputan" class="hide"><?php echo $profile->birthday;?></p></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Gender</label>
				<div class="col-sm-10"><p id="" name="" class="form-control-static"><?php if($profile->gender == 1){echo 'Male';}elseif($profile->gender == 2){ echo 'Female';}else{ echo '-';}?></p></div><p id="gender" name="inputan" class="hide"><?php echo trim($profile->gender);?></p>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Role</label>
				<div class="col-sm-10"><p id="" name="" class="form-control-static"><?php echo $role->name;?></p></div><p id="role_id" name="inputan" class="hide"><?php echo trim($profile->role_id);?></p>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Status</label>
				<div class="col-sm-10"><p id="" name="" class="form-control-static"><?php if($profile->status == 1){echo 'Active';}else{ echo 'Inactive';}?></p></div>
				<p id="status" name="inputan" class="hide"><?php echo trim($profile->status);?></p>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">About me</label>
				<div class="col-sm-10">
					<p id="description" name="inputan" class="form-control-static"><?php echo trim($profile->description);?></p>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-6 col-sm-offset-2">
					<button href="" class="btn btn-white" class="btn btn-white" type="submit">Cancel</button>
					<button id="profile_view_add_new" class="view_update btn btn-primary" type="submit">Edit</button>
					<button id="<?php echo $profile->admin_id;?>" data-url="profile" data-url2="change_password" class="detail btn btn-success" type="submit">Change Password</button>
					<button id="<?php echo $profile->admin_id;?>" data-lang="" data-url="profile" data-title="<?php echo trim($profile->name);?>" class="btn btn-danger modal_delete" title="Delete"><i class="fa fa-trash-o"></i> DELETE</button>
				</div>
			</div>
		</form>
	</div>
</div>