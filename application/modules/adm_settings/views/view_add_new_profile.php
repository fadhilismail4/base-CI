<link href="<?php echo base_url('assets');?>/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<input id="admin_id" name="inputan" type="hidden" class="form-control" value="">
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Profile</h5>
	</div>
	<div class="ibox-content">
		<form class="form-horizontal">
			<div class="alert alert-danger" id="fail" style="display:none;"></div>
			<div class="alert alert-info" id="success" style="display:none;"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Full Name</label>
				<div class="col-sm-10"><input id="name" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group"><label class="col-sm-2 control-label">Gender</label>
				<div class="col-sm-10">
					<select id="gender" name="inputan" class="form-control m-b">
						<option value="">Gender</option>
						<option value="1">Male</option>
						<option value="2">Female</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Date of Birth</label>
				<div class="col-sm-10">
					<div class="input-group date">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input id="birthday" name="inputan" data-date-format="yyyy-mm-dd" type="text" class="form-control">
					</div>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Phone Number</label>
				<div class="col-sm-10"><input id="phone" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Mobile Number</label>
				<div class="col-sm-10"><input id="mobile" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">E-mail</label>
				<div class="col-sm-10"><input id="email" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Address</label>
				<div class="col-sm-10"><input id="address" name="inputan" type="text" class="form-control" value=""></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group"><label class="col-sm-2 control-label">Role</label>
				<div class="col-sm-10">
					<select id="role_id" name="inputan" class="form-control m-b">
						<option value="">role</option>
						<?php foreach($role as $r){?>
						<option value="<?php echo trim($r->role_id);?>"><?php echo $r->name;?></option>
						<?php };?>
					</select>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">About me</label>
				<div class="col-sm-10">
					<textarea id="description" name="inputan" type="text" class="form-control" rows="5" maxlength="200"></textarea>
				</div>
			</div>
			<div class="form-group"><label class="col-sm-2 control-label">Status</label>
				<div class="col-sm-10">
					<select id="status" name="inputan" class="form-control m-b" name="account">
						<option value="">Status</option>
						<option value="1">Active</option>
						<option value="2">Inactive</option>
					</select>
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<button class="btn btn-white" type="submit">Cancel</button>
					<button id="profile" class="create btn btn-primary" type="submit">Create</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Data picker -->
<script src="<?php echo base_url('assets');?>/admin/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('assets');?>/admin/js/ajaxfileupload.js"></script>
<script>
$(document).ready(function(){
	$('#birthday').datepicker({
		keyboardNavigation: false,
		todayBtn: "linked",
		format: 'yyyy-mm-dd',		
		forceParse: false
	});
});
</script>