<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Change Password</h5>
	</div>
	<div class="ibox-content">
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Current Password</label>
				<div class="col-sm-10"><input id="c_password" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">New Password</label>
				<div class="col-sm-10"><input id="old_password" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Confirmation Password</label>
				<div class="col-sm-10"><input id="cf_password" name="inputan" type="text" class="form-control"></input></div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-sm-6 col-sm-offset-2">
					<button id="profile" class="menu btn btn-white" class="btn btn-white" type="submit">Cancel</button>
					<button id="change_password" data-detail="<?php echo $this->session->userdata('adm_admin_id');?>" class="create btn btn-success" type="submit">Change Password</button>
				</div>
			</div>
		</form>
	</div>
</div>