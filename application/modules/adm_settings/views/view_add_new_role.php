<link href="<?php echo base_url('assets');?>/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
<div class="row">
	<div class="col-lg-9">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Role</h5>
			</div>
			<div class="ibox-content">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<form class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label">Title:</label>

										<div class="col-sm-10">
											<input id="title" name="inputan" type="text" class="form-control" value="">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Description:</label>

										<div class="col-sm-10">
											<input id="desc" name="inputan" type="text" class="form-control" value="">
										</div>
									</div>
								</form>	
							</div>
							<div class="col-md-12">
								<form class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label">Module:</label>
										<input id="module" name="inputan" type="hidden" value=""></input>
										<div class="col-sm-10">
											<?php foreach($module as $m){?>
												<?php if($m->parent_id == 0){;?> 
													<h5 style="margin-top: 10px;"><strong><?php echo $m->name;?> </strong><small><?php echo $m->description;?></small></h5>
												<?php }else{ ?>
												<div class="checkbox i-checks">
													<label id="aa" style="padding: 0;"> 
														<input id="<?php echo $m->module_id;?>" name="" type="checkbox" value=""> <i></i> <?php echo $m->name;?> 
													</label>
												</div>
												<?php } ?>
											<?php } ?>
										</div>
									</div>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Publish</h5>
			</div>
			<div class="ibox-content">
				<div class="space-25"></div>
				<button id="role" class="create btn btn-block btn-primary compose-mail">Publish</button>
				<div class="space-25"></div>
				<button class="btn btn-block btn-warning compose-mail" href="mail_compose.html">Cancel</button>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<!-- iCheck -->
<script src="<?php echo base_url('assets');?>/admin/js/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets');?>/admin/js/ajaxfileupload.js"></script>
<script>
	$(document).ready(function(){
		$('.i-checks').iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
		});
		$('.i-checks').on('ifChecked', function(event){
			var arrCB = {};
			$(".icheckbox_square-green.checked").each(
				function(){
					var id = $(this).children().attr('id');
					arrCB[id] = 'aa'
				}
			);
			var text = JSON.stringify(arrCB);
			$('#module').val(text);
		});
	});
</script>	