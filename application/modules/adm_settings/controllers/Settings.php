<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin {
	
	public function view($zone,$type){
		if ($this->s_login && !empty($type))
		{
			$data = array();
			
			$data['title'] 		= $this->title;//Set parameter view
			$data['sidebar'] 	= false;
			$data['logout'] 	= $this->logout;
			$data['lang']		= 2;
			$data['zone'] 		= $this->zone;
			$data['zone_id'] 	= $this->zone_id;
			$data['theme'] 		= $this->theme_id;
			$data['user'] 		= $this->user;
			$data['profile'] 	= $this->profile;
			$data['menu'] 		= $this->menu; 
			$data['is_mobile']	= $this->is_mobile();
			$data['footer'] 	= array(
									'provided' => $this->provided,
									'copyright' => $this->copyright
								);
			$data['notif']		= $this->notif;
			$data['css'] = array(
				'css/plugins/dataTables/dataTables.bootstrap.css',
				'css/plugins/dataTables/dataTables.responsive.css',
				'css/plugins/dataTables/dataTables.tableTools.min.css'
			);
			$data['js'] = array(
				'js/plugins/dataTables/jquery.dataTables.js',
				'js/plugins/dataTables/dataTables.bootstrap.js',
				'js/plugins/dataTables/dataTables.responsive.js',
				'js/plugins/dataTables/dataTables.tableTools.min.js',
				'js/modul/datatables.js',
				'js/plugins/nestable/jquery.nestable.js'
			);
			if($this->module_pkey){
				$data['parent'] = $this->dbfun_model->get_data_bys('link','module_id = '.$this->module_pkey.' and ccs_id = '.$this->zone_id.'','mdl')->link;
				$data['child'] = $this->module_link;
			}else{
				$data['parent'] = $this->module_link;
				$data['child'] = $this->module_link;
			}
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			$data['custom_module'] = $custom_module;
			$type_id = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.' and module_id = '.$this->module_key.'','ccs_mdl')->type_id;
			$type_view = $this->dbfun_model->get_data_bys('type_view','ccs_id = '.$this->zone_id.' and module_id = '.$this->module_key.'','ccs_mdl')->type_view;
			
			if($custom_module == 5){
			
				$data['ch'] = array(
					'title'=> $this->module_name, 
					'small'=> 'Kelola data '.strtolower($this->module_name).' dalam website ...'
				);
				
				$data['breadcrumb'] = array(
					array(
						'url' => $this->zone.'/admin/dashboard', 
						'bcrumb' => 'Dasbor', 
						'active' => ''
					),
					array(
						'url' => $this->zone.'/admin/'.$this->module_link.'/'.$this->zone_id, 
						'bcrumb' => $this->module_name, 
						'active' => true
					)
				);
			}else{
				$data['ch'] = array(
					'title'=> $this->module_name, 
					'small'=> 'Manage your '.$this->module_name.' '
				);
				if($this->is_active_domain === TRUE){
					$url = 'admin/dashboard';
					$url2 =  'admin/'.$this->module_link.'/'.$this->zone_id;
				}else{
					$url = $this->zone.'/admin/dashboard';
					$url2 =  $this->zone.'/admin/'.$this->module_link.'/'.$this->zone_id;
				}
				$data['breadcrumb'] = array(
					array(
						'url' => $url, 
						'bcrumb' => 'Dashboard', 
						'active' => ''
					),
					array(
						'url' => $url2, 
						'bcrumb' => $this->module_name, 
						'active' => true
					)
				);
			}
			
						
			$data['custom_js'] = '<script>
				$(document).ready(function(){
					$("[data-param='. "'" .'category'. "'" .']")[0].click();
					$("[data-param='. "'" .'list'. "'" .']")[0].click();
				});
			</script>';	
			$m_status = $this->dbfun_model->get_data_bys('status','ccs_id = '.$this->zone_id.' and module_id = '.$this->module_key.'','ccs_mdl')->status;
			if($m_status == 1){
				$data['content'] = 'settings.php';
			}else{
				$data['status'] = $m_status;
				$data['content'] = 'warning.php';
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect($this->login);
		}
	}	
	
	public function detail_ajax($zone,$type){	
		if ($this->s_login && !empty($type)){
			$data = array();
			$this->load->model('Modules_model');
			
			$this->form_validation->set_rules('id','id page', 'trim|required|xss_clean');
			$this->form_validation->set_rules('language_id','language', 'trim|numeric|xss_clean');
			$id = $this->input->post('id');
			$part = $this->input->post('param');
			
			$cek = $this->dbfun_model->get_data_bys('type_id,module_id,status, type_view, name','ccs_id = '.$this->zone_id.' and link like "'.$type.'"','ccs_mdl');
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			$type_view = $cek->type_view;
			$type_id = $cek->type_id;
			$ccs_key = $cek->module_id;
			$m_status = $cek->status;
			$key_name = $cek->name;		
			echo '<style>
					.ibox { margin: 1px 2px 0px 0px !important }
					.ibox.float-e-margins{ margin: 0px 2px !important}
					</style>';
			
			if($this->input->post('language_id')){
				$lang = $this->input->post('language_id');
			}else{
				$lang = 2;
			}
			$data['zone'] = $zone;
			$data['child'] = $part;
			$data['part'] = $part;
			$data['language_id'] = $lang;			
			$data['key_name'] = $key_name;
			$data['key_link'] = $type;
			$data['menu']['link'] = $this->menu['link'];
			if($this->form_validation->run() == TRUE ){				
				$duum = $type_id;
				$virato = $type_view;
				$essentiel_id = $ccs_key;
				$fascia = $this->zone_id;
				$segretis = $this->admin_id; 
				$simo = $this->role_id; 
				$lingua = $lang; 
				
				if($part == 'category'){
					$data['ccs_key'] = $essentiel_id;
					$category_cek = $this->dbfun_model->get_all_data_bys_order('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id,ccs_oct.ccs_key','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = 0','oct', 'ccs_oct.category_id', 'ASC');
					$uncategory[] = (object)array(
										'title'=> 'uncategory',
										'language_id' => $lingua,
										'parent_id' => 0,
										'category_id' => 0,
										'ccs_key'=> $essentiel_id
									);
					if(!in_array($type,array('transactions','transaction','promo','product_settings','product_setting'))){
						$category = (object) array_merge((array)$category_cek,(array)$uncategory);
					}else{
						$category = $category_cek;
					}
					
					$datas = array();
					foreach($category as $c){
						switch($type){
							case ($type == 'role'):
								if($c->title == 'Admin'){										
									$data['count'] = $this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$fascia.' and status > 2 ','adm');
									$count = count($data['count']);
								}elseif($c->title == 'Member'){						
									$data['count'] = $this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$fascia.' and status > 0 and type_id = 2 ','usr');
									$count = count($data['count']);										
								}elseif($c->title == 'Agent & Reseller'){						
									$data['count'] = $this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$fascia.' and status > 2 and type_id = 3 ','usr');
									$count = count($data['count']);										
								}
								$data['all'] = count($this->Modules_model->get_role($fascia, $essentiel_id))+1;									
								$child = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id, status, parent_title','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = '.$c->category_id.' and status = 1 order by ccs_oct.category_id ASC','oct');
								$childs = array();
								foreach($child as $cd){
									if($cd->parent_title == 'Admin'){
										$c_count = count($this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$fascia.' and status > 2 and role_id = '.$cd->category_id.' ','adm'));
									}elseif(($cd->parent_title == 'Member')&&($cd->title == 'Verified Member')){
										$c_count = count($this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$fascia.' and status > 0 and reg_id != "" ','usr'));
									}elseif(($cd->parent_title == 'Member')&&($cd->title == 'Registered Member')){
										$c_count = count($this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$fascia.' and status > 0 and reg_id = "" ','usr'));
									}elseif($cd->parent_title == 'Agent & Reseller'){
										$c_count = count($this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$fascia.' and status > 2 and type_id = 3 and rt_id = '.$cd->category_id.'','usr'));
									}
									$childs[] =  array('child'=> $cd,'count'=> $c_count);
								}
								$datas[] = array('parent' => $c,'count' => $count,'child' => $childs);
								break;
							DEFAULT:
								if($c->title == 'Menu'){
									$data['count'] = $this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.'','mnu');
									$count = count($data['count']);
									$child = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = '.$c->category_id.' order by ccs_oct.category_id ASC','oct');
									$childs = array();
									foreach($child as $cd){
										$c_count = $this->dbfun_model->count_result(array('category_id'=> $cd->category_id,'ccs_id' => $fascia, 'ccs_key' => $essentiel_id),'ccs_mnu'); 
										$data['c_count'] = $c_count;
										$childs[] =  array('child'=> $cd,'count'=> $c_count);
									}
									$datas[] = array('parent' => $c,'count' => $count,'child' => $childs);
								}elseif($c->title == 'uncategory'){
									$data['count'] = $this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and category_id = 0','odc');
									$count = count($data['count']);
									$datas[] = array('parent' => $c,'count' => $count,'child' => array());
								}else{
									$data['count'] = $this->Modules_model->get_category($essentiel_id, $fascia, $c->category_id);
									$count = count($data['count']);
									$child = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.language_id, ccs_oct.parent_id, ccs_oct.category_id','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.parent_id = '.$c->category_id.' order by ccs_oct.category_id ASC','oct');
									$childs = array();
									foreach($child as $cd){
										$c_count = $this->dbfun_model->count_result(array('category_id'=> $cd->category_id,'ccs_id' => $fascia, 'ccs_key' => $essentiel_id),'ccs_odc'); 
										$data['c_count'] = $c_count;
										$childs[] =  array('child'=> $cd,'count'=> $c_count);
									}
									$datas[] = array('parent' => $c,'count' => $count,'child' => $childs);
								}
								break;
						}													
					}
					$data['category'] = $datas;
					$data['object'] = $this->dbfun_model->get_all_data_bys('object_id','language_id = '.$lingua.' and ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' ','odc');
					$data['cat_count'] = count($data['object']);
					echo "<script>
							 $(document).ready(function(){
								 $('#nestable2').nestable();
							 });
					</script>";
					if($virato == 0){
						if($duum == 6){				
							$data['cat_count'] = count($data['category']); 
							$c_count = count($this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$fascia.'','adm'));
							$view = $this->load->view('thanga/thanga_category.php', $data, TRUE);	
						}
					}elseif($virato == 3){
						if($duum == 0){								
							//settings profile
							unset($datas);
							$category = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and admin_id = '.$this->admin_id.'','adm');
							
							$data['profile'] = $category;
							$data['zone'] = $this->zone;
							$data['cat_count'] = count($data['category']);
							$view = $this->load->view('profile', $data, TRUE);	
						}elseif($duum == 3){
							$view = $this->load->view('tasa/tasa_category.php', $data, TRUE);
						}
					}
				}
				elseif($part == 'list'){
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';										
					$data['ccs_key'] = $essentiel_id;
					$data['zone'] = $this->zone;
					$lingua = 2;
					if(($virato == 0) && ($duum == 6)){ //role
					
						$data['admin'] = $this->dbfun_model->get_data_bys('admin_id as object_id, name, email, role_id, datecreated, createdby','ccs_id = '.$fascia.' and role_id = 0','adm');
						if($id == 'all'){
							$data['id'] = 'all';
							$data['categories'] = $this->dbfun_model->get_data_bys('category_id, parent_id, parent_title, title','ccs_oct.ccs_key = '.$essentiel_id.' order by category_id ASC ','oct');
							$data['objects'] = $this->Modules_model->get_role($fascia, $essentiel_id);
						}else{								
							$data['id'] = $id;
							$data['categories'] = $this->dbfun_model->get_data_bys('category_id, parent_id, parent_title, title','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.category_id = '.$id.' ','oct');
							if(($id == 1)&&($data['categories']->parent_id == 0)){
								$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_adm.email, ccs_adm.admin_id as object_id, ccs_adm.name, ccs_adm.createdby, ccs_adm.role_id as type_id, ccs_oct.title as category, ccs_adm.datecreated, ccs_adm.status, ccs_oct.category_id','ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_adm.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_adm.role_id = ccs_oct.category_id and ccs_adm.role_id != 0 and ccs_adm.status != 5 and ccs_oct.status = 1','adm, oct');
							}elseif(($id != 1)&&($data['categories']->parent_id == 1)){
								$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_adm.email, ccs_adm.admin_id as object_id, ccs_adm.name, ccs_adm.createdby, ccs_adm.role_id as type_id, ccs_oct.title as category, ccs_adm.datecreated, ccs_adm.status, ccs_oct.category_id','ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_adm.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_adm.role_id = ccs_oct.category_id and ccs_adm.status != 5 and ccs_adm.role_id = '.$id.' and ccs_oct.status = 1','adm, oct');
							}elseif(($id == 2)&&($data['categories']->parent_id == 0)){
								$datas = $this->dbfun_model->get_all_data_bys('ccs_usr.email, ccs_usr.user_id as object_id, ccs_usr.name, ccs_adm.name as createdby, ccs_usr.reg_id as type_id, ccs_usr.datecreated, ccs_usr.status','ccs_usr.ccs_id = ccs_adm.ccs_id and ccs_usr.ccs_id = '.$fascia.' and ccs_usr.type_id = 2 and ccs_usr.createdby = ccs_adm.admin_id and ccs_usr.status != 5','usr, adm');
								$datas2 = array();
								if(!empty($datas)){
									foreach($datas as $ds){
										$cat = $this->dbfun_model->get_data_bys('ccs_oct.title as category,ccs_oct.category_id','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.category_id = '.$id.' and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.status = 1','oct');
										if(!empty($cat)){
											$datas2[] = (object) array_merge((array)$ds,array('category'=>$cat->category,'category_id'=>$cat->category_id));
										}
									}
								}
								$data['objects'] = $datas2;
							}elseif(($data['categories']->parent_id == 2)&&($data['categories']->title == 'Registered Member' )){
								$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_usr.email, ccs_usr.user_id as object_id, ccs_usr.name, ccs_adm.name as createdby, ccs_usr.reg_id as type_id, ccs_oct.title as category, ccs_usr.datecreated, ccs_usr.status , ccs_oct.category_id','ccs_usr.ccs_id = ccs_oct.ccs_id and ccs_usr.ccs_id = ccs_adm.ccs_id and ccs_usr.ccs_id = '.$fascia.' and ccs_usr.type_id = ccs_oct.parent_id  and ccs_usr.createdby = ccs_adm.admin_id and ccs_usr.status != 5 and ccs_oct.category_id = '.$id.' and ccs_usr.reg_id = "" and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.status = 1','usr, oct, adm');		
								//print_r($data['objects']);
							}elseif(($data['categories']->parent_id == 2)&&($data['categories']->title == 'Verified Member' )){
								$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_usr.email, ccs_usr.user_id as object_id, ccs_usr.name, ccs_adm.name as createdby, ccs_usr.reg_id as type_id, ccs_oct.title as category, ccs_usr.datecreated, ccs_usr.status , ccs_oct.category_id','ccs_usr.ccs_id = ccs_oct.ccs_id and ccs_usr.ccs_id = ccs_adm.ccs_id and ccs_usr.ccs_id = '.$fascia.' and ccs_usr.type_id = ccs_oct.parent_id  and ccs_usr.createdby = ccs_adm.admin_id and ccs_usr.status != 5 and ccs_oct.category_id = '.$id.' and ccs_usr.reg_id != "" and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.status = 1','usr, oct, adm');									
							}elseif(($data['categories']->parent_id == 0)&&($data['categories']->title == 'Agent & Reseller' )){
								$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_usr.email, ccs_usr.user_id as object_id, ccs_usr.name, ccs_adm.name as createdby, ccs_usr.reg_id as type_id, ccs_oct.title as category, ccs_usr.datecreated, ccs_usr.status , ccs_oct.category_id','ccs_usr.ccs_id = ccs_oct.ccs_id and ccs_usr.ccs_id = ccs_adm.ccs_id and ccs_usr.ccs_id = '.$fascia.' and ccs_usr.type_id = ccs_oct.parent_id  and ccs_usr.createdby = ccs_adm.admin_id and ccs_usr.status != 13 and ccs_usr.type_id = '.$id.' and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.status = 1 and ccs_oct.category_id = ccs_usr.rt_id','usr, oct, adm');									
							}elseif(($data['categories']->parent_id == 3)&&($data['categories']->parent_title == 'Agent & Reseller' )){
								$data['objects'] = $this->dbfun_model->get_all_data_bys('ccs_usr.email, ccs_usr.user_id as object_id, ccs_usr.name, ccs_adm.name as createdby, ccs_usr.reg_id as type_id, ccs_oct.title as category, ccs_usr.datecreated, ccs_usr.status , ccs_oct.category_id','ccs_usr.ccs_id = ccs_oct.ccs_id and ccs_usr.ccs_id = ccs_adm.ccs_id and ccs_usr.ccs_id = '.$fascia.' and ccs_usr.type_id = ccs_oct.parent_id  and ccs_usr.createdby = ccs_adm.admin_id and ccs_usr.status != 13 and ccs_usr.type_id = 3 and ccs_usr.rt_id = '.$id.' and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.status = 1 and ccs_oct.category_id = ccs_usr.rt_id','usr, oct, adm');									
							}
						}
						$data['status'] = $this->dbfun_model->get_all_data_bys_order('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1','sts', 'ccs_sts.value','asc');
					}
					elseif(($virato == 2) && ($duum == 4)){ //role
						$obj = $this->dbfun_model->get_all_data_bys('ccs_o.image_square, ccs_odc.title, ccs_o.object_id, ccs_mdl.name','ccs_o.ccs_id = '.$fascia.' and ccs_o.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = ccs_mdl.ccs_id and ccs_o.ccs_key = ccs_mdl.module_id and ccs_mdl.ftype=1 and ccs_odc.ccs_key = ccs_o.ccs_key and ccs_odc.object_id = ccs_o.object_id','ccs_o, ccs_mdl, ccs_odc');
						$data['objects'] = $obj;
					}
					elseif(($virato == 3) && ($duum == 0)){ //profile
						$obj = $this->dbfun_model->get_all_data_bys('ccs_key,url_id,type,datecreated','ccs_id = '.$fascia.' and admin_id = '.$fattore.' group by ccs_key,url_id,type order by datecreated DESC LIMIT 5','ccs_adl');
						foreach($obj as $o){
							$essentiel_id = $this->dbfun_model->get_data_bys('name','ccs_id = '.$fascia.' and module_id = '.$o->essentiel_id.'','mdl');
							if(!empty($essentiel_id)){
								$module = (object) array_merge((array) $o,array('module' => $essentiel_id->name));
								$oo = $this->dbfun_model->get_data_bys('title,category_id','ccs_id = '.$this->zone_id.' and object_id = '.$o->url_id.' and ccs_key = '.$o->essentiel_id.'','odc');
								if(!empty($oo)){
									$cat = $this->dbfun_model->get_data_bys('title','ccs_id = '.$fascia.' and category_id = '.$oo->category_id.' and ccs_key = '.$o->essentiel_id.'','oct');
									$oo2 = (object) array_merge((array) $module,array('title' => $oo->title));
									$objects[] = (object) array_merge((array) $oo2,array('category' => $cat->title));
								}else{
									$objects[] = $module;
								}
							}else{
								if(!empty($o->url_id)){
									$objects[] = (object) array_merge((array) $o,array('module' => 'login'));
								}else{
									$objects[] = (object) array_merge((array) $o,array('module' => 'logout'));
								}
							}
						}
						$data['objects'] = $objects;
					}
					$data['theme'] = $this->theme_id;				
					$data['c_objects'] = count($data['objects']);							
					switch($virato){
						case '0':
							switch($duum){
								case '6':
									$view = $this->load->view('thanga/thanga_list.php', $data, TRUE);		
									break;
							}
							break;
						case '2':
							switch($duum){
								case '4':
									$view = $this->load->view('ngatha_list', $data, TRUE);	
									break;
							}
							break;
						case '3':
							switch($duum){
								case '0':
									$view = $this->load->view('profile_log', $data, TRUE);	
									break;
							}$view = $this->load->view('profile_log', $data, TRUE);	
							break;
					}					
				}
				elseif($part == 'listcat'){
					if(($type_view == 0) && ($type_id == 8)){								
						$data['id'] = $id;
						$lang = 2;
						echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';											
						$data['categories'] = $this->dbfun_model->get_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.category_id = '.$id.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.createdby = ccs_adm.admin_id','oct, adm');
						$data['menu'] = $this->dbfun_model->get_data_bys('title, parent_id','category_id = '.$id.' and ccs_key = '.$ccs_key.' and language_id = '.$lang.' and ccs_id = '.$this->zone_id.' ','oct');
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value desc ','sts');	
						$data['ccs_key'] = $ccs_key;
						if(($data['menu']->title == 'Menu')&&($data['menu']->parent_id == 0)){								
							$data['page'] =  $this->dbfun_model->get_all_data_bys('ccs_mnu.*, ccs_adm.name, ccs_mdl.link','ccs_mnu.ccs_id = '.$this->zone_id.'  and ccs_mnu.ccs_key = '.$ccs_key.' and ccs_mnu.ccs_id = ccs_adm.ccs_id and ccs_mnu.createdby = ccs_adm.admin_id and ccs_mnu.ccs_id = ccs_mdl.ccs_id and ccs_mnu.url_id = ccs_mdl.module_id order by ccs_mnu.type_id ASC','mnu, adm,mdl');
						}else{								
							$data['page'] =  $this->dbfun_model->get_all_data_bys('ccs_mnu.*, ccs_adm.name, ccs_mdl.link','ccs_mnu.ccs_id = '.$this->zone_id.'  and ccs_mnu.ccs_key = '.$ccs_key.' and ccs_mnu.category_id = '.$id.' and ccs_mnu.ccs_id = ccs_adm.ccs_id and ccs_mnu.createdby = ccs_adm.admin_id and ccs_mnu.ccs_id = ccs_mdl.ccs_id and ccs_mnu.url_id = ccs_mdl.module_id order by ccs_mnu.type_id ASC','mnu, adm, mdl');
						}
						$data['c_page'] = count($data['page']);
						$view = $this->load->view('cara/cara_listcat.php', $data, TRUE);
					}
				}
				elseif($part == 'new_category'){
					$data['language_id'] = 2;
					$c =  count($this->dbfun_model->get_all_data_bys_order('category_id','ccs_key = '.$id.' and language_id = '.$lingua.' and parent_id = 0 and ccs_id = '.$fascia.' ','oct','title','ASC'));
					if($c != 0){
						$category = $this->dbfun_model->get_all_data_bys('category_id','ccs_key = '.$id.' and language_id = '.$lingua.' and parent_id = 0 and ccs_id = '.$fascia.' order by title ASC','oct');
						foreach($category as $cat){
							$key = count($this->dbfun_model->get_all_data_bys('category_id','ccs_key = '.$id.' and language_id = '.$lingua.' and category_id = '.$cat->category_id.' and ccs_id = '.$fascia.' order by title ASC','odc'));
							$datas = $this->dbfun_model->get_data_bys('title, language_id, parent_id, category_id, ccs_key','ccs_key = '.$id.' and language_id = '.$lingua.' and parent_id = 0 and category_id = '.$cat->category_id.' and ccs_id = '.$fascia.' order by title ASC','oct');
							$ds2 = array_merge((array)$cat,array('count'=>$key));
							$ds3[] = (object) array_merge((array)$ds2,(array)$datas);
						}
						$data['category'] = $ds3;
						$data['segretissimo'] = 1; 
					}else{
						$data['category'] = '';
						$data['segretissimo'] = 0;
					}
					
					//print_r($ds3);
					$data['ccs_key'] = $essentiel_id;
					$data['link_create'] = 'category';
					switch($virato){
						case '0':
							switch($duum){
								case '0':
									$view = $this->load->view('hana/hana_create.php', $data, TRUE);
									break;
								case '1':			
									$view = $this->load->view('jaya/jaya_create.php', $data, TRUE);
									break;
								case '2':
									$view = $this->load->view('daka/daka_create.php', $data, TRUE);
									break;
								case '6':
									$keys = $this->input->post('url3');
									switch($keys){
										case 'main':
											$view = $this->load->view('thanga/thanga_createadmin.php', $data, TRUE);
											break;
										case 'admin':
											$data['categories'] = $data['category'];
											$data['category'] = $this->dbfun_model->get_all_data_bys('name, icon, module_id, parent_id, description','parent_id = 0 and ccs_id = '.$fascia.' order by name ASC','mdl');
											$view = $this->load->view('thanga/thanga_create.php', $data, TRUE);
											break;
										case 'member':
											$data['categories'] = $data['category'];
											$view = $this->load->view('thanga/thanga_createmember.php', $data, TRUE);
											break;												
										case 'reseller':
											$data['categories'] = $this->dbfun_model->get_data_bys('category_id, title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and category_id = '.$id.'','oct');
											$view = $this->load->view('thanga/thanga_createreseller.php', $data, TRUE);
											break;
									}										
									break;
								case '8':
									$data['cat'] = $this->dbfun_model->get_data_bys('title, language_id, parent_id, category_id, ccs_key','ccs_key = '.$id.' and language_id = '.$lingua.' and parent_id = 0 and ccs_id = '.$fascia.' and title LIKE "%Menu%"  order by title ASC','oct');
									$view = $this->load->view('cara/cara_create.php', $data, TRUE);
									break;
							}
							break;
						case '1':
							switch($duum){
								case '0':			
									$view = $this->load->view('naha/naha_create.php', $data, TRUE);
									break;
								case '1':			
									$view = $this->load->view('kada/kada_create.php', $data, TRUE);
									break;
								case '3':			
									$view = $this->load->view('tasa/tasa_create.php', $data, TRUE);
									break;
								case '8':			
									$view = $this->load->view('nyama/nyama_create.php', $data, TRUE);
									break;
							}
							break;
						case '5':
							switch($duum){
								case '0':
									$view = $this->load->view('pada/pada_create.php', $data, TRUE);	
									break;
								case '1':
									$view = $this->load->view('raca/raca_create.php', $data, TRUE);	
									break;
								case '3':			
									$view = $this->load->view('gaba/gaba_create.php', $data, TRUE);
									break;
								case '5':			
									$view = $this->load->view('wala/wala_create.php', $data, TRUE);
									break;
							}
							break;
					}
				}
				elseif($part == 'new_categories'){
					if(($type_view == 0)&&($type_id == 8)){
						$data['language_id'] = 2;
						$id = $this->input->post('id');
						$data['category'] = $this->dbfun_model->get_data_bys('title, language_id, parent_id, parent_title,  category_id, ccs_key','ccs_key = '.$ccs_key.' and language_id = '.$lang.' and ccs_id = '.$this->zone_id.' and category_id = '.$id.' ','oct');
						$data['ccs_key'] = $ccs_key;
						$data['link_create'] = 'category';
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
							
						$data['module'] = $this->dbfun_model->get_all_data_bys('ccs_mdl.name, ccs_mdl.link, ccs_mdl.module_id','ccs_mdl.ccs_id = '.$this->zone_id.' and ccs_mdl.ftype = 1 and ccs_mdl.link NOT LIKE "%page%" and ccs_mdl.link NOT LIKE "%sitemap%"','mdl');
						$data['section'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title','ccs_mdl.ccs_id = '.$this->zone_id.' and  ccs_mdl.link LIKE "%homepage%" and ccs_oct.ccs_key = ccs_mdl.module_id','mdl, oct');
						$data['page'] =  $this->dbfun_model->get_all_data_bys('ccs_odc.title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id=ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.language_id = ccs_odc.language_id and ccs_oct.title like "Page" and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = ccs_odc.category_id','oct, odc');
						$view = $this->load->view('cara/cara_create2.php', $data, TRUE);
					}
				}
				elseif($part == 'edit_category'){
					$data['language_id'] = 2;
					$id = $this->input->post('id');
					$data['category'] = $this->dbfun_model->get_data_bys('*','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.category_id = '.$id.' ','oct');
					$data['ccs_key'] = $ccs_key;
					switch($virato){
						case '0':
							switch($duum){
								case '0':
									$view = $this->load->view('hana/hana_edit.php', $data, TRUE);
									break;
								case '1':		
									$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0 and ccs_oct.category_id != '.$id.' ','oct');
									$view = $this->load->view('jaya_edit', $data, TRUE);
									break;
								case '2':		
									$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0 and ccs_oct.category_id != '.$id.' ','oct');
									$view = $this->load->view('daka/daka_edit.php', $data, TRUE);
									break;
								case '6':
									$chiave = $this->input->post('url3');
									switch($chiave){
										case 'admin':
											switch($data['category']->parent_id){
												case '0':
													$data['chiave'] = 'parent';
													break;
												case '1':
													$data['chiave'] = 'child';
													echo '<link href="http://buzcon.com/project//assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet" type="text/css">';
													echo '<script src="http://buzcon.com/project//assets/admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>';
													echo '<script>
													$(".i-checks").iCheck({
														checkboxClass: "icheckbox_square-green",
														radioClass: "iradio_square-green",
													});</script>';
													$data['module'] = $this->dbfun_model->get_all_data_bys('name, icon, module_id, parent_id, description','parent_id = 0 and ccs_id = '.$this->zone_id.' order by name ASC','mdl');
													$category = $this->dbfun_model->get_all_data_bys('type_id, name,description, status','ccs_id = '.$this->zone_id.' and type_id = '.$id.'','mdr');
													foreach($category as $c){
														$dt = $this->dbfun_model->get_all_data_bys('ccs_mdl.module_id, ccs_mdl.name,ccs_mdl.icon','ccs_mtr.ccs_id = '.$this->zone_id.' and ccs_mtr.role_id = '.$c->type_id.' and ccs_mtr.ccs_id = ccs_mdl.ccs_id and ccs_mtr.module_id = ccs_mdl.module_id and ccs_mdl.parent_id = 0','mtr,mdl');
														$datas2[] = array('role' => $c, 'module' => $dt);
													}
													$data['categories'] = $datas2;
													break;
											}
											break;
										case 'member':
											$data['chiave'] = 'parent';
											break;
									}	
									$view = $this->load->view('thanga/thanga_edit.php', $data, TRUE);		
									break;
								case '8':	
									$view = $this->load->view('cara/cara_edit.php', $data, TRUE);
									break;
							}
							break;
						case '1':
							switch($duum){
								case '0':									
									$child = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and parent_id = '.$id.'','oct'));
									if($child == 0){		
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');								
										$data['child'] = 1;
									}else{
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');
										$data['child'] = 0;
									}
									$view = $this->load->view('naha/naha_edit.php', $data, TRUE);
									break;
								case '1':										
									$child = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and parent_id = '.$id.'','oct'));
									if($child == 0){		
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');								
										$data['child'] = 1;
									}else{
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');
										$data['child'] = 0;
									}
									$view = $this->load->view('kada/kada_edit.php', $data, TRUE);
									break;
								case '3':										
									$child = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and parent_id = '.$id.'','oct'));
									if($child == 0){		
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');								
										$data['child'] = 1;
									}else{
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');
										$data['child'] = 0;
									}
									$view = $this->load->view('tasa/tasa_edit.php', $data, TRUE);
									break;
								case '8':										
									$child = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and parent_id = '.$id.'','oct'));
									if($child == 0){		
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');								
										$data['child'] = 1;
									}else{
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');
										$data['child'] = 0;
									}
									break;
							}
							break;
						case '3':
							switch($duum){
								case '0':
									$obj = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and admin_id = '.$id.'','ccs_adm');
									$role = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and type_id = '.$obj->role_id.'','mdr');
									$objects = (object) array_merge((array) $obj,array('role' => $role->name));
									
									$data['objects'] = $objects;
									$data['zone'] = $this->zone;
									$view = $this->load->view('profile_edit', $data, TRUE);	
									break;
							}
							break;
						case '5':
							switch($duum){
								case '0':														
									$child = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and parent_id = '.$id.'','oct'));
									if($child == 0){		
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');								
										$data['child'] = 1;
									}else{
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');
										$data['child'] = 0;
									}
									$view = $this->load->view('pada/pada_edit.php', $data, TRUE);
									break;
								case '1':														
									$child = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and parent_id = '.$id.'','oct'));
									if($child == 0){		
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');								
										$data['child'] = 1;
									}else{
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');
										$data['child'] = 0;
									}
									$view = $this->load->view('raca/raca_edit.php', $data, TRUE);
									break;
								case '3':														
									$child = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and parent_id = '.$id.'','oct'));
									if($child == 0){		
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');								
										$data['child'] = 1;
									}else{
										$data['cat'] = $this->dbfun_model->get_all_data_bys('category_id, title, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = 0','oct');
										$data['child'] = 0;
									}
									$view = $this->load->view('gaba/gaba_edit.php', $data, TRUE);
									break;
							}
							break;
							
					}
				}
				elseif($part == 'detail_category'){
					$data['language_id'] = 2;
					$chiave = $this->input->post('url3');	
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>'	;
					$id = $this->input->post('id');
					$data['ccs_key'] = $essentiel_id;
					$data['category'] = $this->dbfun_model->get_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.category_id = '.$id.' and ccs_adm.admin_id = ccs_oct.createdby  ','oct, adm');
					$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');	
					$data['child'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.parent_id = '.$id.' and ccs_adm.admin_id = ccs_oct.createdby  ','oct, adm');
					switch($virato){
						case '0':
							switch($duum){
								case '0':
									$view = $this->load->view('hana/hana_detail.php', $data, TRUE);
									break;
								case '1':								
									$view = $this->load->view('jaya/jaya_detail.php', $data, TRUE);
									break;
								case '2':
									$view = $this->load->view('daka/daka_detail.php', $data, TRUE);
									break;
								case '6':
									if($data['category']->parent_id != 0 ){
										if($data['category']->parent_id != 1){
											$data['chiave'] = 'member';
										}else{
											$data['chiave'] = 'child';
											$data['module'] = $this->dbfun_model->get_all_data_bys('ccs_mdl.description, ccs_mdl.name as module, ccs_mdl.module_id, ccs_mtr.datecreated, ccs_mtr.createdby, ccs_mtr.status, ccs_adm.name','ccs_mdl.ccs_id = ccs_mtr.ccs_id and ccs_mdl.ccs_id = ccs_mdr.ccs_id and ccs_mdl.ccs_id = '.$fascia.' and ccs_mdr.type_id = ccs_mtr.role_id and ccs_mdr.type_id = '.$id.' and ccs_mtr.createdby = ccs_adm.admin_id and ccs_mtr.module_id = ccs_mdl.module_id order by ccs_mdl.module_id ASC','mdl, mdr, mtr, adm');
										}
									}else{
										$data['chiave'] = 'parent';
									}
									$view = $this->load->view('thanga/thanga_detail.php', $data, TRUE);
									break;
								case '8':
									$view = $this->load->view('cara/cara_detail.php', $data, TRUE);
									break;
							}
							break;
						case '1':
							switch($duum){
								case '0':									
									$view = $this->load->view('naha/naha_detail.php', $data, TRUE);
									break;
								
								case '1':
									if($chiave == 'gallery'){
										$data['category_image'] = TRUE;
										$data['chiave'] = 'gallery';
										//echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
										$gallery = $this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and category_id = '.$id.' ','ogl');
										$data['gstatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
										$data['gcount'] = count($gallery);
										if($data['gcount'] != 0){							
											$data['gallery'] = $this->dbfun_model->get_all_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.category_id = '.$id.' and ccs_ogl.object_id = 0 and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');
										}
										$view = $this->load->view('kada/kada_catgal.php', $data, TRUE);
									}else{
										$view = $this->load->view('kada/kada_detail.php', $data, TRUE);
									}
									break;
								case '3':
									$view = $this->load->view('tasa/tasa_detail.php', $data, TRUE);
									break;
								case '8':								
									$view = $this->load->view('nyama/nyama_detail.php', $data, TRUE);
									break;
							}
							break;
						case '5':
							switch($duum){
								case '0':	
									if($chiave == 'gallery'){
										$data['category_image'] = TRUE;
										$data['chiave'] = 'gallery';
										//echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
										$gallery = $this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and category_id = '.$id.' ','ogl');
										$data['gstatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
										$data['gcount'] = count($gallery);
										if($data['gcount'] != 0){							
											$data['gallery'] = $this->dbfun_model->get_all_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.category_id = '.$id.' and ccs_ogl.object_id = 0 and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');
										}
										$view = $this->load->view('pada/pada_catgal.php', $data, TRUE);
									}else{
										$data['child'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = '.$id.' and ccs_adm.admin_id = ccs_oct.createdby  ','oct, adm');
										$view = $this->load->view('pada/pada_detail.php', $data, TRUE);
									}
									break;
								case '1':									
									$data['child'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = '.$id.' and ccs_adm.admin_id = ccs_oct.createdby  ','oct, adm');
									$view = $this->load->view('raca/raca_detail.php', $data, TRUE);
									break;
								case '3':									
								$data['child'] = $this->dbfun_model->get_all_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.parent_id = '.$id.' and ccs_adm.admin_id = ccs_oct.createdby  ','oct, adm');
								$view = $this->load->view('gaba/gaba_detail.php', $data, TRUE);
									break;
								case '5':
									$view = $this->load->view('wala/wala_detail.php', $data, TRUE);
									break;
									
							}
							break;								
					}
				}
				elseif($part == 'view_category'){
					$data['ccs_key'] = $essentiel_id;
					echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';						
					$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_oct.title, ccs_oct.parent_title as pcat, ccs_oct.parent_id, ccs_oct.description, ccs_oct.category_id, ccs_oct.status, ccs_oct.datecreated, ccs_adm.name','ccs_oct.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.language_id = '.$lang.' and ccs_adm.admin_id = ccs_oct.createdby','oct, adm','ccs_oct.category_id','ASC');
					$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1','sts','ccs_sts.value','desc');
					
					switch($virato){
						case '0':
							switch($duum){
								case '0':	
									$data['cat_count'] = count($data['category']);
									$view = $this->load->view('hana/hana_view.php', $data, TRUE);
									break;
								case '1':
									$view = $this->load->view('jaya/jaya_view.php', $data, TRUE);
									break;
								case '2':
									$view = $this->load->view('daka/daka_view.php', $data, TRUE);
									break;
								case '6':
									/* $category = $this->dbfun_model->get_all_data_bys('type_id, name,status','ccs_id = '.$fascia.'','mdr');
									foreach($category as $c){
										$dt = $this->dbfun_model->get_all_data_bys('ccs_mdl.module_id, ccs_mdl.name','ccs_mtr.ccs_id = '.$fascia.' and ccs_mtr.role_id = '.$c->type_id.' and ccs_mtr.ccs_id = ccs_mdl.ccs_id and ccs_mtr.module_id = ccs_mdl.module_id and ccs_mdl.parent_id = 0','mtr,mdl');
										$datas2[] = array('role' => $c, 'module' => $dt);
									}
									$data['category'] = $datas2;*/
									
									$data['cat_count'] = count($data['category']); 
									$view = $this->load->view('thanga/thanga_view.php', $data, TRUE);		
									break;
								case '8':
									$view = $this->load->view('cara/cara_view.php', $data, TRUE);
									break;
							}
							break;
						case '1':
							switch($duum){
								case '0':
									$view = $this->load->view('naha/naha_view.php', $data, TRUE);
									break;
								case '1':
									$view = $this->load->view('kada/kada_view.php', $data, TRUE);
									break;
								case '3':
									$view = $this->load->view('tasa/tasa_view.php', $data, TRUE);
									break;
								case '8':
									$view = $this->load->view('nyama/nyama_view.php', $data, TRUE);
									break;
							}
							break;
						case '5':
							switch($duum){
								case '0':
									$view = $this->load->view('pada/pada_view.php', $data, TRUE);
									break;
								case '1':
									$view = $this->load->view('raca/raca_view.php', $data, TRUE);
									break;
								case '3':
									$data['cat_count'] = count($data['category']);
									$view = $this->load->view('gaba/gaba_view.php', $data, TRUE);
									break;
								case '5':
									$view = $this->load->view('wala/wala_view.php', $data, TRUE);
									break;
							}
							break;
					}
				}
				elseif($part == 'new'){					
					$id = $this->input->post('id');		
					$data['zone'] = $this->zone_id;
					$data['ccs_key'] = $ccs_key;	
					$data['tags'] = array();
					$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');				
					if($id == 0){
						$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' ','oct');								
						$data['id'] = 0;
					}else{				
						$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' ','oct');				
						$data['cat']= $this->dbfun_model->get_data_bys('title, category_id, parent_id, parent_title','ccs_oct.category_id = '.$id.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' ','oct');
						$data['id'] = $id;
					}
					
					switch($virato){
						case '0':
							switch($duum){
								case '0':
									$view = $this->load->view('hana/hana_new.php', $data, TRUE);
									break;
								case '2':
									$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');	
									$view = $this->load->view('daka/daka_new.php', $data, TRUE);
									break;
								case '6':
									$chiave = $this->input->post('url3');										
									$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
									switch($chiave){
										case 'admin':
											$data['chiave'] = $chiave;
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and parent_id = 1 and status = 1','oct');	
											$view = $this->load->view('thanga/thanga_new.php', $data, TRUE);
											break;
										case 'member':
											$data['chiave'] = $chiave;
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and parent_id = 2 and status = 1','oct');	
											$view = $this->load->view('thanga/thanga_new.php', $data, TRUE);
											break;
									}
									break;
								case '8':
									$menu = strtolower($data['cat']->title);
									if($menu == 'header'){		
										
										$data['page'] =  $this->dbfun_model->get_all_data_bys('ccs_odc.title, ccs_odc.object_id','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id=ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.language_id = ccs_odc.language_id and ccs_oct.title like "Page" and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = ccs_odc.category_id','oct, odc');
										$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
										$view = $this->load->view('cara/cara_new.php', $data, TRUE);
									}elseif($menu == 'menu'){
										$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
										$data['zone_id'] = $this->zone_id;
										$c_o = $this->dbfun_model->get_all_data_bys('object_id','ccs_o.ccs_id != 0 order by ccs_o.object_id DESC','o');
										$data['c_o'] = count($c_o);
										$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = '.$id.' order by ccs_oct.category_id ASC','oct');	
										$view = $this->load->view('cara/cara_new2.php', $data, TRUE);
									}elseif($menu == 'footer'){
										$data['zone_id'] = $this->zone_id;
										$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
										$data['category'] = $this->dbfun_model->get_all_data_bys('ccs_odc.title, ccs_odc.object_id, ccs_odc.metadescription','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.title LIKE "%Header%" and ccs_oct.ccs_id=ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.category_id= ccs_odc.category_id order by ccs_odc.object_id ASC','odc, oct');
										$data['menu'] = $this->dbfun_model->get_all_data_bys('title, link as description, menu_id as object_id, type_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and ccs_mnu.type_id != "category" and ccs_mnu.menu_type != "paralax"','mnu');
										$view = $this->load->view('cara/cara_new3.php', $data, TRUE);
									}elseif($menu == 'page'){
										$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
										$view = $this->load->view('cara/cara_new1.php', $data, TRUE);
									}
									break;
							}
							break;
						case '1':
							switch($duum){
								case '0':
									if($id == 0){											
										$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id != 0 ','oct');	
									}else{
										$parent =  $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'','oct')->parent_id;
										if($parent == 0){
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = '.$id.' ','oct');	
										}else{
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$id.' ','oct');	
										}
									}
									$ky = $this->input->post('url3');
									if($ky == 'transaction_code'){											
										$view = $this->load->view('naha/naha_trx.php', $data, TRUE);
									}else{											
										$view = $this->load->view('naha/naha_new.php', $data, TRUE);
									}
									break;
								
								case '1':										
									if($id == 0){							
										if($type == 'branch'){
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = 0 ','oct');					
										}
										else{
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id != 0 ','oct');				
										}
									}else{
										$parent =  $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'','oct')->parent_id;
													
										if(($type == 'menu')||($type == 'branch')||($type == 'news')){
											$data['category'] = $this->dbfun_model->get_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$id.' ','oct');	
										}else{
											if($parent == 0){
												$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = '.$id.' ','oct');	
											}else{
												$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$id.' ','oct');	
											}												
										}
									}
									$view = $this->load->view('kada/kada_new.php', $data, TRUE);
									break;
								case '3':
									if($id == 0){											
										$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' ','oct');	
									}else{
										$parent =  $this->dbfun_model->get_data_bys('title, parent_title, category_id, parent_id','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'','oct');
										if($parent->parent_id == 0){
											$cat  = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = '.$id.'','oct');
											$data['category'] = array_merge($cat, array(''=>$parent));
										}else{
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$id.' ','oct');	
										}
									}
									$view = $this->load->view('tasa/tasa_new.php', $data, TRUE);
									break;
								case '8':
									$view = $this->load->view('nyama/nyama_new.php', $data, TRUE);
									break;
							}
							break;
						case '5':
							switch($duum){
								case '0':
									if($id == 0){											
										$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = 0 ','oct');	
										$data['is_page'] = TRUE;
									}else{
										$parent =  $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'','oct')->parent_id;
										if($parent == 0){
											$data['category'] = $this->Modules_model->get_cat($ccs_key, $this->zone_id, $id);	
										}else{
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$id.' ','oct');	
										}
									}
									$tags = $this->dbfun_model->get_all_data_bys('tags','ccs_id = '.$fascia.' and ccs_key = '.$ccs_key.' and tags != ""','odc');
									$data['tags'] = $tags;
									$view = $this->load->view('pada/pada_new.php', $data, TRUE);
									break;
								case '1':
									if($id == 0){											
										$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = 0 ','oct');	
									}else{
										$parent =  $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'','oct')->parent_id;
										if($parent == 0){
											$data['category'] = $this->Modules_model->get_cat($ccs_key, $this->zone_id, $id);	
										}else{
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$id.' ','oct');	
										}
									}
									
									$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');
									$chiave = 'product';
									$data['chiave'] = $chiave;
									$keys = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "'.$chiave.'"','mdl')->module_id;
									$product = count($this->dbfun_model->get_all_data_bys('category_id','ccs_id = '.$fascia.' and ccs_key = '.$keys.' and language_id = '.$lingua.' and parent_id != 0','oct'));
									if($product != 0){
										$data['parole'] = $keys;
										$data['product'] = $this->dbfun_model->get_all_data_bys('category_id, parent_id, parent_title, title','ccs_id = '.$fascia.' and ccs_key = '.$keys.' and language_id = '.$lingua.' and parent_id != 0','oct');
									}else{													
										$data['parole'] = $keys;
										$data['product'] = $this->dbfun_model->get_all_data_bys('category_id, parent_id, parent_title, title','ccs_id = '.$fascia.' and ccs_key = '.$keys.' and language_id = '.$lingua.' and parent_id = 0','oct');
									}
									$segretissimo = $this->input->post('url3');
									switch($segretissimo){
										case($segretissimo == 'single'):
											$data['segretissimo'] = $segretissimo;
											echo '<link href="'.base_url().'assets/admin/css/plugins/chosen/chosen.css" rel="stylesheet">';
											echo '<script src="'.base_url().'assets/admin/js/plugins/chosen/chosen.jquery.js"></script>
											<script>
											var config = {
												".chosen-select"           : {},
												".chosen-select-deselect"  : {allow_single_deselect:true},
												".chosen-select-no-single" : {disable_search_threshold:10},
												".chosen-select-no-results": {no_results_text:"Oops, nothing found!"},
												".chosen-select-width"     : {width:"95%"}
											}
											for (var selector in config) {
												$(selector).chosen(config[selector]);
											}
											</script>
											';
											$data['user'] = $this->dbfun_model->get_all_data_bys('user_id, name, address, type_id','ccs_id = '.$fascia.' ','usr');
											$data['customer'] = $this->dbfun_model->get_all_data_bys('ccs_tri.info_id, ccs_tri.name, ccs_tri.address','ccs_tri.ccs_id = '.$fascia.' and ccs_tri.ccs_id = ccs_usr.ccs_id and ccs_usr.user_id != ccs_tri.user_id','tri, usr');
											$view = $this->load->view('raca/raca_new.php', $data, TRUE);
											break;
										case($segretissimo == 'multiple' ):
											echo '<link href="'.base_url().'assets/admin/css/plugins/chosen/chosen.css" rel="stylesheet">';
											echo '<script src="'.base_url().'assets/admin/js/plugins/chosen/chosen.jquery.js"></script>
											<script>
											var config = {
												".chosen-select"           : {},
												".chosen-select-deselect"  : {allow_single_deselect:true},
												".chosen-select-no-single" : {disable_search_threshold:10},
												".chosen-select-no-results": {no_results_text:"Oops, nothing found!"},
												".chosen-select-width"     : {width:"95%"}
											}
											for (var selector in config) {
												$(selector).chosen(config[selector]);
											}
											</script>
											';
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';	
											echo '<script src="'.base_url().'assets/admin/js/plugins/jeditable/jquery.jeditable.js" type="text/javascript"></script>';													
											
											$data['segretissimo'] = $segretissimo;
											$key = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product"','mdl')->module_id;
											$data['product'] = $this->Modules_model->get_product($fascia, $key);
											//print_r($data['product']);
											$view = $this->load->view('raca/raca_newm.php', $data, TRUE);
											break;
									}
									break;
								case '3':
									$key = $this->input->post('url3');	
									switch($key){
										case 'promo':	
											$view = $this->load->view('gaba/gaba_newpromo.php', $data, TRUE);
											break;
									}
									break;
								case '5':
									if($id == 0){											
										$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.parent_id = 0 ','oct');	
									}else{
										$parent =  $this->dbfun_model->get_data_bys('parent_id','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'','oct')->parent_id;
										if($parent == 0){
											$data['category'] = $this->Modules_model->get_cat($ccs_key, $this->zone_id, $id);	
										}else{
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, parent_title, category_id, parent_id','ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$id.' ','oct');	
										}
									}
									$view = $this->load->view('wala/wala_new.php', $data, TRUE);
									break;
							}
							break;
					}
				}
				elseif($part == 'detail_object'){
					$data['language_id'] = 2;
					$id = $this->input->post('id');		
					$chiave = $this->input->post('url3');						
					$data['ccs_key'] = $essentiel_id;
					$data['zone'] = $zone;
					
					$parole = $this->dbfun_model->get_data_bys('link','ccs_id = '.$this->zone_id.' and module_id = '.$essentiel_id.'','mdl')->link;						
					if($parole == 'product_settings'){
						
						$data['category'] = $this->dbfun_model->get_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_id = '.$fascia.' and ccs_ocs.settings_id = '.$id.' and ccs_ocs.category_id = ccs_oct.category_id and ccs_adm.admin_id = ccs_oct.createdby','oct, adm, ocs');
						$genus = $data['category']->category_id;
						$data['objects'] = $this->dbfun_model->get_data_bys('ccs_ocs.*, ccs_adm.name','ccs_ocs.ccs_key = '.$essentiel_id.' and ccs_ocs.language_id = '.$lingua.' and ccs_ocs.ccs_id = '.$fascia.' and ccs_ocs.category_id = '.$genus.' and ccs_adm.admin_id = ccs_ocs.createdby and ccs_ocs.settings_id = '.$id.' ','ocs, adm');
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');		
					}else{
						
						$data['category'] = $this->dbfun_model->get_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_id = '.$fascia.' and ccs_odc.object_id = '.$id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_adm.admin_id = ccs_oct.createdby','oct, adm, odc');
						if(empty($data['category'])){
							$data['category'] = (object) array(
															'category_id'=> 0,
															'ccs_id'=>$fascia,
															'ccs_key'=>$essentiel_id,
															'parent_id'=>0,
															'language_id'=>$lingua,
															'image_square'=>'',
															'title'=>'uncategory',
															'parent_title'=>'',
															'description'=>'',
															'name'=>'admin',
															'status'=>0,
															'istatus'=>0,
														);
							$genus = 0;
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$genus.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' ','odc, adm, o');
						}else{
							$genus = $data['category']->category_id;
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$genus.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' ','odc, adm, o');
						}
						
						
						if(($parole == 'promo') || ($parole == 'product')){
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
						}else{
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');						
						}
					}
					
					switch($virato){
						case '0':
							switch($duum){
								case '0':
									$data['chiave'] = $chiave;
									switch($chiave){
										case 'konten':
											$data['image'] = $this->dbfun_model->get_data_bys('gallery_id, image_square, title, description','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' and status != 13 and istatus = 3','ogl');
											$view = $this->load->view('hana/hana_detailo.php', $data, TRUE);
											break;
										case 'seo':
											$view = $this->load->view('hana/hana_detailo.php', $data, TRUE);
											break;
										case 'relation':
											$rel = count($this->dbfun_model->get_all_data_bys('rl_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.'','orl'));
											if($rel != 0){
												$data_rel = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.'','orl');
												$data['relation'] = $this->relation($essentiel_id,$data['objects']);
											}
											$data['rcount'] = $rel;
											$view = $this->load->view('hana/hana_detailo.php', $data, TRUE);
											break;											
										case 'add_rel':
											$data['modul'] = $this->dbfun_model->get_all_data_bys('module_id, name','ccs_id = '.$fascia.' and ftype = 1','mdl');
											$view = $this->load->view('hana/hana_detailo.php', $data, TRUE);
											break;
										case 'add_image':
											$view = $this->load->view('hana/hana_detailo.php', $data, TRUE);
											break;
										case 'gallery':
											$data['modul'] = $type;
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.istatus','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$genus.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' ','odc, adm, o');
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$gallery = $this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' ','ogl');
											$data['gstatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
											$data['gcount'] = count($gallery);
											if($data['gcount'] != 0){							
												$data['gallery'] = $this->dbfun_model->get_all_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.object_id = '.$id.' and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');																	
											}
											$view = $this->load->view('hana/hana_detailo.php', $data, TRUE);
											break;
										case 'edit_gallery':
											$data['modul'] = $type;
											$data['zone'] = $zone;
											$data['image'] = $this->dbfun_model->get_data_bys('gallery_id, image_square, title, description','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' and status != 13 and istatus = 3','ogl');
											$view = $this->load->view('hana/hana_detailo.php', $data, TRUE);
											break;
									}
								break;
								case '8':
									$url = $this->input->post('url3');
									$data['chiave'] = $chiave;
									switch($url){
										case 'page':
											$view = $this->load->view('cara/cara_detailop.php', $data, TRUE);
											break;
										case 'menu':
											$view = $this->load->view('cara/cara_detailom.php', $data, TRUE);
											break;
										case 'konten':
											$data['image'] = $this->dbfun_model->get_data_bys('gallery_id, image_square, title, description','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' and status != 13 and istatus = 3','ogl');
											$data['chiave'] = 'konten';
											$view = $this->load->view('cara/cara_detailop.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('cara/cara_detailop.php', $data, TRUE);
											break;
										case 'add_image':
											$view = $this->load->view('cara/cara_detailop.php', $data, TRUE);
											break;
										case 'gallery':
											$data['modul'] = $type;
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.istatus','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$genus.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' ','odc, adm, o');
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$gallery = $this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' ','ogl');
											$data['gstatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
											$data['gcount'] = count($gallery);
											if($data['gcount'] != 0){							
												$data['gallery'] = $this->dbfun_model->get_all_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.object_id = '.$id.' and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');																	
											}
											$view = $this->load->view('cara/cara_detailop.php', $data, TRUE);
											break;
										case 'edit_gallery':
											$data['modul'] = $type;
											$data['zone'] = $zone;
											$data['image'] = $this->dbfun_model->get_data_bys('gallery_id, image_square, title, description','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' and status != 13 and istatus = 3','ogl');
											$view = $this->load->view('cara/cara_detailop.php', $data, TRUE);
											break;
										default:	
											if($data['objects']->metadescription == 'page') {
												$key = trim($data['objects']->title);
												$data['relation'] = $this->dbfun_model->get_data_bys('ccs_o.image_square, ccs_odc.*, ccs_adm.name','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_o.ccs_id = '.$fascia.' and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_key = '.$essentiel_id.' and ccs_odc.title = "'.$key.'" and ccs_o.object_id = ccs_odc.object_id and ccs_odc.createdby = ccs_adm.admin_id and ccs_odc.ccs_id = ccs_adm.ccs_id and ccs_odc.category_id = ccs_oct.category_id and ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.title = "Page" and ccs_odc.category_id != '.$data['objects']->category_id.'','oct, o, odc, adm');
											}elseif($data['objects']->metadescription == 'module'){
												$key = trim($data['objects']->title);
												$data['relation'] = $this->dbfun_model->get_data_bys('ccs_mdl.*, ccs_adm.name as creator','ccs_odc.ccs_id = ccs_o.ccs_id and ccs_o.ccs_id = '.$fascia.' and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.ccs_key = '.$essentiel_id.' and ccs_odc.title = "'.$key.'" and ccs_odc.title = ccs_mdl.name and ccs_mdl.parent_id != 0','mdl, o, odc, adm');
											}
											$view = $this->load->view('cara/cara_detailo.php', $data, TRUE);
											break;
									}
								break;
							}
							break;
						case '1':
							switch($duum){
								case '0':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';												
											$ky = $data['category']->title;
											if($ky == 'Transaction Code'){	
												$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');			
												$view = $this->load->view('naha/naha_detailtrx.php', $data, TRUE);
											}else{											
												$view = $this->load->view('naha/naha_detailo.php', $data, TRUE);
											}
											break;
										case 'product':
											$data['chiave'] = 'seo';
											$view = $this->load->view('naha/naha_detailo.php', $data, TRUE);
											break;
									}
								break;
								case '1':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';
											$view = $this->load->view('kada/kada_detailo.php', $data, TRUE);
											break;
										case 'gallery':
											$data['chiave'] = 'gallery';
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.istatus','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$genus.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' ','odc, adm, o');
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$gallery = $this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' ','ogl');
											$data['gstatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
											$data['gcount'] = count($gallery);
											if($data['gcount'] != 0){							
												$data['gallery'] = $this->dbfun_model->get_all_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.object_id = '.$id.' and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');																	
											}
											$view = $this->load->view('pada/pada_detailgal.php', $data, TRUE);
											break;	
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('kada/kada_detailo.php', $data, TRUE);
											break;
									}
								break;
								case '3':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';
											$view = $this->load->view('tasa/tasa_detailo.php', $data, TRUE);
											break;
										case 'gallery':
											$data['chiave'] = 'gallery';
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.istatus','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$genus.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' ','odc, adm, o');
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$gallery = $this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' ','ogl');
											$data['gstatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
											$data['gcount'] = count($gallery);
											if($data['gcount'] != 0){							
												$data['gallery'] = $this->dbfun_model->get_all_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.object_id = '.$id.' and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');																	
											}
											$view = $this->load->view('pada/pada_detailgal.php', $data, TRUE);
											break;	
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('tasa/tasa_detailo.php', $data, TRUE);
											break;
									}
								break;
								case '8':
									$view = $this->load->view('nyama/nyama_detailo.php', $data, TRUE);
								break;
							}
							break;
						case '5':
							switch($duum){
								case '0':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';
											$view = $this->load->view('pada/pada_detailo.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('pada/pada_detailo.php', $data, TRUE);
											break;
										case 'gallery':
											$data['chiave'] = 'gallery';
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.istatus','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$genus.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' ','odc, adm, o');
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$gallery = $this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' ','ogl');
											$data['gstatus'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
											$data['gcount'] = count($gallery);
											if($data['gcount'] != 0){							
												$data['gallery'] = $this->dbfun_model->get_all_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.object_id = '.$id.' and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');																	
											}
											$view = $this->load->view('pada/pada_detailgal.php', $data, TRUE);
											break;
										case 'price':
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';					
											$data['chiave'] = 'price';
											$data['price'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' and category_id = '.$genus.' order by price_id ASC','prc');
											$var = $data['price']->vstatus;
											if($var == 0){
												$data['stock'] = $data['price']->stock;													
												$data['sold'] = $data['price']->sold;
												if($data['price']->dstatus == 1){		
													$segretissimo = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "promo"','mdl')->module_id;
													$data['discount'] = $this->dbfun_model->get_data_bys('discount, stock','ccs_id = '.$fascia.' and ccs_key = '.$segretissimo.' and oid = '.$id.' and cat_id = '.$genus.' and url_id = '.$essentiel_id.'','tkt');
												}
											}else{
												$data['stock'] = $this->dbfun_model->get_data_bys('SUM(stock) as stock','object_id = '.$id.' and ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.'','prv')->stock;
												$data['sold'] = $this->dbfun_model->get_data_bys('SUM(sold) as sold','object_id = '.$id.' and ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.'','prv')->sold;
											}
											$key = count($this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product_settings" and status = 1','mdl'));
											if($key == 0){
												$data['key'] = 0;
											}else{
												$data['key'] = 1;
											}											
											$key = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product_settings"','mdl')->module_id;
											$keypro = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "promo"','mdl')->module_id;
											$trx = count($this->dbfun_model->get_all_data_bys('varian_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id =  '.$id.' and category_id = '.$genus.'','prv'));
											if($trx != 0){															
												$promo = $this->dbfun_model->get_all_data_bys('dstatus, varian_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id =  '.$id.' and category_id = '.$genus.'','prv');		
												
												foreach($promo as $p){
													$pnp = $this->dbfun_model->get_data_bys('ccs_oct.title as category, ccs_oct.parent_title as pcat, ccs_prv.varian_id, ccs_prv.settings_id, ccs_ocs.title, ccs_prv.basic, ccs_prv.publish, ccs_prv.stock, ccs_prv.sold, ccs_prv.status, ccs_adm.name, ccs_prv.datecreated, ccs_prv.dstatus','ccs_ocs.ccs_id = ccs_prv.ccs_id and ccs_ocs.ccs_id = '.$fascia.' and ccs_ocs.ccs_key = ccs_prv.url_id and ccs_ocs.ccs_key = '.$key.' and ccs_prv.ccs_key = '.$essentiel_id.' and ccs_prv.category_id = '.$genus.' and object_id = '.$id.' and ccs_prv.varian_id = '.$p->varian_id.' and ccs_ocs.settings_id = ccs_prv.settings_id and ccs_prv.createdby = ccs_adm.admin_id and ccs_prv.ccs_id = ccs_adm.ccs_id and ccs_oct.ccs_id = ccs_ocs.ccs_id and ccs_oct.category_id = ccs_ocs.category_id and ccs_oct.ccs_key = ccs_ocs.ccs_key and ccs_ocs.language_id = ccs_oct.language_id and ccs_ocs.language_id = 2','oct, ocs, prv, adm ');
													
													if($p->dstatus == 1){
														$dsc = $this->dbfun_model->get_data_bys('discount, stock as tstock, sold as tsold, status as tstatus','ccs_id = '.$fascia.' and ccs_key = '.$keypro.' and oid = '.$id.' and cat_id = '.$genus.' and url_id = '.$essentiel_id.' and varian_id = '.$p->varian_id.'','tkt');
													}elseif($p->dstatus == 0){
														$dsc = $this->dbfun_model->get_data_bys('discount, stock as tstock, sold as tsold, status as tstatus','ccs_id = '.$fascia.' and ccs_key = '.$keypro.' and url_id = '.$essentiel_id.' and varian_id = 0','tkt');
													}
													
													$ds3[] = (object) array_merge((array)$pnp,(array)$dsc);
												}
												$data['trx'] = $ds3; 
												//print_r($data['trx']);
											}
											
											$data['c_trx'] = $trx;
											$view = $this->load->view('pada/pada_detailprice.php', $data, TRUE);
											break;	
										case 'pnp':																	
											$data['chiave'] = 'pnp';	
											$key = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product_settings"','mdl')->module_id;
											$data['varian'] = $this->dbfun_model->get_all_data_bys('title as varian, category_id as varian_id, ccs_key, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$key.' and parent_id != 0','oct');
											$data['key'] = $key;
											$data['price'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' and category_id = '.$genus.' order by price_id ASC','prc');
											$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info, ccs_sts.status','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
											$view = $this->load->view('pada/pada_detailvarian.php', $data, TRUE);
											break;
									}										
								break;
								case '3':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';												
											$view = $this->load->view('gaba/gaba_detailo.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';												
											$view = $this->load->view('gaba/gaba_detailo.php', $data, TRUE);
											break;
										case 'promo':
											$data['chiave'] = 'promo';	
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$segretis = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product"','mdl')->module_id;
											$simo = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product_settings"','mdl')->module_id;		
											$this->load->model('Modules_model');
											$genus = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$fascia.' and object_id = '.$id.' and ccs_key = '.$essentiel_id.'','odc')->category_id;
											$data['pcount'] = count($this->Modules_model->get_promo($fascia, $essentiel_id, $genus, $id, $segretis));
											if($data['pcount'] != 0){
												$data['promo'] = $this->Modules_model->get_promo($fascia, $essentiel_id, $genus, $id, $segretis);
											}
											$view = $this->load->view('gaba/gaba_detailpromo.php', $data, TRUE);
											break;
										case 'product':
											$data['chiave'] = 'product';
											$keys = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "'.$chiave.'"','mdl')->module_id;
											$product = count($this->dbfun_model->get_all_data_bys('category_id','ccs_id = '.$fascia.' and ccs_key = '.$keys.' and language_id = '.$lingua.' and parent_id != 0','oct'));
											if($product != 0){
												$data['parole'] = $keys;
												$data['product'] = $this->dbfun_model->get_all_data_bys('category_id, parent_id, parent_title, title','ccs_id = '.$fascia.' and ccs_key = '.$keys.' and language_id = '.$lingua.' and parent_id != 0','oct');
											}else{													
												$data['parole'] = $keys;
												$data['product'] = $this->dbfun_model->get_all_data_bys('category_id, parent_id, parent_title, title','ccs_id = '.$fascia.' and ccs_key = '.$keys.' and language_id = '.$lingua.' and parent_id = 0','oct');
											}
											$view = $this->load->view('gaba/gaba_newpro.php', $data, TRUE);
											break;
									}
								break;
							}
							break;								
					}
				}
				elseif($part == 'edit_object'){
					$data['language_id'] = 2;
					$id = $this->input->post('id');		
					$chiave = $this->input->post('url3');						
					$data['ccs_key'] = $essentiel_id;
					$data['zone'] = $zone;
					
					$parole = $this->dbfun_model->get_data_bys('link','ccs_id = '.$this->zone_id.' and module_id = '.$essentiel_id.'','mdl')->link;
					if($parole == 'promo'){
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');
					}else{
						$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');						
					}
					$cek_category = $this->dbfun_model->get_data_bys('ccs_odc.*,ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.object_id = '.$id.' and ccs_o.object_id = ccs_odc.object_id and ccs_o.ccs_key = ccs_odc.ccs_key','o,odc');
					if(isset($cek_category->category_id)){
						if($cek_category->category_id == 0){//uncategory
							$cats = (object) array(
												'parent_id'=>0,
												'category'=>'uncategory',
												'parent_title'=>'',
											);
							$data['objects'] = (object) array_merge((array)$cek_category, (array)$cats);
						}else{
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.category_id = ccs_odc.category_id','odc, adm, o, oct');
						}
					}else{
						$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.category_id = ccs_odc.category_id','odc, adm, o, oct');
					}
					
					switch($virato){
						case '0':
							switch($duum){
								case '0':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';												
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id = 0','oct');
											$view = $this->load->view('hana/hana_update.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('hana/hana_update.php', $data, TRUE);
											break;
										case 'relation':
											$data['chiave'] = 'relation';
											$data['modul'] = $this->dbfun_model->get_all_data_bys('module_id, name','ccs_id = '.$fascia.' and ftype = 1','mdl');
											$rel = count($this->dbfun_model->get_all_data_bys('rl_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.'','orl'));
											if($rel != 0){
												$data['relation'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.'','orl');
											}
											$data['rcount'] = $rel;
											$view = $this->load->view('hana/hana_update.php', $data, TRUE);
											break;												
									}
								break;
								case '8':
									switch($chiave){
										case 'menu':
											break;												
										case 'konten':	
											$data['chiave'] = 'konten';			
											$view = $this->load->view('cara/cara_updated.php', $data, TRUE);
											break;
										case 'seo':	
											$data['chiave'] = 'seo';			
											$view = $this->load->view('cara/cara_updated.php', $data, TRUE);
											break;
										default:
											$data['page'] =  $this->dbfun_model->get_all_data_bys('ccs_odc.title, ccs_odc.object_id','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id=ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.language_id = ccs_odc.language_id and ccs_oct.title like "Page" and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = ccs_odc.category_id','oct, odc');
											$data['category'] = $this->dbfun_model->get_data_bys('ccs_oct.*, ccs_adm.name','ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_id = '.$fascia.' and ccs_odc.object_id = '.$id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_adm.admin_id = ccs_oct.createdby ','oct, adm, odc');
											$cat = $data['category']->category_id;
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.*, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.category_id = '.$cat.' and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.'  ','odc, adm, o');
											$view = $this->load->view('cara/cara_update.php', $data, TRUE);
											break;
									}
								break;
							}
							break;
						case '1':
							switch($duum){
								case '0':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';	
											
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_ocs.*, ccs_adm.name, ccs_oct.title as category','ccs_ocs.ccs_key = '.$essentiel_id.' and ccs_ocs.language_id = '.$lingua.' and ccs_ocs.ccs_id = '.$fascia.' and ccs_adm.admin_id = ccs_ocs.createdby and ccs_ocs.settings_id = '.$id.' and ccs_oct.category_id = ccs_ocs.category_id and ccs_oct.ccs_id = ccs_ocs.ccs_id and ccs_oct.ccs_key = ccs_ocs.ccs_key','ocs, adm, oct');
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id != 0','oct');
											$ky = $data['objects']->category;
											if($ky == 'Transaction Code'){	
												$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');			
												$view = $this->load->view('naha/naha_updatetrx.php', $data, TRUE);
											}else{											
												$view = $this->load->view('naha/naha_update.php', $data, TRUE);
											}
											break;
										}
									break;	
								case '1':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';				
											if($parole == 'branch'){
												$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.'','oct');
												$data['loc'] = $this->dbfun_model->get_data_bys('latitude, longitude','ccs_key = '.$essentiel_id.' and ccs_id = '.$fascia.' and object_id = '.$id.'','olc');
											}else{
												$data_cek = array();
												$p_cat = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id = 0','oct');
												foreach($p_cat as $cp){
													$ceks = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id = '.$cp->category_id.'','oct');
													if(!empty($ceks)){
														foreach($ceks as $cc){
															$data_cek[] = (object) array('title'=>$cc->title,'category_id'=>$cc->category_id,'parent_title'=>$cc->parent_title);
														}
													}else{
														$data_cek[] = (object) array('title'=>$cp->title,'category_id'=>$cp->category_id,'parent_title'=>$cp->parent_title);
													}
												}
												$data['category'] = $data_cek;
											}
											$view = $this->load->view('kada/kada_update.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('kada/kada_update.php', $data, TRUE);
											break;
									}
									break;	
								case '3':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';												
											$data_cek = array();
												$p_cat = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id = 0','oct');
											foreach($p_cat as $cp){
												$ceks = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id = '.$cp->category_id.'','oct');
												if(!empty($ceks)){
													foreach($ceks as $cc){
														$data_cek[] = (object) array('title'=>$cc->title,'category_id'=>$cc->category_id,'parent_title'=>$cc->parent_title);
													}
												}else{
													$data_cek[] = (object) array('title'=>$cp->title,'category_id'=>$cp->category_id,'parent_title'=>$cp->parent_title);
												}
											}
											$data['category'] = $data_cek;
											$view = $this->load->view('tasa/tasa_update.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('tasa/tasa_update.php', $data, TRUE);
											break;
									}
									break;	
							}
							break;
						case '5':
							switch($duum){
								case '0':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';
											$cat = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id = 0','oct');
											foreach($cat as $cat){
												$cats[] = (object) array_merge((array)$cat, array('child'=> $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.' and parent_id = '.$cat->category_id.'','oct')));
											}												
											$data['category'] = $cats;
											$data['tags'] = array();
											$tags = $this->dbfun_model->get_all_data_bys('tags','ccs_id = '.$fascia.' and ccs_key = '.$ccs_key.' and tags != ""','odc');
											$data['tags'] = $tags;
											$view = $this->load->view('pada/pada_update.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('pada/pada_update.php', $data, TRUE);
											break;
										case 'price':
											$data['chiave'] = 'price';
											$genus = $data['objects']->category_id;
											$data['price'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and object_id = '.$id.' and category_id = '.$genus.' order by price_id ASC','prc');
											$data['var'] = $data['price']->vstatus;
											$var = $data['var'];
											if($var == 1){
												$data['stock'] = $this->dbfun_model->get_data_bys('SUM(stock) as stock','object_id = '.$id.' and ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.'','prv')->stock;
											}else{
												$data['stock'] = $data['price']->stock;
											}
											$view = $this->load->view('pada/pada_updateprice.php', $data, TRUE);
											break;
									}
									break;									
								case '3':
									switch($chiave){
										case 'konten':
											$data['chiave'] = 'konten';												
											$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and language_id = '.$lingua.'','oct');
											$view = $this->load->view('gaba/gaba_update.php', $data, TRUE);
											break;
										case 'seo':
											$data['chiave'] = 'seo';
											$view = $this->load->view('gaba/gaba_update.php', $data, TRUE);
											break;
									}
								break;
							}
							break;
					}
				}
				elseif($part == 'edit_custom'){
					$key = $this->input->post('url3');
					$data['zone'] = $zone;
					
					switch($key){
						case 'pnp':
							$id = $this->input->post('id');
							$lingua = 2;
							$key = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product_settings"','mdl')->module_id;
							
							$data['trx'] = $this->dbfun_model->get_data_bys('ccs_prv.*, ccs_ocs.title as varian, ccs_oct.category_id as category, ccs_oct.title as settings, ccs_oct.parent_title, ccs_oct.parent_id','ccs_ocs.ccs_id = ccs_prv.ccs_id and ccs_ocs.ccs_id = '.$fascia.' and ccs_ocs.ccs_key = ccs_prv.url_id and ccs_prv.varian_id ='.$id.' and ccs_ocs.settings_id = ccs_prv.settings_id and ccs_oct.ccs_id =ccs_prv.ccs_id and ccs_oct.ccs_id = ccs_ocs.ccs_id and ccs_oct.ccs_key = ccs_ocs.ccs_key and ccs_ocs.ccs_key = '.$key.' and ccs_oct.category_id = ccs_ocs.category_id ','ocs,prv, oct ');
							$data['settings'] = $this->dbfun_model->get_all_data_bys('title as varian, category_id as varian_id, ccs_key, parent_title','ccs_id = '.$fascia.' and ccs_key = '.$key.' and parent_id = '.$data['trx']->parent_id.'','oct');
							$data['varian'] = $this->dbfun_model->get_all_data_bys('title, settings_id','ccs_id = '.$fascia.' and ccs_key = '.$key.' and category_id = '.$data['trx']->category.'','ocs');
							$trx = $data['trx'];
							$segretis = $trx->object_id;
							$simo = $trx->ccs_key;
							$genus = $trx->category_id;
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.ccs_key, ccs_odc.object_id, ccs_odc.title, ccs_o.image_square, ccs_odc.category_id, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title','ccs_odc.ccs_key = '.$simo.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.' and ccs_o.object_id = ccs_odc.object_id and ccs_odc.object_id = '.$segretis.' and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.category_id = ccs_odc.category_id and ccs_oct.category_id = '.$genus.'','odc, o, oct');
							$data['price'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$simo.' and object_id = '.$segretis.' and category_id = '.$genus.'','prc');
							if($trx->publish != $data['price']->publish){
								$data['chiave'] = 1;
							}else{
								$data['chiave'] =  0;
							}
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info, ccs_sts.status','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');
							$view = $this->load->view('pada/pada_updatevarian.php', $data, TRUE);
							break;
						case 'role_edit':
							$lang = $this->input->post('language_id');
							if($lang == 1){
								$segretissimo = 'admin';
								$lingua = 2;
							}elseif($lang == 2){
								$segretissimo = 'member';
								$lingua = 2;
							}
							$id = $this->input->post('id');
							//$lingua = 2;						
							$data['ccs_key'] = $essentiel_id;
							$data['chiave'] = $id;
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');	
							switch($segretissimo){
								case 'admin':
									$data['chiave'] = $segretissimo;
									$data['objects'] = $this->dbfun_model->get_data_bys('ccs_adm.*, ccs_oct.category_id, ccs_oct.title, ccs_oct.parent_id, ccs_oct.parent_title','ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.category_id = ccs_adm.role_id and ccs_adm.admin_id = '.$id.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_key = '.$essentiel_id.'','oct, adm');	
									$data['category'] = $this->dbfun_model->get_all_data_bys('title, category_id','ccs_id = '.$fascia.' and language_id = '.$lingua.' and ccs_key = '.$essentiel_id.' and parent_id = '.$data['objects']->parent_id.' and status = 1','oct');
									$view = $this->load->view('thanga/thanga_update.php', $data, TRUE);
									break;
								case 'member':			
									$data['chiave'] = $segretissimo;
									$data['objects'] = $this->dbfun_model->get_data_bys('ccs_usr.*, ccs_adm.name as creator, ccs_usr.avatar as main_image','ccs_usr.ccs_id = ccs_adm.ccs_id and ccs_usr.ccs_id = '.$fascia.' and ccs_usr.user_id = '.$id.' and ccs_usr.createdby = ccs_adm.admin_id and ccs_usr.type_id = 2','usr, adm');	
									if($data['objects']->reg_id == ''){
										$data['category'] = $this->dbfun_model->get_data_bys('title, category_id, parent_title, parent_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and title = "Registered Member" and parent_id = '.$data['objects']->type_id.'','oct');
										$data['verified'] = $this->dbfun_model->get_data_bys('title, category_id, parent_title, parent_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and title = "Verified Member" and parent_id = '.$data['objects']->type_id.'','oct');
									}else{
										$data['category'] = $this->dbfun_model->get_data_bys('title, category_id, parent_title, parent_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and title = "Verified Member" and parent_id = '.$data['objects']->type_id.'','oct');												
									}
									$view = $this->load->view('thanga/thanga_update.php', $data, TRUE);
									break;
							}
							break;
						case 'main_gallery':
							$data['category_image'] = TRUE;
							$lingua = $this->input->post('language_id');
							$id = $this->input->post('id');
							$data['chiave'] = $key;
							$data['ccs_key'] = $essentiel_id;
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');	
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.object_id, ccs_odc.title, ccs_odc.tagline, ccs_o.status, ccs_adm.name, ccs_o.image_square, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_o.istatus, ccs_oct.parent_id, ccs_oct.category_id , ccs_oct.title as category, ccs_oct.parent_title as pcat','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.'  and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_odc.object_id = '.$id.' and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_odc.category_id = ccs_oct.category_id and ccs_odc.language_id = ccs_oct.language_id','odc, adm, o, oct');	
							$view = $this->load->view('pada/pada_updategal.php', $data, TRUE);
							break;
						case 'gallery':
							$data['category_image'] = TRUE;
							$lingua = $this->input->post('language_id');
							$id = $this->input->post('id');
							$data['chiave'] = $key;
							$data['ccs_key'] = $essentiel_id;
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');	
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_ogl.*, ccs_odc.title as object, ccs_oct.parent_id, ccs_oct.category_id , ccs_oct.title as category, ccs_oct.parent_title as pcat','ccs_odc.ccs_key = '.$essentiel_id.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_id = '.$fascia.'  and ccs_o.object_id = ccs_odc.object_id and ccs_adm.admin_id = ccs_odc.createdby and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_odc.category_id = ccs_oct.category_id and ccs_odc.language_id = ccs_oct.language_id and ccs_ogl.ccs_id = ccs_odc.ccs_id and ccs_ogl.gallery_id = '.$id.' and ccs_ogl.ccs_key = ccs_odc.ccs_key and ccs_ogl.object_id = ccs_odc.object_id and ccs_ogl.category_id = ccs_odc.category_id and ccs_ogl.language_id = ccs_odc.language_id','odc, adm, o, oct, ogl');
							if(empty($data['objects'])){
								$cek = $this->dbfun_model->get_data_bys('ccs_ogl.*, ccs_adm.name','ccs_ogl.ccs_id = '.$fascia.' and ccs_ogl.ccs_key = '.$essentiel_id.' and ccs_ogl.gallery_id = '.$id.' and ccs_adm.ccs_id = ccs_ogl.ccs_id and ccs_adm.admin_id = ccs_ogl.createdby','ogl, adm');
								$cat = $this->dbfun_model->get_data_bys('title, category_id, parent_title, parent_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and category_id = '.$cek->category_id.'','oct');
								$data['objects'] = (object) array_merge((array)$cek, array('category' => $cat->title));
							}
							$view = $this->load->view('pada/pada_updategal.php', $data, TRUE);
							break;
						case 'varian':
							$lingua = $this->input->post('language_id');
							$id = $this->input->post('id');
							$data['chiave'] = $key;
							$data['ccs_key'] = $essentiel_id;
							$product = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product"','mdl')->module_id;	
							$trxs = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "transactions"','mdl')->module_id;							
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_tkt.*, ccs_adm.name, ccs_odc.title, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat, ccs_prv.stock as pstock, ccs_prv.sold as psold, ccs_prv.dstatus, ccs_ocs.title as varian, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_prv.basic, ccs_prv.publish','ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_prv.ccs_id and ccs_ocs.ccs_id = ccs_odc.ccs_id and ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_odc.object_id = ccs_tkt.object_id and ccs_oct.category_id = ccs_tkt.category_id and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_oct.ccs_key = ccs_tkt.ccs_key and ccs_tkt.ccs_key = '.$essentiel_id.' and ccs_tkt.url_id = '.$product.' and ccs_prv.ccs_key = ccs_prv.ccs_key and ccs_tkt.createdby = ccs_adm.admin_id and ccs_oct.category_id = ccs_odc.category_id and ccs_tkt.ticket_id = '.$id.' and ccs_prv.varian_id = ccs_tkt.varian_id and ccs_ocs.settings_id = ccs_prv.settings_id','oct, odc, tkt, adm, prv, ocs, o');
							$data['product'] = $this->dbfun_model->get_data_bys('ccs_o.image_square, ccs_odc.title, ccs_odc.category_id, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat','ccs_o.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = '.$fascia.' and ccs_o.ccs_key =  ccs_odc.ccs_key and ccs_o.ccs_key = '.$product.' and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = '.$data['objects']->oid.' and ccs_odc.category_id = '.$data['objects']->cat_id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_oct.language_id = ccs_odc.language_id and ccs_odc.language_id = '.$lingua.'','o, odc, oct');
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');	
							$view = $this->load->view('gaba/gaba_updateproduct.php', $data, TRUE);
							break;
						case 'price':
							$lingua = $this->input->post('language_id');
							$id = $this->input->post('id');
							$data['chiave'] = $key;
							$data['ccs_key'] = $essentiel_id;
							$product = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product"','mdl')->module_id;	
							$trxs = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "transactions"','mdl')->module_id;							
							$data['objects'] = $this->dbfun_model->get_data_bys('ccs_tkt.*, ccs_adm.name, ccs_odc.title, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat, ccs_prc.stock as pstock, ccs_prc.sold as psold, ccs_prc.dstatus, ccs_prc.vstatus as varian, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_prc.basic, ccs_prc.publish','ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_prc.ccs_id and ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_oct.ccs_key = ccs_tkt.ccs_key and ccs_tkt.ccs_key = '.$essentiel_id.' and ccs_tkt.url_id = '.$product.' and ccs_prc.ccs_key = ccs_prc.ccs_key and ccs_prc.object_id = ccs_tkt.oid and ccs_tkt.createdby = ccs_adm.admin_id and ccs_oct.category_id = ccs_odc.category_id and ccs_tkt.ticket_id = '.$id.' and ccs_tkt.varian_id = 0 and ccs_odc.object_id = ccs_tkt.object_id and ccs_oct.category_id = ccs_tkt.category_id ','oct, odc, tkt, adm, prc, o');
							$data['product'] = $this->dbfun_model->get_data_bys('ccs_o.image_square, ccs_odc.title, ccs_odc.category_id, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat','ccs_o.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = '.$fascia.' and ccs_o.ccs_key =  ccs_odc.ccs_key and ccs_o.ccs_key = '.$product.' and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = '.$data['objects']->oid.' and ccs_odc.category_id = '.$data['objects']->cat_id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_oct.language_id = ccs_odc.language_id and ccs_odc.language_id = '.$lingua.'','o, odc, oct');
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 0 order by ccs_sts.value asc ','sts');	
							$view = $this->load->view('gaba/gaba_updateproduct.php', $data, TRUE);
							break;
					}			
				}
				elseif($part == 'detail_custom'){
					$data['language_id'] = 2;
					$id = $this->input->post('id');		
					$chiave = $this->input->post('url3');						
					$data['ccs_key'] = $essentiel_id;
					$data['zone'] = $zone;
					
					switch($virato){
						case '0':
							if($type != 'role'){
								$data['objects'] = $this->dbfun_model->get_data_bys('ccs_orc.*, ccs_oct.title as category, ccs_adm.name, ccs_oct.datecreated','ccs_oct.ccs_id = ccs_orc.ccs_id and ccs_orc.ccs_id = ccs_adm.ccs_id and ccs_orc.ccs_id = '.$fascia.' and ccs_orc.ccs_key = ccs_oct.ccs_key and ccs_orc.ccs_key = '.$essentiel_id.' and ccs_oct.category_id = ccs_orc.category_id and ccs_oct.language_id = '.$lingua.' and ccs_orc.rc_id = '.$id.' and ccs_adm.admin_id = ccs_oct.createdby','orc, oct, adm');									
							}								
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 1 order by ccs_sts.value asc ','sts');		
							
							switch($duum){
								case '2':
									switch($chiave){
										case 'interval':
											$data['chiave'] = 'interval';
											$data['parole'] = 'detail';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
										case 'bank':
											$data['chiave'] = 'bank';
											$data['parole'] = 'detail';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
										case 'info':
											$data['chiave'] = 'info';
											$data['parole'] = 'detail';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
										case 'email':
											$data['chiave'] = 'email';
											$data['parole'] = 'detail';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
										case 'edit_interval':
											$data['parole'] = 'edit';
											$data['chiave'] = 'edit_interval';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
										case 'edit_bank':
											$data['parole'] = 'edit';
											$data['chiave'] = 'edit_bank';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
										case 'edit_info':
											$data['parole'] = 'edit';
											$data['chiave'] = 'edit_info';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
										case 'edit_email':
											$data['parole'] = 'edit';
											$data['chiave'] = 'edit_email';
											$view = $this->load->view('daka/daka_detailo.php', $data, TRUE);
											break;
									}
									break;
								case '6':
									$data['admin'] = $this->dbfun_model->get_data_bys('admin_id, name, email, role_id, datecreated, createdby','ccs_id = '.$fascia.' and role_id = 0','adm');							
									switch($chiave){
										case 'admin':
											$data['chiave'] = $chiave;												
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_adm.*, ccs_oct.category_id, ccs_oct.title, ccs_oct.parent_id, ccs_oct.parent_title','ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_oct.ccs_id = '.$fascia.' and ccs_oct.category_id = ccs_adm.role_id and ccs_adm.admin_id = '.$id.' and ccs_oct.language_id = '.$lingua.' and ccs_oct.ccs_key = '.$essentiel_id.'','oct, adm');	
											$data['module'] = $this->dbfun_model->get_all_data_bys('ccs_mdl.name, ccs_mdl.description, ccs_mdl.module_id, ccs_mtr.datecreated, ccs_mtr.status, ccs_mtr.createdby','ccs_mtr.ccs_id = ccs_mdl.ccs_id and ccs_mdl.ccs_id = '.$fascia.' and ccs_mdl.module_id = ccs_mtr.module_id and ccs_mtr.role_id = '.$data['objects']->role_id.'','mtr, mdl');
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$view = $this->load->view('thanga/thanga_detailo.php', $data, TRUE);
											break;
										case 'member':
											$data['chiave'] = $chiave;												
											$data['objects'] = $this->dbfun_model->get_data_bys('ccs_usr.*, ccs_adm.name as creator','ccs_usr.ccs_id = ccs_adm.ccs_id and ccs_usr.ccs_id = '.$fascia.' and ccs_usr.user_id = '.$id.' and ccs_usr.createdby = ccs_adm.admin_id and ccs_usr.type_id = 2','usr, adm');	
											if($data['objects']->reg_id == ''){
												$data['category'] = $this->dbfun_model->get_data_bys('title, category_id, parent_title, parent_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and title = "Registered Member" and parent_id = '.$data['objects']->type_id.'','oct');
											}else{
												$data['category'] = $this->dbfun_model->get_data_bys('title, category_id, parent_title, parent_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and title = "Verified Member" and parent_id = '.$data['objects']->type_id.'','oct');			
											}
											$view = $this->load->view('thanga/thanga_detailo.php', $data, TRUE);
											break;
									}
									break;
							}
							break;
						case '5':
							$product = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product"','mdl')->module_id;								
							$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 2 order by ccs_sts.value asc ','sts');		
							switch($duum){
							case '1':		
								$data['dod'] = count($this->dbfun_model->get_all_data_bys('trx_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and trx_code = "'.$id.'"','trx'));
								if($data['dod'] == 1){
									$data['objects'] = $this->dbfun_model->get_data_bys('ccs_odc.ccs_key as url_id, ccs_odc.title, ccs_oct.title as category, ccs_trx.*, ccs_inv.inv_id, ccs_inv.inv_code, ccs_inv.icode, ccs_inv.dateinv, ccs_inv.status as i_status, ccs_inv.datecreated, ccs_adm.name, ccs_o.image_square','ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_odc.ccs_id = ccs_trx.ccs_id and ccs_inv.ccs_id = ccs_trx.ccs_id and ccs_trx.ccs_key = ccs_inv.ccs_key and ccs_trx.ccs_id = '.$fascia.' and ccs_oct.ccs_key = '.$essentiel_id.' and ccs_oct.ccs_key = ccs_trx.ccs_key and ccs_oct.category_id = ccs_trx.category_id and ccs_trx.object_id = ccs_odc.object_id and ccs_odc.category_id = ccs_trx.cat_id and ccs_odc.language_id = ccs_oct.language_id and ccs_odc.language_id = '.$lingua.' and ccs_odc.ccs_key = '.$product.' and ccs_inv.type = 0 and ccs_inv.code = ccs_trx.code and ccs_inv.trx_code = ccs_trx.trx_code and ccs_adm.admin_id = ccs_inv.createdby and ccs_trx.trx_code = "'.$id.'" and ccs_o.object_id = ccs_odc.object_id','oct, odc, trx, inv, adm, o');
									if($data['objects']->varian_id == 0){
										$data['stock'] = $this->dbfun_model->get_data_bys('stock, basic, publish, sold, price_id','ccs_id = '.$fascia.' and ccs_key = '.$product.' and category_id = '.$data['objects']->cat_id.' and object_id = '.$data['objects']->object_id.' and status = 1','prc');
										if($data['objects']->ticket_id != 0){
											$data['discount'] = $this->dbfun_model->get_data_bys('tkt.*','ccs_id = '.$fascia.' and ticket_id = '.$data['objects']->ticket_id.' and cat_id = '.$data['objects']->cat_id.' and oid = '.$data['objects']->object_id.' ','tkt');
										}
										$data['varian'] = 0;
									}else{
										$data['stock'] = $this->dbfun_model->get_data_bys('stock, basic, publish, sold, varian_id as price_id, settings_id','ccs_id = '.$fascia.' and ccs_key = '.$product.' and category_id = '.$data['objects']->cat_id.' and object_id = '.$data['objects']->object_id.' and varian_id = '.$data['objects']->varian_id.'','prv');
										if($data['objects']->ticket_id != ''){
											$data['discount'] = $this->dbfun_model->get_data_bys('tkt.*','ccs_id = '.$fascia.' and ticket_id = '.$data['objects']->ticket_id.' and cat_id = '.$data['objects']->cat_id.' and oid = '.$data['objects']->object_id.' and varian_id = '.$data['objects']->varian_id.'','tkt');
										}
										$data['varian'] = $this->dbfun_model->get_data_bys('title','ccs_id = '.$fascia.' and settings_id = '.$data['stock']->settings_id.'','ocs');
									}
									$data['category'] = $this->dbfun_model->get_data_bys('title','ccs_id = '.$fascia.' and ccs_key = '.$product.' and language_id = '.$lingua.' and category_id = '.$data['objects']->cat_id.'','oct');
									$data['shipment'] = $this->dbfun_model->get_data_bys('ticket, price','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_code = "'.$data['objects']->trx_code.'" and object_id = 0','trx');
									$data['shipping'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and info_id = '.$data['objects']->info_id.'','tri');
										
									
								}else{
									$data['objects'] = $this->Modules_model->get_product_by_trx($fascia, $essentiel_id, $product, $id);		
									$list = $this->dbfun_model->get_all_data_bys('distinct(ccs_inv.datecreated),ccs_inv.trx_code,ccs_inv.code,ccs_inv.status,ccs_trx.user_id,ccs_trx.c_status','ccs_trx.ccs_id = '.$this->zone_id.' and ccs_trx.ccs_id = ccs_inv.ccs_id and ccs_trx.trx_code = ccs_inv.trx_code and ccs_trx.code = ccs_inv.code and ccs_trx.trx_code = "'.$id.'"order by ccs_trx.datecreated DESC','ccs_trx,ccs_inv');
							
									$list2 = array();
									foreach($list as $ds){
										$ds2 = $this->dbfun_model->get_all_data_bys('price,total,object_id','user_id = '.$ds->user_id.' and ccs_id = '.$this->zone_id.' and trx_code = '.$ds->trx_code.'','ccs_trx');
										$count_prc = '';
										$total = '';
										if(!empty($ds2)){
											foreach($ds2 as $ds3){
												$count_prc += $ds3->price;
												if($ds3->object_id != 0){
													$total += $ds3->total;
												}
											}
										}
									}
									$tot = (object) array('total'=> $count_prc);
									$data['total'] = array($tot);
									//$data['total'] = $this->Modules_model->get_total_by_trx($fascia, $essentiel_id, $product, $id);	
									$data['shipment'] = $this->dbfun_model->get_data_bys('ticket, price','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_code = "'.$data['objects'][0]->trx_code.'" and object_id = 0','trx');	
									$data['shipping'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and info_id = '.$data['objects'][0]->info_id.'','tri');
																					
									$data['invoice'] = $this->dbfun_model->get_data_bys('code, icode, inv_code','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_code = "'.$data['objects'][0]->trx_code.'"','inv');
									//$data['shipping'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and trx_code = "'.$id.'"','tri');
								}
								$data['chiave'] = $chiave;
								switch($chiave){
									case 'detail':									
										$view = $this->load->view('raca/raca_detailo.php', $data, TRUE);
										break;
									case 'edit':
										$view = $this->load->view('raca/raca_detailo.php', $data, TRUE);
										break;
									case 'invoice':
										$account = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "web_settings"','mdl')->module_id;
										$data['owner'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$account.' and char_7 = "info"','orc');	
										$view = $this->load->view('raca/raca_detaili.php', $data, TRUE);
										break;
									case 'receipt':
										$account = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "web_settings"','mdl')->module_id;
										$data['owner'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$account.' and char_7 = "info"','orc');
										$receipt = $this->dbfun_model->get_data_bys('rcp_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and type = 0 and code = "RCP" and icodex = "'.$id.'" ','rcp');
										if($receipt){
											$data['receipt'] = $this->dbfun_model->get_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and type = 0 and code = "RCP" and icodex = "'.$id.'" ','rcp');
										}		
										$data['rcp'] = $receipt;
										$view = $this->load->view('raca/raca_detaili.php', $data, TRUE);
										break;
									case 'confirmation':
										$cnf = count($this->dbfun_model->get_data_bys('inv_id','ccs_id ='.$fascia.' and ccs_key ='.$essentiel_id.' and code = "CNF" and inv_code = "'.$id.'" and type = 2 ','inv'));
										if(($cnf != 0)&&( $data['objects'][0]->code == 'TRXON')){
											$data['cnf'] = $this->dbfun_model->get_data_bys('ccs_inv.*, ccs_adm.name','ccs_inv.ccs_id ='.$fascia.' and ccs_inv.ccs_key ='.$essentiel_id.' and ccs_inv.code = "CNF" and ccs_inv.inv_code = "'.$id.'"  and ccs_inv.type = 2 and ccs_adm.ccs_id = ccs_inv.ccs_id and ccs_inv.updatedby = ccs_adm.admin_id order by inv_id DESC','inv, adm');
											//print_r($id);
										}
										$data['ccount'] = $cnf;
										$view = $this->load->view('raca/raca_detailo.php', $data, TRUE);
										break;
									case 'delivery':
										$receipt = $this->dbfun_model->get_data_bys('rcp_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and type = 0 and code = "RCP" and icodex = "'.$id.'" ','rcp');
										$data['trxcode'] = $id;
										if($receipt){								
											$data['status'] = $this->dbfun_model->get_all_data_bys('ccs_sts.status_id, ccs_sts.value, ccs_sts.invert, ccs_sts.name, ccs_sts.icon, ccs_sts.color, ccs_sts.info','ccs_sts.ccs_id = 2 and ccs_sts.ccs_key = 5 order by ccs_sts.value asc ','sts');		
											$dlv = $this->dbfun_model->get_data_bys('rcp_id','ccs_id = '.$fascia.' and ccs_key = '.$essentiel_id.' and type = 1 and code = "DLV" and codex = "'.$id.'" ','rcp');
											if($dlv){
												$data['delivery'] = $this->dbfun_model->get_data_bys('ccs_rcp.*, ccs_adm.name','ccs_rcp.ccs_id = '.$fascia.' and ccs_rcp.ccs_key = '.$essentiel_id.' and ccs_rcp.type = 1 and ccs_rcp.code = "DLV" and ccs_rcp.codex = "'.$id.'" and ccs_adm.ccs_id = ccs_rcp.ccs_id and ccs_rcp.updatedby = ccs_adm.admin_id','rcp, adm');
											}
											$data['dlv'] =  $dlv;
										}		
										$data['rcp'] = $receipt;
										$view = $this->load->view('raca/raca_detailo.php', $data, TRUE);
										break;
								}
								break;
							case '3':									
								$trxs = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "transactions"','mdl')->module_id;
								switch($chiave){
									case 'varian':
										$data['chiave'] = $chiave;
										$data['objects'] = $this->dbfun_model->get_data_bys('ccs_tkt.*, ccs_adm.name, ccs_odc.title, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat, ccs_prv.stock as pstock, ccs_prv.sold as psold, ccs_prv.dstatus, ccs_ocs.title as varian, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_prv.basic, ccs_prv.publish','ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_prv.ccs_id and ccs_ocs.ccs_id = ccs_odc.ccs_id and ccs_odc.ccs_id = ccs_o.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_oct.ccs_key = ccs_tkt.ccs_key and ccs_tkt.ccs_key = '.$essentiel_id.' and ccs_tkt.url_id = '.$product.' and ccs_prv.ccs_key = ccs_prv.ccs_key and ccs_tkt.createdby = ccs_adm.admin_id and ccs_oct.category_id = ccs_odc.category_id and ccs_tkt.ticket_id = '.$id.' and ccs_prv.varian_id = ccs_tkt.varian_id and ccs_ocs.settings_id = ccs_prv.settings_id','oct, odc, tkt, adm, prv, ocs, o');
										$data['product'] = $this->dbfun_model->get_data_bys('ccs_o.image_square, ccs_odc.title, ccs_odc.category_id, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat','ccs_o.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = '.$fascia.' and ccs_o.ccs_key =  ccs_odc.ccs_key and ccs_o.ccs_key = '.$product.' and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = '.$data['objects']->oid.' and ccs_odc.category_id = '.$data['objects']->cat_id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_oct.language_id = ccs_odc.language_id and ccs_odc.language_id = '.$lingua.'','o, odc, oct');
										$trx = count($this->dbfun_model->get_all_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$trxs.' and varian_id = '.$id.' and object_id = '.$data['objects']->oid.' and cat_id = '.$data['objects']->cat_id.'','trx'));
										$data['ctrx'] = $trx;
										if($trx != 0){
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$data['trx'] = $this->dbfun_model->get_all_data_bys('ccs_trx.trx_id, ccs_trx.ticket_id, ccs_trx.total, ccs_trx.status, ccs_trx.c_status, ccs_trx.price, ccs_trx.datecreated, ccs_trx.datesales, ccs_trx.code, ccs_trx.trx_code, ccs_oct.title, ccs_oct.category_id, ccs_adm.name','ccs_trx.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_adm.ccs_id = '.$fascia.' and ccs_oct.ccs_key = ccs_trx.ccs_key and ccs_trx.ccs_key = '.$trxs.' and ccs_trx.createdby = ccs_adm.admin_id and ccs_trx.category_id = ccs_oct.category_id and ccs_trx.varian_id = '.$data['objects']->varian_id.' and ccs_trx.object_id = '.$data['objects']->oid.' order by ccs_trx.datesales desc','trx, oct, adm');
										}
										$view = $this->load->view('gaba/gaba_detailproduct.php', $data, TRUE);
										break;
									case 'price':
										$data['chiave'] = $chiave;
										$data['objects'] = $this->dbfun_model->get_data_bys('ccs_tkt.*, ccs_adm.name, ccs_odc.title, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat, ccs_prc.stock as pstock, ccs_prc.sold as psold, ccs_prc.dstatus, ccs_prc.vstatus as varian, ccs_o.datecreated as datestart, ccs_o.dateupdated as dateend, ccs_prc.basic, ccs_prc.publish','ccs_adm.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_odc.ccs_id and ccs_tkt.ccs_id = ccs_prc.ccs_id and ccs_odc.ccs_id = '.$fascia.' and ccs_o.ccs_key = ccs_odc.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_oct.ccs_key = ccs_tkt.ccs_key and ccs_tkt.ccs_key = '.$essentiel_id.' and ccs_tkt.url_id = '.$product.' and ccs_prc.ccs_key = ccs_prc.ccs_key and ccs_tkt.createdby = ccs_adm.admin_id and ccs_oct.category_id = ccs_odc.category_id and ccs_tkt.ticket_id = '.$id.' and ccs_prc.vstatus = ccs_tkt.varian_id','oct, odc, tkt, adm, prc, o');
										$data['product'] = $this->dbfun_model->get_data_bys('ccs_o.image_square, ccs_odc.title, ccs_odc.category_id, ccs_oct.title as category, ccs_oct.parent_id, ccs_oct.parent_title as pcat','ccs_o.ccs_id = ccs_odc.ccs_id and ccs_oct.ccs_id = ccs_odc.ccs_id and ccs_o.ccs_id = '.$fascia.' and ccs_o.ccs_key =  ccs_odc.ccs_key and ccs_o.ccs_key = '.$product.' and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_o.object_id = ccs_odc.object_id and ccs_o.object_id = '.$data['objects']->oid.' and ccs_odc.category_id = '.$data['objects']->cat_id.' and ccs_odc.category_id = ccs_oct.category_id and ccs_oct.language_id = ccs_odc.language_id and ccs_odc.language_id = '.$lingua.'','o, odc, oct');
										$trx = count($this->dbfun_model->get_all_data_bys('*','ccs_id = '.$fascia.' and ccs_key = '.$trxs.' and varian_id = '.$data['objects']->varian_id.' and object_id = '.$data['objects']->oid.' and cat_id = '.$data['objects']->cat_id.'','trx'));
										$data['ctrx'] = $trx;
										if($trx != 0){
											echo '<script src="'.base_url().'assets/admin/js/modul/datatables.js" type="text/javascript"></script>';							
											$data['trx'] = $this->dbfun_model->get_all_data_bys('ccs_trx.trx_id, ccs_trx.ticket_id, ccs_trx.total, ccs_trx.status, ccs_trx.c_status, ccs_trx.price, ccs_trx.datecreated, ccs_trx.datesales, ccs_trx.code, ccs_trx.trx_code, ccs_oct.title, ccs_oct.category_id, ccs_adm.name','ccs_trx.ccs_id = ccs_oct.ccs_id and ccs_oct.ccs_id = ccs_adm.ccs_id and ccs_adm.ccs_id = '.$fascia.' and ccs_oct.ccs_key = ccs_trx.ccs_key and ccs_trx.ccs_key = '.$trxs.' and ccs_trx.createdby = ccs_adm.admin_id and ccs_trx.category_id = ccs_oct.category_id and ccs_trx.varian_id = '.$data['objects']->varian_id.' and ccs_trx.object_id = '.$data['objects']->oid.' order by ccs_trx.datesales desc','trx, oct, adm');
										}
										$view = $this->load->view('gaba/gaba_detailproduct.php', $data, TRUE);
										break;
								}
								break;
							}
							break;
					}
				}
				elseif($part == 'list_url'){
					$id = $this->input->post('id');
					if($virato == 5){							
						if($duum == 0){								
							$key = $this->input->post('url');						
							if($key == 'product'){
								$k = $this->input->post('url2');					
								$url = $this->dbfun_model->get_all_data_bys('settings_id as url_id, title','ccs_id = '.$fascia.' and ccs_key = '.$k.' and language_id = '.$lingua.' and status = 1 and category_id = '.$id.'','ocs');
								if(count($url) != 0){
									
									echo '<option value="">Select Varian</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'" >'.$url->title.'</option>';
									}
								}else{
									echo '<option value="">There is no varian </option>';
								}
							}
							$view = '';
						}
						elseif($duum == 1){								
							$key = $this->input->post('url');						
							if($key == 'trx'){
								$k = $this->input->post('url2');					
								$url = $this->dbfun_model->get_all_data_bys('ccs_odc.object_id as url_id, ccs_odc.title, ccs_prc.vstatus, ccs_prc.publish, ccs_odc.ccs_key','ccs_odc.ccs_id = '.$fascia.' and ccs_odc.ccs_key = '.$k.' and ccs_odc.language_id = '.$lingua.' and ccs_odc.status = 1 and ccs_odc.category_id = '.$id.' and ccs_prc.ccs_id = ccs_odc.ccs_id and ccs_prc.ccs_key = ccs_odc.ccs_key and ccs_odc.category_id = ccs_prc.category_id and ccs_odc.object_id = ccs_prc.object_id','odc, prc');
								if(count($url) != 0){										
									echo '<option value="">Select Product</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'" data-price="'.number_format($url->publish ,0,',','.').'" data-total= "'.$url->publish.'"   data-ttl= "'.$url->publish.'"  data-status = "'.$url->vstatus.'" data-url2="'.$url->ccs_key.'" data-url="varian"> '.$url->title.'</option>';
									}
								}else{
									echo '<option value="0">There is no product ... </option>';
								}
							}elseif($key == 'varian'){									
								$k = $this->input->post('url2');			
								$keys = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$fascia.' and link = "product_settings"','mdl')->module_id;
								$url = $this->dbfun_model->get_all_data_bys('ccs_prv.varian_id as url_id, ccs_prv.publish, ccs_ocs.title','ccs_prv.ccs_id = '.$fascia.' and ccs_prv.ccs_key = '.$k.' and ccs_prv.url_id = '.$keys.' and ccs_prv.object_id = '.$id.' and ccs_ocs.settings_id = ccs_prv.settings_id and ccs_ocs.ccs_key = ccs_prv.url_id and ccs_prv.ccs_id = ccs_ocs.ccs_id','prv, ocs');
								if(count($url) != 0){										
									echo '<option value="">Select Varian</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'" data-price="'.number_format($url->publish ,0,',','.').'" data-total= "'.$url->publish.'" data-ttl= "'.$url->publish.'" > '.$url->title.'</option>';
									}
								}else{
									echo '<option value="0">There is no product ... </option>';
								}
							}
							$view = '';
						}
						elseif($duum == 3){
							$key = $this->input->post('url');						
							if($key == 'product'){
								$k = $this->input->post('url2');					
								$url = $this->dbfun_model->get_all_data_bys('object_id as url_id, title','ccs_id = '.$fascia.' and ccs_key = '.$k.' and language_id = '.$lingua.' and status = 1 and category_id = '.$id.'','odc');
								if(count($url) != 0){										
									echo '<option value="">Select Product</option><option value="0">All Product</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'" >'.$url->title.'</option>';
									}
								}else{
									echo '<option value="0">There is no product ... </option>';
								}
							}
							$view = '';
						}							
					}elseif($virato == 0){
						if($duum == 0){								
							$key = $this->input->post('url');						
							if($key == 'module'){					
								$url = $this->dbfun_model->get_all_data_bys('category_id as url_id, title, ccs_key','ccs_id = '.$fascia.' and ccs_key = '.$id.' and language_id = '.$lingua.' and status = 1 ','oct');
								if(count($url) != 0){
									echo '<option value="">Select Category</option>';
									echo '<option value="0">Only Module</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'"  data-url2="'.$url->url_id.'" data-url3="'.$url->ccs_key.'" data-url="category"  >'.$url->title.'</option>';
									}
								}else{
									echo '<option value="">There is no category </option>';
								}
							}elseif($key == 'category'){	
								$cat = $this->input->post('url2');
								$mdl = $this->input->post('url3');
								$url = $this->dbfun_model->get_all_data_bys('object_id as url_id, title','ccs_id = '.$fascia.' and ccs_key = '.$mdl.' and language_id = '.$lingua.' and status = 1 and category_id = '.$cat.' ','odc');
								if(count($url) != 0){
									
									echo '<option value="">Select Content</option>';
									echo '<option value="0">Only Category</option>';
									foreach($url as $url){
										echo '<option value="'.$url->url_id.'" >'.$url->title.'</option>';
									}
								}else{
									echo '<option value="0">Only Category </option>';
								}
							}
							$view = '';
						}
						
					}
				}
				elseif($part == 'gallery'){
					
				}
				elseif($part == 'update_gal'){
					
				}
				
				echo $view;
			}
		}else{
			redirect($this->login);
		}
	}
	
	public function status($zone, $type){
		if ($this->s_login && !empty($type)){
			$data = array();
    		$status = '';
    		$message = '';
			$cek = $this->dbfun_model->get_data_bys('type_id, type_view, module_id','ccs_id = '.$this->zone_id.' and link like "'.$type.'"','ccs_mdl');
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			
			$this->form_validation->set_rules('id', 'Zone Status', 'trim|required|numeric|xss_clean');
			$id = $this->input->post('id');
			$param = $this->input->post('param');
			$key_link = $this->input->post('url');
			
			if ($this->form_validation->run() === TRUE){
				if($custom_module == 5){	
					if($type == 'objects'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							$this->dbfun_model->update_table(array('object_id' => $id), $update, 'odc');
											
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'module'){
						
						$role = $this->input->post('lang');
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							$this->dbfun_model->update_table(array('module_id' => $id, 'role_id' => $role, 'ccs_id' => $this->zone_id), $update, 'mtr');										
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'category'){
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
								'status' => $s
							);
							$this->dbfun_model->update_table(array('category_id' => $id), $update, 'oct');
							if($key_link == 'role'){
								$cek = count($this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.'','adm'));
								if($cek != 0){
									$admin = $this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.'','adm');
									foreach($admin as $a){
										if($s == 0){
											$stato = 2;
										}else{
											$stato = $s;
										}
										$update_adm = array(
											'status' => $stato
										);
										$this->dbfun_model->update_table(array('admin_id' => $a->admin_id, 'role_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
									}								
								}
							}				
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'roles'){					
						$role = $this->input->post('lang');
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							if($role == 'admin'){
								$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update, 'adm');
							}else{
								$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update, 'usr');							
							}
																	
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'gallery'){					
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$update_gal = array(
								'dateupdated' => date('Y-m-d H:i:s'),
								'istatus' => $param
							);											
							
							$cek_gal = count($this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$param.'  ','ogl'));
							if($cek_gal != 0){
								$gid = $this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$param.'','ogl')->gallery_id;
								$update_img = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'updatedby' => $this->admin_id,
									'istatus' => 2
								);											
								$this->dbfun_model->update_table(array('gallery_id' => $gid,'object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_img, 'ogl');	
							}
							
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_gal, 'o');	
							
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
				}
				else{
					if($type == 'objects'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
								'status' => $s
							);
							$this->dbfun_model->update_table(array('object_id' => $id,'ccs_id'=>$this->zone_id), $update, 'odc');
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'module'){
						
						$role = $this->input->post('lang');
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							$this->dbfun_model->update_table(array('module_id' => $id, 'role_id' => $role, 'ccs_id' => $this->zone_id), $update, 'mtr');										
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'category'){
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
								'status' => $s
							);
							$this->dbfun_model->update_table(array('category_id' => $id,'ccs_key'=>$ccs_key,'ccs_id'=>$this->zone_id), $update, 'oct');
							if($key_link == 'role'){
								$cek = count($this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.'','adm'));
								if($cek != 0){
									$admin = $this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$this->zone_id.' and role_id = '.$id.'','adm');
									foreach($admin as $a){
										if($s == 0){
											$stato = 2;
										}else{
											$stato = $s;
										}
										$update_adm = array(
											'status' => $stato
										);
										$this->dbfun_model->update_table(array('admin_id' => $a->admin_id, 'role_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
									}								
								}
							}				
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'user'){					
						$role = $this->input->post('lang');
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							if($role == 'admin'){
								$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update, 'adm');
							}else{
								$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update, 'usr');							
							}
																	
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}elseif($type == 'settings'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							$this->dbfun_model->update_table(array('settings_id' => $id,'ccs_id'=>$this->zone_id), $update, 'ocs');
											
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'gallery'){					
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$update_gal = array(
								'dateupdated' => date('Y-m-d H:i:s'),
								'istatus' => $param
							);											
							
							$cek_gal = count($this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$param.'  ','ogl'));
							if($cek_gal != 0){
								$gid = $this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$param.'','ogl')->gallery_id;
								$update_img = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'updatedby' => $this->admin_id,
									'istatus' => 2
								);											
								$this->dbfun_model->update_table(array('gallery_id' => $gid,'object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_img, 'ogl');	
							}
							
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update_gal, 'o');	
							
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'web'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							$this->dbfun_model->update_table(array('rc_id' => $id), $update, 'orc');
											
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'relation'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							$this->dbfun_model->update_table(array('obm_id' => $id), $update, 'obm');
											
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'menu'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
							'status' => $s
							);
							$this->dbfun_model->update_table(array('menu_id' => $id,'ccs_id'=>$this->zone_id), $update, 'mnu');
											
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'varian'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
								'status' => $s
							);
							$this->dbfun_model->update_table(array('varian_id' => $id,'ccs_id'=>$this->zone_id), $update, 'prv');
											
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'promo'){
						
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$s = $this->dbfun_model->get_data_bys('ccs_sts.value','ccs_sts.status_id = '.$param.' ','sts')->value;
							$update = array(
								'status' => $s
							);
							$this->dbfun_model->update_table(array('ticket_id' => $id,'ccs_id'=>$this->zone_id), $update, 'tkt');
							
							$t = $this->dbfun_model->get_data_bys('url_id, cat_id, oid','ticket_id = '.$id.' ','tkt');
							$tcount = count($this->dbfun_model->get_all_data_bys('ticket_id','cat_id = '.$t->cat_id.' and url_id = '.$t->url_id.' and oid = '.$t->oid.' ','tkt'));
							if($tcount == 1){
								$updates = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'dstatus' => $s,
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('object_id' => $t->oid, 'ccs_key' => $t->url_id, 'category_id' => $t->cat_id ,'ccs_id'=>$this->zone_id), $updates, 'prc');
							}
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'setup'){					
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						if($ccs_key){
							$galtype = trim($this->input->post('param'));
							$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and gallery_id = '.$id.'','ogl')->object_id;
							$update_gal = array(
								'dateupdated' => date('Y-m-d H:i:s'),
								'updatedby' => $this->admin_id,
								'istatus' => $param
							);		
							$cek_main = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$object.' and istatus = '.$param.'','o'));
							if($cek_main != 0){
								$update = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'istatus' => 2
								);
								$this->dbfun_model->update_table(array('object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update, 'o');	
							}
							$cek_gal = count($this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$object.' and istatus = '.$param.' and gallery_id != '.$id.' ','ogl'));
							if($cek_gal != 0){
								$gid = $this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$object.' and istatus = '.$param.' and gallery_id != '.$id.' ','ogl')->gallery_id;
								$update_img = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'updatedby' => $this->admin_id,
									'istatus' => 2
								);											
								$this->dbfun_model->update_table(array('gallery_id' => $gid,'object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_img, 'ogl');	
							}
																
							$this->dbfun_model->update_table(array('gallery_id' => $id,'object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_gal, 'ogl');	
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
					elseif($type == 'sort'){
						$sort = substr($param, 0, 2);
						$sortid = str_replace( $sort,'',$param);
						$ccs_key = $this->dbfun_model->get_data_bys('ccs_mdl.module_id','ccs_mdl.link like  "'.$key_link.'" and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->module_id;
						$category_id = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and menu_id = '.$id.'','mnu')->category_id;
						
						if($sort == 'dn'){							
							$update = array(
								'orders' => $sortid+1
							);
							$dnid = $sortid+1;
							$resort = $this->dbfun_model->get_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$category_id.' and orders = '.$dnid.' and menu_id != '.$id.'','mnu')->menu_id;
							$updates = array(
								'orders' => $sortid
							);
						}elseif($sort == 'up'){							
							$update = array(
								'orders' => $sortid-1
							);							
							$dnid = $sortid-1;
							$resort = $this->dbfun_model->get_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$category_id.' and orders = '.$dnid.' and menu_id != '.$id.'','mnu')->menu_id;
							$updates = array(
								'orders' => $sortid
							);
						}
							
						if($ccs_key){
							
							$this->dbfun_model->update_table(array('menu_id' => $id), $update, 'mnu');
							$this->dbfun_model->update_table(array('menu_id' => $resort), $updates, 'mnu');
											
							$status = 'success';
							$message = 'Data updated successfully';
						}
					}
				}
				
				
			}else{
				$status = 'error';
				$message = validation_errors();
			}
			echo json_encode(array('status' => $status, 'm' => $message));
			exit();
		}
	}
	
	public function delete($zone, $type){
    	if ($this->s_login && !empty($type))
    	{
			$data = array();
    		$status = '';
    		$message = '';
			$id = $this->input->post('id');
			$lang = $this->input->post('lang');
			$key_name = $this->input->post('part');
			$cat = $this->input->post('param');
			$cek = $this->dbfun_model->get_data_bys('type_id, type_view, module_id','ccs_id = '.$this->zone_id.' and link like "'.$key_name.'"','ccs_mdl');
			$type_id = $cek->type_id;
			$type_view = $cek->type_view;
			$ccs_key = $cek->module_id;
			
			if($cat == 'category'){
				
				$this->form_validation->set_rules('id', 'category id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{	
					$key = $this->dbfun_model->get_data_bys('parent_title','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$id.'','oct')->parent_title;
					if($key = 'Menu'){
						$cek_object = $this->dbfun_model->get_all_data_bys('menu_id','category_id = '.$id.' and ccs_key = '.$ccs_key.'  and ccs_id = '.$this->zone_id.' ','mnu');
						foreach($cek_object as $c){
							$this->cek_n_delete_data('menu_id','menu_id = '.$c->menu_id.'','mnu','','','','','');
						}
					}else{
						$cek_object = $this->dbfun_model->get_all_data_bys('object_id','category_id = '.$id.' and ccs_key = '.$ccs_key.'  and ccs_id = '.$this->zone_id.' ','odc');
						foreach($cek_object as $c){
							$this->cek_n_delete_data('ccs_o.object_id, ccs_o.image_square', 'ccs_o.object_id = '.$c->object_id.' and ccs_o.ccs_id = '.$this->zone_id.' and ccs_o.ccs_key = '.$ccs_key.' ', 'ccs_o', 'image_square',''.$this->zone.'/'.$key_name.'',array('object_id' => $c->object_id, 'ccs_odc.ccs_id' => $this->zone_id, 'ccs_odc.ccs_key' => $ccs_key),'odc',array('object_id' => $c->object_id, 'ccs_odc.ccs_id' => $this->zone_id, 'ccs_odc.ccs_key' => $ccs_key));
						}
					}
					$this->dbfun_model->del_tables(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'oct');
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif($cat == 'web'){
				
				$this->form_validation->set_rules('id', 'web id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{					
					
					$this->dbfun_model->del_tables(array('rc_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'orc');
					
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif($cat == 'objects'){
				
				$this->form_validation->set_rules('id', 'category id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{					
					$cek_object = count($this->dbfun_model->get_all_data_bys('object_id','object_id = '.$id.' and ccs_key = '.$ccs_key.' and ccs_id = '.$this->zone_id.' ','odc'));
					if($cek_object > 1){
						$this->dbfun_model->del_tables(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'odc');
					}else{
						$this->cek_n_delete_data('ccs_o.object_id, ccs_o.image_square', 'ccs_o.object_id = '.$id.' and ccs_o.ccs_id = '.$this->zone_id.' and ccs_o.ccs_key = '.$ccs_key.' ', 'ccs_o', 'image_square',''.$this->zone.'/'.$key_name.'',array('object_id' => $id, 'ccs_odc.ccs_id' => $this->zone_id, 'ccs_odc.ccs_key' => $ccs_key),'odc',array('object_id' => $id, 'ccs_odc.ccs_id' => $this->zone_id, 'ccs_odc.ccs_key' => $ccs_key));
					}
					if($key_name == 'promo'){
						$cek_tkt = $this->dbfun_model->get_data_bys('ticket_id','object_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'','tkt');
						if(!empty($cek_tkt)){
							$this->dbfun_model->del_tables(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'tkt');
						}
					}
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif($cat == 'varian'){
				
				$this->form_validation->set_rules('id', 'category id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{					
					
					$key = $this->dbfun_model->get_data_bys('object_id, category_id','varian_id = '.$id.' and ccs_key = '.$ccs_key.' and ccs_id = '.$this->zone_id.' ','prv');
					$simo = $key->object_id;
					$cat = $key->category_id;
					$cek_object = count($this->dbfun_model->get_all_data_bys('object_id','object_id = '.$simo.' and category_id = '.$cat.' and ccs_key = '.$ccs_key.' and ccs_id = '.$this->zone_id.' ','prv'));
					
					if($cek_object > 1){
						$this->dbfun_model->del_tables(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'prv');
					}else{
						$update = array(
							'vstatus' => 0,
							'dateupdated' => date('Y-m-d H:i:s'),
							'updatedby' => $this->admin_id
						);
						$this->dbfun_model->update_table(array('object_id' => $simo, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'prc');
						
						$this->dbfun_model->del_tables(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'prv');
					}
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif($cat == 'user'){
				
				$this->form_validation->set_rules('id', 'User ID', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{					
					if($lang == 'admin'){
						/* $update = array(
							'status' => 5,
							'dateupdated' => date('Y-m-d H:i:s'),
							'updatedby' => $this->admin_id
						);
						$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id ), $update, 'adm');	 */					
						$this->dbfun_model->del_tables(array('admin_id' => $id, 'ccs_id' => $this->zone_id ), 'adm');						
						$status = 'success';
						$message = 'Data deleted successfully';
					}elseif($lang == 'member'){
						/* $update = array(
							'status' => 5,
							'dateupdated' => date('Y-m-d H:i:s'),
							'approvedby' => $this->admin_id
						); */
						//$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id ), $update, 'usr');						
						$this->dbfun_model->del_tables(array('user_id' => $id, 'ccs_id' => $this->zone_id ), 'usr');						
						$this->dbfun_model->del_tables(array('user_id' => $id, 'ccs_id' => $this->zone_id ), 'tri');						
						$status = 'success';
						$message = 'Data deleted successfully';						
					}				
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif($cat == 'gallery'){				
				$this->form_validation->set_rules('id', 'category id', 'trim|required|numeric|xss_clean');
				if ($this->form_validation->run() === TRUE)
				{			
					$gallery = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and gallery_id = '.$id.'','ogl')->image_square;
					$file_path = './assets/'.$this->zone.'/gallery'.'/'.trim($gallery);
					if (file_exists($file_path))
					{
						unlink($file_path);
					}	
					$file_thumb = './assets/'.$this->zone.'/gallery/thumbnail'.'/'.str_replace('.','_thumb.',trim($gallery));
					if (file_exists($file_thumb))
					{
						unlink($file_thumb);
					}						
					$this->dbfun_model->del_tables(array('gallery_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'ogl');
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif($cat == 'menu'){				
				$this->form_validation->set_rules('id', 'web id', 'trim|required|numeric|xss_clean');

				if ($this->form_validation->run() === TRUE)
				{					
					$this->dbfun_model->del_tables(array('menu_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'mnu');
					$status = 'success';
					$message = 'Data deleted successfully';
				}
				else
				{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			
		}else{
			redirect($this->login);
		}
    }
	
	public function create($zone,$type){
    	if ($this->s_login && !empty($type)){
			$data = array();
			$status = '';
			$message = '';
			$cek = $this->dbfun_model->get_data_bys('type_id, type_view, module_id','ccs_id = '.$this->zone_id.' and link like "'.$type.'"','ccs_mdl');
			$custom_module = $this->dbfun_model->get_data_bys('type_id','ccs_id = '.$this->zone_id.'','ccs_zone')->type_id;
			
			$status = $this->input->post('status');
			$param = $this->input->post('param');
			$type_id = $cek->type_id;
			$type_view = $cek->type_view;
			if($type && ($param == 'sitemap')){
				$lang = $this->input->post('language_id');
				$admin_id = $this->admin_id;
				if(($type_view == 0) && ($type_id == 8)){					
					$id = $this->input->post('object_id');
					$cat = $this->input->post('category_id');
					$ccs_key = $this->input->post('ccs_key');
					if($id){ #update
						if ($this->form_validation->run('ccs_odc') === TRUE){											
							
							$fascia = $this->input->post('description');
							if($fascia == 'header'){
								$chiave = $this->input->post('chiave');
								$regress = trim($this->input->post('title'));
								if($chiave == 'socmed'){
									$olddesc = trim($this->input->post('olddescs'));
									$oldlink = trim($this->input->post('oldlink'));
									$q = ' and tagline = "'.$oldlink.'"';
								}elseif($chiave == 'page'){
									$olddesc = trim($this->input->post('olddesc'));
									$oldlink = strtolower(str_replace(' ','-', $regress));
									$q = ' and title = "'.$regress.'"';
								}
								
								if($olddesc && $oldlink){
									$old = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'  and description = "'.$olddesc.'" and object_id = '.$id.' and metadescription = "'.$chiave.'" and status = '.$status.''.$q,'odc'));
									$keys = $old;
								}
								switch($keys){
									case '1':
										switch($chiave){	
											case 'page':
												$essentiel = $this->input->post('header_type');		
												switch($essentiel){
													case 'socmed':
														$segunda = $this->input->post('socmed');
														$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'  and title = "'.$segunda.'" and metadescription = "'.$essentiel.'" and metakeyword = "'.$fascia.'" ','odc'));
														switch($cek){
															case '1':
																$status = 'error';
																$message = 'Data already exist on the system, please input another social media account. Cheers.';
																break;
															case '0':
																$update = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'title' => $segunda,
																	'tagline' =>  trim($this->input->post('link')),
																	'description' => trim($this->input->post('desc')),
																	'metadescription' => $essentiel,
																	'updatedby' => $this->admin_id,
																	'status' => $status
																);
																$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');	
																$fuck = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc'));
																if($fuck != 0){
																	$fk = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc')->category_id;
																	$footer = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc'));
																	if($footer != 0){
																		$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc')->object_id;	
																		$updates = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'title' => $segunda,
																			'tagline' =>  trim($this->input->post('link')),
																			'description' => trim($this->input->post('desc')),
																			'metadescription' => $essentiel,
																			'updatedby' => $this->admin_id,
																			'status' => $status
																			);
																		$this->dbfun_model->update_table(array('object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $fk ), $updates, 'odc');									
																	}
																}
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.'.$segunda;
																break;
														}
														break;
													case 'page':
														$simo = $this->input->post('simo');
														if($simo == 'simo'){
															$simo = $regress;
														}else{
															$simo = $this->input->post('simo');
														}
														switch($simo == $regress){
															case TRUE:
																$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and description = "'.$olddesc.'" and status = '.$status.' and metadescription = "'.$chiave.'" and metakeyword = "'.$fascia.'" ','odc'));
																if($cek == 1){
																	$status = 'error';
																	$message = 'No data updated'.$simo.$regress;
																}		
																break;
															case FALSE:
																$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'  and title = "'.$simo.'" and metadescription = "'.$essentiel.'" and metakeyword = "'.$fascia.'" ','odc'));
																switch($cek){
																	case '1':
																		$status = 'error';
																		$message = 'Data already exist on the system, please input another social media account. Cheers.';
																		break;
																	case '0':
																		$update = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'title' => $simo,
																			'tagline' => strtolower(str_replace(' ','-', $simo)),
																			'description' => trim($this->input->post('descs')),
																			'updatedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');	
																		$fuck = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc'));
																		if($fuck != 0){
																			$fk = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc')->category_id;
																			$footer = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc'));
																			if($footer != 0){
																				$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc')->object_id;	
																				$updates = array(
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'title' => $simo,
																					'tagline' => strtolower(str_replace(' ','-', $simo)),
																					'description' => trim($this->input->post('descs')),
																					'updatedby' => $this->admin_id,
																					'status' => $status
																					);
																				$this->dbfun_model->update_table(array('object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $fk ), $updates, 'odc');									
																			}
																		}
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.'.$simo;
																		break;
																} 
																break;
														}																	
														break;
												}
												break;
											case 'socmed':
												$essentiel = $this->input->post('header_type');		
												switch($essentiel){
													case 'socmed':
														$simo = $this->input->post('socmed');
														if($simo == ''){
															$simo = $regress;
														}else{
															$simo = $this->input->post('socmed');
														}
														switch($simo == $regress){
															case TRUE:
																$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'  and tagline = "'.$oldlink.'" and description = "'.$olddesc.'" and status = '.$status.' and metadescription = "'.$chiave.'" and metakeyword = "'.$fascia.'" ','odc'));
																if($cek == 1){
																	$status = 'error';
																	$message = 'No data updated'.$simo.$regress;
																}																	
																break;
															case FALSE:
																$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'  and title = "'.$simo.'" and metadescription = "'.$essentiel.'" and metakeyword = "'.$fascia.'" ','odc'));
																switch($cek){
																	case '1':
																		$status = 'error';
																		$message = 'Data already exist on the system, please input another social media account. Cheers.';
																		break;
																	case '0':
																		$update = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'title' => $simo,
																			'tagline' => trim($this->input->post('link')),
																			'description' => trim($this->input->post('desc')),
																			'updatedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');	
																		$fuck = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc'));
																		if($fuck != 0){
																			$fk = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc')->category_id;
																			$footer = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc'));
																			if($footer != 0){
																				$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc')->object_id;	
																				$updates = array(
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'title' => $simo,
																					'tagline' => trim($this->input->post('link')),
																					'description' => trim($this->input->post('desc')),
																					'updatedby' => $this->admin_id,
																					'status' => $status
																					);
																				$this->dbfun_model->update_table(array('object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $fk ), $updates, 'odc');									
																			}
																		}
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.'.$simo;
																		break;
																}
																break;
														}
														break;
													case 'page':
														$segunda = $this->input->post('simo');
														$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.'  and title = "'.$segunda.'" and metadescription = "'.$essentiel.'" and metakeyword = "'.$fascia.'" ','odc'));
														switch($cek){
															case '1':
																$status = 'error';
																$message = 'Data already exist on the system, please input another page. Cheers.';
																break;
															case '0':
																$update = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'title' => $segunda,
																	'tagline' =>  strtolower(str_replace(' ','-', $segunda)),
																	'description' => trim($this->input->post('olddesc')),
																	'metadescription' => $essentiel,
																	'updatedby' => $this->admin_id,
																	'status' => $status
																);
																$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');	
																$fuck = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc'));
																if($fuck != 0){
																	$fk = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc')->category_id;
																	$footer = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc'));
																	if($footer != 0){
																		$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc')->object_id;	
																		$updates = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'title' => $segunda,
																			'tagline' =>  strtolower(str_replace(' ','-', $segunda)),
																			'description' => trim($this->input->post('olddesc')),
																			'metadescription' => $essentiel,
																			'updatedby' => $this->admin_id,
																			'status' => $status
																			);
																		$this->dbfun_model->update_table(array('object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $fk ), $updates, 'odc');									
																	}
																}
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.'.$segunda;
																break;
														}
														break;															
												}
												break;
										}
										break;
									case '0':
										switch($chiave){	
											case 'page':
												$simo = $this->input->post('simo');
												if($simo == 'simo'){
													$simo = $regress;
												}else{
													$simo = $this->input->post('simo');
												}
												switch($simo == $regress){
													case TRUE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'description' => trim($this->input->post('olddesc')),
															'updatedby' => $this->admin_id,
															'status' => $status
														);
														$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');	
														$fuck = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc'));
														if($fuck != 0){
															$fk = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc')->category_id;
															$footer = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc'));
															if($footer != 0){
																$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc')->object_id;	
																$updates = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'description' => trim($this->input->post('olddesc')),
																	'updatedby' => $this->admin_id,
																	'status' => $status
																	);
																$this->dbfun_model->update_table(array('object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $fk ), $updates, 'odc');									
															}
														}
														$status = 'success';
														$message = 'Data successfully updated. Thankyou';
														break;
												}
												break;
											case 'socmed':
												$simo = $this->input->post('simo');
												$simo = $this->input->post('socmed');
												if($simo == ''){
													$simo = $regress;
												}else{
													$simo = $this->input->post('socmed');
												}
												$segunda = trim($this->input->post('title'));
												switch($simo == $regress){
													case TRUE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'tagline' => trim($this->input->post('oldlink')),
															'description' => trim($this->input->post('olddescs')),
															'updatedby' => $this->admin_id,
															'status' => $status
														);
														$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');	
														$fuck = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc'));
														if($fuck != 0){
															$fk = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and metakeyword = "footer"','odc')->category_id;
															$footer = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc'));
															if($footer != 0){
																$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "footer" and title = "'.$regress.'" and category_id = '.$fk.'','odc')->object_id;	
																$updates = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'tagline' => trim($this->input->post('oldlink')),
																	'description' => trim($this->input->post('olddescs')),
																	'updatedby' => $this->admin_id,
																	'status' => $status
																	);
																$this->dbfun_model->update_table(array('object_id' => $object, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $fk ), $updates, 'odc');									
															}
														}
														$status = 'success';
														$message = 'Data successfully updated. Thankyou test atas test'.$oldlink.$olddesc;
														break;
												}
												break;
										}
										break;
								} 
							}
						}
						else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else{
						if ($this->form_validation->run('ccs_odc') === TRUE){
							$image = $this->input->post('image_square');
							$type_id = $this->dbfun_model->get_data_bys('type_id','ccs_mdl.module_id = '.$ccs_key.' and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->type_id;
							
							$ctg = $this->dbfun_model->get_data_bys('title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$cat.'','oct')->title;
							//$prt = $this->dbfun_model->get_data_bys('parent_title','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = '.$cat.'','oct')->parent_title;
							$ct = strtolower($ctg);
							
							if($ct == 'footer'){								
								$fid = $this->input->post('footer_type');
								$footer = $this->input->post('description');
								$cate = $this->input->post('cat_id');
								
								if($fid == 'header' ){
									$object = $this->input->post('objects_id');
									$objects = $this->dbfun_model->get_data_bys('title, object_id, metadescription, description, tagline','ccs_odc.ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and language_id = '.$lang.' and object_id = '.$object.'','odc');
									$ttl = $objects->title;
									$desc = $objects->description;
									$tagline = $objects->tagline;
									$meta = $objects->metadescription;
								}
								elseif($fid == 'menu' ){
									$object = $this->input->post('cat_id');
									$objects = $this->dbfun_model->get_data_bys('title, description, link, type_id','menu_id = '.$object.'','mnu');
									$ttl = $objects->title;
									$desc = $objects->description;
									$tagline = $objects->link;
									$meta = $objects->type_id;
								}
								
								$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "'.$footer.'" and category_id = '.$cat.' and title = "'.$ttl.'"','odc'));
								if($cek == 0){
									$custom_primary = array(
											'type_id' => $type_id,
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key
										);
									if (!is_dir('assets/'.$this->zone.'/'.$type))
									{
										mkdir('./assets/'.$this->zone.'/'.$type, 0777, true);
									}
									$value = $this->new_data('ccs_o',$custom_primary, 'image_square','image_square', $this->zone.'/'.$type,'jpeg|jpg|png');		
								
									
									$custom = array(
										'object_id' => $value,
										'ccs_id' => $this->zone_id,
										'ccs_key' => $ccs_key,
										'createdby' => $admin_id,
										'datecreated' => date('Y-m-d H:i:s'),
										'title' => $ttl, 
										'tagline' => $tagline, 
										'description' => $desc,
										'metakeyword' => $footer,
										'metadescription' => $meta
									);
										
									$create_new_secondary = $this->new_data_secondary('ccs_odc',$custom,'','','','');	
									$this->dbfun_model->ins_to('ccs_odc', $create_new_secondary);
									$status = 'success';
									$message = 'Data created successfully';		
								}else{										
									$status = 'error';
									$message = 'Data already exist on system. Please choose another data. Cheers';	
								}
							}
							elseif($ct == 'header'){		
								$header = $this->input->post('header_type');
								if($header == 'socmed'){									
									$tagline = $this->input->post('link');							
									$title = $this->input->post('socmed');						
									$keywords = 'header';
									$meta = 'socmed';
									$desc = $this->input->post('descriptions');
								}elseif($header == 'page'){
									
									$page_id = $this->input->post('page_id');
									$object =  $this->dbfun_model->get_data_bys('ccs_odc.title, ccs_odc.object_id, ccs_odc.description','ccs_oct.ccs_id = '.$this->zone_id.' and ccs_oct.ccs_key = '.$ccs_key.' and ccs_oct.ccs_id=ccs_odc.ccs_id and ccs_oct.ccs_key = ccs_odc.ccs_key and ccs_oct.language_id = ccs_odc.language_id and ccs_oct.title like "Page" and ccs_oct.language_id = '.$lang.' and ccs_oct.category_id = ccs_odc.category_id and ccs_odc.object_id = '.$page_id.' ','oct, odc');
									$desc = $this->input->post('desc');							
									$title = $object->title;						
									$keywords = 'header';
									$tagline = strtolower(str_replace(' ','-', $object->title));
									$meta = 'page';	
								}
								$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and metakeyword = "'.$keywords.'" and category_id = '.$cat.' and title = "'.$title.'"','odc'));
								if($cek == 0){
									$custom_primary = array(
											'type_id' => $type_id,
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key
										);
									if (!is_dir('assets/'.$this->zone.'/'.$type))
									{
										mkdir('./assets/'.$this->zone.'/'.$type, 0777, true);
									}
									$value = $this->new_data('ccs_o',$custom_primary, 'image_square','image_square', $this->zone.'/'.$type,'jpeg|jpg|png');		
								
									$custom = array(
										'object_id' => $value,
										'ccs_id' => $this->zone_id,
										'ccs_key' => $ccs_key,
										'createdby' => $admin_id,
										'datecreated' => date('Y-m-d H:i:s'), 
										'title' => $title, 
										'tagline' => $tagline, 
										'description' => $desc,
										'metadescription' => $meta,
										'metakeyword' => $keywords
									);
									$create_new_secondary = $this->new_data_secondary('ccs_odc',$custom,'','','','');	
									$this->dbfun_model->ins_to('ccs_odc', $create_new_secondary);
									$status = 'success';
									$message = 'Data created successfully';	
								}else{										
									$status = 'error';
									$message = 'Data already exist on system. Please choose another data. Cheers';	
								}
							}							
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
				}				
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif($type && ($param == 'menu')){
				$lang = $this->input->post('language_id');
				$admin_id = $this->admin_id;																		
				if(($type_view == 0) && ($type_id == 8)){	
					$cat = $this->input->post('category_id');
					$menu = $this->input->post('menu_type');
					$ccs_key = $this->input->post('ccs_key');
					$id = $this->input->post('menu_id');
					if($id){ #update
						
					}else{
						if ($this->form_validation->run('ccs_mnu') === TRUE){
							if($menu =='multisite'){
								$module = $this->input->post('type_id');
								if($module == 'module'){
									$category = $this->input->post('category');
									if($category == 0){
										$page = $this->input->post('module');
										$mdl = $this->dbfun_model->get_data_bys('module_id, name, link as links','ccs_id = '.$this->zone_id.' and name = "'.$page.'" ','mdl');
										$link = $mdl->links;
										$title = $mdl->name;
										$mdl_id = $mdl->module_id;										
										$desc = $this->input->post('description');
										$cek = count($this->dbfun_model->get_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$mdl_id.' and link = "'.$link.'" ','mnu'));
										if($cek == 0){	
											$custom = array();
											if($status == 0){
												$custom = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'url_id' => $mdl_id, 
													'createdby' => $admin_id,
													'title' => $title, 
													'link' => $link, 
													'description'=> $desc, 
													'object_id'=> $mdl_id, 
													'type_id' => 'module'
												);
											}elseif($status == 1){
												$custom = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key, 
													'datecreated' => date('Y-m-d H:i:s'),
													'url_id' => $mdl_id, 
													'createdby' => $admin_id,
													'title' => $title, 
													'link' => $link, 
													'description'=> $desc, 
													'object_id'=> $mdl_id, 
													'type_id' => 'module'
												);
											}
											$new = $this->new_data('ccs_mnu',$custom,'','','','');	
											$status = 'success';
											$message = 'Data created successfully';		
										}else{
											$status = 'error';
											$message = 'Data already exist on system. Please choose another data. Cheers';	
										}
									}
									elseif($category == 1){
										$page = $this->input->post('module');
										$mdl = $this->dbfun_model->get_data_bys('module_id, name, link as links','ccs_id = '.$this->zone_id.' and LOWER(name) = "'.strtolower($page).'" ','mdl');
										$link = $mdl->links;
										$title = $mdl->name;
										$mdl_id = $mdl->module_id;										
										$desc = $this->input->post('description');
										$categories = $this->dbfun_model->get_all_data_bys('category_id, title, status, ccs_key','ccs_key = '.$mdl_id.' and ccs_id = '.$this->zone_id.' ','oct');
										if(!empty($categories)){
											foreach($categories as $c){
												$cek = count($this->dbfun_model->get_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$c->category_id.' and url_id = '.$c->ccs_key.' ','mnu'));
												if($cek == 0){	
													$customs = array(
														'ccs_id' => $this->zone_id,
														'ccs_key' => $ccs_key,
														'url_id' => $c->ccs_key, 
														'createdby' => $admin_id,
														'title' => $c->title, 
														'link' => strtolower(str_replace(' ','_', $c->title)), 
														'description'=> $desc, 
														'object_id'=> $c->category_id, 
														'type_id' => 'category',
														'status' =>$c->status
													);
													$news = $this->new_data('ccs_mnu',$customs,'','','','');	
													$status = 'success';
													$message = 'Data created successfully';	
												}else{
													$status = 'error';
													$message = 'Data already exist on system. Please choose another data. Cheers';	
												}
											}	
										}else{
											$status = 'error';
											$message = 'No Category on '.$title.'';	
										}
									}
								}
								elseif($module == 'page'){
									$page = $this->input->post('page');
									$object = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and title = "'.$page.'" and ccs_key = '.$ccs_key.'','odc')->object_id;
									$link = strtolower(str_replace(' ','_', $page));
									$desc = $this->input->post('description');
									$cek = count($this->dbfun_model->get_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$object.' and link = "'.$link.'" ','mnu'));
									if($cek == 0){
										$custom = array();
										if($status == 0){
											$custom = array(
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'url_id' => $ccs_key, 
												'createdby' => $admin_id,
												'title' => $page, 
												'link' => $link, 
												'description'=> $desc, 
												'object_id'=> $object
											);
										}elseif($status == 1){
											$custom = array(
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key, 
												'url_id' => $ccs_key, 
												'createdby' => $admin_id,
												'title' => $page,
												'link' => $link, 
												'description'=> $desc,
												'datecreated' => date('Y-m-d H:i:s'),
												'object_id'=> $object
											);
										}
										$new = $this->new_data('ccs_mnu',$custom,'','','','');	
									
										$status = 'success';
										$message = 'Data created successfully';		
									}else{
										$status = 'error';
										$message = 'Data already exist on system. Please choose another data. Cheers';	
									}
								}
							}
							elseif($menu =='paralax'){
								$modul = $this->input->post('section');
								$ccs_key = $this->input->post('ccs_key');
								$section = $this->dbfun_model->get_data_bys('ccs_key, category_id','ccs_id = '.$this->zone_id.' and title = "'.$modul.'" and ccs_key != '.$ccs_key.'','oct');
								$url_id = $section->ccs_key;								
								$category = $section->category_id;
								
								$link = '#'.strtolower(str_replace(' ','_', $modul));
								$desc = $this->input->post('description');	
								$custom = array();
								$status = $this->input->post('status');
								$cek = count($this->dbfun_model->get_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$category.' and link = "'.$link.'" ','mnu'));
								if($cek == 0){
									if($status == 0){
										$custom = array(
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key,
											'url_id' => $url_id, 
											'createdby' => $admin_id,
											'title' => $modul, 
											'link' => $link, 
											'type_id' => 'category', 
											'description'=> $desc, 
											'object_id'=> $category
										);
									}elseif($status == 1){
										$custom = array(
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key, 
											'url_id' => $url_id, 
											'createdby' => $admin_id,
											'title' => $modul,
											'link' => $link, 
											'type_id' => 'category', 
											'description'=> $desc,
											'datecreated' => date('Y-m-d H:i:s'),
											'object_id'=> $category
										);
									}
									$new = $this->new_data('ccs_mnu',$custom,'','','','');	
									//$this->dbfun_model->ins_to('ccs_odc', $create_new_secondary);
									$status = 'success';
									$message = 'Data created successfully';	
								}else{
									$status = 'error';
									$message = 'Data already exist on system. Please choose another data. Cheers';	
								}
							}							
							
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
				}				
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) &&($param == 'object')){ 
				$id = $this->input->post('object_id');
				$ccs_key = $this->input->post('ccs_key');
				$lang = $this->input->post('language_id');
				$cat = $this->input->post('category_id');
				$admin_id = $this->admin_id;
				$img_from = strtolower($this->input->post('image_from'));
				$img_from_other = $this->input->post('image_square2');
				$tags = $this->input->post('tags');
				
				if($id)
				{//update
					$chiave = trim($this->input->post('metakeyword')); 
					$parole = trim($this->input->post('metadescription'));
					$segretis = trim($this->input->post('title')); 
					$simo = trim($this->input->post('tagline'));
					$prc = $this->input->post('param2');
					$desc = $this->input->post('description');
					if(!empty($tags)){
						$update['tags'] = $tags;
					}else{
						$tags = '';
					}
					//$desccek =  count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and object_id = '.$id.' and ccs_key = '.$ccs_key.' and description = "'.$desc.'"','odc'));
					if($chiave || $parole){
						$cek = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and object_id = '.$id.' and ccs_key = '.$ccs_key.' and metakeyword = "'.$chiave.'" and metadescription = "'.$parole.'"','odc');
						if(!$cek){	
							$update = array(
								'dateupdated' => date('Y-m-d H:i:s'),
								'metakeyword' => $chiave,
								'metadescription' => $parole,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}else{								
							$status = 'error';
							$message = 'No data updated. Please check the data you want to update..';
						}
					}
					elseif($segretis || $simo){							
						$ceks = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and object_id = '.$id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" and tagline = "'.$simo.'" ','odc');
						$cate = $this->input->post('cat_id');
						$tu = $this->input->post('tu');
						if($cate == $cat){
							$ct = $cat;
						}else{
							$ct = $cate; 
						}
						$times = $this->input->post('date');
						$pub = $this->input->post('datepublish');
						if(($status == 2) && ($pub)){
							$publish = $pub;
						}elseif(($status == 2) && ($times)){
							$publish = $times;
						}else{
							$publish = '';
						}
													
						if(!$ceks){	
							$update = array(
								'title' => $segretis,
								'tagline' => $simo,
								'description' =>$desc,
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'datepublish' => $publish,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							if($tu){
								$update = array(
									'tu' => $tu,
									'tags' => $tags,
									'dateupdated' => date('Y-m-d H:i:s'),
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							}
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}
						elseif($desc){	
						
							$update = array(
								'title' => $segretis,
								'tagline' => $simo,
								'description' =>$desc,
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'datepublish' => $publish,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							if($tu){
								$update = array(
									'tu' => $tu,
									'tags' => $tags,
									'dateupdated' => date('Y-m-d H:i:s'),
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							}
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}
						elseif($status != ''){
							$update = array(
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'datepublish' => $publish,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							if($tu){
								$update = array(
									'tu' => $tu,
									'tags' => $tags,
									'dateupdated' => date('Y-m-d H:i:s'),
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							}
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}elseif($ct){
							$update = array(
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('settings_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'ocs');
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
							
						}
						
						if(!empty($this->input->post('latitude'))){
							$cek_loc = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and object_id = '.$id.' and ccs_key = '.$ccs_key.' ','olc');
							if(empty($cek_loc)){
								$custom_loc = array(
									'object_id' => $id,
									'ccs_id' => $this->zone_id,
									'ccs_key' => $ccs_key,
									'latitude' => $this->input->post('latitude'),
									'longitude' => $this->input->post('longitude')
								);
								$create_new_loc = $this->new_data_secondary('ccs_olc',$custom_loc,'','','','');	
								$this->dbfun_model->ins_to('ccs_olc', $create_new_loc);
							}else{
								$update_loc = array(
									'category_id' => $ct,
									'latitude' => $this->input->post('latitude'),
									'longitude' => $this->input->post('longitude'),
									'status' => $status
								);
								$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update_loc, 'olc');
							}
						}
						if(isset($_FILES['image_square'])){
							$updates = $this->update_data_bys('*', array('object_id' => $id), 'ccs_o','image_square', $this->zone.'/'.$type,'image_square','jpeg|jpg|png|mp4');
						}else{
							$updates = $this->update_data_bys('*', array('object_id' => $id), 'ccs_o','','','','');
							if(!empty($img_from) && !empty($img_from_other)){//FDL WAS HERE HAHAHA
								$prev_img = $this->dbfun_model->get_data_bys('image_square',array('object_id' => $id,'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key),'ccs_o')->image_square;
								$cek_from_o = count($this->dbfun_model->get_all_data_bys('image_square',array('image_square' => $prev_img,'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key),'ccs_o'));
								//$cek_from_ogl = count($this->dbfun_model->get_all_data_bys('image_square',array('image_square' => $prev_img),'ccs_ogl'));
								//$cek_from_oct = count($this->dbfun_model->get_all_data_bys('image_square',array('image_square' => $prev_img),'ccs_oct'));
								if($cek_from_o == 1){
									$file_path = './assets/'.$this->zone.'/'.$type.'/'.$prev_img;
									if (file_exists($file_path))
									{
										unlink($file_path);
									}
								}
								
								if($img_from == $type){
									$data_name = $img_from_other;
								}else{
									$data_from = './assets/'.$this->zone.'/'.$img_from.'/'.$img_from_other;
									$data_to = './assets/'.$this->zone.'/'.$type.'/'.$img_from_other;
									$data_name = $img_from_other;
									copy($data_from, $data_to);
								}
								$updates['image_square'] = $data_name;
							}
						}
						$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $updates, 'o');
						
						if($type_id == 3){
							$datestart =  str_replace(' 00:00:00','',$this->input->post('datestart'));
							$dateend =  str_replace(' 00:00:00','',$this->input->post('dateend'));										
							if($this->date_valid($datestart)){
								$cek_cat = strtolower($this->category_id_to_category($ccs_key,$this->input->post('category_id')));
								if($this->date_valid($dateend)){
										if(($dateend >= $datestart) || ($cek_cat == 'weekly event')){
											if($cek_cat == 'weekly event'){
												if(empty($dateend)){
													$dateend = $datestart;
												}
												$updated = array(
													'datecreated' => $datestart,
													'dateupdated' => $dateend
												);
											}else{
												$updated = array(
													'datecreated' => $datestart,
													'dateupdated' => $dateend
												);
											}
											
											$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $updated, 'o');						
											$status = 'success';
											$message = 'Data has been updated. Thank you...';
											$cdate = 0;
										}
									else{													
										$cdate = 1;
										$status = 'error';
										$message = 'The end date must be bigger than start date. Thankyou...';
									}
								}
								else{
									$cdate = 1;
									$status = 'error';
									$message = 'Date must be on format " yyyy-mm-dd "';
								}
							}else{
								$cdate = 1;
								$status = 'error';
								$message = 'Date must be on format " yyyy-mm-dd "';
							}
							
							$love = $this->input->post('love');
							if($love < 100){
								if($cdate == 0){
									$update = array(
										'love' => $love,
										'dateupdated' => date('Y-m-d H:i:s'),
										'updatedby' => $this->admin_id
									);
									$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
									$status = 'success';
									$message = 'Data has been updated. Thank you...'.$datestart.$dateend;
								}
							}else{
								$status = 'error';
								$message = 'Discount must be below 100%. Thank you...';
							}
						}
					}
					elseif($prc == 'price'){
						
						$o = $this->input->post('basic');
						$z = str_replace('.','',$o);
						$b = str_replace(',','.',$z);
						$ol = $this->input->post('publish');
						$zl = str_replace('.','',$ol);
						$p = str_replace(',','.',$zl);
						$s = $this->input->post('stock');
						$cek = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$id.' and basic = '.$b.' and publish = '.$p.'','prc'));
						if($s < 0){
							$stock = $s;
						}else{								
							$stock = $this->dbfun_model->get_data_bys('stock','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$id.' ','prc')->stock;
						}
						switch($cek){
							case '1':
								if($s == $stock){										
									$status = 'error';
									$message = 'No data updated';
								}else{
									$update_prc = array(
										'basic' => $b,
										'stock' => $s,
										'publish' => $p,
										'dateupdated' => date('Y-m-d H:i:s'),
										'status' => $status,
										'updatedby' => $this->admin_id
									);										
									$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $id ), $update_prc, 'prc');
									$status = 'success';
									$message = 'Data has been updated. Thank you...';
								}
								
								break;
							case '0':
								if($s == $stock){
									$update_prc = array(
										'basic' => $b,
										'publish' => $p,
										'dateupdated' => date('Y-m-d H:i:s'),
										'updatedby' => $this->admin_id
									);
								}else{
									$update_prc = array(
										'basic' => $b,
										'stock' => $this->input->post('stock'),
										'publish' => $p,
										'dateupdated' => date('Y-m-d H:i:s'),
										'updatedby' => $this->admin_id
									);
								}						
								$trx = count($this->dbfun_model->get_all_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$id.' ','prv'));
								if($trx != 0){
									$price = $this->dbfun_model->get_data_bys('basic, publish','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$id.' ','prc');
									$prv = $this->dbfun_model->get_all_data_bys('varian_id, basic, publish','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$id.' ','prv');
									foreach($prv as $v){
										if($price->basic == $v->basic){
											$basic = $b;
										}else{
											$basic = $v->basic;
										}
										
										if($price->publish == $v->publish){
											$publish = $p;
										}else{
											$publish = $v->publish;
										}
										
										$update = array(
											'basic' => $basic,
											'publish' => $publish,
											'dateupdated' => date('Y-m-d H:i:s'),
											'updatedby' => $this->admin_id
										);
										$this->dbfun_model->update_table(array('varian_id' => $v->varian_id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $id ), $update, 'prv');											
									}										
								}								
											
								$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $id ), $update_prc, 'prc');
								
								$status = 'success';
								$message = 'Data has been updated. Thank you...';
								break;
						}
					}
					else{
						$status = 'error';
						$message = 'No data updated. Please check the data you want to update..';
					}
				}
				else{ // create
					if ($this->form_validation->run('ccs_odc') === TRUE){
						$title = $this->input->post('title');
						$cekttl = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$title.'" and status != 13 ','odc'));
						if($cekttl == 0){
							$duum = $this->dbfun_model->get_data_bys('type_id','ccs_mdl.module_id = '.$ccs_key.' and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->type_id;
							$virato = $type_view;
							$love = $this->input->post('love');
							if(($duum == 3)&&( $virato == 5)&&( $love > 100 )){
								$disc = 1; 
							}else{
								$disc = 0;
							}
							if($disc == 0){
								$image = $this->input->post('image_square');
								if($duum == 3 ){
									$datestart = $this->input->post('datestart');
									$dateend = $this->input->post('dateend');											
									if($this->date_valid($datestart)){
										if($this->date_valid($dateend)){
											$cek_cat = strtolower($this->category_id_to_category($ccs_key,$this->input->post('category_id')));
												if(($dateend >= $datestart) || ($cek_cat == 'weekly event')){
													if($cek_cat == 'weekly event'){
														if(empty($dateend)){
															$dateend = $datestart;
														}
														$custom_primary = array(
															'type_id' => $type_id,
															'ccs_id' => $this->zone_id,
															'ccs_key' => $ccs_key,
															'datecreated' => $datestart,
															'dateupdated' => $dateend
														);
													}else{
														$custom_primary = array(
															'type_id' => $type_id,
															'ccs_id' => $this->zone_id,
															'ccs_key' => $ccs_key,
															'datecreated' => $datestart,
															'dateupdated' => $dateend
														);
													}
													
													$cdate = 0;
												}
											else{													
												$cdate = 1;
												$status = 'error';
												$message = 'The end date must be bigger than start date. Thankyou...';
											}
										}
										else{
											$cdate = 1;
											$status = 'error';
											$message = 'Date must be on format " yyyy-mm-dd "';
										}
									}else{
										$cdate = 1;
										$status = 'error';
										$message = 'Date must be on format " yyyy-mm-dd "';
									}
								}else{
									$custom_primary = array(
										'type_id' => $type_id,
										'ccs_id' => $this->zone_id,
										'datecreated' => date('Y-m-d H:i:s'),
										'ccs_key' => $ccs_key
									);
									$cdate = 0;
								}												
								if($cdate == 0){
									if (!is_dir('assets/'.$this->zone.'/'.$type))
									{
										mkdir('./assets/'.$this->zone.'/'.$type, 0777, true);
									}
									if(!empty($img_from) && !empty($img_from_other)){//FDL WAS HERE HAHAHA
										if($img_from == $type){
											$data_name = $img_from_other;
										}else{
											$data_from = './assets/'.$this->zone.'/'.$img_from.'/'.$img_from_other;
											$data_to = './assets/'.$this->zone.'/'.$type.'/'.$img_from_other;
											$data_name = $img_from_other;
											copy($data_from, $data_to);
										}
										
										$custom_primary['image_square'] = $data_name;
										$value = $this->new_data('ccs_o',$custom_primary, '','', '','');
									}else{
										$value = $this->new_data('ccs_o',$custom_primary, 'image_square','image_square', $this->zone.'/'.$type,'jpeg|jpg|png|mp4');
									}
									
									$pub = $this->input->post('datepublish');
									if(($status == 2) && ($pub)){
										$publish = $pub;
									}else{
										$publish = '';
									}
									
									if(($virato == 5)&&($duum == 0)){
										$custom_prc = array(													
											'object_id' => $value,
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key,
											'category_id' => $cat,
											'basic' => $this->input->post('basic'),
											'publish' => $this->input->post('publish'),
											'stock' => $this->input->post('stock'),
											'datecreated' =>  date('Y-m-d H:i:s'),
											'status' => 1
										);
										$this->dbfun_model->ins_to('ccs_prc', $custom_prc); 
										
										$custom = array(
											'object_id' => $value,
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key,
											'createdby' => $admin_id,
											'datecreated' =>  date('Y-m-d H:i:s'),
											'datepublish' =>$publish
										);										
									}else{
										$custom = array(
											'object_id' => $value,
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key,
											'createdby' => $admin_id,
											'datecreated' =>  date('Y-m-d H:i:s'),
											'datepublish' =>$publish
										);	
										if(!empty($this->input->post('latitude'))){
											$custom_loc = array(
												'object_id' => $value,
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'latitude' => $this->input->post('latitude'),
												'longitude' => $this->input->post('longitude')
											);
											$create_new_loc = $this->new_data_secondary('ccs_olc',$custom_loc,'','','','');	
											$this->dbfun_model->ins_to('ccs_olc', $create_new_loc);
										}
									}
									$create_new_secondary = $this->new_data_secondary('ccs_odc',$custom,'','','','');	
									$this->dbfun_model->ins_to('ccs_odc', $create_new_secondary); 
									$status = 'success';
									$message = 'Data created successfully';	
								}									
							}else{
								$status = 'error';
								$message = 'Discount must be below 100%. Thankyou';	
							}
						}else{
							$status = 'error';
							$message = 'Data with title " '.$title.' " has already exist on the systems. Thankyou';	
						}	
					}
					else{
						$status = 'error';
						$message = validation_errors();
					}
				}				
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) &&($param == 'settings')){ 
				$ccs_key = $this->input->post('ccs_key');
				$lang = $this->input->post('language_id');
				$cat = $this->input->post('category_id');
				$admin_id = $this->admin_id;
				$key = $this->input->post('param2');
				if($key == 'settings'){
					$id = $this->input->post('settings_id');
					if($id)
					{//update
						$segretis = trim($this->input->post('title')); 
						$desc = $this->input->post('description');
						//$simo = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and settings_id = '.$id.' and ccs_key = '.$ccs_key.' ','ocs')->title;						
						$cate = $this->input->post('cat_id');
						if($cate == $cat){
							$ct = $this->input->post('category_id');
						}else{
							$ct = $this->input->post('cat_id');
						}	
						$object = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and settings_id = '.$id.' and ccs_key = '.$ccs_key.'','ocs');
						$rule_1 =  trim($this->input->post('rule_1')); 
						$rule_2 = trim($this->input->post('rule_2')); 
						if($segretis != $object->title){
							if(($cat == $cate)||($rule_1 == $object->rule_1)||($object->rule_2 == $rule_2)||($status == $object->status)){
								if(isset($_FILES['image_square'])){
									$updates = $this->update_data_bys('*', array('settings_id' => $id), 'ccs_ocs','image_square', $this->zone.'/'.$type,'image_square','jpeg|jpg|png');
								}else{
									$updates = $this->update_data_bys('*', array('settings_id' => $id), 'ccs_ocs','','','','');
								}
								$this->dbfun_model->update_table(array('settings_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $updates, 'ocs');
								
								$update = array(
									'title' => $segretis,
									'rule_1' => $this->input->post('rule_1'),
									'rule_2' => $this->input->post('rule_2'),
									'description' =>$desc,
									'category_id' => $ct,
									'dateupdated' => date('Y-m-d H:i:s'),
									'status' => $status,
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('settings_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'ocs');
								$status = 'success';
								$message = 'Data has been updated. Thank you...';
							}
						}else{
							$keys = count($this->dbfun_model->get_data_bys('settings_id','settings_id = '.$id.' and rule_1 = "'.$rule_1.'" and description = "'.$desc.'"','ocs'));
							if($keys == 0){
								$update = array(
									'rule_1' => $this->input->post('rule_1'),
									'rule_2' => $this->input->post('rule_2'),
									'description' =>$desc,
									'category_id' => $ct,
									'dateupdated' => date('Y-m-d H:i:s'),
									'status' => $status,
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('settings_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'ocs');
								$status = 'success';
								$message = 'Data has been updated. Thank you...';
							}else{									
								$status = 'error';
								$message = 'Varian product already exist, please choose another "name / title" for varian of product. Cheers';
							}
						}
							
							
					}
					else{ // create
						if ($this->form_validation->run('ccs_ocs') === TRUE){
							$image = $this->input->post('image_square');
							$duum = $this->dbfun_model->get_data_bys('type_id','ccs_mdl.module_id = '.$ccs_key.' and ccs_mdl.ccs_id = '.$this->zone_id.' ','mdl')->type_id;
							$virato = $type_view;
							
							$custom_primary = array(
								'ccs_id' => $this->zone_id,
								'datecreated' => date('Y-m-d H:i:s'),
								'ccs_key' => $ccs_key
							);											
							
							if (!is_dir('assets/'.$this->zone.'/'.$type))
							{
								mkdir('./assets/'.$this->zone.'/'.$type, 0777, true);
							}
							$value = $this->new_data('ccs_ocs',$custom_primary, 'image_square','image_square', $this->zone.'/'.$type,'jpeg|jpg|png');
							
							$status = 'success';
							$message = 'Data created successfully';			
						}
						else{
							$status = 'error';
							$message = validation_errors();
						}
					}				
				}
				elseif($key == 'pnp'){
					
					$id = $this->input->post('varian_id');
					if($id){
						$chiave = $this->input->post('set');
						$parole = $this->input->post('settings_id');
						$segretis = $this->input->post('object_id');
						$simo = $this->input->post('url_id');
						$key = $this->dbfun_model->get_data_bys('ccs_prv.settings_id, ccs_ocs.category_id, ccs_prv.stock','ccs_ocs.ccs_id = ccs_prv.ccs_id and ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.ccs_key = ccs_prv.url_id and ccs_prv.url_id = '.$simo.' and ccs_prv.ccs_key = '.$ccs_key.' and ccs_prv.varian_id = '.$id.' and ccs_prv.category_id = '.$cat.' and ccs_prv.object_id = '.$segretis.'','ocs, prv');
						$duum = $key->settings_id;
						$virato = $key->category_id;
						$stock = $key->stock;
						if($chiave == $virato){
							if($duum == $parole){
								$segretissimo = $duum;
							}else{									
								$segretissimo = $parole;
							}								
						}else{
							$segretissimo = $parole;
						}
						$cek = count($this->dbfun_model->get_data_bys('ccs_prv.varian_id','ccs_ocs.ccs_id = ccs_prv.ccs_id and ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.ccs_key = ccs_prv.url_id and ccs_prv.url_id = '.$simo.' and ccs_prv.ccs_key = '.$ccs_key.' and ccs_prv.settings_id = '.$segretissimo.' and ccs_prv.category_id = '.$cat.' and ccs_prv.object_id = '.$segretis.'','ocs, prv'));
						switch($cek){
							case '1':
								switch($duum == $parole){
									case TRUE:
										$s = $this->input->post('stock');
										$q = $this->input->post('oldprc');
										$z = str_replace('.','',$q);
										$p = str_replace(',','.',$z);
										$keys = count($this->dbfun_model->get_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and varian_id = '.$id.' and status = '.$status.' and publish = '.$p.' and stock = '.$s.' ','prv'));
										switch($keys){
											case '1':
												$n = $this->input->post('new');
												switch($n){
													case FALSE:
														$status = 'error';
														$message = 'No data updated';
														break;
														
													case TRUE:
														$cek2 = count($this->dbfun_model->get_data_bys('ccs_prv.varian_id','ccs_ocs.ccs_id = ccs_prv.ccs_id and ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.ccs_key = ccs_prv.url_id and ccs_prv.url_id = '.$simo.' and ccs_prv.ccs_key = '.$ccs_key.' and ccs_prv.settings_id = '.$n.' and ccs_prv.category_id = '.$cat.' and ccs_prv.object_id = '.$segretis.'','ocs, prv'));
														switch($cek2){
															case '1':
																$status = 'error';
																$message = 'Data already exist on the system, please input another varian for this product. Cheers';
																break;
															case '0':
																$prc = $this->input->post('prc');
																switch($prc){
																	case '0':
																		$update = array(
																			'basic' => $this->input->post('basic'),
																			'stock' => $this->input->post('newstock'),
																			'publish' => $this->input->post('publish'),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'status' => $status,
																			'settings_id' => $n,
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $segretis ), $update, 'prv');
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou atas';
																		break;
																	case '1':
																		$update = array(
																			'basic' => $this->input->post('basic'),
																			'stock' => $this->input->post('newstock2'),
																			'publish' => $this->input->post('publish2'),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'status' => $status,
																			'settings_id' => $n,
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $segretis ), $update, 'prv');
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou';
																		break;
																}
																break;
														}
														break;
												}													
												break;
											case '0':
												if($stock == $s){
													$total = $s;
												}elseif($stock > $s){
													$total = $s;
												}elseif($stock < $s){
													$total = $s + $stock;
												}
												$update = array(
													'stock' => $total,
													'publish' => $p,
													'dateupdated' => date('Y-m-d H:i:s'),
													'status' => $status,
													'updatedby' => $this->admin_id
												);
												$this->dbfun_model->update_table(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $segretis ), $update, 'prv');
							
												$status = 'success';
												$message = 'Data has been successfully updated. Thankyou';
												break;
										}
										break;
									case FALSE:
										$keys = count($this->dbfun_model->get_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and settings_id = '.$segretissimo.' and object_id = '.$segretis.' and category_id = '.$cat.'','prv'));
										switch($keys){
											case '1':
												$status = 'error';
												$message = 'Data already exist on the system, please input another varian for this product. Cheers';
												break;
											case '0':
												$prc = $this->input->post('prc');
												switch($prc){
													case '0':
														$update = array(
															'basic' => $this->input->post('basic'),
															'stock' => $this->input->post('newstock'),
															'publish' => $this->input->post('publish'),
															'dateupdated' => date('Y-m-d H:i:s'),
															'status' => $status,
															'settings_id' => $segretissimo,
															'updatedby' => $this->admin_id
														);
														$this->dbfun_model->update_table(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $segretis ), $update, 'prv');
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou atas'.$segretissimo;
														break;
													case '1':
														$update = array(
															'basic' => $this->input->post('basic'),
															'stock' => $this->input->post('newstock2'),
															'publish' => $this->input->post('publish2'),
															'dateupdated' => date('Y-m-d H:i:s'),
															'status' => $status,
															'settings_id' => $segretissimo,
															'updatedby' => $this->admin_id
														);
														$this->dbfun_model->update_table(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $segretis ), $update, 'prv');
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou'.$segretissimo;
														break;
												}
												break;
										}													
										break;
								}
								break;
							case '0':
								$keys = count($this->dbfun_model->get_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and settings_id = '.$segretissimo.' and object_id = '.$segretis.' and category_id = '.$cat.'','prv'));
								switch($keys){
									case '1':
										$status = 'error';
										$message = 'Data already exist on the system, please input another varian for this product. Cheers';
										break;
									case '0':
										$prc = $this->input->post('prc');
										switch($prc){
											case '0':
												$update = array(
													'basic' => $this->input->post('basic'),
													'stock' => $this->input->post('newstock'),
													'publish' => $this->input->post('publish'),
													'dateupdated' => date('Y-m-d H:i:s'),
													'status' => $status,
													'settings_id' => $segretissimo,
													'updatedby' => $this->admin_id
												);
												$this->dbfun_model->update_table(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $segretis ), $update, 'prv');
												$status = 'success';
												$message = 'Data has been successfully updated. Thankyou atas'.$segretissimo;
												break;
											case '1':
												$update = array(
													'basic' => $this->input->post('basic'),
													'stock' => $this->input->post('newstock2'),
													'publish' => $this->input->post('publish2'),
													'dateupdated' => date('Y-m-d H:i:s'),
													'status' => $status,
													'settings_id' => $segretissimo,
													'updatedby' => $this->admin_id
												);
												$this->dbfun_model->update_table(array('varian_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $segretis ), $update, 'prv');
												$status = 'success';
												$message = 'Data has been successfully updated. Thankyou'.$segretissimo;
												break;
										}
										break;
								}
								break;
						}
						
					}
					else{			
						if ($this->form_validation->run('ccs_prv') === TRUE){	
							$segretis = $this->input->post('prc');
						
							$simo = $this->input->post('object_id');
							$set = $this->input->post('settings_id');
							$url = $this->input->post('url_id');
							if($segretis == 0){
								$cek = count($this->dbfun_model->get_data_bys('varian_id','ccs_id = '.$this->zone_id.' and object_id = '.$simo.' and settings_id = '.$set.' and ccs_key = '.$ccs_key.' and url_id = '.$url.'','prv'));
								if($cek == 0){
									//$prc = $this->dbfun_model->get_data_bys('publish','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$simo.'','prc')->publish;
									$custom_prv = array(
										'ccs_id' => $this->zone_id, 
										//'publish' => $prc
									);	
									$value = $this->new_data('ccs_prv',$custom_prv, '','','','');	
									$update = array(
										'vstatus' => 1,
										'dateupdated' => date('Y-m-d H:i:s'),
										'updatedby' => $this->admin_id
									);
									$this->dbfun_model->update_table(array('object_id' => $simo, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'prc');
												
									$status = 'success';
									$message = 'Data created successfully';
								}else{
									$status = 'error';
									$message = 'Varian product already exist, please choose another varian of product. Cheers';
								}			
							}else{
								$cek = count($this->dbfun_model->get_data_bys('varian_id','ccs_id = '.$this->zone_id.' and object_id = '.$simo.' and settings_id = '.$set.' and ccs_key = '.$ccs_key.' and url_id = '.$url.'','prv'));
								if($cek == 0){
									$custom_prv = array(
										'ccs_id' => $this->zone_id,
										'publish' => $this->input->post('publish2')
									);	
									$value = $this->new_data('ccs_prv',$custom_prv, '','','','');		
									
									$update = array(
										'vstatus' => 1,
										'dateupdated' => date('Y-m-d H:i:s'),
										'updatedby' => $this->admin_id
									);
									$this->dbfun_model->update_table(array('object_id' => $simo, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'prc');
									$status = 'success';
									$message = 'Data created successfully';
								}else{
									$status = 'error';
									$message = 'Varian product already exist, please choose another varian of product. Cheers';
								}									
							}
						}
						else{
							$status = 'error';
							$message = validation_errors();
						}
					}
				}
				elseif($key == 'product'){
					$id = $this->input->post('ticket_id');
					if($id){
						$status = 'success';
						$message = 'Data has been successfully updated. Thankyou';
					}else{	
						if ($this->form_validation->run('ccs_tkt') === TRUE){	
							$chiave = $this->input->post('oid');
							$parole = $this->input->post('cat_id');
							$segretis = $this->input->post('url_id');
							$o = $this->input->post('object_id');
							$simo = $this->input->post('dsc');
							$url = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$this->zone_id.' and link = "product"','mdl')->module_id;
							$code = $this->dbfun_model->get_data_bys('tagline','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','odc')->tagline;
							switch($chiave == 0){
								case FALSE:
									switch($simo == 0){
										case TRUE:
											$keys = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.'','odc'));
											switch($keys != 0){
												case TRUE:
													$v = count($this->dbfun_model->get_all_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$chiave.'','prv'));
													switch($v != 0){
														case TRUE:
															$var = $this->dbfun_model->get_all_data_bys('varian_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$chiave.'','prv');
															foreach($var as $vr){
																$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$vr->object_id.' and varian_id = '.$vr->varian_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																if($cek == 0){				
																	$custom_tkt = array(
																		'ccs_id' => $this->zone_id,
																		'discount' => $this->input->post('default'),
																		'oid' => $vr->object_id,
																		'vstatus' => 1,
																		'code' => $code,
																		'varian_id' => $vr->varian_id,
																		'datecreated' => date('Y-m-d H:i:s'),
																		'createdby' => $this->admin_id
																	);	
																	$value = $this->new_data('tkt',$custom_tkt, '','','','');	

																	$update = array(
																		'dateupdated' => date('Y-m-d H:i:s'),
																		'dstatus' => 1,
																		'updatedby' => $this->admin_id
																	);
																	$this->dbfun_model->update_table(array('object_id' => $chiave, 'ccs_id' => $this->zone_id, 'ccs_key' => $url, 'category_id' => $parole, 'varian_id' => $vr->varian_id ), $update, 'prv');																		
																	$status = 'success';
																	$message = 'Data has been successfully created. Thankyou';
																}else{																									
																	$status = 'error';
																	$message = 'Data already exist on the system, please input another product data. Cheers';
																}
															}
															break;
														case FALSE:
															$prc = $this->dbfun_model->get_all_data_bys('price_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$chiave.' and vstatus = 0','prc');
															foreach($prc as $pr){
																$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$pr->object_id.' and varian_id = 0 and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																if($cek == 0){				
																	$custom_tkt = array(
																		'ccs_id' => $this->zone_id,
																		'discount' => $this->input->post('default'),
																		'oid' => $pr->object_id,
																		'vstatus' => 0,
																		'code' => $code,
																		'varian_id' => 0,
																		'datecreated' => date('Y-m-d H:i:s'),
																		'createdby' => $this->admin_id
																	);	
																	$value = $this->new_data('tkt',$custom_tkt, '','','','');

																	$update = array(
																		'dateupdated' => date('Y-m-d H:i:s'),
																		'dstatus' => 1,
																		'updatedby' => $this->admin_id
																	);
																	$this->dbfun_model->update_table(array('object_id' => $chiave, 'ccs_id' => $this->zone_id, 'ccs_key' => $url, 'category_id' => $parole ), $update,'prc');														
																	$status = 'success';
																	$message = 'Data has been successfully created. Thankyou';
																}else{																									
																	$status = 'error';
																	$message = 'Data already exist on the system, please input another product data. Cheers';
																}
															}
															break;
													}
													break;
											}
											break;
										case FALSE:
											$keys = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.'','odc'));
											switch($keys != 0){
												case TRUE:
													$v = count($this->dbfun_model->get_all_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$chiave.'','prv'));
													switch($v != 0){
														case TRUE:
															$var = $this->dbfun_model->get_all_data_bys('varian_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$chiave.'','prv');
															foreach($var as $vr){
																$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$vr->object_id.' and varian_id = '.$vr->varian_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																if($cek == 0){				
																	$custom_tkt = array(
																		'ccs_id' => $this->zone_id,
																		'discount' => $this->input->post('discount'),
																		'oid' => $vr->object_id,
																		'vstatus' => 1,
																		'code' => $code,
																		'varian_id' => $vr->varian_id,
																		'datecreated' => date('Y-m-d H:i:s'),
																		'createdby' => $this->admin_id
																	);	
																	$value = $this->new_data('tkt',$custom_tkt, '','','','');		
																	$status = 'success';
																	$message = 'Data has been successfully created. Thankyou';
																}else{																									
																	$status = 'error';
																	$message = 'Data already exist on the system, please input another product data. Cheers';
																}
															}
															break;
														case FALSE:
															$prc = $this->dbfun_model->get_all_data_bys('price_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$chiave.' and vstatus = 0','prc');
															foreach($prc as $pr){
																$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$pr->object_id.' and varian_id = 0 and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																if($cek == 0){				
																	$custom_tkt = array(
																		'ccs_id' => $this->zone_id,
																		'discount' => $this->input->post('discount'),
																		'oid' => $pr->object_id,
																		'vstatus' => 0,
																		'code' => $code,
																		'varian_id' => 0,
																		'datecreated' => date('Y-m-d H:i:s'),
																		'createdby' => $this->admin_id
																	);	
																	$value = $this->new_data('tkt',$custom_tkt, '','','','');		
																	$status = 'success';
																	$message = 'Data has been successfully created. Thankyou';
																}else{																									
																	$status = 'error';
																	$message = 'Data already exist on the system, please input another product data. Cheers';
																}
															}
															break;
													}
													break;
											}
											break;
									}
									break;
								case TRUE:
									switch($simo == 0){
										case TRUE:
											$keys = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.'','odc'));
											switch($keys != 0){
												case TRUE:
													$keys = $this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.'','odc');
													foreach($keys as $k){
														$v = count($this->dbfun_model->get_all_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$k->object_id.'','prv'));
														switch($v != 0){
															case TRUE:
																$var = $this->dbfun_model->get_all_data_bys('varian_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$k->object_id.'','prv');
																foreach($var as $vr){
																	$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$vr->object_id.' and varian_id = '.$vr->varian_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																	if($cek == 0){				
																		$custom_tkt = array(
																			'ccs_id' => $this->zone_id,
																			'discount' => $this->input->post('default'),
																			'oid' => $vr->object_id,
																			'vstatus' => 1,
																			'code' => $code,
																			'varian_id' => $vr->varian_id,
																			'datecreated' => date('Y-m-d H:i:s'),
																			'createdby' => $this->admin_id
																		);	
																		$value = $this->new_data('tkt',$custom_tkt, '','','','');	
																		$update = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'dstatus' => 1,
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('object_id' => $k->object_id, 'ccs_id' => $this->zone_id, 'ccs_key' => $url, 'category_id' => $parole , 'varian_id' => $vr->varian_id ), $update, 'prv');
																		
																		$status = 'success';
																		$message = 'Data has been successfully created. Thankyou';
																	}else{																									
																		$status = 'error';
																		$message = 'Data already exist on the system, please input another product data. Cheers';
																	}
																}
																break;
															case FALSE:
																$prc = $this->dbfun_model->get_all_data_bys('price_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$k->object_id.' and vstatus = 0','prc');
																foreach($prc as $pr){
																	$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$pr->object_id.' and varian_id = 0 and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																	if($cek == 0){				
																		$custom_tkt = array(
																			'ccs_id' => $this->zone_id,
																			'discount' => $this->input->post('default'),
																			'oid' => $pr->object_id,
																			'vstatus' => 0,
																			'code' => $code,
																			'varian_id' => 0,
																			'datecreated' => date('Y-m-d H:i:s'),
																			'createdby' => $this->admin_id
																		);	
																		$value = $this->new_data('tkt',$custom_tkt, '','','','');	

																		$update = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'dstatus' => 1,
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('object_id' => $k->object_id, 'ccs_id' => $this->zone_id, 'ccs_key' => $url, 'category_id' => $parole ), $update, 'prc');																				
																		$status = 'success';
																		$message = 'Data has been successfully created. Thankyou';
																	}else{																									
																		$status = 'error';
																		$message = 'Data already exist on the system, please input another product data. Cheers';
																	}
																}
																break;
														}
													}
													break;
												case FALSE:														
													$status = 'error';
													$message = 'Data has been successfully updated. Thankyou';
													break;
											}
											break;
										case FALSE:
											$keys = count($this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.'','odc'));
											switch($keys != 0){
												case TRUE:
													$keys = $this->dbfun_model->get_all_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.'','odc');
													foreach($keys as $k){
														$v = count($this->dbfun_model->get_all_data_bys('varian_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$k->object_id.'','prv'));
														switch($v != 0){
															case TRUE:
																$var = $this->dbfun_model->get_all_data_bys('varian_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$k->object_id.'','prv');
																foreach($var as $vr){
																	$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$vr->object_id.' and varian_id = '.$vr->varian_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																	if($cek == 0){				
																		$custom_tkt = array(
																			'ccs_id' => $this->zone_id,
																			'discount' => $this->input->post('discount'),
																			'oid' => $vr->object_id,
																			'vstatus' => 1,
																			'code' => $code,
																			'varian_id' => $vr->varian_id,
																			'datecreated' => date('Y-m-d H:i:s'),
																			'createdby' => $this->admin_id
																		);	
																		$value = $this->new_data('tkt',$custom_tkt, '','','','');	
																		
																		$update = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'dstatus' => 1,
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('object_id' => $k->object_id, 'ccs_id' => $this->zone_id, 'ccs_key' => $url, 'category_id' => $parole, 'varian_id' => $vr->varian_id ), $update, 'prv');
																		$status = 'success';
																		$message = 'Data has been successfully created. Thankyou';
																	}else{																									
																		$status = 'error';
																		$message = 'Data already exist on the system, please input another product data. Cheers';
																	}
																}
																break;
															case FALSE:
																$prc = $this->dbfun_model->get_all_data_bys('price_id, object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$url.' and category_id ='.$parole.' and object_id = '.$k->object_id.' and vstatus = 0','prc');
																foreach($prc as $pr){
																	$cek = count($this->dbfun_model->get_all_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and url_id = '.$url.' and cat_id ='.$parole.' and oid = '.$pr->object_id.' and varian_id = 0 and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$o.'','tkt'));
																	if($cek == 0){				
																		$custom_tkt = array(
																			'ccs_id' => $this->zone_id,
																			'discount' => $this->input->post('discount'),
																			'oid' => $pr->object_id,
																			'vstatus' => 0,
																			'code' => $code,
																			'varian_id' => 0,
																			'datecreated' => date('Y-m-d H:i:s'),
																			'createdby' => $this->admin_id
																		);	
																		$value = $this->new_data('tkt',$custom_tkt, '','','','');	

																		$update = array(
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'dstatus' => 1,
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('object_id' => $k->object_id, 'ccs_id' => $this->zone_id, 'ccs_key' => $url, 'category_id' => $parole ), $update, 'prc');																				
																		$status = 'success';
																		$message = 'Data has been successfully created. Thankyou';
																	}else{																									
																		$status = 'error';
																		$message = 'Data already exist on the system, please input another product data. Cheers';
																	}
																}
																break;
														}													
													}
													break;
												case FALSE:														
													$status = 'error';
													$message = 'Data has been successfully updated. Thankyou';
													break;
											} 
											break;
									}
									break;
							}
						}
						else{
							$status = 'error';
							$message = validation_errors();
						}
					}
				}					
				elseif($key == 'web'){
					$id = $this->input->post('rc_id');
					$object = $this->input->post('object_id');
					if($id){	
						if($cat == 1){
							if($object == 0){									
								$this->form_validation->set_rules('char_1', 'Company Name', 'xss_clean|trim|max_length[75]');
								$this->form_validation->set_rules('char_2', 'Email Account', 'trim|xss_clean|max_length[50]|valid_email');
								$this->form_validation->set_rules('char_3', 'Phone Number', 'trim|xss_clean|max_length[15]');
								$this->form_validation->set_rules('char_4', 'Fax Number', 'trim|xss_clean|max_length[15]');
								$this->form_validation->set_rules('char_5', 'Company Address', 'trim|xss_clean|max_length[250]');
								$this->form_validation->set_rules('char_6', 'Short Description', 'trim|xss_clean|max_length[250]');
								$this->form_validation->set_rules('url_id', 'Postal Code', 'numeric|xss_clean|max_length[5]');
							}
							elseif($object == 1){
								$this->form_validation->set_rules('char_1', 'Bank Provider Name', 'xss_clean|trim|max_length[25]');
								$this->form_validation->set_rules('char_2', 'Account Name', 'trim|xss_clean|max_length[50]');
								$this->form_validation->set_rules('char_3', 'Account Number', 'trim|xss_clean|max_length[25]');
								$this->form_validation->set_rules('char_4', 'Short Description', 'trim|xss_clean|max_length[100]');									
							}
							elseif($object == 2){									
								$this->form_validation->set_rules('char_2', 'Interval Type', 'xss_clean|trim|max_length[15]');
								$this->form_validation->set_rules('char_3', 'Interval', 'numeric|xss_clean|max_length[2]');
								$this->form_validation->set_rules('char_4', 'Short Description', 'trim|xss_clean|max_length[250]');		
							}							
						}
						elseif($cat == 2){
							$this->form_validation->set_rules('object_id', 'Email Type', 'xss_clean|numeric|max_length[1]');
							$this->form_validation->set_rules('char_1', 'SMTP Port', 'xss_clean|numeric|max_length[5]');
							$this->form_validation->set_rules('char_2', 'SMTP Host', 'trim|xss_clean|max_length[50]');
							$this->form_validation->set_rules('char_3', 'Password', 'trim|xss_clean|max_length[25]');
							$this->form_validation->set_rules('char_4', 'Name', 'trim|xss_clean|max_length[50]');
							$this->form_validation->set_rules('char_5', 'Email Account', 'trim|xss_clean|max_length[50]|valid_email');
							$this->form_validation->set_rules('char_6', 'Short Description', 'trim|xss_clean|max_length[250]');
						}
						if ($this->form_validation->run() == TRUE){
							switch($cat){
								case '2':
									$c1 = trim($this->input->post('char_1'));
									$c2 = trim($this->input->post('char_2'));
									$pk = trim($this->input->post('password'));
									$c4 = trim($this->input->post('char_4'));
									$c5 = trim($this->input->post('char_5'));
									$c6 = trim($this->input->post('char_6'));
									$c7 = trim($this->input->post('char_7'));
									$cek = count($this->dbfun_model->get_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.' and rc_id = '.$id.' ','orc'));
									$content = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.' and rc_id = '.$id.' ','orc');
									$co1 = trim($content->char_1);
									$co2 = trim($content->char_2);
									$co3 = trim($content->char_3);										
									if($pk == 0){
										$c3 = $co3;
									}else{
										$c3 = 'b03zc0n'.trim($this->input->post('char_3'));
									}
									$co4 = trim($content->char_4);
									$co5 = trim($content->char_5);
									$co6 = trim($content->char_6);
									$co7 = trim($content->char_7);
									$cos = $content->status;
									if(($c1 == $co1)&&($c2 == $co2)&&($c3 == $co3)&&($c4 == $co4)&&($c5 == $co5)&&($c6 == $co6)&&($c7 == $co7)&&($pk == 0)&&($status == $cos)){
										$valid = 1;
									}else{
										$valid = 0;
									}
									if(($valid == 1)&&($cek == 1)){											
										$status = "error";
										$message = "No data updated";
									}
									else{		
										$cekhost = count($this->dbfun_model->get_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and char_5 = "'.$c5.'" and rc_id != '.$id.'','orc'));
										$cekname = count($this->dbfun_model->get_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and char_4 = "'.$c4.'" and rc_id != '.$id.'','orc'));
										if($pk == 1){
											if($c3 == $co3){
												$c8 = trim($this->input->post('char_8'));
												$c9 = trim($this->input->post('char_9'));
												if(($c8 != $c9)){
													$pass = 2;
												}else{
													$pass = 0;
												}													
											}else{
												$pass = 1;
											}
										}
										if($cekhost == 1){																	
											$status = "error";
											$message = "Email data for ".$c5." already exist on the systems";
										}elseif(($cekhost == 0)&&($cekname == 1)&&($pk)){
											$status = "error";
											$message = "Name for ".strtolower(trim($c4))." already exist on the systems";										
										}elseif(($cekhost == 0)&&($cekname == 0)&&($pk == 1)&&($pass == 1)){
											$status = "error";
											$message = "Sorry, your old password doesn't match on the system database. Cheers";										
										}elseif(($cekhost == 0)&&($cekname == 0)&&($pk == 1)&&($pass == 2)){
											$status = "error";
											$message = "Sorry, your new password doesn't match. Cheers";										
										}else{
											if($cekhost == 1){
												$host = $co5;
											}else{
												$host = $c5;
											}
											if($cekname == 1){
												$cname = $co4;
											}else{
												$cname = $c4;
											}
											if($pk ==1){
												if($pass == 1){
													$newpass = $co3;
												}elseif($pass == 0){
													$newpass = 'b03zc0n'.$c8;
												}													
											}else{
												$newpass = $co3;
											}
											$update_email = array(
												'char_1' => $c1,
												'char_2' => $c2,
												'char_3' => $newpass,
												'char_4' => $cname,
												'char_5' => $host,
												'char_6' => $c6,
												'char_7' => $c7,
												'status' => $status
											);
											$this->dbfun_model->update_table(array('rc_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $object ), $update_email, 'orc');
											$status = 'success';
											$message = 'Data has been updated. Thank you...';
										}											
									}
									break;
								case '1':
									if($object == 0){
										$c1 = trim($this->input->post('char_1'));
										$c2 = trim($this->input->post('char_2'));
										$c3 = trim($this->input->post('char_3'));
										$c4 = trim($this->input->post('char_4'));
										$c5 = trim($this->input->post('char_5'));
										$c6 = trim($this->input->post('char_6'));
										$c7 = trim($this->input->post('char_7'));
										$c8 = $this->input->post('url_id');
										$content = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.' and rc_id = '.$id.' ','orc');
										$co1 = trim($content->char_1);
										$co2 = trim($content->char_2);
										$co3 = trim($content->char_3);
										$co4 = trim($content->char_4);
										$co5 = trim($content->char_5);
										$co6 = trim($content->char_6);
										$co7 = trim($content->char_7);
										$co8 = trim($content->url_id);
										$cos = $content->status;
										if(($c1 == $co1)&&($c2 == $co2)&&($c3 == $co3)&&($c4 == $co4)&&($c5 == $co5)&&($c6 == $co6)&&($c7 == $co7)&&($c8 == $co8)&&($status == $cos)){
											$valid = 1;
										}else{
											$valid = 0;
										}
										if($valid == 0){		
											$update_info = array(
												'char_1' => $c1,
												'char_2' => $c2,
												'char_3' => $c3,
												'char_4' => $c4,
												'char_5' => $c5,
												'char_6' => $c6,
												'char_7' => $c7,
												'url_id' => $c8,
												'status' => $status
											);
											$this->dbfun_model->update_table(array('rc_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $object ), $update_info, 'orc');
											$status = 'success';
											$message = 'Data has been updated. Thank you...';
										}else{
											$status = 'error';
											$message = 'No data updated.';
										}
									}elseif($object == 1){
										$c1 = trim($this->input->post('char_1'));
										$c2 = trim($this->input->post('char_2'));
										$c3 = trim($this->input->post('char_3'));
										$c4 = trim($this->input->post('char_4'));
										$c7 = trim($this->input->post('char_7'));
										$content = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.' and rc_id = '.$id.' ','orc');
										$co1 = trim($content->char_1);
										$co2 = trim($content->char_2);
										$co3 = trim($content->char_3);
										$co4 = trim($content->char_4);
										$cos = $content->status;
										if(($c1 == $co1)&&($c2 == $co2)&&($c3 == $co3)&&($c4 == $co4)&&($status == $cos)){
											$valid = 1;
										}else{
											$valid = 0;
										}
										if($valid == 0){	
										
											$ceka = count($this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.' and rc_id != '.$id.' and char_3 = "'.$c3.'" ','orc'));
											
											if($ceka == 1){
												$status = 'error';
												$message = 'Account number for'.$c3.' already exist on the systems. Cheers';
											}else{
												$update_account = array(
													'char_1' => $c1,
													'char_2' => $c2,
													'char_3' => $c3,
													'char_4' => $c4,
													'char_7' => $c7,
													'status' => $status
												);
												$this->dbfun_model->update_table(array('rc_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $object ), $update_account, 'orc');
												$status = 'success';
												$message = 'Data has been updated. Thank you...';
											}
										}else{
											$status = 'error';
											$message = 'No data updated.';
										}
									}elseif($object == 2){												
										$c3 = trim($this->input->post('char_3'));
										$c4 = trim($this->input->post('char_4'));
										$c7 = trim($this->input->post('char_7'));
										$content = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.' and rc_id = '.$id.' ','orc');
										$co3 = trim($content->char_3);
										$co4 = trim($content->char_4);
										$cos = $content->status;
										if(($c3 == $co3)&&($c4 == $co4)&&($status == $cos)){
											$valid = 1;
										}else{
											$valid = 0;
										}
										if($valid == 0){	
											$update_interval = array(
												'char_3' => $c3,
												'char_4' => $c4,
												'char_7' => $c7,
												'status' => $status
											);
											$this->dbfun_model->update_table(array('rc_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat, 'object_id' => $object ), $update_interval, 'orc');
											$status = 'success';
											$message = 'Data has been updated. Thank you...';
										}else{
											$status = 'error';
											$message = 'No data updated.';
										}
									}
									break;
							}
						
						}else{
							$status = 'error';
							$message = validation_errors();
						}
					}
					else{
						
						$this->form_validation->set_rules('category_id', 'Category', 'xss_clean|required|numeric|max_length[1]');	
						if($cat == 1){
							$this->form_validation->set_rules('object_id', 'Payment Info', 'xss_clean|required|numeric|max_length[1]');	
							if($object == 0){									
								$this->form_validation->set_rules('char_1', 'Company Name', 'xss_clean|required|trim|max_length[75]');
								$this->form_validation->set_rules('char_2', 'Email Account', 'trim|xss_clean|required|max_length[50]|valid_email');
								$this->form_validation->set_rules('char_3', 'Phone Number', 'trim|xss_clean|required|max_length[15]');
								$this->form_validation->set_rules('char_4', 'Fax Number', 'trim|xss_clean|max_length[15]');
								$this->form_validation->set_rules('char_5', 'Company Address', 'trim|xss_clean|required|max_length[250]');
								$this->form_validation->set_rules('char_6', 'Short Description', 'trim|xss_clean|required|max_length[250]');
								$this->form_validation->set_rules('url_id', 'Postal Code', 'numeric|xss_clean|required|max_length[5]');
							}
							elseif($object == 1){
								$this->form_validation->set_rules('char_1', 'Bank Provider Name', 'xss_clean|required|trim|max_length[25]');
								$this->form_validation->set_rules('char_2', 'Account Name', 'trim|xss_clean|required|max_length[50]');
								$this->form_validation->set_rules('char_3', 'Account Number', 'trim|xss_clean|required|max_length[25]');
								$this->form_validation->set_rules('char_4', 'Short Description', 'trim|xss_clean|max_length[100]');									
							}
							elseif($object == 2){									
								$this->form_validation->set_rules('char_2', 'Interval Type', 'xss_clean|required|trim|max_length[15]');
								$this->form_validation->set_rules('char_3', 'Interval', 'numeric|xss_clean|required|max_length[2]');
								$this->form_validation->set_rules('char_4', 'Short Description', 'trim|required|xss_clean|max_length[250]');		
							}							
						}
						elseif($cat == 2){
							$this->form_validation->set_rules('object_id', 'Email Type', 'xss_clean|required|numeric|max_length[1]');
							$this->form_validation->set_rules('char_1', 'SMTP Port', 'xss_clean|required|numeric|max_length[5]');
							$this->form_validation->set_rules('char_2', 'SMTP Host', 'trim|xss_clean|required|max_length[50]');
							$this->form_validation->set_rules('char_3', 'Password', 'trim|xss_clean|required|max_length[25]');
							$this->form_validation->set_rules('char_4', 'Name', 'trim|xss_clean|required|max_length[50]');
							$this->form_validation->set_rules('char_5', 'Email Account', 'trim|xss_clean|required|max_length[50]|valid_email');
							$this->form_validation->set_rules('char_6', 'Short Description', 'trim|xss_clean|required|max_length[250]');
						}
						$this->form_validation->set_rules('status', 'Activation Status', 'xss_clean|required|numeric|max_length[1]');
						if ($this->form_validation->run() == TRUE){
							if($cat == 2){
								$email =  trim($this->input->post('char_5'));
								$cek = count($this->dbfun_model->get_all_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.'','orc'));
								if($cek == 0){
									$cek2 = count($this->dbfun_model->get_all_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and char_5 = "'.$email.'"','orc'));
									if($cek2 == 0){
										$password =  trim($this->input->post('char_3'));
										$confirm =  trim($this->input->post('char_7'));
										if($password == $confirm){		
											if($object == 0){
												$etype = 'subscribe';
											}elseif($object == 1){	
												$etype = 'online';												
											}elseif($object == 2){	
												$etype = 'blast';												
											}
											$emaildata = array(
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'category_id' => $cat,
												'object_id' => $object,
												'char_1' => $this->input->post('char_1'),
												'char_2' => $this->input->post('char_2'),
												'char_3' => 'b03zc0n'.$password,
												'char_4' => $this->input->post('char_4'),
												'char_5' => $email,
												'char_6' => $this->input->post('char_6'),
												'char_7' =>	$etype,
												'status' => $this->input->post('status')
											);	
											$this->new_data('ccs_orc',$emaildata, '','', '','');
											$status = 'success';
											$message = 'Data created successfully';	
										}else{												
											$status = 'error';
											$message = "Password confirmation doesn't match. Be sure about your password input. Cheers";	
										}
									}else{
										$status = 'error';
										$message = 'Email '.strtolower($email).' already exist on the system. Please input another email account';	
									}
								}else{
									$status = 'error';
									$message = 'Data already exist on the system. Cheers';	
								}
							}
							elseif($cat == 1){	
								if($object == 0){
									$etype = 'info';
								}elseif($object == 1){	
									$etype = 'bank';												
								}elseif($object == 2){	
									$etype = 'interval';												
								}	
								if($object == 1){									
									$account =  trim($this->input->post('char_3'));
									$cek = count($this->dbfun_model->get_all_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and char_3 = "'.$account.'"','orc'));
									if($cek == 0){
										$accountdata = array(
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key,
											'category_id' => $cat,
											'object_id' => $object,
											'char_1' => $this->input->post('char_1'),
											'char_2' => $this->input->post('char_2'),
											'char_3' => $this->input->post('char_3'),
											'char_4' => $this->input->post('char_4'),
											'char_7' =>	$etype,
											'status' => $this->input->post('status')
										);	
										$this->new_data('ccs_orc',$accountdata, '','', '','');
										$status = 'success';
										$message = 'Data created successfully';	
									}else{											
										$status = 'error';
										$message = 'Sorry, bank account number for '.$account.'  already exist on the system. Cheers';	
									}
									
								}elseif($object == 0){			
									$cek = count($this->dbfun_model->get_all_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and object_id = '.$object.'','orc'));
									if($cek == 0){
										$infodata = array(
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key,
											'category_id' => $cat,
											'object_id' => $object,
											'url_id' => $this->input->post('url_id'),
											'char_1' => $this->input->post('char_1'),
											'char_2' => $this->input->post('char_2'),
											'char_3' => $this->input->post('char_3'),
											'char_4' => $this->input->post('char_4'),
											'char_5' => $this->input->post('char_5'),
											'char_6' => $this->input->post('char_6'),
											'char_7' =>	$etype,
											'status' => $this->input->post('status')
										);	
										$this->new_data('ccs_orc',$infodata, '','', '','');
										$status = 'success';
										$message = 'Data created successfully';	
									}else{											
										$status = 'error';
										$message = 'Sorry, data already exist on the system. Cheers';	
									}
								}elseif($object == 2){
									$interval =  trim($this->input->post('char_2'));
									$cek = count($this->dbfun_model->get_all_data_bys('rc_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and char_2 = "'.$interval.'"','orc'));
									if($cek == 0){
										if($interval == 'hours'){
											$timely = "Hourly Interval";
										}else{
											$timely = "Daily Interval";
										}
										$infodata = array(
											'ccs_id' => $this->zone_id,
											'ccs_key' => $ccs_key,
											'category_id' => $cat,
											'object_id' => $object,
											'char_1' => $timely,
											'char_2' => $this->input->post('char_2'),
											'char_3' => $this->input->post('char_3'),
											'char_4' => $this->input->post('char_4'),
											'char_7' =>	$etype,
											'status' => $this->input->post('status')
										);	
										$this->new_data('ccs_orc',$infodata, '','', '','');
										$status = 'success';
										$message = 'Data created successfully';	
									}else{											
										$status = 'error';
										$message = 'Sorry, data '.$interval.' interval already exist on the system. Cheers';	
									}
								}
								
							}
						}
						else{
							$status = 'error';
							$message = validation_errors();
						}
					}
				}
				elseif($key == 'edit_product'){
					$var = $this->input->post('varian_id');
					$id = $this->input->post('oid');
					$disc = $this->input->post('discount');
					$stock = $this->input->post('stock');
					$ticket = $this->input->post('ticket_id');
					$cek = count($this->dbfun_model->get_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and ticket_id = '.$ticket.' and oid = '.$id.' and varian_id = '.$var.' and discount = '.$disc.' and stock = '.$stock.' and status = '.$status.' ','tkt'));
					if($cek == 1){							
						$status = 'error';
						$message = 'No data updated. Cheers';
					}else{		
						$update = array(
							'dateupdated' => date('Y-m-d H:i:s'),
							'updatedby' => $this->admin_id,
							'discount' => $disc,
							'stock' => $stock,
							'status' => $status
						);
						$this->dbfun_model->update_table(array('oid' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'ticket_id' => $ticket ), $update, 'tkt');
						$status = 'success';
						$message = 'Data has been updated. Thank you...';							
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}				
			elseif(($type) &&($param == 'pages')){ 
				$id = $this->input->post('object_id');
				$ccs_key = $this->input->post('ccs_key');
				$lang = $this->input->post('language_id');
				$cat = $this->input->post('category_id');
				$admin_id = $this->admin_id;

				if($id)
				{//update
					$chiave = trim($this->input->post('metakeyword')); 
					$parole = trim($this->input->post('metadescription'));
					$segretis = trim($this->input->post('title')); 
					$simo = trim($this->input->post('tagline'));
					$ttl = trim($this->input->post('ttl'));
					$desc = $this->input->post('description');
					
					if($chiave || $parole){
						$cek = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and object_id = '.$id.' and ccs_key = '.$ccs_key.' and metakeyword = "'.$chiave.'" and metadescription = "'.$parole.'"','odc');
						if(!$cek){	
							$update = array(
								'dateupdated' => date('Y-m-d H:i:s'),
								'metakeyword' => $chiave,
								'metadescription' => $parole,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}else{								
							$status = 'error';
							$message = 'No data updated. Please check the data you want to update..';
						}
					}elseif($simo || $segretis || $desc){
						$ceks = $this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and object_id = '.$id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" ','odc');
						$ct = $cat;
						
						$times = $this->input->post('date');
						$pub = $this->input->post('datepublish');
						if(($status == 2) && ($pub)){
							$publish = $pub;
						}elseif(($status == 2) && ($times)){
							$publish = $times;
						}else{
							$publish = '';
						}
						
						if(!$ceks){	
							$update = array(
								'title' => $segretis,
								'tagline' => $simo,
								'description' =>$desc,
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'datepublish' => $publish,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}
						elseif($desc){	
						
							$update = array(
								'title' => $segretis,
								'tagline' => $simo,
								'description' =>$desc,
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'datepublish' => $publish,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}
						elseif($status != ''){
							$update = array(
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'datepublish' => $publish,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'odc');
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
						}elseif($ct){
							$update = array(
								'category_id' => $ct,
								'dateupdated' => date('Y-m-d H:i:s'),
								'status' => $status,
								'updatedby' => $this->admin_id
							);
							$this->dbfun_model->update_table(array('settings_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key, 'category_id' => $cat ), $update, 'ocs');
							$status = 'success';
							$message = 'Data has been updated. Thank you...';
							
						}
						
						$hf = count($this->dbfun_model->get_all_data_bys('ccs_odc.object_id','ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_odc.ccs_id = '.$this->zone_id.' and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_odc.ccs_key = '.$ccs_key.' and ccs_odc.category_id = ccs_oct.category_id and ccs_odc.title = "'.$ttl.'" and ccs_oct.title != "Page"','odc, oct'));
						if($hf != 0){
							$keys = $this->dbfun_model->get_all_data_bys('ccs_odc.object_id','ccs_odc.ccs_id = ccs_oct.ccs_id and ccs_odc.ccs_id = '.$this->zone_id.' and ccs_odc.ccs_key = ccs_oct.ccs_key and ccs_odc.ccs_key = '.$ccs_key.' and ccs_odc.category_id = ccs_oct.category_id and ccs_odc.title = "'.$ttl.'" and ccs_oct.title != "Page"','odc, oct');
							foreach($keys as $k){
								$update_hf = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'title' => $segretis,
									'tagline' => strtolower(str_replace(' ','-', $segretis)),
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('object_id' => $k->object_id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_hf, 'odc');
							}
						}
						$mn = count($this->dbfun_model->get_all_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$ttl.'" and type_id = "page"','mnu'));
						if($mn != 0){
							$menu = $this->dbfun_model->get_all_data_bys('menu_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$ttl.'" and type_id = "page"','mnu');
							foreach($menu as $m){
								$update_mn = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'title' => $segretis,
									'link' => strtolower(str_replace(' ','-', $segretis)),
									'updatedby' => $this->admin_id
								);
								$this->dbfun_model->update_table(array('menu_id' => $m->menu_id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_mn, 'mnu');
							}
						}
							$status = 'success';
							$message = 'Data has been updated. Thank you...'.$mn;
						
						if(isset($_FILES['image_square'])){
							$updates = $this->update_data_bys('*', array('object_id' => $id), 'ccs_o','image_square', $this->zone.'/'.$type,'image_square','jpeg|jpg|png');
						}else{
							$updates = $this->update_data_bys('*', array('object_id' => $id), 'ccs_o','','','','');
						}
						$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $updates, 'o');
					}					
					else{
						$status = 'error';
						$message = 'No data updated. Please check the data you want to update..';
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) &&($param == 'category')){ 
				$id = $this->input->post('category_id');					
				$title = trim($this->input->post('title'));				
				$desc = trim($this->input->post('description'));
				$lang = $this->input->post('language_id');
				$ccs_key = $this->input->post('ccs_key');
				$admin_id = $this->admin_id;
				$img_from = strtolower($this->input->post('image_from'));
				$img_from_other = $this->input->post('image_square2');
				if($id){ //update
					if ($this->form_validation->run('ccs_oct') === TRUE){
						$prt = $this->input->post('parent_id');
						$parole = $this->dbfun_model->get_data_bys('parent_id, title, description','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and language_id = '.$lang.'','oct');	
						$ttl = trim($parole->title);
						$dsc = trim($parole->description);
						$parent = $parole->parent_id;
						
						switch($ttl == $title){
							case TRUE:
								$segretis = $ttl;
								break;
							case FALSE:
								$segretis = $this->input->post('title');
								break;
						}
						$status_image = FALSE;
						if(isset($_FILES['image_square'])){
							$update_image = $this->update_data_bys('image_square', array('category_id' => $id), 'ccs_oct','image_square', $this->zone.'/category','image_square','jpeg|jpg|png|mp4');
							$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update_image, 'oct');
							$status_image = TRUE;
						}elseif(!empty($img_from) && !empty($img_from_other)){//FDL WAS HERE HAHAHA
							$type = 'category';
							$prev_img = $this->dbfun_model->get_data_bys('image_square',array('category_id' => $id,'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key),'oct')->image_square;
							$cek_from_oct = count($this->dbfun_model->get_all_data_bys('image_square',array('image_square' => $prev_img,'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key),'oct'));
							if($cek_from_oct == 1){
								$file_path = './assets/'.$this->zone.'/'.$type.'/'.$prev_img;
								if (file_exists($file_path))
								{
									unlink($file_path);
								}
							}
							
							if($img_from == $type){
								$data_name = $img_from_other;
							}else{
								$data_from = './assets/'.$this->zone.'/'.$img_from.'/'.$img_from_other;
								$data_to = './assets/'.$this->zone.'/'.$type.'/'.$img_from_other;
								$data_name = $img_from_other;
								copy($data_from, $data_to);
							}
							$update_image = array();
							$update_image['image_square'] = $data_name;
							$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update_image, 'oct');
							$status_image = TRUE;
						}
						$chiave = count($this->dbfun_model->get_data_bys('category_id','category_id = '.$id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" and language_id = '.$lang.'','oct'));	
						switch($chiave){
							case '0':	
								switch($prt == $parent){
									case TRUE:
										$cek = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" and language_id = '.$lang.'','oct'));
										switch($cek){
											case '1':
												$status = 'error';
												$message = 'Data already exist on the system. Please choose another name / title for your category. Cheers. atas ';
												break;
											case '0':
												switch($dsc == $desc){
													case TRUE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'title' => $segretis
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');										
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
													case FALSE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'title' => $segretis,
															'description' => $desc
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');	
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.'.$desc;
														break;
												}
												break;
										}
										break;
									case FALSE:
										switch($prt == 0){
											case TRUE:
												$cek = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" and language_id = '.$lang.'','oct'));
												switch($cek){
													case '1':
														$status = 'error';
														$message = 'Data already exist on the system. Please choose another name / title for your category. Cheers. atas ';
														break;
													case '0':
														switch($dsc == $desc){
															case TRUE:
																$update = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'title' => $segretis, 
																	'parent_id' => $prt,
																	'parent_title' => $prl
																);
																$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');										
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.';
																break;
															case FALSE:
																$update = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'title' => $segretis,
																	'description' => $desc,
																	'parent_id' => $prt,
																	'parent_title' => $prl
																);
																$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');	
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.'.$desc;
																break;
														}
														break;
												}
												break;
											case FALSE:													
												$prl = $this->input->post('prl');
												$cek = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" and language_id = '.$lang.'','oct'));
												switch($cek){
													case '1':
														$status = 'error';
														$message = 'Data already exist on the system. Please choose another name / title for your category. Cheers. atas ';
														break;
													case '0':
														switch($dsc == $desc){
															case TRUE:
																$update = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'title' => $segretis, 
																	'parent_id' => $prt,
																	'parent_title' => $prl
																);
																$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');										
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.';
																break;
															case FALSE:
																$update = array(
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'title' => $segretis,
																	'description' => $desc,
																	'parent_id' => $prt,
																	'parent_title' => $prl
																);
																$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');	
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.'.$desc;
																break;
														}
														break;
												}
												break;
										}
										break;
								}
								break;
							case '1':
								switch($prt == 0){
									case TRUE:
										$cek = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" and parent_id = 0 and language_id = '.$lang.'','oct'));
										switch($cek){
											case '1':
												switch($dsc == $desc){
													case FALSE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'description' => $desc
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');										
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
													case TRUE:
														$status = 'error';
														$message = 'No data updated.';
														if($status_image){
															$status = 'success';
															$message = 'Data has been successfully updated. Thankyou.';
														}
														break;
												}
												break;
											case '0':
												switch($dsc == $desc){
													case TRUE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'parent_id' => 0,
															'parent_title' => ''
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');										
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
													case FALSE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'parent_id' => 0,
															'parent_title' => '',
															'description' => $desc
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');	
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.'.$desc;
														break;
												}
												break;
										}
										break;
									case FALSE:
										$prl = $this->input->post('prl');
										$cek = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'" and parent_id = '.$prt.' and language_id = '.$lang.'','oct'));
										switch($cek){
											case '1':
												switch($dsc == $desc){
													case FALSE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'description' => $desc
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');										
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
													case TRUE:	
														$status = 'error';
														$message = 'No data updated.';
														if($status_image){
															$status = 'success';
															$message = 'Data has been successfully updated. Thankyou.';
														}
														break;
												}
												break;
											case '0':
												switch($dsc == $desc){
													case TRUE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'title' => $segretis, 
															'parent_id' => $prt,
															'parent_title' => $prl
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');										
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
													case FALSE:
														$update = array(
															'dateupdated' => date('Y-m-d H:i:s'),
															'title' => $segretis,
															'description' => $desc,
															'parent_id' => $prt,
															'parent_title' => $prl
														);
														$this->dbfun_model->update_table(array('category_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $update, 'oct');	
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.'.$desc;
														break;
												}
												break;
										}
								
										break;
								}
								break;
						}
					}
					else{
						$status = 'error';
						$message = validation_errors();
					}
				}					
				else{ // create
					if ($this->form_validation->run('ccs_oct') === TRUE){	
						$parent = $this->input->post('parent_id');
						if($parent != 0){								
							$title = $this->dbfun_model->get_data_bys('title','category_id = '.$parent.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' ','ccs_oct')->title;
						}else{								
							$title = '';
						}
						$segretis = trim($this->input->post('title'));
						$simo = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$segretis.'"','oct'));
						switch($simo){
							case '0':
								$category_id = $this->dbfun_model->get_data_bys('MAX(category_id) as category_id','ccs_key = '.$ccs_key.' ','ccs_oct')->category_id + 1;
								switch($type_view){
									case '0':
										switch($type_id){
											case '0':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '1':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '2':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '6':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '8':
												$keywords = $this->input->post('metakeyword');	
												$desc = $this->input->post('metadescription');
												$descs = $this->input->post('metadescriptions');
													$link = '';
													if($keywords == 'paralax'){
														$link = '#'.str_replace(' ','-',strtolower($desc));
													}elseif($keywords == 'multisite'){
														if($descs == 'post'){
															$link = str_replace(' ','-',strtolower($this->input->post('title')));
														}elseif($descs != 'post'){												
															$link = $descs;
														}else{
															$link = '';
														}
													}
												$custom_primary = array(
															'ccs_id' => $this->zone_id,
															'ccs_key' => $ccs_key,
															'status' => 1,
															'createdby' => $admin_id,
															'parent_title' => $title,
															'metadescription' => $link,
															'category_id' => $category_id,
															'datecreated' =>  date('Y-m-d H:i:s')
														);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
										}
										break;
									case '1':
										switch($type_id){												
											case '0':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '1':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '3':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '8':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
										}
										break;
									case '5':
										switch($type_id){
											case '0':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												if(isset($_FILES['image_square'])){
													$this->new_data('ccs_oct',$custom_primary, 'image_square','image_square', $this->zone.'/category','jpeg|jpg|png|mp4');
												}elseif(!empty($img_from) && !empty($img_from_other)){//FDL WAS HERE HAHAHA
													$type = 'category';
													if (!is_dir('assets/'.$this->zone.'/'.$type.'/'))
													{
														mkdir('./assets/'.$this->zone.'/'.$type.'/', 0777, true);
													}
													if($img_from == $type){
														$data_name = $img_from_other;
													}else{
														$data_from = './assets/'.$this->zone.'/'.$img_from.'/'.$img_from_other;
														$data_to = './assets/'.$this->zone.'/'.$type.'/'.$img_from_other;
														$data_name = $img_from_other;
														copy($data_from, $data_to);
													}
													
													$custom_primary['image_square'] = $data_name;
													$this->new_data('ccs_oct',$custom_primary, '','', '','');
												}else{
													$this->new_data('ccs_oct',$custom_primary, '','','','');
												}
												break;
											
											case '1':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '3':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
											case '5':
												$custom_primary = array(
													'ccs_id' => $this->zone_id,
													'ccs_key' => $ccs_key,
													'status' => 1,
													'createdby' => $admin_id,
													'parent_title' => $title,
													'category_id' => $category_id,
													'datecreated' =>  date('Y-m-d H:i:s')
												);	
												$this->new_data('ccs_oct',$custom_primary, '','', '','');
												break;
										}
										break;
								}
								$status = 'success';
								$message = 'Data created successfully';
								break;
							case '1':
								$status = 'error';
								$message = 'Category for '.ucwords($segretis).' already exist on the system. Please choose another name for your category. Cheers.';
								break;
						}
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) && ($param == 'role')){
				$admin_id = $this->admin_id;
				$id = $this->input->post('type_id');
				$key = trim($this->input->post('param2'));
				if($id){
					unset($cek);
					if ($this->form_validation->run('ccs_mdr') === TRUE){
						$module = $this->input->post('module_id');
						$this->dbfun_model->del_tables('ccs_id = '.$this->zone_id.' and role_id = '.$id.'','ccs_mtr');
						
						if(strpos($module,',') !== false){//cek if the value from post is array
							$ids = explode(',',$module);
							
							foreach($ids as $module_id){
								$custom_primarys = array(
									'role_id' => $id,
									'status' => 1,
									'datecreated' => date('Y-m-d H:i:s'),
									'ccs_id' => $this->zone_id,
									'module_id' => $module_id,
								);
								$this->new_data('ccs_mtr',$custom_primarys, '','', '','');
								$cekmodule = count($this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module_id.' ','mdl'));
								if($cekmodule != 0){
									$child = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module_id.' and status = 1','mdl');
									foreach($child as $c){
										$custom_child = array(
											'role_id' => $id,
											'status' => 1,
											'datecreated' => date('Y-m-d H:i:s'),
											'ccs_id' => $this->zone_id,
											'module_id' => $c->module_id,
										);
										$this->new_data('ccs_mtr',$custom_child, '','', '','');
									}
								}
							}
							
						}else{
							$custom_primarys = array(
								'role_id' => $id,
								'status' => 1,
								'datecreated' => date('Y-m-d H:i:s'),
								'ccs_id' => $this->zone_id,
								'module_id' => $module,
							);
							$this->new_data('ccs_mtr',$custom_primarys, '','', '','');
							$cekmodule = count($this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module.' ','mdl'));
							if($cekmodule != 0){
								$child = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module.' and status = 1','mdl');
								foreach($child as $c){
									$custom_child = array(
										'role_id' => $id,
										'status' => 1,
										'datecreated' => date('Y-m-d H:i:s'),
										'ccs_id' => $this->zone_id,
										'module_id' => $c->module_id,
									);
									$this->new_data('ccs_mtr',$custom_child, '','', '','');
								}
							}
						}
						
						$datas = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'type_id'=>$id), 'ccs_mdr','','','','');
						$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'type_id' => $id),$datas,'mdr');
						$datas2 = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'ccs_key' => $this->input->post('ccs_key'), 'category_id'=>$id), 'ccs_oct','','','','');
						$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'ccs_key' => $this->input->post('ccs_key'),'category_id' => $id),$datas2,'oct');
						$status = 'success';
						$message = 'Data updated successfully';
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				else{
					$ccs_key = $this->input->post('ccs_key');
					$parent = $this->input->post('parent_id');
					switch($key){;
						case 'admin':
							if($this->form_validation->run('ccs_oct') === TRUE){
								$title = trim($this->input->post('title'));
								$cekcat = count($this->dbfun_model->get_all_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$title.'"','oct'));
								if($cekcat == 1){														
									$status = 'error';
									$message = 'Category title '.strtolower($title).' has already exist on the systems. Cheers';
								}else{	
									$mct = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' order by category_id DESC','oct')->category_id;
									if($mct == 2){
										$maxcat = 3;
									}else{
										$maxcat = $mct;
									}
									$name = strtolower(str_replace(' ','',$title));
									$cekrole = count($this->dbfun_model->get_all_data_bys('type_id','ccs_id = '.$this->zone_id.' and name = "'.$name.'"','mdr'));
									if($cekrole == 0){
										$role_id = $maxcat+1;
										
										$custom_admin = array(
											'title' => $title,
											'status' => 1,
											'language_id' => 2,
											'createdby' => $this->admin_id,
											'category_id' => $role_id,
											'parent_id' => $parent,
											'parent_title' => 'Admin',
											'datecreated' => date('Y-m-d H:i:s'),
											'ccs_id' => $this->zone_id
										);
										$this->new_data('ccs_oct',$custom_admin, '','', '','');
										
										$custom_primary = array(
											'name' => $name,
											'status' => 1,
											'createdby' => $this->admin_id,
											'datecreated' => date('Y-m-d H:i:s'),
											'type_id' => $role_id,
											'ccs_id' => $this->zone_id
										);
										$this->new_data('ccs_mdr',$custom_primary, '','', '','');
										
										$module = $this->input->post('module_id');
										
										if(strpos($module,',') !== false){//cek if the value from post is array
											$ids = explode(',',$module);
											
											foreach($ids as $module_id){
												$custom_primarys = array(
													'role_id' => $role_id,
													'status' => 1,
													'datecreated' => date('Y-m-d H:i:s'),
													'ccs_id' => $this->zone_id,
													'module_id' => $module_id,
												);
												$this->new_data('ccs_mtr',$custom_primarys, '','', '','');
												$cekmodule = count($this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module_id.' ','mdl'));
												if($cekmodule != 0){
													$child = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module_id.' and status = 1','mdl');
													foreach($child as $c){
														$custom_child = array(
															'role_id' => $role_id,
															'status' => 1,
															'datecreated' => date('Y-m-d H:i:s'),
															'ccs_id' => $this->zone_id,
															'module_id' => $c->module_id,
														);
														$this->new_data('ccs_mtr',$custom_child, '','', '','');
													}
												}
											}
											
										}else{
											$custom_primarys = array(
												'role_id' => $role_id,
												'status' => 1,
												'datecreated' => date('Y-m-d H:i:s'),
												'ccs_id' => $this->zone_id,
												'module_id' => $module,
											);
											$this->new_data('ccs_mtr',$custom_primarys, '','', '','');
											$cekmodule = count($this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module.' ','mdl'));
											if($cekmodule != 0){
												$child = $this->dbfun_model->get_all_data_bys('module_id','ccs_id = '.$this->zone_id.' and parent_id = '.$module.' and status = 1','mdl');
												foreach($child as $c){
													$custom_child = array(
														'role_id' => $role_id,
														'status' => 1,
														'datecreated' => date('Y-m-d H:i:s'),
														'ccs_id' => $this->zone_id,
														'module_id' => $c->module_id,
													);
													$this->new_data('ccs_mtr',$custom_child, '','', '','');
												}
											}
										}
										 
										$status = 'success';
										$message = 'Data created successfully'; 
									}
									else{
										$status = 'error';
										$message = 'Role name '.strtolower($name).' has already exist on the systems. Cheers';
									}
								}
							}else{
								$status = 'error';
								$message = validation_errors();
							}							
							break;
						case 'member':								
							if($this->form_validation->run('ccs_oct') === TRUE){
								$title = trim($this->input->post('title'));
								$cekcat = count($this->dbfun_model->get_all_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$title.'"','oct'));
								if($cekcat == 1){														
									$status = 'error';
									$message = 'Category title '.strtolower($title).' has already exist on the systems. Cheers';
								}else{	
									$mct = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' order by category_id DESC','oct')->category_id;
									if($mct == 2){
										$maxcat = 3;
									}else{
										$maxcat = $mct+1;
									}										
									$custom_member = array(
										'title' => $title,
										'status' => 1,
										'language_id' => 2,
										'createdby' => $this->admin_id,
										'category_id' => $maxcat,
										'parent_id' => $parent,
										'parent_title' => 'Member',
										'datecreated' => date('Y-m-d H:i:s'),
										'ccs_id' => $this->zone_id
									);
									$this->new_data('ccs_oct',$custom_member, '','', '','');
									$status = 'success';
									$message = 'Data has been successfully created. Thankyou';
								}
							}else{
								$status = 'error';
								$message = validation_errors();
							}	
							break;
						case 'reseller':
							if($this->form_validation->run('ccs_oct') === TRUE){
								$title = trim($this->input->post('title'));
								$cekcat = count($this->dbfun_model->get_all_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and title = "'.$title.'"','oct'));
								if($cekcat == 1){														
									$status = 'error';
									$message = 'Category title '.strtolower($title).' has already exist on the systems. Cheers';
								}else{	
									$mct = $this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' order by category_id DESC','oct')->category_id;
									if($mct == 2){
										$maxcat = 3;
									}else{
										$maxcat = $mct+1;
									}
									$custom_reseller = array(
										'title' => $title,
										'status' => $status,
										'language_id' => 2,
										'createdby' => $this->admin_id,
										'category_id' => $maxcat,
										'parent_id' => $parent,
										'parent_title' => 'Reseller',
										'datecreated' => date('Y-m-d H:i:s'),
										'ccs_id' => $this->zone_id
									);
									if(isset($_FILES['image_square'])){
										$this->new_data('ccs_oct',$custom_reseller, 'image_square','image_square', $this->zone.'/category','jpeg|jpg|png|mp4');
									}else{
										$this->new_data('ccs_oct',$custom_reseller, '','','','');
									}
									$status = 'success';
									$message = 'Data has been successfully created. Thankyou';
									$status = 'success';
									$message = 'Data has been successfully created. Thankyou';
								}
							}else{
								$status = 'error';
								$message = validation_errors();
							}	
													
							break;
					}					
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) && ($param == 'profile')){
				$admin_id = $this->admin_id;
				$id = $this->input->post('admin_id');
				if($id){
					unset($cek);
					$this->form_validation->set_rules('admin_id', 'User ID', 'trim|required|numeric|xss_clean');
					$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean');
					if ($this->form_validation->run() === TRUE){
						$cek = $this->dbfun_model->get_data_bys('*','ccs_id = '.$this->zone_id.' and admin_id = '.$id.'','adm');
						if($cek){
							foreach($_POST as $key => $val)  
							{
								$data1[$key] = $this->input->post($key); 
							}
							if(isset($_FILES['main_image'])){
								$data1['main_image'] = true; 
							}
							$data2 = get_object_vars($cek);
							$data_profile = array_intersect_key(array_diff_assoc($data1, $data2),$data2);
							if($data_profile){
								if(isset($_FILES['main_image'])){
									if (!is_dir('assets/'.$this->zone.'/admin'))
										{
											mkdir('./assets/'.$this->zone.'/admin', 0777, true);
										}
									if (!is_dir('assets/'.$this->zone.'/admin/'.$id))
										{
											mkdir('./assets/'.$this->zone.'/admin/'.$id, 0777, true);
										}
									$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'admin_id'=>$id), 'ccs_adm','main_image',$this->zone.'/admin/'.$id,'main_image','jpeg|jpg|png');
								}else{
									$update = $this->update_data_bys('*', array('ccs_id' => $this->zone_id, 'admin_id'=>$id), 'ccs_adm','','','','');
								}
								$this->dbfun_model->update_table(array('ccs_id' => $this->zone_id, 'admin_id' => $id),$update,'adm');
								$new_data = $this->dbfun_model->get_data_bys('name,main_image','ccs_id = '.$this->zone_id.' and admin_id = '.$id.'','adm');
								$foto = 'assets/'.$zone.'/admin/'.$id.'/'.$new_data->main_image;
								$session = array(
									strtolower($zone).'_nama' => $new_data->name,
									strtolower($zone).'_foto' => $foto
								);
								$this->session->set_userdata($session);
								$status = 'success';
								$message = 'Data updated successfully';
							}else{
								$status = 'error';
								$message = 'No Data updated';
							}
						}else{
							$status = 'error';
							$message = 'No Data Updated';
						}
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}else{
					if ($this->form_validation->run('ccs_mdr') === TRUE){
						$ceks = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and LOWER(name) like "'.strtolower($this->input->post('name')).'"','mdr');
						if(empty($ceks)){
							$ceks2 = $this->dbfun_model->get_max_data_where('type_id','ccs_id = '.$this->zone_id.' order by datecreated desc','mdr');
							$custom_primary = array(
								'status' => 1,
								'createdby' => $this->admin_id,
								'type_id' => $ceks2->type_id+1,
								'ccs_id' => $this->zone_id
							);
							$this->new_data('ccs_mdr',$custom_primary, '','', '','');
							$role_id = $ceks2->type_id+1;
							$module = $this->input->post('module_id');
							if(strpos($module,',') !== false){//cek if the value from post is array
								$ids = explode(',',$module);
								
								foreach($ids as $module_id){
									$custom_primarys = array(
										'role_id' => $role_id,
										'status' => 1,
										'ccs_id' => $this->zone_id,
										'module_id' => $module_id,
									);
									$this->new_data('ccs_mtr',$custom_primarys, '','', '','');
								}
								
							}else{
								$custom_primarys = array(
									'role_id' => $role_id,
									'status' => 1,
									'ccs_id' => $this->zone_id,
									'module_id' => $module,
								);
								$this->new_data('ccs_mtr',$custom_primarys, '','', '','');
							}
							$status = 'success';
							$message = 'Data created successfully';
						}else{
							$status = 'error';
							$message = 'Role named '.$ceks->name.' has beed created';
						}
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) && ($param == 'trx')){					
				$id = $this->input->post('trx_code');
				$ccs_key = $this->input->post('ccs_key');
				$lang = $this->input->post('language_id');
				$cat = $this->input->post('category_id');
				$admin_id = $this->admin_id;
				$keys = $this->input->post('param2');
				
				if($id){
					$edit = $this->input->post('edit');
					switch($keys){
						case 'single':
							switch($edit){
								case 'info':	
									$info_id = $this->input->post('info_id');
									$email = trim($this->input->post('email'));
									$phone = trim($this->input->post('phone'));
									$mobile = trim($this->input->post('mobile'));
									$name = trim($this->input->post('name'));
									$address = trim($this->input->post('address'));
									$address2 = trim($this->input->post('shipping'));
									$cekaddres = count($this->dbfun_model->get_data_bys('info_id','info_id = '.$info_id.' and ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and email = "'.$email.'" and mobile = "'.$mobile.'" and phone = "'.$phone.'" and address = "'.$address.'" and address_2 = "'.$address2.'"','tri'));
									if($cekaddres == 1){											
										$status = 'error';
										$message = 'No data updated';
									}else{
										$update_tri = array(
											'dateupdated' => date('Y-m-d H:i:s'),
											'updatedby' => $this->admin_id,
											'email' => $email,
											'phone' => $phone,
											'mobile' => $mobile,
											'name' => $name,
											'address' => $address,
											'address_2' => $address2
										);	

										$this->dbfun_model->update_table(array('info_id' => $info_id), $update_tri, 'tri');											
										$status = 'success';
										$message = 'Data has been successfully updated. Thankyou';											
									}
									break;
							}
							break;
					}
				}
				else{						
					if ($this->form_validation->run('ccs_trx') === TRUE){
						$user = trim($this->input->post('user_id'));
						$useri = trim($this->input->post('info_id'));
						$name = trim($this->input->post('name'));
						$desc = trim($this->input->post('desc'));
						$product = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$this->zone_id.' and link = "product" ','mdl')->module_id;
						$info = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and category_id = '.$cat.' and ccs_key = '.$ccs_key.'','oct')->title;
						$code = $this->dbfun_model->get_data_bys('rule_1','ccs_id = '.$this->zone_id.' and title = "'.$info.'"','ocs')->rule_1;
						switch($keys){
							case 'single':									
								if(($user != '') || ($name != '')){
									$segretissimo = trim($this->input->post('ticket'));
									$date = $this->input->post('datecreated');								
									$var = $this->input->post('var');
									$oid = $this->input->post('object_id');
									$cat_id = $this->input->post('cat_id');
									$total = $this->input->post('total');
									$p = $this->input->post('price');
									$price = str_replace('.','',$p);
									if($name != ''){
										$uname = trim($this->input->post('name'));
										$mobile = trim($this->input->post('mobile'));
										$utype = 0;
										$user = 0;
										$phone = trim($this->input->post('phone'));
										$email = trim($this->input->post('email'));
										$address = trim($this->input->post('address'));
										$sia = $this->input->post('sia');
										if($sia == 0){
											$shipping = $address;
										}else{
											$shipping = trim($this->input->post('shipping'));
										}											
									}
									else{
										if($user != 0){												
											$userinfo = $this->dbfun_model->get_data_bys('name, address, email, phone, mobile, type_id','ccs_id = '.$this->zone_id.' and user_id = '.$user.'','usr');
											$user = $user;
											$utype = $userinfo->type_id;
										}else{												
											$userinfo = $this->dbfun_model->get_data_bys('name, address, email, phone, mobile','ccs_id = '.$this->zone_id.' and info_id = '.$useri.'','tri');
											$user = $useri;
											$utype = 0;
										}
										
										$uname = $userinfo->name;
										$mobile = $userinfo->mobile;
										$phone = $userinfo->phone;
										$email = $userinfo->email;
										$address = $userinfo->address;
										$sia = $this->input->post('sia');
										if($sia == 0){
											$shipping = $address;
										}else{
											$shipping = trim($this->input->post('shipping'));
										}		
									}
									if($date != ''){
										$sdate = $date.date(' H:i:s');
									}else{
										$sdate = date('Y-m-d H:i:s');
									}
									if($var == 0){
										$varian = 0;									
										$s = $this->dbfun_model->get_data_bys('stock, sold','ccs_id = '.$this->zone_id.' and ccs_key = '.$product.' and object_id = '.$oid.' and category_id = '.$cat_id.' and vstatus = 0','prc');
										$stock = $s->stock;
										$sold = $s->sold;
									}else{
										$varian = $var;					
										$s = $this->dbfun_model->get_data_bys('stock, sold','ccs_id = '.$this->zone_id.' and ccs_key = '.$product.' and object_id = '.$oid.' and category_id = '.$cat_id.' and varian_id = '.$varian.'','prv');
										$stock = $s->stock;
										$sold = $s->sold;
									}
									$stato = $this->input->post('status');										
									if($stock >= $total){
										$sts = $stato;
									}else{
										$sts = 0;
									}
									switch($segretissimo == ''){
										case TRUE:										
											$custom_trx = array(
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'category_id' => $cat,
												'price' => $price,
												'varian_id' => $varian,
												'datecreated' => date('Y-m-d H:i:s'), 
												'datesales' => $sdate,
												'createdby' => $this->admin_id,
												'trx_code' => date("ymdHis", strtotime($sdate)),
												'code' => $code,
												'status' => $sts 
											);
											$custom_tri = array(
												'ccs_id' => $this->zone_id,
												'ccs_key' => $ccs_key,
												'type_id'=> $utype,
												'user_id'=> $user,
												'name'=> $uname,
												'phone'=> $phone,
												'mobile'=> $mobile,
												'email'=> $email,
												'address'=> $address,
												'address_2'=> $shipping,
												'description'=> $desc,
												'datecreated' => date('Y-m-d H:i:s'), 
												'createdby' => $this->admin_id,
												'status' => 1 
											);
											if($stock == 0){											
												$status = 'error';
												$message = 'Sorry, stock is empty. Please check the detail product. Cheers';
											}else{
												switch($stock >= $total){
													case TRUE:	
														$value = $this->new_data('ccs_trx',$custom_trx, '','', '','');
														if($user != 0){
															$usercek = count($this->dbfun_model->get_all_data_bys('info_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$user.'','tri'));
														}else{
															$usercek = 0;
														}
														
														if(($usercek == 0) || ($name != '')){																
															$tri = $this->new_data('ccs_tri',$custom_tri, '','', '','');
														}
														$trx = $this->dbfun_model->get_data_bys('trx_code, code','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_id = '.$value.'','trx');
														$custom_inv = array(
															'ccs_id' => $this->zone_id,
															'ccs_key' => $ccs_key,
															'type' => 0,
															'code' => $trx->code, 
															'icode' => 'INV', 
															'inv_code' => date("ymdHis"),
															'trx_code' => $trx->trx_code, 
															'description' => 'No description for this invoice',
															'dateinv' => date('Y-m-d H:i:s'),
															'createdby' => $this->admin_id,
															'datecreated' => date('Y-m-d H:i:s'),
															'status' => $sts
														);
														switch($sts == 0){
															case TRUE:																		
																$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');	
																if($stock > $total){															
																	$status = 'success';
																	$message = 'Sales has been successfully created. Thank you';
																}else{
																	$status = 'success';
																	$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';															
																}
																break;
															case FALSE:
																$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');	
																$srt = $this->dbfun_model->get_data_bys('icode, inv_code','ccs_id = '.$this->zone_id.' and inv_id = '.$inv.'','inv');
																$custom_srt = array(
																	'ccs_id' => $this->zone_id,
																	'ccs_key' => $ccs_key,
																	'type' => 1,
																	'code' => $srt->icode, 
																	'icode' => 'SRT', 
																	'inv_code' => date("ymdHis"),
																	'trx_code' => $srt->inv_code, 
																	'description' => 'Thankyou for your transaction.',
																	'dateinv' => date('Y-m-d H:i:s'),
																	'datecreated' => date('Y-m-d H:i:s'),
																	'createdby' => $this->admin_id,
																	'status' => 1
																);
																$this->new_data('ccs_inv',$custom_srt, '','', '','');
																$update = array(
																	'stock' => $stock-$total,
																	'sold'=> $sold+$total,
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'updatedby' => $this->admin_id
																);
																if($varian == 0){															
																	$this->dbfun_model->update_table(array('object_id' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $product, 'category_id' => $cat_id ), $update, 'prc');
																}else{
																	$this->dbfun_model->update_table(array('object_id' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $product, 'category_id' => $cat_id, 'varian_id' => $varian ), $update, 'prv');
																}															
																if($stock > $total){															
																	$status = 'success';
																	$message = 'Sales has been successfully created. Thank you';
																}else{
																	$status = 'success';
																	$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';															
																}
																break;
														}
														break;
													case FALSE:
														$value = $this->new_data('ccs_trx',$custom_trx, '','', '','');															
														if($user != 0){
															$usercek = count($this->dbfun_model->get_all_data_bys('info_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$user.'','tri'));
														}else{
															$usercek = 0;
														}
														
														if(($usercek == 0) || ($name != '')){																
															$tri = $this->new_data('ccs_tri',$custom_tri, '','', '','');
														}
														$trx = $this->dbfun_model->get_data_bys('trx_code, code','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_id = '.$value.'','trx');
														$custom_inv = array(
															'ccs_id' => $this->zone_id,
															'ccs_key' => $ccs_key,
															'type' => 0,
															'code' => $trx->code, 
															'icode' => 'INV', 
															'inv_code' => date("ymdHis"),
															'trx_code' => $trx->trx_code, 
															'description' => 'No description for this invoice',
															'dateinv' => date('Y-m-d H:i:s'),
															'createdby' => $this->admin_id,
															'datecreated' => date('Y-m-d H:i:s'),
															'status' => $sts
														);																											
														$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');	
														$status = 'success';
														$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';
														break;
												}
											}
											break;
										case FALSE:
											$chiave = $this->dbfun_model->get_data_bys('module_id','ccs_id = '.$this->zone_id.' and link = "promo"','mdl')->module_id;
											$cek = count($this->dbfun_model->get_data_bys('ticket_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$chiave.' and code = "'.$segretissimo.'" and cat_id = '.$cat_id.' and oid = '.$oid.' and varian_id = '.$varian.'','tkt'));
											switch($cek == 1){
												case TRUE:
													$d = $this->dbfun_model->get_data_bys('discount, stock, ticket_id, sold, counter','ccs_id = '.$this->zone_id.' and ccs_key = '.$chiave.' and code = "'.$segretissimo.'" and cat_id = '.$cat_id.' and oid = '.$oid.' and varian_id = '.$varian.'','tkt');
													$discount = $d->discount;
													$tstock = $d->stock;
													$tsold = $d->sold;
													$counter = $d->counter;
													if($tstock >= $total){
														$dsc =  $price - ($price*$discount)/100;
													}else{													
														$dsc = $price;
													}
													$custom_trx = array(
														'ccs_id' => $this->zone_id,
														'ccs_key' => $ccs_key,
														'ticket_id' => $d->ticket_id,
														'category_id' => $cat,
														'price' => $dsc,
														'varian_id' => $varian,
														'datecreated' => date('Y-m-d H:i:s'), 
														'datesales' => $sdate,
														'createdby' => $this->admin_id,
														'trx_code' => date("ymdHis", strtotime($sdate)),
														'code' => $code,
														'status' => $sts 
													);
													$custom_tri = array(
														'ccs_id' => $this->zone_id,
														'ccs_key' => $ccs_key,
														'type_id'=> $utype,
														'user_id'=> $user,
														'name'=> $uname,
														'phone'=> $phone,
														'mobile'=> $mobile,
														'email'=> $email,
														'address'=> $address,
														'address_2'=> $shipping,
														'description'=> $desc,
														'datecreated' => date('Y-m-d H:i:s'), 
														'createdby' => $this->admin_id,
														'status' => 1 
													);
													if($tstock == 0){
														$update_tkt = array(
															'counter'=> $counter+1,
															'dateupdated' => date('Y-m-d H:i:s'),
															'updatedby' => $this->admin_id
														);
														$this->dbfun_model->update_table(array('oid' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $chiave, 'cat_id' => $cat_id , 'ticket_id' => $d->ticket_id, 'url_id' => $product, 'varian_id' => $varian), $update_tkt, 'tkt');
														$status = 'error';
														$message = 'Sorry, discount stock is empty. Please check the detail promo. Cheers';
													
													}else{
														switch($tstock >= $total){
															case TRUE:
																$value = $this->new_data('ccs_trx',$custom_trx, '','', '','');
																if($user != 0){
																	$usercek = count($this->dbfun_model->get_all_data_bys('info_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$user.'','tri'));
																}else{
																	$usercek = 0;
																}
																
																if(($usercek == 0) || ($name != '')){																
																	$tri = $this->new_data('ccs_tri',$custom_tri, '','', '','');
																}												
																$trx = $this->dbfun_model->get_data_bys('trx_code, code','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_id = '.$value.'','trx');
																$custom_inv = array(
																	'ccs_id' => $this->zone_id,
																	'ccs_key' => $ccs_key,
																	'type' => 0,
																	'code' => $trx->code, 
																	'icode' => 'INV', 
																	'inv_code' => date("ymdHis"),
																	'trx_code' => $trx->trx_code, 
																	'description' => 'No description for this invoice',
																	'dateinv' => date('Y-m-d H:i:s'),
																	'createdby' => $this->admin_id,
																	'datecreated' => date('Y-m-d H:i:s'),
																	'status' => $sts
																);
																switch($sts == 0){
																	case TRUE:
																		$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');											
																		$update_tkt = array(
																			'counter'=> $counter+1,
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('oid' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $chiave, 'cat_id' => $cat_id , 'ticket_id' => $d->ticket_id, 'url_id' => $product), $update_tkt, 'tkt');
																		if($stock >= $total){																			
																			$status = 'success';
																				$message = 'Sales has been successfully created. Thank you';
																			}else{
																				$status = 'success';
																				$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';															
																			}
																		break;
																	case FALSE:
																		$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');	
																		$srt = $this->dbfun_model->get_data_bys('icode, inv_code','ccs_id = '.$this->zone_id.' and inv_id = '.$inv.'','inv');
																		$custom_srt = array(
																			'ccs_id' => $this->zone_id,
																			'ccs_key' => $ccs_key,
																			'type' => 1,
																			'code' => $srt->icode, 
																			'icode' => 'SRT', 
																			'inv_code' => date("ymdHis"),
																			'trx_code' => $srt->inv_code, 
																			'description' => 'Thankyou for your transaction.',
																			'dateinv' => date('Y-m-d H:i:s'),
																			'datecreated' => date('Y-m-d H:i:s'),
																			'createdby' => $this->admin_id,
																			'status' => 1
																		);
																		$this->new_data('ccs_inv',$custom_srt, '','', '','');	
																		if($tstock >= $total){
																			$update_tkt = array(
																				'stock' => $tstock-$total,
																				'sold'=> $tsold+$total,
																				'counter'=> $counter+1,
																				'dateupdated' => date('Y-m-d H:i:s'),
																				'updatedby' => $this->admin_id
																			);
																			$this->dbfun_model->update_table(array('oid' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $chiave, 'cat_id' => $cat_id , 'ticket_id' => $d->ticket_id, 'url_id' => $product, 'varian_id' => $varian), $update_tkt, 'tkt');
																		}
																		else{
																			$update_tkt = array(
																				'counter'=> $counter+1,
																				'dateupdated' => date('Y-m-d H:i:s'),
																				'updatedby' => $this->admin_id
																			);
																			$this->dbfun_model->update_table(array('oid' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $chiave, 'cat_id' => $cat_id , 'ticket_id' => $d->ticket_id, 'url_id' => $product, 'varian_id' => $varian), $update_tkt, 'tkt');
																		}
																		if($stock >= $total){																	
																			$update = array(
																				'stock' => $stock-$total,
																				'sold'=> $sold+$total,
																				'dateupdated' => date('Y-m-d H:i:s'),
																				'updatedby' => $this->admin_id
																			);															
																			if($varian == 0){															
																				$this->dbfun_model->update_table(array('object_id' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $product, 'category_id' => $cat_id ), $update, 'prc');
																			}else{
																				$this->dbfun_model->update_table(array('object_id' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $product, 'category_id' => $cat_id, 'varian_id' => $varian ), $update, 'prv');
																			}
																			$status = 'success';
																			$message = 'Sales has been successfully created. Thank you';
																		}else{
																			$status = 'success';
																			$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';															
																		}
																		break;
																}														
																break;
															case FALSE:																		
																switch($stock >= $total){
																	case TRUE:	
																		$value = $this->new_data('ccs_trx',$custom_trx, '','', '','');
																		if($user != 0){
																			$usercek = count($this->dbfun_model->get_all_data_bys('info_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$user.'','tri'));
																		}else{
																			$usercek = 0;
																		}
																		
																		if(($usercek == 0) || ($name != '')){																
																			$tri = $this->new_data('ccs_tri',$custom_tri, '','', '','');
																		}		
																		$trx = $this->dbfun_model->get_data_bys('trx_code, code','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_id = '.$value.'','trx');
																		$custom_inv = array(
																			'ccs_id' => $this->zone_id,
																			'ccs_key' => $ccs_key,
																			'type' => 0,
																			'code' => $trx->code, 
																			'icode' => 'INV', 
																			'inv_code' => date("ymdHis"),
																			'trx_code' => $trx->trx_code, 
																			'description' => 'No description for this invoice',
																			'dateinv' => date('Y-m-d H:i:s'),
																			'createdby' => $this->admin_id,
																			'datecreated' => date('Y-m-d H:i:s'),
																			'status' => $sts
																		);
																		$update_tkt = array(
																			'counter'=> $counter+1,
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('oid' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $chiave, 'cat_id' => $cat_id , 'ticket_id' => $d->ticket_id, 'url_id' => $product, 'varian_id' => $varian), $update_tkt, 'tkt');
																																		
																		switch($sts == 0){
																			case TRUE:																		
																				$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');	
																				if($stock >= $total){										
																					$status = 'success';
																					$message = 'Sales has been successfully created. Thank you';
																				}else{
																					$status = 'success';
																					$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';															
																				}
																				break;
																			case FALSE:
																				$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');	
																				$srt = $this->dbfun_model->get_data_bys('icode, inv_code','ccs_id = '.$this->zone_id.' and inv_id = '.$inv.'','inv');
																				$custom_srt = array(
																					'ccs_id' => $this->zone_id,
																					'ccs_key' => $ccs_key,
																					'type' => 1,
																					'code' => $srt->icode, 
																					'icode' => 'SRT', 
																					'inv_code' => date("ymdHis"),
																					'trx_code' => $srt->inv_code, 
																					'description' => 'Thankyou for your transaction.',
																					'dateinv' => date('Y-m-d H:i:s'),
																					'datecreated' => date('Y-m-d H:i:s'),
																					'createdby' => $this->admin_id,
																					'status' => 1
																				);
																				$this->new_data('ccs_inv',$custom_srt, '','', '','');
																				
																				
																				if($stock >= $total){															
																					$update = array(
																						'stock' => $stock-$total,
																						'sold'=> $sold+$total,
																						'dateupdated' => date('Y-m-d H:i:s'),
																						'updatedby' => $this->admin_id
																					);
																					if($varian == 0){															
																						$this->dbfun_model->update_table(array('object_id' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $product, 'category_id' => $cat_id ), $update, 'prc');
																					}else{
																						$this->dbfun_model->update_table(array('object_id' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $product, 'category_id' => $cat_id, 'varian_id' => $varian ), $update, 'prv');
																					}
																					$status = 'success';
																					$message = 'Sales has been successfully created. Thank you';
																				}else{
																					$status = 'success';
																					$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';															
																				}
																				break;
																		}
																		break;
																	case FALSE:
																		$value = $this->new_data('ccs_trx',$custom_trx, '','', '','');
																		if($user != 0){
																			$usercek = count($this->dbfun_model->get_all_data_bys('info_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and user_id = '.$user.'','tri'));
																		}else{
																			$usercek = 0;
																		}
																		
																		if(($usercek == 0) || ($name != '')){																
																			$tri = $this->new_data('ccs_tri',$custom_tri, '','', '','');
																		}		
																		$trx = $this->dbfun_model->get_data_bys('trx_code, code','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and trx_id = '.$value.'','trx');
																		$custom_inv = array(
																			'ccs_id' => $this->zone_id,
																			'ccs_key' => $ccs_key,
																			'type' => 0,
																			'code' => $trx->code, 
																			'icode' => 'INV', 
																			'inv_code' => date("ymdHis"),
																			'trx_code' => $trx->trx_code, 
																			'description' => 'No description for this invoice',
																			'dateinv' => date('Y-m-d H:i:s'),
																			'createdby' => $this->admin_id,
																			'datecreated' => date('Y-m-d H:i:s'),
																			'status' => $sts
																		);	
																		
																		$inv = $this->new_data('ccs_inv',$custom_inv, '','', '','');	
																		$update_tkt = array(
																			'counter'=> $counter+1,
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'updatedby' => $this->admin_id
																		);
																		$this->dbfun_model->update_table(array('oid' => $oid, 'ccs_id' => $this->zone_id, 'ccs_key' => $chiave, 'cat_id' => $cat_id , 'ticket_id' => $d->ticket_id, 'url_id' => $product, 'varian_id' => $varian), $update_tkt, 'tkt');
																		
																		$status = 'success';
																		$message = 'Sales has been successfully created, but you should check the stock of product you have been sold. Thank you';
																		break;
																}
																break;
														}
													}
													break;
												case FALSE:
													$status = 'error';
													$message = 'Sorry, invalid code, please use another code. Cheers';
													break;
											}
											break;
									}
								}else{
									$status = 'error';
									$message = 'Please input customer info on '.strtolower($info).'. Cheers';
								}						
								break;
							case 'multiple':
								break;
						}
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}		
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type)&&($param == 'user')){
				$id = $this->input->post('role');
				$ccs_key = $this->input->post('ccs_key');
				$lang = $this->input->post('language_id');
				$admin_id = $this->admin_id;
				$key =  $this->input->post('key');
				$keys = $this->input->post('param2');
				if($id){
					$cat =  $this->input->post('type_id');
					$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|max_length[25]');
					$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean|max_length[50]');
					$this->form_validation->set_rules('email', 'Email Account', 'trim|required|xss_clean|max_length[50]|valid_email');
					$this->form_validation->set_rules('phone', 'Fix Phone', 'trim|xss_clean|max_length[15]');
					$this->form_validation->set_rules('gender', 'Gender', 'numeric|required|xss_clean|max_length[1]');
					$this->form_validation->set_rules('description', 'Short Description', 'trim|xss_clean|max_length[250]');
					$this->form_validation->set_rules('status', 'Activation Status', 'numeric|required|xss_clean|max_length[1]');
					$this->form_validation->set_rules('pass', 'Password', 'trim|xss_clean|max_length[50]');
					if($key == 'verifiedmember'){							
						$this->form_validation->set_rules('reg_id', 'Member ID', 'trim|required|xss_clean|max_length[15]');
						$this->form_validation->set_rules('datestart', 'Start Date', 'trim|required|xss_clean|max_length[10]');
						$this->form_validation->set_rules('dateend', 'Valid Date', 'trim|required|xss_clean|max_length[10]');
						$this->form_validation->set_rules('zip', 'Postal Code', 'numeric|xss_clean|max_length[5]');
						$this->form_validation->set_rules('birthday', 'Birth of Day', 'trim|required|xss_clean|max_length[10]');
						$this->form_validation->set_rules('mobile', 'Mobile Phone', 'trim|required|xss_clean|max_length[15]');
						$this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean|max_length[250]');
					}elseif($key == 'registeredmember'){		
						$cekcat = count($this->dbfun_model->get_data_bys('category_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and category_id = '.$cat.' and title = "Registered Member"','oct'));
						if($cekcat == 0){								
							$this->form_validation->set_rules('reg_id', 'Member ID', 'trim|required|xss_clean|max_length[15]');
							$this->form_validation->set_rules('datestart', 'Start Date', 'trim|required|xss_clean|max_length[10]');
							$this->form_validation->set_rules('dateend', 'Valid Date', 'trim|required|xss_clean|max_length[10]');
							$this->form_validation->set_rules('zip', 'Postal Code', 'numeric|required|xss_clean|max_length[5]');
							$this->form_validation->set_rules('birthday', 'Birth of Day', 'trim|required|xss_clean|max_length[10]');
							$this->form_validation->set_rules('mobile', 'Mobile Phone', 'trim|required|xss_clean|max_length[15]');
							$this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean|max_length[250]');
						}else{								
							$this->form_validation->set_rules('zip', 'Postal Code', 'numeric|xss_clean|max_length[5]');
							$this->form_validation->set_rules('birthday', 'Birth of Day', 'trim|xss_clean|max_length[10]');
							$this->form_validation->set_rules('mobile', 'Mobile Phone', 'trim|xss_clean|max_length[15]');
							$this->form_validation->set_rules('address', 'Address', 'trim|xss_clean|max_length[250]');
						}
					}else{							
						$this->form_validation->set_rules('birthday', 'Birth of Day', 'trim|required|xss_clean|max_length[10]');
						$this->form_validation->set_rules('mobile', 'Mobile Phone', 'trim|required|xss_clean|max_length[15]');
						$this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean|max_length[250]');
					}
					
					$bod = $this->input->post('birthday');
					if($bod == ''){
						$birthday = '0000-00-00';
					}else{
						$birthday = $bod;
					}
					if ($this->form_validation->run() === TRUE){						
						switch($keys){
							case 'admin':									
								$username = trim($this->input->post('username'));
								$email = trim($this->input->post('email'));
								$pwd = $this->input->post('pwd');
								$cekmember = $this->dbfun_model->get_data_bys('email, username, role_id','ccs_id = '.$this->zone_id.' and admin_id = '.$id.'','adm');
								$memberemail = $cekmember->email;
								$membername = $cekmember->username;
								$cekusername = count($this->dbfun_model->get_data_bys('username','ccs_id = '.$this->zone_id.' and role_id = '.$cekmember->role_id.' and username = "'.$username.'" and admin_id != '.$id.'','adm'));
								$cekemail = count($this->dbfun_model->get_data_bys('email','ccs_id = '.$this->zone_id.' and role_id = '.$cekmember->role_id.' and email = "'.$email.'" and admin_id != '.$id.'','adm'));
								switch($pwd){
									case '1':
										$pass = trim($this->input->post('pass'));
										$confirm = trim($this->input->post('confirm'));
										switch($pass == $confirm){
											case TRUE:
												switch($cekusername == 0){
													case TRUE:
														switch($cekemail == 0){
															case TRUE:
																$update_adm = array(
																	'username' => $username,
																	'email' => $email,
																	'role_id' => $this->input->post('role_id'),
																	'password' => md5($pass),
																	'gender' => $this->input->post('gender'),
																	'phone' => trim($this->input->post('phone')),
																	'mobile' => trim($this->input->post('mobile')),
																	'birthday' => date('Y-m-d',strtotime($birthday)),
																	'address' => trim($this->input->post('address')),											
																	'description' => trim($this->input->post('description')),
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'updatedby' => $this->admin_id,
																	'status' => $status
																);
																$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.';
																break;
															case FALSE:
																$update_adm = array(
																	'email' => $email,
																	'role_id' => $this->input->post('role_id'),
																	'password' => md5($pass),
																	'gender' => $this->input->post('gender'),
																	'phone' => trim($this->input->post('phone')),
																	'mobile' => trim($this->input->post('mobile')),
																	'birthday' => date('Y-m-d',strtotime($birthday)),
																	'address' => trim($this->input->post('address')),											
																	'description' => trim($this->input->post('description')),
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'updatedby' => $this->admin_id,
																	'status' => $status
																);
																$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
																
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.';
																break;
														}
														break;
													case FALSE:																	
														switch($cekemail == 0){
															case TRUE:
																$update_adm = array(
																	'email' => $email,
																	'role_id' => $this->input->post('role_id'),
																	'password' => md5($pass),
																	'gender' => $this->input->post('gender'),
																	'phone' => trim($this->input->post('phone')),
																	'mobile' => trim($this->input->post('mobile')),
																	'birthday' => date('Y-m-d',strtotime($birthday)),
																	'address' => trim($this->input->post('address')),											
																	'description' => trim($this->input->post('description')),
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'updatedby' => $this->admin_id,
																	'status' => $status
																);
																$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');																					
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.';
																break;
															case FALSE:
																$update_adm = array(
																	'role_id' => $this->input->post('role_id'),
																	'password' => md5($pass),
																	'gender' => $this->input->post('gender'),
																	'phone' => trim($this->input->post('phone')),
																	'mobile' => trim($this->input->post('mobile')),
																	'birthday' => date('Y-m-d',strtotime($birthday)),
																	'address' => trim($this->input->post('address')),											
																	'description' => trim($this->input->post('description')),
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'updatedby' => $this->admin_id,
																	'status' => $status
																);
																$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
																
																$status = 'success';
																$message = 'Data has been successfully updated. Thankyou.';
																break;
														}
														break;
												}
												break;
											case FALSE:
												$status = 'error';
												$message = "Sorry, password doesn't macth. Cheers";
												break;
										}
										break;
									case '0':
										switch($cekusername == 0){
											case TRUE:
												switch($cekemail == 0){
													case TRUE:
														$update_adm = array(
															'username' => $username,
															'email' => $email,
															'role_id' => $this->input->post('role_id'),
															'gender' => $this->input->post('gender'),
															'phone' => trim($this->input->post('phone')),
															'mobile' => trim($this->input->post('mobile')),
															'birthday' => date('Y-m-d',strtotime($birthday)),
															'address' => trim($this->input->post('address')),											
															'description' => trim($this->input->post('description')),
															'dateupdated' => date('Y-m-d H:i:s'),
															'updatedby' => $this->admin_id,
															'status' => $status
														);
														$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
													case FALSE:
														$update_adm = array(
															'email' => $email,
															'role_id' => $this->input->post('role_id'),
															'gender' => $this->input->post('gender'),
															'phone' => trim($this->input->post('phone')),
															'mobile' => trim($this->input->post('mobile')),
															'birthday' => date('Y-m-d',strtotime($birthday)),
															'address' => trim($this->input->post('address')),											
															'description' => trim($this->input->post('description')),
															'dateupdated' => date('Y-m-d H:i:s'),
															'updatedby' => $this->admin_id,
															'status' => $status
														);
														$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
														
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
												}
												break;
											case FALSE:																	
												switch($cekemail == 0){
													case TRUE:
														$update_adm = array(
															'email' => $email,
															'role_id' => $this->input->post('role_id'),
															'gender' => $this->input->post('gender'),
															'phone' => trim($this->input->post('phone')),
															'mobile' => trim($this->input->post('mobile')),
															'birthday' => date('Y-m-d',strtotime($birthday)),
															'address' => trim($this->input->post('address')),											
															'description' => trim($this->input->post('description')),
															'dateupdated' => date('Y-m-d H:i:s'),
															'updatedby' => $this->admin_id,
															'status' => $status
														);
														$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');																					
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
													case FALSE:
														$update_adm = array(
															'role_id' => $this->input->post('role_id'),
															'gender' => $this->input->post('gender'),
															'phone' => trim($this->input->post('phone')),
															'mobile' => trim($this->input->post('mobile')),
															'birthday' => date('Y-m-d',strtotime($birthday)),
															'address' => trim($this->input->post('address')),											
															'description' => trim($this->input->post('description')),
															'dateupdated' => date('Y-m-d H:i:s'),
															'updatedby' => $this->admin_id,
															'status' => $status
														);
														$this->dbfun_model->update_table(array('admin_id' => $id, 'ccs_id' => $this->zone_id), $update_adm, 'adm');
														
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou.';
														break;
												}
												break;
										}
												
										break;
								}
								break;
							case 'member':
								switch($key){
									case 'verifiedmember':
										$memberid = trim($this->input->post('reg_id'));
										$username = trim($this->input->post('username'));
										$email = trim($this->input->post('email'));
										$pwd = $this->input->post('pwd');
										$cekmember = $this->dbfun_model->get_data_bys('reg_id, email, username, type_id','ccs_id = '.$this->zone_id.' and user_id = '.$id.'','usr');
										$member = $cekmember->reg_id;
										$memberemail = $cekmember->email;
										$membername = $cekmember->username;
										$cekusername = count($this->dbfun_model->get_data_bys('username','ccs_id = '.$this->zone_id.' and type_id = '.$cekmember->type_id.' and username = "'.$username.'" and user_id != '.$id.'','usr'));
										$cekemail = count($this->dbfun_model->get_data_bys('email','ccs_id = '.$this->zone_id.' and type_id = '.$cekmember->type_id.' and email = "'.$email.'" and user_id != '.$id.'','usr'));
										$cekmemberid = count($this->dbfun_model->get_data_bys('reg_id','ccs_id = '.$this->zone_id.' and type_id = '.$cekmember->type_id.' and reg_id = "'.$memberid.'" and user_id != '.$id.'','usr'));
										switch($pwd){
											case '1':
												$pass = trim($this->input->post('pass'));
												$confirm = trim($this->input->post('confirm'));
												switch($pass == $confirm){
													case TRUE:
														switch($cekmemberid == 0){
															case TRUE:
																switch($cekusername == 0){
																	case TRUE:
																		switch($cekemail == 0){
																			case TRUE:
																				$update_ver = array(
																					'reg_id' => $memberid,
																					'username' => $username,
																					'email' => $email,
																					'type_id' => 2,
																					'password' => md5($pass),
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																			case FALSE:
																				$update_ver = array(
																					'reg_id' => $memberid,
																					'username' => $username,
																					'type_id' => 2,
																					'password' => md5($pass),
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																				
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																		}
																		break;
																	case FALSE:																	
																		switch($cekemail == 0){
																			case TRUE:
																				$update_ver = array(
																					'reg_id' => $memberid,
																					'email' => $email,
																					'type_id' => 2,
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');																					
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																			case FALSE:
																				$update_ver = array(
																					'reg_id' => $memberid,
																					'type_id' => 2,
																					'password' => md5($pass),
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																				
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																		}
																		break;
																}
																break;
															case FALSE:
																switch($cekusername == 0){
																	case TRUE:
																		switch($cekemail == 0){
																			case TRUE:
																				$update_ver = array(
																					'username' => $username,
																					'email' => $email,
																					'type_id' => 2,
																					'password' => md5($pass),
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');																					
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																			case FALSE:
																				$update_ver = array(
																					'reg_id' => $memberid,
																					'username' => $username,
																					'type_id' => 2,
																					'password' => md5($pass),
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');																					
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																		}
																		break;
																	case FALSE:																	
																		switch($cekemail == 0){
																			case TRUE:
																				$update_ver = array(
																					'email' => $email,
																					'type_id' => 2,
																					'password' => md5($pass),
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																				
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																			case FALSE:
																				$update_ver = array(
																					'type_id' => 2,
																					'password' => md5($pass),
																					'zip' => $this->input->post('zip'),
																					'gender' => $this->input->post('gender'),
																					'phone' => trim($this->input->post('phone')),
																					'mobile' => trim($this->input->post('mobile')),
																					'birthday' => date('Y-m-d',strtotime($birthday)),
																					'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																					'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																					'address' => trim($this->input->post('address')),											
																					'description' => trim($this->input->post('description')),
																					'dateupdated' => date('Y-m-d H:i:s'),
																					'approvedby' => $this->admin_id,
																					'status' => $status
																				);
																				$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																				
																				$status = 'success';
																				$message = 'Data has been successfully updated. Thankyou.';
																				break;
																		}
																		break;
																}
																break;
														}															
														break;
													case FALSE:														
														$status = 'error';
														$message = "Sorry, password doesn't macth. Cheers";
														break;
												}
												break;
											case '0':	
												switch($cekmemberid == 0){
													case TRUE:
														switch($cekusername == 0){
															case TRUE:
																switch($cekemail == 0){
																	case TRUE:
																		$update_ver = array(
																			'reg_id' => $memberid,
																			'username' => $username,
																			'email' => $email,
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																	case FALSE:
																		$update_ver = array(
																			'reg_id' => $memberid,
																			'username' => $username,
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																		
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																}
																break;
															case FALSE:																	
																switch($cekemail == 0){
																	case TRUE:
																		$update_ver = array(
																			'reg_id' => $memberid,
																			'email' => $email,
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');																					
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																	case FALSE:
																		$update_ver = array(
																			'reg_id' => $memberid,
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																		
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																}
																break;
														}
														break;
													case FALSE:
														switch($cekusername == 0){
															case TRUE:
																switch($cekemail == 0){
																	case TRUE:
																		$update_ver = array(
																			'username' => $username,
																			'email' => $email,
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');																					
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																	case FALSE:
																		$update_ver = array(
																			'reg_id' => $memberid,
																			'username' => $username,
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');																					
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																}
																break;
															case FALSE:																	
																switch($cekemail == 0){
																	case TRUE:
																		$update_ver = array(
																			'email' => $email,
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																		
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																	case FALSE:
																		$update_ver = array(
																			'type_id' => 2,
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																			'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																			'address' => trim($this->input->post('address')),											
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																		
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou.';
																		break;
																}
																break;
														}
														break;
												}																	
												break;
										}
										break;
									case 'registeredmember':
										switch($cekcat){
											case '0':
												$memberid = $this->input->post('reg_id');
												$cekm = count($this->dbfun_model->get_all_data_bys('reg_id','ccs_id = '.$this->zone_id.' and reg_id = "'.$memberid.'" and type_id = 2','usr'));
												switch($cekm == 1){
													case TRUE:
														$status = 'error';
														$message = 'Data member ID for '.$memberid.' already exist ont the systems. Cheers';
														break;
													case FALSE:
														$pwd = $this->input->post('pwd');
														switch($pwd == 0){
															case TRUE:																	
																$update_ver = array(
																	'reg_id' => $memberid,
																	'type_id' => 2,
																	'zip' => $this->input->post('zip'),
																	'gender' => $this->input->post('gender'),
																	'phone' => trim($this->input->post('phone')),
																	'mobile' => trim($this->input->post('mobile')),
																	'birthday' => date('Y-m-d',strtotime($birthday)),
																	'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
																	'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
																	'address' => trim($this->input->post('address')),													
																	'description' => trim($this->input->post('description')),
																	'dateupdated' => date('Y-m-d H:i:s'),
																	'approvedby' => $this->admin_id,
																	'status' => $status
																);
																$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																$status = 'success';
																$message = 'Data has been successfully updated to become verified member. Thankyou';
																break;
															case FALSE:
															switch($pass == ''){
																case TRUE:
																	$status = 'error';
																	$message = 'Sorry, you have not reset the password. cheers';
																	break;
																case FALSE:
																	switch($pass == $confirm){
																		case TRUE:
																			$update_ver = array(
																				'type_id' => 2,
																				'reg_id' => $memberid,
																				'password' => md5($pass),
																				'zip' => $this->input->post('zip'),
																				'gender' => $this->input->post('gender'),
																				'phone' => trim($this->input->post('phone')),
																				'mobile' => trim($this->input->post('mobile')),
																				'datestart' => '0000-00-00 00:00:00',
																				'dateend' => '0000-00-00 00:00:00',
																				'birthday' => date('Y-m-d',strtotime($birthday)),
																				'address' => trim($this->input->post('address')),												
																				'description' => trim($this->input->post('description')),
																				'dateupdated' => date('Y-m-d H:i:s'),
																				'approvedby' => $this->admin_id,
																				'status' => $status
																			);
																			$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																			$status = 'success';
																			$message = "Password and other data have been successfully updated";
																			break;
																		case FALSE:
																			$status = 'error';
																			$message = "Sorry, password doesn't match. Cheers";
																			break;
																	}
																	break;
															}
															break;																
														}
														break;
												}
												break;
											case '1':
												$pwd = $this->input->post('pwd');
												switch($pwd == 0){
													case TRUE:
														$update_ver = array(
															'type_id' => 2,
															'reg_id' => '',
															'zip' => $this->input->post('zip'),
															'gender' => $this->input->post('gender'),
															'phone' => trim($this->input->post('phone')),
															'mobile' => trim($this->input->post('mobile')),
															'datestart' => '0000-00-00 00:00:00',
															'dateend' => '0000-00-00 00:00:00',
															'birthday' => date('Y-m-d',strtotime($birthday)),
															'address' => trim($this->input->post('address')),													
															'description' => trim($this->input->post('description')),
															'dateupdated' => date('Y-m-d H:i:s'),
															'approvedby' => $this->admin_id,
															'status' => $status
														);
														$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
														$status = 'success';
														$message = 'Data has been successfully updated. Thankyou';
														break;
													case FALSE:															
														$pass = trim($this->input->post('pass'));
														$confirm = trim($this->input->post('confirm'));
														switch($pass == ''){
															case TRUE:
																$status = 'error';
																$message = 'Sorry, you have not reset the password. cheers';
																break;
															case FALSE:
																switch($pass == $confirm){
																	case TRUE:
																		$update_ver = array(
																			'type_id' => 2,
																			'reg_id' => '',
																			'password' => md5($pass),
																			'zip' => $this->input->post('zip'),
																			'gender' => $this->input->post('gender'),
																			'phone' => trim($this->input->post('phone')),
																			'mobile' => trim($this->input->post('mobile')),
																			'datestart' => '0000-00-00 00:00:00',
																			'dateend' => '0000-00-00 00:00:00',
																			'birthday' => date('Y-m-d',strtotime($birthday)),
																			'address' => trim($this->input->post('address')),													
																			'description' => trim($this->input->post('description')),
																			'dateupdated' => date('Y-m-d H:i:s'),
																			'approvedby' => $this->admin_id,
																			'status' => $status
																		);
																		$this->dbfun_model->update_table(array('user_id' => $id, 'ccs_id' => $this->zone_id), $update_ver, 'usr');
																		$status = 'success';
																		$message = 'Data has been successfully updated. Thankyou';
																		break;
																	case FALSE:
																		$status = 'error';
																		$message = "Sorry, password doesn't match. Cheers";
																		break;
																}
																break;
														}
														break;
												}
												
												break;
										}
										break;
								}
								break;
						}
					}
					else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				else{
					$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|max_length[25]');
					$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean|max_length[50]');
					$this->form_validation->set_rules('email', 'Email Account', 'trim|required|xss_clean|max_length[50]|valid_email');
					$this->form_validation->set_rules('mobile', 'Mobile Phone', 'trim|required|xss_clean|max_length[15]');
					$this->form_validation->set_rules('phone', 'Fix Phone', 'trim|xss_clean|max_length[15]');
					$this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean|max_length[250]');
					$this->form_validation->set_rules('gender', 'Gender', 'numeric|required|xss_clean|max_length[1]');
					$this->form_validation->set_rules('status', 'Activation Status', 'numeric|required|xss_clean|max_length[1]');
					$this->form_validation->set_rules('birthday', 'Birth of Day', 'trim|required|xss_clean|max_length[10]');
					if($key == 'verifiedmember'){							
						$this->form_validation->set_rules('reg_id', 'Member ID', 'trim|required|xss_clean|max_length[15]');
						$this->form_validation->set_rules('datestart', 'Start Date', 'trim|required|xss_clean|max_length[10]');
						$this->form_validation->set_rules('dateend', 'Valid Date', 'trim|required|xss_clean|max_length[10]');
						$this->form_validation->set_rules('zip', 'Postal Code', 'numeric|xss_clean|max_length[5]');
						$this->form_validation->set_rules('description', 'Short Description', 'trim|xss_clean|max_length[250]');
					}elseif($key == 'reseller'){							
						$this->form_validation->set_rules('zip', 'Postal Code', 'numeric|xss_clean|max_length[5]');
					}else{							
						$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|max_length[50]');
						$this->form_validation->set_rules('description', 'Short Description', 'trim|required|xss_clean|max_length[250]');
					}
					if ($this->form_validation->run() === TRUE){
						switch($keys){
							case 'admin':
								$name = trim($this->input->post('name'));
								$cek = count($this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$this->zone_id.' and name = "'.$name.'"','adm'));
								switch($cek != 0){
									case TRUE:
										$status = "error";
										$message = "Name for ".$name." has already exist on the systems. Cheers";
										break;
									case FALSE:
										$un = trim($this->input->post('username'));
										$username = count($this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$this->zone_id.' and username = "'.$un.'"','adm'));
										switch($username == 1){
											case TRUE:
												$status = "error";
												$message = "Username for ".$un." has already exist on the systems. Please choose another username. Cheers";
												break;
											case FALSE:	
												$eml = trim($this->input->post('email'));
												$email = count($this->dbfun_model->get_all_data_bys('admin_id','ccs_id = '.$this->zone_id.' and email = "'.$eml.'"','adm'));
												switch($email == 1){
													case TRUE:
														$status = "error";
														$message = "Email for ".$eml." has already exist on the systems. Please choose another username. Cheers";
														break;
													case FALSE:
														$pass = trim($this->input->post('password'));
														$confirm = trim($this->input->post('confirm'));
														switch($pass == $confirm){
															case FALSE:																	
																$status = "error";
																$message = "Sorry, password doesn't match. Cheers";
																break;
															case TRUE:
																$custom_primary = array(
																	'ccs_id' => $this->zone_id,
																	'role_id' => $this->input->post('role_id'),
																	'language_id' => 2,
																	'name' => $name,
																	'username' => $un,
																	'email' => $eml,
																	'password' => MD5($pass),
																	'gender' => $this->input->post('gender'),
																	'phone' => trim($this->input->post('phone')),
																	'mobile' => trim($this->input->post('mobile')),
																	'birthday' => date('Y-m-d',strtotime($this->input->post('birthday'))),
																	'address' => trim($this->input->post('address')),															
																	'description' => trim($this->input->post('description')),
																	'datecreated' => date('Y-m-d H:i:s'),
																	'status' => $status
																);
																if (!is_dir('assets/'.$this->zone.'/admin'))
																{
																	mkdir('./assets/'.$this->zone.'/admin', 0777, true);
																}
																$value = $this->new_data('ccs_adm',$custom_primary, 'main_image','main_image', $this->zone.'/admin','jpeg|jpg|png');
																$status = "success";
																$message = "Data created successfully";
																break;
														}
														break;
												}
												break;
										}
										break;
								}
								break;
							case 'member':
								$name = trim($this->input->post('name'));
								$member_id = trim($this->input->post('reg_id'));									
								$cek = count($this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$this->zone_id.' and reg_id = "'.$member_id.'"','usr'));
								switch($cek != 0){
									case TRUE:
										$status = "error";
										$message = "Member ID for ".$member_id." has already exist on the systems. Cheers";
										break;
									case FALSE:
										$un = trim($this->input->post('username'));
										$username = count($this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$this->zone_id.' and username = "'.$un.'"','usr'));
										switch($username == 1){
											case TRUE:
												$status = "error";
												$message = "Username for ".$un." has already exist on the systems. Please choose another username. Cheers";
												break;
											case FALSE:	
												$eml = trim($this->input->post('email'));
												$email = count($this->dbfun_model->get_all_data_bys('user_id','ccs_id = '.$this->zone_id.' and email = "'.$eml.'"','usr'));
												switch($email == 1){
													case TRUE:
														$status = "error";
														$message = "Email for ".$eml." has already exist on the systems. Please choose another username. Cheers";
														break;
													case FALSE:
														$custom_verified = array(
															'ccs_id' => $this->zone_id,
															'language_id' => 2,
															'reg_id' => $member_id,
															'type_id' => 2,
															'name' => $name,
															'username' => $un,
															'email' => $eml,
															'password' => MD5($member_id),
															'gender' => $this->input->post('gender'),
															'phone' => trim($this->input->post('phone')),
															'mobile' => trim($this->input->post('mobile')),
															'birthday' => date('Y-m-d',strtotime($this->input->post('birthday'))),
															'datestart' => date('Y-m-d H:i:s',strtotime($this->input->post('datestart'))),
															'dateend' => date('Y-m-d H:i:s',strtotime($this->input->post('dateend'))),
															'address' => trim($this->input->post('address')),													
															'description' => trim($this->input->post('description')),
															'datecreated' => date('Y-m-d H:i:s'),
															'activation_key' => 'b03zc0n'.$this->zone.date('ymdHis'),
															'status' => $status
														);
														if (!is_dir('assets/'.$this->zone.'/user/member'))
														{
															mkdir('./assets/'.$this->zone.'/user/member', 0777, true);
														}
														$verified = $this->new_data('ccs_usr',$custom_verified, 'avatar','avatar', $this->zone.'/user/member','jpeg|jpg|png');
														$status = "success";
														$message = "Data created successfully";
														break;																
												}
												break;
										}											
										break;
								}
								break;
						}
					}
					else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type)&&($param == 'gallery')){
				$ids = $this->input->post('object_id');
				$ccs_key = $this->input->post('ccs_key');
				$lang = $this->input->post('language_id');
				$admin_id = $this->admin_id;
				$keys = trim($this->input->post('param2'));
				if($keys == 'portrait'){
					if($this->input->post('img_id')){
						$id = $this->input->post('img_id');
					}
					$ids = $ids;
				}else{
					$id = $ids;
				}
				$galtype = trim($this->input->post('istatus'));
				//if($id || ($id == 0)){
				if(isset($id)){
					switch($keys){
						case 'main':										
							if(isset($_FILES['image_square'])){
								$updates = $this->update_data_bys('*', array('object_id' => $id), 'ccs_o','image_square', $this->zone.'/'.$type,'image_square','jpeg|jpg|png');
							}else{
								$updates = $this->update_data_bys('*', array('object_id' => $id), 'ccs_o','','','','');
							}
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $updates, 'o');						
							$update = array(
								'dateupdated' => date('Y-m-d H:i:s'),
								'istatus' => $galtype
							);
							$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update, 'o');	
							$gallery = count($this->dbfun_model->get_all_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$galtype.'','ogl'));
							if($gallery != 0){
								 $gt = $this->dbfun_model->get_all_data_bys('gallery_id, istatus','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.'','ogl');
								foreach($gt as $g){
									if(($g->istatus == $galtype)&&($galtype != 2)){
										$update_gal = array(
											'dateupdated' => date('Y-m-d H:i:s'),
											'updatedby' => $admin_id,
											'istatus' => 2
										);											
										$this->dbfun_model->update_table(array('gallery_id' => $g->gallery_id,'object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_gal, 'ogl');	
									}
								}
							}	
							$status = "success";
							$message = "Data has been successfully updated. Thankyou.";								
							break;
						case 'gallery':
							$this->load->library('image_lib'); 
							$path = $this->zone.'/gallery';
							$path_thumb = $path.'/thumbnail';
							if (!is_dir('./assets/'.$path_thumb))
							{
								mkdir('./assets/'.$path_thumb, 0777, true);
							}
							
							$gal = $this->input->post('gallery_id');								
							$status = $this->input->post('status');		
							$gallery = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and gallery_id = '.$gal.'','ogl')->image_square;
							if(isset($_FILES['image_square'])){
								$updates = $this->update_data_bys('*', array('gallery_id' => $gal, 'object_id' => $id), 'ccs_ogl','image_square', $path,'image_square','jpeg|jpg|png|mp4');
								$file_path = './assets/'.$path_thumb.'/'.trim($gallery);
								if (file_exists($file_path))
								{
									unlink($file_path);
								}									
								$this->dbfun_model->update_table(array('gallery_id' => $gal, 'object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), $updates, 'ogl');	
								$thumb = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and gallery_id = '.$gal.'','ogl')->image_square;

								$this->load->library('image_lib');
								$img_cfg['image_library'] = 'gd2';
								$img_cfg['source_image'] = './assets/'.$path.'/'.$thumb;
								$img_cfg['maintain_ratio'] = TRUE;
								$img_cfg['create_thumb'] = TRUE;
								$img_cfg['new_image'] = './assets/'.$path_thumb;
								$img_cfg['width'] = 200;
								$img_cfg['height']	= 200;
								$img_cfg['file_name'] = $thumb;
								$this->image_lib->initialize($img_cfg);
								$this->image_lib->resize();
								$this->image_lib->clear();
								
							}else{
								$updates = $this->update_data_bys('*', array('gallery_id' => $gal, 'object_id' => $id), 'ccs_ogl','','','','');
							} 	
							
							$update = array(
								'dateupdated' => date('Y-m-d H:i:s'),
								'updatedby' => $admin_id,
								'title' => trim($this->input->post('title')),									
								'description' => trim($this->input->post('description')),
								'istatus' => $galtype,
								'status' => $status
							);
							$this->dbfun_model->update_table(array('gallery_id' => $gal, 'object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update, 'ogl');	
							if(($galtype == 0)||($galtype == 1)){
								$update = array(
									'dateupdated' => date('Y-m-d H:i:s'),
									'istatus' => 2
								);
								$cek_image = count($this->dbfun_model->get_data_bys('object_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$galtype.'','o'));
								if($cek_image != 0){
									$this->dbfun_model->update_table(array('object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update, 'o');	
								}
									
								$cek_gal = count($this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$galtype.' and gallery_id != '.$gal.' ','ogl'));
								if($cek_gal != 0){
									$gid = $this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$id.' and istatus = '.$galtype.' and gallery_id != '.$gal.' ','ogl')->gallery_id;
									$update_img = array(
										'dateupdated' => date('Y-m-d H:i:s'),
										'updatedby' => $this->admin_id,
										'istatus' => 2
									);											
									$this->dbfun_model->update_table(array('gallery_id' => $gid,'object_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_img, 'ogl');	
								}
							}
							$status = "success";
							$message = "Data has been successfully updated. Thankyou.";	
							break;	
						case 'portrait':
							$title = trim($this->input->post('title'));
							$desc = trim($this->input->post('description'));
							$path = $this->zone.'/'.$type;
							$file = 'image_square';
							$cek = $this->dbfun_model->get_data_bys('gallery_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$ids.' and gallery_id = '.$id.' and istatus = 3 and title = "'.$title.'" and description = "'.$desc.'"','ogl');														
							$cek_img = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$ids.' and gallery_id = '.$id.' and istatus = 3','ogl')->image_square;
							$config = unggah_berkas($path, $cek_img, 'png|jpg|jpeg');
							$this->upload->initialize($config);
															
							if (!$this->upload->do_upload($file))
							{	
								if(!$cek){				
									$update_img = array(
										'dateupdated' => date('Y-m-d H:i:s'),
										'title' => $title,
										'description' => $desc,
										'updatedby' => $this->admin_id
									);											
									$this->dbfun_model->update_table(array('gallery_id' => $id,'object_id' => $ids, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_img, 'ogl');	
									$status = "success";
									$message = "Data has been successfully updated. Thankyou.";	
								}
								else{
									$status = 'error';
									$message = 'No data updated. Cheers...';										
								}
							}else{
								$upload_data = $this->upload->data();
								if(!$cek){
									$update_img = array(
										'dateupdated' => date('Y-m-d H:i:s'),
										'title' => $title,
										'description' => $desc,
										'updatedby' => $this->admin_id
									);											
									$this->dbfun_model->update_table(array('gallery_id' => $id,'object_id' => $ids, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key ), $update_img, 'ogl');	
								}
								$status = "success";
								$message = "Data has been successfully updated. Thankyou.";	
							}
							
							break;	
					}
					
				}else{
					switch($keys){
						case 'portrait':	
							$path = $this->zone.'/'.$type;
							$file = 'image_square';
							if (!is_dir('assets/'.$path.'/'))
							{
								mkdir('./assets/'.$path.'/', 0777, true);
							}
							$cek_img = trim($this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and object_id = '.$ids.'','o')->image_square);
							if(!empty($cek_img)){									
								$img = explode('.',$cek_img);
								$nama_file = $img[0].'_portrait.'.$img[1];
								$config = unggah_berkas($path, $nama_file, 'png|jpg|jpeg');
								$this->upload->initialize($config);									
								if (!$this->upload->do_upload($file))
								{
									$status = 'error';
									$message = $this->upload->display_errors('', '');
								}else{
									$upload_data = $this->upload->data();
									$imagedata = array(
										'ccs_id' => $this->zone_id,
										'ccs_key' => $ccs_key,
										'category_id' => $this->input->post('category_id'),
										'object_id' => $ids,
										'language_id' => 2,
										'image_square' => $nama_file,
										'datecreated' => date('Y-m-d H:i:s'),
										'dateupdated' => date('Y-m-d H:i:s'),
										'createdby' => $admin_id,
										'updatedby' => $admin_id,
										'title' => trim($this->input->post('title')),									
										'description' => trim($this->input->post('description')),
										'status' => 1,
										'istatus' => 3
									);	
									
									$inv = $this->new_data('ccs_ogl',$imagedata, '','', '','');
									$status = "success";
									$message = "Data has been successfully created. Thankyou.";	
								}	
							}else{
								$status = "error";
								$message = "Sorry, you must upload image for website view first. Cheers.";	
							}
							//$status = "error";
							//$message = "Data has been successfully created. Thankyou.".$nama_file;	
						break;
					}
				}
				
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) && ($param == 'relation')){
				$id = $this->input->post('rl_id');
				$obj = $this->input->post('object_id');
				$cat = $this->input->post('category_id');
				$ccs_key = $this->input->post('ccs_key');
				$lang = $this->input->post('language_id');
				$admin_id = $this->admin_id;
				$catid = $this->input->post('catid');
				$oid = $this->input->post('oid');
				$url_key = $this->input->post('url_key');
				$sts = $this->input->post('sts');
				$is_okay = FALSE;
				if ($this->form_validation->run('ccs_orl') === TRUE){
					
					$mname = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and module_id = '.$url_key.'','mdl')->name;
					if($catid == 0){
						$oid = 0;
						$title = $this->dbfun_model->get_data_bys('name','ccs_id = '.$this->zone_id.' and module_id = '.$url_key.'','mdl')->name;
					}else{
						$oid = $this->input->post('oid');
						if($oid == 0){
							$title = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and category_id = '.$catid.' and ccs_key = '.$url_key.'','oct')->title;
						}else{
							$cate = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and category_id = '.$catid.' and ccs_key = '.$url_key.'','oct')->title;
							$o = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$oid.' and category_id = '.$catid.' and ccs_key = '.$url_key.'','odc')->title;
							$title = $cate.'/'.$o;
						}
					}
					if($id){//update
						$this->dbfun_model->del_tables(array('rl_id' => $id, 'ccs_id' => $this->zone_id, 'ccs_key' => $ccs_key), 'orl');
						$is_okay = TRUE;
						$status = "success";
						$message = "Data has been updated. Thankyou.";	
					}
					else{
						$cek = $this->dbfun_model->get_data_bys('rl_id','ccs_id = '.$this->zone_id.' and ccs_key = '.$ccs_key.' and url_key = '.$url_key.' and category_id = '.$cat.' and object_id = '.$obj.' and catid = '.$catid.' and oid = '.$oid.'','orl');
						if($cek){								
							$status = "error";
							$message = "Data already exist on the system. Thankyou.";	
						}
						else{		
							$is_okay = TRUE;
							$status = "success";
							$message = "Data has been successfully created. Thankyou.";	
						}
					}
					
					if($is_okay){
						$rel_data = array(
							'ccs_id' => $this->zone_id,
							'ccs_key' => $ccs_key,
							'url_key' => $url_key,
							'category_id' => $cat,
							'object_id' => $obj,
							'catid' => $catid,
							'oid' => $oid,									
							'createdby' => $this->admin_id,
							'updatedby' => $this->admin_id,
							'datecreated' => date('Y-m-d H:i:s'),
							'status' => $sts,
							'link' => str_replace(' ','_',strtolower($title)),
							'module' => strtolower($mname)
						);	
						
						$inv = $this->new_data('ccs_orl',$rel_data, '','', '','');
					}
				}
				else{
					$status = 'error';
					$message = validation_errors();
				}
				
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type) && ($param == 'cnf')){
				// Ini dhil, buat fungsi konfirmasi
				$id = $this->input->post('param2');
				$trx_code = trim($this->input->post('trx_code'));
				if($id){
					$trigger = '';
					if($status == 1){
						$valid = '[ '.strtoupper($this->zone).' ] Payment Confirmation Accepted at INV#'.$trx_code.'';
						$status = "success";
						$m_status = '<h2 style="color:#1AB394">PAID</h2>';
						$e_status = 'paid';
						$message = "Payment Confirmation has been validated. Thankyou.";	
						$notes = 'Payment confirmation for INV#'.$trx_code.' has been validated. Thank you for your transactions';
						$trigger = 'valid';
					}
					elseif($status == 2){
						$valid = '[ '.strtoupper($this->zone).' ] Payment Confirmation Rejected at INV#'.$trx_code.'';
						$status = "success";
						$m_status = '<h2 style="color:#ED5565">REJECTED</h2>';
						$e_status = 'rejected';
						$message = "Payment Confirmation has been rejected. Thankyou.";	
						$notes = 'Payment confirmation for INV#'.$trx_code.' has been rejected. Please upload another valid data. Cheers';
					}
					if(($status)&&($trigger == 'valid')){
						$list = $this->dbfun_model->get_all_data_bys('ccs_key,trx_code,cat_id,object_id,datecreated,datevalid,total,price,varian_id,info_id,ticket','ccs_id = '.$this->zone_id.' and trx_code = '.$trx_code.'','ccs_trx');
						$count_prc = '';
						foreach($list as $l){
							$o = $this->dbfun_model->get_data_bys('image_square','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','o');
							$odc = $this->dbfun_model->get_data_bys('title','ccs_id = '.$this->zone_id.' and object_id = '.$l->object_id.'','odc');
							$cat = $this->dbfun_model->get_data_bys('title','ccs_oct.ccs_id = '.$this->zone_id.' and status != 0 and category_id = '.$l->cat_id.'','oct');
							if($l->varian_id != 0){
								$settings_id = $this->dbfun_model->get_data_bys('settings_id','ccs_prv.ccs_id = '.$this->zone_id.' and ccs_prv.object_id = '.$l->object_id.' and ccs_prv.varian_id = '.$l->varian_id.'','prv')->settings_id;
								$varian = $this->dbfun_model->get_data_bys('ccs_ocs.title','ccs_ocs.ccs_id = '.$this->zone_id.' and ccs_ocs.settings_id = '.$settings_id.'','ocs,oct');
								if(!empty($varian)){
									$varian = $varian->title;
								}else{
									$varian = '';
								}
							}else{
								$varian = '';
							}
							if(!empty($l->object_id != 0)){//expedisi not listed here
								$count_prc += $l->price;
								$datas2[] = (object) array_merge((array) $l, array('varian'=>$varian,'title'=>$odc->title,'image'=>base_url('assets/'.$zone.'/product/'.$o->image_square)));
							}else{
								$expedisi_type = $l->ticket;
								$expedisi_price = $l->price;
							}
							$this->update_stock($l);
						}
						
						$total_trx = array(
							'count_prc'=> $count_prc,
							'date'=> date('l, d F Y H:i:s',strtotime($list[0]->datecreated)),
							'date_valid'=> date('l, d F Y H:i:s',strtotime($list[0]->datevalid)),
						);
						$user = $this->dbfun_model->get_data_bys('name,phone,mobile,email,address,address_2','ccs_id = '.$this->zone_id.' and info_id = '.$list[0]->info_id.'','ccs_tri');
						$customer = array(
							'name' => $user->name,
							'phone' => $user->phone,
							'mobile' => $user->mobile,
							'email' => $user->email,
							'address' => $user->address_2,
						);
						
						$email = $user->email;
						$from = 'online';
						$subject = $valid;
						$bank = $this->dbfun_model->get_all_data_bys('char_1,char_2,char_3','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "bank" and status = 1','orc');
						$froms = $this->dbfun_model->get_all_data_bys('url_id,char_1,char_2,char_3,char_4,char_5,char_6,char_7','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "info" and status = 1','orc');
						$cek = $this->dbfun_model->get_data_bys('name,logo,color_id,active_user,type_id','LOWER(name) like "'.strtolower($this->zone).'"','zone');
						$color = $this->dbfun_model->get_data_bys('primaryc, secondaryc,background','color_id = '.$cek->color_id.' and ccs_id = '.$this->zone_id.'','clr');
						if(isset($color->primaryc)){
							$menu['primaryc'] = $color->primaryc;
						}else{
							$menu['primaryc'] = '';
						}
						
						$content = array(
								'm_status'=> $m_status,
								'data'=> $datas2,
								'total_trx'=> $total_trx,
								'customer'=> $customer,
								'from'=> $froms,
								'zone'=> $this->zone,
								'logo' => base_url('assets').'/'.$this->zone.'/'.$cek->logo,
								'style'=> $menu['primaryc'],
								'expedisi'=> array('price'=>$expedisi_price,'type'=>$expedisi_type),
								'notes' => $notes,
								'e_status' => $e_status,
								'link_to_confirm'=> $this->menu['link'].'/confirm?'.$this->get_hash($list[0]->info_id.'-INV'.$trx_code,'en')
							);
						
						$table = 'ccs_rcp';
						$code = 'TRXON';
						$code2 = 'RCP';
						$code3 = 'CNF';
						$dates = date('YmdHis');
						$dates2 = date('Y-m-d H:i:s');
						$where = 'ccs_id = '.$this->zone_id.' and trx_code like "'.$trx_code.'" and code like "'.$code.'"';
						$where2 = 'ccs_id = '.$this->zone_id.' and trx_code like "'.$trx_code.'" and code like "'.$code3.'"';
						if (!is_dir('assets/'.$this->zone.'/trx/'))
						{
							mkdir('./assets/'.$this->zone.'/trx/', 0777, true);
						}
						if (!is_dir('assets/'.$this->zone.'/trx/rcp/'))
						{
							mkdir('./assets/'.$this->zone.'/trx/rcp/', 0777, true);
						}
						$file_name = 'file-'.date('YmdHis').'.pdf';
						$path = './assets/'.$this->zone.'/trx/rcp/'.$file_name;
						
						$custom_data = array(
							'ccs_id'=> $this->zone_id,
							'ccs_key'=> $list[0]->ccs_key,
							'type'=> 0,
							'code'=> $code2,
							'icode'=> 'INV',
							'codex'=> $dates,
							'icodex'=> $trx_code,
							'description'=> $notes,
							'datecreated'=> $dates2,
							'dateupdated'=> $dates2,
							'createdby'=> $this->admin_id,
							'updatedby'=> $this->admin_id,
							'status'=> 1,
						);
						$this->new_data($table,$custom_data,'','','','');
						$content['code'][] = '#RCP'.$dates;
						$content['code'][]= '#INV'.$trx_code;
						$content['code'][] = date('l, d F Y H:i:s',strtotime($dates2));
						$this->create_pdf($path,$content);
						$custom_s = array(
									'status'=> 1,
									'dateupdated'=> $dates2,
									'updatedby'=> $this->admin_id,
								);
						$this->dbfun_model->update_table($where, $custom_s, 'ccs_trx');
						$this->dbfun_model->update_table($where2, $custom_s, 'ccs_inv');							
						$attach_path = $path;//contoh hahaha
						if($this->zone == 'shisha'){
							$this->send_mail_attach($email,$subject,$content,$attach_path,'email/update_trx_simple',$from);
						}else{
							$this->send_mail_attach($email,$subject,$content,$attach_path,'email/update_trx',$from);
						}
					}
					elseif(($status)&&($trigger != 'valid')){
						
						$where2 = 'ccs_id = '.$this->zone_id.' and trx_code like "'.$trx_code.'" and code like "CNF"';
						$custom_s = array(
							'status'=> 2,
							'dateupdated'=> date('Y-m-d H:i:s'),
							'updatedby'=> $this->admin_id,
						);
						$this->dbfun_model->update_table($where2, $custom_s, 'ccs_inv');
						$list = $this->dbfun_model->get_data_bys('info_id','ccs_id = '.$this->zone_id.' and trx_code = '.$trx_code.'','ccs_trx');
						
						$user = $this->dbfun_model->get_data_bys('name,phone,mobile,email,address,address_2','ccs_id = '.$this->zone_id.' and info_id = '.$list->info_id.'','ccs_tri');
						$email = $user->email;
						$from = 'online';
						$subject = $valid;
						$froms = $this->dbfun_model->get_all_data_bys('url_id,char_1,char_2,char_3,char_4,char_5,char_6,char_7','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "info" and status = 1','orc');
						$cek = $this->dbfun_model->get_data_bys('name,logo,color_id,active_user,type_id','LOWER(name) like "'.strtolower($this->zone).'"','zone');
						$color = $this->dbfun_model->get_data_bys('primaryc, secondaryc,background','color_id = '.$cek->color_id.' and ccs_id = '.$this->zone_id.'','clr');
						if(isset($color->primaryc)){
							$menu['primaryc'] = $color->primaryc;
						}else{
							$menu['primaryc'] = '';
						}
						$title = 'Payment Rejected';
						
						$content = array(
								'logo'=> base_url('assets').'/'.$this->zone.'/'.$cek->logo,
								'from'=> $froms,
								'zone'=> $this->zone,
								'url'=> $this->menu['link'],
								'style'=> $menu['primaryc'],
								'title' => $title,
								'notes' => $notes,
								'link_to_confirm'=> $this->menu['link'].'/confirm?'.$this->get_hash($list->info_id.'-INV'.$trx_code,'en')
							);
						if($this->zone == 'shisha'){
							$this->send_mail($email,$subject,$content,'email/rejected_simple',$from);
						}else{
							$this->send_mail($email,$subject,$content,'email/rejected',$from);
						}
					}
					//$subject = $valid;
				}else{
					$status = 'error';
					$message = validation_errors();
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
			elseif(($type)&&($param == 'dlv')){
				$id = $this->input->post('rcp_id');
				$codex = $this->input->post('codex');
				$trx_code = trim($this->input->post('trx_code'));
				if($status == 2){
					$this->form_validation->set_rules('codex','Shipping Number', 'trim|required|max_length[12]|xss_clean');
				}
				else{
					$this->form_validation->set_rules('codex','Shipping Number', 'trim|max_length[12]|xss_clean');
				}
				$this->form_validation->set_rules('trx_code','Shipping Number', 'trim|required|numeric[12]|xss_clean');
				$this->form_validation->set_rules('status','status', 'trim|numeric[1]|required|xss_clean');
				$this->form_validation->set_rules('description','status', 'trim|max_length[250]|xss_clean');
				$table = 'ccs_rcp';
				$ccs_key = $this->input->post('ccs_key');
				$dates = date('YmdHis');
				$dates2 = date('Y-m-d H:i:s');
				if(!$id){
					if($this->form_validation->run() == TRUE ){
						$m = 'Packaging';
						$custom_data = array(
							'ccs_id'=> $this->zone_id,
							'ccs_key'=> $ccs_key,
							'type'=> 1,
							'code'=> 'DLV',
							'icode'=> 'RCP',
							'codex'=> $dates,
							'icodex'=> $trx_code,
							'description'=> $codex.'<br>'.trim($this->input->post('description')),
							'datecreated'=> $dates2,
							'dateupdated'=> $dates2,
							'createdby'=> $this->admin_id,
							'updatedby'=> $this->admin_id,
							'status'=> $status,
						);
						if($status == 2){
							$valid = '[ '.strtoupper($this->zone).' ] Shipping Product';
							
							
							$list = $this->dbfun_model->get_all_data_bys('info_id,object_id,ticket','ccs_id = '.$this->zone_id.' and trx_code = '.$trx_code.'','ccs_trx');
							$user = $this->dbfun_model->get_data_bys('name,phone,mobile,email,address,address_2','ccs_id = '.$this->zone_id.' and info_id = '.$list[0]->info_id.'','ccs_tri');
							$email = $user->email;
							$from = 'online';
							$subject = $valid;
							$froms = $this->dbfun_model->get_all_data_bys('url_id,char_1,char_2,char_3,char_4,char_5,char_6,char_7','ccs_id = '.$this->zone_id.' and LOWER(char_7) like "info" and status = 1','orc');
							$cek = $this->dbfun_model->get_data_bys('name,logo,color_id,active_user,type_id','LOWER(name) like "'.strtolower($this->zone).'"','zone');
							$color = $this->dbfun_model->get_data_bys('primaryc, secondaryc,background','color_id = '.$cek->color_id.' and ccs_id = '.$this->zone_id.'','clr');
							if(isset($color->primaryc)){
								$menu['primaryc'] = $color->primaryc;
							}else{
								$menu['primaryc'] = '';
							}
							$title = 'Shipping Product for #INV'.$trx_code.'';
							$shipping_type = '';
							foreach($list as $l){
								if($l->object_id == 0){
									$shipping_type = strtoupper($l->ticket);
								}
							}
							$add = '<h3>Your shipping code: <b>'.$codex.'</b></h3><h3>Shipping Provider: <b>'.$shipping_type.'</b></h3><br>';
							$content = array(
									'logo'=> base_url('assets').'/'.$this->zone.'/'.$cek->logo,
									'from'=> $froms,
									'zone'=> $this->zone,
									'url'=> base_url($this->zone),
									'style'=> $menu['primaryc'],
									'title' => $title,
									'notes' => $add.trim($this->input->post('description')),
									'link_to_confirm'=> $this->menu['link'].'/confirm?'.$this->get_hash($list[0]->info_id.'-INV'.$trx_code,'en')
								);
							
							if($this->zone == 'shisha'){
								$this->send_mail($email,$subject,$content,'email/rejected_simple',$from);
							}else{
								$this->send_mail($email,$subject,$content,'email/rejected',$from);
							}
							
							$m = $title;
						}
						$this->new_data($table,$custom_data,'','','','');
						$status = 'success';
						$message = $m;
					}else{
						$status = 'error';
						$message = validation_errors();
					}
				}
				else{
					
				}
				echo json_encode(array('status' => $status, 'm' => $message));
				exit();
			}
		
		}else{
			redirect($this->login);
		}
	}
	
	protected function date_valid($date){
	    $parts = explode("-", $date);
	    if (count($parts) == 3) {      
		    if (checkdate($parts[1], $parts[2], $parts[0]))
		    {
		    	return TRUE;
		    }
	    }
	    $this->form_validation->set_message('date_valid', 'Tanggal harus dengan format yyyy-mm-dd');
	    return false;
	}
}