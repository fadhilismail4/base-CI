<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function unggah_berkas($path, $nama_file, $type)
{
	$config['upload_path'] = './assets/'.$path.'/';
	$config['allowed_types'] = $type;
	$config['max_size']	= '3072';	
	$config['file_name'] = $nama_file;
	$config['overwrite'] = TRUE;

	return $config;
}

function unggah_file($path, $nama_file, $type)
{
	$config['upload_path'] = './assets/'.$path.'/';
	$config['allowed_types'] = $type;
	$config['max_size']	= '102400';	
	$config['file_name'] = $nama_file;
	$config['overwrite'] = TRUE;

	return $config;
}

/* End of file upload_file_helper.php */