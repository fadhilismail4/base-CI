
// When the window has finished loading google map
google.maps.event.addDomListener(window, 'load', init);

function init() {
	// Options for Google map
	// More info see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
	var myLatLng = {lat: -6.920867, lng: 107.609558};
	var mapOptions1 = {
		zoom: 13,
		center: myLatLng,
		// Style for Google Maps
		styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
	};
	
	var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">CITARUM II</h1>'+
      '<div id="bodyContent">'+
	  '<div class="col-md-3" style="padding: 0;">'+
	  '<img src="http://yapyek.com/assets/Upload/Photo/Arah%20Cirebon.jpg" style="width:100%;">'+
	  '</div>'+
	  '<div class="col-md-9">'+
      '<p><b>CITARUM II</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
      'Heritage Site.</p>'+
	  '<p>Lokasi (KM): BDG / 12</p>'+
	  '<p>Status: 2 (Nilai Kondisi)</p>'+
	   '<p>Attribution: CITARUM, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
      'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
	  '<p>Read More >></p>'+
      '(last visited June 22, 2009).</p>'+
      '</div>'+
	  '</div>'+
      '</div>';

	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	
	
	// Get all html elements for map
	var mapElement1 = document.getElementById('map1');

	// Create the Google Map using elements
	var map1 = new google.maps.Map(mapElement1, mapOptions1);
	
	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map1,
		title: 'Hello World!'
	  });
	
	
	marker.setMap(map1);
	marker.addListener('click', function() {
		infowindow.open(map1, marker);
	});
}