// ajax to call view
			$('.menu').on("click", function(e) {
				e.preventDefault();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var obj = $(this).attr('id');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					lang : 1
				};
				$('.nav li').removeClass('active');
				$(this).parent('li').addClass('active');
				$(this).parent('li').parent('ul').parent('li').addClass('active');
				$('.content').empty();
				$('#button-action').empty();
				$('.content').append('<div class="sk-spinner sk-spinner-wandering-cubes"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
				var url = "<?php echo site_url('admin');?>/"+obj+"/view";
				$.ajax({
					url : url,
					type: "POST",
					dataType: 'html',
					data: data,
					success: function(html){
						$('.content').empty();
						$('.content').append(html);
						$('.btn').each(function(){$(this).removeAttr('disabled');});
						eval(document.getElementById("crud_script").innerHTML);
					},
					error: function(jqXHR){ location.reload(true); }
				});
				(function($){
					$( document ).ajaxComplete(function() {
						$('.create').each(function(){
							$(this).html('Create');
						});
						$('[name="inputan"]').each(function(){
							$(this).val('');
						});
						$('[name="inputan_array"]').each(function(){
							$(this).chosen().val('');
						});
					});
				})(jQuery);
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			// end ajax to call view
			
			// ajax to call view inside page
			$('.detail').on("click", function(e) {
				e.preventDefault();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var obj = $(this).attr('id');
				var data = {
					<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>',
					id : $(this).data('detail')
				};
				$('.detail_content').empty();
				$('.detail_content').append('<div class="sk-spinner sk-spinner-wandering-cubes" style="    position: fixed;"><div class="sk-cube1"></div> <div class="sk-cube2"></div></div>');
				var url = "<?php echo site_url('admin');?>/"+obj+"/view_detail";
				$.ajax({
					url : url,
					type: "POST",
					dataType: 'html',
					data: data,
					success: function(html){
						$('.detail_content').empty();
						$('.detail_content').append(html);
						$('.btn').each(function(){$(this).removeAttr('disabled');});
						eval(document.getElementById("crud_script").innerHTML);
					},
					error: function(jqXHR){ location.reload(true); }
				});
				function removeCrud( itemid ) {
					var element = document.getElementById(itemid); // will return element
					elemen.parentNode.removeChild(element); // will remove the element from DOM
				}
				removeCrud('crud_script');
			});
			// end ajax to call view inside page
	
			// ajax to create
			$('.create').on("click", function(e) {
				e.preventDefault();
				$('#success').empty();
				$('#fail').empty();
				$('.btn').each(function(){
					$(this).attr('disabled', 'disabled');
				});
				var obj = $(this).attr('id');
					file = $(".file").attr('id');
				function get_val(){
					var item_obj = {};
						$('[name="inputan"]').each(function(){
							item_obj[this.id] = this.value;
						});
					var item_array = {};
						$('[name="inputan_array"]').each(function(){
							item_array[this.id] = $(this).chosen().val();
						});
					var item_summer = {};
						$('[name="inputan_summer"]').each(function(){
							item_summer[this.id] = $(this).code();
						});
					$.extend($.extend($.extend(item_obj, item_array), item_summer),{<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo trim($this->security->get_csrf_hash()); ?>'});
					return item_obj;
				}
				var data = get_val();
				$.ajax({
					url : "<?php echo site_url('admin');?>/"+obj+"/create",
					secureuri: false,
					fileElementId: file,
					type: "POST",
					dataType: 'json',
					data: data,
					success: function(data){
						if (data.status == "success"){
							$('<p>'+data.m+'</p>').appendTo('#success');
							scrolltonote('.ibox-content');
							$('#success').show();
							$('#success').fadeTo(2000, 500).slideUp(500);
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}else{
							$('<p>'+data.m+'</p>').appendTo('#fail');
							scrolltonote('.ibox-content');
							$('#fail').show();
							$('#fail').fadeTo(4000, 500).slideUp(500); 
							$('.btn').each(function(){$(this).removeAttr('disabled');});
						}
					}
				});
			});
			// end ajax to create
			
			// ajax to view update
			$('.view_update').on("click", function(e) {
				e.preventDefault();
				var obj = $(this).attr('id');
					lang = $(this).data('lang');
					id = $(this).data('id');
				function get_val(){
					var item_obj = {};
						$('[name="inputan"]').each(function(){
							item_obj[this.id] = $(this).html();
						});
					var item_array = {};
						$('[name="inputan_array"]').each(function(){
							item_array[this.id] = $(this).chosen().html();
						});
					var item_summer = {};
						$('[name="inputan_summer"]').each(function(){
							item_summer[this.id] = $(this).html();
						});
					$.extend($.extend(item_obj, item_array), item_summer);
					return item_obj;
				}
				var data = get_val();
				$("#"+obj+"")[0].click();
				(function($){
					$( document ).ajaxComplete(function() {
						$.each(data, function(i, val) {
							$('[name="inputan"]').each(function(){
								if( $(this).attr('id') == i ){
									$('#'+i).val(val.trim());
								}
							});
							$('[name="inputan_array"]').each(function(){
								if( $(this).attr('id') == i ){
									$('#'+i).chosen().val(val.trim());
								}
							});
							$('[name="inputan_summer"]').each(function(){
								if( $(this).attr('id') == i ){
									$('#'+i).code(val.trim());
								}
							});
						});
						$('.create').html('Update');
					});
				})(jQuery);
			});
			// end ajax to view update